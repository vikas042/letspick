#import "ApxorSDK/ApxorPlugin.h"
#import "ApxorSDK/APXLogger.h"
#import "ApxorSDK/APXEvent.h"

extern const int kAPXRTAPluginVersion;


@interface UIViewController (APXExtension)

- (NSString *)apxTitle;

@end


@interface APXRTAPlugin : NSObject<ApxorPlugin, APXEventListener>

@property (readwrite) BOOL actionInProgress;

+ (instancetype)sharedInstance;

@end
