#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ApxorSDK/ApxorPlugin.h"
#import "ApxorSDK/APXEvent.h"


@interface APXInapp : NSObject<UIWebViewDelegate,APXEventListener>

+ (instancetype)sharedInstance;
- (void)buildAndShowInAppWithUIConfig:(NSDictionary *)config;

@end
