//
//  APXRTAUtilities.h
//  APXRTAPlugin
//
//  Created by Uday Koushik on 28/10/19.
//  Copyright © 2019 Apxor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, APXPosition) {
    APXTop,
    APXBottom,
    APXLeft,
    APXRight
};

typedef NS_ENUM(NSUInteger, APXNudgePosition) {
    APXNudgeLeft,
    APXNudgeRight,
    APXNudgeTopCenter,
    APXNudgeCenter,
};

@interface APXRTAUtilities : NSObject

+ (UIView *)getViewWithIdentifier:(NSString *)identifier inView:(UIView *)view;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (UIViewController *)topViewController:(UIViewController *)rootViewController;
+ (UIView *)topView;
+ (void)logEventWithTitle:(NSString*)title messageName:(NSString*)messageName identifier:(NSString*)identifer;
+ (double) angleWithValue:(int) value;
+ (CGSize)getBoundingRectSizeForText:(NSString*)text withFontSize:(CGFloat)fontSize inView:(UIView*)view;
+ (CGSize)getBoundingRectSizeForText:(NSString*)text withStyle:(NSString*)style andFontSize:(CGFloat)fontSize withMaxSize:(CGSize)maxSize;
+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData;
+ (APXPosition)positionForString:(NSString *)direction;
+ (APXNudgePosition)nudgePositionForString:(NSString *)direction;
@end

NS_ASSUME_NONNULL_END
