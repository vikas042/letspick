#import <Foundation/Foundation.h>
#import <sys/utsname.h>
#import "APXTypes.h"


@class UIApplication;
@interface APXUtilities : NSObject

+(void) initializeDateFormatter;
+(APXTime) installationDate;
+(BOOL) isFirstSession;
+(NSString*) deviceName;

+(NSDictionary*) addTwoDictionaries:(NSDictionary*) one and: (NSDictionary*) two;
+(NSDate*) dateFromAPXDateString:(NSString*) dateString;
+(NSString*) dateAPXStringFromNSDate:(NSDate*) date;
+(APXTime) timeIntervalFromAPXDateString:(NSString*) dateString;
+(NSString*) dateAPXStringFromTimeInterval:(APXTime) timeInterval;
+(NSString*) getHashForModule:(NSString*) module;
+(void) saveHash:(NSString*) value ForModule:(NSString*) module;
+(BOOL) saveConfig:(NSData*) config atPath:(NSString*) path;
+(NSDictionary *) readConfig:(NSString*) filePath;

#pragma mark - JSON Utility Methods
+(NSData*) jsonDataFromNSDictionary:(NSDictionary*) dictionary;
+(NSDictionary*) dictionaryFromNSData:(NSData*) data;
+(NSDictionary*) dictionaryFromJsonString:(NSString*) string;
+(NSString*) jsonStringFromNSDictionary:(NSDictionary*) dictionary;
+(NSArray*) arrayFromJsonString:(NSString*) string;
+(NSString*) jsonStringFromNSArray:(NSArray*) array;

@end
