#ifndef APXPostman_h
#define APXPostman_h

NS_ASSUME_NONNULL_BEGIN

@interface APXPostman : NSObject

- (void)getDataFromURLPath:(NSString *)path completionHandler:(void (^)(NSData *, NSURLResponse *, NSError *))completionHandler;
- (void)postDataToURL:(NSString*)path withRequestBodyDictionary:(NSDictionary*)requestData completionHandler:(void (^)(NSData *, NSURLResponse *, NSError *))completionHandler;
- (void)makeDeleteRequestToURLPath:(NSString *)path completionHandler:(void (^)(NSData *, NSURLResponse *, NSError *))completionHandler;

@end

NS_ASSUME_NONNULL_END

#endif /* APXPostman_h */
