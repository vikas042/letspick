#import <Foundation/Foundation.h>
#import "ApxorSDK/ApxorPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@interface APXContextEvaluator : NSObject<ApxorPlugin>

+ (instancetype)sharedInstance;
- (void)parseConfiguration:(NSDictionary*)configuration;

@end

NS_ASSUME_NONNULL_END
