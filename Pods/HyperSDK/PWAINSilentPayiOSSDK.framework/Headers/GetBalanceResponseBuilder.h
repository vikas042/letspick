@protocol GetBalanceResponseBuilder <NSObject>

- (void) withBalance:(NSString *) balance;

- (void) withCurrencyCode:(NSString *) currencyCode;

@end


