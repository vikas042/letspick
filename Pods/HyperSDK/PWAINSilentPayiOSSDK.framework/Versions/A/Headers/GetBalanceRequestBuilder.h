@protocol GetBalanceRequestBuilder <NSObject>

- (void) withMerhcnatId:(NSString *) merchantId;

- (void) withIsSandbox:(BOOL) isSandbox;

@end

