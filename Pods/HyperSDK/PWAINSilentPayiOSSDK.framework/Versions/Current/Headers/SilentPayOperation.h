/**
 * Enum values for operations for Pay With Amazon IN SDK
 */
typedef NS_ENUM(NSInteger, SilentPayOperation)
{
    AUTHORIZE,
    GET_BALANCE,
    PROCESS_CHARGE,
    GET_CHARGE_STATUS
};

