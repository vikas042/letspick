//
//  Hyper.h
//  HyperSDK
//
//  Copyright © 2018 Juspay Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Hyper : UIView

/**
 Return the current version of SDK.

 @return Version number in string representation.
 
 @since v0.1
 */
+(NSString * _Nonnull)HyperSDKVersion;

/**
 Response block for communicating between service to callee.
 
 @param status Status after execution of service.
 @param responseData Response from service once execution is complete.
 @param error Error object with details if any error has occurred.
 
 @since v0.1
 */
typedef void (^HyperResponseBlock)(int status, id _Nullable responseData, NSError * _Nullable error);

/**
 Response block for communicating logs between service to callee.
 
 @param logData Logs being passed from service.
 
 @since v0.1
 */
typedef void (^HyperLogBlock)(id _Nonnull logData);

/**
 Callback block for communicating between callee to service .
 
 @param data Data being passed for service.
 
 @since v0.1
 */
typedef void (^CallbackBlock)(NSDictionary* _Nonnull data);

/**
 Callback block for communicating between callee to service .
 
 @param data Data being passed from service to callee.
 @param callback Callback to be triggered if required.
 
 @since v0.1
 */
typedef void (^HyperCommunicationBlock)(id _Nonnull data, CallbackBlock _Nonnull callback);

/**
 Data passed by calling app.Contains services to start - Mandatory to pass {"service":"service to be started"}.
 @warning `data` must have a valid service of type `{"service":"service to be started"}`.
 
 @since v0.1
 */
@property (nonatomic, strong, nonnull) NSDictionary *data;

/**
 * Handles the redirection back to the app from the PWA UI
 *
 * @param url the redirect URL
 * @param sourceApplication the sourceApplication where it comes from
 *
 * @return whether the response with the URl was handled successfully or not.
 *
 * @since v0.1
 */
+ (BOOL)handleRedirectURL:(NSURL * _Nonnull)url sourceApplication:(NSString * _Nonnull)sourceApplication;

/**
 For updating assets and configs.
 */
- (void)preFetch:(NSString * _Nonnull)clientId;

///---------------------
/// @name Hyper entry points
///---------------------

/**
 Entry point for starting hyper.
 
 @param viewController Reference ViewController marked as starting point of view.
 @param data Data params to be passed for service - Mandatory to pass {"service":"service to be started"}.
 @param callback HyperResponse callback returns status and additional data required to calling process.
 @warning `data` must have a valid service of type `{"service":"service to be started"}`.
 
 @since v0.1
 */
- (void)startViewController:(nonnull UIViewController*)viewController data:(nonnull NSDictionary*)data callback:(nonnull HyperResponseBlock)callback;

/**
 Entry point for starting hyper.
 
 @param viewController Reference ViewController marked as starting point of view.
 @param data Data params to be passed for service - Mandatory to pass {"service":"service to be started"}.
 @param logs HyperLog callback returns logs from hyper modules.
 @param comm HyperCommunication used to pass data to and from calling service in current running service.
 @param callback HyperResponse callback returns status and additional data required to calling process.
 @warning `data` must have a valid service of type `{"service":"service to be started"}`.
 
 @since v0.1
 */
- (void)startViewController:(nonnull UIViewController*)viewController data:(nonnull NSDictionary*)data logCallback:(nullable HyperLogBlock)logs commBlock:(nullable HyperCommunicationBlock)comm callback:( nonnull HyperResponseBlock)callback;



///---------------------
/// @name Payment Browser Properties
///---------------------

/**
@property shouldLoadPaymentEndUrl
@brief Set it as true if the end URL should be loaded after payment completion. Default is false.
*/
@property (nonatomic) Boolean shouldLoadPaymentEndUrl;

/**
@property navigationBarTitle
@brief The title text to be displayed on the navigation bar. No default value.
*/
@property (nonatomic, strong) NSString * _Nonnull navigationBarTitle;

/**
@property navigationBarTitleTextColor
@brief The navigation bar's title text color. Default color is black.
*/
@property (nonatomic, strong) UIColor * _Nonnull navigationBarTitleTextColor;

/**
@property navigationBarItemTitle
@brief The title text to be displayed on the navigation left bar item. Default value is "Close".
*/
@property (nonatomic, strong) NSString * _Nonnull navigationBarItemTitle;

/**
@property navigationBarItemTintColor
@brief The navigation left bar item's tint color. Default color is black.
*/
@property (nonatomic, strong) UIColor * _Nonnull navigationBarItemTintColor;

/**
@property navigationBarBackgroundColor
@brief The navigation bar's background color. Default color is white.
*/
@property (nonatomic, strong) UIColor * _Nonnull navigationBarBackgroundColor;

/**
@property confirmationAlertContent
@brief Array of Strings which specifies the content of payment cancellation confirmation dialog. [ "Title", "Message", "Yes", "No" ]
*/
@property (nonatomic, strong) NSArray * _Nonnull confirmationAlertContent;

@end
