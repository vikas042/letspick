/******/ (function(modules) { // webpackBootstrap
/******/    // The module cache
/******/    var installedModules = {};
/******/
/******/    // The require function
/******/    function __webpack_require__(moduleId) {
/******/
/******/        // Check if module is in cache
/******/        if(installedModules[moduleId]) {
/******/            return installedModules[moduleId].exports;
/******/        }
/******/        // Create a new module (and put it into the cache)
/******/        var module = installedModules[moduleId] = {
/******/            i: moduleId,
/******/            l: false,
/******/            exports: {}
/******/        };
/******/
/******/        // Execute the module function
/******/        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/        // Flag the module as loaded
/******/        module.l = true;
/******/
/******/        // Return the exports of the module
/******/        return module.exports;
/******/    }
/******/
/******/
/******/    // expose the modules object (__webpack_modules__)
/******/    __webpack_require__.m = modules;
/******/
/******/    // expose the module cache
/******/    __webpack_require__.c = installedModules;
/******/
/******/    // define getter function for harmony exports
/******/    __webpack_require__.d = function(exports, name, getter) {
/******/        if(!__webpack_require__.o(exports, name)) {
/******/            Object.defineProperty(exports, name, {
/******/                configurable: false,
/******/                enumerable: true,
/******/                get: getter
/******/            });
/******/        }
/******/    };
/******/
/******/    // getDefaultExport function for compatibility with non-harmony modules
/******/    __webpack_require__.n = function(module) {
/******/        var getter = module && module.__esModule ?
/******/            function getDefault() { return module['default']; } :
/******/            function getModuleExports() { return module; };
/******/        __webpack_require__.d(getter, 'a', getter);
/******/        return getter;
/******/    };
/******/
/******/    // Object.prototype.hasOwnProperty.call
/******/    __webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/    // __webpack_public_path__
/******/    __webpack_require__.p = "/dist/";
/******/
/******/    // Load entry module and return exports
/******/    return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var scope = (typeof global !== "undefined" && global) ||
            (typeof self !== "undefined" && self) ||
            window;
var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, scope, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, scope, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(scope, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__(5);
// On some exotic environments, it's not clear which object `setimmediate` was
// able to install onto.  Search each possibility in the same order as the
// `setimmediate` library.
exports.setImmediate = (typeof self !== "undefined" && self.setImmediate) ||
                       (typeof global !== "undefined" && global.setImmediate) ||
                       (this && this.setImmediate);
exports.clearImmediate = (typeof self !== "undefined" && self.clearImmediate) ||
                         (typeof global !== "undefined" && global.clearImmediate) ||
                         (this && this.clearImmediate);

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),
/* 1 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
    return this;
})();

try {
    // This works if eval is allowed (see CSP)
    g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
    // This works if the window reference is available
    if(typeof window === "object")
        g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(3);


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* !!!!!!!!!!!!!!!!!!!! Never remove this !!!!!!!!!!!!!!!!!!!! */
if (!Array.prototype.findIndex) {
  Object.defineProperty(Array.prototype, 'findIndex', {
    value: function value(predicate) {
      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this); // 2. Let len be ? ToLength(? Get(O, "length")).

      var len = o.length >>> 0; // 3. If IsCallable(predicate) is false, throw a TypeError exception.

      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      } // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.


      var thisArg = arguments[1]; // 5. Let k be 0.

      var k = 0; // 6. Repeat, while k < len

      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return k.
        var kValue = o[k];

        if (predicate.call(thisArg, kValue, k, o)) {
          return k;
        } // e. Increase k by 1.


        k++;
      } // 7. Return -1.


      return -1;
    },
    configurable: true,
    writable: true
  });
}

window._jbridge = JBridge;
/* !!!!!!!!!!!!!!!!!!!! Never remove this !!!!!!!!!!!!!!!!!!!! */

var guid = function guid() {
  var s4 = function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  };

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

window.hyper_session_id = guid();

_jbridge.setSessionId(window.hyper_session_id);

var purescript = __webpack_require__(4); //TODO: Change this to purs after 10th April, 2018


window.onBackPressed = function () {
  if (window.mappStack.length > 0 && typeof window.mapps != "undefined") {
    var app = window.mappStack[window.mappStack.length - 1];
    var iframe = window.mapps[app.callee];

    if (iframe && iframe.contentWindow && typeof iframe.contentWindow.onBackPressed == "function") {
      iframe.contentWindow.onBackPressed();
    }
  } else {
    _jbridge.exitApp(0, JSON.stringify({
      backPressed: "true"
    }));
  }
};

window.onLifeCycleEvent = function (event) {
  if (window.mappStack.length > 0 && typeof window.mapps != "undefined") {
    var app = window.mappStack[window.mappStack.length - 1];
    var iframe = window.mapps[app.callee];

    if (iframe && iframe.contentWindow && typeof iframe.contentWindow.onLifeCycleEvent == "function") {
      iframe.contentWindow.onLifeCycleEvent(event);
    }
  } else {
    _jbridge.exitApp(0, JSON.stringify({
      backPressed: "true"
    }));
  }
};

window.callUICallback = function () {
  if (window.mappStack.length > 0 && typeof window.mapps != "undefined") {
    var app = window.mappStack[window.mappStack.length - 1];
    var iframe = window.mapps[app.callee];

    if (iframe && iframe.contentWindow && typeof iframe.contentWindow.callUICallback == "function") {
      iframe.contentWindow.callUICallback.apply(null, arguments);
    }
  }
};

window.onBundleUpdate = function () {
  setTimeout(function () {
    if (window.mappStack.length > 0 && typeof window.mapps != "undefined") {
      var app = window.mappStack[0];
      var iframe = window.mapps[app.callee];

      if (iframe && iframe.contentWindow) {
        var json = JSON.parse(_jbridge.getSessionAttribute("bundleParams", "{}"));

        for (var i in json) {
          iframe.contentWindow.__payload[i] = json[i];
        }

        if (typeof iframe.contentWindow.onBundleUpdate == "function") {
          iframe.contentWindow.onBundleUpdate();
        }
      }
    }
  }, 100);
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(setImmediate, clearImmediate) {

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// Generated by purs bundle 0.11.7
var PS = {};

(function (exports) {
  "use strict";

  exports.arrayMap = function (f) {
    return function (arr) {
      var l = arr.length;
      var result = new Array(l);

      for (var i = 0; i < l; i++) {
        result[i] = f(arr[i]);
      }

      return result;
    };
  };
})(PS["Data.Functor"] = PS["Data.Functor"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Semigroupoid = function Semigroupoid(compose) {
    this.compose = compose;
  };

  var semigroupoidFn = new Semigroupoid(function (f) {
    return function (g) {
      return function (x) {
        return f(g(x));
      };
    };
  });

  var compose = function compose(dict) {
    return dict.compose;
  };

  exports["compose"] = compose;
  exports["Semigroupoid"] = Semigroupoid;
  exports["semigroupoidFn"] = semigroupoidFn;
})(PS["Control.Semigroupoid"] = PS["Control.Semigroupoid"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Semigroupoid = PS["Control.Semigroupoid"];

  var Category = function Category(Semigroupoid0, id) {
    this.Semigroupoid0 = Semigroupoid0;
    this.id = id;
  };

  var id = function id(dict) {
    return dict.id;
  };

  var categoryFn = new Category(function () {
    return Control_Semigroupoid.semigroupoidFn;
  }, function (x) {
    return x;
  });
  exports["Category"] = Category;
  exports["id"] = id;
  exports["categoryFn"] = categoryFn;
})(PS["Control.Category"] = PS["Control.Category"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var otherwise = true;
  exports["otherwise"] = otherwise;
})(PS["Data.Boolean"] = PS["Data.Boolean"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Category = PS["Control.Category"];
  var Data_Boolean = PS["Data.Boolean"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ring = PS["Data.Ring"];

  var flip = function flip(f) {
    return function (b) {
      return function (a) {
        return f(a)(b);
      };
    };
  };

  var $$const = function $$const(a) {
    return function (v) {
      return a;
    };
  };

  exports["flip"] = flip;
  exports["const"] = $$const;
})(PS["Data.Function"] = PS["Data.Function"] || {});

(function (exports) {
  "use strict";

  exports.unit = {};
})(PS["Data.Unit"] = PS["Data.Unit"] || {});

(function (exports) {
  "use strict";

  exports.showIntImpl = function (n) {
    return n.toString();
  };

  exports.showStringImpl = function (s) {
    var l = s.length;
    return "\"" + s.replace(/[\0-\x1F\x7F"\\]/g, // eslint-disable-line no-control-regex
    function (c, i) {
      switch (c) {
        case "\"":
        case "\\":
          return "\\" + c;

        case "\x07":
          return "\\a";

        case "\b":
          return "\\b";

        case "\f":
          return "\\f";

        case "\n":
          return "\\n";

        case "\r":
          return "\\r";

        case "\t":
          return "\\t";

        case "\v":
          return "\\v";
      }

      var k = i + 1;
      var empty = k < l && s[k] >= "0" && s[k] <= "9" ? "\\&" : "";
      return "\\" + c.charCodeAt(0).toString(10) + empty;
    }) + "\"";
  };

  exports.showArrayImpl = function (f) {
    return function (xs) {
      var ss = [];

      for (var i = 0, l = xs.length; i < l; i++) {
        ss[i] = f(xs[i]);
      }

      return "[" + ss.join(",") + "]";
    };
  };
})(PS["Data.Show"] = PS["Data.Show"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Show"];

  var Show = function Show(show) {
    this.show = show;
  };

  var showString = new Show($foreign.showStringImpl);
  var showInt = new Show($foreign.showIntImpl);

  var show = function show(dict) {
    return dict.show;
  };

  var showArray = function showArray(dictShow) {
    return new Show($foreign.showArrayImpl(show(dictShow)));
  };

  exports["Show"] = Show;
  exports["show"] = show;
  exports["showInt"] = showInt;
  exports["showString"] = showString;
  exports["showArray"] = showArray;
})(PS["Data.Show"] = PS["Data.Show"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Unit"];
  var Data_Show = PS["Data.Show"];
  exports["unit"] = $foreign.unit;
})(PS["Data.Unit"] = PS["Data.Unit"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Functor"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Function = PS["Data.Function"];
  var Data_Unit = PS["Data.Unit"];

  var Functor = function Functor(map) {
    this.map = map;
  };

  var map = function map(dict) {
    return dict.map;
  };

  var $$void = function $$void(dictFunctor) {
    return map(dictFunctor)(Data_Function["const"](Data_Unit.unit));
  };

  var functorFn = new Functor(Control_Semigroupoid.compose(Control_Semigroupoid.semigroupoidFn));
  var functorArray = new Functor($foreign.arrayMap);
  exports["Functor"] = Functor;
  exports["map"] = map;
  exports["void"] = $$void;
  exports["functorFn"] = functorFn;
  exports["functorArray"] = functorArray;
})(PS["Data.Functor"] = PS["Data.Functor"] || {});

(function (exports) {
  "use strict";

  exports.concatString = function (s1) {
    return function (s2) {
      return s1 + s2;
    };
  };
})(PS["Data.Semigroup"] = PS["Data.Semigroup"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Semigroup"];
  var Data_Unit = PS["Data.Unit"];
  var Data_Void = PS["Data.Void"];

  var Semigroup = function Semigroup(append) {
    this.append = append;
  };

  var semigroupString = new Semigroup($foreign.concatString);

  var append = function append(dict) {
    return dict.append;
  };

  exports["Semigroup"] = Semigroup;
  exports["append"] = append;
  exports["semigroupString"] = semigroupString;
})(PS["Data.Semigroup"] = PS["Data.Semigroup"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Data_Functor = PS["Data.Functor"];
  var Data_Semigroup = PS["Data.Semigroup"];

  var Alt = function Alt(Functor0, alt) {
    this.Functor0 = Functor0;
    this.alt = alt;
  };

  var alt = function alt(dict) {
    return dict.alt;
  };

  exports["Alt"] = Alt;
  exports["alt"] = alt;
})(PS["Control.Alt"] = PS["Control.Alt"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Control.Apply"];
  var Control_Category = PS["Control.Category"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];

  var Apply = function Apply(Functor0, apply) {
    this.Functor0 = Functor0;
    this.apply = apply;
  };

  var applyFn = new Apply(function () {
    return Data_Functor.functorFn;
  }, function (f) {
    return function (g) {
      return function (x) {
        return f(x)(g(x));
      };
    };
  });

  var apply = function apply(dict) {
    return dict.apply;
  };

  var applySecond = function applySecond(dictApply) {
    return function (a) {
      return function (b) {
        return apply(dictApply)(Data_Functor.map(dictApply.Functor0())(Data_Function["const"](Control_Category.id(Control_Category.categoryFn)))(a))(b);
      };
    };
  };

  exports["Apply"] = Apply;
  exports["apply"] = apply;
  exports["applySecond"] = applySecond;
  exports["applyFn"] = applyFn;
})(PS["Control.Apply"] = PS["Control.Apply"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Apply = PS["Control.Apply"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Unit = PS["Data.Unit"];

  var Applicative = function Applicative(Apply0, pure) {
    this.Apply0 = Apply0;
    this.pure = pure;
  };

  var pure = function pure(dict) {
    return dict.pure;
  };

  var unless = function unless(dictApplicative) {
    return function (v) {
      return function (v1) {
        if (!v) {
          return v1;
        }

        ;

        if (v) {
          return pure(dictApplicative)(Data_Unit.unit);
        }

        ;
        throw new Error("Failed pattern match at Control.Applicative line 62, column 1 - line 62, column 65: " + [v.constructor.name, v1.constructor.name]);
      };
    };
  };

  var liftA1 = function liftA1(dictApplicative) {
    return function (f) {
      return function (a) {
        return Control_Apply.apply(dictApplicative.Apply0())(pure(dictApplicative)(f))(a);
      };
    };
  };

  var applicativeFn = new Applicative(function () {
    return Control_Apply.applyFn;
  }, function (x) {
    return function (v) {
      return x;
    };
  });
  exports["Applicative"] = Applicative;
  exports["pure"] = pure;
  exports["liftA1"] = liftA1;
  exports["unless"] = unless;
  exports["applicativeFn"] = applicativeFn;
})(PS["Control.Applicative"] = PS["Control.Applicative"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Data_Functor = PS["Data.Functor"];

  var Plus = function Plus(Alt0, empty) {
    this.Alt0 = Alt0;
    this.empty = empty;
  };

  var empty = function empty(dict) {
    return dict.empty;
  };

  exports["Plus"] = Plus;
  exports["empty"] = empty;
})(PS["Control.Plus"] = PS["Control.Plus"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Plus = PS["Control.Plus"];
  var Data_Functor = PS["Data.Functor"];

  var Alternative = function Alternative(Applicative0, Plus1) {
    this.Applicative0 = Applicative0;
    this.Plus1 = Plus1;
  };

  exports["Alternative"] = Alternative;
})(PS["Control.Alternative"] = PS["Control.Alternative"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Control.Bind"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Category = PS["Control.Category"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Unit = PS["Data.Unit"];

  var Bind = function Bind(Apply0, bind) {
    this.Apply0 = Apply0;
    this.bind = bind;
  };

  var Discard = function Discard(discard) {
    this.discard = discard;
  };

  var discard = function discard(dict) {
    return dict.discard;
  };

  var bind = function bind(dict) {
    return dict.bind;
  };

  var bindFlipped = function bindFlipped(dictBind) {
    return Data_Function.flip(bind(dictBind));
  };

  var composeKleisliFlipped = function composeKleisliFlipped(dictBind) {
    return function (f) {
      return function (g) {
        return function (a) {
          return bindFlipped(dictBind)(f)(g(a));
        };
      };
    };
  };

  var composeKleisli = function composeKleisli(dictBind) {
    return function (f) {
      return function (g) {
        return function (a) {
          return bind(dictBind)(f(a))(g);
        };
      };
    };
  };

  var discardUnit = new Discard(function (dictBind) {
    return bind(dictBind);
  });

  var ifM = function ifM(dictBind) {
    return function (cond) {
      return function (t) {
        return function (f) {
          return bind(dictBind)(cond)(function (cond$prime) {
            if (cond$prime) {
              return t;
            }

            ;
            return f;
          });
        };
      };
    };
  };

  exports["Bind"] = Bind;
  exports["bind"] = bind;
  exports["bindFlipped"] = bindFlipped;
  exports["Discard"] = Discard;
  exports["discard"] = discard;
  exports["composeKleisli"] = composeKleisli;
  exports["composeKleisliFlipped"] = composeKleisliFlipped;
  exports["ifM"] = ifM;
  exports["discardUnit"] = discardUnit;
})(PS["Control.Bind"] = PS["Control.Bind"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Unit = PS["Data.Unit"];

  var Monad = function Monad(Applicative0, Bind1) {
    this.Applicative0 = Applicative0;
    this.Bind1 = Bind1;
  };

  var ap = function ap(dictMonad) {
    return function (f) {
      return function (a) {
        return Control_Bind.bind(dictMonad.Bind1())(f)(function (v) {
          return Control_Bind.bind(dictMonad.Bind1())(a)(function (v1) {
            return Control_Applicative.pure(dictMonad.Applicative0())(v(v1));
          });
        });
      };
    };
  };

  exports["Monad"] = Monad;
  exports["ap"] = ap;
})(PS["Control.Monad"] = PS["Control.Monad"] || {});

(function (exports) {
  /* globals setImmediate, clearImmediate, setTimeout, clearTimeout */

  /* jshint -W083, -W098, -W003 */
  "use strict";

  var Aff = function () {
    // A unique value for empty.
    var EMPTY = {};
    /*
    An awkward approximation. We elide evidence we would otherwise need in PS for
    efficiency sake.
    data Aff eff a
    = Pure a
    | Throw Error
    | Catch (Aff eff a) (Error -> Aff eff a)
    | Sync (Eff eff a)
    | Async ((Either Error a -> Eff eff Unit) -> Eff eff (Canceler eff))
    | forall b. Bind (Aff eff b) (b -> Aff eff a)
    | forall b. Bracket (Aff eff b) (BracketConditions eff b) (b -> Aff eff a)
    | forall b. Fork Boolean (Aff eff b) ?(Fiber eff b -> a)
    | Sequential (ParAff aff a)
    */

    var PURE = "Pure";
    var THROW = "Throw";
    var CATCH = "Catch";
    var SYNC = "Sync";
    var ASYNC = "Async";
    var BIND = "Bind";
    var BRACKET = "Bracket";
    var FORK = "Fork";
    var SEQ = "Sequential";
    /*
    data ParAff eff a
    = forall b. Map (b -> a) (ParAff eff b)
    | forall b. Apply (ParAff eff (b -> a)) (ParAff eff b)
    | Alt (ParAff eff a) (ParAff eff a)
    | ?Par (Aff eff a)
    */

    var MAP = "Map";
    var APPLY = "Apply";
    var ALT = "Alt"; // Various constructors used in interpretation

    var CONS = "Cons"; // Cons-list, for stacks

    var RESUME = "Resume"; // Continue indiscriminately

    var RELEASE = "Release"; // Continue with bracket finalizers

    var FINALIZER = "Finalizer"; // A non-interruptible effect

    var FINALIZED = "Finalized"; // Marker for finalization

    var FORKED = "Forked"; // Reference to a forked fiber, with resumption stack

    var FIBER = "Fiber"; // Actual fiber reference

    var THUNK = "Thunk"; // Primed effect, ready to invoke

    function Aff(tag, _1, _2, _3) {
      this.tag = tag;
      this._1 = _1;
      this._2 = _2;
      this._3 = _3;
    }

    function AffCtr(tag) {
      var fn = function fn(_1, _2, _3) {
        return new Aff(tag, _1, _2, _3);
      };

      fn.tag = tag;
      return fn;
    }

    function nonCanceler(error) {
      return new Aff(PURE, void 0);
    }

    function runEff(eff) {
      try {
        eff();
      } catch (error) {
        setTimeout(function () {
          throw error;
        }, 0);
      }
    }

    function runSync(left, right, eff) {
      try {
        return right(eff());
      } catch (error) {
        return left(error);
      }
    }

    function runAsync(left, eff, k) {
      try {
        return eff(k)();
      } catch (error) {
        k(left(error))();
        return nonCanceler;
      }
    }

    var Scheduler = function () {
      var limit = 1024;
      var size = 0;
      var ix = 0;
      var queue = new Array(limit);
      var draining = false;

      function drain() {
        var thunk;
        draining = true;

        while (size !== 0) {
          size--;
          thunk = queue[ix];
          queue[ix] = void 0;
          ix = (ix + 1) % limit;
          thunk();
        }

        draining = false;
      }

      return {
        isDraining: function isDraining() {
          return draining;
        },
        enqueue: function enqueue(cb) {
          var i, tmp;

          if (size === limit) {
            tmp = draining;
            drain();
            draining = tmp;
          }

          queue[(ix + size) % limit] = cb;
          size++;

          if (!draining) {
            drain();
          }
        }
      };
    }();

    function Supervisor(util) {
      var fibers = {};
      var fiberId = 0;
      var count = 0;
      return {
        register: function register(fiber) {
          var fid = fiberId++;
          fiber.onComplete({
            rethrow: true,
            handler: function handler(result) {
              return function () {
                count--;
                delete fibers[fid];
              };
            }
          });
          fibers[fid] = fiber;
          count++;
        },
        isEmpty: function isEmpty() {
          return count === 0;
        },
        killAll: function killAll(killError, cb) {
          return function () {
            var killCount = 0;
            var kills = {};

            function kill(fid) {
              kills[fid] = fibers[fid].kill(killError, function (result) {
                return function () {
                  delete kills[fid];
                  killCount--;

                  if (util.isLeft(result) && util.fromLeft(result)) {
                    setTimeout(function () {
                      throw util.fromLeft(result);
                    }, 0);
                  }

                  if (killCount === 0) {
                    cb();
                  }
                };
              })();
            }

            for (var k in fibers) {
              if (fibers.hasOwnProperty(k)) {
                killCount++;
                kill(k);
              }
            }

            fibers = {};
            fiberId = 0;
            count = 0;
            return function (error) {
              return new Aff(SYNC, function () {
                for (var k in kills) {
                  if (kills.hasOwnProperty(k)) {
                    kills[k]();
                  }
                }
              });
            };
          };
        }
      };
    } // Fiber state machine


    var SUSPENDED = 0; // Suspended, pending a join.

    var CONTINUE = 1; // Interpret the next instruction.

    var STEP_BIND = 2; // Apply the next bind.

    var STEP_RESULT = 3; // Handle potential failure from a result.

    var PENDING = 4; // An async effect is running.

    var RETURN = 5; // The current stack has returned.

    var COMPLETED = 6; // The entire fiber has completed.

    function Fiber(util, supervisor, aff) {
      // Monotonically increasing tick, increased on each asynchronous turn.
      var runTick = 0; // The current branch of the state machine.

      var status = SUSPENDED; // The current point of interest for the state machine branch.

      var step = aff; // Successful step

      var fail = null; // Failure step

      var interrupt = null; // Asynchronous interrupt
      // Stack of continuations for the current fiber.

      var bhead = null;
      var btail = null; // Stack of attempts and finalizers for error recovery. Every `Cons` is also
      // tagged with current `interrupt` state. We use this to track which items
      // should be ignored or evaluated as a result of a kill.

      var attempts = null; // A special state is needed for Bracket, because it cannot be killed. When
      // we enter a bracket acquisition or finalizer, we increment the counter,
      // and then decrement once complete.

      var bracketCount = 0; // Each join gets a new id so they can be revoked.

      var joinId = 0;
      var joins = null;
      var rethrow = true; // Each invocation of `run` requires a tick. When an asynchronous effect is
      // resolved, we must check that the local tick coincides with the fiber
      // tick before resuming. This prevents multiple async continuations from
      // accidentally resuming the same fiber. A common example may be invoking
      // the provided callback in `makeAff` more than once, but it may also be an
      // async effect resuming after the fiber was already cancelled.

      function _run(localRunTick) {
        var tmp, result, attempt, canceler;

        while (true) {
          tmp = null;
          result = null;
          attempt = null;
          canceler = null;

          switch (status) {
            case STEP_BIND:
              status = CONTINUE;
              step = bhead(step);

              if (btail === null) {
                bhead = null;
              } else {
                bhead = btail._1;
                btail = btail._2;
              }

              break;

            case STEP_RESULT:
              if (util.isLeft(step)) {
                status = RETURN;
                fail = step;
                step = null;
              } else if (bhead === null) {
                status = RETURN;
              } else {
                status = STEP_BIND;
                step = util.fromRight(step);
              }

              break;

            case CONTINUE:
              switch (step.tag) {
                case BIND:
                  if (bhead) {
                    btail = new Aff(CONS, bhead, btail);
                  }

                  bhead = step._2;
                  status = CONTINUE;
                  step = step._1;
                  break;

                case PURE:
                  if (bhead === null) {
                    status = RETURN;
                    step = util.right(step._1);
                  } else {
                    status = STEP_BIND;
                    step = step._1;
                  }

                  break;

                case SYNC:
                  status = STEP_RESULT;
                  step = runSync(util.left, util.right, step._1);
                  break;

                case ASYNC:
                  status = PENDING;
                  step = runAsync(util.left, step._1, function (result) {
                    return function () {
                      if (runTick !== localRunTick) {
                        return;
                      }

                      runTick++;
                      Scheduler.enqueue(function () {
                        status = STEP_RESULT;
                        step = result;

                        _run(runTick);
                      });
                    };
                  });
                  return;

                case THROW:
                  status = RETURN;
                  fail = util.left(step._1);
                  step = null;
                  break;
                // Enqueue the Catch so that we can call the error handler later on
                // in case of an exception.

                case CATCH:
                  if (bhead === null) {
                    attempts = new Aff(CONS, step, attempts, interrupt);
                  } else {
                    attempts = new Aff(CONS, step, new Aff(CONS, new Aff(RESUME, bhead, btail), attempts, interrupt), interrupt);
                  }

                  bhead = null;
                  btail = null;
                  status = CONTINUE;
                  step = step._1;
                  break;
                // Enqueue the Bracket so that we can call the appropriate handlers
                // after resource acquisition.

                case BRACKET:
                  bracketCount++;

                  if (bhead === null) {
                    attempts = new Aff(CONS, step, attempts, interrupt);
                  } else {
                    attempts = new Aff(CONS, step, new Aff(CONS, new Aff(RESUME, bhead, btail), attempts, interrupt), interrupt);
                  }

                  bhead = null;
                  btail = null;
                  status = CONTINUE;
                  step = step._1;
                  break;

                case FORK:
                  status = STEP_RESULT;
                  tmp = Fiber(util, supervisor, step._2);

                  if (supervisor) {
                    supervisor.register(tmp);
                  }

                  if (step._1) {
                    tmp.run();
                  }

                  step = util.right(tmp);
                  break;

                case SEQ:
                  status = CONTINUE;
                  step = sequential(util, supervisor, step._1);
                  break;
              }

              break;

            case RETURN:
              bhead = null;
              btail = null; // If the current stack has returned, and we have no other stacks to
              // resume or finalizers to run, the fiber has halted and we can
              // invoke all join callbacks. Otherwise we need to resume.

              if (attempts === null) {
                status = COMPLETED;
                step = interrupt || fail || step;
              } else {
                // The interrupt status for the enqueued item.
                tmp = attempts._3;
                attempt = attempts._1;
                attempts = attempts._2;

                switch (attempt.tag) {
                  // We cannot recover from an interrupt. Otherwise we should
                  // continue stepping, or run the exception handler if an exception
                  // was raised.
                  case CATCH:
                    // We should compare the interrupt status as well because we
                    // only want it to apply if there has been an interrupt since
                    // enqueuing the catch.
                    if (interrupt && interrupt !== tmp) {
                      status = RETURN;
                    } else if (fail) {
                      status = CONTINUE;
                      step = attempt._2(util.fromLeft(fail));
                      fail = null;
                    }

                    break;
                  // We cannot resume from an interrupt or exception.

                  case RESUME:
                    // As with Catch, we only want to ignore in the case of an
                    // interrupt since enqueing the item.
                    if (interrupt && interrupt !== tmp || fail) {
                      status = RETURN;
                    } else {
                      bhead = attempt._1;
                      btail = attempt._2;
                      status = STEP_BIND;
                      step = util.fromRight(step);
                    }

                    break;
                  // If we have a bracket, we should enqueue the handlers,
                  // and continue with the success branch only if the fiber has
                  // not been interrupted. If the bracket acquisition failed, we
                  // should not run either.

                  case BRACKET:
                    bracketCount--;

                    if (fail === null) {
                      result = util.fromRight(step); // We need to enqueue the Release with the same interrupt
                      // status as the Bracket that is initiating it.

                      attempts = new Aff(CONS, new Aff(RELEASE, attempt._2, result), attempts, tmp); // We should only coninue as long as the interrupt status has not changed or
                      // we are currently within a non-interruptable finalizer.

                      if (interrupt === tmp || bracketCount > 0) {
                        status = CONTINUE;
                        step = attempt._3(result);
                      }
                    }

                    break;
                  // Enqueue the appropriate handler. We increase the bracket count
                  // because it should not be cancelled.

                  case RELEASE:
                    bracketCount++;
                    attempts = new Aff(CONS, new Aff(FINALIZED, step), attempts, interrupt);
                    status = CONTINUE; // It has only been killed if the interrupt status has changed
                    // since we enqueued the item.

                    if (interrupt && interrupt !== tmp) {
                      step = attempt._1.killed(util.fromLeft(interrupt))(attempt._2);
                    } else if (fail) {
                      step = attempt._1.failed(util.fromLeft(fail))(attempt._2);
                    } else {
                      step = attempt._1.completed(util.fromRight(step))(attempt._2);
                    }

                    break;

                  case FINALIZER:
                    bracketCount++;
                    attempts = new Aff(CONS, new Aff(FINALIZED, step), attempts, interrupt);
                    status = CONTINUE;
                    step = attempt._1;
                    break;

                  case FINALIZED:
                    bracketCount--;
                    status = RETURN;
                    step = attempt._1;
                    break;
                }
              }

              break;

            case COMPLETED:
              for (var k in joins) {
                if (joins.hasOwnProperty(k)) {
                  rethrow = rethrow && joins[k].rethrow;
                  runEff(joins[k].handler(step));
                }
              }

              joins = null; // If we have an interrupt and a fail, then the thread threw while
              // running finalizers. This should always rethrow in a fresh stack.

              if (interrupt && fail) {
                setTimeout(function () {
                  throw util.fromLeft(fail);
                }, 0); // If we have an unhandled exception, and no other fiber has joined
                // then we need to throw the exception in a fresh stack.
              } else if (util.isLeft(step) && rethrow) {
                setTimeout(function () {
                  // Guard on reathrow because a completely synchronous fiber can
                  // still have an observer which was added after-the-fact.
                  if (rethrow) {
                    throw util.fromLeft(step);
                  }
                }, 0);
              }

              return;

            case SUSPENDED:
              status = CONTINUE;
              break;

            case PENDING:
              return;
          }
        }
      }

      function onComplete(join) {
        return function () {
          if (status === COMPLETED) {
            rethrow = rethrow && join.rethrow;
            join.handler(step)();
            return function () {};
          }

          var jid = joinId++;
          joins = joins || {};
          joins[jid] = join;
          return function () {
            if (joins !== null) {
              delete joins[jid];
            }
          };
        };
      }

      function kill(error, cb) {
        return function () {
          if (status === COMPLETED) {
            cb(util.right(void 0))();
            return function () {};
          }

          var canceler = onComplete({
            rethrow: false,
            handler: function handler()
            /* unused */
            {
              return cb(util.right(void 0));
            }
          })();

          switch (status) {
            case SUSPENDED:
              interrupt = util.left(error);
              status = COMPLETED;
              step = interrupt;

              _run(runTick);

              break;

            case PENDING:
              if (interrupt === null) {
                interrupt = util.left(error);
              }

              if (bracketCount === 0) {
                if (status === PENDING) {
                  attempts = new Aff(CONS, new Aff(FINALIZER, step(error)), attempts, interrupt);
                }

                status = RETURN;
                step = null;
                fail = null;

                _run(++runTick);
              }

              break;

            default:
              if (interrupt === null) {
                interrupt = util.left(error);
              }

              if (bracketCount === 0) {
                status = RETURN;
                step = null;
                fail = null;
              }

          }

          return canceler;
        };
      }

      function join(cb) {
        return function () {
          var canceler = onComplete({
            rethrow: false,
            handler: cb
          })();

          if (status === SUSPENDED) {
            _run(runTick);
          }

          return canceler;
        };
      }

      return {
        kill: kill,
        join: join,
        onComplete: onComplete,
        isSuspended: function isSuspended() {
          return status === SUSPENDED;
        },
        run: function run() {
          if (status === SUSPENDED) {
            if (!Scheduler.isDraining()) {
              Scheduler.enqueue(function () {
                _run(runTick);
              });
            } else {
              _run(runTick);
            }
          }
        }
      };
    }

    function runPar(util, supervisor, par, cb) {
      // Table of all forked fibers.
      var fiberId = 0;
      var fibers = {}; // Table of currently running cancelers, as a product of `Alt` behavior.

      var killId = 0;
      var kills = {}; // Error used for early cancelation on Alt branches.

      var early = new Error("[ParAff] Early exit"); // Error used to kill the entire tree.

      var interrupt = null; // The root pointer of the tree.

      var root = EMPTY; // Walks a tree, invoking all the cancelers. Returns the table of pending
      // cancellation fibers.

      function kill(error, par, cb) {
        var step = par;
        var head = null;
        var tail = null;
        var count = 0;
        var kills = {};
        var tmp, kid;

        loop: while (true) {
          tmp = null;

          switch (step.tag) {
            case FORKED:
              if (step._3 === EMPTY) {
                tmp = fibers[step._1];
                kills[count++] = tmp.kill(error, function (result) {
                  return function () {
                    count--;

                    if (count === 0) {
                      cb(result)();
                    }
                  };
                });
              } // Terminal case.


              if (head === null) {
                break loop;
              } // Go down the right side of the tree.


              step = head._2;

              if (tail === null) {
                head = null;
              } else {
                head = tail._1;
                tail = tail._2;
              }

              break;

            case MAP:
              step = step._2;
              break;

            case APPLY:
            case ALT:
              if (head) {
                tail = new Aff(CONS, head, tail);
              }

              head = step;
              step = step._1;
              break;
          }
        }

        if (count === 0) {
          cb(util.right(void 0))();
        } else {
          // Run the cancelation effects. We alias `count` because it's mutable.
          kid = 0;
          tmp = count;

          for (; kid < tmp; kid++) {
            kills[kid] = kills[kid]();
          }
        }

        return kills;
      } // When a fiber resolves, we need to bubble back up the tree with the
      // result, computing the applicative nodes.


      function join(result, head, tail) {
        var fail, step, lhs, rhs, tmp, kid;

        if (util.isLeft(result)) {
          fail = result;
          step = null;
        } else {
          step = result;
          fail = null;
        }

        loop: while (true) {
          lhs = null;
          rhs = null;
          tmp = null;
          kid = null; // We should never continue if the entire tree has been interrupted.

          if (interrupt !== null) {
            return;
          } // We've made it all the way to the root of the tree, which means
          // the tree has fully evaluated.


          if (head === null) {
            cb(fail || step)();
            return;
          } // The tree has already been computed, so we shouldn't try to do it
          // again. This should never happen.
          // TODO: Remove this?


          if (head._3 !== EMPTY) {
            return;
          }

          switch (head.tag) {
            case MAP:
              if (fail === null) {
                head._3 = util.right(head._1(util.fromRight(step)));
                step = head._3;
              } else {
                head._3 = fail;
              }

              break;

            case APPLY:
              lhs = head._1._3;
              rhs = head._2._3; // If we have a failure we should kill the other side because we
              // can't possible yield a result anymore.

              if (fail) {
                head._3 = fail;
                tmp = true;
                kid = killId++;
                kills[kid] = kill(early, fail === lhs ? head._2 : head._1, function ()
                /* unused */
                {
                  return function () {
                    delete kills[kid];

                    if (tmp) {
                      tmp = false;
                    } else if (tail === null) {
                      join(step, null, null);
                    } else {
                      join(step, tail._1, tail._2);
                    }
                  };
                });

                if (tmp) {
                  tmp = false;
                  return;
                }
              } else if (lhs === EMPTY || rhs === EMPTY) {
                // We can only proceed if both sides have resolved.
                return;
              } else {
                step = util.right(util.fromRight(lhs)(util.fromRight(rhs)));
                head._3 = step;
              }

              break;

            case ALT:
              lhs = head._1._3;
              rhs = head._2._3; // We can only proceed if both have resolved or we have a success

              if (lhs === EMPTY && util.isLeft(rhs) || rhs === EMPTY && util.isLeft(lhs)) {
                return;
              } // If both sides resolve with an error, we should continue with the
              // first error


              if (lhs !== EMPTY && util.isLeft(lhs) && rhs !== EMPTY && util.isLeft(rhs)) {
                fail = step === lhs ? rhs : lhs;
                step = null;
                head._3 = fail;
              } else {
                head._3 = step;
                tmp = true;
                kid = killId++; // Once a side has resolved, we need to cancel the side that is still
                // pending before we can continue.

                kills[kid] = kill(early, step === lhs ? head._2 : head._1, function ()
                /* unused */
                {
                  return function () {
                    delete kills[kid];

                    if (tmp) {
                      tmp = false;
                    } else if (tail === null) {
                      join(step, null, null);
                    } else {
                      join(step, tail._1, tail._2);
                    }
                  };
                });

                if (tmp) {
                  tmp = false;
                  return;
                }
              }

              break;
          }

          if (tail === null) {
            head = null;
          } else {
            head = tail._1;
            tail = tail._2;
          }
        }
      }

      function resolve(fiber) {
        return function (result) {
          return function () {
            delete fibers[fiber._1];
            fiber._3 = result;
            join(result, fiber._2._1, fiber._2._2);
          };
        };
      } // Walks the applicative tree, substituting non-applicative nodes with
      // `FORKED` nodes. In this tree, all applicative nodes use the `_3` slot
      // as a mutable slot for memoization. In an unresolved state, the `_3`
      // slot is `EMPTY`. In the cases of `ALT` and `APPLY`, we always walk
      // the left side first, because both operations are left-associative. As
      // we `RETURN` from those branches, we then walk the right side.


      function run() {
        var status = CONTINUE;
        var step = par;
        var head = null;
        var tail = null;
        var tmp, fid;

        loop: while (true) {
          tmp = null;
          fid = null;

          switch (status) {
            case CONTINUE:
              switch (step.tag) {
                case MAP:
                  if (head) {
                    tail = new Aff(CONS, head, tail);
                  }

                  head = new Aff(MAP, step._1, EMPTY, EMPTY);
                  step = step._2;
                  break;

                case APPLY:
                  if (head) {
                    tail = new Aff(CONS, head, tail);
                  }

                  head = new Aff(APPLY, EMPTY, step._2, EMPTY);
                  step = step._1;
                  break;

                case ALT:
                  if (head) {
                    tail = new Aff(CONS, head, tail);
                  }

                  head = new Aff(ALT, EMPTY, step._2, EMPTY);
                  step = step._1;
                  break;

                default:
                  // When we hit a leaf value, we suspend the stack in the `FORKED`.
                  // When the fiber resolves, it can bubble back up the tree.
                  fid = fiberId++;
                  status = RETURN;
                  tmp = step;
                  step = new Aff(FORKED, fid, new Aff(CONS, head, tail), EMPTY);
                  tmp = Fiber(util, supervisor, tmp);
                  tmp.onComplete({
                    rethrow: false,
                    handler: resolve(step)
                  })();
                  fibers[fid] = tmp;

                  if (supervisor) {
                    supervisor.register(tmp);
                  }

              }

              break;

            case RETURN:
              // Terminal case, we are back at the root.
              if (head === null) {
                break loop;
              } // If we are done with the right side, we need to continue down the
              // left. Otherwise we should continue up the stack.


              if (head._1 === EMPTY) {
                head._1 = step;
                status = CONTINUE;
                step = head._2;
                head._2 = EMPTY;
              } else {
                head._2 = step;
                step = head;

                if (tail === null) {
                  head = null;
                } else {
                  head = tail._1;
                  tail = tail._2;
                }
              }

          }
        } // Keep a reference to the tree root so it can be cancelled.


        root = step;

        for (fid = 0; fid < fiberId; fid++) {
          fibers[fid].run();
        }
      } // Cancels the entire tree. If there are already subtrees being canceled,
      // we need to first cancel those joins. We will then add fresh joins for
      // all pending branches including those that were in the process of being
      // canceled.


      function cancel(error, cb) {
        interrupt = util.left(error);
        var innerKills;

        for (var kid in kills) {
          if (kills.hasOwnProperty(kid)) {
            innerKills = kills[kid];

            for (kid in innerKills) {
              if (innerKills.hasOwnProperty(kid)) {
                innerKills[kid]();
              }
            }
          }
        }

        kills = null;
        var newKills = kill(error, root, cb);
        return function (killError) {
          return new Aff(ASYNC, function (killCb) {
            return function () {
              for (var kid in newKills) {
                if (newKills.hasOwnProperty(kid)) {
                  newKills[kid]();
                }
              }

              return nonCanceler;
            };
          });
        };
      }

      run();
      return function (killError) {
        return new Aff(ASYNC, function (killCb) {
          return function () {
            return cancel(killError, killCb);
          };
        });
      };
    }

    function sequential(util, supervisor, par) {
      return new Aff(ASYNC, function (cb) {
        return function () {
          return runPar(util, supervisor, par, cb);
        };
      });
    }

    Aff.EMPTY = EMPTY;
    Aff.Pure = AffCtr(PURE);
    Aff.Throw = AffCtr(THROW);
    Aff.Catch = AffCtr(CATCH);
    Aff.Sync = AffCtr(SYNC);
    Aff.Async = AffCtr(ASYNC);
    Aff.Bind = AffCtr(BIND);
    Aff.Bracket = AffCtr(BRACKET);
    Aff.Fork = AffCtr(FORK);
    Aff.Seq = AffCtr(SEQ);
    Aff.ParMap = AffCtr(MAP);
    Aff.ParApply = AffCtr(APPLY);
    Aff.ParAlt = AffCtr(ALT);
    Aff.Fiber = Fiber;
    Aff.Supervisor = Supervisor;
    Aff.Scheduler = Scheduler;
    Aff.nonCanceler = nonCanceler;
    return Aff;
  }();

  exports._pure = Aff.Pure;
  exports._throwError = Aff.Throw;

  exports._catchError = function (aff) {
    return function (k) {
      return Aff.Catch(aff, k);
    };
  };

  exports._map = function (f) {
    return function (aff) {
      if (aff.tag === Aff.Pure.tag) {
        return Aff.Pure(f(aff._1));
      } else {
        return Aff.Bind(aff, function (value) {
          return Aff.Pure(f(value));
        });
      }
    };
  };

  exports._bind = function (aff) {
    return function (k) {
      return Aff.Bind(aff, k);
    };
  };

  exports._fork = function (immediate) {
    return function (aff) {
      return Aff.Fork(immediate, aff);
    };
  };

  exports._liftEff = Aff.Sync;

  exports._parAffMap = function (f) {
    return function (aff) {
      return Aff.ParMap(f, aff);
    };
  };

  exports._parAffApply = function (aff1) {
    return function (aff2) {
      return Aff.ParApply(aff1, aff2);
    };
  };

  exports._parAffAlt = function (aff1) {
    return function (aff2) {
      return Aff.ParAlt(aff1, aff2);
    };
  };

  exports.makeAff = Aff.Async;

  exports._makeFiber = function (util, aff) {
    return function () {
      return Aff.Fiber(util, null, aff);
    };
  };

  exports._delay = function () {
    function setDelay(n, k) {
      if (n === 0 && typeof setImmediate !== "undefined") {
        return setImmediate(k);
      } else {
        return setTimeout(k, n);
      }
    }

    function clearDelay(n, t) {
      if (n === 0 && typeof clearImmediate !== "undefined") {
        return clearImmediate(t);
      } else {
        return clearTimeout(t);
      }
    }

    return function (right, ms) {
      return Aff.Async(function (cb) {
        return function () {
          var timer = setDelay(ms, cb(right()));
          return function () {
            return Aff.Sync(function () {
              return right(clearDelay(ms, timer));
            });
          };
        };
      });
    };
  }();

  exports._sequential = Aff.Seq;
})(PS["Control.Monad.Aff"] = PS["Control.Monad.Aff"] || {});

(function (exports) {
  "use strict";

  exports.pureE = function (a) {
    return function () {
      return a;
    };
  };

  exports.bindE = function (a) {
    return function (f) {
      return function () {
        return f(a())();
      };
    };
  };

  exports.runPure = function (f) {
    return f();
  };
})(PS["Control.Monad.Eff"] = PS["Control.Monad.Eff"] || {});

(function (exports) {
  "use strict";

  exports.refEq = function (r1) {
    return function (r2) {
      return r1 === r2;
    };
  };
})(PS["Data.Eq"] = PS["Data.Eq"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Eq"];
  var Data_Unit = PS["Data.Unit"];
  var Data_Void = PS["Data.Void"];

  var Eq = function Eq(eq) {
    this.eq = eq;
  };

  var eqString = new Eq($foreign.refEq);

  var eq = function eq(dict) {
    return dict.eq;
  };

  exports["Eq"] = Eq;
  exports["eq"] = eq;
  exports["eqString"] = eqString;
})(PS["Data.Eq"] = PS["Data.Eq"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Data_Boolean = PS["Data.Boolean"];
  var Data_Eq = PS["Data.Eq"];
  var Data_EuclideanRing = PS["Data.EuclideanRing"];
  var Data_Function = PS["Data.Function"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var Monoid = function Monoid(Semigroup0, mempty) {
    this.Semigroup0 = Semigroup0;
    this.mempty = mempty;
  };

  var monoidString = new Monoid(function () {
    return Data_Semigroup.semigroupString;
  }, "");

  var mempty = function mempty(dict) {
    return dict.mempty;
  };

  exports["Monoid"] = Monoid;
  exports["mempty"] = mempty;
  exports["monoidString"] = monoidString;
})(PS["Data.Monoid"] = PS["Data.Monoid"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Control.Monad.Eff"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad = PS["Control.Monad"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Unit = PS["Data.Unit"];
  var monadEff = new Control_Monad.Monad(function () {
    return applicativeEff;
  }, function () {
    return bindEff;
  });
  var bindEff = new Control_Bind.Bind(function () {
    return applyEff;
  }, $foreign.bindE);
  var applyEff = new Control_Apply.Apply(function () {
    return functorEff;
  }, Control_Monad.ap(monadEff));
  var applicativeEff = new Control_Applicative.Applicative(function () {
    return applyEff;
  }, $foreign.pureE);
  var functorEff = new Data_Functor.Functor(Control_Applicative.liftA1(applicativeEff));
  exports["functorEff"] = functorEff;
  exports["applyEff"] = applyEff;
  exports["applicativeEff"] = applicativeEff;
  exports["bindEff"] = bindEff;
  exports["monadEff"] = monadEff;
  exports["runPure"] = $foreign.runPure;
})(PS["Control.Monad.Eff"] = PS["Control.Monad.Eff"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Category = PS["Control.Category"];
  var Control_Monad = PS["Control.Monad"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];

  var MonadEff = function MonadEff(Monad0, liftEff) {
    this.Monad0 = Monad0;
    this.liftEff = liftEff;
  };

  var liftEff = function liftEff(dict) {
    return dict.liftEff;
  };

  exports["liftEff"] = liftEff;
  exports["MonadEff"] = MonadEff;
})(PS["Control.Monad.Eff.Class"] = PS["Control.Monad.Eff.Class"] || {});

(function (exports) {
  "use strict";

  exports.error = function (msg) {
    return new Error(msg);
  };

  exports.message = function (e) {
    return e.message;
  };

  exports.catchException = function (c) {
    return function (t) {
      return function () {
        try {
          return t();
        } catch (e) {
          if (e instanceof Error || Object.prototype.toString.call(e) === "[object Error]") {
            return c(e)();
          } else {
            return c(new Error(e.toString()))();
          }
        }
      };
    };
  };
})(PS["Control.Monad.Eff.Exception"] = PS["Control.Monad.Eff.Exception"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Category = PS["Control.Category"];

  var Bifunctor = function Bifunctor(bimap) {
    this.bimap = bimap;
  };

  var bimap = function bimap(dict) {
    return dict.bimap;
  };

  var lmap = function lmap(dictBifunctor) {
    return function (f) {
      return bimap(dictBifunctor)(f)(Control_Category.id(Control_Category.categoryFn));
    };
  };

  exports["bimap"] = bimap;
  exports["Bifunctor"] = Bifunctor;
  exports["lmap"] = lmap;
})(PS["Data.Bifunctor"] = PS["Data.Bifunctor"] || {});

(function (exports) {
  "use strict";

  exports.foldrArray = function (f) {
    return function (init) {
      return function (xs) {
        var acc = init;
        var len = xs.length;

        for (var i = len - 1; i >= 0; i--) {
          acc = f(xs[i])(acc);
        }

        return acc;
      };
    };
  };

  exports.foldlArray = function (f) {
    return function (init) {
      return function (xs) {
        var acc = init;
        var len = xs.length;

        for (var i = 0; i < len; i++) {
          acc = f(acc)(xs[i]);
        }

        return acc;
      };
    };
  };
})(PS["Data.Foldable"] = PS["Data.Foldable"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Extend = PS["Control.Extend"];
  var Control_Monad = PS["Control.Monad"];
  var Control_MonadZero = PS["Control.MonadZero"];
  var Control_Plus = PS["Control.Plus"];
  var Data_Bounded = PS["Data.Bounded"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Functor_Invariant = PS["Data.Functor.Invariant"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Show = PS["Data.Show"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var Nothing = function () {
    function Nothing() {}

    ;
    Nothing.value = new Nothing();
    return Nothing;
  }();

  var Just = function () {
    function Just(value0) {
      this.value0 = value0;
    }

    ;

    Just.create = function (value0) {
      return new Just(value0);
    };

    return Just;
  }();

  var maybe$prime = function maybe$prime(v) {
    return function (v1) {
      return function (v2) {
        if (v2 instanceof Nothing) {
          return v(Data_Unit.unit);
        }

        ;

        if (v2 instanceof Just) {
          return v1(v2.value0);
        }

        ;
        throw new Error("Failed pattern match at Data.Maybe line 232, column 1 - line 232, column 62: " + [v.constructor.name, v1.constructor.name, v2.constructor.name]);
      };
    };
  };

  var maybe = function maybe(v) {
    return function (v1) {
      return function (v2) {
        if (v2 instanceof Nothing) {
          return v;
        }

        ;

        if (v2 instanceof Just) {
          return v1(v2.value0);
        }

        ;
        throw new Error("Failed pattern match at Data.Maybe line 219, column 1 - line 219, column 51: " + [v.constructor.name, v1.constructor.name, v2.constructor.name]);
      };
    };
  };

  var isJust = maybe(false)(Data_Function["const"](true));

  var fromMaybe = function fromMaybe(a) {
    return maybe(a)(Control_Category.id(Control_Category.categoryFn));
  };

  exports["Nothing"] = Nothing;
  exports["Just"] = Just;
  exports["maybe"] = maybe;
  exports["maybe'"] = maybe$prime;
  exports["fromMaybe"] = fromMaybe;
  exports["isJust"] = isJust;
})(PS["Data.Maybe"] = PS["Data.Maybe"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Prelude = PS["Prelude"];

  var Newtype = function Newtype(unwrap, wrap) {
    this.unwrap = unwrap;
    this.wrap = wrap;
  };

  var wrap = function wrap(dict) {
    return dict.wrap;
  };

  var unwrap = function unwrap(dict) {
    return dict.unwrap;
  };

  exports["unwrap"] = unwrap;
  exports["wrap"] = wrap;
  exports["Newtype"] = Newtype;
})(PS["Data.Newtype"] = PS["Data.Newtype"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Foldable"];
  var Control_Alt = PS["Control.Alt"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Plus = PS["Control.Plus"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Maybe_First = PS["Data.Maybe.First"];
  var Data_Maybe_Last = PS["Data.Maybe.Last"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Monoid_Additive = PS["Data.Monoid.Additive"];
  var Data_Monoid_Conj = PS["Data.Monoid.Conj"];
  var Data_Monoid_Disj = PS["Data.Monoid.Disj"];
  var Data_Monoid_Dual = PS["Data.Monoid.Dual"];
  var Data_Monoid_Endo = PS["Data.Monoid.Endo"];
  var Data_Monoid_Multiplicative = PS["Data.Monoid.Multiplicative"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var Foldable = function Foldable(foldMap, foldl, foldr) {
    this.foldMap = foldMap;
    this.foldl = foldl;
    this.foldr = foldr;
  };

  var foldr = function foldr(dict) {
    return dict.foldr;
  };

  var oneOfMap = function oneOfMap(dictFoldable) {
    return function (dictPlus) {
      return function (f) {
        return foldr(dictFoldable)(function ($194) {
          return Control_Alt.alt(dictPlus.Alt0())(f($194));
        })(Control_Plus.empty(dictPlus));
      };
    };
  };

  var traverse_ = function traverse_(dictApplicative) {
    return function (dictFoldable) {
      return function (f) {
        return foldr(dictFoldable)(function ($195) {
          return Control_Apply.applySecond(dictApplicative.Apply0())(f($195));
        })(Control_Applicative.pure(dictApplicative)(Data_Unit.unit));
      };
    };
  };

  var foldl = function foldl(dict) {
    return dict.foldl;
  };

  var intercalate = function intercalate(dictFoldable) {
    return function (dictMonoid) {
      return function (sep) {
        return function (xs) {
          var go = function go(v) {
            return function (x) {
              if (v.init) {
                return {
                  init: false,
                  acc: x
                };
              }

              ;
              return {
                init: false,
                acc: Data_Semigroup.append(dictMonoid.Semigroup0())(v.acc)(Data_Semigroup.append(dictMonoid.Semigroup0())(sep)(x))
              };
            };
          };

          return foldl(dictFoldable)(go)({
            init: true,
            acc: Data_Monoid.mempty(dictMonoid)
          })(xs).acc;
        };
      };
    };
  };

  var foldMapDefaultR = function foldMapDefaultR(dictFoldable) {
    return function (dictMonoid) {
      return function (f) {
        return foldr(dictFoldable)(function (x) {
          return function (acc) {
            return Data_Semigroup.append(dictMonoid.Semigroup0())(f(x))(acc);
          };
        })(Data_Monoid.mempty(dictMonoid));
      };
    };
  };

  var foldableArray = new Foldable(function (dictMonoid) {
    return foldMapDefaultR(foldableArray)(dictMonoid);
  }, $foreign.foldlArray, $foreign.foldrArray);

  var foldMap = function foldMap(dict) {
    return dict.foldMap;
  };

  exports["Foldable"] = Foldable;
  exports["foldr"] = foldr;
  exports["foldl"] = foldl;
  exports["foldMap"] = foldMap;
  exports["foldMapDefaultR"] = foldMapDefaultR;
  exports["traverse_"] = traverse_;
  exports["oneOfMap"] = oneOfMap;
  exports["intercalate"] = intercalate;
  exports["foldableArray"] = foldableArray;
})(PS["Data.Foldable"] = PS["Data.Foldable"] || {});

(function (exports) {
  "use strict"; // jshint maxparams: 3

  exports.traverseArrayImpl = function () {
    function Cont(fn) {
      this.fn = fn;
    }

    var emptyList = {};

    var ConsCell = function ConsCell(head, tail) {
      this.head = head;
      this.tail = tail;
    };

    function consList(x) {
      return function (xs) {
        return new ConsCell(x, xs);
      };
    }

    function listToArray(list) {
      var arr = [];
      var xs = list;

      while (xs !== emptyList) {
        arr.push(xs.head);
        xs = xs.tail;
      }

      return arr;
    }

    return function (apply) {
      return function (map) {
        return function (pure) {
          return function (f) {
            var buildFrom = function buildFrom(x, ys) {
              return apply(map(consList)(f(x)))(ys);
            };

            var go = function go(acc, currentLen, xs) {
              if (currentLen === 0) {
                return acc;
              } else {
                var last = xs[currentLen - 1];
                return new Cont(function () {
                  return go(buildFrom(last, acc), currentLen - 1, xs);
                });
              }
            };

            return function (array) {
              var result = go(pure(emptyList), array.length, array);

              while (result instanceof Cont) {
                result = result.fn();
              }

              return map(listToArray)(result);
            };
          };
        };
      };
    };
  }();
})(PS["Data.Traversable"] = PS["Data.Traversable"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Traversable"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Category = PS["Control.Category"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Maybe_First = PS["Data.Maybe.First"];
  var Data_Maybe_Last = PS["Data.Maybe.Last"];
  var Data_Monoid_Additive = PS["Data.Monoid.Additive"];
  var Data_Monoid_Conj = PS["Data.Monoid.Conj"];
  var Data_Monoid_Disj = PS["Data.Monoid.Disj"];
  var Data_Monoid_Dual = PS["Data.Monoid.Dual"];
  var Data_Monoid_Multiplicative = PS["Data.Monoid.Multiplicative"];
  var Data_Traversable_Accum = PS["Data.Traversable.Accum"];
  var Data_Traversable_Accum_Internal = PS["Data.Traversable.Accum.Internal"];
  var Prelude = PS["Prelude"];

  var Traversable = function Traversable(Foldable1, Functor0, sequence, traverse) {
    this.Foldable1 = Foldable1;
    this.Functor0 = Functor0;
    this.sequence = sequence;
    this.traverse = traverse;
  };

  var traverse = function traverse(dict) {
    return dict.traverse;
  };

  var sequenceDefault = function sequenceDefault(dictTraversable) {
    return function (dictApplicative) {
      return traverse(dictTraversable)(dictApplicative)(Control_Category.id(Control_Category.categoryFn));
    };
  };

  var traversableArray = new Traversable(function () {
    return Data_Foldable.foldableArray;
  }, function () {
    return Data_Functor.functorArray;
  }, function (dictApplicative) {
    return sequenceDefault(traversableArray)(dictApplicative);
  }, function (dictApplicative) {
    return $foreign.traverseArrayImpl(Control_Apply.apply(dictApplicative.Apply0()))(Data_Functor.map(dictApplicative.Apply0().Functor0()))(Control_Applicative.pure(dictApplicative));
  });

  var sequence = function sequence(dict) {
    return dict.sequence;
  };

  exports["Traversable"] = Traversable;
  exports["traverse"] = traverse;
  exports["sequence"] = sequence;
  exports["sequenceDefault"] = sequenceDefault;
  exports["traversableArray"] = traversableArray;
})(PS["Data.Traversable"] = PS["Data.Traversable"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Extend = PS["Control.Extend"];
  var Control_Monad = PS["Control.Monad"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Bifoldable = PS["Data.Bifoldable"];
  var Data_Bifunctor = PS["Data.Bifunctor"];
  var Data_Bitraversable = PS["Data.Bitraversable"];
  var Data_Bounded = PS["Data.Bounded"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Functor_Invariant = PS["Data.Functor.Invariant"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Show = PS["Data.Show"];
  var Data_Traversable = PS["Data.Traversable"];
  var Prelude = PS["Prelude"];

  var Left = function () {
    function Left(value0) {
      this.value0 = value0;
    }

    ;

    Left.create = function (value0) {
      return new Left(value0);
    };

    return Left;
  }();

  var Right = function () {
    function Right(value0) {
      this.value0 = value0;
    }

    ;

    Right.create = function (value0) {
      return new Right(value0);
    };

    return Right;
  }();

  var functorEither = new Data_Functor.Functor(function (v) {
    return function (v1) {
      if (v1 instanceof Left) {
        return new Left(v1.value0);
      }

      ;

      if (v1 instanceof Right) {
        return new Right(v(v1.value0));
      }

      ;
      throw new Error("Failed pattern match at Data.Either line 36, column 1 - line 36, column 45: " + [v.constructor.name, v1.constructor.name]);
    };
  });

  var either = function either(v) {
    return function (v1) {
      return function (v2) {
        if (v2 instanceof Left) {
          return v(v2.value0);
        }

        ;

        if (v2 instanceof Right) {
          return v1(v2.value0);
        }

        ;
        throw new Error("Failed pattern match at Data.Either line 229, column 1 - line 229, column 64: " + [v.constructor.name, v1.constructor.name, v2.constructor.name]);
      };
    };
  };

  var bifunctorEither = new Data_Bifunctor.Bifunctor(function (v) {
    return function (v1) {
      return function (v2) {
        if (v2 instanceof Left) {
          return new Left(v(v2.value0));
        }

        ;

        if (v2 instanceof Right) {
          return new Right(v1(v2.value0));
        }

        ;
        throw new Error("Failed pattern match at Data.Either line 43, column 1 - line 43, column 45: " + [v.constructor.name, v1.constructor.name, v2.constructor.name]);
      };
    };
  });
  exports["Left"] = Left;
  exports["Right"] = Right;
  exports["either"] = either;
  exports["functorEither"] = functorEither;
  exports["bifunctorEither"] = bifunctorEither;
})(PS["Data.Either"] = PS["Data.Either"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Control.Monad.Eff.Exception"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Show = PS["Data.Show"];
  var Prelude = PS["Prelude"];

  var $$try = function $$try(action) {
    return $foreign.catchException(function ($0) {
      return Control_Applicative.pure(Control_Monad_Eff.applicativeEff)(Data_Either.Left.create($0));
    })(Data_Functor.map(Control_Monad_Eff.functorEff)(Data_Either.Right.create)(action));
  };

  exports["try"] = $$try;
  exports["error"] = $foreign.error;
  exports["message"] = $foreign.message;
})(PS["Control.Monad.Eff.Exception"] = PS["Control.Monad.Eff.Exception"] || {});

(function (exports) {
  "use strict";

  exports.unsafeCoerceEff = function (f) {
    return f;
  };
})(PS["Control.Monad.Eff.Unsafe"] = PS["Control.Monad.Eff.Unsafe"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Control.Monad.Eff.Unsafe"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  exports["unsafeCoerceEff"] = $foreign.unsafeCoerceEff;
})(PS["Control.Monad.Eff.Unsafe"] = PS["Control.Monad.Eff.Unsafe"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var MonadThrow = function MonadThrow(Monad0, throwError) {
    this.Monad0 = Monad0;
    this.throwError = throwError;
  };

  var MonadError = function MonadError(MonadThrow0, catchError) {
    this.MonadThrow0 = MonadThrow0;
    this.catchError = catchError;
  };

  var throwError = function throwError(dict) {
    return dict.throwError;
  };

  var catchError = function catchError(dict) {
    return dict.catchError;
  };

  exports["catchError"] = catchError;
  exports["throwError"] = throwError;
  exports["MonadThrow"] = MonadThrow;
  exports["MonadError"] = MonadError;
})(PS["Control.Monad.Error.Class"] = PS["Control.Monad.Error.Class"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Comonad = PS["Control.Comonad"];
  var Control_Extend = PS["Control.Extend"];
  var Control_Lazy = PS["Control.Lazy"];
  var Control_Monad = PS["Control.Monad"];
  var Data_BooleanAlgebra = PS["Data.BooleanAlgebra"];
  var Data_Bounded = PS["Data.Bounded"];
  var Data_CommutativeRing = PS["Data.CommutativeRing"];
  var Data_Eq = PS["Data.Eq"];
  var Data_EuclideanRing = PS["Data.EuclideanRing"];
  var Data_Field = PS["Data.Field"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Functor_Invariant = PS["Data.Functor.Invariant"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Show = PS["Data.Show"];
  var Data_Traversable = PS["Data.Traversable"];
  var Prelude = PS["Prelude"];

  var Identity = function Identity(x) {
    return x;
  };

  var newtypeIdentity = new Data_Newtype.Newtype(function (n) {
    return n;
  }, Identity);
  var functorIdentity = new Data_Functor.Functor(function (f) {
    return function (v) {
      return f(v);
    };
  });
  var applyIdentity = new Control_Apply.Apply(function () {
    return functorIdentity;
  }, function (v) {
    return function (v1) {
      return v(v1);
    };
  });
  var bindIdentity = new Control_Bind.Bind(function () {
    return applyIdentity;
  }, function (v) {
    return function (f) {
      return f(v);
    };
  });
  var applicativeIdentity = new Control_Applicative.Applicative(function () {
    return applyIdentity;
  }, Identity);
  var monadIdentity = new Control_Monad.Monad(function () {
    return applicativeIdentity;
  }, function () {
    return bindIdentity;
  });
  exports["Identity"] = Identity;
  exports["newtypeIdentity"] = newtypeIdentity;
  exports["functorIdentity"] = functorIdentity;
  exports["applyIdentity"] = applyIdentity;
  exports["applicativeIdentity"] = applicativeIdentity;
  exports["bindIdentity"] = bindIdentity;
  exports["monadIdentity"] = monadIdentity;
})(PS["Data.Identity"] = PS["Data.Identity"] || {});

(function (exports) {
  "use strict"; // module Partial.Unsafe

  exports.unsafePartial = function (f) {
    return f();
  };
})(PS["Partial.Unsafe"] = PS["Partial.Unsafe"] || {});

(function (exports) {
  "use strict"; // module Partial

  exports.crashWith = function () {
    return function (msg) {
      throw new Error(msg);
    };
  };
})(PS["Partial"] = PS["Partial"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Partial"];
  exports["crashWith"] = $foreign.crashWith;
})(PS["Partial"] = PS["Partial"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Partial.Unsafe"];
  var Partial = PS["Partial"];

  var unsafeCrashWith = function unsafeCrashWith(msg) {
    return $foreign.unsafePartial(function (dictPartial) {
      return Partial.crashWith(dictPartial)(msg);
    });
  };

  exports["unsafeCrashWith"] = unsafeCrashWith;
})(PS["Partial.Unsafe"] = PS["Partial.Unsafe"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad = PS["Control.Monad"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Unsafe = PS["Control.Monad.Eff.Unsafe"];
  var Control_Monad_ST = PS["Control.Monad.ST"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Bifunctor = PS["Data.Bifunctor"];
  var Data_Either = PS["Data.Either"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Identity = PS["Data.Identity"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Unit = PS["Data.Unit"];
  var Partial_Unsafe = PS["Partial.Unsafe"];
  var Prelude = PS["Prelude"];

  var Loop = function () {
    function Loop(value0) {
      this.value0 = value0;
    }

    ;

    Loop.create = function (value0) {
      return new Loop(value0);
    };

    return Loop;
  }();

  var Done = function () {
    function Done(value0) {
      this.value0 = value0;
    }

    ;

    Done.create = function (value0) {
      return new Done(value0);
    };

    return Done;
  }();

  var MonadRec = function MonadRec(Monad0, tailRecM) {
    this.Monad0 = Monad0;
    this.tailRecM = tailRecM;
  };

  var tailRecM = function tailRecM(dict) {
    return dict.tailRecM;
  };

  exports["Loop"] = Loop;
  exports["Done"] = Done;
  exports["MonadRec"] = MonadRec;
  exports["tailRecM"] = tailRecM;
})(PS["Control.Monad.Rec.Class"] = PS["Control.Monad.Rec.Class"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Biapplicative = PS["Control.Biapplicative"];
  var Control_Biapply = PS["Control.Biapply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Comonad = PS["Control.Comonad"];
  var Control_Extend = PS["Control.Extend"];
  var Control_Lazy = PS["Control.Lazy"];
  var Control_Monad = PS["Control.Monad"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Bifoldable = PS["Data.Bifoldable"];
  var Data_Bifunctor = PS["Data.Bifunctor"];
  var Data_Bitraversable = PS["Data.Bitraversable"];
  var Data_BooleanAlgebra = PS["Data.BooleanAlgebra"];
  var Data_Bounded = PS["Data.Bounded"];
  var Data_CommutativeRing = PS["Data.CommutativeRing"];
  var Data_Distributive = PS["Data.Distributive"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Functor_Invariant = PS["Data.Functor.Invariant"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Maybe_First = PS["Data.Maybe.First"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Show = PS["Data.Show"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];
  var Type_Equality = PS["Type.Equality"];

  var Tuple = function () {
    function Tuple(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    Tuple.create = function (value0) {
      return function (value1) {
        return new Tuple(value0, value1);
      };
    };

    return Tuple;
  }();

  var fst = function fst(v) {
    return v.value0;
  };

  exports["Tuple"] = Tuple;
  exports["fst"] = fst;
})(PS["Data.Tuple"] = PS["Data.Tuple"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var MonadState = function MonadState(Monad0, state) {
    this.Monad0 = Monad0;
    this.state = state;
  };

  var state = function state(dict) {
    return dict.state;
  };

  var put = function put(dictMonadState) {
    return function (s) {
      return state(dictMonadState)(function (v) {
        return new Data_Tuple.Tuple(Data_Unit.unit, s);
      });
    };
  };

  var get = function get(dictMonadState) {
    return state(dictMonadState)(function (s) {
      return new Data_Tuple.Tuple(s, s);
    });
  };

  exports["state"] = state;
  exports["MonadState"] = MonadState;
  exports["get"] = get;
  exports["put"] = put;
})(PS["Control.Monad.State.Class"] = PS["Control.Monad.State.Class"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Prelude = PS["Prelude"];

  var MonadTrans = function MonadTrans(lift) {
    this.lift = lift;
  };

  var lift = function lift(dict) {
    return dict.lift;
  };

  exports["lift"] = lift;
  exports["MonadTrans"] = MonadTrans;
})(PS["Control.Monad.Trans.Class"] = PS["Control.Monad.Trans.Class"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Monad = PS["Control.Monad"];
  var Control_Monad_Cont_Class = PS["Control.Monad.Cont.Class"];
  var Control_Monad_Eff_Class = PS["Control.Monad.Eff.Class"];
  var Control_Monad_Error_Class = PS["Control.Monad.Error.Class"];
  var Control_Monad_Reader_Class = PS["Control.Monad.Reader.Class"];
  var Control_Monad_Rec_Class = PS["Control.Monad.Rec.Class"];
  var Control_Monad_State_Class = PS["Control.Monad.State.Class"];
  var Control_Monad_Trans_Class = PS["Control.Monad.Trans.Class"];
  var Control_Monad_Writer_Class = PS["Control.Monad.Writer.Class"];
  var Control_MonadPlus = PS["Control.MonadPlus"];
  var Control_MonadZero = PS["Control.MonadZero"];
  var Control_Plus = PS["Control.Plus"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Tuple = PS["Data.Tuple"];
  var Prelude = PS["Prelude"];

  var ExceptT = function ExceptT(x) {
    return x;
  };

  var runExceptT = function runExceptT(v) {
    return v;
  };

  var monadTransExceptT = new Control_Monad_Trans_Class.MonadTrans(function (dictMonad) {
    return function (m) {
      return Control_Bind.bind(dictMonad.Bind1())(m)(function (v) {
        return Control_Applicative.pure(dictMonad.Applicative0())(new Data_Either.Right(v));
      });
    };
  });

  var mapExceptT = function mapExceptT(f) {
    return function (v) {
      return f(v);
    };
  };

  var functorExceptT = function functorExceptT(dictFunctor) {
    return new Data_Functor.Functor(function (f) {
      return mapExceptT(Data_Functor.map(dictFunctor)(Data_Functor.map(Data_Either.functorEither)(f)));
    });
  };

  var monadExceptT = function monadExceptT(dictMonad) {
    return new Control_Monad.Monad(function () {
      return applicativeExceptT(dictMonad);
    }, function () {
      return bindExceptT(dictMonad);
    });
  };

  var bindExceptT = function bindExceptT(dictMonad) {
    return new Control_Bind.Bind(function () {
      return applyExceptT(dictMonad);
    }, function (v) {
      return function (k) {
        return Control_Bind.bind(dictMonad.Bind1())(v)(Data_Either.either(function ($97) {
          return Control_Applicative.pure(dictMonad.Applicative0())(Data_Either.Left.create($97));
        })(function (a) {
          var v1 = k(a);
          return v1;
        }));
      };
    });
  };

  var applyExceptT = function applyExceptT(dictMonad) {
    return new Control_Apply.Apply(function () {
      return functorExceptT(dictMonad.Bind1().Apply0().Functor0());
    }, Control_Monad.ap(monadExceptT(dictMonad)));
  };

  var applicativeExceptT = function applicativeExceptT(dictMonad) {
    return new Control_Applicative.Applicative(function () {
      return applyExceptT(dictMonad);
    }, function ($98) {
      return ExceptT(Control_Applicative.pure(dictMonad.Applicative0())(Data_Either.Right.create($98)));
    });
  };

  var monadThrowExceptT = function monadThrowExceptT(dictMonad) {
    return new Control_Monad_Error_Class.MonadThrow(function () {
      return monadExceptT(dictMonad);
    }, function ($102) {
      return ExceptT(Control_Applicative.pure(dictMonad.Applicative0())(Data_Either.Left.create($102)));
    });
  };

  var altExceptT = function altExceptT(dictSemigroup) {
    return function (dictMonad) {
      return new Control_Alt.Alt(function () {
        return functorExceptT(dictMonad.Bind1().Apply0().Functor0());
      }, function (v) {
        return function (v1) {
          return Control_Bind.bind(dictMonad.Bind1())(v)(function (v2) {
            if (v2 instanceof Data_Either.Right) {
              return Control_Applicative.pure(dictMonad.Applicative0())(new Data_Either.Right(v2.value0));
            }

            ;

            if (v2 instanceof Data_Either.Left) {
              return Control_Bind.bind(dictMonad.Bind1())(v1)(function (v3) {
                if (v3 instanceof Data_Either.Right) {
                  return Control_Applicative.pure(dictMonad.Applicative0())(new Data_Either.Right(v3.value0));
                }

                ;

                if (v3 instanceof Data_Either.Left) {
                  return Control_Applicative.pure(dictMonad.Applicative0())(new Data_Either.Left(Data_Semigroup.append(dictSemigroup)(v2.value0)(v3.value0)));
                }

                ;
                throw new Error("Failed pattern match at Control.Monad.Except.Trans line 88, column 9 - line 90, column 49: " + [v3.constructor.name]);
              });
            }

            ;
            throw new Error("Failed pattern match at Control.Monad.Except.Trans line 84, column 5 - line 90, column 49: " + [v2.constructor.name]);
          });
        };
      });
    };
  };

  exports["ExceptT"] = ExceptT;
  exports["runExceptT"] = runExceptT;
  exports["mapExceptT"] = mapExceptT;
  exports["functorExceptT"] = functorExceptT;
  exports["applyExceptT"] = applyExceptT;
  exports["applicativeExceptT"] = applicativeExceptT;
  exports["bindExceptT"] = bindExceptT;
  exports["monadExceptT"] = monadExceptT;
  exports["altExceptT"] = altExceptT;
  exports["monadTransExceptT"] = monadTransExceptT;
  exports["monadThrowExceptT"] = monadThrowExceptT;
})(PS["Control.Monad.Except.Trans"] = PS["Control.Monad.Except.Trans"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Cont_Trans = PS["Control.Monad.Cont.Trans"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Class = PS["Control.Monad.Eff.Class"];
  var Control_Monad_Eff_Ref = PS["Control.Monad.Eff.Ref"];
  var Control_Monad_Eff_Unsafe = PS["Control.Monad.Eff.Unsafe"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Monad_Maybe_Trans = PS["Control.Monad.Maybe.Trans"];
  var Control_Monad_Reader_Trans = PS["Control.Monad.Reader.Trans"];
  var Control_Monad_Writer_Trans = PS["Control.Monad.Writer.Trans"];
  var Control_Plus = PS["Control.Plus"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Functor_Compose = PS["Data.Functor.Compose"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var Parallel = function Parallel(Applicative1, Monad0, parallel, sequential) {
    this.Applicative1 = Applicative1;
    this.Monad0 = Monad0;
    this.parallel = parallel;
    this.sequential = sequential;
  };

  var sequential = function sequential(dict) {
    return dict.sequential;
  };

  var parallel = function parallel(dict) {
    return dict.parallel;
  };

  exports["Parallel"] = Parallel;
  exports["parallel"] = parallel;
  exports["sequential"] = sequential;
})(PS["Control.Parallel.Class"] = PS["Control.Parallel.Class"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alternative = PS["Control.Alternative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Category = PS["Control.Category"];
  var Control_Parallel_Class = PS["Control.Parallel.Class"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Traversable = PS["Data.Traversable"];
  var Prelude = PS["Prelude"];

  var parOneOf = function parOneOf(dictParallel) {
    return function (dictAlternative) {
      return function (dictFoldable) {
        return function (dictFunctor) {
          return function ($23) {
            return Control_Parallel_Class.sequential(dictParallel)(Data_Foldable.oneOfMap(dictFoldable)(dictAlternative.Plus1())(Control_Parallel_Class.parallel(dictParallel))($23));
          };
        };
      };
    };
  };

  exports["parOneOf"] = parOneOf;
})(PS["Control.Parallel"] = PS["Control.Parallel"] || {});

(function (exports) {
  "use strict";

  exports.runFn4 = function (fn) {
    return function (a) {
      return function (b) {
        return function (c) {
          return function (d) {
            return fn(a, b, c, d);
          };
        };
      };
    };
  };
})(PS["Data.Function.Uncurried"] = PS["Data.Function.Uncurried"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Function.Uncurried"];
  var Data_Unit = PS["Data.Unit"];
  exports["runFn4"] = $foreign.runFn4;
})(PS["Data.Function.Uncurried"] = PS["Data.Function.Uncurried"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Apply = PS["Control.Apply"];
  var Control_Category = PS["Control.Category"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Eq = PS["Data.Eq"];
  var Data_EuclideanRing = PS["Data.EuclideanRing"];
  var Data_Generic = PS["Data.Generic"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Show = PS["Data.Show"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var Duration = function Duration(fromDuration, toDuration) {
    this.fromDuration = fromDuration;
    this.toDuration = toDuration;
  };

  var toDuration = function toDuration(dict) {
    return dict.toDuration;
  };

  var fromDuration = function fromDuration(dict) {
    return dict.fromDuration;
  };

  var durationMilliseconds = new Duration(Control_Category.id(Control_Category.categoryFn), Control_Category.id(Control_Category.categoryFn));
  exports["fromDuration"] = fromDuration;
  exports["toDuration"] = toDuration;
  exports["Duration"] = Duration;
  exports["durationMilliseconds"] = durationMilliseconds;
})(PS["Data.Time.Duration"] = PS["Data.Time.Duration"] || {});

(function (exports) {
  "use strict"; // module Unsafe.Coerce

  exports.unsafeCoerce = function (x) {
    return x;
  };
})(PS["Unsafe.Coerce"] = PS["Unsafe.Coerce"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Unsafe.Coerce"];
  exports["unsafeCoerce"] = $foreign.unsafeCoerce;
})(PS["Unsafe.Coerce"] = PS["Unsafe.Coerce"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Control.Monad.Aff"];
  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Lazy = PS["Control.Lazy"];
  var Control_Monad = PS["Control.Monad"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Class = PS["Control.Monad.Eff.Class"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Eff_Unsafe = PS["Control.Monad.Eff.Unsafe"];
  var Control_Monad_Error_Class = PS["Control.Monad.Error.Class"];
  var Control_Monad_Rec_Class = PS["Control.Monad.Rec.Class"];
  var Control_Parallel = PS["Control.Parallel"];
  var Control_Parallel_Class = PS["Control.Parallel.Class"];
  var Control_Plus = PS["Control.Plus"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Function = PS["Data.Function"];
  var Data_Function_Uncurried = PS["Data.Function.Uncurried"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Time_Duration = PS["Data.Time.Duration"];
  var Data_Unit = PS["Data.Unit"];
  var Partial_Unsafe = PS["Partial.Unsafe"];
  var Prelude = PS["Prelude"];
  var Unsafe_Coerce = PS["Unsafe.Coerce"];

  var Canceler = function Canceler(x) {
    return x;
  };

  var functorParAff = new Data_Functor.Functor($foreign._parAffMap);
  var functorAff = new Data_Functor.Functor($foreign._map);

  var forkAff = $foreign._fork(true);

  var ffiUtil = function () {
    var unsafeFromRight = function unsafeFromRight(v) {
      if (v instanceof Data_Either.Right) {
        return v.value0;
      }

      ;

      if (v instanceof Data_Either.Left) {
        return Partial_Unsafe.unsafeCrashWith("unsafeFromRight: Left");
      }

      ;
      throw new Error("Failed pattern match at Control.Monad.Aff line 402, column 21 - line 404, column 31: " + [v.constructor.name]);
    };

    var unsafeFromLeft = function unsafeFromLeft(v) {
      if (v instanceof Data_Either.Left) {
        return v.value0;
      }

      ;

      if (v instanceof Data_Either.Right) {
        return Partial_Unsafe.unsafeCrashWith("unsafeFromLeft: Right");
      }

      ;
      throw new Error("Failed pattern match at Control.Monad.Aff line 397, column 20 - line 401, column 3: " + [v.constructor.name]);
    };

    var isLeft = function isLeft(v) {
      if (v instanceof Data_Either.Left) {
        return true;
      }

      ;

      if (v instanceof Data_Either.Right) {
        return false;
      }

      ;
      throw new Error("Failed pattern match at Control.Monad.Aff line 392, column 12 - line 394, column 20: " + [v.constructor.name]);
    };

    return {
      isLeft: isLeft,
      fromLeft: unsafeFromLeft,
      fromRight: unsafeFromRight,
      left: Data_Either.Left.create,
      right: Data_Either.Right.create
    };
  }();

  var makeFiber = function makeFiber(aff) {
    return $foreign._makeFiber(ffiUtil, aff);
  };

  var launchAff = function launchAff(aff) {
    return function __do() {
      var v = makeFiber(aff)();
      v.run();
      return v;
    };
  };

  var launchAff_ = function launchAff_($49) {
    return Data_Functor["void"](Control_Monad_Eff.functorEff)(launchAff($49));
  };

  var delay = function delay(v) {
    return $foreign._delay(Data_Either.Right.create, v);
  };

  var applyParAff = new Control_Apply.Apply(function () {
    return functorParAff;
  }, $foreign._parAffApply);
  var monadAff = new Control_Monad.Monad(function () {
    return applicativeAff;
  }, function () {
    return bindAff;
  });
  var bindAff = new Control_Bind.Bind(function () {
    return applyAff;
  }, $foreign._bind);
  var applyAff = new Control_Apply.Apply(function () {
    return functorAff;
  }, Control_Monad.ap(monadAff));
  var applicativeAff = new Control_Applicative.Applicative(function () {
    return applyAff;
  }, $foreign._pure);
  var monadEffAff = new Control_Monad_Eff_Class.MonadEff(function () {
    return monadAff;
  }, $foreign._liftEff);

  var effCanceler = function effCanceler($50) {
    return Canceler(Data_Function["const"](Control_Monad_Eff_Class.liftEff(monadEffAff)($50)));
  };

  var liftEff$prime = function liftEff$prime($51) {
    return Control_Monad_Eff_Class.liftEff(monadEffAff)(Control_Monad_Eff_Unsafe.unsafeCoerceEff($51));
  };

  var monadThrowAff = new Control_Monad_Error_Class.MonadThrow(function () {
    return monadAff;
  }, $foreign._throwError);
  var monadErrorAff = new Control_Monad_Error_Class.MonadError(function () {
    return monadThrowAff;
  }, $foreign._catchError);
  var parallelAff = new Control_Parallel_Class.Parallel(function () {
    return applicativeParAff;
  }, function () {
    return monadAff;
  }, Unsafe_Coerce.unsafeCoerce, $foreign._sequential);
  var applicativeParAff = new Control_Applicative.Applicative(function () {
    return applyParAff;
  }, function ($54) {
    return Control_Parallel_Class.parallel(parallelAff)(Control_Applicative.pure(applicativeAff)($54));
  });
  var monadRecAff = new Control_Monad_Rec_Class.MonadRec(function () {
    return monadAff;
  }, function (k) {
    var go = function go(a) {
      return Control_Bind.bind(bindAff)(k(a))(function (v) {
        if (v instanceof Control_Monad_Rec_Class.Done) {
          return Control_Applicative.pure(applicativeAff)(v.value0);
        }

        ;

        if (v instanceof Control_Monad_Rec_Class.Loop) {
          return go(v.value0);
        }

        ;
        throw new Error("Failed pattern match at Control.Monad.Aff line 101, column 7 - line 103, column 22: " + [v.constructor.name]);
      });
    };

    return go;
  });
  var nonCanceler = Data_Function["const"](Control_Applicative.pure(applicativeAff)(Data_Unit.unit));
  var altParAff = new Control_Alt.Alt(function () {
    return functorParAff;
  }, $foreign._parAffAlt);
  var altAff = new Control_Alt.Alt(function () {
    return functorAff;
  }, function (a1) {
    return function (a2) {
      return Control_Monad_Error_Class.catchError(monadErrorAff)(a1)(Data_Function["const"](a2));
    };
  });
  var plusAff = new Control_Plus.Plus(function () {
    return altAff;
  }, Control_Monad_Error_Class.throwError(monadThrowAff)(Control_Monad_Eff_Exception.error("Always fails")));
  var plusParAff = new Control_Plus.Plus(function () {
    return altParAff;
  }, Control_Parallel_Class.parallel(parallelAff)(Control_Plus.empty(plusAff)));
  var alternativeParAff = new Control_Alternative.Alternative(function () {
    return applicativeParAff;
  }, function () {
    return plusParAff;
  });
  exports["Canceler"] = Canceler;
  exports["launchAff"] = launchAff;
  exports["launchAff_"] = launchAff_;
  exports["forkAff"] = forkAff;
  exports["liftEff'"] = liftEff$prime;
  exports["delay"] = delay;
  exports["nonCanceler"] = nonCanceler;
  exports["effCanceler"] = effCanceler;
  exports["functorAff"] = functorAff;
  exports["applyAff"] = applyAff;
  exports["applicativeAff"] = applicativeAff;
  exports["bindAff"] = bindAff;
  exports["monadAff"] = monadAff;
  exports["altAff"] = altAff;
  exports["plusAff"] = plusAff;
  exports["monadRecAff"] = monadRecAff;
  exports["monadThrowAff"] = monadThrowAff;
  exports["monadErrorAff"] = monadErrorAff;
  exports["monadEffAff"] = monadEffAff;
  exports["functorParAff"] = functorParAff;
  exports["applyParAff"] = applyParAff;
  exports["applicativeParAff"] = applicativeParAff;
  exports["altParAff"] = altParAff;
  exports["plusParAff"] = plusParAff;
  exports["alternativeParAff"] = alternativeParAff;
  exports["parallelAff"] = parallelAff;
  exports["makeAff"] = $foreign.makeAff;
})(PS["Control.Monad.Aff"] = PS["Control.Monad.Aff"] || {});

(function (exports) {
  /* globals exports, setTimeout */

  /* jshint -W097 */
  "use strict";

  var AVar = function () {
    function MutableQueue() {
      this.head = null;
      this.last = null;
      this.size = 0;
    }

    function MutableCell(queue, value) {
      this.queue = queue;
      this.value = value;
      this.next = null;
      this.prev = null;
    }

    function AVar(value) {
      this.draining = false;
      this.error = null;
      this.value = value;
      this.takes = new MutableQueue();
      this.reads = new MutableQueue();
      this.puts = new MutableQueue();
    }

    var EMPTY = {};

    function putLast(queue, value) {
      var cell = new MutableCell(queue, value);

      switch (queue.size) {
        case 0:
          queue.head = cell;
          break;

        case 1:
          cell.prev = queue.head;
          queue.head.next = cell;
          queue.last = cell;
          break;

        default:
          cell.prev = queue.last;
          queue.last.next = cell;
          queue.last = cell;
      }

      queue.size++;
      return cell;
    }

    function takeLast(queue) {
      var cell;

      switch (queue.size) {
        case 0:
          return null;

        case 1:
          cell = queue.head;
          queue.head = null;
          break;

        case 2:
          cell = queue.last;
          queue.head.next = null;
          queue.last = null;
          break;

        default:
          cell = queue.last;
          queue.last = cell.prev;
          queue.last.next = null;
      }

      cell.prev = null;
      cell.queue = null;
      queue.size--;
      return cell.value;
    }

    function takeHead(queue) {
      var cell;

      switch (queue.size) {
        case 0:
          return null;

        case 1:
          cell = queue.head;
          queue.head = null;
          break;

        case 2:
          cell = queue.head;
          queue.last.prev = null;
          queue.head = queue.last;
          queue.last = null;
          break;

        default:
          cell = queue.head;
          queue.head = cell.next;
          queue.head.prev = null;
      }

      cell.next = null;
      cell.queue = null;
      queue.size--;
      return cell.value;
    }

    function deleteCell(cell) {
      if (cell.queue === null) {
        return;
      }

      if (cell.queue.last === cell) {
        takeLast(cell.queue);
        return;
      }

      if (cell.queue.head === cell) {
        takeHead(cell.queue);
        return;
      }

      if (cell.prev) {
        cell.prev.next = cell.next;
      }

      if (cell.next) {
        cell.next.prev = cell.prev;
      }

      cell.queue.size--;
      cell.queue = null;
      cell.value = null;
      cell.next = null;
      cell.prev = null;
    }

    function drainVar(util, avar) {
      if (avar.draining) {
        return;
      }

      var ps = avar.puts;
      var ts = avar.takes;
      var rs = avar.reads;
      var p, r, t, value, rsize;
      avar.draining = true;
      /* jshint -W084 */

      while (1) {
        p = null;
        r = null;
        t = null;
        value = avar.value;
        rsize = rs.size;

        if (avar.error !== null) {
          value = util.left(avar.error);

          while (p = takeHead(ps)) {
            runEff(p.cb(value));
          }

          while (r = takeHead(rs)) {
            runEff(r(value));
          }

          while (t = takeHead(ts)) {
            runEff(t(value));
          }

          break;
        } // Process the next put. We do not immediately invoke the callback
        // because we want to preserve ordering. If there are takes/reads
        // we want to run those first.


        if (value === EMPTY && (p = takeHead(ps))) {
          avar.value = value = p.value;
        }

        if (value !== EMPTY) {
          // We go ahead and queue up the next take for the same reasons as
          // above. Invoking the read callbacks can affect the mutable queue.
          t = takeHead(ts); // We only want to process the reads queued up before running these
          // callbacks so we guard on rsize.

          while (rsize-- && (r = takeHead(rs))) {
            runEff(r(util.right(value)));
          }

          if (t !== null) {
            avar.value = EMPTY;
            runEff(t(util.right(value)));
          }
        }

        if (p !== null) {
          runEff(p.cb(util.right(void 0)));
        } // Callbacks could have queued up more items so we need to guard on the
        // actual mutable properties.


        if (avar.value === EMPTY && ps.size === 0 || avar.value !== EMPTY && ts.size === 0) {
          break;
        }
      }
      /* jshint +W084 */


      avar.draining = false;
    }

    function runEff(eff) {
      try {
        eff();
      } catch (error) {
        setTimeout(function () {
          throw error;
        }, 0);
      }
    }

    AVar.EMPTY = EMPTY;
    AVar.putLast = putLast;
    AVar.takeLast = takeLast;
    AVar.takeHead = takeHead;
    AVar.deleteCell = deleteCell;
    AVar.drainVar = drainVar;
    return AVar;
  }();

  exports.makeEmptyVar = function () {
    return new AVar(AVar.EMPTY);
  };

  exports.makeVar = function (value) {
    return function () {
      return new AVar(value);
    };
  };

  exports._putVar = function (util, value, avar, cb) {
    return function () {
      var cell = AVar.putLast(avar.puts, {
        cb: cb,
        value: value
      });
      AVar.drainVar(util, avar);
      return function () {
        AVar.deleteCell(cell);
      };
    };
  };

  exports._takeVar = function (util, avar, cb) {
    return function () {
      var cell = AVar.putLast(avar.takes, cb);
      AVar.drainVar(util, avar);
      return function () {
        AVar.deleteCell(cell);
      };
    };
  };

  exports._readVar = function (util, avar, cb) {
    return function () {
      var cell = AVar.putLast(avar.reads, cb);
      AVar.drainVar(util, avar);
      return function () {
        AVar.deleteCell(cell);
      };
    };
  };
})(PS["Control.Monad.Eff.AVar"] = PS["Control.Monad.Eff.AVar"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Control.Monad.Eff.AVar"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Function_Uncurried = PS["Data.Function.Uncurried"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Maybe = PS["Data.Maybe"];
  var Prelude = PS["Prelude"];

  var Killed = function () {
    function Killed(value0) {
      this.value0 = value0;
    }

    ;

    Killed.create = function (value0) {
      return new Killed(value0);
    };

    return Killed;
  }();

  var Filled = function () {
    function Filled(value0) {
      this.value0 = value0;
    }

    ;

    Filled.create = function (value0) {
      return new Filled(value0);
    };

    return Filled;
  }();

  var Empty = function () {
    function Empty() {}

    ;
    Empty.value = new Empty();
    return Empty;
  }();

  var ffiUtil = {
    left: Data_Either.Left.create,
    right: Data_Either.Right.create,
    nothing: Data_Maybe.Nothing.value,
    just: Data_Maybe.Just.create,
    killed: Killed.create,
    filled: Filled.create,
    empty: Empty.value
  };

  var putVar = function putVar(value) {
    return function (avar) {
      return function (cb) {
        return $foreign._putVar(ffiUtil, value, avar, cb);
      };
    };
  };

  var readVar = function readVar(avar) {
    return function (cb) {
      return $foreign._readVar(ffiUtil, avar, cb);
    };
  };

  var takeVar = function takeVar(avar) {
    return function (cb) {
      return $foreign._takeVar(ffiUtil, avar, cb);
    };
  };

  exports["Killed"] = Killed;
  exports["Filled"] = Filled;
  exports["Empty"] = Empty;
  exports["takeVar"] = takeVar;
  exports["putVar"] = putVar;
  exports["readVar"] = readVar;
  exports["makeVar"] = $foreign.makeVar;
  exports["makeEmptyVar"] = $foreign.makeEmptyVar;
})(PS["Control.Monad.Eff.AVar"] = PS["Control.Monad.Eff.AVar"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Aff = PS["Control.Monad.Aff"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_AVar = PS["Control.Monad.Eff.AVar"];
  var Control_Monad_Eff_Class = PS["Control.Monad.Eff.Class"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Maybe = PS["Data.Maybe"];
  var Prelude = PS["Prelude"];

  var takeVar = function takeVar(avar) {
    return Control_Monad_Aff.makeAff(function (k) {
      return function __do() {
        var v = Control_Monad_Eff_AVar.takeVar(avar)(k)();
        return Control_Monad_Aff.effCanceler(v);
      };
    });
  };

  var readVar = function readVar(avar) {
    return Control_Monad_Aff.makeAff(function (k) {
      return function __do() {
        var v = Control_Monad_Eff_AVar.readVar(avar)(k)();
        return Control_Monad_Aff.effCanceler(v);
      };
    });
  };

  var putVar = function putVar(value) {
    return function (avar) {
      return Control_Monad_Aff.makeAff(function (k) {
        return function __do() {
          var v = Control_Monad_Eff_AVar.putVar(value)(avar)(k)();
          return Control_Monad_Aff.effCanceler(v);
        };
      });
    };
  };

  var makeVar = function makeVar($10) {
    return Control_Monad_Eff_Class.liftEff(Control_Monad_Aff.monadEffAff)(Control_Monad_Eff_AVar.makeVar($10));
  };

  var makeEmptyVar = Control_Monad_Eff_Class.liftEff(Control_Monad_Aff.monadEffAff)(Control_Monad_Eff_AVar.makeEmptyVar);
  exports["makeVar"] = makeVar;
  exports["makeEmptyVar"] = makeEmptyVar;
  exports["takeVar"] = takeVar;
  exports["putVar"] = putVar;
  exports["readVar"] = readVar;
})(PS["Control.Monad.Aff.AVar"] = PS["Control.Monad.Aff.AVar"] || {});

(function (exports) {
  "use strict";

  exports.runEffFn1 = function runEffFn1(fn) {
    return function (a) {
      return function () {
        return fn(a);
      };
    };
  };
})(PS["Control.Monad.Eff.Uncurried"] = PS["Control.Monad.Eff.Uncurried"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Control.Monad.Eff.Uncurried"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  exports["runEffFn1"] = $foreign.runEffFn1;
})(PS["Control.Monad.Eff.Uncurried"] = PS["Control.Monad.Eff.Uncurried"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Monad_Error_Class = PS["Control.Monad.Error.Class"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Identity = PS["Data.Identity"];
  var Data_Newtype = PS["Data.Newtype"];
  var Prelude = PS["Prelude"];

  var runExcept = function runExcept($0) {
    return Data_Newtype.unwrap(Data_Identity.newtypeIdentity)(Control_Monad_Except_Trans.runExceptT($0));
  };

  var mapExcept = function mapExcept(f) {
    return Control_Monad_Except_Trans.mapExceptT(function ($1) {
      return Data_Identity.Identity(f(Data_Newtype.unwrap(Data_Identity.newtypeIdentity)($1)));
    });
  };

  exports["runExcept"] = runExcept;
  exports["mapExcept"] = mapExcept;
})(PS["Control.Monad.Except"] = PS["Control.Monad.Except"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Category = PS["Control.Category"];
  var Control_Plus = PS["Control.Plus"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_FoldableWithIndex = PS["Data.FoldableWithIndex"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_FunctorWithIndex = PS["Data.FunctorWithIndex"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semigroup_Foldable = PS["Data.Semigroup.Foldable"];
  var Data_Show = PS["Data.Show"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_TraversableWithIndex = PS["Data.TraversableWithIndex"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unfoldable = PS["Data.Unfoldable"];
  var Data_Unfoldable1 = PS["Data.Unfoldable1"];
  var Prelude = PS["Prelude"];

  var NonEmpty = function () {
    function NonEmpty(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    NonEmpty.create = function (value0) {
      return function (value1) {
        return new NonEmpty(value0, value1);
      };
    };

    return NonEmpty;
  }();

  var singleton = function singleton(dictPlus) {
    return function (a) {
      return new NonEmpty(a, Control_Plus.empty(dictPlus));
    };
  };

  var showNonEmpty = function showNonEmpty(dictShow) {
    return function (dictShow1) {
      return new Data_Show.Show(function (v) {
        return "(NonEmpty " + (Data_Show.show(dictShow)(v.value0) + (" " + (Data_Show.show(dictShow1)(v.value1) + ")")));
      });
    };
  };

  var functorNonEmpty = function functorNonEmpty(dictFunctor) {
    return new Data_Functor.Functor(function (f) {
      return function (v) {
        return new NonEmpty(f(v.value0), Data_Functor.map(dictFunctor)(f)(v.value1));
      };
    });
  };

  exports["NonEmpty"] = NonEmpty;
  exports["singleton"] = singleton;
  exports["showNonEmpty"] = showNonEmpty;
  exports["functorNonEmpty"] = functorNonEmpty;
})(PS["Data.NonEmpty"] = PS["Data.NonEmpty"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Comonad = PS["Control.Comonad"];
  var Control_Extend = PS["Control.Extend"];
  var Control_Monad = PS["Control.Monad"];
  var Control_MonadPlus = PS["Control.MonadPlus"];
  var Control_MonadZero = PS["Control.MonadZero"];
  var Control_Plus = PS["Control.Plus"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_FoldableWithIndex = PS["Data.FoldableWithIndex"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_FunctorWithIndex = PS["Data.FunctorWithIndex"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_NonEmpty = PS["Data.NonEmpty"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semigroup_Foldable = PS["Data.Semigroup.Foldable"];
  var Data_Semigroup_Traversable = PS["Data.Semigroup.Traversable"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Show = PS["Data.Show"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_TraversableWithIndex = PS["Data.TraversableWithIndex"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unfoldable = PS["Data.Unfoldable"];
  var Prelude = PS["Prelude"];

  var Nil = function () {
    function Nil() {}

    ;
    Nil.value = new Nil();
    return Nil;
  }();

  var Cons = function () {
    function Cons(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    Cons.create = function (value0) {
      return function (value1) {
        return new Cons(value0, value1);
      };
    };

    return Cons;
  }();

  var NonEmptyList = function NonEmptyList(x) {
    return x;
  };

  var toList = function toList(v) {
    return new Cons(v.value0, v.value1);
  };

  var foldableList = new Data_Foldable.Foldable(function (dictMonoid) {
    return function (f) {
      return Data_Foldable.foldl(foldableList)(function (acc) {
        return function ($158) {
          return Data_Semigroup.append(dictMonoid.Semigroup0())(acc)(f($158));
        };
      })(Data_Monoid.mempty(dictMonoid));
    };
  }, function (f) {
    var go = function go($copy_b) {
      return function ($copy_v) {
        var $tco_var_b = $copy_b;
        var $tco_done = false;
        var $tco_result;

        function $tco_loop(b, v) {
          if (v instanceof Nil) {
            $tco_done = true;
            return b;
          }

          ;

          if (v instanceof Cons) {
            $tco_var_b = f(b)(v.value0);
            $copy_v = v.value1;
            return;
          }

          ;
          throw new Error("Failed pattern match at Data.List.Types line 81, column 12 - line 83, column 30: " + [v.constructor.name]);
        }

        ;

        while (!$tco_done) {
          $tco_result = $tco_loop($tco_var_b, $copy_v);
        }

        ;
        return $tco_result;
      };
    };

    return go;
  }, function (f) {
    return function (b) {
      var rev = Data_Foldable.foldl(foldableList)(Data_Function.flip(Cons.create))(Nil.value);
      return function ($159) {
        return Data_Foldable.foldl(foldableList)(Data_Function.flip(f))(b)(rev($159));
      };
    };
  });
  var functorList = new Data_Functor.Functor(function (f) {
    return Data_Foldable.foldr(foldableList)(function (x) {
      return function (acc) {
        return new Cons(f(x), acc);
      };
    })(Nil.value);
  });
  var functorNonEmptyList = Data_NonEmpty.functorNonEmpty(functorList);
  var semigroupList = new Data_Semigroup.Semigroup(function (xs) {
    return function (ys) {
      return Data_Foldable.foldr(foldableList)(Cons.create)(ys)(xs);
    };
  });
  var semigroupNonEmptyList = new Data_Semigroup.Semigroup(function (v) {
    return function (as$prime) {
      return new Data_NonEmpty.NonEmpty(v.value0, Data_Semigroup.append(semigroupList)(v.value1)(toList(as$prime)));
    };
  });

  var showList = function showList(dictShow) {
    return new Data_Show.Show(function (v) {
      if (v instanceof Nil) {
        return "Nil";
      }

      ;
      return "(" + (Data_Foldable.intercalate(foldableList)(Data_Monoid.monoidString)(" : ")(Data_Functor.map(functorList)(Data_Show.show(dictShow))(v)) + " : Nil)");
    });
  };

  var showNonEmptyList = function showNonEmptyList(dictShow) {
    return new Data_Show.Show(function (v) {
      return "(NonEmptyList " + (Data_Show.show(Data_NonEmpty.showNonEmpty(dictShow)(showList(dictShow)))(v) + ")");
    });
  };

  var applyList = new Control_Apply.Apply(function () {
    return functorList;
  }, function (v) {
    return function (v1) {
      if (v instanceof Nil) {
        return Nil.value;
      }

      ;

      if (v instanceof Cons) {
        return Data_Semigroup.append(semigroupList)(Data_Functor.map(functorList)(v.value0)(v1))(Control_Apply.apply(applyList)(v.value1)(v1));
      }

      ;
      throw new Error("Failed pattern match at Data.List.Types line 120, column 1 - line 120, column 33: " + [v.constructor.name, v1.constructor.name]);
    };
  });
  var applyNonEmptyList = new Control_Apply.Apply(function () {
    return functorNonEmptyList;
  }, function (v) {
    return function (v1) {
      return new Data_NonEmpty.NonEmpty(v.value0(v1.value0), Data_Semigroup.append(semigroupList)(Control_Apply.apply(applyList)(v.value1)(new Cons(v1.value0, Nil.value)))(Control_Apply.apply(applyList)(new Cons(v.value0, v.value1))(v1.value1)));
    };
  });
  var altList = new Control_Alt.Alt(function () {
    return functorList;
  }, Data_Semigroup.append(semigroupList));
  var plusList = new Control_Plus.Plus(function () {
    return altList;
  }, Nil.value);
  var applicativeNonEmptyList = new Control_Applicative.Applicative(function () {
    return applyNonEmptyList;
  }, function ($168) {
    return NonEmptyList(Data_NonEmpty.singleton(plusList)($168));
  });
  exports["Nil"] = Nil;
  exports["Cons"] = Cons;
  exports["NonEmptyList"] = NonEmptyList;
  exports["toList"] = toList;
  exports["showList"] = showList;
  exports["semigroupList"] = semigroupList;
  exports["functorList"] = functorList;
  exports["foldableList"] = foldableList;
  exports["applyList"] = applyList;
  exports["altList"] = altList;
  exports["plusList"] = plusList;
  exports["showNonEmptyList"] = showNonEmptyList;
  exports["functorNonEmptyList"] = functorNonEmptyList;
  exports["applyNonEmptyList"] = applyNonEmptyList;
  exports["applicativeNonEmptyList"] = applicativeNonEmptyList;
  exports["semigroupNonEmptyList"] = semigroupNonEmptyList;
})(PS["Data.List.Types"] = PS["Data.List.Types"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Lazy = PS["Control.Lazy"];
  var Control_Monad_Rec_Class = PS["Control.Monad.Rec.Class"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Bifunctor = PS["Data.Bifunctor"];
  var Data_Boolean = PS["Data.Boolean"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_FunctorWithIndex = PS["Data.FunctorWithIndex"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_NonEmpty = PS["Data.NonEmpty"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Show = PS["Data.Show"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unfoldable = PS["Data.Unfoldable"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var singleton = function singleton(a) {
    return new Data_List_Types.Cons(a, Data_List_Types.Nil.value);
  };

  var reverse = function () {
    var go = function go($copy_acc) {
      return function ($copy_v) {
        var $tco_var_acc = $copy_acc;
        var $tco_done = false;
        var $tco_result;

        function $tco_loop(acc, v) {
          if (v instanceof Data_List_Types.Nil) {
            $tco_done = true;
            return acc;
          }

          ;

          if (v instanceof Data_List_Types.Cons) {
            $tco_var_acc = new Data_List_Types.Cons(v.value0, acc);
            $copy_v = v.value1;
            return;
          }

          ;
          throw new Error("Failed pattern match at Data.List line 368, column 3 - line 368, column 19: " + [acc.constructor.name, v.constructor.name]);
        }

        ;

        while (!$tco_done) {
          $tco_result = $tco_loop($tco_var_acc, $copy_v);
        }

        ;
        return $tco_result;
      };
    };

    return go(Data_List_Types.Nil.value);
  }();

  var $$null = function $$null(v) {
    if (v instanceof Data_List_Types.Nil) {
      return true;
    }

    ;
    return false;
  };

  var fromFoldable = function fromFoldable(dictFoldable) {
    return Data_Foldable.foldr(dictFoldable)(Data_List_Types.Cons.create)(Data_List_Types.Nil.value);
  };

  exports["fromFoldable"] = fromFoldable;
  exports["singleton"] = singleton;
  exports["null"] = $$null;
  exports["reverse"] = reverse;
})(PS["Data.List"] = PS["Data.List"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Data_List = PS["Data.List"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Show = PS["Data.Show"];
  var Data_Tuple = PS["Data.Tuple"];

  var CatQueue = function () {
    function CatQueue(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    CatQueue.create = function (value0) {
      return function (value1) {
        return new CatQueue(value0, value1);
      };
    };

    return CatQueue;
  }();

  var uncons = function uncons($copy_v) {
    var $tco_done = false;
    var $tco_result;

    function $tco_loop(v) {
      if (v.value0 instanceof Data_List_Types.Nil && v.value1 instanceof Data_List_Types.Nil) {
        $tco_done = true;
        return Data_Maybe.Nothing.value;
      }

      ;

      if (v.value0 instanceof Data_List_Types.Nil) {
        $copy_v = new CatQueue(Data_List.reverse(v.value1), Data_List_Types.Nil.value);
        return;
      }

      ;

      if (v.value0 instanceof Data_List_Types.Cons) {
        $tco_done = true;
        return new Data_Maybe.Just(new Data_Tuple.Tuple(v.value0.value0, new CatQueue(v.value0.value1, v.value1)));
      }

      ;
      throw new Error("Failed pattern match at Data.CatQueue line 50, column 1 - line 50, column 63: " + [v.constructor.name]);
    }

    ;

    while (!$tco_done) {
      $tco_result = $tco_loop($copy_v);
    }

    ;
    return $tco_result;
  };

  var snoc = function snoc(v) {
    return function (a) {
      return new CatQueue(v.value0, new Data_List_Types.Cons(a, v.value1));
    };
  };

  var $$null = function $$null(v) {
    if (v.value0 instanceof Data_List_Types.Nil && v.value1 instanceof Data_List_Types.Nil) {
      return true;
    }

    ;
    return false;
  };

  var empty = new CatQueue(Data_List_Types.Nil.value, Data_List_Types.Nil.value);
  exports["CatQueue"] = CatQueue;
  exports["empty"] = empty;
  exports["null"] = $$null;
  exports["snoc"] = snoc;
  exports["uncons"] = uncons;
})(PS["Data.CatQueue"] = PS["Data.CatQueue"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad = PS["Control.Monad"];
  var Control_MonadPlus = PS["Control.MonadPlus"];
  var Control_MonadZero = PS["Control.MonadZero"];
  var Control_Plus = PS["Control.Plus"];
  var Data_CatQueue = PS["Data.CatQueue"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_List = PS["Data.List"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_NaturalTransformation = PS["Data.NaturalTransformation"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Show = PS["Data.Show"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unfoldable = PS["Data.Unfoldable"];

  var CatNil = function () {
    function CatNil() {}

    ;
    CatNil.value = new CatNil();
    return CatNil;
  }();

  var CatCons = function () {
    function CatCons(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    CatCons.create = function (value0) {
      return function (value1) {
        return new CatCons(value0, value1);
      };
    };

    return CatCons;
  }();

  var link = function link(v) {
    return function (cat) {
      if (v instanceof CatNil) {
        return cat;
      }

      ;

      if (v instanceof CatCons) {
        return new CatCons(v.value0, Data_CatQueue.snoc(v.value1)(cat));
      }

      ;
      throw new Error("Failed pattern match at Data.CatList line 110, column 1 - line 110, column 54: " + [v.constructor.name, cat.constructor.name]);
    };
  };

  var foldr = function foldr(k) {
    return function (b) {
      return function (q) {
        var foldl = function foldl($copy_v) {
          return function ($copy_c) {
            return function ($copy_v1) {
              var $tco_var_v = $copy_v;
              var $tco_var_c = $copy_c;
              var $tco_done = false;
              var $tco_result;

              function $tco_loop(v, c, v1) {
                if (v1 instanceof Data_List_Types.Nil) {
                  $tco_done = true;
                  return c;
                }

                ;

                if (v1 instanceof Data_List_Types.Cons) {
                  $tco_var_v = v;
                  $tco_var_c = v(c)(v1.value0);
                  $copy_v1 = v1.value1;
                  return;
                }

                ;
                throw new Error("Failed pattern match at Data.CatList line 125, column 3 - line 125, column 59: " + [v.constructor.name, c.constructor.name, v1.constructor.name]);
              }

              ;

              while (!$tco_done) {
                $tco_result = $tco_loop($tco_var_v, $tco_var_c, $copy_v1);
              }

              ;
              return $tco_result;
            };
          };
        };

        var go = function go($copy_xs) {
          return function ($copy_ys) {
            var $tco_var_xs = $copy_xs;
            var $tco_done = false;
            var $tco_result;

            function $tco_loop(xs, ys) {
              var v = Data_CatQueue.uncons(xs);

              if (v instanceof Data_Maybe.Nothing) {
                $tco_done = true;
                return foldl(function (x) {
                  return function (i) {
                    return i(x);
                  };
                })(b)(ys);
              }

              ;

              if (v instanceof Data_Maybe.Just) {
                $tco_var_xs = v.value0.value1;
                $copy_ys = new Data_List_Types.Cons(k(v.value0.value0), ys);
                return;
              }

              ;
              throw new Error("Failed pattern match at Data.CatList line 121, column 14 - line 123, column 67: " + [v.constructor.name]);
            }

            ;

            while (!$tco_done) {
              $tco_result = $tco_loop($tco_var_xs, $copy_ys);
            }

            ;
            return $tco_result;
          };
        };

        return go(q)(Data_List_Types.Nil.value);
      };
    };
  };

  var uncons = function uncons(v) {
    if (v instanceof CatNil) {
      return Data_Maybe.Nothing.value;
    }

    ;

    if (v instanceof CatCons) {
      return new Data_Maybe.Just(new Data_Tuple.Tuple(v.value0, function () {
        var $41 = Data_CatQueue["null"](v.value1);

        if ($41) {
          return CatNil.value;
        }

        ;
        return foldr(link)(CatNil.value)(v.value1);
      }()));
    }

    ;
    throw new Error("Failed pattern match at Data.CatList line 101, column 1 - line 101, column 61: " + [v.constructor.name]);
  };

  var empty = CatNil.value;

  var append = function append(v) {
    return function (v1) {
      if (v1 instanceof CatNil) {
        return v;
      }

      ;

      if (v instanceof CatNil) {
        return v1;
      }

      ;
      return link(v)(v1);
    };
  };

  var semigroupCatList = new Data_Semigroup.Semigroup(append);

  var snoc = function snoc(cat) {
    return function (a) {
      return append(cat)(new CatCons(a, Data_CatQueue.empty));
    };
  };

  exports["CatNil"] = CatNil;
  exports["CatCons"] = CatCons;
  exports["empty"] = empty;
  exports["append"] = append;
  exports["snoc"] = snoc;
  exports["uncons"] = uncons;
  exports["semigroupCatList"] = semigroupCatList;
})(PS["Data.CatList"] = PS["Data.CatList"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Monad = PS["Control.Monad"];
  var Control_Monad_Rec_Class = PS["Control.Monad.Rec.Class"];
  var Control_Monad_Trans_Class = PS["Control.Monad.Trans.Class"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_CatList = PS["Data.CatList"];
  var Data_Either = PS["Data.Either"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_Tuple = PS["Data.Tuple"];
  var Prelude = PS["Prelude"];
  var Unsafe_Coerce = PS["Unsafe.Coerce"];

  var Free = function () {
    function Free(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    Free.create = function (value0) {
      return function (value1) {
        return new Free(value0, value1);
      };
    };

    return Free;
  }();

  var Return = function () {
    function Return(value0) {
      this.value0 = value0;
    }

    ;

    Return.create = function (value0) {
      return new Return(value0);
    };

    return Return;
  }();

  var Bind = function () {
    function Bind(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    Bind.create = function (value0) {
      return function (value1) {
        return new Bind(value0, value1);
      };
    };

    return Bind;
  }();

  var toView = function toView($copy_v) {
    var $tco_done = false;
    var $tco_result;

    function $tco_loop(v) {
      var runExpF = function runExpF(v2) {
        return v2;
      };

      var concatF = function concatF(v2) {
        return function (r) {
          return new Free(v2.value0, Data_Semigroup.append(Data_CatList.semigroupCatList)(v2.value1)(r));
        };
      };

      if (v.value0 instanceof Return) {
        var v2 = Data_CatList.uncons(v.value1);

        if (v2 instanceof Data_Maybe.Nothing) {
          $tco_done = true;
          return new Return(v.value0.value0);
        }

        ;

        if (v2 instanceof Data_Maybe.Just) {
          $copy_v = concatF(runExpF(v2.value0.value0)(v.value0.value0))(v2.value0.value1);
          return;
        }

        ;
        throw new Error("Failed pattern match at Control.Monad.Free line 220, column 7 - line 224, column 64: " + [v2.constructor.name]);
      }

      ;

      if (v.value0 instanceof Bind) {
        $tco_done = true;
        return new Bind(v.value0.value0, function (a) {
          return concatF(v.value0.value1(a))(v.value1);
        });
      }

      ;
      throw new Error("Failed pattern match at Control.Monad.Free line 218, column 3 - line 226, column 56: " + [v.value0.constructor.name]);
    }

    ;

    while (!$tco_done) {
      $tco_result = $tco_loop($copy_v);
    }

    ;
    return $tco_result;
  };

  var fromView = function fromView(f) {
    return new Free(f, Data_CatList.empty);
  };

  var freeMonad = new Control_Monad.Monad(function () {
    return freeApplicative;
  }, function () {
    return freeBind;
  });
  var freeFunctor = new Data_Functor.Functor(function (k) {
    return function (f) {
      return Control_Bind.bindFlipped(freeBind)(function ($118) {
        return Control_Applicative.pure(freeApplicative)(k($118));
      })(f);
    };
  });
  var freeBind = new Control_Bind.Bind(function () {
    return freeApply;
  }, function (v) {
    return function (k) {
      return new Free(v.value0, Data_CatList.snoc(v.value1)(k));
    };
  });
  var freeApply = new Control_Apply.Apply(function () {
    return freeFunctor;
  }, Control_Monad.ap(freeMonad));
  var freeApplicative = new Control_Applicative.Applicative(function () {
    return freeApply;
  }, function ($119) {
    return fromView(Return.create($119));
  });

  var liftF = function liftF(f) {
    return fromView(new Bind(f, function ($120) {
      return Control_Applicative.pure(freeApplicative)($120);
    }));
  };

  var foldFree = function foldFree(dictMonadRec) {
    return function (k) {
      var go = function go(f) {
        var v = toView(f);

        if (v instanceof Return) {
          return Data_Functor.map(dictMonadRec.Monad0().Bind1().Apply0().Functor0())(Control_Monad_Rec_Class.Done.create)(Control_Applicative.pure(dictMonadRec.Monad0().Applicative0())(v.value0));
        }

        ;

        if (v instanceof Bind) {
          return Data_Functor.map(dictMonadRec.Monad0().Bind1().Apply0().Functor0())(function ($127) {
            return Control_Monad_Rec_Class.Loop.create(v.value1($127));
          })(k(v.value0));
        }

        ;
        throw new Error("Failed pattern match at Control.Monad.Free line 151, column 10 - line 153, column 37: " + [v.constructor.name]);
      };

      return Control_Monad_Rec_Class.tailRecM(dictMonadRec)(go);
    };
  };

  exports["liftF"] = liftF;
  exports["foldFree"] = foldFree;
  exports["freeFunctor"] = freeFunctor;
  exports["freeBind"] = freeBind;
  exports["freeApplicative"] = freeApplicative;
  exports["freeApply"] = freeApply;
  exports["freeMonad"] = freeMonad;
})(PS["Control.Monad.Free"] = PS["Control.Monad.Free"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_List = PS["Data.List"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var allM = function allM(dictMonad) {
    return function (v) {
      return function (v1) {
        if (v1 instanceof Data_List_Types.Nil) {
          return Control_Applicative.pure(dictMonad.Applicative0())(true);
        }

        ;

        if (v1 instanceof Data_List_Types.Cons) {
          return Control_Bind.ifM(dictMonad.Bind1())(v(v1.value0))(allM(dictMonad)(v)(v1.value1))(Control_Applicative.pure(dictMonad.Applicative0())(false));
        }

        ;
        throw new Error("Failed pattern match at Control.Monad.Loops line 163, column 1 - line 163, column 71: " + [v.constructor.name, v1.constructor.name]);
      };
    };
  };

  exports["allM"] = allM;
})(PS["Control.Monad.Loops"] = PS["Control.Monad.Loops"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Lazy = PS["Control.Lazy"];
  var Control_Monad = PS["Control.Monad"];
  var Control_Monad_Cont_Class = PS["Control.Monad.Cont.Class"];
  var Control_Monad_Eff_Class = PS["Control.Monad.Eff.Class"];
  var Control_Monad_Error_Class = PS["Control.Monad.Error.Class"];
  var Control_Monad_Reader_Class = PS["Control.Monad.Reader.Class"];
  var Control_Monad_Rec_Class = PS["Control.Monad.Rec.Class"];
  var Control_Monad_State_Class = PS["Control.Monad.State.Class"];
  var Control_Monad_Trans_Class = PS["Control.Monad.Trans.Class"];
  var Control_Monad_Writer_Class = PS["Control.Monad.Writer.Class"];
  var Control_MonadPlus = PS["Control.MonadPlus"];
  var Control_MonadZero = PS["Control.MonadZero"];
  var Control_Plus = PS["Control.Plus"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];

  var StateT = function StateT(x) {
    return x;
  };

  var runStateT = function runStateT(v) {
    return v;
  };

  var monadTransStateT = new Control_Monad_Trans_Class.MonadTrans(function (dictMonad) {
    return function (m) {
      return function (s) {
        return Control_Bind.bind(dictMonad.Bind1())(m)(function (v) {
          return Control_Applicative.pure(dictMonad.Applicative0())(new Data_Tuple.Tuple(v, s));
        });
      };
    };
  });

  var functorStateT = function functorStateT(dictFunctor) {
    return new Data_Functor.Functor(function (f) {
      return function (v) {
        return function (s) {
          return Data_Functor.map(dictFunctor)(function (v1) {
            return new Data_Tuple.Tuple(f(v1.value0), v1.value1);
          })(v(s));
        };
      };
    });
  };

  var evalStateT = function evalStateT(dictFunctor) {
    return function (v) {
      return function (s) {
        return Data_Functor.map(dictFunctor)(Data_Tuple.fst)(v(s));
      };
    };
  };

  var monadStateT = function monadStateT(dictMonad) {
    return new Control_Monad.Monad(function () {
      return applicativeStateT(dictMonad);
    }, function () {
      return bindStateT(dictMonad);
    });
  };

  var bindStateT = function bindStateT(dictMonad) {
    return new Control_Bind.Bind(function () {
      return applyStateT(dictMonad);
    }, function (v) {
      return function (f) {
        return function (s) {
          return Control_Bind.bind(dictMonad.Bind1())(v(s))(function (v1) {
            var v3 = f(v1.value0);
            return v3(v1.value1);
          });
        };
      };
    });
  };

  var applyStateT = function applyStateT(dictMonad) {
    return new Control_Apply.Apply(function () {
      return functorStateT(dictMonad.Bind1().Apply0().Functor0());
    }, Control_Monad.ap(monadStateT(dictMonad)));
  };

  var applicativeStateT = function applicativeStateT(dictMonad) {
    return new Control_Applicative.Applicative(function () {
      return applyStateT(dictMonad);
    }, function (a) {
      return function (s) {
        return Control_Applicative.pure(dictMonad.Applicative0())(new Data_Tuple.Tuple(a, s));
      };
    });
  };

  var monadRecStateT = function monadRecStateT(dictMonadRec) {
    return new Control_Monad_Rec_Class.MonadRec(function () {
      return monadStateT(dictMonadRec.Monad0());
    }, function (f) {
      return function (a) {
        var f$prime = function f$prime(v) {
          return Control_Bind.bind(dictMonadRec.Monad0().Bind1())(function () {
            var v1 = f(v.value0);
            return v1;
          }()(v.value1))(function (v1) {
            return Control_Applicative.pure(dictMonadRec.Monad0().Applicative0())(function () {
              if (v1.value0 instanceof Control_Monad_Rec_Class.Loop) {
                return new Control_Monad_Rec_Class.Loop(new Data_Tuple.Tuple(v1.value0.value0, v1.value1));
              }

              ;

              if (v1.value0 instanceof Control_Monad_Rec_Class.Done) {
                return new Control_Monad_Rec_Class.Done(new Data_Tuple.Tuple(v1.value0.value0, v1.value1));
              }

              ;
              throw new Error("Failed pattern match at Control.Monad.State.Trans line 88, column 16 - line 90, column 40: " + [v1.value0.constructor.name]);
            }());
          });
        };

        return function (s) {
          return Control_Monad_Rec_Class.tailRecM(dictMonadRec)(f$prime)(new Data_Tuple.Tuple(a, s));
        };
      };
    });
  };

  var monadStateStateT = function monadStateStateT(dictMonad) {
    return new Control_Monad_State_Class.MonadState(function () {
      return monadStateT(dictMonad);
    }, function (f) {
      return StateT(function ($111) {
        return Control_Applicative.pure(dictMonad.Applicative0())(f($111));
      });
    });
  };

  var monadThrowStateT = function monadThrowStateT(dictMonadThrow) {
    return new Control_Monad_Error_Class.MonadThrow(function () {
      return monadStateT(dictMonadThrow.Monad0());
    }, function (e) {
      return Control_Monad_Trans_Class.lift(monadTransStateT)(dictMonadThrow.Monad0())(Control_Monad_Error_Class.throwError(dictMonadThrow)(e));
    });
  };

  exports["StateT"] = StateT;
  exports["runStateT"] = runStateT;
  exports["evalStateT"] = evalStateT;
  exports["functorStateT"] = functorStateT;
  exports["applyStateT"] = applyStateT;
  exports["applicativeStateT"] = applicativeStateT;
  exports["bindStateT"] = bindStateT;
  exports["monadStateT"] = monadStateT;
  exports["monadRecStateT"] = monadRecStateT;
  exports["monadTransStateT"] = monadTransStateT;
  exports["monadThrowStateT"] = monadThrowStateT;
  exports["monadStateStateT"] = monadStateStateT;
})(PS["Control.Monad.State.Trans"] = PS["Control.Monad.State.Trans"] || {});

(function (exports) {
  "use strict";

  exports._copyEff = function (m) {
    return function () {
      var r = {};

      for (var k in m) {
        if (hasOwnProperty.call(m, k)) {
          r[k] = m[k];
        }
      }

      return r;
    };
  };

  exports.empty = {};

  exports.runST = function (f) {
    return f;
  };

  exports._lookup = function (no, yes, k, m) {
    return k in m ? yes(m[k]) : no;
  };

  function toArrayWithKey(f) {
    return function (m) {
      var r = [];

      for (var k in m) {
        if (hasOwnProperty.call(m, k)) {
          r.push(f(k)(m[k]));
        }
      }

      return r;
    };
  }
})(PS["Data.StrMap"] = PS["Data.StrMap"] || {});

(function (exports) {
  "use strict"; //------------------------------------------------------------------------------
  // Array creation --------------------------------------------------------------
  //------------------------------------------------------------------------------

  exports.range = function (start) {
    return function (end) {
      var step = start > end ? -1 : 1;
      var result = new Array(step * (end - start) + 1);
      var i = start,
          n = 0;

      while (i !== end) {
        result[n++] = i;
        i += step;
      }

      result[n] = i;
      return result;
    };
  }; //------------------------------------------------------------------------------
  // Array size ------------------------------------------------------------------
  //------------------------------------------------------------------------------


  exports.length = function (xs) {
    return xs.length;
  };

  exports.filter = function (f) {
    return function (xs) {
      return xs.filter(f);
    };
  }; //------------------------------------------------------------------------------
  // Zipping ---------------------------------------------------------------------
  //------------------------------------------------------------------------------


  exports.zipWith = function (f) {
    return function (xs) {
      return function (ys) {
        var l = xs.length < ys.length ? xs.length : ys.length;
        var result = new Array(l);

        for (var i = 0; i < l; i++) {
          result[i] = f(xs[i])(ys[i]);
        }

        return result;
      };
    };
  };
})(PS["Data.Array"] = PS["Data.Array"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Array"];
  var Control_Alt = PS["Control.Alt"];
  var Control_Alternative = PS["Control.Alternative"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Lazy = PS["Control.Lazy"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Rec_Class = PS["Control.Monad.Rec.Class"];
  var Control_Monad_ST = PS["Control.Monad.ST"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Array_ST = PS["Data.Array.ST"];
  var Data_Array_ST_Iterator = PS["Data.Array.ST.Iterator"];
  var Data_Boolean = PS["Data.Boolean"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_NonEmpty = PS["Data.NonEmpty"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unfoldable = PS["Data.Unfoldable"];
  var Partial_Unsafe = PS["Partial.Unsafe"];
  var Prelude = PS["Prelude"];
  var zip = $foreign.zipWith(Data_Tuple.Tuple.create);

  var singleton = function singleton(a) {
    return [a];
  };

  exports["singleton"] = singleton;
  exports["zip"] = zip;
  exports["range"] = $foreign.range;
  exports["length"] = $foreign.length;
  exports["filter"] = $foreign.filter;
  exports["zipWith"] = $foreign.zipWith;
})(PS["Data.Array"] = PS["Data.Array"] || {});

(function (exports) {
  "use strict";

  exports.poke = function (m) {
    return function (k) {
      return function (v) {
        return function () {
          m[k] = v;
          return m;
        };
      };
    };
  };
})(PS["Data.StrMap.ST"] = PS["Data.StrMap.ST"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.StrMap.ST"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_ST = PS["Control.Monad.ST"];
  var Data_Maybe = PS["Data.Maybe"];
  exports["poke"] = $foreign.poke;
})(PS["Data.StrMap.ST"] = PS["Data.StrMap.ST"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.StrMap"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_ST = PS["Control.Monad.ST"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Array = PS["Data.Array"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_FoldableWithIndex = PS["Data.FoldableWithIndex"];
  var Data_Function = PS["Data.Function"];
  var Data_Function_Uncurried = PS["Data.Function.Uncurried"];
  var Data_Functor = PS["Data.Functor"];
  var Data_FunctorWithIndex = PS["Data.FunctorWithIndex"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Show = PS["Data.Show"];
  var Data_StrMap_ST = PS["Data.StrMap.ST"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_TraversableWithIndex = PS["Data.TraversableWithIndex"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unfoldable = PS["Data.Unfoldable"];
  var Prelude = PS["Prelude"];
  var thawST = $foreign._copyEff;

  var pureST = function pureST(f) {
    return Control_Monad_Eff.runPure($foreign.runST(f));
  };

  var mutate = function mutate(f) {
    return function (m) {
      return pureST(function __do() {
        var v = thawST(m)();
        var v1 = f(v)();
        return v;
      });
    };
  };

  var lookup = Data_Function_Uncurried.runFn4($foreign._lookup)(Data_Maybe.Nothing.value)(Data_Maybe.Just.create);

  var insert = function insert(k) {
    return function (v) {
      return mutate(function (s) {
        return Data_Functor["void"](Control_Monad_Eff.functorEff)(Data_StrMap_ST.poke(s)(k)(v));
      });
    };
  };

  exports["insert"] = insert;
  exports["lookup"] = lookup;
  exports["thawST"] = thawST;
  exports["pureST"] = pureST;
  exports["empty"] = $foreign.empty;
})(PS["Data.StrMap"] = PS["Data.StrMap"] || {});

(function (exports) {
  exports["showUI'"] = function (sc, screen) {
    return function () {
      var screenJSON = JSON.parse(screen);
      var screenName = screenJSON.tag;
      screenJSON.screen = screenName;

      if (screenName == "InitScreen") {
        screenJSON.screen = "INIT_UI";
      }

      window.__duiShowScreen(sc, screenJSON);
    };
  };

  exports["callAPI'"] = function () {
    console.log("Yet to be implemented");
  };

  exports["getValueFromWindow'"] = function (key) {
    return function (defaultValue) {
      return function () {
        if ((typeof window === "undefined" ? "undefined" : _typeof(window)) == "object" && typeof window[key] != "undefined") {
          return window[key];
        } else {
          return defaultValue;
        }
      };
    };
  };

  exports["toJSON"] = function (str) {
    return function () {
      try {
        return JSON.parse(str);
      } catch (err) {
        console.error(err);
        return {};
      }
    };
  };
})(PS["Engineering.Helpers.Commons"] = PS["Engineering.Helpers.Commons"] || {});

(function (exports) {
  var callbackMapper = {
    map: function map(fn) {
      if (typeof window.__FN_INDEX !== 'undefined' && window.__FN_INDEX !== null) {
        var proxyFnName = 'F' + window.__FN_INDEX;
        window.__PROXY_FN[proxyFnName] = fn;
        window.__FN_INDEX++;
        return proxyFnName;
      } else {
        throw new Error("Please initialise window.__FN_INDEX = 0 in index.js of your project.");
      }
    }
  };

  exports["getPermissionStatus'"] = function (permission) {
    return function () {
      _jbridge.checkPermission(permission);
    };
  };

  exports["requestPermission'"] = function (err, success, permissions) {
    return function () {
      var callback = callbackMapper.map(function (params) {
        console.warn("Permissions", params);
        success(JSON.stringify(params))();
      });
      console.log(permissions, _typeof(permissions));

      _jbridge.requestPermission(permissions, "2", callback);
    };
  };
})(PS["Engineering.OS.Permission"] = PS["Engineering.OS.Permission"] || {});

(function (exports) {
  "use strict";

  exports.toForeign = function (value) {
    return value;
  };

  exports.unsafeFromForeign = function (value) {
    return value;
  };

  exports.typeOf = function (value) {
    return _typeof(value);
  };

  exports.tagOf = function (value) {
    return Object.prototype.toString.call(value).slice(8, -1);
  };

  exports.isNull = function (value) {
    return value === null;
  };

  exports.isUndefined = function (value) {
    return value === undefined;
  };

  exports.isArray = Array.isArray || function (value) {
    return Object.prototype.toString.call(value) === "[object Array]";
  };
})(PS["Data.Foreign"] = PS["Data.Foreign"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Boolean = PS["Data.Boolean"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_List = PS["Data.List"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_NonEmpty = PS["Data.NonEmpty"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semigroup_Foldable = PS["Data.Semigroup.Foldable"];
  var Data_Semigroup_Traversable = PS["Data.Semigroup.Traversable"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unfoldable = PS["Data.Unfoldable"];
  var Partial_Unsafe = PS["Partial.Unsafe"];
  var Prelude = PS["Prelude"];

  var singleton = function singleton($160) {
    return Data_List_Types.NonEmptyList(Data_NonEmpty.singleton(Data_List_Types.plusList)($160));
  };

  exports["singleton"] = singleton;
})(PS["Data.List.NonEmpty"] = PS["Data.List.NonEmpty"] || {});

(function (exports) {
  "use strict";

  exports._indexOf = function (just) {
    return function (nothing) {
      return function (x) {
        return function (s) {
          var i = s.indexOf(x);
          return i === -1 ? nothing : just(i);
        };
      };
    };
  };
})(PS["Data.String"] = PS["Data.String"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.String"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Function = PS["Data.Function"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Newtype = PS["Data.Newtype"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Show = PS["Data.Show"];
  var Data_String_Unsafe = PS["Data.String.Unsafe"];
  var Prelude = PS["Prelude"];

  var indexOf = $foreign._indexOf(Data_Maybe.Just.create)(Data_Maybe.Nothing.value);

  var contains = function contains(pat) {
    return function ($48) {
      return Data_Maybe.isJust(indexOf(pat)($48));
    };
  };

  exports["contains"] = contains;
  exports["indexOf"] = indexOf;
})(PS["Data.String"] = PS["Data.String"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Foreign"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Monad_Error_Class = PS["Control.Monad.Error.Class"];
  var Control_Monad_Except = PS["Control.Monad.Except"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Boolean = PS["Data.Boolean"];
  var Data_Either = PS["Data.Either"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Function = PS["Data.Function"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Identity = PS["Data.Identity"];
  var Data_Int = PS["Data.Int"];
  var Data_List_NonEmpty = PS["Data.List.NonEmpty"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Show = PS["Data.Show"];
  var Data_String = PS["Data.String"];
  var Prelude = PS["Prelude"];

  var ForeignError = function () {
    function ForeignError(value0) {
      this.value0 = value0;
    }

    ;

    ForeignError.create = function (value0) {
      return new ForeignError(value0);
    };

    return ForeignError;
  }();

  var TypeMismatch = function () {
    function TypeMismatch(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    TypeMismatch.create = function (value0) {
      return function (value1) {
        return new TypeMismatch(value0, value1);
      };
    };

    return TypeMismatch;
  }();

  var ErrorAtIndex = function () {
    function ErrorAtIndex(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    ErrorAtIndex.create = function (value0) {
      return function (value1) {
        return new ErrorAtIndex(value0, value1);
      };
    };

    return ErrorAtIndex;
  }();

  var ErrorAtProperty = function () {
    function ErrorAtProperty(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    ErrorAtProperty.create = function (value0) {
      return function (value1) {
        return new ErrorAtProperty(value0, value1);
      };
    };

    return ErrorAtProperty;
  }();

  var JSONError = function () {
    function JSONError(value0) {
      this.value0 = value0;
    }

    ;

    JSONError.create = function (value0) {
      return new JSONError(value0);
    };

    return JSONError;
  }();

  var showForeignError = new Data_Show.Show(function (v) {
    if (v instanceof ForeignError) {
      return "(ForeignError " + (Data_Show.show(Data_Show.showString)(v.value0) + ")");
    }

    ;

    if (v instanceof ErrorAtIndex) {
      return "(ErrorAtIndex " + (Data_Show.show(Data_Show.showInt)(v.value0) + (" " + (Data_Show.show(showForeignError)(v.value1) + ")")));
    }

    ;

    if (v instanceof ErrorAtProperty) {
      return "(ErrorAtProperty " + (Data_Show.show(Data_Show.showString)(v.value0) + (" " + (Data_Show.show(showForeignError)(v.value1) + ")")));
    }

    ;

    if (v instanceof JSONError) {
      return "(JSONError " + (Data_Show.show(Data_Show.showString)(v.value0) + ")");
    }

    ;

    if (v instanceof TypeMismatch) {
      return "(TypeMismatch " + (Data_Show.show(Data_Show.showString)(v.value0) + (" " + (Data_Show.show(Data_Show.showString)(v.value1) + ")")));
    }

    ;
    throw new Error("Failed pattern match at Data.Foreign line 64, column 1 - line 64, column 47: " + [v.constructor.name]);
  });

  var fail = function fail($121) {
    return Control_Monad_Error_Class.throwError(Control_Monad_Except_Trans.monadThrowExceptT(Data_Identity.monadIdentity))(Data_List_NonEmpty.singleton($121));
  };

  var readArray = function readArray(value) {
    if ($foreign.isArray(value)) {
      return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))($foreign.unsafeFromForeign(value));
    }

    ;

    if (Data_Boolean.otherwise) {
      return fail(new TypeMismatch("array", $foreign.tagOf(value)));
    }

    ;
    throw new Error("Failed pattern match at Data.Foreign line 145, column 1 - line 145, column 42: " + [value.constructor.name]);
  };

  var unsafeReadTagged = function unsafeReadTagged(tag) {
    return function (value) {
      if ($foreign.tagOf(value) === tag) {
        return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))($foreign.unsafeFromForeign(value));
      }

      ;

      if (Data_Boolean.otherwise) {
        return fail(new TypeMismatch(tag, $foreign.tagOf(value)));
      }

      ;
      throw new Error("Failed pattern match at Data.Foreign line 104, column 1 - line 104, column 55: " + [tag.constructor.name, value.constructor.name]);
    };
  };

  var readBoolean = unsafeReadTagged("Boolean");
  var readString = unsafeReadTagged("String");
  exports["ForeignError"] = ForeignError;
  exports["TypeMismatch"] = TypeMismatch;
  exports["ErrorAtIndex"] = ErrorAtIndex;
  exports["ErrorAtProperty"] = ErrorAtProperty;
  exports["JSONError"] = JSONError;
  exports["unsafeReadTagged"] = unsafeReadTagged;
  exports["readString"] = readString;
  exports["readBoolean"] = readBoolean;
  exports["readArray"] = readArray;
  exports["fail"] = fail;
  exports["showForeignError"] = showForeignError;
  exports["toForeign"] = $foreign.toForeign;
  exports["typeOf"] = $foreign.typeOf;
  exports["isNull"] = $foreign.isNull;
  exports["isUndefined"] = $foreign.isUndefined;
})(PS["Data.Foreign"] = PS["Data.Foreign"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Monad_Except = PS["Control.Monad.Except"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Array = PS["Data.Array"];
  var Data_Bifunctor = PS["Data.Bifunctor"];
  var Data_Either = PS["Data.Either"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Foreign_Internal = PS["Data.Foreign.Internal"];
  var Data_Foreign_NullOrUndefined = PS["Data.Foreign.NullOrUndefined"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Identity = PS["Data.Identity"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_StrMap = PS["Data.StrMap"];
  var Data_Traversable = PS["Data.Traversable"];
  var Prelude = PS["Prelude"];

  var Decode = function Decode(decode) {
    this.decode = decode;
  };

  var Encode = function Encode(encode) {
    this.encode = encode;
  };

  var stringEncode = new Encode(Data_Foreign.toForeign);
  var stringDecode = new Decode(Data_Foreign.readString);

  var encode = function encode(dict) {
    return dict.encode;
  };

  var decode = function decode(dict) {
    return dict.decode;
  };

  var booleanDecode = new Decode(Data_Foreign.readBoolean);

  var arrayDecode = function arrayDecode(dictDecode) {
    return new Decode(function () {
      var readElement = function readElement(i) {
        return function (value) {
          return Control_Monad_Except.mapExcept(Data_Bifunctor.lmap(Data_Either.bifunctorEither)(Data_Functor.map(Data_List_Types.functorNonEmptyList)(Data_Foreign.ErrorAtIndex.create(i))))(decode(dictDecode)(value));
        };
      };

      var readElements = function readElements(arr) {
        return Data_Traversable.sequence(Data_Traversable.traversableArray)(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))(Data_Array.zipWith(readElement)(Data_Array.range(0)(Data_Array.length(arr)))(arr));
      };

      return Control_Bind.composeKleisli(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Data_Foreign.readArray)(readElements);
    }());
  };

  exports["decode"] = decode;
  exports["encode"] = encode;
  exports["Decode"] = Decode;
  exports["Encode"] = Encode;
  exports["stringDecode"] = stringDecode;
  exports["booleanDecode"] = booleanDecode;
  exports["arrayDecode"] = arrayDecode;
  exports["stringEncode"] = stringEncode;
})(PS["Data.Foreign.Class"] = PS["Data.Foreign.Class"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var TaggedObject = function () {
    function TaggedObject(value0) {
      this.value0 = value0;
    }

    ;

    TaggedObject.create = function (value0) {
      return new TaggedObject(value0);
    };

    return TaggedObject;
  }();

  exports["TaggedObject"] = TaggedObject;
})(PS["Data.Foreign.Generic.Types"] = PS["Data.Foreign.Generic.Types"] || {});

(function (exports) {
  "use strict";

  exports.unsafeReadPropImpl = function (f, s, key, value) {
    return value == null ? f : s(value[key]);
  };

  exports.unsafeHasOwnProperty = function (prop, value) {
    return Object.prototype.hasOwnProperty.call(value, prop);
  };

  exports.unsafeHasProperty = function (prop, value) {
    return prop in value;
  };
})(PS["Data.Foreign.Index"] = PS["Data.Foreign.Index"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Foreign.Index"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Function = PS["Data.Function"];
  var Data_Function_Uncurried = PS["Data.Function.Uncurried"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Identity = PS["Data.Identity"];
  var Data_List_NonEmpty = PS["Data.List.NonEmpty"];
  var Prelude = PS["Prelude"];

  var Index = function Index(errorAt, hasOwnProperty, hasProperty, index) {
    this.errorAt = errorAt;
    this.hasOwnProperty = hasOwnProperty;
    this.hasProperty = hasProperty;
    this.index = index;
  };

  var unsafeReadProp = function unsafeReadProp(k) {
    return function (value) {
      return $foreign.unsafeReadPropImpl(Data_Foreign.fail(new Data_Foreign.TypeMismatch("object", Data_Foreign.typeOf(value))), Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity)), k, value);
    };
  };

  var readProp = unsafeReadProp;

  var index = function index(dict) {
    return dict.index;
  };

  var hasPropertyImpl = function hasPropertyImpl(v) {
    return function (value) {
      if (Data_Foreign.isNull(value)) {
        return false;
      }

      ;

      if (Data_Foreign.isUndefined(value)) {
        return false;
      }

      ;

      if (Data_Foreign.typeOf(value) === "object" || Data_Foreign.typeOf(value) === "function") {
        return $foreign.unsafeHasProperty(v, value);
      }

      ;
      return false;
    };
  };

  var hasProperty = function hasProperty(dict) {
    return dict.hasProperty;
  };

  var hasOwnPropertyImpl = function hasOwnPropertyImpl(v) {
    return function (value) {
      if (Data_Foreign.isNull(value)) {
        return false;
      }

      ;

      if (Data_Foreign.isUndefined(value)) {
        return false;
      }

      ;

      if (Data_Foreign.typeOf(value) === "object" || Data_Foreign.typeOf(value) === "function") {
        return $foreign.unsafeHasOwnProperty(v, value);
      }

      ;
      return false;
    };
  };

  var indexString = new Index(Data_Foreign.ErrorAtProperty.create, hasOwnPropertyImpl, hasPropertyImpl, Data_Function.flip(readProp));

  var hasOwnProperty = function hasOwnProperty(dict) {
    return dict.hasOwnProperty;
  };

  var errorAt = function errorAt(dict) {
    return dict.errorAt;
  };

  exports["Index"] = Index;
  exports["readProp"] = readProp;
  exports["index"] = index;
  exports["hasProperty"] = hasProperty;
  exports["hasOwnProperty"] = hasOwnProperty;
  exports["errorAt"] = errorAt;
  exports["indexString"] = indexString;
})(PS["Data.Foreign.Index"] = PS["Data.Foreign.Index"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Data_Maybe = PS["Data.Maybe"];

  var Inl = function () {
    function Inl(value0) {
      this.value0 = value0;
    }

    ;

    Inl.create = function (value0) {
      return new Inl(value0);
    };

    return Inl;
  }();

  var Inr = function () {
    function Inr(value0) {
      this.value0 = value0;
    }

    ;

    Inr.create = function (value0) {
      return new Inr(value0);
    };

    return Inr;
  }();

  var Product = function () {
    function Product(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    Product.create = function (value0) {
      return function (value1) {
        return new Product(value0, value1);
      };
    };

    return Product;
  }();

  var NoArguments = function () {
    function NoArguments() {}

    ;
    NoArguments.value = new NoArguments();
    return NoArguments;
  }();

  var Field = function Field(x) {
    return x;
  };

  var Constructor = function Constructor(x) {
    return x;
  };

  var Generic = function Generic(from, to) {
    this.from = from;
    this.to = to;
  };

  var to = function to(dict) {
    return dict.to;
  };

  var from = function from(dict) {
    return dict.from;
  };

  exports["Generic"] = Generic;
  exports["to"] = to;
  exports["from"] = from;
  exports["NoArguments"] = NoArguments;
  exports["Inl"] = Inl;
  exports["Inr"] = Inr;
  exports["Product"] = Product;
  exports["Constructor"] = Constructor;
  exports["Field"] = Field;
})(PS["Data.Generic.Rep"] = PS["Data.Generic.Rep"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Data_Semigroup = PS["Data.Semigroup"];
  var Prelude = PS["Prelude"];
  var Unsafe_Coerce = PS["Unsafe.Coerce"];

  var SProxy = function () {
    function SProxy() {}

    ;
    SProxy.value = new SProxy();
    return SProxy;
  }();

  var IsSymbol = function IsSymbol(reflectSymbol) {
    this.reflectSymbol = reflectSymbol;
  };

  var reflectSymbol = function reflectSymbol(dict) {
    return dict.reflectSymbol;
  };

  exports["IsSymbol"] = IsSymbol;
  exports["reflectSymbol"] = reflectSymbol;
  exports["SProxy"] = SProxy;
})(PS["Data.Symbol"] = PS["Data.Symbol"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad = PS["Control.Monad"];
  var Data_BooleanAlgebra = PS["Data.BooleanAlgebra"];
  var Data_Bounded = PS["Data.Bounded"];
  var Data_CommutativeRing = PS["Data.CommutativeRing"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Functor = PS["Data.Functor"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Ord = PS["Data.Ord"];
  var Data_Ordering = PS["Data.Ordering"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Show = PS["Data.Show"];
  var Prelude = PS["Prelude"];

  var $$Proxy = function () {
    function $$Proxy() {}

    ;
    $$Proxy.value = new $$Proxy();
    return $$Proxy;
  }();

  exports["Proxy"] = $$Proxy;
})(PS["Type.Proxy"] = PS["Type.Proxy"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Alt = PS["Control.Alt"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Except = PS["Control.Monad.Except"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Bifunctor = PS["Data.Bifunctor"];
  var Data_Either = PS["Data.Either"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Foreign_Class = PS["Data.Foreign.Class"];
  var Data_Foreign_Generic_Types = PS["Data.Foreign.Generic.Types"];
  var Data_Foreign_Index = PS["Data.Foreign.Index"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Generic_Rep = PS["Data.Generic.Rep"];
  var Data_Identity = PS["Data.Identity"];
  var Data_List = PS["Data.List"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Monoid = PS["Data.Monoid"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Show = PS["Data.Show"];
  var Data_StrMap = PS["Data.StrMap"];
  var Data_Symbol = PS["Data.Symbol"];
  var Data_Unfoldable = PS["Data.Unfoldable"];
  var Prelude = PS["Prelude"];
  var Type_Proxy = PS["Type.Proxy"];

  var GenericDecode = function GenericDecode(decodeOpts) {
    this.decodeOpts = decodeOpts;
  };

  var GenericDecodeArgs = function GenericDecodeArgs(decodeArgs) {
    this.decodeArgs = decodeArgs;
  };

  var GenericDecodeFields = function GenericDecodeFields(decodeFields) {
    this.decodeFields = decodeFields;
  };

  var GenericCountArgs = function GenericCountArgs(countArgs) {
    this.countArgs = countArgs;
  };

  var genericDecodeFieldsField = function genericDecodeFieldsField(dictIsSymbol) {
    return function (dictDecode) {
      return new GenericDecodeFields(function (opts) {
        return function (x) {
          var name = opts.fieldTransform(Data_Symbol.reflectSymbol(dictIsSymbol)(Data_Symbol.SProxy.value));
          return Data_Functor.map(Control_Monad_Except_Trans.functorExceptT(Data_Identity.functorIdentity))(Data_Generic_Rep.Field)(Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Data_Foreign_Index.index(Data_Foreign_Index.indexString)(x)(name))(function ($158) {
            return Control_Monad_Except.mapExcept(Data_Bifunctor.lmap(Data_Either.bifunctorEither)(Data_Functor.map(Data_List_Types.functorNonEmptyList)(Data_Foreign.ErrorAtProperty.create(name))))(Data_Foreign_Class.decode(dictDecode)($158));
          }));
        };
      });
    };
  };

  var genericDecodeArgsNoArguments = new GenericDecodeArgs(function (v) {
    return function (v1) {
      return function (v2) {
        if (v2 instanceof Data_List_Types.Nil) {
          return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))({
            result: Data_Generic_Rep.NoArguments.value,
            rest: Data_List_Types.Nil.value,
            next: v1
          });
        }

        ;
        return Data_Foreign.fail(new Data_Foreign.ForeignError("Too many constructor arguments"));
      };
    };
  });

  var genericDecodeArgsArgument = function genericDecodeArgsArgument(dictDecode) {
    return new GenericDecodeArgs(function (v) {
      return function (v1) {
        return function (v2) {
          if (v2 instanceof Data_List_Types.Cons) {
            return Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Control_Monad_Except.mapExcept(Data_Bifunctor.lmap(Data_Either.bifunctorEither)(Data_Functor.map(Data_List_Types.functorNonEmptyList)(Data_Foreign.ErrorAtIndex.create(v1))))(Data_Foreign_Class.decode(dictDecode)(v2.value0)))(function (v3) {
              return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))({
                result: v3,
                rest: v2.value1,
                next: v1 + 1 | 0
              });
            });
          }

          ;
          return Data_Foreign.fail(new Data_Foreign.ForeignError("Not enough constructor arguments"));
        };
      };
    });
  };

  var genericCountArgsRec = new GenericCountArgs(function (v) {
    return new Data_Either.Right(1);
  });
  var genericCountArgsNoArguments = new GenericCountArgs(function (v) {
    return new Data_Either.Left(Data_Generic_Rep.NoArguments.value);
  });
  var genericCountArgsArgument = new GenericCountArgs(function (v) {
    return new Data_Either.Right(1);
  });

  var decodeOpts = function decodeOpts(dict) {
    return dict.decodeOpts;
  };

  var genericDecodeSum = function genericDecodeSum(dictGenericDecode) {
    return function (dictGenericDecode1) {
      return new GenericDecode(function (opts) {
        return function (f) {
          var opts$prime = function () {
            var $111 = {};

            for (var $112 in opts) {
              if ({}.hasOwnProperty.call(opts, $112)) {
                $111[$112] = opts[$112];
              }

              ;
            }

            ;
            $111.unwrapSingleConstructors = false;
            return $111;
          }();

          return Control_Alt.alt(Control_Monad_Except_Trans.altExceptT(Data_List_Types.semigroupNonEmptyList)(Data_Identity.monadIdentity))(Data_Functor.map(Control_Monad_Except_Trans.functorExceptT(Data_Identity.functorIdentity))(Data_Generic_Rep.Inl.create)(decodeOpts(dictGenericDecode)(opts$prime)(f)))(Data_Functor.map(Control_Monad_Except_Trans.functorExceptT(Data_Identity.functorIdentity))(Data_Generic_Rep.Inr.create)(decodeOpts(dictGenericDecode1)(opts$prime)(f)));
        };
      });
    };
  };

  var decodeFields = function decodeFields(dict) {
    return dict.decodeFields;
  };

  var genericDecodeArgsRec = function genericDecodeArgsRec(dictGenericDecodeFields) {
    return new GenericDecodeArgs(function (v) {
      return function (v1) {
        return function (v2) {
          if (v2 instanceof Data_List_Types.Cons) {
            return Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Control_Monad_Except.mapExcept(Data_Bifunctor.lmap(Data_Either.bifunctorEither)(Data_Functor.map(Data_List_Types.functorNonEmptyList)(Data_Foreign.ErrorAtIndex.create(v1))))(decodeFields(dictGenericDecodeFields)(v)(v2.value0)))(function (v3) {
              return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))({
                result: v3,
                rest: v2.value1,
                next: v1 + 1 | 0
              });
            });
          }

          ;
          return Data_Foreign.fail(new Data_Foreign.ForeignError("Not enough constructor arguments"));
        };
      };
    });
  };

  var genericDecodeFieldsProduct = function genericDecodeFieldsProduct(dictGenericDecodeFields) {
    return function (dictGenericDecodeFields1) {
      return new GenericDecodeFields(function (opts) {
        return function (x) {
          return Control_Apply.apply(Control_Monad_Except_Trans.applyExceptT(Data_Identity.monadIdentity))(Data_Functor.map(Control_Monad_Except_Trans.functorExceptT(Data_Identity.functorIdentity))(Data_Generic_Rep.Product.create)(decodeFields(dictGenericDecodeFields)(opts)(x)))(decodeFields(dictGenericDecodeFields1)(opts)(x));
        };
      });
    };
  };

  var decodeArgs = function decodeArgs(dict) {
    return dict.decodeArgs;
  };

  var genericDecodeArgsProduct = function genericDecodeArgsProduct(dictGenericDecodeArgs) {
    return function (dictGenericDecodeArgs1) {
      return new GenericDecodeArgs(function (opts) {
        return function (i) {
          return function (xs) {
            return Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(decodeArgs(dictGenericDecodeArgs)(opts)(i)(xs))(function (v) {
              return Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(decodeArgs(dictGenericDecodeArgs1)(opts)(v.next)(v.rest))(function (v1) {
                return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))({
                  result: new Data_Generic_Rep.Product(v.result, v1.result),
                  rest: v1.rest,
                  next: v1.next
                });
              });
            });
          };
        };
      });
    };
  };

  var countArgs = function countArgs(dict) {
    return dict.countArgs;
  };

  var genericCountArgsProduct = function genericCountArgsProduct(dictGenericCountArgs) {
    return function (dictGenericCountArgs1) {
      return new GenericCountArgs(function (v) {
        var v1 = countArgs(dictGenericCountArgs1)(Type_Proxy["Proxy"].value);
        var v2 = countArgs(dictGenericCountArgs)(Type_Proxy["Proxy"].value);

        if (v2 instanceof Data_Either.Left && v1 instanceof Data_Either.Left) {
          return new Data_Either.Left(new Data_Generic_Rep.Product(v2.value0, v1.value0));
        }

        ;

        if (v2 instanceof Data_Either.Left && v1 instanceof Data_Either.Right) {
          return new Data_Either.Right(v1.value0);
        }

        ;

        if (v2 instanceof Data_Either.Right && v1 instanceof Data_Either.Left) {
          return new Data_Either.Right(v2.value0);
        }

        ;

        if (v2 instanceof Data_Either.Right && v1 instanceof Data_Either.Right) {
          return new Data_Either.Right(v2.value0 + v1.value0 | 0);
        }

        ;
        throw new Error("Failed pattern match at Data.Foreign.Generic.Class line 205, column 5 - line 209, column 40: " + [v2.constructor.name, v1.constructor.name]);
      });
    };
  };

  var genericDecodeConstructor = function genericDecodeConstructor(dictIsSymbol) {
    return function (dictGenericDecodeArgs) {
      return function (dictGenericCountArgs) {
        return new GenericDecode(function (opts) {
          return function (f) {
            var numArgs = countArgs(dictGenericCountArgs)(Type_Proxy["Proxy"].value);

            var readArguments = function readArguments(args) {
              if (numArgs instanceof Data_Either.Left) {
                return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))(numArgs.value0);
              }

              ;

              if (numArgs instanceof Data_Either.Right && numArgs.value0 === 1 && opts.unwrapSingleArguments) {
                return Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(decodeArgs(dictGenericDecodeArgs)(opts)(0)(Data_List.singleton(args)))(function (v) {
                  return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Control_Applicative.unless(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))(Data_List["null"](v.rest))(Data_Foreign.fail(new Data_Foreign.ForeignError("Expected a single argument"))))(function () {
                    return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))(v.result);
                  });
                });
              }

              ;

              if (numArgs instanceof Data_Either.Right) {
                return Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Data_Foreign.readArray(args))(function (v) {
                  return Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(decodeArgs(dictGenericDecodeArgs)(opts)(0)(Data_List.fromFoldable(Data_Foldable.foldableArray)(v)))(function (v1) {
                    return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Control_Applicative.unless(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))(Data_List["null"](v1.rest))(Data_Foreign.fail(new Data_Foreign.ForeignError("Expected " + (Data_Show.show(Data_Show.showInt)(numArgs.value0) + " constructor arguments")))))(function () {
                      return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))(v1.result);
                    });
                  });
                });
              }

              ;
              throw new Error("Failed pattern match at Data.Foreign.Generic.Class line 74, column 9 - line 86, column 24: " + [numArgs.constructor.name]);
            };

            var ctorName = Data_Symbol.reflectSymbol(dictIsSymbol)(Data_Symbol.SProxy.value);

            if (opts.unwrapSingleConstructors) {
              return Data_Functor.map(Control_Monad_Except_Trans.functorExceptT(Data_Identity.functorIdentity))(Data_Generic_Rep.Constructor)(readArguments(f));
            }

            ;
            return Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Control_Monad_Except.mapExcept(Data_Bifunctor.lmap(Data_Either.bifunctorEither)(Data_Functor.map(Data_List_Types.functorNonEmptyList)(Data_Foreign.ErrorAtProperty.create(opts.sumEncoding.value0.contentsFieldName))))(Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Data_Foreign_Index.index(Data_Foreign_Index.indexString)(f)(opts.sumEncoding.value0.tagFieldName))(Data_Foreign.readString))(function (v) {
              var expected = opts.sumEncoding.value0.constructorTagTransform(ctorName);
              return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Control_Applicative.unless(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))(opts.sumEncoding.value0.constructorTagTransform(v) === expected)(Data_Foreign.fail(new Data_Foreign.ForeignError("Expected " + (Data_Show.show(Data_Show.showString)(expected) + " tag")))))(function () {
                return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))(v);
              });
            })))(function (v) {
              return Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Control_Monad_Except.mapExcept(Data_Bifunctor.lmap(Data_Either.bifunctorEither)(Data_Functor.map(Data_List_Types.functorNonEmptyList)(Data_Foreign.ErrorAtProperty.create(opts.sumEncoding.value0.contentsFieldName))))(Control_Bind.bind(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(Data_Foreign_Index.index(Data_Foreign_Index.indexString)(f)(opts.sumEncoding.value0.contentsFieldName))(readArguments)))(function (v1) {
                return Control_Applicative.pure(Control_Monad_Except_Trans.applicativeExceptT(Data_Identity.monadIdentity))(v1);
              });
            });
          };
        });
      };
    };
  };

  exports["countArgs"] = countArgs;
  exports["decodeArgs"] = decodeArgs;
  exports["decodeFields"] = decodeFields;
  exports["decodeOpts"] = decodeOpts;
  exports["GenericDecode"] = GenericDecode;
  exports["GenericDecodeArgs"] = GenericDecodeArgs;
  exports["GenericDecodeFields"] = GenericDecodeFields;
  exports["GenericCountArgs"] = GenericCountArgs;
  exports["genericDecodeConstructor"] = genericDecodeConstructor;
  exports["genericDecodeSum"] = genericDecodeSum;
  exports["genericDecodeArgsNoArguments"] = genericDecodeArgsNoArguments;
  exports["genericDecodeArgsArgument"] = genericDecodeArgsArgument;
  exports["genericDecodeArgsProduct"] = genericDecodeArgsProduct;
  exports["genericDecodeArgsRec"] = genericDecodeArgsRec;
  exports["genericDecodeFieldsField"] = genericDecodeFieldsField;
  exports["genericDecodeFieldsProduct"] = genericDecodeFieldsProduct;
  exports["genericCountArgsNoArguments"] = genericCountArgsNoArguments;
  exports["genericCountArgsArgument"] = genericCountArgsArgument;
  exports["genericCountArgsProduct"] = genericCountArgsProduct;
  exports["genericCountArgsRec"] = genericCountArgsRec;
})(PS["Data.Foreign.Generic.Class"] = PS["Data.Foreign.Generic.Class"] || {});

(function (exports) {
  "use strict";

  exports.parseJSONImpl = function (str) {
    return JSON.parse(str);
  };
})(PS["Data.Foreign.JSON"] = PS["Data.Foreign.JSON"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Data.Foreign.JSON"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Eff_Uncurried = PS["Control.Monad.Eff.Uncurried"];
  var Control_Monad_Except = PS["Control.Monad.Except"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Bifunctor = PS["Data.Bifunctor"];
  var Data_Either = PS["Data.Either"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Identity = PS["Data.Identity"];
  var Data_List_Types = PS["Data.List.Types"];
  var Prelude = PS["Prelude"];

  var parseJSON = function parseJSON($0) {
    return Control_Monad_Except_Trans.ExceptT(Data_Identity.Identity(Data_Bifunctor.lmap(Data_Either.bifunctorEither)(function ($1) {
      return Control_Applicative.pure(Data_List_Types.applicativeNonEmptyList)(Data_Foreign.JSONError.create(Control_Monad_Eff_Exception.message($1)));
    })(Control_Monad_Eff.runPure(Control_Monad_Eff_Exception["try"](Control_Monad_Eff_Uncurried.runEffFn1($foreign.parseJSONImpl)($0))))));
  };

  var decodeJSONWith = function decodeJSONWith(f) {
    return Control_Bind.composeKleisliFlipped(Control_Monad_Except_Trans.bindExceptT(Data_Identity.monadIdentity))(f)(parseJSON);
  };

  exports["parseJSON"] = parseJSON;
  exports["decodeJSONWith"] = decodeJSONWith;
})(PS["Data.Foreign.JSON"] = PS["Data.Foreign.JSON"] || {});

(function (exports) {
  /* globals exports, JSON */
  "use strict"; // module Global.Unsafe

  exports.unsafeStringify = function (x) {
    return JSON.stringify(x);
  };
})(PS["Global.Unsafe"] = PS["Global.Unsafe"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Global.Unsafe"];
  exports["unsafeStringify"] = $foreign.unsafeStringify;
})(PS["Global.Unsafe"] = PS["Global.Unsafe"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Foreign_Class = PS["Data.Foreign.Class"];
  var Data_Foreign_Generic_Class = PS["Data.Foreign.Generic.Class"];
  var Data_Foreign_Generic_Types = PS["Data.Foreign.Generic.Types"];
  var Data_Foreign_JSON = PS["Data.Foreign.JSON"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Generic_Rep = PS["Data.Generic.Rep"];
  var Data_Identity = PS["Data.Identity"];
  var Global_Unsafe = PS["Global.Unsafe"];
  var Prelude = PS["Prelude"];

  var genericDecode = function genericDecode(dictGeneric) {
    return function (dictGenericDecode) {
      return function (opts) {
        return function ($12) {
          return Data_Functor.map(Control_Monad_Except_Trans.functorExceptT(Data_Identity.functorIdentity))(Data_Generic_Rep.to(dictGeneric))(Data_Foreign_Generic_Class.decodeOpts(dictGenericDecode)(opts)($12));
        };
      };
    };
  };

  var defaultOptions = {
    sumEncoding: new Data_Foreign_Generic_Types.TaggedObject({
      tagFieldName: "tag",
      contentsFieldName: "contents",
      constructorTagTransform: Control_Category.id(Control_Category.categoryFn)
    }),
    unwrapSingleConstructors: false,
    unwrapSingleArguments: true,
    fieldTransform: Control_Category.id(Control_Category.categoryFn)
  };

  var decodeJSON = function decodeJSON(dictDecode) {
    return Data_Foreign_JSON.decodeJSONWith(Data_Foreign_Class.decode(dictDecode));
  };

  exports["defaultOptions"] = defaultOptions;
  exports["genericDecode"] = genericDecode;
  exports["decodeJSON"] = decodeJSON;
})(PS["Data.Foreign.Generic"] = PS["Data.Foreign.Generic"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Unsafe_Coerce = PS["Unsafe.Coerce"];
  var runExists = Unsafe_Coerce.unsafeCoerce;
  var mkExists = Unsafe_Coerce.unsafeCoerce;
  exports["mkExists"] = mkExists;
  exports["runExists"] = runExists;
})(PS["Data.Exists"] = PS["Data.Exists"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Category = PS["Control.Category"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Foreign_Generic = PS["Data.Foreign.Generic"];
  var Data_Foreign_Generic_Class = PS["Data.Foreign.Generic.Class"];
  var Data_Foreign_Generic_EnumEncoding = PS["Data.Foreign.Generic.EnumEncoding"];
  var Data_Foreign_Generic_Types = PS["Data.Foreign.Generic.Types"];
  var Data_Generic_Rep = PS["Data.Generic.Rep"];
  var Prelude = PS["Prelude"];

  var options = function () {
    var $13 = {};

    for (var $14 in Data_Foreign_Generic.defaultOptions) {
      if ({}.hasOwnProperty.call(Data_Foreign_Generic.defaultOptions, $14)) {
        $13[$14] = Data_Foreign_Generic["defaultOptions"][$14];
      }

      ;
    }

    ;
    $13.unwrapSingleConstructors = true;
    return $13;
  }();

  var defaultDecode = function defaultDecode(dictGeneric) {
    return function (dictGenericDecode) {
      return function (x) {
        return Data_Foreign_Generic.genericDecode(dictGeneric)(dictGenericDecode)(options)(x);
      };
    };
  };

  exports["defaultDecode"] = defaultDecode;
})(PS["Presto.Core.Utils.Encoding"] = PS["Presto.Core.Utils.Encoding"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Data_Foreign = PS["Data.Foreign"];
  var Data_Foreign_Class = PS["Data.Foreign.Class"];
  var Data_Foreign_Generic_Class = PS["Data.Foreign.Generic.Class"];
  var Data_Generic_Rep = PS["Data.Generic.Rep"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Show = PS["Data.Show"];
  var Data_Symbol = PS["Data.Symbol"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Utils_Encoding = PS["Presto.Core.Utils.Encoding"];

  var POST = function () {
    function POST() {}

    ;
    POST.value = new POST();
    return POST;
  }();

  var GET = function () {
    function GET() {}

    ;
    GET.value = new GET();
    return GET;
  }();

  var PUT = function () {
    function PUT() {}

    ;
    PUT.value = new PUT();
    return PUT;
  }();

  var DELETE = function () {
    function DELETE() {}

    ;
    DELETE.value = new DELETE();
    return DELETE;
  }();

  var Header = function () {
    function Header(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    Header.create = function (value0) {
      return function (value1) {
        return new Header(value0, value1);
      };
    };

    return Header;
  }();

  var showMethod = new Data_Show.Show(function (v) {
    if (v instanceof POST) {
      return "POST";
    }

    ;

    if (v instanceof GET) {
      return "GET";
    }

    ;

    if (v instanceof PUT) {
      return "PUT";
    }

    ;

    if (v instanceof DELETE) {
      return "DELETE";
    }

    ;
    throw new Error("Failed pattern match at Presto.Core.Types.API line 98, column 1 - line 98, column 35: " + [v.constructor.name]);
  });
  var genericRequest = new Data_Generic_Rep.Generic(function (x) {
    return new Data_Generic_Rep.Product(x.headers, new Data_Generic_Rep.Product(x.method, new Data_Generic_Rep.Product(x.payload, x.url)));
  }, function (x) {
    return {
      headers: x.value0,
      method: x.value1.value0,
      payload: x.value1.value1.value0,
      url: x.value1.value1.value1
    };
  });
  var genericMethod = new Data_Generic_Rep.Generic(function (x) {
    if (x instanceof POST) {
      return new Data_Generic_Rep.Inl(Data_Generic_Rep.NoArguments.value);
    }

    ;

    if (x instanceof GET) {
      return new Data_Generic_Rep.Inr(new Data_Generic_Rep.Inl(Data_Generic_Rep.NoArguments.value));
    }

    ;

    if (x instanceof PUT) {
      return new Data_Generic_Rep.Inr(new Data_Generic_Rep.Inr(new Data_Generic_Rep.Inl(Data_Generic_Rep.NoArguments.value)));
    }

    ;

    if (x instanceof DELETE) {
      return new Data_Generic_Rep.Inr(new Data_Generic_Rep.Inr(new Data_Generic_Rep.Inr(Data_Generic_Rep.NoArguments.value)));
    }

    ;
    throw new Error("Failed pattern match at Presto.Core.Types.API line 93, column 8 - line 93, column 50: " + [x.constructor.name]);
  }, function (x) {
    if (x instanceof Data_Generic_Rep.Inl) {
      return POST.value;
    }

    ;

    if (x instanceof Data_Generic_Rep.Inr && x.value0 instanceof Data_Generic_Rep.Inl) {
      return GET.value;
    }

    ;

    if (x instanceof Data_Generic_Rep.Inr && x.value0 instanceof Data_Generic_Rep.Inr && x.value0.value0 instanceof Data_Generic_Rep.Inl) {
      return PUT.value;
    }

    ;

    if (x instanceof Data_Generic_Rep.Inr && x.value0 instanceof Data_Generic_Rep.Inr && x.value0.value0 instanceof Data_Generic_Rep.Inr) {
      return DELETE.value;
    }

    ;
    throw new Error("Failed pattern match at Presto.Core.Types.API line 93, column 8 - line 93, column 50: " + [x.constructor.name]);
  });
  var genericHeaders = new Data_Generic_Rep.Generic(function (x) {
    return x;
  }, function (x) {
    return x;
  });
  var genericHeader = new Data_Generic_Rep.Generic(function (x) {
    return new Data_Generic_Rep.Product(x.value0, x.value1);
  }, function (x) {
    return new Header(x.value0, x.value1);
  });
  var decodeMethod = new Data_Foreign_Class.Decode(Presto_Core_Utils_Encoding.defaultDecode(genericMethod)(Data_Foreign_Generic_Class.genericDecodeSum(Data_Foreign_Generic_Class.genericDecodeConstructor(new Data_Symbol.IsSymbol(function () {
    return "POST";
  }))(Data_Foreign_Generic_Class.genericDecodeArgsNoArguments)(Data_Foreign_Generic_Class.genericCountArgsNoArguments))(Data_Foreign_Generic_Class.genericDecodeSum(Data_Foreign_Generic_Class.genericDecodeConstructor(new Data_Symbol.IsSymbol(function () {
    return "GET";
  }))(Data_Foreign_Generic_Class.genericDecodeArgsNoArguments)(Data_Foreign_Generic_Class.genericCountArgsNoArguments))(Data_Foreign_Generic_Class.genericDecodeSum(Data_Foreign_Generic_Class.genericDecodeConstructor(new Data_Symbol.IsSymbol(function () {
    return "PUT";
  }))(Data_Foreign_Generic_Class.genericDecodeArgsNoArguments)(Data_Foreign_Generic_Class.genericCountArgsNoArguments))(Data_Foreign_Generic_Class.genericDecodeConstructor(new Data_Symbol.IsSymbol(function () {
    return "DELETE";
  }))(Data_Foreign_Generic_Class.genericDecodeArgsNoArguments)(Data_Foreign_Generic_Class.genericCountArgsNoArguments))))));
  var decodeHeaderG = new Data_Foreign_Class.Decode(Presto_Core_Utils_Encoding.defaultDecode(genericHeader)(Data_Foreign_Generic_Class.genericDecodeConstructor(new Data_Symbol.IsSymbol(function () {
    return "Header";
  }))(Data_Foreign_Generic_Class.genericDecodeArgsProduct(Data_Foreign_Generic_Class.genericDecodeArgsArgument(Data_Foreign_Class.stringDecode))(Data_Foreign_Generic_Class.genericDecodeArgsArgument(Data_Foreign_Class.stringDecode)))(Data_Foreign_Generic_Class.genericCountArgsProduct(Data_Foreign_Generic_Class.genericCountArgsArgument)(Data_Foreign_Generic_Class.genericCountArgsArgument))));
  var decodeHeadersG = new Data_Foreign_Class.Decode(Presto_Core_Utils_Encoding.defaultDecode(genericHeaders)(Data_Foreign_Generic_Class.genericDecodeConstructor(new Data_Symbol.IsSymbol(function () {
    return "Headers";
  }))(Data_Foreign_Generic_Class.genericDecodeArgsArgument(Data_Foreign_Class.arrayDecode(decodeHeaderG)))(Data_Foreign_Generic_Class.genericCountArgsArgument)));
  var decodeRequestG = new Data_Foreign_Class.Decode(Presto_Core_Utils_Encoding.defaultDecode(genericRequest)(Data_Foreign_Generic_Class.genericDecodeConstructor(new Data_Symbol.IsSymbol(function () {
    return "Request";
  }))(Data_Foreign_Generic_Class.genericDecodeArgsRec(Data_Foreign_Generic_Class.genericDecodeFieldsProduct(Data_Foreign_Generic_Class.genericDecodeFieldsField(new Data_Symbol.IsSymbol(function () {
    return "headers";
  }))(decodeHeadersG))(Data_Foreign_Generic_Class.genericDecodeFieldsProduct(Data_Foreign_Generic_Class.genericDecodeFieldsField(new Data_Symbol.IsSymbol(function () {
    return "method";
  }))(decodeMethod))(Data_Foreign_Generic_Class.genericDecodeFieldsProduct(Data_Foreign_Generic_Class.genericDecodeFieldsField(new Data_Symbol.IsSymbol(function () {
    return "payload";
  }))(Data_Foreign_Class.stringDecode))(Data_Foreign_Generic_Class.genericDecodeFieldsField(new Data_Symbol.IsSymbol(function () {
    return "url";
  }))(Data_Foreign_Class.stringDecode))))))(Data_Foreign_Generic_Class.genericCountArgsRec)));
  exports["POST"] = POST;
  exports["GET"] = GET;
  exports["PUT"] = PUT;
  exports["DELETE"] = DELETE;
  exports["Header"] = Header;
  exports["genericMethod"] = genericMethod;
  exports["decodeMethod"] = decodeMethod;
  exports["showMethod"] = showMethod;
  exports["genericHeader"] = genericHeader;
  exports["decodeHeaderG"] = decodeHeaderG;
  exports["genericHeaders"] = genericHeaders;
  exports["decodeHeadersG"] = decodeHeadersG;
  exports["genericRequest"] = genericRequest;
  exports["decodeRequestG"] = decodeRequestG;
})(PS["Presto.Core.Types.API"] = PS["Presto.Core.Types.API"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Except = PS["Control.Monad.Except"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Data_Either = PS["Data.Either"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Foreign_Class = PS["Data.Foreign.Class"];
  var Data_Function = PS["Data.Function"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_Show = PS["Data.Show"];
  var Prelude = PS["Prelude"];

  var ForeignOut = function ForeignOut(x) {
    return x;
  };

  exports["ForeignOut"] = ForeignOut;
})(PS["Presto.Core.Types.Language.Interaction"] = PS["Presto.Core.Types.Language.Interaction"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Data_Eq = PS["Data.Eq"];
  var Data_Foreign_Class = PS["Data.Foreign.Class"];
  var Data_Foreign_Generic = PS["Data.Foreign.Generic"];
  var Data_Foreign_Generic_Class = PS["Data.Foreign.Generic.Class"];
  var Data_Generic_Rep = PS["Data.Generic.Rep"];
  var Data_Generic_Rep_Show = PS["Data.Generic.Rep.Show"];
  var Data_Show = PS["Data.Show"];
  var Data_Symbol = PS["Data.Symbol"];
  var Data_Tuple = PS["Data.Tuple"];
  var Prelude = PS["Prelude"];

  var PermissionGranted = function () {
    function PermissionGranted() {}

    ;
    PermissionGranted.value = new PermissionGranted();
    return PermissionGranted;
  }();

  var PermissionDeclined = function () {
    function PermissionDeclined() {}

    ;
    PermissionDeclined.value = new PermissionDeclined();
    return PermissionDeclined;
  }();

  var PermissionReadPhoneState = function () {
    function PermissionReadPhoneState() {}

    ;
    PermissionReadPhoneState.value = new PermissionReadPhoneState();
    return PermissionReadPhoneState;
  }();

  var PermissionSendSms = function () {
    function PermissionSendSms() {}

    ;
    PermissionSendSms.value = new PermissionSendSms();
    return PermissionSendSms;
  }();

  var PermissionReadStorage = function () {
    function PermissionReadStorage() {}

    ;
    PermissionReadStorage.value = new PermissionReadStorage();
    return PermissionReadStorage;
  }();

  var PermissionWriteStorage = function () {
    function PermissionWriteStorage() {}

    ;
    PermissionWriteStorage.value = new PermissionWriteStorage();
    return PermissionWriteStorage;
  }();

  var PermissionCamera = function () {
    function PermissionCamera() {}

    ;
    PermissionCamera.value = new PermissionCamera();
    return PermissionCamera;
  }();

  var PermissionLocation = function () {
    function PermissionLocation() {}

    ;
    PermissionLocation.value = new PermissionLocation();
    return PermissionLocation;
  }();

  var PermissionCoarseLocation = function () {
    function PermissionCoarseLocation() {}

    ;
    PermissionCoarseLocation.value = new PermissionCoarseLocation();
    return PermissionCoarseLocation;
  }();

  var PermissionContacts = function () {
    function PermissionContacts() {}

    ;
    PermissionContacts.value = new PermissionContacts();
    return PermissionContacts;
  }();

  exports["PermissionReadPhoneState"] = PermissionReadPhoneState;
  exports["PermissionSendSms"] = PermissionSendSms;
  exports["PermissionReadStorage"] = PermissionReadStorage;
  exports["PermissionWriteStorage"] = PermissionWriteStorage;
  exports["PermissionCamera"] = PermissionCamera;
  exports["PermissionLocation"] = PermissionLocation;
  exports["PermissionCoarseLocation"] = PermissionCoarseLocation;
  exports["PermissionContacts"] = PermissionContacts;
  exports["PermissionGranted"] = PermissionGranted;
  exports["PermissionDeclined"] = PermissionDeclined;
})(PS["Presto.Core.Types.Permission"] = PS["Presto.Core.Types.Permission"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Category = PS["Control.Category"];
  var Control_Monad_Aff = PS["Control.Monad.Aff"];
  var Control_Monad_Aff_AVar = PS["Control.Monad.Aff.AVar"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Exists = PS["Data.Exists"];
  var Data_Foreign_Class = PS["Data.Foreign.Class"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Show = PS["Data.Show"];
  var Data_Time_Duration = PS["Data.Time.Duration"];
  var Data_Unit = PS["Data.Unit"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Types_API = PS["Presto.Core.Types.API"];
  var Presto_Core_Types_App = PS["Presto.Core.Types.App"];
  var Presto_Core_Types_Language_APIInteract = PS["Presto.Core.Types.Language.APIInteract"];
  var Presto_Core_Types_Language_Interaction = PS["Presto.Core.Types.Language.Interaction"];
  var Presto_Core_Types_Language_Storage = PS["Presto.Core.Types.Language.Storage"];
  var Presto_Core_Types_Permission = PS["Presto.Core.Types.Permission"];
  var PrestoDOM_Core = PS["PrestoDOM.Core"];
  var PrestoDOM_Types_Core = PS["PrestoDOM.Types.Core"];

  var LocalStore = function () {
    function LocalStore() {}

    ;
    LocalStore.value = new LocalStore();
    return LocalStore;
  }();

  var InMemoryStore = function () {
    function InMemoryStore() {}

    ;
    InMemoryStore.value = new InMemoryStore();
    return InMemoryStore;
  }();

  var ThrowError = function () {
    function ThrowError(value0) {
      this.value0 = value0;
    }

    ;

    ThrowError.create = function (value0) {
      return new ThrowError(value0);
    };

    return ThrowError;
  }();

  var ReturnResult = function () {
    function ReturnResult(value0) {
      this.value0 = value0;
    }

    ;

    ReturnResult.create = function (value0) {
      return new ReturnResult(value0);
    };

    return ReturnResult;
  }();

  var RunUI = function () {
    function RunUI(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    RunUI.create = function (value0) {
      return function (value1) {
        return new RunUI(value0, value1);
      };
    };

    return RunUI;
  }();

  var ForkUI = function () {
    function ForkUI(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    ForkUI.create = function (value0) {
      return function (value1) {
        return new ForkUI(value0, value1);
      };
    };

    return ForkUI;
  }();

  var CallAPI = function () {
    function CallAPI(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    CallAPI.create = function (value0) {
      return function (value1) {
        return new CallAPI(value0, value1);
      };
    };

    return CallAPI;
  }();

  var Get = function () {
    function Get(value0, value1, value2) {
      this.value0 = value0;
      this.value1 = value1;
      this.value2 = value2;
    }

    ;

    Get.create = function (value0) {
      return function (value1) {
        return function (value2) {
          return new Get(value0, value1, value2);
        };
      };
    };

    return Get;
  }();

  var $$Set = function () {
    function $$Set(value0, value1, value2, value3) {
      this.value0 = value0;
      this.value1 = value1;
      this.value2 = value2;
      this.value3 = value3;
    }

    ;

    $$Set.create = function (value0) {
      return function (value1) {
        return function (value2) {
          return function (value3) {
            return new $$Set(value0, value1, value2, value3);
          };
        };
      };
    };

    return $$Set;
  }();

  var Fork = function () {
    function Fork(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    Fork.create = function (value0) {
      return function (value1) {
        return new Fork(value0, value1);
      };
    };

    return Fork;
  }();

  var DoAff = function () {
    function DoAff(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    DoAff.create = function (value0) {
      return function (value1) {
        return new DoAff(value0, value1);
      };
    };

    return DoAff;
  }();

  var Await = function () {
    function Await(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    Await.create = function (value0) {
      return function (value1) {
        return new Await(value0, value1);
      };
    };

    return Await;
  }();

  var Delay = function () {
    function Delay(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    Delay.create = function (value0) {
      return function (value1) {
        return new Delay(value0, value1);
      };
    };

    return Delay;
  }();

  var OneOf = function () {
    function OneOf(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    OneOf.create = function (value0) {
      return function (value1) {
        return new OneOf(value0, value1);
      };
    };

    return OneOf;
  }();

  var HandleError = function () {
    function HandleError(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    HandleError.create = function (value0) {
      return function (value1) {
        return new HandleError(value0, value1);
      };
    };

    return HandleError;
  }();

  var CheckPermissions = function () {
    function CheckPermissions(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    CheckPermissions.create = function (value0) {
      return function (value1) {
        return new CheckPermissions(value0, value1);
      };
    };

    return CheckPermissions;
  }();

  var TakePermissions = function () {
    function TakePermissions(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    TakePermissions.create = function (value0) {
      return function (value1) {
        return new TakePermissions(value0, value1);
      };
    };

    return TakePermissions;
  }();

  var InitUIWithScreen = function () {
    function InitUIWithScreen(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    InitUIWithScreen.create = function (value0) {
      return function (value1) {
        return new InitUIWithScreen(value0, value1);
      };
    };

    return InitUIWithScreen;
  }();

  var InitUI = function () {
    function InitUI(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    InitUI.create = function (value0) {
      return function (value1) {
        return new InitUI(value0, value1);
      };
    };

    return InitUI;
  }();

  var RunScreen = function () {
    function RunScreen(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    RunScreen.create = function (value0) {
      return function (value1) {
        return new RunScreen(value0, value1);
      };
    };

    return RunScreen;
  }();

  var ShowScreen = function () {
    function ShowScreen(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    ShowScreen.create = function (value0) {
      return function (value1) {
        return new ShowScreen(value0, value1);
      };
    };

    return ShowScreen;
  }();

  var FlowWrapper = function FlowWrapper(x) {
    return x;
  };

  var wrap = function wrap($17) {
    return Control_Monad_Free.liftF(FlowWrapper(Data_Exists.mkExists($17)));
  };

  var oneOf = function oneOf(flows) {
    return wrap(new OneOf(flows, Control_Category.id(Control_Category.categoryFn)));
  };

  var launch = function launch(flow) {
    return wrap(new Fork(flow, Control_Category.id(Control_Category.categoryFn)));
  };

  var doAff = function doAff(aff) {
    return wrap(new DoAff(aff, Control_Category.id(Control_Category.categoryFn)));
  };

  var delay = function delay(dictDuration) {
    return function (duration) {
      return wrap(new Delay(Data_Time_Duration.fromDuration(dictDuration)(duration), Data_Unit.unit));
    };
  };

  exports["LocalStore"] = LocalStore;
  exports["InMemoryStore"] = InMemoryStore;
  exports["ThrowError"] = ThrowError;
  exports["ReturnResult"] = ReturnResult;
  exports["RunUI"] = RunUI;
  exports["ForkUI"] = ForkUI;
  exports["CallAPI"] = CallAPI;
  exports["Get"] = Get;
  exports["Set"] = $$Set;
  exports["Fork"] = Fork;
  exports["DoAff"] = DoAff;
  exports["Await"] = Await;
  exports["Delay"] = Delay;
  exports["OneOf"] = OneOf;
  exports["HandleError"] = HandleError;
  exports["CheckPermissions"] = CheckPermissions;
  exports["TakePermissions"] = TakePermissions;
  exports["InitUIWithScreen"] = InitUIWithScreen;
  exports["InitUI"] = InitUI;
  exports["RunScreen"] = RunScreen;
  exports["ShowScreen"] = ShowScreen;
  exports["FlowWrapper"] = FlowWrapper;
  exports["wrap"] = wrap;
  exports["launch"] = launch;
  exports["doAff"] = doAff;
  exports["delay"] = delay;
  exports["oneOf"] = oneOf;
})(PS["Presto.Core.Types.Language.Flow"] = PS["Presto.Core.Types.Language.Flow"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Engineering.OS.Permission"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Aff = PS["Control.Monad.Aff"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Error_Class = PS["Control.Monad.Error.Class"];
  var Control_Monad_Except = PS["Control.Monad.Except"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Control_Monad_Loops = PS["Control.Monad.Loops"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Array = PS["Data.Array"];
  var Data_Either = PS["Data.Either"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Foreign_Class = PS["Data.Foreign.Class"];
  var Data_Foreign_Generic = PS["Data.Foreign.Generic"];
  var Data_Function = PS["Data.Function"];
  var Data_Function_Uncurried = PS["Data.Function.Uncurried"];
  var Data_Functor = PS["Data.Functor"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_List = PS["Data.List"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Show = PS["Data.Show"];
  var Data_String = PS["Data.String"];
  var Data_Tuple = PS["Data.Tuple"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Types_App = PS["Presto.Core.Types.App"];
  var Presto_Core_Types_Language_Flow = PS["Presto.Core.Types.Language.Flow"];
  var Presto_Core_Types_Permission = PS["Presto.Core.Types.Permission"];

  var toAndroidPermission = function toAndroidPermission(v) {
    if (v instanceof Presto_Core_Types_Permission.PermissionSendSms) {
      return "android.permission.SEND_SMS";
    }

    ;

    if (v instanceof Presto_Core_Types_Permission.PermissionReadPhoneState) {
      return "android.permission.READ_PHONE_STATE";
    }

    ;

    if (v instanceof Presto_Core_Types_Permission.PermissionWriteStorage) {
      return "android.permission.WRITE_EXTERNAL_STORAGE";
    }

    ;

    if (v instanceof Presto_Core_Types_Permission.PermissionReadStorage) {
      return "android.permission.READ_EXTERNAL_STORAGE";
    }

    ;

    if (v instanceof Presto_Core_Types_Permission.PermissionCamera) {
      return "android.permission.CAMERA";
    }

    ;

    if (v instanceof Presto_Core_Types_Permission.PermissionLocation) {
      return "android.permission.ACCESS_FINE_LOCATION";
    }

    ;

    if (v instanceof Presto_Core_Types_Permission.PermissionCoarseLocation) {
      return "android.permission.ACCESS_COARSE_LOCATION";
    }

    ;

    if (v instanceof Presto_Core_Types_Permission.PermissionContacts) {
      return "android.permission.READ_CONTACTS";
    }

    ;
    throw new Error("Failed pattern match at Engineering.OS.Permission line 29, column 1 - line 29, column 44: " + [v.constructor.name]);
  };

  var requestPermissions = function requestPermissions(permissions) {
    var toResponse = function toResponse(wasGranted) {
      if (wasGranted) {
        return Presto_Core_Types_Permission.PermissionGranted.value;
      }

      ;
      return Presto_Core_Types_Permission.PermissionDeclined.value;
    };

    var jPermission = Data_Functor.map(Data_Functor.functorArray)(toAndroidPermission)(permissions);
    return Control_Bind.bind(Control_Monad_Aff.bindAff)(Control_Monad_Aff.makeAff(function (cb) {
      return Control_Apply.applySecond(Control_Monad_Eff.applyEff)($foreign["requestPermission'"](function ($24) {
        return cb(Data_Either.Left.create($24));
      }, function ($25) {
        return cb(Data_Either.Right.create($25));
      }, Data_Show.show(Data_Show.showArray(Data_Show.showString))(jPermission)))(Control_Applicative.pure(Control_Monad_Eff.applicativeEff)(Control_Monad_Aff.nonCanceler));
    }))(function (v) {
      var v1 = Control_Monad_Except.runExcept(Data_Foreign_Generic.decodeJSON(Data_Foreign_Class.arrayDecode(Data_Foreign_Class.booleanDecode))(v));

      if (v1 instanceof Data_Either.Right) {
        return Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(Data_Array.zip(permissions)(Data_Functor.map(Data_Functor.functorArray)(toResponse)(v1.value0)));
      }

      ;

      if (v1 instanceof Data_Either.Left) {
        return Control_Monad_Error_Class.throwError(Control_Monad_Aff.monadThrowAff)(Control_Monad_Eff_Exception.error(Data_Show.show(Data_List_Types.showNonEmptyList(Data_Foreign.showForeignError))(v1.value0)));
      }

      ;
      throw new Error("Failed pattern match at Engineering.OS.Permission line 78, column 3 - line 80, column 46: " + [v1.constructor.name]);
    });
  };

  var getPermissionStatus = function getPermissionStatus(permission) {
    return function __do() {
      var v = $foreign["getPermissionStatus'"](Data_Array.singleton(toAndroidPermission(permission)))();
      return Data_String.contains("true")(v);
    };
  };

  var checkIfPermissionsGranted = function checkIfPermissionsGranted(permissions) {
    return Control_Bind.bind(Control_Monad_Aff.bindAff)(Control_Monad_Aff["liftEff'"](Control_Monad_Loops.allM(Control_Monad_Eff.monadEff)(getPermissionStatus)(Data_List.fromFoldable(Data_Foldable.foldableArray)(permissions))))(function (v) {
      return Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(function () {
        if (v) {
          return Presto_Core_Types_Permission.PermissionGranted.value;
        }

        ;
        return Presto_Core_Types_Permission.PermissionDeclined.value;
      }());
    });
  };

  exports["toAndroidPermission"] = toAndroidPermission;
  exports["getPermissionStatus"] = getPermissionStatus;
  exports["checkIfPermissionsGranted"] = checkIfPermissionsGranted;
  exports["requestPermissions"] = requestPermissions;
})(PS["Engineering.OS.Permission"] = PS["Engineering.OS.Permission"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Monad_Aff_AVar = PS["Control.Monad.Aff.AVar"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Console = PS["Control.Monad.Eff.Console"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Eff_Ref = PS["Control.Monad.Eff.Ref"];
  var Control_Monad_Eff_Timer = PS["Control.Monad.Eff.Timer"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var DOM = PS["DOM"];
  var Data_Either = PS["Data.Either"];
  var Data_Functor = PS["Data.Functor"];
  var FRP = PS["FRP"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Types_App = PS["Presto.Core.Types.App"];
  var Presto_Core_Types_Language_Flow = PS["Presto.Core.Types.Language.Flow"];
  var hyperOSVersion = "1.0rc1_14";
  var appId = "hyperos";
  exports["hyperOSVersion"] = hyperOSVersion;
  exports["appId"] = appId;
})(PS["Engineering.Types.App"] = PS["Engineering.Types.App"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Aff = PS["Control.Monad.Aff"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Error_Class = PS["Control.Monad.Error.Class"];
  var Control_Monad_Except = PS["Control.Monad.Except"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Data_Either = PS["Data.Either"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Foreign_Class = PS["Data.Foreign.Class"];
  var Data_Function = PS["Data.Function"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_NaturalTransformation = PS["Data.NaturalTransformation"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Show = PS["Data.Show"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Types_API = PS["Presto.Core.Types.API"];
  var Presto_Core_Types_Language_Interaction = PS["Presto.Core.Types.Language.Interaction"];

  var interpretAPI = function interpretAPI(apiRunner) {
    return function (v) {
      var v1 = Control_Monad_Except.runExcept(Data_Foreign_Class.decode(Presto_Core_Types_API.decodeRequestG)(v.value0));

      if (v1 instanceof Data_Either.Left) {
        return Control_Monad_Error_Class.throwError(Control_Monad_Aff.monadThrowAff)(Control_Monad_Eff_Exception.error("apiInteract is broken: " + Data_Show.show(Data_List_Types.showNonEmptyList(Data_Foreign.showForeignError))(v1.value0)));
      }

      ;

      if (v1 instanceof Data_Either.Right) {
        return Control_Bind.bind(Control_Monad_Aff.bindAff)(apiRunner(v1.value0))(function (v2) {
          return Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(v.value1(Presto_Core_Types_Language_Interaction.ForeignOut(Data_Foreign_Class.encode(Data_Foreign_Class.stringEncode)(v2))));
        });
      }

      ;
      throw new Error("Failed pattern match at Presto.Core.Language.Runtime.API line 22, column 3 - line 27, column 45: " + [v1.constructor.name]);
    };
  };

  var runAPIInteraction = function runAPIInteraction(apiRunner) {
    return Control_Monad_Free.foldFree(Control_Monad_Aff.monadRecAff)(interpretAPI(apiRunner));
  };

  exports["runAPIInteraction"] = runAPIInteraction;
})(PS["Presto.Core.Language.Runtime.API"] = PS["Presto.Core.Language.Runtime.API"] || {});

(function (exports) {
  exports["getValueFromLocalStore'"] = function (key) {
    return function () {
      return JBridge.getFromSharedPrefs(key);
    };
  };

  exports["setValueToLocalStore'"] = function (key) {
    return function (value) {
      return function () {
        JBridge.setInSharedPrefs(key, value);
      };
    };
  };
})(PS["Presto.Core.LocalStorage"] = PS["Presto.Core.LocalStorage"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Presto.Core.LocalStorage"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Aff = PS["Control.Monad.Aff"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Class = PS["Control.Monad.Eff.Class"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Maybe = PS["Data.Maybe"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Types_App = PS["Presto.Core.Types.App"];

  var setValueToLocalStore = function setValueToLocalStore(k) {
    return function (v) {
      return Control_Monad_Eff_Class.liftEff(Control_Monad_Aff.monadEffAff)($foreign["setValueToLocalStore'"](k)(v));
    };
  };

  var getValueFromLocalStore = function getValueFromLocalStore(k) {
    var v = Control_Monad_Eff_Class.liftEff(Control_Monad_Aff.monadEffAff)($foreign["getValueFromLocalStore'"](k));
    return Control_Bind.ifM(Control_Monad_Aff.bindAff)(Data_Functor.map(Control_Monad_Aff.functorAff)(Data_Eq.eq(Data_Eq.eqString)("__failed"))(v))(Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(Data_Maybe.Nothing.value))(Data_Functor.map(Control_Monad_Aff.functorAff)(Data_Maybe.Just.create)(v));
  };

  exports["getValueFromLocalStore"] = getValueFromLocalStore;
  exports["setValueToLocalStore"] = setValueToLocalStore;
})(PS["Presto.Core.LocalStorage"] = PS["Presto.Core.LocalStorage"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Aff = PS["Control.Monad.Aff"];
  var Control_Monad_Aff_AVar = PS["Control.Monad.Aff.AVar"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Error_Class = PS["Control.Monad.Error.Class"];
  var Control_Monad_Except = PS["Control.Monad.Except"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Control_Monad_State_Class = PS["Control.Monad.State.Class"];
  var Control_Monad_State_Trans = PS["Control.Monad.State.Trans"];
  var Control_Monad_Trans_Class = PS["Control.Monad.Trans.Class"];
  var Control_Parallel = PS["Control.Parallel"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Exists = PS["Data.Exists"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Foreign = PS["Data.Foreign"];
  var Data_Foreign_JSON = PS["Data.Foreign.JSON"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_List_Types = PS["Data.List.Types"];
  var Data_NaturalTransformation = PS["Data.NaturalTransformation"];
  var Data_Show = PS["Data.Show"];
  var Data_StrMap = PS["Data.StrMap"];
  var Data_Tuple = PS["Data.Tuple"];
  var Global_Unsafe = PS["Global.Unsafe"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Language_Runtime_API = PS["Presto.Core.Language.Runtime.API"];
  var Presto_Core_LocalStorage = PS["Presto.Core.LocalStorage"];
  var Presto_Core_Types_App = PS["Presto.Core.Types.App"];
  var Presto_Core_Types_Language_Flow = PS["Presto.Core.Types.Language.Flow"];
  var Presto_Core_Types_Language_Interaction = PS["Presto.Core.Types.Language.Interaction"];
  var Presto_Core_Types_Language_Storage = PS["Presto.Core.Types.Language.Storage"];
  var Presto_Core_Types_Permission = PS["Presto.Core.Types.Permission"];

  var PermissionRunner = function () {
    function PermissionRunner(value0, value1) {
      this.value0 = value0;
      this.value1 = value1;
    }

    ;

    PermissionRunner.create = function (value0) {
      return function (value1) {
        return new PermissionRunner(value0, value1);
      };
    };

    return PermissionRunner;
  }();

  var Runtime = function () {
    function Runtime(value0, value1, value2) {
      this.value0 = value0;
      this.value1 = value1;
      this.value2 = value2;
    }

    ;

    Runtime.create = function (value0) {
      return function (value1) {
        return function (value2) {
          return new Runtime(value0, value1, value2);
        };
      };
    };

    return Runtime;
  }();

  var updateState = function updateState(key) {
    return function (value) {
      return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_State_Class.get(Control_Monad_State_Trans.monadStateStateT(Control_Monad_Aff.monadAff)))(function (v) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Monad_Aff_AVar.takeVar(v)))(function (v1) {
          var st$prime = Data_StrMap.insert(key)(value)(v1);
          return Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Monad_Aff_AVar.putVar(st$prime)(v));
        });
      });
    };
  };

  var runErrorHandler = function runErrorHandler(v) {
    if (v instanceof Presto_Core_Types_Language_Flow.ThrowError) {
      return Control_Monad_Error_Class.throwError(Control_Monad_State_Trans.monadThrowStateT(Control_Monad_Aff.monadThrowAff))(Control_Monad_Eff_Exception.error(v.value0));
    }

    ;

    if (v instanceof Presto_Core_Types_Language_Flow.ReturnResult) {
      return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v.value0);
    }

    ;
    throw new Error("Failed pattern match at Presto.Core.Language.Runtime.Interpreter line 81, column 1 - line 81, column 71: " + [v.constructor.name]);
  };

  var readState = Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_State_Class.get(Control_Monad_State_Trans.monadStateStateT(Control_Monad_Aff.monadAff)))(function ($99) {
    return Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Monad_Aff_AVar.readVar($99));
  });

  var interpretUI = function interpretUI(uiRunner) {
    return function (v) {
      return Control_Bind.bind(Control_Monad_Aff.bindAff)(uiRunner(Global_Unsafe.unsafeStringify(v.value0)))(function (v1) {
        var v2 = Control_Monad_Except.runExcept(Data_Foreign_JSON.parseJSON(v1));

        if (v2 instanceof Data_Either.Right) {
          return Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(v.value1(v2.value0));
        }

        ;

        if (v2 instanceof Data_Either.Left) {
          return Control_Monad_Error_Class.throwError(Control_Monad_Aff.monadThrowAff)(Control_Monad_Eff_Exception.error(Data_Show.show(Data_List_Types.showNonEmptyList(Data_Foreign.showForeignError))(v2.value0)));
        }

        ;
        throw new Error("Failed pattern match at Presto.Core.Language.Runtime.Interpreter line 65, column 3 - line 67, column 46: " + [v2.constructor.name]);
      });
    };
  };

  var runUIInteraction = function runUIInteraction(uiRunner) {
    return Control_Monad_Free.foldFree(Control_Monad_Aff.monadRecAff)(interpretUI(uiRunner));
  };

  var run = function run(runtime) {
    return Control_Monad_Free.foldFree(Control_Monad_State_Trans.monadRecStateT(Control_Monad_Aff.monadRecAff))(function (v) {
      return Data_Exists.runExists(interpret(runtime))(v);
    });
  };

  var interpret = function interpret(v) {
    return function (v1) {
      if (v1 instanceof Presto_Core_Types_Language_Flow.CallAPI) {
        return Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Bind.bind(Control_Monad_Aff.bindAff)(Presto_Core_Language_Runtime_API.runAPIInteraction(v.value2)(v1.value0))(function ($100) {
          return Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(v1.value1($100));
        }));
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.RunUI) {
        return Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Bind.bind(Control_Monad_Aff.bindAff)(runUIInteraction(v.value0)(v1.value0))(function ($101) {
          return Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(v1.value1($101));
        }));
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.ForkUI) {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Data_Functor["void"](Control_Monad_State_Trans.functorStateT(Control_Monad_Aff.functorAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Monad_Aff.forkAff(runUIInteraction(v.value0)(v1.value0)))))(function () {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1);
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.Get && v1.value0 instanceof Presto_Core_Types_Language_Flow.LocalStore) {
        return Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Bind.bind(Control_Monad_Aff.bindAff)(Presto_Core_LocalStorage.getValueFromLocalStore(v1.value1))(function ($102) {
          return Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(v1.value2($102));
        }));
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.Get && v1.value0 instanceof Presto_Core_Types_Language_Flow.InMemoryStore) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(readState)(function ($103) {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value2(Data_StrMap.lookup(v1.value1)($103)));
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow["Set"] && v1.value0 instanceof Presto_Core_Types_Language_Flow.LocalStore) {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Presto_Core_LocalStorage.setValueToLocalStore(v1.value1)(v1.value2)))(function () {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value3);
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow["Set"] && v1.value0 instanceof Presto_Core_Types_Language_Flow.InMemoryStore) {
        return Control_Apply.applySecond(Control_Monad_State_Trans.applyStateT(Control_Monad_Aff.monadAff))(updateState(v1.value1)(v1.value2))(Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value3));
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.Fork) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(forkFlow(v)(v1.value0))(function ($104) {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1($104));
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.DoAff) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(v1.value0))(function ($105) {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1($105));
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.InitUIWithScreen) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(v1.value0))(function ($106) {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1($106));
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.InitUI) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(v1.value0))(function ($107) {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1($107));
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.RunScreen) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(v1.value0))(function ($108) {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1($108));
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.ShowScreen) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(v1.value0))(function ($109) {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1($109));
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.Await) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Monad_Aff_AVar.readVar(v1.value0)))(function ($110) {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1($110));
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.Delay) {
        return Control_Apply.applySecond(Control_Monad_State_Trans.applyStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Monad_Aff.delay(v1.value0)))(Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1));
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.OneOf) {
        var parFlow = function parFlow(st) {
          return function (flow) {
            return Control_Monad_State_Trans.runStateT(run(v)(flow))(st);
          };
        };

        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_State_Class.get(Control_Monad_State_Trans.monadStateStateT(Control_Monad_Aff.monadAff)))(function (v2) {
          return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Parallel.parOneOf(Control_Monad_Aff.parallelAff)(Control_Monad_Aff.alternativeParAff)(Data_Foldable.foldableArray)(Data_Functor.functorArray)(Data_Functor.map(Data_Functor.functorArray)(parFlow(v2))(v1.value0))))(function (v3) {
            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_State_Class.put(Control_Monad_State_Trans.monadStateStateT(Control_Monad_Aff.monadAff))(v3.value1))(function () {
              return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1(v3.value0));
            });
          });
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.HandleError) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(run(v)(v1.value0))(runErrorHandler))(function ($111) {
          return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1.value1($111));
        });
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.CheckPermissions) {
        return Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Bind.bind(Control_Monad_Aff.bindAff)(v.value1.value0(v1.value0))(function ($112) {
          return Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(v1.value1($112));
        }));
      }

      ;

      if (v1 instanceof Presto_Core_Types_Language_Flow.TakePermissions) {
        return Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Bind.bind(Control_Monad_Aff.bindAff)(v.value1.value1(v1.value0))(function ($113) {
          return Control_Applicative.pure(Control_Monad_Aff.applicativeAff)(v1.value1($113));
        }));
      }

      ;
      throw new Error("Failed pattern match at Presto.Core.Language.Runtime.Interpreter line 85, column 1 - line 85, column 95: " + [v.constructor.name, v1.constructor.name]);
    };
  };

  var forkFlow = function forkFlow(rt) {
    return function (flow) {
      return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_State_Class.get(Control_Monad_State_Trans.monadStateStateT(Control_Monad_Aff.monadAff)))(function (v) {
        return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Monad_Aff_AVar.makeEmptyVar))(function (v1) {
          var m = Control_Monad_State_Trans.evalStateT(Control_Monad_Aff.functorAff)(run(rt)(flow))(v);
          return Control_Bind.bind(Control_Monad_State_Trans.bindStateT(Control_Monad_Aff.monadAff))(Control_Monad_Trans_Class.lift(Control_Monad_State_Trans.monadTransStateT)(Control_Monad_Aff.monadAff)(Control_Monad_Aff.forkAff(Control_Bind.bind(Control_Monad_Aff.bindAff)(m)(Data_Function.flip(Control_Monad_Aff_AVar.putVar)(v1)))))(function (v2) {
            return Control_Applicative.pure(Control_Monad_State_Trans.applicativeStateT(Control_Monad_Aff.monadAff))(v1);
          });
        });
      });
    };
  };

  exports["Runtime"] = Runtime;
  exports["PermissionRunner"] = PermissionRunner;
  exports["run"] = run;
})(PS["Presto.Core.Language.Runtime.Interpreter"] = PS["Presto.Core.Language.Runtime.Interpreter"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Engineering.Helpers.Commons"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Aff = PS["Control.Monad.Aff"];
  var Control_Monad_Aff_AVar = PS["Control.Monad.Aff.AVar"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Class = PS["Control.Monad.Eff.Class"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Control_Monad_State_Trans = PS["Control.Monad.State.Trans"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Function = PS["Data.Function"];
  var Data_Function_Uncurried = PS["Data.Function.Uncurried"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Show = PS["Data.Show"];
  var Data_StrMap = PS["Data.StrMap"];
  var Engineering_OS_Permission = PS["Engineering.OS.Permission"];
  var Engineering_Types_App = PS["Engineering.Types.App"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Flow = PS["Presto.Core.Flow"];
  var Presto_Core_Language_Runtime_Interpreter = PS["Presto.Core.Language.Runtime.Interpreter"];
  var Presto_Core_Types_API = PS["Presto.Core.Types.API"];
  var Presto_Core_Types_App = PS["Presto.Core.Types.App"];
  var Presto_Core_Types_Language_Flow = PS["Presto.Core.Types.Language.Flow"];
  var Presto_Core_Types_Language_Interaction = PS["Presto.Core.Types.Language.Interaction"];

  var toFlow = function toFlow(a) {
    return Presto_Core_Types_Language_Flow.doAff(Control_Monad_Eff_Class.liftEff(Control_Monad_Aff.monadEffAff)(a));
  };

  var mkNativeHeader = function mkNativeHeader(v) {
    return {
      field: v.value0,
      value: v.value1
    };
  };

  var mkNativeRequest = function mkNativeRequest(v) {
    return {
      method: Data_Show.show(Presto_Core_Types_API.showMethod)(v.method),
      url: v.url,
      payload: v.payload,
      headers: Data_Functor.map(Data_Functor.functorArray)(mkNativeHeader)(v.headers)
    };
  };

  var getValueFromWindow = function getValueFromWindow(key) {
    return function (defaultValue) {
      return toFlow($foreign["getValueFromWindow'"](key)(defaultValue));
    };
  };

  var flowRunner = function flowRunner(f) {
    var uiRunner = function uiRunner(a) {
      return Control_Monad_Aff.makeAff(function (cb) {
        return function __do() {
          var v = $foreign["showUI'"](function ($10) {
            return cb(Data_Either.Right.create($10));
          }, "")();
          return Control_Monad_Aff.nonCanceler;
        };
      });
    };

    var permissionRunner = new Presto_Core_Language_Runtime_Interpreter.PermissionRunner(Engineering_OS_Permission.checkIfPermissionsGranted, Engineering_OS_Permission.requestPermissions);

    var apiRunner = function apiRunner(request) {
      return Control_Monad_Aff.makeAff(function (cb) {
        return function __do() {
          var v = $foreign["callAPI'"](function ($11) {
            return cb(Data_Either.Left.create($11));
          })(function ($12) {
            return cb(Data_Either.Right.create($12));
          })(mkNativeRequest(request))();
          return Control_Monad_Aff.nonCanceler;
        };
      });
    };

    var runtime = new Presto_Core_Language_Runtime_Interpreter.Runtime(uiRunner, permissionRunner, apiRunner);
    var freeFlow = Control_Monad_State_Trans.evalStateT(Control_Monad_Aff.functorAff)(Presto_Core_Language_Runtime_Interpreter.run(runtime)(f));
    return Control_Monad_Aff.launchAff_(Control_Bind.bind(Control_Monad_Aff.bindAff)(Control_Monad_Aff_AVar.makeVar(Data_StrMap.empty))(freeFlow));
  };

  exports["toFlow"] = toFlow;
  exports["mkNativeRequest"] = mkNativeRequest;
  exports["mkNativeHeader"] = mkNativeHeader;
  exports["flowRunner"] = flowRunner;
  exports["getValueFromWindow"] = getValueFromWindow;
  exports["showUI'"] = $foreign["showUI'"];
  exports["callAPI'"] = $foreign["callAPI'"];
  exports["toJSON"] = $foreign.toJSON;
})(PS["Engineering.Helpers.Commons"] = PS["Engineering.Helpers.Commons"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Engineering_Helpers_Commons = PS["Engineering.Helpers.Commons"];
  var Engineering_Types_App = PS["Engineering.Types.App"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Flow = PS["Presto.Core.Flow"];

  var apply0 = function apply0(fn) {
    return Engineering_Helpers_Commons.flowRunner(fn);
  };

  var apply1 = function apply1(fn) {
    return function (a) {
      return apply0(fn(a));
    };
  };

  var apply2 = function apply2(fn) {
    return function (a) {
      return function (b) {
        return apply1(fn(a))(b);
      };
    };
  };

  var apply3 = function apply3(fn) {
    return function (a) {
      return function (b) {
        return function (c) {
          return apply2(fn(a))(b)(c);
        };
      };
    };
  };

  var apply4 = function apply4(fn) {
    return function (a) {
      return function (b) {
        return function (c) {
          return function (d) {
            return apply3(fn(a))(b)(c)(d);
          };
        };
      };
    };
  };

  exports["apply0"] = apply0;
  exports["apply1"] = apply1;
  exports["apply2"] = apply2;
  exports["apply3"] = apply3;
  exports["apply4"] = apply4;
})(PS["Engineering.Helpers.Apply"] = PS["Engineering.Helpers.Apply"] || {});

(function (exports) {
  window.mappStack = [];
  window.ipcChannels = {};
  window.eventListeners = {};
  window.JOS = {};
  window.ipcServer = "IPC_server";
  window.emptyIPCClientResponse = {
    "result": "NOT_FOUND",
    "channelId": "null",
    "ops": function ops() {}
  };

  exports["readFile'"] = function (filePath) {
    return function () {
      return _jbridge.loadFileInDUI(filePath);
    };
  };

  exports["eval'"] = function (string) {
    return function () {
      try {
        eval(string);
      } catch (err) {
        console.error(err);
      }

      return;
    };
  };

  exports["loadS'"] = function (key) {
    return function () {
      return _jbridge.getFromSharedPrefs(key);
    };
  };

  exports["logAny"] = function (data) {
    console.log(data);
  };

  exports["fetchResource'"] = function (key) {
    return function () {
      try {
        return _jbridge.getResourceByName(key);
      } catch (err) {
        console.error(err);
        return "__failed";
      }
    };
  };

  exports["bootComplete'"] = function () {
    try {
      var json = JSON.parse(_jbridge.getSessionAttribute("bundleParams", "{}"));

      if (typeof json.service == "undefined") {
        json.service = "null";
      }

      if (typeof json.pre_fetch == "undefined") {
        json.pre_fetch = "false";
      }

      return json;
    } catch (err) {
      console.error(err);
      return {
        error: "true",
        msg: err,
        service: "null"
      };
    }
  };

  exports["mergeJBridge'"] = function () {
    try {
      if (Object.keys(_jbridge).length > 0) {
        JBridge = {};

        for (var i in _jbridge) {
          if (i != "setSessionId") {
            JBridge[i] = window._jbridge[i].bind(window._jbridge);
          } else {
            JBridge["setSessionId"] = function (id) {
              window._jbridge["setSessionId"](id);

              console.log("Session Id is " + id);
              window.latestSessionId = id;
            };
          }
        }
      }

      if (typeof MerchantJBridge != "undefined" && Object.keys(MerchantJBridge).length > 0) {
        window._merchantJbridge = MerchantJBridge;

        for (var i in _merchantJbridge) {
          JBridge[i] = window._merchantJbridge[i].bind(window._merchantJbridge);
        }
      }

      JBridge.getSessionId = function () {
        return window.latestSessionId || "null";
      };
    } catch (err) {
      console.error(err);
    }
  };

  exports["shutDown'"] = function (resultCode) {
    return function (payload) {
      return function () {
        _jbridge.exitApp(resultCode, payload);
      };
    };
  };

  exports["canOpenApp'"] = function (caller) {
    return function (callee) {
      return function () {
        return true;
      };
    };
  };

  exports["readIfExist'"] = function (filePath) {
    return function () {
      try {
        if (filePath == "") {
          return "__failed";
        }

        console.log(filePath);

        var file = _jbridge.loadFileInDUI(filePath);

        if (typeof file == "undefined" || file == "") {
          return "__failed";
        } else {
          return file;
        }
      } catch (err) {
        console.error(err);
        return "__failed";
      }
    };
  };

  exports["createIframe'"] = function (id) {
    return function (src) {
      return function () {
        var iframe = document.createElement("iframe");
        iframe.id = id;
        iframe.srcdoc = src;

        if (typeof window.mapps == "undefined") {
          window.mapps = {};
        }

        window.mapps[id] = iframe;
        return;
      };
    };
  };

  exports["injectIframe'"] = function (id) {
    return function (payload) {
      return function (os) {
        return function () {
          if (typeof window.mapps == "undefined" || typeof window.mapps[id] == "undefined") {
            return "__failed";
          }

          if (typeof window != "undefined") {
            payload.global_session_id = typeof window.hyper_session_id == "string" ? window.hyper_session_id : "na";
            payload.parent_session_id = typeof window.latestSessionId == "undefined" ? window.hyper_session_id : window.latestSessionId;
          }

          var iframe = window.mapps[id];
          var os_srt = "<html><script>window.__payload = " + JSON.stringify(payload) + "; </script>";
          iframe.srcdoc = iframe.srcdoc.replace("<html>", os_srt);
          document.body.appendChild(iframe);
          return "__success";
        };
      };
    };
  };

  exports["findPath'"] = function (callee) {
    return function () {
      var config = window.getConfig();
      var apps = config.apps || {};

      if (typeof apps[callee] != "undefined") {
        return apps[callee].root + apps[callee].entry || "";
      }

      return "";
    };
  };

  exports["pushToStack'"] = function (item) {
    return function () {
      window.mappStack.push(item);
    };
  };

  exports["popFromStack'"] = function () {
    if (window.mappStack && window.mappStack.length > 0) window.mappStack.pop();
  };

  exports["peekStack'"] = function () {
    if (window.mappStack.length > 0) {
      return window.mappStack[window.mappStack.length - 1];
    }

    return {
      caller: "__failed",
      callee: "__failed"
    };
  };

  exports["registerOS'"] = function (app) {
    return function (os) {
      return function () {
        if (window.mapps[app]) {
          var iframe = window.mapps[app];
          window.JOS = os;
          iframe.contentWindow.JOS = os;
          iframe.contentWindow.Android = Android;
          iframe.contentWindow.JBridge = JBridge;
        }
      };
    };
  };

  exports["removeIframe'"] = function (app) {
    return function () {
      if (typeof window.mapps[app] != "undefined") {
        document.body.removeChild(window.mapps[app]);
      }
    };
  };

  exports["removeView'"] = function (app) {
    return function () {
      if (typeof window.mapps[app] != "undefined") {
        var iframe = window.mapps[app];
        var rootView;

        if (iframe.contentWindow.N) {
          rootView = iframe.contentWindow.N.children[0].__ref.__id;

          if (!isNaN(parseInt(rootView))) {
            Android.runInUI("set_VIEW=ctx->findViewById:i_" + rootView + ";get_VIEW->removeAllViews;", null);
            Android.runInUI("set_VIEW=ctx->findViewById:i_" + rootView + ";set_PARENT=get_VIEW->getParent;get_PARENT->removeView:get_VIEW;", null);
          }

          rootView = iframe.contentWindow.N.__ref.__id;
        } else {
          rootView = (iframe.contentWindow.__ROOTSCREEN || {
            idSet: {
              root: "null"
            }
          }).idSet.root;

          if (!isNaN(parseInt(rootView))) {
            Android.runInUI("set_VIEW=ctx->findViewById:i_" + rootView + ";get_VIEW->removeAllViews;", null);
            Android.runInUI("set_VIEW=ctx->findViewById:i_" + rootView + ";set_PARENT=get_VIEW->getParent;get_PARENT->removeView:get_VIEW;", null);
          }

          rootView = (iframe.contentWindow.__ROOTSCREEN || {
            layout: {
              idSet: {
                id: "null"
              }
            }
          }).layout.idSet.id;
        }

        if (rootView != "null") {
          Android.runInUI("set_VIEW=ctx->findViewById:i_" + rootView + ";get_VIEW->removeAllViews;", null);
          Android.runInUI("set_VIEW=ctx->findViewById:i_" + rootView + ";set_PARENT=get_VIEW->getParent;get_PARENT->removeView:get_VIEW;", null);
          console.log("View Removed");
        } else {
          console.error("Error: ROOTSCREEN can't be found");
        }
      }
    };
  };

  exports["unregister'"] = function (app) {
    return function () {
      if (typeof window.mapps[app.callee] != "undefined") {
        delete window.mapps[app.callee];
      }
    };
  };

  exports["download'"] = function (location) {
    return function (path) {
      return function (success) {
        return function () {
          var file = path + location.substr(location.lastIndexOf("/") + 1);
          window.__BOOT_LOADER = window.__BOOT_LOADER || {};

          window.__BOOT_LOADER["file_" + file] = function (isDownloaded) {
            success(isDownloaded == "true")();
          };

          _jbridge.renewFile(location, file, "file_" + file);
        };
      };
    };
  };

  exports["downloadManifest'"] = function (location) {
    return function (success) {
      return function () {
        window.__BOOT_LOADER = window.__BOOT_LOADER || {};

        window.__BOOT_LOADER['onManifestDownloaded'] = function (isDownloaded) {
          console.log("is manifest new: ", isDownloaded);

          try {
            var manifest = JSON.parse(_jbridge.loadFileInDUI("manifest.json"));
            success({
              "code": -1,
              "result": manifest
            })();
          } catch (err) {
            success({
              "code": 0,
              "result": err
            })();
          }
        };

        _jbridge.renewFile(location, "manifest.json", "onManifestDownloaded");
      };
    };
  };

  exports["shouldDownload"] = function (manifest) {
    return function (assetMeta) {
      return function (url) {
        return function (path) {
          var latestHash = "";

          if (url && typeof url == "string") {
            var location = "";

            try {
              location = new URL(url).pathname;
            } catch (err) {
              console.error(err);
              return false;
            }

            var keys = location.split('/');
            var curr = manifest;

            for (var i in keys) {
              if (keys[i] == "") continue;

              if (curr && curr[keys[i]]) {
                curr = curr[keys[i]];
              } else {
                break;
              }
            }

            if (curr && curr.hash) {
              latestHash = curr.hash;
            }
          }

          if (latestHash != "") {
            // File present in manifest
            var file = url.substr(url.lastIndexOf("/") + 1).replace(".zip", ".jsa");

            if (assetMeta && assetMeta[path + file]) {
              return latestHash != assetMeta[path + file].zipHashInDisk;
            }

            return true;
          } // File not present in manifest, not downloading by default


          return false;
        };
      };
    };
  };

  exports["getConfigStr'"] = function () {
    return JSON.stringify(window.getConfig());
  };

  exports["getValueFromObject"] = function (json) {
    return function (key) {
      if (typeof json != "undefined" && typeof json[key] != "undefined") {
        return json[key];
      }

      return "";
    };
  };

  exports["setValueFromAppWindow'"] = function (app) {
    return function (key) {
      return function (value) {
        return function () {
          if (typeof window != "undefined" && typeof window.mapps != "undefined" && typeof window.mapps[app] != "undefined") {
            window.mapps[app].contentWindow[key] = value;
          }
        };
      };
    };
  };

  exports["registerIPC'"] = function (client) {
    return function (server) {
      return function (streamer) {
        return function (cb) {
          return function () {
            var clientIndex = window.mappStack.findIndex(function (item) {
              return item.callee == client;
            });

            if (clientIndex == -1) {
              console.error("Client: " + client + " not started yet");
              return;
            }

            var serverIndex = window.mappStack.findIndex(function (item) {
              return item.callee == server;
            });

            if (serverIndex == -1) {
              console.error("Server: " + server + " not started yet");
              return;
            }

            var serverResponse = requestServerConnection(client)(server)(streamer);

            if (serverResponse == null || typeof serverResponse == "undefined") {
              console.log("Aborting IPC registration");
              return emptyIPCClientResponse;
            }

            if (typeof serverResponse.result != "undefined" && typeof serverResponse.channelId != "undefined" && typeof serverResponse.OPS != "undefined") {
              var dataBlock = {
                "clientId": client,
                "serverId": server,
                "channelId": serverResponse.channelId,
                "client_callback": cb
              };
              window.ipcChannels[serverResponse.channelId] = dataBlock;
              addToAppIPCList(clientIndex, serverResponse.channelId);
              addToAppIPCList(serverIndex, serverResponse.channelId);
              console.log("IPC Channel list: ", window.ipcChannels);
              return serverResponse;
            } else {
              console.error("Error: Invalid response from server while trying to register IPC. Aborting...");
              return emptyIPCClientResponse;
            }
          };
        };
      };
    };
  };

  exports["unregisterIPC'"] = function (channelId) {
    return function () {
      if (typeof window.ipcChannels != "undefined" && window.ipcChannels.hasOwnProperty(channelId)) {
        var server = window.ipcChannels[channelId]["serverId"];
        var client = window.ipcChannels[channelId]["clientId"];
        var clientCb = window.ipcChannels[channelId]["client_callback"];
        var clientIndex = window.mappStack.findIndex(function (item) {
          return item.callee == client;
        });

        if (clientIndex == -1) {
          console.error("Client: " + client + " not started yet");
        }

        unRegisterIPCClient(clientCb, channelId);
        var serverIndex = window.mappStack.findIndex(function (item) {
          return item.callee == server;
        });

        if (serverIndex == -1) {
          console.error("Server: " + server + " not started yet");
        }

        unBindIPCServer(server, channelId);
        delete window.ipcChannels[channelId];
        removeFromAppIPCList(serverIndex, channelId);
        removeFromAppIPCList(clientIndex, channelId);
      }
    };
  };

  exports["unregisterAllIPC'"] = function (clientId) {
    return function () {
      var clientIndex = window.mappStack.findIndex(function (item) {
        return item.callee == clientId;
      });

      if (clientIndex == -1) {
        console.error("Client: " + client + " not started yet");
        return;
      }

      var channelIds = window.mappStack[clientIndex].ipcChannels;
      console.log("removing ipcs for " + clientId + ": ", channelIds);

      for (var i in channelIds) {
        var channelId = channelIds[i];

        if (typeof window.ipcChannels != "undefined" && window.ipcChannels.hasOwnProperty(channelId)) {
          var server = window.ipcChannels[channelId]["serverId"];
          var client = window.ipcChannels[channelId]["clientId"];
          var clientCb = window.ipcChannels[channelId]["client_callback"];
          unRegisterIPCClient(clientCb, channelId);
          var serverIndex = window.mappStack.findIndex(function (item) {
            return item.callee == server;
          });

          if (serverIndex == -1) {
            console.error("Server: " + server + " not started yet");
          }

          unBindIPCServer(server, channelId);
          delete window.ipcChannels[channelId];
          removeFromAppIPCList(serverIndex, channelId);
          removeFromAppIPCList(clientIndex, channelId);
        }
      }
    };
  };

  exports["acknowledgeClient'"] = function (clientId) {
    return function (serverResponse) {
      return function (ipcClient) {
        return function () {
          if (typeof ipcClient != "undefined") {
            if (typeof ipcClient.onServerConnected != "undefined" && typeof ipcClient.onServerDisconnected != "undefined") {
              ipcClient.onServerConnected(serverResponse);
            } else {
              console.log("Error: Incorrect implementation of IPC Client");
            }
          } else {
            console.log("IPC Client not found");
          }
        };
      };
    };
  };

  var addToAppIPCList = function addToAppIPCList(appIndex, channelId) {
    if (appIndex == -1) return;
    var app = window.mappStack[appIndex];
    app.ipcChannels = app.ipcChannels || [];
    app.ipcChannels.push(channelId);
  };

  var removeFromAppIPCList = function removeFromAppIPCList(appIndex, channelId) {
    if (appIndex == -1) return;
    var app = window.mappStack[appIndex];
    app.ipcChannels = app.ipcChannels || [];
    var index = app.ipcChannels.indexOf(channelId);
    if (index != -1) app.ipcChannels.splice(index, 1);
  };

  var requestServerConnection = function requestServerConnection(client) {
    return function (server) {
      return function (streamer) {
        var response = null;

        if (isIpcServerAvailable(server)) {
          if (typeof window.mapps[server] != "undefined" && window.mapps[server].contentWindow[ipcServer]["onBind"]) {
            response = window.mapps[server].contentWindow[ipcServer]["onBind"](streamer);
          } else {
            console.error("Error: Incorrect implementation of IPC Server");
          }
        } else {
          console.log("IPC server not found");
        }

        return response;
      };
    };
  };

  var isIpcServerAvailable = function isIpcServerAvailable(server) {
    console.log(ipcServer);
    return typeof window != "undefined" && typeof window.mapps[server] != "undefined" && typeof window.mapps[server].contentWindow != "undefined" && typeof window.mapps[server].contentWindow[ipcServer] != "undefined";
  };

  var unBindIPCServer = function unBindIPCServer(server, channelId) {
    try {
      if (isIpcServerAvailable(server)) {
        if (window.mapps[server].contentWindow[ipcServer]["onUnBind"]) {
          window.mapps[server].contentWindow[ipcServer]["onUnBind"](channelId);
        } else {
          console.error("Error: Incorrect implementation of IPC Server");
        }
      } else {
        console.log("IPC server not found");
      }
    } catch (err) {
      console.error(err);
    }
  };

  var unRegisterIPCClient = function unRegisterIPCClient(clientCb, channelId) {
    if (clientCb && typeof clientCb.onServerDisconnected == "function") {
      clientCb.onServerDisconnected(channelId);
    }
  };

  exports["addEventListener'"] = function (caller) {
    return function (event) {
      return function () {
        var index = window.mappStack.findIndex(function (item) {
          return item.callee == caller;
        });

        if (index == -1) {
          console.error("Caller: " + caller + " not started yet");
          return;
        }

        var app = window.mappStack[index];
        window.eventListeners[event] = window.eventListeners[event] || [];
        window.eventListeners[event].push(caller);
        app.eventListeners = app.eventListeners || [];
        app.eventListeners.push(event);

        if (typeof window[event] != "function") {
          window[event] = function () {
            var callers = window.eventListeners[event] || [];

            for (var i = callers.length - 1; i >= 0; i--) {
              var appId = callers[i];
              if (window.mappStack.length == 0 || appId != window.mappStack[window.mappStack.length - 1].callee) continue;

              if (window.mapps[appId]) {
                console.log("window.mapps[" + appId + "].contentWindow[" + event + "](..." + JSON.stringify(arguments) + ");");
                var args = arguments;
                window.mapps[appId].contentWindow[event].apply(undefined, args);
                break;
              }
            }
          };
        }
      };
    };
  };

  exports["removeEventListener'"] = function (caller) {
    return function (event) {
      window.eventListeners[event] = window.eventListeners[event] || [];
      var index = window.eventListeners[event].indexOf(caller);

      if (index != -1) {
        window.eventListeners[event].splice(index, 1);
      }

      if (window.eventListeners[event].length == 0) {
        delete window.eventListeners[event];
      }
    };
  };

  exports["emitEvent'"] = function (caller) {
    return function (receiver) {
      return function (event) {
        return function (data) {
          return function (callback) {
            return function () {
              var callerWindow = (window.mapps[caller] || {}).contentWindow;

              if (receiver === "java" && callerWindow && callerWindow.__PROXY_FN && callerWindow.__FN_INDEX) {
                var index = ++callerWindow.__FN_INDEX + "";

                callerWindow.__PROXY_FN["F" + index] = function (response) {
                  if (typeof callback === "function") {
                    callback(response);
                  }
                };

                _jbridge.runInJuspayBrowser("onEvent", data, "F" + index);
              } else {
                var receiverWindow = (window.mapps[receiver] || {}).contentWindow;

                if (receiverWindow && typeof receiverWindow[event] == "function") {
                  receiverWindow[event](data, callback);
                }
              }
            };
          };
        };
      };
    };
  };

  exports["bringToTop'"] = function (which) {
    return function () {
      if (which == "java") return;
      var index = mappStack.findIndex(function (ele) {
        return ele.callee == which;
      });

      if (index == -1) {
        console.error("App not found!");
        return;
      }

      function move(array, from, to) {
        if (to === from) {
          console.log(which + ": Already on top");
          return array;
        }

        var target = array[from];
        var increment = to < from ? -1 : 1;

        for (var k = from; k != to; k += increment) {
          array[k] = array[k + increment];
        }

        array[to] = target;
        return array;
      }

      var prevApp = window.mappStack[window.mappStack.length - 1];
      window.mappStack = move(window.mappStack, index, window.mappStack.length - 1);
      var currApp = window.mappStack[window.mappStack.length - 1];

      if (currApp.callee != prevApp.callee) {
        if (typeof window.mapps[prevApp.callee].contentWindow.onPause == "function") window.mapps[prevApp.callee].contentWindow.onPause();
      }

      if (typeof window.mapps[currApp.callee].contentWindow.onResume == "function") window.mapps[currApp.callee].contentWindow.onResume();
    };
  };

  exports["resetHooks'"] = function (which) {
    return function () {
      try {
        if (window.mappStack.length > 0) {
          var app = window.mappStack[window.mappStack.length - 1];
          window.__PROXY_FN = window.mapps[app.callee].contentWindow.window.__PROXY_FN;
        } else {
          console.log("No Apps on top");
        }
      } catch (err) {
        console.error(err);

        _jbridge.exitApp(0, "{\"status\":\"failure\",\"order_id\":\"\",\"message\":\"Window not found\"}");
      }
    };
  };

  exports.getSessionInfo = function () {
    return JSON.parse(_jbridge.getSessionInfo());
  };

  exports["getMD5'"] = function (str) {
    return function () {
      return _jbridge.getMd5(str);
    };
  }; // Taken from purescript-foreign-object


  function toArrayWithKey(f) {
    return function (m) {
      var r = [];

      for (var k in m) {
        if (hasOwnProperty.call(m, k)) {
          r.push(f(k)(m[k]));
        }
      }

      return r;
    };
  }

  exports.keys = Object.keys || toArrayWithKey(function (k) {
    return function () {
      return k;
    };
  });
})(PS["Engineering.Helpers.Utils"] = PS["Engineering.Helpers.Utils"] || {});

(function (exports) {
  window.bundle = JSON.parse(_jbridge.getSessionAttribute("bundleParams", "{}"));
  window.logList = [];
  window.canPostLogs = true;
  window.merchantLogsPostingEnabled = !bundle.logsPostingEnabled || !(bundle.logsPostingEnabled.toString() == "false");
  window.pageId = 0;
  window.logsUrl = bundle.logsPostingUrl || _jbridge.getResourceByName("juspay_analytics_endpoint");

  _jbridge.setAnalyticsEndPoint(logsUrl);

  var EVENT_CATEGORY_HYPERPAY = "hyper_pay";
  var EVENT_ACTION_INFO = "info";
  var EVENT_CATEGORY_CONFIG = "config";
  var EVENT_ACTION_CHECK = "check";
  var LOG_LEVEL_MINIMAL = "1";

  var stampLogs = function stampLogs(log) {
    var sn = _jbridge.getSessionAttribute("sn", "0");

    _jbridge.setSessionAttribute("sn", "" + (parseInt(sn) + 1));

    log["at"] = Date.now();
    log["sn"] = sn;
    log["session_id"] = window.hyper_session_id;
    log["parent_session_id"] = "null";
    log["global_session_id"] = window.hyper_session_id;
    return log;
  };

  var buildApiLogs = function buildApiLogs(apiData) {
    var dataMap = {};
    dataMap.type = "api_request";
    dataMap["url"] = apiData.url;
    dataMap["api_load_start"] = apiData.apiStartTime;
    dataMap["api_load_end"] = apiData.apiEndTime;
    var latency = parseInt(apiData.apiEndTime) - parseInt(apiData.apiStartTime);
    dataMap["latency"] = latency == NaN ? "Error while parsing latency" : latency; // If latency is NaN then there is parsing value to int is error

    dataMap["status_code"] = apiData.statusCode;
    dataMap["log_level"] = LOG_LEVEL_MINIMAL;
    return stampLogs(dataMap);
  };

  var preProcessEvent = function preProcessEvent(category) {
    return function (action) {
      return function (key) {
        return function (value) {
          return function () {
            var dataMap = {};
            dataMap["type"] = "event";
            dataMap["category"] = category;
            dataMap["action"] = action;
            dataMap["label"] = key;
            dataMap["value"] = value;
            return stampLogs(dataMap);
          };
        };
      };
    };
  };

  exports.addToLogList = function (logs) {
    return function () {
      _jbridge.addToLogList(JSON.stringify(logs));
    };
  };

  exports.preProcessSessionInfo = function (session) {
    return function () {
      return stampLogs(session);
    };
  };

  exports.preProcessEvent = preProcessEvent;

  exports["trackMicroAppStart'"] = function (caller) {
    return function (callee) {
      return function () {
        return preProcessEvent("microapp")("invocation")(caller)(callee)();
      };
    };
  };

  exports["trackMicroAppEnd'"] = function (callee) {
    return function (code) {
      return function (status) {
        return function () {
          return preProcessEvent("microapp")("termination")(callee)(JSON.stringify({
            code: code,
            status: status
          }))();
        };
      };
    };
  };
})(PS["Tracker.Tracker"] = PS["Tracker.Tracker"] || {});

(function (exports) {
  "use strict";

  var $foreign = PS["Tracker.Tracker"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Timer = PS["Control.Monad.Eff.Timer"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Data_Array = PS["Data.Array"];
  var Data_Boolean = PS["Data.Boolean"];
  var Data_Function = PS["Data.Function"];
  var Data_Ord = PS["Data.Ord"];
  var Engineering_Helpers_Commons = PS["Engineering.Helpers.Commons"];
  var Engineering_Types_App = PS["Engineering.Types.App"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Types_Language_Flow = PS["Presto.Core.Types.Language.Flow"];

  var trackSession = function trackSession(session) {
    return Engineering_Helpers_Commons.toFlow(Control_Bind.bind(Control_Monad_Eff.bindEff)($foreign.preProcessSessionInfo(session))($foreign.addToLogList));
  };

  var trackMicroAppStart = function trackMicroAppStart(caller) {
    return function (callee) {
      return Engineering_Helpers_Commons.toFlow(Control_Bind.bindFlipped(Control_Monad_Eff.bindEff)($foreign.addToLogList)($foreign["trackMicroAppStart'"](caller)(callee)));
    };
  };

  var trackMicroAppEnd = function trackMicroAppEnd(callee) {
    return function (code) {
      return function (status) {
        return Engineering_Helpers_Commons.toFlow(Control_Bind.bindFlipped(Control_Monad_Eff.bindEff)($foreign.addToLogList)($foreign["trackMicroAppEnd'"](callee)(code)(status)));
      };
    };
  };

  var trackEvent = function trackEvent(category) {
    return function (action) {
      return function (label) {
        return function (value) {
          return Engineering_Helpers_Commons.toFlow(Control_Bind.bind(Control_Monad_Eff.bindEff)($foreign.preProcessEvent(category)(action)(label)(value))($foreign.addToLogList));
        };
      };
    };
  };

  var trackOSError = function trackOSError(key) {
    return function (value) {
      return trackEvent("os")("error")(key)(value);
    };
  };

  var trackOSEvent = function trackOSEvent(key) {
    return function (value) {
      return trackEvent("os")("info")(key)(value);
    };
  };

  var initTracking = function initTracking(batch) {
    return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(trackOSEvent("hyper_os_version")(Engineering_Types_App.hyperOSVersion))(function () {
      return trackOSEvent("microapp_name")(Engineering_Types_App.appId);
    });
  };

  exports["initTracking"] = initTracking;
  exports["trackOSEvent"] = trackOSEvent;
  exports["trackOSError"] = trackOSError;
  exports["trackMicroAppStart"] = trackMicroAppStart;
  exports["trackMicroAppEnd"] = trackMicroAppEnd;
  exports["trackSession"] = trackSession;
  exports["trackEvent"] = trackEvent;
})(PS["Tracker.Tracker"] = PS["Tracker.Tracker"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var $foreign = PS["Engineering.Helpers.Utils"];
  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Aff = PS["Control.Monad.Aff"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_Eff_Class = PS["Control.Monad.Eff.Class"];
  var Control_Monad_Eff_Exception = PS["Control.Monad.Eff.Exception"];
  var Control_Monad_Eff_Uncurried = PS["Control.Monad.Eff.Uncurried"];
  var Control_Monad_Except_Trans = PS["Control.Monad.Except.Trans"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Control_Monad_Trans_Class = PS["Control.Monad.Trans.Class"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Array = PS["Data.Array"];
  var Data_Either = PS["Data.Either"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Function = PS["Data.Function"];
  var Data_Functor = PS["Data.Functor"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Nullable = PS["Data.Nullable"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Semiring = PS["Data.Semiring"];
  var Data_Time_Duration = PS["Data.Time.Duration"];
  var Data_Traversable = PS["Data.Traversable"];
  var Data_Unit = PS["Data.Unit"];
  var Engineering_Helpers_Commons = PS["Engineering.Helpers.Commons"];
  var Engineering_Types_App = PS["Engineering.Types.App"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Flow = PS["Presto.Core.Flow"];
  var Presto_Core_Types_App = PS["Presto.Core.Types.App"];
  var Presto_Core_Types_Language_Flow = PS["Presto.Core.Types.Language.Flow"];
  var Product_Types = PS["Product.Types"];
  var Tracker_Tracker = PS["Tracker.Tracker"];
  var Unsafe_Coerce = PS["Unsafe.Coerce"];

  var unregisterIPC = function unregisterIPC(channelId) {
    return Engineering_Helpers_Commons.toFlow($foreign["unregisterIPC'"](channelId));
  };

  var unregisterAllIPC = function unregisterAllIPC(appId) {
    return Engineering_Helpers_Commons.toFlow($foreign["unregisterAllIPC'"](appId));
  };

  var unregister = function unregister(app) {
    if (app.callee === "__failed") {
      return Control_Applicative.pure(Control_Monad_Free.freeApplicative)($foreign.logAny("unregister: App " + (app.callee + " is not registered and thou shall not be unregistered")));
    }

    ;
    return Control_Bind.bind(Control_Monad_Free.freeBind)(Data_Foldable.foldl(Data_Foldable.foldableArray)(function (x) {
      return function (y) {
        return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
      };
    })(Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit))(Data_Functor.map(Data_Functor.functorArray)(function (x) {
      return $foreign["removeEventListener'"](app.callee)(x);
    })(app.eventListeners)))(function (v) {
      return Engineering_Helpers_Commons.toFlow($foreign["unregister'"](app));
    });
  };

  var resetHooks = function resetHooks(which) {
    return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow($foreign["resetHooks'"](which)))(function (v) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow($foreign["setValueFromAppWindow'"](which)("canPostLogs")(false)))(function (v1) {
        return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
      });
    });
  };

  var removeView = function removeView(app) {
    if (app === "__failed") {
      return Control_Applicative.pure(Control_Monad_Free.freeApplicative)($foreign.logAny("removeView: App " + (app + " is not registered and thou shall not be removed")));
    }

    ;
    return Engineering_Helpers_Commons.toFlow($foreign["removeView'"](app));
  };

  var removeIframe = function removeIframe(app) {
    if (app === "__failed") {
      return Control_Applicative.pure(Control_Monad_Free.freeApplicative)($foreign.logAny("removeIframe: iFrame " + (app + " is not registered and thou shall not be removed")));
    }

    ;
    return Engineering_Helpers_Commons.toFlow($foreign["removeIframe'"](app));
  };

  var registerOS = function registerOS(callee) {
    return function (os) {
      return Engineering_Helpers_Commons.toFlow($foreign["registerOS'"](callee)(os));
    };
  };

  var registerIPC = function registerIPC(clientId) {
    return function (serverId) {
      return function (streamer) {
        return function (cb) {
          return Engineering_Helpers_Commons.toFlow($foreign["registerIPC'"](clientId)(serverId)(streamer)(cb));
        };
      };
    };
  };

  var readIfExist = function readIfExist(callee) {
    return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow($foreign["findPath'"](callee)))(function (v) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow($foreign["readIfExist'"](v)))(function (v1) {
        if (v1 === "__failed") {
          return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Maybe.Nothing.value);
        }

        ;
        return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(new Data_Maybe.Just(v1));
      });
    });
  };

  var readFile = function readFile(filePath) {
    return Control_Bind.bind(Control_Monad_Free.freeBind)(Control_Monad_Except_Trans.runExceptT(Control_Monad_Trans_Class.lift(Control_Monad_Except_Trans.monadTransExceptT)(Control_Monad_Free.freeMonad)(Engineering_Helpers_Commons.toFlow($foreign["readFile'"](filePath)))))(function (v) {
      if (v instanceof Data_Either.Right) {
        return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(v.value0);
      }

      ;

      if (v instanceof Data_Either.Left) {
        return Control_Apply.applySecond(Control_Apply.applyFn)(Control_Applicative.pure(Control_Applicative.applicativeFn)($foreign.logAny(v.value0)))(Control_Applicative.pure(Control_Monad_Free.freeApplicative))("");
      }

      ;
      throw new Error("Failed pattern match at Engineering.Helpers.Utils line 85, column 3 - line 89, column 1: " + [v.constructor.name]);
    });
  };

  var pushToStack = function pushToStack(app) {
    return Engineering_Helpers_Commons.toFlow($foreign["pushToStack'"](app));
  };

  var popFromStack = Engineering_Helpers_Commons.toFlow($foreign["popFromStack'"]);
  var peekStack = Engineering_Helpers_Commons.toFlow($foreign["peekStack'"]);

  var loadS = function loadS(key) {
    return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow($foreign["loadS'"](key)))(function (v) {
      if (v === "__failed") {
        return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Maybe.Nothing.value);
      }

      ;
      return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(new Data_Maybe.Just(v));
    });
  };

  var injectIframe = function injectIframe(name) {
    return function (payload) {
      return function (os) {
        return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow($foreign["injectIframe'"](name)(payload)(os)))(function (v) {
          if (v === "__failed") {
            return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(new Data_Either.Left("Error occurred while injecting payload/iframe"));
          }

          ;
          return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(new Data_Either.Right(true));
        });
      };
    };
  };

  var getMD5 = function getMD5(str) {
    return Engineering_Helpers_Commons.toFlow($foreign["getMD5'"](str));
  };

  var trackConfig = function trackConfig(configStr) {
    return Control_Bind.bind(Control_Monad_Free.freeBind)(Control_Bind.bind(Control_Monad_Free.freeBind)(getMD5(configStr))(Tracker_Tracker.trackEvent("JS")("info")("config_min_hash")))(function (v) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.getValueFromWindow("config_version")("null"))(Tracker_Tracker.trackEvent("config")("info")("version")))(function (v1) {
        return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
      });
    });
  };

  var getConfigStr = Engineering_Helpers_Commons.toFlow($foreign["getConfigStr'"]);

  var generateErrorResp = function generateErrorResp(error) {
    return "{\"error\":\"true\", \"error_message\":\"" + (error + "\"}");
  };

  var fetchResource = function fetchResource(name) {
    return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow($foreign["fetchResource'"](name)))(function (v) {
      if (v === "__failed") {
        return Control_Apply.applySecond(Control_Monad_Free.freeApply)(Tracker_Tracker.trackOSError("fetch_resource")("couldn't fetch resource " + name))(Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Maybe.Nothing.value));
      }

      ;
      return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(new Data_Maybe.Just(v));
    });
  };

  var $$eval = function $$eval(cmd) {
    return Engineering_Helpers_Commons.toFlow($foreign["eval'"](cmd));
  };

  var emitEvent = function emitEvent(caller) {
    return function (receiver) {
      return function (event) {
        return function (data$prime) {
          return function (callback) {
            return Engineering_Helpers_Commons.toFlow($foreign["emitEvent'"](caller)(receiver)(event)(data$prime)(callback));
          };
        };
      };
    };
  };

  var downloadManifest_ = function downloadManifest_(v) {
    return function (location) {
      if (v === 6.0) {
        return Control_Apply.applySecond(Control_Monad_Free.freeApply)(Tracker_Tracker.trackOSError("fetch_resource")("couldn't fetch manifest from " + location))(Control_Applicative.pure(Control_Monad_Free.freeApplicative)({}));
      }

      ;
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Presto_Core_Types_Language_Flow.doAff(Control_Monad_Aff.makeAff(function (success) {
        return Control_Apply.applySecond(Control_Monad_Eff.applyEff)($foreign["downloadManifest'"](location)(function ($56) {
          return success(Data_Either.Right.create($56));
        }))(Control_Applicative.pure(Control_Monad_Eff.applicativeEff)(Control_Monad_Aff.nonCanceler));
      })))(function (v1) {
        if (v1.code === -1) {
          return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(v1.result);
        }

        ;
        return Control_Apply.applySecond(Control_Monad_Free.freeApply)(Presto_Core_Types_Language_Flow.delay(Data_Time_Duration.durationMilliseconds)(1000.0 * v))(downloadManifest_(v + 1.0)(location));
      });
    };
  };

  var downloadManifest = downloadManifest_(0.0);

  var download = function download(url) {
    return function (path) {
      return Presto_Core_Types_Language_Flow.doAff(Control_Monad_Aff.makeAff(function (success) {
        return Control_Apply.applySecond(Control_Monad_Eff.applyEff)($foreign["download'"](url)(path)(function ($57) {
          return success(Data_Either.Right.create($57));
        }))(Control_Applicative.pure(Control_Monad_Eff.applicativeEff)(Control_Monad_Aff.nonCanceler));
      }));
    };
  };

  var fetchIfModified = function fetchIfModified(manifest) {
    return function (assetMeta) {
      return function (appManifest) {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(function () {
          var v = $foreign.shouldDownload(manifest)(assetMeta)(appManifest.src)(appManifest.root);

          if (v) {
            return Control_Apply.applySecond(Control_Monad_Free.freeApply)(download(appManifest.src)(appManifest.root))(Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit));
          }

          ;

          if (!v) {
            return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
          }

          ;
          throw new Error("Failed pattern match at Engineering.Helpers.Utils line 194, column 3 - line 196, column 23: " + [v.constructor.name]);
        }())(function () {
          return Control_Bind.bind(Control_Monad_Free.freeBind)(Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Array.filter(function (x) {
            return $foreign.shouldDownload(manifest)(assetMeta)($foreign.getValueFromObject(appManifest.assets)(x))(appManifest.root);
          })($foreign.keys(appManifest.assets))))(function (v) {
            return Control_Bind.bind(Control_Monad_Free.freeBind)(Data_Traversable.traverse(Data_Traversable.traversableArray)(Control_Monad_Free.freeApplicative)(function (x) {
              return download($foreign.getValueFromObject(appManifest.assets)(x))(appManifest.root);
            })(v))(function (v1) {
              return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
            });
          });
        });
      };
    };
  };

  var downloadAssets = function downloadAssets(url) {
    return Control_Bind.bind(Control_Monad_Free.freeBind)(downloadManifest(url))(function (v) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Control_Bind.bind(Control_Monad_Free.freeBind)(loadS("asset_metadata.json"))(Data_Maybe.maybe(Control_Applicative.pure(Control_Monad_Free.freeApplicative)({}))(function ($58) {
        return Engineering_Helpers_Commons.toFlow(Engineering_Helpers_Commons.toJSON($58));
      })))(function (v1) {
        return Control_Bind.bind(Control_Monad_Free.freeBind)(Control_Bind.bindFlipped(Control_Monad_Free.freeBind)(function ($59) {
          return Engineering_Helpers_Commons.toFlow(Engineering_Helpers_Commons.toJSON($59));
        })(getConfigStr))(function (v2) {
          return Control_Bind.bind(Control_Monad_Free.freeBind)(Control_Applicative.pure(Control_Monad_Free.freeApplicative)($foreign.keys($foreign.getValueFromObject(v2)("apps"))))(function (v3) {
            return Control_Bind.bind(Control_Monad_Free.freeBind)(Data_Traversable.traverse(Data_Traversable.traversableArray)(Control_Monad_Free.freeApplicative)(function (x) {
              return Presto_Core_Types_Language_Flow.oneOf([fetchIfModified(v)(v1)($foreign.getValueFromObject($foreign.getValueFromObject(v2)("apps"))(x)), Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit)]);
            })(v3))(function (v4) {
              return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
            });
          });
        });
      });
    });
  };

  var createIframe = function createIframe(name) {
    return function (src) {
      return Engineering_Helpers_Commons.toFlow($foreign["createIframe'"](name)(src));
    };
  };

  var canOpenApp = function canOpenApp(caller) {
    return function (callee) {
      return Engineering_Helpers_Commons.toFlow($foreign["canOpenApp'"](caller)(callee));
    };
  };

  var bringToTop = function bringToTop(which) {
    return Engineering_Helpers_Commons.toFlow($foreign["bringToTop'"](which));
  };

  var bootComplete = Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow($foreign["bootComplete'"]))(function (v) {
    var $51 = [v.service === "null", v.pre_fetch === "true"];

    if ($51.length === 2 && $51[0] && $51[1]) {
      return Control_Apply.applySecond(Control_Monad_Free.freeApply)(Tracker_Tracker.trackOSEvent("boot_completed")("pre fetch mode"))(Control_Applicative.pure(Control_Monad_Free.freeApplicative)(new Data_Maybe.Just(v)));
    }

    ;

    if ($51.length === 2 && $51[0] && !$51[1]) {
      return Control_Apply.applySecond(Control_Monad_Free.freeApply)(Tracker_Tracker.trackOSError("boot_error")("initial service null"))(Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Maybe.Nothing.value));
    }

    ;
    return Control_Apply.applySecond(Control_Monad_Free.freeApply)(Tracker_Tracker.trackOSEvent("boot_completed")("initial service " + v.service))(Control_Applicative.pure(Control_Monad_Free.freeApplicative)(new Data_Maybe.Just(v)));
  });

  var addEventListener = function addEventListener(caller) {
    return function (event) {
      return Engineering_Helpers_Commons.toFlow($foreign["addEventListener'"](caller)(event));
    };
  };

  var acknowledgeClient = function acknowledgeClient(clientId) {
    return function (serverResponse) {
      return function (cb) {
        return Engineering_Helpers_Commons.toFlow($foreign["acknowledgeClient'"](clientId)(serverResponse)(cb));
      };
    };
  };

  exports["readFile"] = readFile;
  exports["eval"] = $$eval;
  exports["fetchResource"] = fetchResource;
  exports["loadS"] = loadS;
  exports["trackConfig"] = trackConfig;
  exports["bootComplete"] = bootComplete;
  exports["canOpenApp"] = canOpenApp;
  exports["readIfExist"] = readIfExist;
  exports["generateErrorResp"] = generateErrorResp;
  exports["createIframe"] = createIframe;
  exports["injectIframe"] = injectIframe;
  exports["removeIframe"] = removeIframe;
  exports["removeView"] = removeView;
  exports["unregister"] = unregister;
  exports["downloadAssets"] = downloadAssets;
  exports["download"] = download;
  exports["fetchIfModified"] = fetchIfModified;
  exports["downloadManifest"] = downloadManifest;
  exports["downloadManifest_"] = downloadManifest_;
  exports["getConfigStr"] = getConfigStr;
  exports["addEventListener"] = addEventListener;
  exports["emitEvent"] = emitEvent;
  exports["registerIPC"] = registerIPC;
  exports["acknowledgeClient"] = acknowledgeClient;
  exports["unregisterIPC"] = unregisterIPC;
  exports["unregisterAllIPC"] = unregisterAllIPC;
  exports["peekStack"] = peekStack;
  exports["popFromStack"] = popFromStack;
  exports["pushToStack"] = pushToStack;
  exports["registerOS"] = registerOS;
  exports["resetHooks"] = resetHooks;
  exports["bringToTop"] = bringToTop;
  exports["getMD5"] = getMD5;
  exports["logAny"] = $foreign.logAny;
  exports["mergeJBridge'"] = $foreign["mergeJBridge'"];
  exports["shutDown'"] = $foreign["shutDown'"];
  exports["getSessionInfo"] = $foreign.getSessionInfo;
})(PS["Engineering.Helpers.Utils"] = PS["Engineering.Helpers.Utils"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Data_Foldable = PS["Data.Foldable"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Traversable = PS["Data.Traversable"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Types_Language_Flow = PS["Presto.Core.Types.Language.Flow"];
  var Presto_Core_Types_Language_Storage = PS["Presto.Core.Types.Language.Storage"];
  var inParallel$prime = Data_Foldable.traverse_(Control_Monad_Free.freeApplicative)(Data_Foldable.foldableArray)(Presto_Core_Types_Language_Flow.launch);
  exports["inParallel'"] = inParallel$prime;
})(PS["Presto.Core.Operators"] = PS["Presto.Core.Operators"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Apply = PS["Control.Apply"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Data_Either = PS["Data.Either"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Function = PS["Data.Function"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Semigroup = PS["Data.Semigroup"];
  var Data_Tuple = PS["Data.Tuple"];
  var Data_Unit = PS["Data.Unit"];
  var Engineering_Helpers_Commons = PS["Engineering.Helpers.Commons"];
  var Engineering_Helpers_Utils = PS["Engineering.Helpers.Utils"];
  var Engineering_Types_App = PS["Engineering.Types.App"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Flow = PS["Presto.Core.Flow"];
  var Product_Types = PS["Product.Types"];
  var Tracker_Tracker = PS["Tracker.Tracker"];

  var unload = function unload(code) {
    return function (status) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.peekStack)(function (v) {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.removeView(v.callee))(function () {
          return Engineering_Helpers_Utils.removeIframe(v.callee);
        });
      });
    };
  };

  var openIPC = function openIPC(clientId) {
    return function (serverId) {
      return function (streamer) {
        return function (cb) {
          return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.registerIPC(clientId)(serverId)(streamer)(cb))(function (v) {
            var $9 = v.result === "OK";

            if ($9) {
              return Engineering_Helpers_Utils.acknowledgeClient(clientId)(v)(cb);
            }

            ;
            return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
          });
        };
      };
    };
  };

  var onResume = function onResume(which) {
    return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.bringToTop(which))(function () {
      return Engineering_Helpers_Utils.resetHooks(which);
    });
  };

  var returnToCaller = function returnToCaller(app) {
    return function (code) {
      return function (status) {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(onResume(app.caller))(function () {
          return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Tracker_Tracker.trackMicroAppEnd(app.caller)(code)(status))(function () {
            return Engineering_Helpers_Commons.toFlow(app.callback(code)(status));
          });
        });
      };
    };
  };

  var onError = function onError(caller) {
    return function (cb) {
      return function (error) {
        return Control_Apply.applySecond(Control_Monad_Free.freeApply)(Tracker_Tracker.trackMicroAppEnd(caller)(0)(error))(Engineering_Helpers_Commons.toFlow(cb(0)(Engineering_Helpers_Utils.generateErrorResp(error))));
      };
    };
  };

  var killIPC = function killIPC(app) {
    return Engineering_Helpers_Utils.unregisterAllIPC(app.callee);
  };

  var init = function init(caller) {
    return function (callee) {
      return function (payload) {
        return function (os) {
          return function (exit) {
            return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.injectIframe(callee)(payload)(os))(Data_Either.either(onError(caller)(exit))(function (v) {
              return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
            }));
          };
        };
      };
    };
  };

  var closeIPC = function closeIPC(channelId) {
    return Engineering_Helpers_Utils.unregisterIPC(channelId);
  };

  var beforeUnload = function beforeUnload(code) {
    return function (status) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.peekStack)(killIPC);
    };
  };

  var beforeInit = function beforeInit(caller) {
    return function (callee) {
      return function (payload) {
        return function (exit) {
          return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.canOpenApp(caller)(callee))(function (v) {
            return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.readIfExist(callee))(function (v1) {
              var v2 = new Data_Tuple.Tuple(v, v1);

              if (v2.value0 && v2.value1 instanceof Data_Maybe.Just) {
                return Engineering_Helpers_Utils.createIframe(callee)(v2.value1.value0);
              }

              ;

              if (v2.value0 && v2.value1 instanceof Data_Maybe.Nothing) {
                return onError(caller)(exit)("Could find/read " + callee);
              }

              ;
              return onError(caller)(exit)("No Sufficient permissions for caller: " + (caller + (" to open callee: " + callee)));
            });
          });
        };
      };
    };
  };

  var afterUnload = function afterUnload(code) {
    return function (status) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.peekStack)(function (v) {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.unregister(v))(function () {
          return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.popFromStack)(function () {
            return returnToCaller(v)(code)(status);
          });
        });
      });
    };
  };

  var afterInit = function afterInit(caller) {
    return function (callee) {
      return function (os) {
        return function (cb) {
          return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.registerOS(callee)(os))(function () {
            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.pushToStack({
              caller: caller,
              callee: callee,
              callback: cb,
              eventListeners: [],
              ipcChannels: []
            }))(function () {
              return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.resetHooks(callee))(function () {
                return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(onResume(callee))(function () {
                  return Tracker_Tracker.trackMicroAppStart(caller)(callee);
                });
              });
            });
          });
        };
      };
    };
  };

  exports["onError"] = onError;
  exports["beforeInit"] = beforeInit;
  exports["init"] = init;
  exports["afterInit"] = afterInit;
  exports["beforeUnload"] = beforeUnload;
  exports["unload"] = unload;
  exports["afterUnload"] = afterUnload;
  exports["openIPC"] = openIPC;
  exports["closeIPC"] = closeIPC;
  exports["killIPC"] = killIPC;
  exports["onResume"] = onResume;
  exports["returnToCaller"] = returnToCaller;
})(PS["Product.Helpers"] = PS["Product.Helpers"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Free = PS["Control.Monad.Free"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Eq = PS["Data.Eq"];
  var Data_Function = PS["Data.Function"];
  var Data_HeytingAlgebra = PS["Data.HeytingAlgebra"];
  var Data_Maybe = PS["Data.Maybe"];
  var Data_Ring = PS["Data.Ring"];
  var Data_Unit = PS["Data.Unit"];
  var Engineering_Helpers_Apply = PS["Engineering.Helpers.Apply"];
  var Engineering_Helpers_Commons = PS["Engineering.Helpers.Commons"];
  var Engineering_Helpers_Utils = PS["Engineering.Helpers.Utils"];
  var Engineering_Types_App = PS["Engineering.Types.App"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Flow = PS["Presto.Core.Flow"];
  var Presto_Core_Operators = PS["Presto.Core.Operators"];
  var Product_Helpers = PS["Product.Helpers"];
  var Product_Types = PS["Product.Types"];
  var Tracker_Tracker = PS["Tracker.Tracker"];

  var postConfigDownload = function postConfigDownload(v) {
    if (!v) {
      return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Engineering_Helpers_Utils.logAny("retaining config"));
    }

    ;

    if (v) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.readFile("v1-config.jsa"))(function (v1) {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils["eval"](v1))(function () {
          return Engineering_Helpers_Utils.trackConfig(v1);
        });
      });
    }

    ;
    throw new Error("Failed pattern match at Product.BootLoader line 44, column 1 - line 44, column 43: " + [v.constructor.name]);
  };

  var updateAssets = function updateAssets(payload) {
    return Control_Bind.bind(Control_Monad_Free.freeBind)(Control_Bind.bindFlipped(Control_Monad_Free.freeBind)(function ($20) {
      return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Maybe.fromMaybe("https://d3n85rao6710xg.cloudfront.net/juspay/payments/v1-config.zip")($20));
    })(Engineering_Helpers_Utils.fetchResource("config_location")))(function (v) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.download(v)(""))(function (v1) {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(postConfigDownload(v1))(function () {
          return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.downloadAssets("https://d3n85rao6710xg.cloudfront.net/juspay/payments/manifest.json"))(function () {
            if (payload instanceof Data_Maybe.Just) {
              var v2 = payload.value0.service === "null" && payload.value0.pre_fetch === "true";

              if (v2) {
                return Engineering_Helpers_Commons.toFlow(Engineering_Helpers_Utils["shutDown'"](-1 | 0)("PreFetch completed"));
              }

              ;

              if (!v2) {
                return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
              }

              ;
              throw new Error("Failed pattern match at Product.BootLoader line 38, column 7 - line 40, column 27: " + [v2.constructor.name]);
            }

            ;

            if (payload instanceof Data_Maybe.Nothing) {
              return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
            }

            ;
            throw new Error("Failed pattern match at Product.BootLoader line 36, column 3 - line 41, column 25: " + [payload.constructor.name]);
          });
        });
      });
    });
  };

  var onFinish = function onFinish(code) {
    return function (status) {
      return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Product_Helpers.beforeUnload(code)(status))(function () {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Product_Helpers.unload(code)(status))(function () {
          return Product_Helpers.afterUnload(code)(status);
        });
      });
    };
  };

  var startApp = function startApp(caller) {
    return function (callee) {
      return function (payload) {
        return function (cb) {
          var os = os_hooks(callee);
          return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Product_Helpers.beforeInit(caller)(callee)(payload)(cb))(function () {
            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Product_Helpers.init(caller)(callee)(payload)(os)(cb))(function () {
              return Product_Helpers.afterInit(caller)(callee)(os)(cb);
            });
          });
        };
      };
    };
  };

  var os_hooks = function os_hooks(id) {
    return {
      finish: Engineering_Helpers_Apply.apply2(onFinish),
      openIPC: Engineering_Helpers_Apply.apply3(Product_Helpers.openIPC(id)),
      closeIPC: Engineering_Helpers_Apply.apply1(Product_Helpers.closeIPC),
      startApp: Engineering_Helpers_Apply.apply3(startApp(id)),
      addEventListener: Engineering_Helpers_Apply.apply1(Engineering_Helpers_Utils.addEventListener(id)),
      emitEvent: Engineering_Helpers_Apply.apply4(Engineering_Helpers_Utils.emitEvent(id))
    };
  };

  var bootSuccess = function bootSuccess(payload) {
    return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Engineering_Helpers_Utils.logAny(payload)))(function () {
      var v = payload.service === "null";

      if (v) {
        return Control_Applicative.pure(Control_Monad_Free.freeApplicative)(Data_Unit.unit);
      }

      ;

      if (!v) {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Tracker_Tracker.initTracking(10000))(function () {
          return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow(Engineering_Helpers_Utils.getSessionInfo))(Tracker_Tracker.trackSession))(function () {
            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Engineering_Helpers_Commons.toFlow(Engineering_Helpers_Utils["mergeJBridge'"]))(function () {
              return startApp("java")(payload.service)(payload)(Engineering_Helpers_Utils["shutDown'"]);
            });
          });
        });
      }

      ;
      throw new Error("Failed pattern match at Product.BootLoader line 64, column 3 - line 70, column 58: " + [v.constructor.name]);
    });
  };

  var bootError = function bootError(v) {
    return Engineering_Helpers_Commons.toFlow(Engineering_Helpers_Utils["shutDown'"](0)(Engineering_Helpers_Utils.generateErrorResp("Service not passed in the bundle")));
  };

  var onBootComplete = function onBootComplete(payload) {
    return Data_Maybe["maybe'"](bootError)(bootSuccess)(payload);
  };

  var mainFlow = Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.readFile("v1-config.jsa"))(function (v) {
    return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils["eval"](v))(function (v1) {
      return Control_Bind.bind(Control_Monad_Free.freeBind)(Engineering_Helpers_Utils.bootComplete)(function (v2) {
        return Presto_Core_Operators["inParallel'"]([updateAssets(v2), onBootComplete(v2), Engineering_Helpers_Utils.trackConfig(v)]);
      });
    });
  });
  exports["mainFlow"] = mainFlow;
  exports["updateAssets"] = updateAssets;
  exports["postConfigDownload"] = postConfigDownload;
  exports["onBootComplete"] = onBootComplete;
  exports["bootError"] = bootError;
  exports["bootSuccess"] = bootSuccess;
  exports["startApp"] = startApp;
  exports["onFinish"] = onFinish;
  exports["os_hooks"] = os_hooks;
})(PS["Product.BootLoader"] = PS["Product.BootLoader"] || {});

(function (exports) {
  // Generated by purs version 0.11.7
  "use strict";

  var Control_Applicative = PS["Control.Applicative"];
  var Control_Bind = PS["Control.Bind"];
  var Control_Monad_Aff = PS["Control.Monad.Aff"];
  var Control_Monad_Aff_AVar = PS["Control.Monad.Aff.AVar"];
  var Control_Monad_Eff = PS["Control.Monad.Eff"];
  var Control_Monad_State_Trans = PS["Control.Monad.State.Trans"];
  var Control_Semigroupoid = PS["Control.Semigroupoid"];
  var Data_Either = PS["Data.Either"];
  var Data_Function = PS["Data.Function"];
  var Data_Function_Uncurried = PS["Data.Function.Uncurried"];
  var Data_StrMap = PS["Data.StrMap"];
  var Engineering_Helpers_Commons = PS["Engineering.Helpers.Commons"];
  var Engineering_OS_Permission = PS["Engineering.OS.Permission"];
  var Engineering_Types_App = PS["Engineering.Types.App"];
  var Prelude = PS["Prelude"];
  var Presto_Core_Flow = PS["Presto.Core.Flow"];
  var Presto_Core_Language_Runtime_Interpreter = PS["Presto.Core.Language.Runtime.Interpreter"];
  var Product_BootLoader = PS["Product.BootLoader"];
  var appFlow = Product_BootLoader.mainFlow;

  var main = function () {
    var uiRunner = function uiRunner(a) {
      return Control_Monad_Aff.makeAff(function (cb) {
        return function __do() {
          var v = Engineering_Helpers_Commons["showUI'"](function ($2) {
            return cb(Data_Either.Right.create($2));
          }, "")();
          return Control_Monad_Aff.nonCanceler;
        };
      });
    };

    var permissionRunner = new Presto_Core_Language_Runtime_Interpreter.PermissionRunner(Engineering_OS_Permission.checkIfPermissionsGranted, Engineering_OS_Permission.requestPermissions);

    var apiRunner = function apiRunner(request) {
      return Control_Monad_Aff.makeAff(function (cb) {
        return function __do() {
          var v = Engineering_Helpers_Commons["callAPI'"](function ($3) {
            return cb(Data_Either.Left.create($3));
          })(function ($4) {
            return cb(Data_Either.Right.create($4));
          })(Engineering_Helpers_Commons.mkNativeRequest(request))();
          return Control_Monad_Aff.nonCanceler;
        };
      });
    };

    var runtime = new Presto_Core_Language_Runtime_Interpreter.Runtime(uiRunner, permissionRunner, apiRunner);
    var freeFlow = Control_Monad_State_Trans.evalStateT(Control_Monad_Aff.functorAff)(Presto_Core_Language_Runtime_Interpreter.run(runtime)(appFlow));
    return Control_Monad_Aff.launchAff_(Control_Bind.bind(Control_Monad_Aff.bindAff)(Control_Monad_Aff_AVar.makeVar(Data_StrMap.empty))(freeFlow));
  }();

  exports["appFlow"] = appFlow;
  exports["main"] = main;
})(PS["Core"] = PS["Core"] || {});

PS["Core"].main();
module.exports = PS;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0).setImmediate, __webpack_require__(0).clearImmediate))

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1), __webpack_require__(6)))

/***/ }),
/* 6 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ })
/******/ ]);
//# sourceMappingURL=index_bundle.js.map
