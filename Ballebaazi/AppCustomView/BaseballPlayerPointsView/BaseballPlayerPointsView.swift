//
//  BaseballPlayerPointsView.swift
//  Letspick
//
//  Created by Vikash Rajput on 20/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class BaseballPlayerPointsView: UIView {
    

    @IBOutlet weak var containerBottomConstraintView: NSLayoutConstraint!
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var playerRoleImgView: UIImageView!
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var lblPointsScoredTitle: UILabel!
    @IBOutlet weak var lblTotalPoints: UILabel!
    @IBOutlet weak var lblPlayerRole: UILabel!
    @IBOutlet weak var lblScoredPlayerPoints: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var lblTotalTitle: UILabel!
    
    
    @IBOutlet weak var lblSingleTitle: UILabel!
    @IBOutlet weak var lblDoubleTitle: UILabel!
    @IBOutlet weak var lblTripleTitle: UILabel!
    @IBOutlet weak var lblHomeRunTitle: UILabel!
    @IBOutlet weak var lblRunsBattleInTitle: UILabel!
    @IBOutlet weak var lblRunScoreTitle: UILabel!
    @IBOutlet weak var lblBallesHitterTitle: UILabel!
    @IBOutlet weak var lblStolenBaseTitle: UILabel!
    @IBOutlet weak var lblInningPitchTitle: UILabel!
    @IBOutlet weak var lblStrikeOutTitle: UILabel!
    @IBOutlet weak var lblEarnedRunTitle: UILabel!
    @IBOutlet weak var lblHitAllowedTitle: UILabel!
    @IBOutlet weak var lblBallPitcherTitle: UILabel!
    
    
    @IBOutlet weak var lblSingleActualPoint: UILabel!
    @IBOutlet weak var lblDoubleActualPoint: UILabel!
    @IBOutlet weak var lblTripleActualPoint: UILabel!
    @IBOutlet weak var lblHomeRunActualPoint: UILabel!
    @IBOutlet weak var lblRunsBattleInActualPoint: UILabel!
    @IBOutlet weak var lblRunScoreActualPoint: UILabel!
    @IBOutlet weak var lblBallesHitterActualPoint: UILabel!
    @IBOutlet weak var lblStolenBaseActualPoint: UILabel!
    @IBOutlet weak var lblInningPitchActualPoint: UILabel!
    @IBOutlet weak var lblStrikeOutActualPoint: UILabel!
    @IBOutlet weak var lblEarnedRunActualPoint: UILabel!
    @IBOutlet weak var lblHitAllowedActualPoint: UILabel!
    @IBOutlet weak var lblBallPitcherActualPoint: UILabel!

    
    @IBOutlet weak var lblSinglePoint: UILabel!
    @IBOutlet weak var lblDoublePoint: UILabel!
    @IBOutlet weak var lblTriplePoint: UILabel!
    @IBOutlet weak var lblHomeRunPoint: UILabel!
    @IBOutlet weak var lblRunsBattleInPoint: UILabel!
    @IBOutlet weak var lblRunScorePoint: UILabel!
    @IBOutlet weak var lblBallesHitterPoint: UILabel!
    @IBOutlet weak var lblStolenBasePoint: UILabel!
    @IBOutlet weak var lblInningPitchPoint: UILabel!
    @IBOutlet weak var lblStrikeOutPoint: UILabel!
    @IBOutlet weak var lblEarnedRunPoint: UILabel!
    @IBOutlet weak var lblHitAllowedPoint: UILabel!
    @IBOutlet weak var lblBallPitcherPoint: UILabel!


    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        if  let entireView =  Bundle.main.loadNibNamed("BaseballPlayerPointsView", owner: self, options: nil)?.first as? UIView{
            entireView.frame = bounds
            entireView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            addSubview(entireView)
            entireView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            lblSingleTitle.text = "Single".localized()
            lblDoubleTitle.text = "Double".localized()
            lblTripleTitle.text = "Triple".localized()
            lblHomeRunTitle.text = "Home Run".localized()
            lblRunsBattleInTitle.text = "Runs Batted In".localized()
            lblRunScoreTitle.text = "Run Scored".localized()
            lblBallesHitterTitle.text = "Base on Balls Hitter".localized()
            lblStolenBaseTitle.text = "Stolen Base".localized()
            lblInningPitchTitle.text = "Inning Pitched".localized()
            lblStrikeOutTitle.text = "StrikeOut".localized()
            lblEarnedRunTitle.text = "Earned Run".localized()
            lblHitAllowedTitle.text = "Hit Allowed".localized()
            lblBallPitcherTitle.text = "Base On Ball Pitcher".localized()
            
            lblTotalTitle.text = "Total".localized()
            lblPointsScoredTitle.text = "Points Scored".localized()
            setupView()
        }
    }
    
    func setupView(){
        playerRoleImgView.layer.cornerRadius = 17.0
        let rectShape = CAShapeLayer()
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 25.0, height: 25.0)).cgPath
        containerView.layer.mask = rectShape
    }
    
    func configData(details: PlayerDetails) {
//        if UIScreen.main.bounds.size.height > 667{
//            containerBottomConstraintView.constant = 635
//            containerView.layoutIfNeeded()
//        }
        
        lblPlayerName.text = details.playerName
        var totalPoints = details.totalClasscPlayerScore
        
        lblCaptain.text = ""
        if details.isCaption{
            totalPoints = details.totalClassicCaptainScore
            lblCaptain.text = "X2(" + "Captain".localized() + ")"
            lblPlayerName.text = details.playerName! + "(C)"
        }
        else if details.isViceCaption {
            totalPoints = details.totalClassicViceCaptainScore
            lblCaptain.text = "X1.5(" + "subCaptain".localized() + ")"
            lblPlayerName.text = details.playerName! + "(VC)"
        }
     
        
        lblStolenBaseActualPoint.text = details.stolenActual
        lblStolenBasePoint.text = details.stolen
        lblInningPitchActualPoint.text = details.inningPitchActual
        lblInningPitchPoint.text = details.inningPitch
        lblStrikeOutActualPoint.text = details.strikeOutActual
        lblStrikeOutPoint.text = details.strikeOut
        lblEarnedRunActualPoint.text = details.earnRunActual
        lblEarnedRunPoint.text = details.earnRun
        lblSingleActualPoint.text = details.singleActual
        lblSinglePoint.text = details.single
        lblDoubleActualPoint.text = details.doubleActual
        lblDoublePoint.text = details.double
        lblBallPitcherActualPoint.text = details.baseballBBHPitchingActual
        lblBallPitcherPoint.text = details.baseballBBHPitching
        lblTripleActualPoint.text = details.tripleActual
        lblTriplePoint.text = details.triple
        lblHomeRunActualPoint.text = details.homeRunActual
        lblHomeRunPoint.text = details.homeRun
        lblRunsBattleInActualPoint.text = details.battleInActual
        lblRunsBattleInPoint.text = details.battleIn
        
        lblRunScoreActualPoint.text = details.runScoreActual
        lblRunScorePoint.text = details.runScore
        lblBallesHitterActualPoint.text = details.baseballBBHActual
        lblBallesHitterPoint.text = details.baseballBBH

        lblHitAllowedActualPoint.text = details.hitAllowedActual
        lblHitAllowedPoint.text = details.hitAllowed
        
        lblScoredPlayerPoints.text = totalPoints
        lblTotalPoints.text = totalPoints
        
        var playerPlayingRole = details.seasonalRole
        if playerPlayingRole.count == 0 {
            playerPlayingRole = details.playerPlayingRole ?? ""
        }
        
        if playerPlayingRole == PlayerType.Outfielders.rawValue {
            lblPlayerRole.text = "Outfielder".localized()
        }
        else if playerPlayingRole == PlayerType.Catcher.rawValue {
            lblPlayerRole.text = "Catcher".localized()
        }
        else if playerPlayingRole == PlayerType.Infielder.rawValue {
            lblPlayerRole.text = "Infielder".localized()
        }
        else if playerPlayingRole == PlayerType.Pitcher.rawValue {
            lblPlayerRole.text = "Pitcher".localized()
        }
        
        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerRoleImgView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerRoleImgView.image = image
                    }
                    else{
                        self?.playerRoleImgView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }
    }
    
    
    @IBAction func reduceButtonAction(_ sender: Any?) {
        
        self.containerBottomConstraintView.constant = -700
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    func showAnimation() {
        self.containerBottomConstraintView.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
        })
    }
    
    @IBAction func swipeGestureTapped(_ sender: Any) {
        reduceButtonAction(nil)
    }
    
}
