//
//  CongratsRewarsClaimPopup.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class CongratsRewarsClaimPopup: UIView {
    
    @IBOutlet weak var lblCongratulations: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    var isViewForClaim = false
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("CongratsRewarsClaimPopup", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            lblCongratulations.text = "Congratulations!".localized()
            
            addSubview(view)
        }
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        if !isViewForClaim {
            removeFromSuperview();
            return
        }
        if let NavVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myClaimVC = storyboard.instantiateViewController(withIdentifier: "MyClaimViewController") as! MyClaimViewController
            NavVC.pushViewController(myClaimVC, animated: true)
        }
        removeFromSuperview();

    }
    
    func configData(mesage: String) {
        lblMessage.text = mesage
    }
}
