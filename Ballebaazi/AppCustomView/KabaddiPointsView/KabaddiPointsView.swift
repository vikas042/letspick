//
//  KabaddiPointsView.swift
//  Letspick
//
//  Created by Vikash Rajput on 12/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class KabaddiPointsView: UIView {
    
    @IBOutlet weak var pointScoredTitle: UILabel!
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblGreenCardTitle: UILabel!
    @IBOutlet weak var lblRedCardTitle: UILabel!
    @IBOutlet weak var yellowCardTitle: UILabel!
    @IBOutlet weak var lblTouchTitle: UILabel!
    @IBOutlet weak var lblSuperTitle: UILabel!
    @IBOutlet weak var lblBonusTitle: UILabel!
    @IBOutlet weak var lblSuccessTitle: UILabel!
    @IBOutlet weak var lblUnSuccessfullTitle: UILabel!
    @IBOutlet weak var lblPushingAllOutPoints: UILabel!
    @IBOutlet weak var lblRaidTitle: UILabel!
    @IBOutlet weak var lblTackleTitle: UILabel!
    @IBOutlet weak var lblCardTitle: UILabel!
    @IBOutlet weak var lblPushingAllOut: UILabel!
    @IBOutlet weak var lblGettingAllOutTitle: UILabel!
    @IBOutlet weak var lblSubstituteTitle: UILabel!
    @IBOutlet weak var lblStartingTitle: UILabel!
    
    
    @IBOutlet weak var templeteViewHeigthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var lblTotalPlayerPoints: UILabel!
    @IBOutlet weak var lblRedCardPoints: UILabel!
    @IBOutlet weak var lblYellowCardPoints: UILabel!
    @IBOutlet weak var lblGreenCardPoints: UILabel!
    @IBOutlet weak var lblGettingAllOut: UILabel!
    @IBOutlet weak var lblTackleSuperPoints: UILabel!
    @IBOutlet weak var lblTackleSucessPoints: UILabel!
    @IBOutlet weak var lblRaidBonusPoint: UILabel!
    @IBOutlet weak var lblRaidUnsuccessfulPoints: UILabel!
    @IBOutlet weak var lblTouchPoint: UILabel!
    @IBOutlet weak var lblSubistitutePoints: UILabel!
    @IBOutlet weak var lblSubstituteYesNo: UILabel!
    @IBOutlet weak var lblYesNo: UILabel!
    @IBOutlet weak var lblPlayerRole: UILabel!
    @IBOutlet weak var lblStartingPoints: UILabel!
    @IBOutlet weak var playerRoleImgView: UIImageView!
    @IBOutlet weak var lblTotalPoint: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var templateBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var templetView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        if  let entireView =  Bundle.main.loadNibNamed("KabaddiPointsView", owner: self, options: nil)?.first as? UIView{
            entireView.frame = bounds
            entireView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            addSubview(entireView)
            entireView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            pointScoredTitle.text = "Points Scored".localized()
            lblGreenCardTitle.text = "Green Card".localized()
            lblRedCardTitle.text = "Red Card".localized()
            yellowCardTitle.text = "Yellow Card".localized()
            lblTouchTitle.text = "Touch".localized()
            lblSuperTitle.text = "Super".localized()
            lblBonusTitle.text = "Bonus".localized()
            lblSuccessTitle.text = "Success".localized()
            lblUnSuccessfullTitle.text = "Unsuccessful".localized()
            lblRaidTitle.text = "Raid".localized()
            lblTackleTitle.text = "Tackle".localized()
            lblCardTitle.text = "Card".localized()
            lblPushingAllOut.text = "Pushing All Out".localized()
            lblGettingAllOutTitle.text = "Getting All Out".localized()
            lblSubstituteTitle.text = "Substitute".localized()
            lblStartingTitle.text = "Starting 7".localized()
            lblTotalTitle.text = "Total".localized()
            
            setupView()
        }
    }
    
    func setupView(){
        playerRoleImgView.layer.cornerRadius = 17.0
        let rectShape = CAShapeLayer()
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 25.0, height: 25.0)).cgPath
        templetView.layer.mask = rectShape
    }
    
    func configData(details: PlayerDetails) {
        if UIScreen.main.bounds.size.height > 667{
            templateBottomConstraint.constant = 612
            templetView.layoutIfNeeded()
        }

        lblPlayerName.text = details.playerName
        var totalPoints = details.totalClasscPlayerScore
       
        lblCaptain.text = ""
        if details.isCaption{
            totalPoints = details.totalClassicCaptainScore
            lblCaptain.text = "X2(" + "Captain".localized() + ")"
            lblPlayerName.text = details.playerName! + "(C)"
            
        }
        else if details.isViceCaption {
            totalPoints = details.totalClassicViceCaptainScore
            lblCaptain.text = "X1.5(Vice-Captain)"
            lblPlayerName.text = details.playerName! + "(VC)"
        }
        
        lblTotalPoint.text = totalPoints
        lblTotalPlayerPoints.text = totalPoints
        
        let startingPoints = details.startingKabaddiPoints
        lblYesNo.text = "No".localized()
        
        if Int(startingPoints) ?? 0 != 0 {
            lblYesNo.text = "Yes".localized()
        }
        
        lblStartingPoints.text = details.startingKabaddiPoints
        
        
        let isSubstitute = details.isSubstitute
        lblSubstituteYesNo.text = "No".localized()
        
        if Int(isSubstitute) ?? 0 != 0 {
            lblSubstituteYesNo.text = "Yes".localized()
        }
        lblSubistitutePoints.text = details.isSubstitute
        
        lblTouchPoint.text = details.raidTouch
        lblRaidBonusPoint.text = details.raidBonus
        lblRaidUnsuccessfulPoints.text = details.raidUnsuccess
        lblTackleSucessPoints.text = details.tacklesSuccess
        lblTackleSuperPoints.text = details.superTackle
        
        lblGettingAllOut.text = details.gettingAllOut
        lblPushingAllOutPoints.text = details.pushingAllOut
        
        lblGreenCardPoints.text = details.greenCard
        lblYellowCardPoints.text = details.yellowCard
        lblRedCardPoints.text = details.redCard

        
        
        var playerPlayingRole = details.seasonalRole
        if playerPlayingRole.count == 0 {
            playerPlayingRole = details.playerPlayingRole ?? ""
        }
        
        if playerPlayingRole == PlayerType.Defender.rawValue {
            playerRoleImgView.image = UIImage(named: "DefenderIconPlaceholder")
            lblPlayerRole.text = "Defender".localized()
        }
        else if playerPlayingRole == PlayerType.AllRounder.rawValue {
            playerRoleImgView.image = UIImage(named: "AllrounderKabaddiPlaceholder")
            lblPlayerRole.text = "All Rounder".localized()
        }
        else if playerPlayingRole == PlayerType.Raider.rawValue {
            playerRoleImgView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Raider".localized()
        }
        
        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerRoleImgView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerRoleImgView.image = image
                    }
                    else{
                        self?.playerRoleImgView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }
    }
    
    
    @IBAction func reduceButtonAction(_ sender: Any?) {
        
        self.templateBottomConstraint.constant = -620
        UIView.animate(withDuration: 0.5, animations: {
            self.templetView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
        
    }
    
    func showAnimation() {
        self.templateBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.templetView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
        })
    }
    
    @IBAction func swipeGestureRecognizer(_ sender: Any) {
        reduceButtonAction(nil)
    }
    
    
}
