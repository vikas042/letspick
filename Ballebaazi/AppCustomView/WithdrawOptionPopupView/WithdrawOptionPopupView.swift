//
//  WithdrawOptionPopupView.swift
//  Letspick
//
//  Created by Vikash Rajput on 04/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

protocol WithdrawOptionPopupViewDelegate {
    func withdrawCashByMethod(methodName: String)
}

class WithdrawOptionPopupView: UIView {
    
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblAddBank: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var addBankButton: UIButton!
    @IBOutlet weak var lblPaytmNumber: UILabel!
    
    // MARK: init
   required init(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)!
       if self.subviews.count == 0 {
           setup()
       }
   }
   
   override init(frame: CGRect) {
       super.init(frame: frame)
       setup()
   }
   
   func setup() {
       
       if let view = Bundle.main.loadNibNamed("WithdrawOptionPopupView", owner: self, options: nil)?.first as? UIView {
           view.frame = bounds
           view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
           view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
           addSubview(view)
       }
   }
       
    @IBAction func addBankButtonTapped(_ sender: Any) {
        hideAnimation()
        if UserDetails.sharedInstance.bankVerified{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let withdrawCashVC = storyboard.instantiateViewController(withIdentifier: "WithdrawCashViewController") as! WithdrawCashViewController
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(withdrawCashVC, animated: true)
            }
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let addBankVC = storyboard.instantiateViewController(withIdentifier: "AddBankViewController") as! AddBankViewController
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(addBankVC, animated: true)
            }
        }
    }
    
    func configData(bankDetails: String) {
        lblPaytmNumber.text = UserDetails.sharedInstance.phoneNumber
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        hideAnimation()
    }
    
    @IBAction func paytmButtonTapped(_ sender: Any) {
        hideAnimation()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let withdrawCashVC = storyboard.instantiateViewController(withIdentifier: "WithdrawCashViewController") as! WithdrawCashViewController
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.pushViewController(withdrawCashVC, animated: true)
        }
    }
    
    
    func hideAnimation() {
        removeFromSuperview()
    }
}
