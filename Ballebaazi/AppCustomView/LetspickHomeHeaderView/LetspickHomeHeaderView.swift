//
//  LetspickHomeHeaderView.swift
//  Letspick
//
//  Created by Vikash Rajput on 27/05/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LetspickHomeHeaderView: UIView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var lblGameType: UILabel!
    
    @IBOutlet weak var sepratorWidthConstant: NSLayoutConstraint!
    var isViewVisiable = false
    var selectedGameType = GameType.Cricket.rawValue
    
    @IBOutlet weak var bottomSepratorLeadingConstraint: NSLayoutConstraint!

    lazy var firstTabGameType = GameType.Cricket.rawValue;
    lazy var secondTabGameType = GameType.Kabaddi.rawValue;
    lazy var thirdTabGameType = GameType.Football.rawValue;
    lazy var fourthTabGameType = GameType.Quiz.rawValue;
    lazy var fifthTabGameType = GameType.Basketball.rawValue;
    lazy var sixthTabGameType = GameType.Baseball.rawValue;

    var tabsArray = Array<Int>()
    
    var cricetHomeBlock = {(selectedTab: Int) -> () in }
    func cricketHomeButtonTapped(complationBlock: @escaping (_ selectedTab: Int) -> Void) {
        cricetHomeBlock = complationBlock
    }
    
    var kabaddiHomeBlock = {(selectedTab: Int) -> () in }
    func kabaddiButtonTapped(complationBlock: @escaping (_ selectedTab: Int) -> Void) {
        kabaddiHomeBlock = complationBlock
    }
    
    var footballHomeBlock = {(selectedTab: Int) -> () in }
    func footballHomeButtonTapped(complationBlock: @escaping (_ selectedTab: Int) -> Void) {
        footballHomeBlock = complationBlock
    }
    
    var quizHomeBlock = {(selectedTab: Int) -> () in }
    func quizHomeButtonTapped(complationBlock: @escaping (_ selectedTab: Int) -> Void) {
        quizHomeBlock = complationBlock
    }

    var basketballHomeBlock = {(selectedTab: Int) -> () in }
    func basketballHomeButtonTapped(complationBlock: @escaping (_ selectedTab: Int) -> Void) {
        basketballHomeBlock = complationBlock
    }

    var baseballHomeBlock = {(selectedTab: Int) -> () in }
    func baseballHomeButtonTapped(complationBlock: @escaping (_ selectedTab: Int) -> Void) {
        baseballHomeBlock = complationBlock
    }

    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("LetspickHomeHeaderView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            addSubview(view)
            NotificationCenter.default.addObserver(self, selector: #selector(self.updateLineupsNotification), name: NSNotification.Name(rawValue: "updateLineupsNotification"), object: nil)

            collectionView.register(UINib(nibName: "GameTabCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GameTabCollectionViewCell")
            (firstTabGameType, secondTabGameType, thirdTabGameType, fourthTabGameType, fifthTabGameType, sixthTabGameType) = AppHelper.getLandingOrder()
            selectedGameType = firstTabGameType

            if firstTabGameType != GameType.None.rawValue {
                tabsArray.append(firstTabGameType)
            }
            
            if secondTabGameType != GameType.None.rawValue {
                tabsArray.append(secondTabGameType)
            }

            if thirdTabGameType != GameType.None.rawValue {
                tabsArray.append(thirdTabGameType)
            }
            
            if fourthTabGameType != GameType.None.rawValue {
                tabsArray.append(fourthTabGameType)
            }
            
            if fifthTabGameType != GameType.None.rawValue {
                tabsArray.append(fifthTabGameType)
            }
            
            if sixthTabGameType != GameType.None.rawValue {
                tabsArray.append(sixthTabGameType)
            }
            
            let sepratorWidth = Int(UIScreen.main.bounds.width)/tabsArray.count
            sepratorWidthConstant.constant = CGFloat(sepratorWidth)


            updateLanguage()
            updateWalletAmount()
        }
    }
    
    @objc func updateLineupsNotification(notification: Notification) {
        collectionView.reloadData()
    }

    //MARK:- -IBAction Methods    
    @IBAction func passesButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "PassIconClicked", info: nil)
          if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
              let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let sessionVC = storyboard.instantiateViewController(withIdentifier: "SessionPassesViewController")
              navVC.pushViewController(sessionVC, animated: true)
          }
    }
    
    @IBAction func walletButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "WalletIconClicked", info: nil)
        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let walletVC = storyboard.instantiateViewController(withIdentifier: "WalletViewController")
            navVC.pushViewController(walletVC, animated: true)
        }
    }
    
    @IBAction func cricketButtonTapped(_ sender: Any?) {
        AppxorEventHandler.logAppEvent(withName: "ChangeSportClicked", info: ["SportType": "Cricket"])

        selectedGameType = GameType.Cricket.rawValue
        
        if selectedGameType == firstTabGameType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedGameType == secondTabGameType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == thirdTabGameType {
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == fourthTabGameType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
            }
            else if tabsArray.count == 4{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
              bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
               bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == fifthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == sixthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 5.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }

        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        
        if sender != nil {
            cricetHomeBlock(selectedGameType)
        }
        else{
            cricetHomeBlock(selectedGameType)
        }
        
        collectionView.reloadData()
    }
    
    @IBAction func kabaddiButtonTapped(_ sender: Any?) {
        
        AppxorEventHandler.logAppEvent(withName: "ChangeSportClicked", info: ["SportType": "Kabaddi"])
        selectedGameType = GameType.Kabaddi.rawValue
        
        if selectedGameType == firstTabGameType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedGameType == secondTabGameType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == thirdTabGameType {
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == fourthTabGameType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
            }
            else if tabsArray.count == 4{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
              bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
               bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == fifthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == sixthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 5.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        if sender != nil {
            kabaddiHomeBlock(selectedGameType)
        }
        else{
            kabaddiHomeBlock(selectedGameType)
        }
        collectionView.reloadData()
    }
    
    @IBAction func footballButtonTapped(_ sender: Any?) {
       
        AppxorEventHandler.logAppEvent(withName: "ChangeSportClicked", info: ["SportType": "Football"])
        selectedGameType = GameType.Football.rawValue
        
        if selectedGameType == firstTabGameType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedGameType == secondTabGameType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == thirdTabGameType {
                        
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == fourthTabGameType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedGameType == fifthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == sixthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 5.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        if sender != nil {
            footballHomeBlock(selectedGameType)
        }
        else{
            footballHomeBlock(selectedGameType)
        }
        collectionView.reloadData()
    }
    
    
    func quizButtonTapped() {
        AppxorEventHandler.logAppEvent(withName: "ChangeSportClicked", info: ["SportType": "Quiz"])
        selectedGameType = GameType.Quiz.rawValue
        
        if selectedGameType == firstTabGameType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedGameType == secondTabGameType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == thirdTabGameType {
                        
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == fourthTabGameType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedGameType == fifthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == sixthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 5.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        quizHomeBlock(selectedGameType)
        collectionView.reloadData()
    }

    func basketballButtonTapped() {
        AppxorEventHandler.logAppEvent(withName: "ChangeSportClicked", info: ["SportType": "Basketball"])
        selectedGameType = GameType.Basketball.rawValue
        
        if selectedGameType == firstTabGameType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedGameType == secondTabGameType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == thirdTabGameType {
                        
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == fourthTabGameType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedGameType == fifthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == sixthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 5.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        basketballHomeBlock(selectedGameType)
        collectionView.reloadData()
    }
    
    func baseballButtonTapped() {
        AppxorEventHandler.logAppEvent(withName: "ChangeSportClicked", info: ["SportType": "Baseball"])
        selectedGameType = GameType.Baseball.rawValue
        
        if selectedGameType == firstTabGameType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedGameType == secondTabGameType {
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 5.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == thirdTabGameType {
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == fourthTabGameType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedGameType == fifthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedGameType == sixthTabGameType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 5.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        baseballHomeBlock(selectedGameType)
        collectionView.reloadData()
    }
    
    @IBAction func notificationButtonTapped(_ sender: Any) {

        AppxorEventHandler.logAppEvent(withName: "NotificationClicked", info: nil)

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let notificationVC = storyboard.instantiateViewController(withIdentifier: "NotificationsViewController")
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(notificationVC, animated: true)
        }        
    }
    
    func callNotificationAPI(isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let params = ["option": "notifications", "user_id": UserDetails.sharedInstance.userID, "last_id": "0"]
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                if let response = result!["response"]?.dictionary{
                    if let responseArray = response["notifications"]?.array{
                        let tempArray = NotificationsDetails.getAllNotificationList(responseArray: responseArray)
                        if tempArray.count > 0{
                            let totalWinner = NotificationsView(frame: APPDELEGATE.window!.frame)
                            totalWinner.notificationArray = tempArray
                            totalWinner.setupData()
                            APPDELEGATE.window!.addSubview(totalWinner)
                            totalWinner.showAnimation()
                            UserDetails.sharedInstance.notificationCount = ""
                            self.updateWalletAmount()
                        }
                        else{
                            AppHelper.showAlertView(message: "You don't have any notification right now.", isErrorMessage: true)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: "You don't have any notification right now.", isErrorMessage: true)
                    }
                }
            }
            
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    //MARK:- Custom Methods
    func updateWalletAmount()  {
        
        if let passStatus = UserDefaults.standard.value(forKey: "passStatus") as? String {
            if passStatus == "1"{
                passView.isHidden = false
            }
        }

        lblNotificationCount.text = UserDetails.sharedInstance.notificationCount
        if (UserDetails.sharedInstance.notificationCount.count == 0) || (UserDetails.sharedInstance.notificationCount == "0") {
            lblNotificationCount.isHidden  = true
        }
        else{
            lblNotificationCount.isHidden  = false
        }
        
        if UserDetails.sharedInstance.notificationCount.count > 1{
            lblNotificationCount.text = "9+"
        }
    }
    
    func showGameChangeOption(title: String) {
        lblGameType.text = title.localized()
        self.layoutIfNeeded()
    }
    
    func updateLanguage() {
        collectionView.reloadData()
    }
    
    //MARK:- CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = UIScreen.main.bounds.width/CGFloat(tabsArray.count)
        if tabsArray.count >= 6{
            let gameType = tabsArray[indexPath.row]
            if gameType != GameType.Basketball.rawValue {
                return CGSize(width: cellWidth - 1, height: collectionView.frame.size.height)
            }
            return CGSize(width: cellWidth + CGFloat(tabsArray.count - 1), height: collectionView.frame.size.height)
        }
        else{
            return CGSize(width: cellWidth , height: collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GameTabCollectionViewCell", for: indexPath) as! GameTabCollectionViewCell
        let gameType = tabsArray[indexPath.row]
        cell.lineUpsView.isHidden = true;
        var isCricketTabSelected = true
        
//        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
//            if let dashboardVC = navVC.viewControllers.first as? DashboardViewController{
//                if dashboardVC.preselectedTab != 100{
//                    isCricketTabSelected = false
//                }
//            }
//        }

        let cellWidth = Int(UIScreen.main.bounds.width)/tabsArray.count
        
        if cellWidth <= 55 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 10)
        }
        else if cellWidth <= 64 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 11)
        }
        else if cellWidth <= 69 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 12)
        }
        else if cellWidth <= 75 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 13)
        }


        if gameType == GameType.Cricket.rawValue {
            cell.lblTitle.text = "Cricket".localized()
            if UserDetails.sharedInstance.isCricketLineupsOut && isCricketTabSelected{
                cell.lineUpsView.isHidden = false
            }
        }
        else if gameType == GameType.Kabaddi.rawValue {
            cell.lblTitle.text = "Kabaddi".localized()
            if UserDetails.sharedInstance.isKabaddiLineupsOut && isCricketTabSelected {
                cell.lineUpsView.isHidden = false
            }
        }
        else if gameType == GameType.Football.rawValue {
            cell.lblTitle.text = "Football".localized()
            if UserDetails.sharedInstance.isFootballLineupsOut && isCricketTabSelected {
                cell.lineUpsView.isHidden = false
            }
        }
        else if gameType == GameType.Basketball.rawValue {
            cell.lblTitle.text = "Basketball".localized()
            if UserDetails.sharedInstance.isBasketballLineupsOut && isCricketTabSelected{
                cell.lineUpsView.isHidden = false
            }
        }
        else if gameType == GameType.Baseball.rawValue {
            cell.lblTitle.text = "Baseball".localized()

            if UserDetails.sharedInstance.isBaseballLineupsOut && isCricketTabSelected{
                cell.lineUpsView.isHidden = false
            }
        }
        else if gameType == GameType.Quiz.rawValue {
            cell.lblTitle.text = "Quiz".localized()
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    cell.lblTitle.text = "क्विज"
                }
            }
        }
        else{
            cell.lblTitle.text = ""
        }
        
        cell.lblTitle.textColor = UIColor.white.withAlphaComponent(0.6)

        if gameType == selectedGameType {
            cell.lblTitle.textColor = UIColor.white.withAlphaComponent(1.0)
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let gameType = tabsArray[indexPath.row]
        selectedGameType = gameType;
        
        if gameType == GameType.Cricket.rawValue {
            cricketButtonTapped(nil)
        }
        else if gameType == GameType.Kabaddi.rawValue {
            kabaddiButtonTapped(nil)
        }
        else if gameType == GameType.Football.rawValue {
            footballButtonTapped(nil)
        }
        else if gameType == GameType.Quiz.rawValue {
            quizButtonTapped()
        }
        else if gameType == GameType.Basketball.rawValue {
            basketballButtonTapped()
        }
        else if gameType == GameType.Baseball.rawValue {
            baseballButtonTapped()
        }
    }
}
