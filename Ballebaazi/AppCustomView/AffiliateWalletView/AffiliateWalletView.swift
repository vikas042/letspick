//
//  AffiliateWalletView.swift
//  Letspick
//
//  Created by Vikash Rajput on 15/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

protocol AffiliateWalletViewDelegate {
    func updateAffilateAmount(newAmount: String, totalEarning: String)
}

class AffiliateWalletView: UIView {
    
    @IBOutlet weak var lblUnusedTitle: UILabel!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var lblBounusTitle: UILabel!
    @IBOutlet weak var lblWinningTitle: UILabel!
    
    @IBOutlet weak var lblUnusedAmt: UILabel!
    @IBOutlet weak var lblBounusAmt: UILabel!
    @IBOutlet weak var lblWinningAmt: UILabel!

    @IBOutlet weak var lblTotalWalletAmount: UILabel!
    @IBOutlet weak var transferAmount: SolidButton!
    @IBOutlet weak var lblTotalAffilateAmt: UILabel!
    
    @IBOutlet weak var lblAmountTrf: UILabel!
    @IBOutlet weak var txtFieldAmount: UITextField!
    @IBOutlet weak var lblAffiliateAmountTitle: UILabel!
    
    var currentAffilateAmount = "0"
    var delegate: AffiliateWalletViewDelegate?
    
    
 // MARK: init
   required init(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)!
       if self.subviews.count == 0 {
           setup()
       }
   }
   
   override init(frame: CGRect) {
       super.init(frame: frame)
       setup()
   }
   
   func setup() {
       
       if let view = Bundle.main.loadNibNamed("AffiliateWalletView", owner: self, options: nil)?.first as? UIView {
           view.frame = bounds
           view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
           view.backgroundColor = UIColor.black.withAlphaComponent(0.5)            
            addSubview(view)
        whiteView.roundViewCorners(corners:[.bottomLeft, .bottomRight], radius: 5)
       }
   }
       
    @IBAction func transferButtonTapped(_ sender: Any) {
        callWithDrawAffilateAmount()
    }
    
    
    func callWithDrawAffilateAmount() {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }

        AppxorEventHandler.logAppEvent(withName: "TransferButtonClicked", info: nil)

        txtFieldAmount.resignFirstResponder()
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "withdraw_affiliates", "user_id": UserDetails.sharedInstance.userID, "amount": txtFieldAmount.text!]

        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    let amount = (Float(self.currentAffilateAmount) ?? 0) - (Float(self.txtFieldAmount.text!) ?? 0)
                    self.currentAffilateAmount = String(Int(amount))
                    self.txtFieldAmount.text = ""
                    self.updateConfig()
                    self.delegate?.updateAffilateAmount(newAmount: self.currentAffilateAmount, totalEarning: "")
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func updateConfig() {
        
        lblUnusedTitle.text = "Unused_affilate".localized() + "(pts)"
        lblBounusTitle.text = "Bonus".localized() + "(pts)"
        lblWinningTitle.text = "Winnings".localized() + "(pts)"
        lblAffiliateAmountTitle.text = "My Affiliate Wallet".localized()
        transferAmount.setTitle("TRANSFER".localized(), for: .normal)
        lblAmountTrf.text = "Amount to be transferred".localized()
        
        lblBounusAmt.text = "" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.bonusCash);
        lblUnusedAmt.text = "" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.unusedCash);
        lblWinningAmt.text = "" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.withdrawableCredits);

        lblTotalWalletAmount.text = "My Wallet".localized() + " pts" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.totalCredits);
        lblTotalAffilateAmt.text = "pts" + AppHelper.formatDecimalNumberString(digites: currentAffilateAmount);
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        removeFromSuperview()
    }
    
}
