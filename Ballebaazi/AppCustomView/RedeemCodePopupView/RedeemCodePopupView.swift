//
//  RedeemCodePopupView.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class RedeemCodePopupView: UIView {

    @IBOutlet weak var okButton: SolidButton!
    @IBOutlet weak var lblRedeemSuccessfully: UILabel!
    @IBOutlet weak var lblCongratulations: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblBonusMessage: UILabel!
    @IBOutlet weak var lblAdminMessage: UILabel!
    
    var isNewUser = false
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("RedeemCodePopupView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            lblCongratulations.text = "Congratulations!".localized()
            lblRedeemSuccessfully.text = "You've redeemed code successfully".localized()
            okButton.setTitle("OK".localized(), for: .normal)
            addSubview(view)
        }
    }
    
    @IBAction func okButtonTapped(_ sender: Any) {
        removeFromSuperview();
        
        if isNewUser {
            if (APPDELEGATE.leagueCode.count > 0) && (APPDELEGATE.leagueCode != kNoValue){
                AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
            }
            else{
                AppHelper.makeDashbaordAsRootViewController()
            }
        }
        else{
//            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
//                navVC.popViewController(animated: true)
//            }
        }
    }
    
    func updateData(details: RedeemCodeDetails)  {
        lblCode.text = details.redeemCode
        lblCongratulations.isHidden = false

        if details.adminMessage.count > 0 {
            lblCode.isHidden = false
            lblBonusMessage.isHidden = true
            lblRedeemSuccessfully.isHidden = true
            lblAdminMessage.isHidden = false
            lblCongratulations.isHidden = true
            lblAdminMessage.showHTML(htmlString: details.adminMessage, isCenterAligment: true)
            return;
        }
        
        lblAdminMessage.isHidden = true
        lblCode.text = details.redeemCode
        lblBonusMessage.text = details.redeemCode

        var bounusStr = "Enjoy "
        
        if details.unusedAmount.count > 0{
            bounusStr = bounusStr + "pts" + details.unusedAmount + " Unused cash"
        }
        
        if details.winningAmount.count > 0{
            bounusStr = bounusStr + ", pts" + details.bonusAmount + " Winning cash"
        }
        
        if details.winningAmount.count > 0{
            bounusStr = bounusStr + ", pts" + details.winningAmount + " Bonus cash"
        }
        
        if details.ticketCounts > 0{
            bounusStr = bounusStr + ", \(details.ticketCounts) " + "Tickets"
        }
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                bounusStr = ""
                
                if details.unusedAmount.count > 0{
                    bounusStr = bounusStr + "pts" + details.unusedAmount + " अनयूज्ड कैश"
                }
                
                if details.winningAmount.count > 0{
                    bounusStr = bounusStr + ", pts" + details.winningAmount + " जीत का कैश"
                }
                
                if details.bonusAmount.count > 0{
                    bounusStr = bounusStr + ", pts" + details.bonusAmount + " बोनस कैश"
                }
                
                if details.ticketCounts > 0{
                    bounusStr = bounusStr + ", \(details.ticketCounts) " + "टिकट"
                }
                bounusStr = bounusStr + " का लाभ उठाएं।"
            }
        }

        if bounusStr.count < 7 {
            lblBonusMessage.isHidden = true
        }
        else{
            lblBonusMessage.isHidden = false
            lblBonusMessage.text = bounusStr
        }
    }
}
