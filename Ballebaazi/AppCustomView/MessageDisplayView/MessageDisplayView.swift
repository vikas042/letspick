//
//  MessageDisplayView.swift
//  apiCallingExample
//
//  Created by Ankit Kejriwal on 27/05/19.
//  Copyright © 2019 madstech. All rights reserved.
//

import UIKit

class MessageDisplayView: UIView {

    var timer: Timer?

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("MessageDisplayView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            
            addSubview(view)
        }
    }
    

    @objc func removeViewFromSuperView(){
        timer?.invalidate()
        timer = nil
        
        self.frame.origin.y = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.frame.origin.y = -100
        }, completion: {res in
        })
    }
    
    func messageDisplay(message:String, color: UIColor?){
        messageLabel.text = message
        containerView.backgroundColor = color
        
    }
    
    func showAnimation() {
        
        var topMargin:CGFloat = 20.0
        if AppHelper.isApplicationRunningOnIphoneX(){
            topMargin = 40.0;
        }
        self.frame.origin.y = -80
        UIView.animate(withDuration: 0.5, animations: {
            self.frame.origin.y = topMargin
        }, completion: {res in
        })

        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            
            timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false){_ in
                weakSelf?.removeViewFromSuperView()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.removeViewFromSuperView), userInfo: nil, repeats: false)
        }

    }
    
}
