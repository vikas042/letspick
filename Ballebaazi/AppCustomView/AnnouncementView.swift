//
//  AnnouncementView.swift
//  Ballebaazi
//
//  Created by Vikash Rajput on 18/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class AnnouncementView: UIView {

    @IBOutlet weak var lblMessage: UILabel!
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("AnnouncementView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            addSubview(view)
        }
    }
    
    func showAnnouncementMessage(message: String) {
        lblMessage.text = message
    }
}
