//
//  BowlingTeamScoreView.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class BowlingTeamScoreView: UIView {
    
    @IBOutlet weak var PointsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewPointsMatchWiseButton: UIButton!
    @IBOutlet weak var lblWicketsTitle: UILabel!
    @IBOutlet weak var pointScoredTitle: UILabel!
    @IBOutlet weak var lblCatchTitle: UILabel!
    
    @IBOutlet weak var lblMaindeTitle: UILabel!
    @IBOutlet weak var lblRunOutTitle: UILabel!
    
    @IBOutlet weak var lblStartingTitle: UILabel!
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblFieldingTitle: UILabel!
    
    @IBOutlet weak var lblBowlingTitle: UILabel!
    
    
    @IBOutlet weak var lblPlayingStatus: UILabel!
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var playerNamelabel: UILabel!
    @IBOutlet weak var matchNameLabel: UILabel!
    @IBOutlet weak var pointsScoredLabel: UILabel!
    @IBOutlet weak var startinPointsLabel: UILabel!
    @IBOutlet weak var wicketsPointsLabel: UILabel!
    @IBOutlet weak var maidenPointsLabel: UILabel!
    @IBOutlet weak var catchPointsLabel: UILabel!
    @IBOutlet weak var runOutPointsLabel: UILabel!
    @IBOutlet weak var totalPointsScoredLabel: UILabel!
    @IBOutlet weak var scoreMultiplyerLabel: UILabel!    
    @IBOutlet weak var templetView: UIView!
    @IBOutlet weak var templeteViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var templatedViewBottomConstaint: NSLayoutConstraint!
    
    
    var playerListArray = Array<PlayerDetails>()
    var selectedGameType = GameType.Cricket.rawValue
    var selectedMathDetails: MatchDetails?
    var playerDetails: PlayerDetails?


    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        if  let entireView =  Bundle.main.loadNibNamed("BowlingTeamScoreView", owner: self, options: nil)?.first as? UIView{
            entireView.frame = bounds
            entireView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            addSubview(entireView)
            entireView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            lblStartingTitle.text = "Starting".localized()
            lblFieldingTitle.text = "Fielding".localized()
            lblWicketsTitle.text = "Wickets".localized()
            lblMaindeTitle.text = "Maiden".localized()
            lblCatchTitle.text = "Catch".localized()
            lblRunOutTitle.text = "Run out".localized()
            lblBowlingTitle.text = "bowling".localized()
            lblTotal.text = "Total".localized()
            pointScoredTitle.text = "Points Scored".localized()

            setupView()
        }
    }
    
    func setupView(){
        
        playerImageView.layer.cornerRadius = 17.0
        let rectShape = CAShapeLayer()
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10.0, height: 10.0)).cgPath
        templetView.layer.mask = rectShape
    }
    
    func configData(details: PlayerDetails, matchDetails: MatchDetails?) {

        if matchDetails?.isMatchTourney ?? false {
            templeteViewHeightConstraint.constant = 380
            PointsViewHeightConstraint.constant = 380
            self.layoutIfNeeded()
        }
        
        playerDetails = details
        
        playerNamelabel.text = details.playerName
        var totalPoints = details.totalBowlingPlayerScore
        scoreMultiplyerLabel.text = ""
        if details.isCaption{
            totalPoints = details.totalBowlingCaptainScore
            scoreMultiplyerLabel.text = "X2(" + "Captain".localized() + ")"
            playerNamelabel.text = details.playerName! + "(C)"
        }
        else if details.isViceCaption{
            totalPoints = details.totalBowlingViceCaptainScore
            scoreMultiplyerLabel.text = "X1.5(" + "subCaptain".localized() + ")"
            playerNamelabel.text = details.playerName! + "(VC)"
        }
        
        totalPointsScoredLabel.text = totalPoints
        pointsScoredLabel.text = totalPoints
        
        let startingPoints = details.startingPoints ?? "0"
        lblPlayingStatus.text = "No".localized()
        
        if Int(startingPoints) ?? 0 > 0 {
            lblPlayingStatus.text = "Yes".localized()
        }
        
        startinPointsLabel.text = details.startingPoints
        wicketsPointsLabel.text = details.bowlerWicketPoints ?? "0"
        maidenPointsLabel.text = details.bowlerMaidenOverPoints ?? "0"
        
        catchPointsLabel.text = details.catchPoints ?? "0"
        runOutPointsLabel.text = details.runoutPoints ?? "0"
                
        var playerPlayingRole = details.seasonalRole
        if playerPlayingRole.count == 0 {
            playerPlayingRole = details.playerPlayingRole ?? ""
        }
        
        showPlayerTypeImg(playerPlayingRole: playerPlayingRole)
        playerImageView.image = UIImage(named: details.playerPlaceholder)

        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerImageView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerImageView.image = image
                    }
                    else{

                        self?.playerImageView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }
    }
    
    func showPlayerTypeImg(playerPlayingRole: String) {
        if playerPlayingRole == PlayerType.Batsman.rawValue {
            matchNameLabel.text = "BatIcon".localized()
        }
        else if playerPlayingRole == PlayerType.Bowler.rawValue {
            matchNameLabel.text = "BowlIcon".localized()
        }
        else if playerPlayingRole == PlayerType.AllRounder.rawValue {
            matchNameLabel.text = "All Rounder".localized()
        }
        else if playerPlayingRole == PlayerType.WicketKeeper.rawValue {
            matchNameLabel.text = "Wicket Keeper".localized()
        }
    }
    
    
    @IBAction func reduceButtonAction(_ sender: Any?) {
        
        self.templatedViewBottomConstaint.constant = -500
        UIView.animate(withDuration: 0.5, animations: {
            self.templetView.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    func showAnimation() {
        
        self.templatedViewBottomConstaint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.templetView.layoutIfNeeded()
        }, completion: {res in
            //Do something
        })
    }
    
    @IBAction func swipeGestureTapped(_ sender: Any) {
        reduceButtonAction(nil)
    }
    
    @IBAction func viewPointsMatchWiseButtonTapped(_ sender: Any) {
        removeFromSuperview()
        goToPlayerStatsViewController()
    }

    
    func goToPlayerStatsViewController()  {
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let playerStatsVC = storyboard.instantiateViewController(withIdentifier: "PlayerStatsViewController") as! PlayerStatsViewController
           
           playerStatsVC.totalPlayerInfoArray = playerListArray
           playerStatsVC.selectedMathDetails = selectedMathDetails
           playerStatsVC.selectedLeagueType = FantasyType.Classic.rawValue
           playerStatsVC.selectedPlayerDetails = playerDetails
           playerStatsVC.gameType = selectedGameType;
          
           playerStatsVC.isLineupsAnnounced = false

           if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(playerStatsVC, animated: true)
           }
    }
}
