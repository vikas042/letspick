//
//  LetspickFantasyHeaderView.swift
//  Letspick
//
//  Created by Vikash Rajput on 15/05/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LetspickFantasyHeaderView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var firstMatchlabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var firstTeamNameTopCostraint: NSLayoutConstraint!
    @IBOutlet weak var bottomSepratorLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblFirstTeamName: UILabel!
    @IBOutlet weak var lblSecondTeamName: UILabel!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var lblMatchRemaingTime: UILabel!
    @IBOutlet weak var bottemSeparatorView: UIView!

    @IBOutlet weak var lblVs: UILabel!

    @IBOutlet weak var collectionView: UICollectionView!
    var isFantasyModeEnable = true
    
    @IBOutlet weak var sepratorViewWidthConstraint: NSLayoutConstraint!
    var classicBlock = {(sucess: Bool) -> () in }
    
    func clasicButtonTapped(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        classicBlock = complationBlock
    }

    var battingBlock = {(sucess: Bool) -> () in }
    
    func battingButtonTapped(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        battingBlock = complationBlock
    }

    var bowlingBlock = {(sucess: Bool) -> () in }
    
    func bowlingButtonTapped(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        bowlingBlock = complationBlock
    }
    
    var reverseFantasyBlock = {(sucess: Bool) -> () in }
    func reverseFantasyButtonTapped(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        reverseFantasyBlock = complationBlock
    }
    
    var wizardFantasyBlock = {(sucess: Bool) -> () in }
    func wizardFantasyButtonTapped(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        wizardFantasyBlock = complationBlock
    }
        
    lazy var firstTabFantasyType = FantasyType.Classic.rawValue;
    lazy var secondTabFantasyType = FantasyType.Batting.rawValue;
    lazy var thirdTabFantasyType = FantasyType.Bowling.rawValue;
    lazy var fourthTabFantasyType = FantasyType.Reverse.rawValue;
    lazy var fifthTabFantasyType = FantasyType.Wizard.rawValue;
    lazy var tabsArray = Array<Int>()

    lazy var selectedFantasyType = FantasyType.Classic.rawValue;

    lazy var matchFantasyType = ""
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("LetspickFantasyHeaderView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            updateWalletAmount()
            addSubview(view)
            collectionView.register(UINib(nibName: "GameTabCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GameTabCollectionViewCell")
        }
    }
    
    func updateTabsData() {
         
         (firstTabFantasyType, secondTabFantasyType, thirdTabFantasyType, fourthTabFantasyType, fifthTabFantasyType) = AppHelper.getFantasyLandingOrder(fantasyType: matchFantasyType)
        
         selectedFantasyType = firstTabFantasyType
        tabsArray.removeAll()
         if firstTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(firstTabFantasyType)
         }
         
         if secondTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(secondTabFantasyType)
         }

         if thirdTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(thirdTabFantasyType)
         }
         
         if fourthTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(fourthTabFantasyType)
         }
         
         if fifthTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(fifthTabFantasyType)
         }
        if tabsArray.count == 0 {
            tabsArray.append(FantasyType.Classic.rawValue)
        }
         let sepratorWidth = Int(UIScreen.main.bounds.width)/tabsArray.count
         sepratorViewWidthConstraint.constant = CGFloat(sepratorWidth)
        self.layoutIfNeeded()
        bottemSeparatorView.isHidden = true
        if tabsArray.count > 1 {
            bottemSeparatorView.isHidden = false
        }
    }

    @IBAction func walletButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "WalletIconClicked", info: nil)

        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let walletVC = storyboard.instantiateViewController(withIdentifier: "WalletViewController")
            navVC.pushViewController(walletVC, animated: true)
        }
    }

    func classicButtonTapped(isNeedToScroll: Bool) {
        
        selectedFantasyType = FantasyType.Classic.rawValue
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
            }
            else if tabsArray.count == 4{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
              bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
               bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }


        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        
        if isNeedToScroll {
            classicBlock(true)
        }
        else{
            classicBlock(false)
        }

        collectionView.reloadData()
    }
    
//    func classicButtonTapped(isNeedToScroll: Bool) {
//        showClassicTabSelected()
//
//        if isNeedToScroll {
//            classicBlock(true)
//        }
//        else{
//            classicBlock(false)
//        }
//    }

//    func battingButtonTapped(isNeedToScroll: Bool) {
//        showBattingTabSelected()
//        if isNeedToScroll {
//            battingBlock(true)
//        }
//        else{
//            battingBlock(false)
//        }
//    }
    
     
    func battingButtonTapped(isNeedToScroll: Bool) {
        
        selectedFantasyType = FantasyType.Batting.rawValue
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
            }
            else if tabsArray.count == 4{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
              bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
               bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }

        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }

        if isNeedToScroll {
            battingBlock(true)
        }
        else{
            battingBlock(false)
        }

        collectionView.reloadData()
    }

//    func bowlingButtonTapped(isNeedToScroll: Bool) {
//        showBowlingTabSelected();
//        if isNeedToScroll {
//            bowlingBlock(true)
//        }
//        else{
//            bowlingBlock(false)
//        }
//    }
    
    
    
    func bowlingButtonTapped(isNeedToScroll: Bool) {

        selectedFantasyType = FantasyType.Bowling.rawValue
        
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
                        
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }

        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }

        collectionView.reloadData()
        
        if isNeedToScroll {
            bowlingBlock(true)
        }
        else{
            bowlingBlock(false)
        }
    }

    func reverseFantasyButtonTapped(isNeedToScroll: Bool) {

        selectedFantasyType = FantasyType.Reverse.rawValue
        
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
                        
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }

        if isNeedToScroll {
            reverseFantasyBlock(true)
        }
        else{
            reverseFantasyBlock(false)
        }

        collectionView.reloadData()
    }

    
//    func reverseFantasyButtonTapped(isNeedToScroll: Bool) {
//        showBowlingTabSelected();
//        if isNeedToScroll {
//            reverseFantasyBlock(true)
//        }
//        else{
//            reverseFantasyBlock(false)
//        }
//    }

    
    func wizardFantasyButtonTapped(isNeedToScroll: Bool) {
        selectedFantasyType = FantasyType.Wizard.rawValue
        
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
                        
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        
        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }

        if isNeedToScroll {
            wizardFantasyBlock(true)
        }
        else{
            wizardFantasyBlock(false)
        }

        collectionView.reloadData()
    }
    
//    func wizardFantasyButtonTapped(isNeedToScroll: Bool) {
//        showBowlingTabSelected();
//        if isNeedToScroll {
//            wizardFantasyBlock(true)
//        }
//        else{
//            wizardFantasyBlock(false)
//        }
//    }

    @IBAction func backButtonTapped(_ sender: Any?) {
        
        if self.tag == 5000 {
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.popToRootViewController(animated: true)
            }
        }
        else if self.tag == 1001 {
            AppHelper.makeDashbaordAsRootViewController()
        }
        else{
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.popViewController(animated: true)
            }
        }
    }
    
    func updateWalletAmount()  {
        
        lblNotificationCount.text = UserDetails.sharedInstance.notificationCount
        if (UserDetails.sharedInstance.notificationCount.count == 0) || (UserDetails.sharedInstance.notificationCount == "0") {
            lblNotificationCount.isHidden  = true
        }
        else{
            lblNotificationCount.isHidden  = false
        }
        
        if UserDetails.sharedInstance.notificationCount.count > 1{
            lblNotificationCount.text = "9+"
        }
    }
    
    func updateMatchName(matchDetails: MatchDetails?) {
        guard let details = matchDetails else {
            lblMatchRemaingTime.text = ""
            lblFirstTeamName.text = ""
            lblSecondTeamName.text = ""
            return;
        }
        
        lblFirstTeamName.text = details.firstTeamShortName
        lblSecondTeamName.text = details.secondTeamShortName
        lblVs.isHidden = false
        if details.isMatchTourney {
            lblFirstTeamName.text = details.matchShortName
            lblSecondTeamName.text = ""
            lblVs.isHidden = true
        }

        if details.isMatchClosed  {
            lblMatchRemaingTime.text = "Leagues Closed".localized()
        }
        else{
            lblMatchRemaingTime.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: details.startDateTimestemp)
        }
    }
    
    @IBAction func notificationButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "NotificationClicked", info: nil)

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let notificationVC = storyboard.instantiateViewController(withIdentifier: "NotificationsViewController")
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(notificationVC, animated: true)
        }
    }
    
    func updateData(details: MatchDetails?) {
        if !isFantasyModeEnable {
            collectionView.isHidden = true
            bottemSeparatorView.isHidden = true
        }
        
        lblFirstTeamName.text = details?.firstTeamShortName
        lblSecondTeamName.text = details?.secondTeamShortName
        
        lblVs.isHidden = false
        if details?.isMatchTourney ?? false{
            lblFirstTeamName.text = details?.matchShortName
            lblSecondTeamName.text = ""
            lblVs.isHidden = true
        }
        else if details?.isQuizMatch ?? false{
            lblFirstTeamName.text = details?.matchShortName
            lblSecondTeamName.text = ""
            firstMatchlabelWidthConstraint.constant = UIScreen.main.bounds.width - 130
            firstTeamNameTopCostraint.constant = 15
            self.layoutIfNeeded()
            lblVs.isHidden = true
            let startDateIndia = Double(details!.startDateIndia) ?? 0
            let startDateTimestemp = Double(details!.startDateTimestemp ?? "0") ?? 0
            let serverTimeStemp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
            
            if startDateIndia > 0 {
                if startDateTimestemp < serverTimeStemp {
                    lblMatchRemaingTime.text  = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: String(startDateTimestemp + details!.closingTs * 60.0))
                }
            }
        }
        
        collectionView.reloadData()
    }
    
    // MARK:- Timer Handlers
    func updateTimerValue(matchDetails: MatchDetails)  {
        
        if matchDetails.isMatchClosed  {
            lblMatchRemaingTime.text = "Leagues Closed".localized()
        }
        else if UInt64((matchDetails.startDateTimestemp)!) != nil{
            lblMatchRemaingTime.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: matchDetails.startDateTimestemp)
        }
    }
        
    func callNotificationAPI(isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let params = ["option": "notifications", "user_id": UserDetails.sharedInstance.userID, "last_id": "0"]

        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                if let response = result!["response"]?.dictionary{
                    if let responseArray = response["notifications"]?.array{
                        UserDetails.sharedInstance.notificationCount = ""
                        let tempArray = NotificationsDetails.getAllNotificationList(responseArray: responseArray)
                        if tempArray.count > 0{
                            let totalWinner = NotificationsView(frame: APPDELEGATE.window!.frame)
                            totalWinner.notificationArray = tempArray
                            totalWinner.setupData()
                            APPDELEGATE.window!.addSubview(totalWinner)
                            totalWinner.showAnimation()
                            UserDetails.sharedInstance.notificationCount = ""
                            self.updateWalletAmount()
                        }
                        else{
                            AppHelper.showAlertView(message: "You don't have any notification right now.", isErrorMessage: true)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: "You don't have any notification right now.", isErrorMessage: true)
                    }
                }
            }
            
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    //MARK:- CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if tabsArray.count > 1 {
            return tabsArray.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = UIScreen.main.bounds.width/CGFloat(tabsArray.count)
        return CGSize(width: cellWidth, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GameTabCollectionViewCell", for: indexPath) as! GameTabCollectionViewCell
        let fantasyType = tabsArray[indexPath.row]
        cell.lineUpsView.isHidden = true;
        
        let cellWidth = Int(UIScreen.main.bounds.width)/tabsArray.count
        
        if cellWidth <= 55 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 10)
        }
        else if cellWidth <= 64 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 11)
        }
        else if cellWidth <= 69 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 12)
        }
        else if cellWidth <= 75 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 13)
        }

        if fantasyType == FantasyType.Classic.rawValue {
            cell.lblTitle.text = "classic".localized()
        }
        else if fantasyType == FantasyType.Batting.rawValue {
            cell.lblTitle.text = "batting".localized()
        }
        else if fantasyType == FantasyType.Bowling.rawValue {
            cell.lblTitle.text = "bowling".localized()
        }
        else if fantasyType == FantasyType.Reverse.rawValue {
            cell.lblTitle.text = "Reverse".localized()
        }
        else if fantasyType == FantasyType.Wizard.rawValue {
            cell.lblTitle.text = "Wizard".localized()
        }
        else{
            cell.lblTitle.text = ""
        }
        
        cell.lblTitle.textColor = UIColor.white.withAlphaComponent(0.6)

        if fantasyType == selectedFantasyType {
            cell.lblTitle.textColor = UIColor.white.withAlphaComponent(1.0)
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let fantasyType = tabsArray[indexPath.row]
        selectedFantasyType = fantasyType;
        
        if fantasyType == FantasyType.Classic.rawValue {
            classicButtonTapped(isNeedToScroll: true)
        }
        else if fantasyType == FantasyType.Batting.rawValue {
            battingButtonTapped(isNeedToScroll: true)
        }
        else if fantasyType == FantasyType.Bowling.rawValue {
            bowlingButtonTapped(isNeedToScroll: true)
        }
        else if fantasyType == FantasyType.Reverse.rawValue {
            reverseFantasyButtonTapped(isNeedToScroll: true)
        }
        else if fantasyType == FantasyType.Wizard.rawValue {
            wizardFantasyButtonTapped(isNeedToScroll: true)
        }
    }

}

