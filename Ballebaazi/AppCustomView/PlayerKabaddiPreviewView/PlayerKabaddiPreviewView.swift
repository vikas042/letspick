//
//  PlayerKabaddiPreviewView.swift
//  Letspick
//
//  Created by Vikash Rajput on 05/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit


let kKabaddiTeam1Color = UIColor(red: 216.0/255, green: 148.0/255, blue: 26.0/255, alpha: 1)
let kKabaddiTeam2Color = UIColor(red: 202.0/255, green: 87.0/255, blue: 58.0/255, alpha: 1)


class PlayerKabaddiPreviewView: UIView {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCaptainRole: UILabel!
    
    var playerDetails: PlayerDetails?
    var fantasyType = ""
    var matchClosed = false
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("PlayerKabaddiPreviewView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            lblCaptainRole.layer.borderColor = lblCaptainRole.textColor.cgColor
            lblCaptainRole.layer.borderWidth = 1.0
            addSubview(view)
        }
    }
    
    func showPlayerInformation(details: PlayerDetails, legueType: Int, isMatchClosed: Bool, firstTeamName: String) -> Float {
        var totalPoints: Float = 0.0
        matchClosed = isMatchClosed
        
        if let url = NSURL(string: details.imgURL){
            imgUser.setImage(with: url as URL, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                 self?.imgUser.image = image
                }
                else{
                    self?.imgUser.image = UIImage(named: details.playerPlaceholder)
                }
            })
        }
        else{
            self.imgUser.image = UIImage(named: details.playerPlaceholder)
        }

        
        if firstTeamName == details.teamShortName{
            lblName.backgroundColor = kKabaddiTeam2Color
        }
        else if firstTeamName == details.teamKey{
            lblName.backgroundColor = kKabaddiTeam2Color
        }
        else{
            lblName.backgroundColor = kKabaddiTeam1Color
        }
        
        playerDetails = details
        let playerNameArray = playerDetails!.playerName?.components(separatedBy: " ")
        lblName.text = playerDetails!.playerName
        var nameplayerNameArray = ""
        
        if playerNameArray != nil {
            if playerNameArray!.count > 1 {
                let name = playerNameArray![0]
                if name.count > 2{
                    nameplayerNameArray = String(name.prefix(1))
                    for i in 1 ..< playerNameArray!.count{
                        nameplayerNameArray = nameplayerNameArray + " " +  playerNameArray![i]
                    }
                }
            }
        }
        
        if nameplayerNameArray.count > 0 {
            lblName.text = nameplayerNameArray
        }
        lblCaptainRole.isHidden = true
        if details.isCaption{
            lblCaptainRole.text = "C"
            lblCaptainRole.isHidden = false
        }
        else if details.isViceCaption{
            lblCaptainRole.text = "VC"
            lblCaptainRole.isHidden = false
        }
        
        if legueType == FantasyType.Classic.rawValue {
            fantasyType = "1"
            if !isMatchClosed{
                lblPoints.text = details.credits + " CR"
                totalPoints = totalPoints + (Float(details.credits) ?? 0)
            }
            else{
                if details.isCaption{
                    lblPoints.text = details.totalClassicCaptainScore + " Pts"
                    totalPoints = totalPoints + (Float(details.totalClassicCaptainScore) ?? 0)
                    
                }
                else if details.isViceCaption{
                    lblPoints.text = details.totalClassicViceCaptainScore + " Pts"
                    totalPoints = totalPoints + (Float(details.totalClassicViceCaptainScore) ?? 0)
                }
                else{
                    lblPoints.text = details.totalClasscPlayerScore + " Pts"
                    totalPoints = totalPoints + (Float(details.totalClasscPlayerScore) ?? 0)
                }
            }
        }
        else if legueType == FantasyType.Batting.rawValue{
            fantasyType = "2"
            if !isMatchClosed{
                lblPoints.text = details.credits + " CR"
                totalPoints = totalPoints + (Float(details.credits) ?? 0)
            }
            else{
                if details.isCaption{
                    lblPoints.text = details.totalBattingCaptainScore + " Pts"
                    totalPoints = totalPoints + (Float(details.totalBattingCaptainScore) ?? 0)
                    
                }
                else if details.isViceCaption{
                    lblPoints.text = details.totalBattingViceCaptainScore + " Pts"
                    totalPoints = totalPoints + (Float(details.totalBattingViceCaptainScore) ?? 0)
                }
                else{
                    lblPoints.text = details.totalBattingPlayerScore + " Pts"
                    totalPoints = totalPoints + (Float(details.totalBattingPlayerScore) ?? 0)
                }
                
            }
        }
        else if legueType == FantasyType.Bowling.rawValue{
            fantasyType = "3"
            if !isMatchClosed{
                lblPoints.text = details.credits + " CR"
                totalPoints = totalPoints + (Float(details.credits) ?? 0)
            }
            else{
                if details.isCaption{
                    lblPoints.text = details.totalBowlingCaptainScore + " Pts"
                    totalPoints = totalPoints + (Float(details.totalBowlingCaptainScore) ?? 0)
                }
                else if details.isViceCaption{
                    lblPoints.text = details.totalBowlingViceCaptainScore + " Pts"
                    totalPoints = totalPoints + (Float(details.totalBowlingViceCaptainScore) ?? 0)
                }
                else{
                    lblPoints.text = details.totalBowlingPlayerScore + " Pts"
                    totalPoints = totalPoints + (Float(details.totalBowlingPlayerScore) ?? 0)
                }
            }
        }
        return totalPoints
    }
    
    @IBAction func playerInfoButtonTapped(_ sender: Any) {

        guard let details = playerDetails else {
            return;
        }
        if !matchClosed {
            return
        }

        let teamScoresView = KabaddiPointsView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
        teamScoresView.configData(details: details)
        APPDELEGATE.window!.addSubview(teamScoresView)
        teamScoresView.showAnimation()

    }
}
