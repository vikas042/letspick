//
//  BannerView.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/03/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class BannerView: UIView {

    @IBOutlet weak var gotoButton: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var bannerDetails: BannerDetails?
    var filePath = ""
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("BannerView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.65)
            self.backgroundColor = UIColor.clear
            
            gotoButton.layer.cornerRadius = 17;
            gotoButton.layer.shadowColor = UIColor(red: 23.0/255, green: 37.0/255, blue: 42.0/255, alpha: 0.5).cgColor
            gotoButton.layer.shadowOffset = CGSize(width: 0, height: 0)
            gotoButton.layer.shadowOpacity = 0.33
            gotoButton.layer.shadowRadius = 3.0
            gotoButton.layer.masksToBounds = false

            addSubview(view)
        }
    }
    
    func updateData() {
        
        guard let details = bannerDetails else {
            return;
        }
        
//        gotoButton.isHidden = false
        gotoButton.setTitle(details.buttonTxt, for: .normal)

        if (details.redirectType == "1") || (details.redirectType == "2") || (details.redirectType == "3") || (details.redirectType == "4") || (details.redirectType == "5") || (details.redirectType == "6") || (details.redirectType == "7") || (details.redirectType == "8") || (details.redirectType == "9") || (details.redirectType == "10") || (details.redirectType == "11") || (details.redirectType == "12")  {
            gotoButton.isHidden = false
        }
        else{
            gotoButton.isHidden = true
        }
        
        activityIndicator.startAnimating()
        if let url = NSURL(string: filePath + details.image){
            imgView.setImage(with: url as URL, placeholder: nil, progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                if (image != nil){
                    self?.imgView.image = image
                    let imageSize = image!.size
                    let imageRation = imageSize.height/imageSize.width
                    let viewHeight = imageRation * (UIScreen.main.bounds.width - 66)
                    DispatchQueue.main.async {
                        self?.heightConstraint.constant = viewHeight
                        self?.layoutIfNeeded()
                    }
                }
            })
        }
    }
    
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        removeFromSuperview()
    }
    
    @IBAction func bannerButtonClicked(_ sender: Any) {
        guard let details = bannerDetails else {
            return;
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            
            var sportType = ""

            if details.gameType == "1" {
                sportType = "Cricket"
            }
            else if details.gameType == "2" {
                sportType = "Kabaddi"
            }
            else if details.gameType == "3" {
                sportType = "Football"
            }
            else if details.gameType == "4" {
                sportType = "Quiz"
            }
            else if details.gameType == "5" {
                sportType = "Basketball"
            }
            else if details.gameType == "6" {
                sportType = "Baseball"
            }

            AppxorEventHandler.logAppEvent(withName: "BannerClicked", info: ["BannerRedirectionType": details.redirectType, "SportType": sportType, "BannerName": details.title])
        
            if details.redirectType == "1" {
                if details.gameType == "1" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "2" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "3" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "4" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "QuizLeaguesViewController") as! QuizLeaguesViewController
                    let matchDetails = MatchDetails()
                    matchDetails.matchKey = details.matchKey
                    matchDetails.isQuizMatch = true
                    leagueVC.matchDetails = matchDetails
                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                        navVC.pushViewController(leagueVC, animated: true)
                    }
                }
                else if details.gameType == "5" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as! BasketballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "6" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BaseballLeagueViewController") as! BaseballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else if details.redirectType == "2" {
                let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
                navVC.pushViewController(leagueVC, animated: true)
            }
            else if details.redirectType == "3" {
                
                if details.websiteUrl.count == 0{
                    return;
                }
                let webviewVC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webviewVC.urlString = details.websiteUrl
                navVC.pushViewController(webviewVC, animated: true)
            }
            else if details.redirectType == "4" {
                let playerView = YoutubeVideoPlayerView(frame: APPDELEGATE.window!.frame)
                playerView.videoID = details.videoUrl
                playerView.playView()
                APPDELEGATE.window!.addSubview(playerView)
            }
            else if details.redirectType == "5" {
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                    if let dashboardVC = navVC.viewControllers.first as? DashboardViewController{
                        if details.redirectSportType == "1"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Cricket.rawValue)
                        }
                        else if details.redirectSportType == "2"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Kabaddi.rawValue)
                        }
                        else if details.redirectSportType == "3"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Football.rawValue)
                        }
                        else if details.redirectSportType == "4"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Quiz.rawValue)
                        }
                        else if details.redirectSportType == "5"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Basketball.rawValue)
                        }
                        else if details.redirectSportType == "6"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Baseball.rawValue)
                        }
                    }
                }
            }
            else if details.redirectType == "6" {
                let promoCodeVC = storyboard.instantiateViewController(withIdentifier: "PromotionsViewController") as! PromotionsViewController
                promoCodeVC.isFromSetting = true
                navVC.pushViewController(promoCodeVC, animated: true)
            }
            else if details.redirectType == "7" {
                let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
                navVC.pushViewController(addCashVC, animated: true)
            }
            else if details.redirectType == "8" {
                
                let howToPlayVC = storyboard.instantiateViewController(withIdentifier: "HowToPlayViewController") as! HowToPlayViewController
                navVC.pushViewController(howToPlayVC, animated: true)
            }
            else if details.redirectType == "9" {

                let becomePartnerVC = storyboard.instantiateViewController(withIdentifier: "BecomePartnerViewController") as! BecomePartnerViewController
                navVC.pushViewController(becomePartnerVC, animated: true)
            }
            else if details.redirectType == "10" {
                // Batting fantasy
                if details.gameType == "1" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.matchKey = details.matchKey
                    leagueVC.selectedFantasy = FantasyType.Batting.rawValue
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "2" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "3" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "5" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as! BasketballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "6" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BaseballLeagueViewController") as! BaseballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else if details.redirectType == "11" {
                // Bowling fantasy
                if details.gameType == "1" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.matchKey = details.matchKey
                    leagueVC.selectedFantasy = FantasyType.Bowling.rawValue
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "2" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "3" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "5" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as! BasketballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "6" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BaseballLeagueViewController") as! BaseballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else if details.redirectType == "12" {
                let rewardVC = storyboard.instantiateViewController(withIdentifier: "RewardStoreViewController") as! RewardStoreViewController
                rewardVC.isFromTabBar = false
                navVC.pushViewController(rewardVC, animated: true)
            }
            else if details.redirectType == "13" {
                let sessionVC = storyboard.instantiateViewController(withIdentifier: "SessionPassesViewController")
                navVC.pushViewController(sessionVC, animated: true)
            }

        }
        
        removeFromSuperview()

    }

}
