//
//  TotalWinnersView.swift
//  Letspick
//
//  Created by Vikash Rajput on 12/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class NotificationsView: UIView, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    @IBOutlet weak var lblNotifications: UILabel!
    
    @IBOutlet weak var contentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentHeightViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    lazy var notificationArray = [NotificationsDetails]()
    var isNeedToShowLoadMore = true
    var viewHeightConstraint: CGFloat = 0;
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setupData() {
        tblView.register(UINib(nibName: "NotificationsTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationsTableViewCell")
        tblView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        lblNotifications.text = "Notifications".localized();
        tblView.tableFooterView = UIView()
        tblView.estimatedRowHeight = 50
        tblView.rowHeight = UITableViewAutomaticDimension
        var viewHeight: CGFloat = CGFloat(notificationArray.count * 95) + 62
        
        if viewHeight > UIScreen.main.bounds.height - 120 {
            viewHeight = UIScreen.main.bounds.height - 120
        }
        
        contentHeightViewConstraint.constant = viewHeight
        viewHeightConstraint = viewHeight + 10;
        self.contentViewBottomConstraint.constant = -viewHeightConstraint
        self.layoutIfNeeded()
        tblView.reloadData()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("NotificationsView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            contentView.layer.cornerRadius = 10.0
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }

    
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if notificationArray.count == indexPath.row{
            var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            if isNeedToShowLoadMore{
                isNeedToShowLoadMore = false
                callNotificationAPI(isNeedToShowLoader: false)
            }
            
            return cell!
            
        }else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell") as? NotificationsTableViewCell
            if cell == nil {
                cell = NotificationsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "NotificationsTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            let notificationDetails = notificationArray[indexPath.row]
            cell?.lblTitle.text = notificationDetails.notificationTitle
            cell?.lblNotificationMessage.text = notificationDetails.notificationMessage.htmlToString
            cell?.lblDate.text = AppHelper.getNotificationFormattedDate(dateString: notificationDetails.notificationDate)
            return cell!
        }
    }

    func showAnimation() {

        self.contentViewBottomConstraint.constant = -10
        UIView.animate(withDuration: 0.5, animations: {
            self.contentView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
            self.layoutIfNeeded()
        })
    }

    @IBAction func closedButtonTapped(_ sender: Any?) {
        self.contentViewBottomConstraint.constant = -viewHeightConstraint
        UIView.animate(withDuration: 0.5, animations: {
            self.contentView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    @IBAction func swipeGestureTapped(_ sender: Any) {
        closedButtonTapped(nil)
    }
    
    func callNotificationAPI(isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        var notificationID = "0"
        
        if let details = notificationArray.last{
            notificationID = details.notificationId
        }
        
        let params = ["option": "notifications", "user_id": UserDetails.sharedInstance.userID, "last_id": notificationID]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                if let response = result!["response"]?.dictionary{
                    if let responseArray = response["notifications"]?.array{
                        weakSelf?.notificationArray = weakSelf!.notificationArray + NotificationsDetails.getAllNotificationList(responseArray: responseArray)
                        
                        if responseArray.count == 0{
                            weakSelf?.isNeedToShowLoadMore = false
                        }
                        else{
                            weakSelf?.isNeedToShowLoadMore = true
                        }
                        weakSelf?.tblView.reloadData()
                    }
                }
            }
            
            AppHelper.sharedInstance.removeSpinner()
        }
    }
}
