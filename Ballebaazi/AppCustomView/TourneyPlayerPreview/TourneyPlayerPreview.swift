//
//  TourneyPlayerPreview.swift
//  Letspick
//
//  Created by Vikash Rajput on 01/04/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class TourneyPlayerPreview: UIView {

    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var announceView: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCaptainRole: UILabel!
    
    var playerDetails: PlayerDetails?
    var fantasyType = ""
    var matchClosed = false
    var matchDetails: MatchDetails?

    var playerListArray = Array<PlayerDetails>()
    var selectedGameType = GameType.Cricket.rawValue

    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("TourneyPlayerPreview", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            addSubview(view)
        }
    }
    
    func showPlayerInformation(details: PlayerDetails, legueType: Int, isMatchClosed: Bool, firstTeamkey: String) -> Float {
        
        var totalPoints: Float = 0.0
        matchClosed = isMatchClosed

        
        if let url = NSURL(string: details.imgURL){
            imgUser.setImage(with: url as URL, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                 self?.imgUser.image = image
                }
                else{
                    self?.imgUser.image = UIImage(named: details.playerPlaceholder)
                }
            })
        }
        else{
            self.imgUser.image = UIImage(named: details.playerPlaceholder)
        }
        
        announceView.isHidden = true
        if details.isPlayerPlaying{
            announceView.isHidden = false
        }
        
        if firstTeamkey == details.teamKey{
            lblName.backgroundColor = kTeam1Color
        }
        else{
            lblName.backgroundColor = kTeam2Color
        }
        
        playerDetails = details
        lblTeamName.text = details.teamShortName

        let playerNameArray = playerDetails!.playerName?.components(separatedBy: " ")
        lblName.text = playerDetails!.playerName
        var nameplayerNameArray = ""
        
        if playerNameArray != nil {
            if playerNameArray!.count > 1 {
                let name = playerNameArray![0]
                if name.count > 2{
                    nameplayerNameArray = String(name.prefix(1))
                    for i in 1 ..< playerNameArray!.count{
                        nameplayerNameArray = nameplayerNameArray + " " +  playerNameArray![i]
                    }
                }
            }
        }
        
        if nameplayerNameArray.count > 0 {
            lblName.text = nameplayerNameArray
        }
        lblCaptainRole.isHidden = true
        if details.isCaption{
            lblCaptainRole.text = "C"
            lblCaptainRole.isHidden = false
        }
        else if details.isViceCaption{
            lblCaptainRole.text = "VC"
            lblCaptainRole.isHidden = false
        }
        
        if legueType == FantasyType.Classic.rawValue {
            fantasyType = "1"
            if !isMatchClosed{
                lblPoints.text = details.credits + " CR"
                totalPoints = totalPoints + (Float(details.credits) ?? 0)
            }
            else{
                if details.isCaption{
                    lblPoints.text = details.totalClassicCaptainScore + ""
                    totalPoints = totalPoints + (Float(details.totalClassicCaptainScore) ?? 0)

                }
                else if details.isViceCaption{
                    lblPoints.text = details.totalClassicViceCaptainScore + ""
                    totalPoints = totalPoints + (Float(details.totalClassicViceCaptainScore) ?? 0)
                }
                else{
                    lblPoints.text = details.totalClasscPlayerScore + ""
                    totalPoints = totalPoints + (Float(details.totalClasscPlayerScore) ?? 0)
                }
            }
        }
        else if legueType == FantasyType.Batting.rawValue{
            fantasyType = "2"
            if !isMatchClosed{
                lblPoints.text = details.credits + " CR"
                totalPoints = totalPoints + (Float(details.credits) ?? 0)
            }
            else{
                if details.isCaption{
                    lblPoints.text = details.totalBattingCaptainScore + ""
                    totalPoints = totalPoints + (Float(details.totalBattingCaptainScore) ?? 0)
                    
                }
                else if details.isViceCaption{
                    lblPoints.text = details.totalBattingViceCaptainScore + ""
                    totalPoints = totalPoints + (Float(details.totalBattingViceCaptainScore) ?? 0)
                }
                else{
                    lblPoints.text = details.totalBattingPlayerScore + ""
                    totalPoints = totalPoints + (Float(details.totalBattingPlayerScore) ?? 0)
                }
                
            }
        }
        else if legueType == FantasyType.Bowling.rawValue{
            fantasyType = "3"
            if !isMatchClosed{
                lblPoints.text = details.credits + " CR"
                totalPoints = totalPoints + (Float(details.credits) ?? 0)
            }
            else{
                if details.isCaption{
                    lblPoints.text = details.totalBowlingCaptainScore + ""
                    totalPoints = totalPoints + (Float(details.totalBowlingCaptainScore) ?? 0)
                }
                else if details.isViceCaption{
                    lblPoints.text = details.totalBowlingViceCaptainScore + ""
                    totalPoints = totalPoints + (Float(details.totalBowlingViceCaptainScore) ?? 0)
                }
                else{
                    lblPoints.text = details.totalBowlingPlayerScore + ""
                    totalPoints = totalPoints + (Float(details.totalBowlingPlayerScore) ?? 0)
                }
            }
        }
        return totalPoints
    }

    @IBAction func playerInfoButtonTapped(_ sender: Any) {
        
        guard let details = playerDetails else {
            return;
        }
        if !matchClosed {
            return
        }
        
        //if (fantasyType == "1") || (fantasyType == "4") || (fantasyType == "5"){
        if (fantasyType == "1") || (fantasyType == "5"){
            let teamScoresView = ClassicTeamScoresView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
            teamScoresView.configData(details: details, matchDetails: matchDetails)
            
            teamScoresView.playerListArray = playerListArray
            teamScoresView.selectedGameType = selectedGameType
            teamScoresView.selectedMathDetails = matchDetails
            teamScoresView.playerDetails = playerDetails
            
            APPDELEGATE.window!.addSubview(teamScoresView)
            teamScoresView.showAnimation()
        }
        else if fantasyType == "2"{
            let teamScoresView = BattingTeamScoreView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
            teamScoresView.configData(details: details, matchDetails: matchDetails)
            teamScoresView.playerListArray = playerListArray
            teamScoresView.selectedGameType = selectedGameType
            teamScoresView.selectedMathDetails = matchDetails
            teamScoresView.playerDetails = playerDetails

            APPDELEGATE.window!.addSubview(teamScoresView)
            teamScoresView.showAnimation()
        }
        else if fantasyType == "3"{
            let teamScoresView = BowlingTeamScoreView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
            teamScoresView.configData(details: details, matchDetails: matchDetails)
            teamScoresView.playerListArray = playerListArray
            teamScoresView.selectedGameType = selectedGameType
            teamScoresView.selectedMathDetails = matchDetails
            teamScoresView.playerDetails = playerDetails

            
            APPDELEGATE.window!.addSubview(teamScoresView)
            teamScoresView.showAnimation()
        }else if fantasyType == "4"{
            let teamScoresView = TeamScoreInstructionView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
            teamScoresView.selectedFantasyType = FantasyType.Reverse.rawValue
            teamScoresView.configData(details: details, matchDetails: matchDetails)
            APPDELEGATE.window!.addSubview(teamScoresView)
            teamScoresView.showAnimation()
        }
    }
}
