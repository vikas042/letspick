
//
//  LeaderboardPriceView.swift
//  Letspick
//
//  Created by Vikash Rajput on 11/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LeaderboardPriceView: UIView, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bottomSepratorLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var bowlingButton: UIButton!
    @IBOutlet weak var classicButton: UIButton!
    @IBOutlet weak var battingButton: UIButton!
    @IBOutlet weak var bottemSeparatorView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var containerViewBottomConstraint: NSLayoutConstraint!
    
    var imagesArray = Array<String>()
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("LeaderboardPriceView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            AppHelper.showShodowOnCellsView(innerView: contentView);
            contentView.layer.cornerRadius = 10.0
            contentView.clipsToBounds = true
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            classicButton.setTitle("classic".localized(), for: .normal)
            battingButton.setTitle("batting".localized(), for: .normal)
            bowlingButton.setTitle("bowling".localized(), for: .normal)

            addSubview(view)
        }
    }
    
    func setupDefaultData(filePath:String, leaderboardDetails: Leaderboards?) {
        if leaderboardDetails != nil {
            imagesArray.append(filePath + leaderboardDetails!.classicImage)
            imagesArray.append(filePath + leaderboardDetails!.battingImage)
            imagesArray.append(filePath + leaderboardDetails!.bowlingImage)
        }
        lblTitle.text = leaderboardDetails?.title;
        collectionView.register(UINib(nibName: "FullImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FullImageCollectionViewCell")
    }
    
    @IBAction func classicButtonTapped(_ sender: Any?) {
        showClassicTabSelected()
        let indexPath = IndexPath(item: 0, section: 0)
        weak var weakSelf = self
        
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if sender != nil{
                    weakSelf!.collectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf!.collectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf!.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func battingButtonTapped(_ sender: Any?) {
        showBattingTabSelected()
        
        let indexPath = IndexPath(item: 1, section: 0)
        weak var weakSelf = self
        
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if sender != nil{
                    weakSelf?.collectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.collectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func bowlingButtonTapped(_ sender: Any?) {
        showBowlingTabSelected()
        let indexPath = IndexPath(item: 2, section: 0)
        weak var weakSelf = self
        
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if sender != nil{
                    weakSelf?.collectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.collectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.collectionView.reloadData()
            }
        }
    }
    
    func showClassicTabSelected() {
        classicButton.setTitleColor(UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1), for: .normal)
        battingButton.setTitleColor(UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1), for: .normal)
        bowlingButton.setTitleColor(UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1), for: .normal)
        
        bottomSepratorLeadingConstraint.constant = 4
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
    func showBattingTabSelected() {
    
        classicButton.setTitleColor(UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1), for: .normal)
        battingButton.setTitleColor(UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1), for: .normal)
        bowlingButton.setTitleColor(UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1), for: .normal)
        
        bottomSepratorLeadingConstraint.constant = self.frame.size.width/3.0
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        
    }
    
    func showBowlingTabSelected() {
        classicButton.setTitleColor(UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1), for: .normal)
        battingButton.setTitleColor(UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1), for: .normal)
        bowlingButton.setTitleColor(UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1), for: .normal)
        
        bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3) * 2.0 - 3.0
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        

    }
    
    @IBAction func swipeGestureTapped(_ sender: Any) {
        closeButtonTapped(nil)
    }

    @IBAction func closeButtonTapped(_ sender: Any?) {
        self.containerViewBottomConstraint.constant = -550
        UIView.animate(withDuration: 0.5, animations: {
            self.contentView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
        
    func showAnimation() {
        
        self.containerViewBottomConstraint.constant = -20
        UIView.animate(withDuration: 0.5, animations: {
            self.contentView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
            self.layoutIfNeeded()
        })
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FullImageCollectionViewCell", for: indexPath) as? FullImageCollectionViewCell
        let imageName = imagesArray[indexPath.row]
        cell!.activityLoader.isHidden = false
        cell!.activityLoader.startAnimating()
        cell!.imgView.image = nil
        if let url = NSURL(string: imageName){
            cell!.imgView.setImage(with: url as URL, placeholder: nil, progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                DispatchQueue.main.async {
                    cell!.activityLoader.stopAnimating()
                    cell!.activityLoader.isHidden = true
                }
                if (image != nil){
                    cell!.imgView.image = image
                }
                else{
                    cell!.imgView.image = UIImage(named: "LeaderboardPlaceHolderImage")
                }
            })
        }
        else{
            cell!.activityLoader.isHidden = true
            cell!.imgView.image = UIImage(named: "LeaderboardPlaceHolderImage")
        }
        
        return cell!;
    }
    
    
    //MARK:- Scroll View Delegates
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
        if Int(currentPage) == 0{
            showClassicTabSelected()
        }
        else if Int(currentPage) == 1{
            showBattingTabSelected()
        }
        else if Int(currentPage) == 2{
            showBowlingTabSelected()
        }
    }
}
