//
//  RewardTransactionPopup.swift
//  Letspick
//
//  Created by Vikash Rajput on 04/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class RewardTransactionPopup: UIView {

    @IBOutlet weak var lblTransactionDateTitle: UILabel!
    @IBOutlet weak var lblTransactionDate: UILabel!
    @IBOutlet weak var lblTransactionType: UILabel!
    @IBOutlet weak var lblTransactionTypeTitle: UILabel!
    @IBOutlet weak var lblRedeemAgainstTitle: UILabel!
    @IBOutlet weak var lblRedeemAgainst: UILabel!
    @IBOutlet weak var lblTransactionCoinsTitle: UILabel!
    @IBOutlet weak var lblTranactionAmount: UILabel!
    @IBOutlet weak var lblTransactionDetailsTitle: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    var transactionDetails: TransactionsDetails?
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("RewardTransactionPopup", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.backgroundColor = UIColor.clear
            containerView.roundViewCorners(corners: .topRight, radius: 10)
            containerView.roundViewCorners(corners: .topRight, radius: 10)
            
            lblTransactionCoinsTitle.text = "Transaction Amount".localized();
            lblTransactionDateTitle.text = "Transaction details".localized();

            lblTransactionDateTitle.text = "Date_trans".localized()
            lblTransactionTypeTitle.text = "Transaction_Type".localized()
            lblRedeemAgainstTitle.text = "RedeemAgainst".localized()

            addSubview(view)
        }
    }
    
        
    func updateTransactionDetails() {

        guard let details = transactionDetails else {
            return
        }
        
        if details.isNegativeTranaction {
            lblTranactionAmount.text = "-" + details.bbCoins
            lblTranactionAmount.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        else{
            lblTranactionAmount.text = "+" + details.bbCoins
            lblTranactionAmount.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }

        lblRedeemAgainst.text = details.rewardName
        lblTransactionDate.text = details.transactionDate

        if details.transactionType == "1" {
              lblTransactionType.text = "LP Coins Earned".localized()
          }
          else if details.transactionType == "2" {
              lblTransactionType.text = "LP Coins Redeemed".localized()
          }
          else{
              lblTransactionType.text = details.transactionMessage
          }
    }
 
    @IBAction func tapGestureTapped(_ sender: Any) {
        hideViewWithAnimation()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        hideViewWithAnimation()
    }
    
    func hideViewWithAnimation() {
        self.bottomConstraint.constant = -420
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    func showAnimation() {
        
        self.bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
            self.layoutIfNeeded()
        })
    }
}
