//
//  AnnouncementView.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//




import UIKit

class AnnouncementView: UIView {

    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var announcementImageView: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("AnnouncementView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            imageContainerView.clipsToBounds = true
            let colorLeft = UIColor(red: 57/255, green: 59/255, blue: 158/255, alpha: 1)
            let colorRight = UIColor(red: 20/255, green: 22/255, blue: 119/255, alpha: 1)
            let colors = Colors(colorLeft: colorLeft, colorRight: colorRight)
            var backgroundLayer = colors.gl
            backgroundLayer!.frame = imageContainerView.frame
            imageContainerView.layer.insertSublayer(backgroundLayer!, at: 0)
            
            addSubview(view)
        }
    }
    
    func showAnnouncementMessage(message: String) {
        lblMessage.text = message
    }
}
