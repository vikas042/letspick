//
//  TeamScoreView.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class TeamScoreView: UIView {

    @IBOutlet weak var scorecardButtonSingle: UIButton!
    @IBOutlet weak var lblFirstTeamScore: UILabel!
    @IBOutlet weak var scoreboardButton: UIButton!
    @IBOutlet weak var lblSecondScore: UILabel!
    @IBOutlet weak var lblSecondTeamName: UILabel!
    @IBOutlet weak var lblFirstTeamName: UILabel!
    @IBOutlet weak var oneDayScoreView: UIView!
    
    @IBOutlet weak var firstTeamNameAndOver1: UILabel!
    @IBOutlet weak var firstTeamNameAndOver2: UILabel!

    @IBOutlet weak var secondTeamNameAndOver1: UILabel!
    @IBOutlet weak var secondTeamNameAndOver2: UILabel!
    
    @IBOutlet weak var testMatchScoreView: UIView!
    
    @IBOutlet weak var firstTeamNameAndOverScore1: UILabel!
    @IBOutlet weak var firstTeamNameAndOver2Score: UILabel!
    
    @IBOutlet weak var secondTeamNameAndOver1Score: UILabel!
    @IBOutlet weak var secondTeamNameAndOver2Score: UILabel!
    var matchDetails: MatchDetails?
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("TeamScoreView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            scoreboardButton.setTitle("Scorecard".localized().uppercased(), for: .normal)
            scoreboardButton.setTitle("Scorecard".localized().uppercased(), for: .selected)
            scorecardButtonSingle.setTitle("Scorecard".localized().uppercased(), for: .normal)
            scorecardButtonSingle.setTitle("Scorecard".localized().uppercased(), for: .selected)

            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    scoreboardButton.setTitle("  " + "Scorecard".localized().uppercased(), for: .normal)
                    scoreboardButton.setTitle("  " + "Scorecard".localized().uppercased(), for: .selected)
                    scorecardButtonSingle.setTitle("  " + "Scorecard".localized().uppercased(), for: .normal)
                    scorecardButtonSingle.setTitle("  " + "Scorecard".localized().uppercased(), for: .selected)
                }
            }
            addSubview(view)
        }
    }
    
    func showTeamScore(details: MatchDetails) {
        matchDetails = details;
        if details.isViewForTestMatch{
            lblFirstTeamName.text = ""
            lblSecondTeamName.text = ""
            lblFirstTeamScore.text = ""
            lblSecondScore.text = ""
            oneDayScoreView.isHidden = true
            testMatchScoreView.isHidden = false
    
            firstTeamNameAndOver1.text = details.firstTeamLiveScoreShortName + "(" + details.firstTeamOver + ")"
            secondTeamNameAndOver1.text = details.firstTeamLiveScoreShortName + "(" + details.fourthTeamOver + ")"
            firstTeamNameAndOver2.text = details.secondTeamLiveScoreShortName + "(" + details.secondTeamOver + ")"
            secondTeamNameAndOver2.text = details.secondTeamLiveScoreShortName + "(" + details.thirdTeamOver + ")"
            
            firstTeamNameAndOverScore1.text = details.firstTeamLiveScore
            secondTeamNameAndOver1Score.text = details.thirdTeamLiveScore
            firstTeamNameAndOver2Score.text = details.secondTeamLiveScore
            secondTeamNameAndOver2Score.text = details.fourthTeamLiveScore
        }
        else{
            oneDayScoreView.isHidden = false
            testMatchScoreView.isHidden = true
            if details.firstTeamLiveScore.count == 0{
                lblFirstTeamScore.text = details.secondTeamLiveScore + "(" + details.secondTeamOver + " ov)"
                lblFirstTeamName.text = details.secondTeamLiveScoreShortName
            }
            else{
                lblFirstTeamName.text = details.firstTeamLiveScoreShortName
                lblSecondTeamName.text = details.secondTeamLiveScoreShortName
                lblFirstTeamScore.text = details.firstTeamLiveScore + "(" + details.firstTeamOver + " ov)"
                lblSecondScore.text = details.secondTeamLiveScore + "(" + details.secondTeamOver + " ov)"

            }
        }
    }
    
    @IBAction func scorebuttonTapped(_ sender: Any) {
        guard let details = matchDetails else {
            return
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let fullScoreboard = storyboard.instantiateViewController(withIdentifier: "FullScoreboardViewController") as! FullScoreboardViewController
        fullScoreboard.matchKey = details.matchKey 
        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.pushViewController(fullScoreboard, animated: true)
        }
    }
    
    
}
