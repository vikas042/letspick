//
//  BattingTeamScoreView.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class BattingTeamScoreView: UIView {
    
    @IBOutlet weak var templetViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var PointsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewPointsMatchWiseButton: UIButton!

    @IBOutlet weak var pointScoredTitle: UILabel!
    @IBOutlet weak var lblStrikeRateTitle: UILabel!
    
    @IBOutlet weak var lblNegativeTitle: UILabel!
    @IBOutlet weak var lblBonusTitle: UILabel!
    @IBOutlet weak var lblRunTitle: UILabel!
    @IBOutlet weak var lblTotalTitle: UILabel!
    
    @IBOutlet weak var lblStartingTitle: UILabel!
    
    @IBOutlet weak var lblBatingTitle: UILabel!
    
    @IBOutlet weak var lblExtraTitle: UILabel!
    
    @IBOutlet weak var lblPlayingStatus: UILabel!
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var playerNamelabel: UILabel!
    @IBOutlet weak var matchNameLabel: UILabel!
    @IBOutlet weak var pointsScoredLabel: UILabel!
    @IBOutlet weak var startinPointsLabel: UILabel!
    @IBOutlet weak var runsScoredLabel: UILabel!
    @IBOutlet weak var foursScoredLabel: UILabel!
    @IBOutlet weak var sixScoredLabel: UILabel!
    @IBOutlet weak var strikeRatePointsLabel: UILabel!
    @IBOutlet weak var fiftyOrHundresPointsLabel: UILabel!
    @IBOutlet weak var bonusPointsLabel: UILabel!
    @IBOutlet weak var totalPointsScoredLabel: UILabel!
    @IBOutlet weak var scoreMultiplyerLabel: UILabel!    
    @IBOutlet weak var lblNegativeExtra: UILabel!
    @IBOutlet weak var templetView: UIView!
    
    @IBOutlet weak var templateBottomConstraint: NSLayoutConstraint!

    var playerListArray = Array<PlayerDetails>()
    var selectedGameType = GameType.Cricket.rawValue
    var selectedMathDetails: MatchDetails?
    var playerDetails: PlayerDetails?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        if  let entireView =  Bundle.main.loadNibNamed("BattingTeamScoreView", owner: self, options: nil)?.first as? UIView{
            entireView.frame = bounds
            entireView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            addSubview(entireView)
            entireView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            lblStartingTitle.text = "Starting".localized()
            lblStrikeRateTitle.text = "Strike Rate".localized()
            lblExtraTitle.text = "Extra".localized()
            lblBatingTitle.text = "batting".localized()
            lblRunTitle.text = "Runs".localized()
            lblBonusTitle.text = "Bonus".localized()
            lblNegativeTitle.text = "Negative".localized()
            lblTotalTitle.text = "Total".localized()
            pointScoredTitle.text = "Points Scored".localized()

            setupView()
        }
    }
    
    func setupView(){
        
        playerImageView.layer.cornerRadius = 17.0
        let rectShape = CAShapeLayer()
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10.0, height: 10.0)).cgPath
        templetView.layer.mask = rectShape
    }
    
    func configData(details: PlayerDetails, matchDetails: MatchDetails?) {
        playerDetails = details
        if matchDetails?.isMatchTourney ?? false {
            templetViewHeightConstraint.constant = 460
            PointsViewHeightConstraint.constant = 90
            self.layoutIfNeeded()
        }
        playerNamelabel.text = details.playerName
        var totalPoints = details.totalBattingPlayerScore
        scoreMultiplyerLabel.text = ""
        if details.isCaption{
            totalPoints = details.totalBattingCaptainScore
            scoreMultiplyerLabel.text = "X2(" + "Captain".localized() + ")"
            playerNamelabel.text = details.playerName! + "(C)"
        }
        else if details.isViceCaption{
            totalPoints = details.totalBattingViceCaptainScore
            scoreMultiplyerLabel.text = "X1.5(" + "subCaptain".localized() + ")"
            playerNamelabel.text = details.playerName! + "(VC)"
        }
        
        totalPointsScoredLabel.text = totalPoints
        pointsScoredLabel.text = totalPoints
        
        let startingPoints = details.startingPoints ?? "0"
        lblPlayingStatus.text = "No".localized()
        
        if Int(startingPoints) ?? 0 > 0 {
            lblPlayingStatus.text = "Yes".localized()
        }
        
        startinPointsLabel.text = details.startingPoints
        foursScoredLabel.text = details.battingFoursPoints ?? "0"
        runsScoredLabel.text = details.battingRunsPoints ?? "0"
        sixScoredLabel.text = details.battingSixsPoints ?? "0"
        strikeRatePointsLabel.text = details.battingSixsPoints ?? "0"
        fiftyOrHundresPointsLabel.text = details.battingCentryPoints
        if details.seasonalRole == "bowler"{
            strikeRatePointsLabel.text = "0"
        }

        bonusPointsLabel.text = "0"
        lblNegativeExtra.text = "0"
        
        if details.pointsBowlingWicket5 != "0"{
            bonusPointsLabel.text = details.pointsBowlingWicket5
        }
        else if details.pointsBowlingWicket4 != "0"{
            bonusPointsLabel.text = details.pointsBowlingWicket4
        }
        
        if details.playerPlayingRole != PlayerType.Bowler.rawValue{
            lblNegativeExtra.text = details.battingDuckPoints
        }
        
        var playerPlayingRole = details.seasonalRole
        if playerPlayingRole.count == 0 {
            playerPlayingRole = details.playerPlayingRole ?? ""
        }
        
        showPlayerTypeImg(playerPlayingRole: playerPlayingRole)
        playerImageView.image = UIImage(named: details.playerPlaceholder)

        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerImageView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerImageView.image = image
                    }
                    else{
                        self?.playerImageView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }
    }
    
    
    func showPlayerTypeImg(playerPlayingRole: String) {
        if playerPlayingRole == PlayerType.Batsman.rawValue {
            matchNameLabel.text = "Batsman".localized()
        }
        else if playerPlayingRole == PlayerType.Bowler.rawValue {
            matchNameLabel.text = "Bowler".localized()
        }
        else if playerPlayingRole == PlayerType.AllRounder.rawValue {
            matchNameLabel.text = "All Rounder".localized()
        }
        else if playerPlayingRole == PlayerType.WicketKeeper.rawValue {
            matchNameLabel.text = "Wicket Keeper".localized()
        }
    }
    
    @IBAction func reduceButtonAction(_ sender: Any?) {
        
        self.templateBottomConstraint.constant = -360
        UIView.animate(withDuration: 0.5, animations: {
            self.templetView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    func showAnimation() {
        
        self.templateBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.templetView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
            self.layoutIfNeeded()
        })
    }

    @IBAction func swipeGestureTapped(_ sender: Any) {
        reduceButtonAction(nil)
    }
    
    @IBAction func viewPointsMatchWiseButtonTapped(_ sender: Any) {
        removeFromSuperview()
        goToPlayerStatsViewController()
    }
    
    func goToPlayerStatsViewController()  {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//         let playerStatsVC = storyboard.instantiateViewController(withIdentifier: "PlayerStatsViewController") as! PlayerStatsViewController
//
//         playerStatsVC.totalPlayerInfoArray = playerListArray
//         playerStatsVC.selectedMathDetails = selectedMathDetails
//         playerStatsVC.selectedLeagueType = FantasyType.Classic.rawValue
//         playerStatsVC.selectedPlayerDetails = playerDetails
//         playerStatsVC.gameType = selectedGameType;
//
//         playerStatsVC.isLineupsAnnounced = false
//
//         if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
//          navVC.pushViewController(playerStatsVC, animated: true)
//         }
    }

    
}
