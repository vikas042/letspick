//
//  MatchLongPressView.swift
//  Letspick
//
//  Created by Vikash Rajput on 22/06/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class MatchLongPressView: UIView, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamBImageView: UIImageView!
    @IBOutlet weak var lblTeamaMatchName: UILabel!
    @IBOutlet weak var lblSeriesName: UILabel!
    @IBOutlet weak var lblTeambMatchName: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNote: UILabel!

    var dataArray = Array<[String: String]>()
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)!
      if self.subviews.count == 0 {
          setup()
      }
    }

    override init(frame: CGRect) {
      super.init(frame: frame)
      setup()
    }

    func setup() {
      
      if let view = Bundle.main.loadNibNamed("MatchLongPressView", owner: self, options: nil)?.first as? UIView {
        view.frame = bounds
        view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        tblView.register(UINib(nibName: "MatchLongTableViewCell", bundle: nil), forCellReuseIdentifier: "MatchLongTableViewCell")
        addSubview(view)
      }
    }
    
    
    @IBAction func tapGestureTapped(_ sender: Any) {
        removeFromSuperview()
    }
    
    func updateDetails(matchDetails: MatchDetails, gameType: Int) {
        lblNote.isHidden = true
        lblTeamaMatchName.text = matchDetails.firstTeamName
        lblTeambMatchName.text = matchDetails.secondTeamName
        lblSeriesName.text = "  \(matchDetails.seasonShortName.uppercased())  "
        if matchDetails.firstTeamImageUrl.count < 5 {
            self.teamAImageView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: matchDetails.firstTeamImageUrl){
            teamAImageView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                 self?.teamAImageView.image = image
                }
                else{
                    self?.teamAImageView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.teamAImageView.image = UIImage(named: "Placeholder")
        }
        
        if matchDetails.secondTeamImageUrl.count < 5 {
            self.teamBImageView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: matchDetails.secondTeamImageUrl){
            teamBImageView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                 self?.teamBImageView.image = image
                }
                else{
                    self?.teamBImageView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.teamBImageView.image = UIImage(named: "Placeholder")
        }
        
        if matchDetails.stadium.count != 0 {
            dataArray.append(["title": "Venue & Stadium".localized(), "value": matchDetails.stadium])
        }
        
        if (matchDetails.city.count != 0) && (matchDetails.country.count != 0) {
            dataArray.append(["title": "City & Country".localized(), "value": "\(matchDetails.city), \(matchDetails.country)"])
        }

        if matchDetails.startDateTimestemp?.count != 0 {
            let unixTimestamp = Double(matchDetails.startDateTimestemp ?? "0") ?? 0
            if unixTimestamp > 0.0 {
                let date = Date(timeIntervalSince1970: unixTimestamp)
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "IST") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                dateFormatter.dateFormat = "dd MMM yyyy, EEE hh:mm a"
                dateFormatter.amSymbol = "AM"
                dateFormatter.pmSymbol = "PM"
                let strDate = dateFormatter.string(from: date)
                if strDate.count > 0{
                    dataArray.append(["title": "Date & Time".localized(), "value": strDate])
                }
            }
        }

        if gameType == GameType.Football.rawValue {
            if matchDetails.hostName.count != 0 {
                dataArray.append(["title": "Host".localized(), "value": matchDetails.hostName])
            }
            
            if matchDetails.firstTeamRank.count != 0 {
                var isDataAdded = false
                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{
                        isDataAdded = true
                        dataArray.append(["title": "\(matchDetails.firstTeamShortName ?? "") की रैंक", "value": matchDetails.firstTeamRank])
                    }
                }
                
                if !isDataAdded {
                    dataArray.append(["title": "Rank of \(matchDetails.firstTeamShortName ?? "")", "value": matchDetails.firstTeamRank])
                }
            }

            if matchDetails.secondTeamRank.count != 0 {
                var isDataAdded = false
                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{
                        isDataAdded = true
                        dataArray.append(["title": "\(matchDetails.secondTeamShortName ?? "") की रैंक", "value": matchDetails.secondTeamRank])
                    }
                }
                
                if !isDataAdded {
                    dataArray.append(["title": "Rank of \(matchDetails.secondTeamShortName ?? "")", "value": matchDetails.secondTeamRank])
                }
            }
        }
        else{
            if matchDetails.tossWinner.count != 0 {
                dataArray.append(["title": "Toss Won".localized(), "value": matchDetails.tossWinner])
            }
        }
        
        var informationHeight: CGFloat = 0.0
        lblNote.isHidden = true
        if matchDetails.information.count != 0 {
            lblNote.isHidden = false
            lblNote.text = matchDetails.information
            informationHeight = matchDetails.information.height(withConstrainedWidth: lblNote.frame.width, font: lblNote.font)
            informationHeight = informationHeight + 30
        }

        tblViewHeightConstraint.constant = CGFloat(dataArray.count*45)
        containerViewHeightConstraint.constant = CGFloat(dataArray.count*45) + 125 + informationHeight
        self.layoutIfNeeded()
        tblView.reloadData()
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "MatchLongTableViewCell") as? MatchLongTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "MatchLongTableViewCell") as? MatchLongTableViewCell
        }
        
        let details = dataArray[indexPath.row]
        cell?.lblTitle.text = details["title"]
        cell?.lblTitleValue.text = details["value"]

        return cell!
    }

}


