//
//  TeamScoreInstructionView.swift
//  Letspick
//
//  Created by anurag singh on 20/07/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class TeamScoreInstructionView: UIView {
    
    @IBOutlet weak var templateBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var templetView: UIView!
    @IBOutlet weak var lblWizardTag: UILabel!
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var playerNamelabel: UILabel!
    @IBOutlet weak var matchNameLabel: UILabel!
    @IBOutlet weak var pointsScoredLabel: UILabel!
    @IBOutlet weak var pointScoredTitle: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var scoreMultiplyerLabel: UILabel!
    @IBOutlet weak var totalPointsScoredLabel: UILabel!
    
    var selectedFantasyType = FantasyType.Classic.rawValue
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        if  let entireView =  Bundle.main.loadNibNamed("TeamScoreInstructionView", owner: self, options: nil)?.first as? UIView{
            entireView.frame = bounds
            entireView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            addSubview(entireView)
            entireView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            /*
            lblStartingTitle.text = "Starting".localized()
            lblFieldingTitle.text = "Fielding".localized()
            lblStrikeRateTitle.text = "Strike Rate".localized()
            lblWicketTitle.text = "Wickets".localized()
            lblMaidenTitle.text = "Maiden".localized()
            lblCatchTitle.text = "Catch".localized()
            lblRunOutTitle.text = "Run out".localized()
            lblExtraTitle.text = "Extra".localized()
            lblBowlingTitle.text = "bowling".localized()
            lblBatingTitle.text = "batting".localized()
            lblRunTitle.text = "Runs".localized()
            lblBounsTitle.text = "Bonus".localized()
            lblNegtiveTitle.text = "Negative".localized()
            lblTotal.text = "Total".localized();
            pointScoredTitle.text = "Points Scored".localized()
            */
            setupView()
        }
    }
    
    func setupView(){
        /*
        playerImageView.layer.cornerRadius = 17.0
        let rectShape = CAShapeLayer()
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 25.0, height: 25.0)).cgPath
        templetView.layer.mask = rectShape
        
        if UIScreen.main.bounds.height < 656
        {
            templetViewHeight.constant = UIScreen.main.bounds.height-50
        }
        else{
            templetViewHeight.constant = 606.0
        }
        */
    }
    
    func showAnimation() {
        self.templateBottomConstraint.constant = 300
        UIView.animate(withDuration: 0.5, animations: {
            self.templetView.layoutIfNeeded()
            self.layoutIfNeeded()
            self.templateBottomConstraint.constant = 0
        }, completion: {res in
            //Do something
        })
    }
    
    @IBAction func reduceButtonAction(_ sender: Any?) {
        
        self.templateBottomConstraint.constant = -300
        UIView.animate(withDuration: 0.5, animations: {
            self.templetView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    
    
    func configData(details: PlayerDetails, matchDetails: MatchDetails?) {
            if matchDetails?.isMatchTourney ?? false {
               // totalPointsHeightConstraint.constant = 90
               // templetViewHeight.constant = 558
            }
            
            playerNamelabel.text = details.playerName
            var totalPoints = details.totalClasscPlayerScore
            
    //        var totalPoints = ""
    //
    //        if selectedFantasyType == FantasyType.Reverse.rawValue {
    //            totalPoints = details.reversePoints
    //        }
    //        else if selectedFantasyType == FantasyType.Wizard.rawValue {
    //            totalPoints = details.wizardScore
    //        }
    //        else{
    //            totalPoints = details.totalClasscPlayerScore
    //        }
            
            scoreMultiplyerLabel.text = ""
            if details.isWizard{
                lblWizardTag.isHidden = false
            }

            if details.isCaption{
                
                if selectedFantasyType == FantasyType.Reverse.rawValue {
                    totalPoints = details.captainScoreReverse
                }
                else if (selectedFantasyType == FantasyType.Wizard.rawValue) && details.isWizard {
                    totalPoints = details.captainScoreWizard
                }
                else{
                    totalPoints = details.totalClassicCaptainScore
                }
                
                if selectedFantasyType == FantasyType.Reverse.rawValue {
                    scoreMultiplyerLabel.text = "X0.5(" + "Captain".localized() + ")"
                }
                else if (selectedFantasyType == FantasyType.Wizard.rawValue) && details.isWizard {
                    scoreMultiplyerLabel.text = "X5(" + "Captain".localized() + ", \("Wizard".localized())" + ")"
                }
                else{
                    scoreMultiplyerLabel.text = "X2(" + "Captain".localized() + ")"
                }
                playerNamelabel.text = details.playerName! + "(C)"
            }
            else if details.isViceCaption {
                
                if (selectedFantasyType == FantasyType.Reverse.rawValue) {
                    totalPoints = details.viceCaptainScoreReverse
                }
                else if (selectedFantasyType == FantasyType.Wizard.rawValue) && details.isWizard {
                    totalPoints = details.viceCaptainScoreWizard
                }
                else{
                    totalPoints = details.totalClassicViceCaptainScore
                }

                if (selectedFantasyType == FantasyType.Reverse.rawValue) {
                    scoreMultiplyerLabel.text = "X0.75(" + "subCaptain".localized() + ")"
                }
                else if (selectedFantasyType == FantasyType.Wizard.rawValue) && details.isWizard {
                    scoreMultiplyerLabel.text = "X4.5(" + "subCaptain".localized() + ", \("Wizard".localized())" + ")"
                }
                else{
                    scoreMultiplyerLabel.text = "X1.5(" + "subCaptain".localized() + ")"
                }
                playerNamelabel.text = details.playerName! + "(VC)"
            }
            else if (selectedFantasyType == FantasyType.Wizard.rawValue) && details.isWizard {
                scoreMultiplyerLabel.text = "X3" + "( \("Wizard".localized()) )"
                totalPoints = details.wizardScore
            }
            
            totalPointsScoredLabel.text = totalPoints
            pointsScoredLabel.text = totalPoints
            let startingPoints = details.startingPoints ?? "0"
        
        /*
            lblPlayingStatus.text = "No".localized()
            
            if Int(startingPoints) ?? 0 != 0 {
                lblPlayingStatus.text = "Yes".localized()
            }
            
            startinPointsLabel.text = details.startingPoints
            foursScoredLabel.text = details.battingFoursPoints ?? "0"
            runsScoredLabel.text = details.battingRunsPoints ?? "0"
            sixScoredLabel.text = details.battingSixsPoints ?? "0"
            strikeRatePointsLabel.text = details.battingStrickRatePoints ?? "0"
            fiftyOrHundresPointsLabel.text = details.battingCentryPoints
            if details.seasonalRole == "bowler"{
                strikeRatePointsLabel.text = "0"
            }
            
            wicketsPointsLabel.text = details.bowlerWicketPoints ?? "0"
            maidenPointsLabel.text = details.bowlerMaidenOverPoints ?? "0"
            catchPointsLabel.text = details.catchPoints ?? "0"
            runOutPointsLabel.text = details.runoutPoints ?? "0"
            bonusPointsLabel.text = "0"
            lblNegativeExtra.text = "0"
            
            if details.pointsBowlingWicket5 != "0"{
                bonusPointsLabel.text = details.pointsBowlingWicket5
            }
            else if details.pointsBowlingWicket4 != "0"{
                bonusPointsLabel.text = details.pointsBowlingWicket4
            }
            
            if details.playerPlayingRole != PlayerType.Bowler.rawValue{
                lblNegativeExtra.text = details.battingDuckPoints
            }
            */
            var playerPlayingRole = details.seasonalRole
            if playerPlayingRole.count == 0 {
                playerPlayingRole = details.playerPlayingRole ?? ""
            }
            
            showPlayerTypeImg(playerPlayingRole: playerPlayingRole)
            playerImageView.image = UIImage(named: details.playerPlaceholder)
            if details.imgURL.count > 0 {
                if let url = URL(string: details.imgURL){
                    playerImageView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                        // Report progress
                    }, completion: { [weak self] image in
                        if (image != nil){
                            self?.playerImageView.image = image
                        }
                        else{
                            self?.playerImageView.image = UIImage(named: details.playerPlaceholder)

                        }
                    })
                }
            }
        }
    
    func showPlayerTypeImg(playerPlayingRole: String) {
        if playerPlayingRole == PlayerType.Batsman.rawValue {
            matchNameLabel.text = "Batsman".localized()
        }
        else if playerPlayingRole == PlayerType.Bowler.rawValue {
            matchNameLabel.text = "Bowler".localized()
        }
        else if playerPlayingRole == PlayerType.AllRounder.rawValue {
            matchNameLabel.text = "All Rounder".localized()
        }
        else if playerPlayingRole == PlayerType.WicketKeeper.rawValue {
            matchNameLabel.text = "Wicket Keeper".localized()
        }
    }
}
