//
//  FootballPointsScoreView.swift
//  Letspick
//
//  Created by Madstech on 30/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class FootballPointsScoreView: UIView {

    
    @IBOutlet weak var lblActualPlayingtime: UILabel!
    @IBOutlet weak var lblActualGoalPoints: UILabel!
    @IBOutlet weak var lblActualAssistsPoints: UILabel!
    @IBOutlet weak var lblActual10PassPoints: UILabel!
    @IBOutlet weak var lblActual2ShotsPoints: UILabel!
    @IBOutlet weak var lblActualCleenSheetPoints: UILabel!
    @IBOutlet weak var lblActual3SavedPoints: UILabel!
    @IBOutlet weak var lblActualPanelitySavedPoints: UILabel!
    @IBOutlet weak var lblActualTacklePoints: UILabel!
    @IBOutlet weak var lblActualYellowCardPoints: UILabel!
    @IBOutlet weak var lblActualRedCardsPoints: UILabel!
    @IBOutlet weak var lblActualOwnGoaledPoints: UILabel!
    @IBOutlet weak var lblActualGoalConceded: UILabel!
    @IBOutlet weak var lblActualPenalityMissed: UILabel!
    

    @IBOutlet weak var pointScoredTitle: UILabel!
    @IBOutlet weak var lblCardTitle: UILabel!
    @IBOutlet weak var penalityMissedTitle: UILabel!
    @IBOutlet weak var lblOwnGoalTitle: UILabel!
    @IBOutlet weak var lbl2GoalConductedTitle: UILabel!
    @IBOutlet weak var lbl2ShotsTitle: UILabel!
    @IBOutlet weak var lbl3ShotTitle: UILabel!
    @IBOutlet weak var lblCleanSheetTitle: UILabel!
    @IBOutlet weak var lbl10PassesTitle: UILabel!
    @IBOutlet weak var lblGoalTitle: UILabel!
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblYellowCardTitle: UILabel!
    @IBOutlet weak var lblRedCardTitle: UILabel!
    @IBOutlet weak var lblTakleTitle: UILabel!
    @IBOutlet weak var lblPenalitySavedTitle: UILabel!
    @IBOutlet weak var lblAssitsTitle: UILabel!
    @IBOutlet weak var playingTimeTitle2: UILabel!
    @IBOutlet weak var lblPlayingTime1: UILabel!
    @IBOutlet weak var lblAttackTitle: UILabel!
    @IBOutlet weak var lblDefenceTitle: UILabel!
    @IBOutlet weak var lblExtraTitle: UILabel!
    
    @IBOutlet weak var playerRoleImgView: UIImageView!
    @IBOutlet weak var lblGoalPoints: UILabel!
    @IBOutlet weak var lbl10Passes: UILabel!
    @IBOutlet weak var lbl2Shots: UILabel!
    
    @IBOutlet weak var lblYellowCard: UILabel!
    @IBOutlet weak var lblPaneltyPoints: UILabel!
    @IBOutlet weak var lblCleanSheet: UILabel!
    @IBOutlet weak var lblTacklePoints: UILabel!
    
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var lblOwnGoal: UILabel!
    @IBOutlet weak var lblRedCard: UILabel!
    @IBOutlet weak var lbl3Shots: UILabel!
    
    @IBOutlet weak var lbl2GoalConceded: UILabel!
    @IBOutlet weak var lblPanilityMissed: UILabel!
    
    @IBOutlet weak var containerBottomConstraintView: NSLayoutConstraint!
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTotalPoints: UILabel!
    @IBOutlet weak var lblAssistPoints: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblPlayerRole: UILabel!
    @IBOutlet weak var lblScoredPlayerPoints: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    
    @IBOutlet weak var lblPlayingTime: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        if  let entireView =  Bundle.main.loadNibNamed("FootballPointsScoreView", owner: self, options: nil)?.first as? UIView{
            entireView.frame = bounds
            entireView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            addSubview(entireView)
            entireView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            lblPlayingTime1.text = "Playing Time".localized()
            playingTimeTitle2.text = "Playing Time".localized()
            lblGoalTitle.text = "Goal".localized()
            lblAssitsTitle.text = "Assist".localized()
            lbl10PassesTitle.text = "10 Passes".localized()
            lbl2ShotsTitle.text = "2 Shots".localized()
            lblCleanSheetTitle.text = "Clean Sheet".localized()
            lbl3ShotTitle.text = "3 Shots Saved".localized()
            lblPenalitySavedTitle.text = "Penalty Saved".localized()
            lblTakleTitle.text = "3 Successful Tackle".localized()
            lblYellowCardTitle.text = "Yellow Card".localized()
            lblRedCardTitle.text = "Red Card".localized()
            penalityMissedTitle.text = "Panalty Missed".localized()
            lbl2GoalConductedTitle.text = "2 Goal Conceded".localized()
            lblOwnGoalTitle.text = "Own Goal".localized()

            lblAttackTitle.text = "Attack".localized()
            lblDefenceTitle.text = "Defence".localized()
            lblCardTitle.text = "Card".localized()
            lblExtraTitle.text = "Extra".localized()
            lblTotalTitle.text = "Total".localized()
            
            pointScoredTitle.text = "Points Scored".localized()
            setupView()
        }
    }
    
    func setupView(){
        playerRoleImgView.layer.cornerRadius = 17.0
        let rectShape = CAShapeLayer()
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 25.0, height: 25.0)).cgPath
        containerView.layer.mask = rectShape
    }
    
    
    func configData(details: PlayerDetails) {
        if UIScreen.main.bounds.size.height > 667{
            containerBottomConstraintView.constant = 635
            containerView.layoutIfNeeded()
        }
        
        lblPlayerName.text = details.playerName
        var totalPoints = details.totalClasscPlayerScore
        
        lblCaptain.text = ""
        if details.isCaption{
            totalPoints = details.totalClassicCaptainScore
            lblCaptain.text = "X2(" + "Captain".localized() + ")"
            lblPlayerName.text = details.playerName! + "(C)"
            
        }
        else if details.isViceCaption {
            totalPoints = details.totalClassicViceCaptainScore
            lblCaptain.text = "X1.5(" + "subCaptain".localized() + ")"
            lblPlayerName.text = details.playerName! + "(VC)"
        }
        
        

        lblActual3SavedPoints.text = details.goal_shot_saved        
        lblActual2ShotsPoints.text = details.shot_on_target
        lblActual10PassPoints.text = details.passes_completed
        lblActualPlayingtime.text = details.min_played
        lblActualGoalPoints.text = details.goal_scored
        lblActualAssistsPoints.text = details.goal_assist
        lblActualCleenSheetPoints.text = details.clean_sheet
        lblActualPanelitySavedPoints.text = details.penality_saved
        lblActualTacklePoints.text = details.tackles_made
        lblActualRedCardsPoints.text = details.red_card
        lblActualYellowCardPoints.text = details.yellow_card
        lblActualOwnGoaledPoints.text = details.own_goal
        lblActualGoalConceded.text = details.goal_conceded
        lblActualPenalityMissed.text = details.penality_won



        lblScoredPlayerPoints.text = totalPoints
        lblTotalPoints.text = totalPoints
        lblPlayingTime.text = details.minPlayed
        lblGoalPoints.text = details.goalScored
        lblAssistPoints.text = details.goalAssist
        lbl10Passes.text = details.passesCompleted
        lbl2Shots.text = details.shotOnTarget
        lblCleanSheet.text = details.cleanSheet
        lbl3Shots.text = details.goalShotSaved
        lblPaneltyPoints.text = details.tacklesMade
        lblPaneltyPoints.text = details.penalitySaved
        lblTacklePoints.text = details.tacklesMade
        lblYellowCard.text = details.footballYellowCard
        lblRedCard.text = details.footballRedCard
        lblOwnGoal.text = details.ownGoal
        lbl2GoalConceded.text = details.goalConceded
        lblPanilityMissed.text = details.penalityWon
        
        var playerPlayingRole = details.seasonalRole
        if playerPlayingRole.count == 0 {
            playerPlayingRole = details.playerPlayingRole ?? ""
        }
        
        if playerPlayingRole == PlayerType.GoalKeeper.rawValue {
            playerRoleImgView.image = UIImage(named: "FootballGoalKeeperIcon")
            lblPlayerRole.text = "Goal Keeper".localized()
        }
        else if playerPlayingRole == PlayerType.Defender.rawValue {
            playerRoleImgView.image = UIImage(named: "FootballDefenderIcon")
            lblPlayerRole.text = "Defender".localized()
        }
        else if playerPlayingRole == PlayerType.MidFielder.rawValue {
            playerRoleImgView.image = UIImage(named: "FootballMidFielderIcon")
            lblPlayerRole.text = "MidFielder".localized()
        }
        else if playerPlayingRole == PlayerType.Sticker.rawValue {
            playerRoleImgView.image = UIImage(named: "FootballForwardIcon")
            lblPlayerRole.text = "Sticker".localized()
        }
        
        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerRoleImgView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerRoleImgView.image = image
                    }
                    else{
                        self?.playerRoleImgView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }
        

    }
    
    
    @IBAction func reduceButtonAction(_ sender: Any?) {
        
        self.containerBottomConstraintView.constant = -640
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
        
    }
    
    func showAnimation() {
        self.containerBottomConstraintView.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
        })
    }
    
  
    
    
    @IBAction func swipeGestureTapped(_ sender: Any) {
        reduceButtonAction(nil)

    }
    
}
