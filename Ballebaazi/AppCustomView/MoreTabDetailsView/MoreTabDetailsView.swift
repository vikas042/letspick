//
//  MoreTabDetailsView.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SafariServices

class MoreTabDetailsView: UIView,UITableViewDataSource,UITableViewDelegate, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var settingTblView: UITableView!
    
//    var dataArray = Array<String>()
    
    var dataArray = [[String: String]]()

    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("MoreTabDetailsView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            
            view.backgroundColor = UIColor.clear.withAlphaComponent(0)
            self.backgroundColor = UIColor.clear
            settingTblView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
            settingTblView.register(UINib(nibName: "MoreTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreTableViewCell")
            settingTblView.register(UINib(nibName: "AppVersionTableViewCell", bundle: nil), forCellReuseIdentifier: "AppVersionTableViewCell")

            updateDataArray()
            addSubview(view)
        }
    }
    
    func updateDataArray() {
        dataArray.removeAll()
        dataArray = [["title": "How To Play".localized(), "image": "HowToPlayIcon"], ["title": "Leaderboards".localized(), "image": "LeaderboardsIcon"], ["title": "Promotions".localized(), "image": "PromotionsIcon"],["title": "Redeem Code".localized(), "image": "RedeemCodeIcon"], ["title": "Point System".localized(), "image": "PointSystemIcon"], ["title": "Join League".localized(), "image": "JoinLeagueMoreIcon"], /*["title": "Blog".localized(), "image": "BlogIcon"],*/ ["title": "Letspick Policies".localized(), "image": "BalleBaaziPoliciesIcon"], ["title": "Settings".localized(), "image": "SettingsIcon"], ["title": "Update my App".localized(), "image": "UpdateMyAppIcon"]]

        
//        dataArray = [
//            "How To Play".localized(),
//            "Leaderboards".localized(),
//            "Promotions".localized(),
//            "Redeem Code".localized(),
//            "Point System".localized(),
//            "Join League".localized(),
//            "Blog".localized(),
//            "Letspick Policies".localized(),
//            "Settings".localized(),
//            "Update my App".localized()]
//
//        dataArray.insert("Refer & Earn".localized(), at: 5)

        
        dataArray.insert(["title": "Partnership Program".localized(), "image": "PartnershipProgramIcon"], at: 5)
        /*
        if UserDetails.sharedInstance.userAffilate != "0" {
            dataArray.insert(["title": "Referral Earning".localized(), "image": "ReferralEarningIcon"], at: 6)
        }
        */
        settingTblView.reloadData()
    }
    //MARK:- Table view Data and Delegats
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return dataArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == dataArray.count {
            var cell = tableView.dequeueReusableCell(withIdentifier: "AppVersionTableViewCell") as? AppVersionTableViewCell
            
            if cell == nil {
                cell = AppVersionTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "AppVersionTableViewCell")
            }

            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            cell?.lblTitle.text = "Version " + appVersion 
            cell?.lblTitle.textAlignment = .center
            return cell!
        }
        else{
//            var cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as? SettingsTableViewCell
//            if cell == nil {
//                cell = SettingsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "SettingsTableViewCell")
//            }
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as? MoreTableViewCell
            if cell == nil {
                cell = MoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "MoreTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
//            cell?.lblReferAndEarnTitle.text = "(\("Refer & Earn".localized()))"
//            cell?.lblReferAndEarnTitle.isHidden = true
//            if dataArray[indexPath.row].localized() == "Partnership Program".localized() {
//                cell?.lblReferAndEarnTitle.isHidden = false
//            }
            
            let details = dataArray[indexPath.row]
            let cellTitle = details["title"]!.localized()
            AppHelper.showShodowOnCellsView(innerView: cell!.innerView)
            cell?.lblCoin.isHidden = true
            cell?.lblTitle.text = cellTitle
            cell?.imgView.image = UIImage(named: details["image"]!)
            cell?.imgView.tintColor = selectedTabColor
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.row >= dataArray.count {
            return;
        }
        
        let details = dataArray[indexPath.row]
        let cellTitle = details["title"]!.localized()

        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if cellTitle == "Language".localized() {
            let languagueVC = storyboard.instantiateViewController(withIdentifier: "LanguageViewController") as? LanguageViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(languagueVC!, animated: true)
            }
        }
        else if cellTitle == "Leaderboards".localized() {
            AppxorEventHandler.logAppEvent(withName: "LeaderboardClicked", info: nil)

            let transactionsVC = storyboard.instantiateViewController(withIdentifier: "LeaderboardViewController") as? LeaderboardViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(transactionsVC!, animated: true)
            }
        }
        else if cellTitle == "Letspick Policies".localized() {
            AppxorEventHandler.logAppEvent(withName: "BalleBaaziPoliciesClicked", info: nil)

            let privacyPolicyVC = storyboard.instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "Transactions".localized() {
            let transactionsVC = storyboard.instantiateViewController(withIdentifier: "TransactionsViewController") as? TransactionsViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(transactionsVC!, animated: true)
            }
        }
        else if cellTitle == "Referral Earning".localized() {
            let myEarningVC = storyboard.instantiateViewController(withIdentifier: "PartnershipWalletViewController") as? PartnershipWalletViewController
            myEarningVC?.isNeedToCallAPI = true
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(myEarningVC!, animated: true)
            }
        }
        else if cellTitle == "Partnership Program".localized() {
            AppxorEventHandler.logAppEvent(withName: "PrtnershipProgramClicked", info: nil)

            if UserDetails.sharedInstance.userAffilate != "0" {
                let partnershipProgramVC = storyboard.instantiateViewController(withIdentifier: "PartnershipProgramViewController") as? PartnershipProgramViewController
                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navigationVC.pushViewController(partnershipProgramVC!, animated: true)
                }
            }
            else{
                let becomePartnerVC = storyboard.instantiateViewController(withIdentifier: "BecomePartnerViewController") as? BecomePartnerViewController
                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navigationVC.pushViewController(becomePartnerVC!, animated: true)
                }
            }
        }
        else if cellTitle == "Verifications".localized() {
            
            let verifyDocumentsVC = storyboard.instantiateViewController(withIdentifier: "VerifyDocumentsViewController") as? VerifyDocumentsViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(verifyDocumentsVC!, animated: true)
            }
        }
        else if cellTitle == "Join League".localized() {
            AppxorEventHandler.logAppEvent(withName: "JoinLeagueClicked", info: nil)

            let joinPrivateLeagueVC = storyboard.instantiateViewController(withIdentifier: "JoinPrivateLeagueViewController") as? JoinPrivateLeagueViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(joinPrivateLeagueVC!, animated: true)
            }
        }
        else if cellTitle == "Blog".localized() {
            let urlString = "https://www.letspick.com/blog"

            if let url = URL(string: urlString) {
                let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
                vc.delegate = self
                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navigationVC.present(vc, animated: true)
                }
            }
        }
        else if cellTitle == "Contact Us".localized()  {
            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.ContactUs.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
            
        else if cellTitle == "How To Play".localized()  {
            AppxorEventHandler.logAppEvent(withName: "HowToPlayClicked", info: nil)

            let howtoPlayVC = storyboard.instantiateViewController(withIdentifier: "HowToPlayViewController") as? HowToPlayViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(howtoPlayVC!, animated: true)
            }
        }
        else if cellTitle == "Point System".localized()  {
            AppxorEventHandler.logAppEvent(withName: "PointSystemClicked", info: nil)

            let privacyPolicyVC = storyboard.instantiateViewController(withIdentifier: "PointSystemViewController") as? PointSystemViewController
           // privacyPolicyVC?.selectedType = SelectedWebViewType.PointSysytem.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
            
            /*
            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.PointSysytem.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
            */
        }
        else if cellTitle == "FAQ".localized()  {
            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.FAQ.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "Referral Policies".localized()  {
            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.ReferralPolicies.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "Privacy Policy & Legalities".localized()  {
            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.PrivacyPolicy.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "Promotions".localized()  {
            AppxorEventHandler.logAppEvent(withName: "PromotionsClicked", info: nil)

            let promotionsVC = storyboard.instantiateViewController(withIdentifier: "PromotionsViewController") as? PromotionsViewController
            promotionsVC?.isFromSetting = true
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(promotionsVC!, animated: true)
            }
        }
        else if cellTitle == "Refer & Earn".localized()  {
            
            let referEarnVC = storyboard.instantiateViewController(withIdentifier: "ReferAndEarnViewController") as? ReferAndEarnViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(referEarnVC!, animated: true)
            }
        }
        else if cellTitle == "Redeem Code".localized()  {
            AppxorEventHandler.logAppEvent(withName: "RedeemCodeClicked", info: nil)

            let redeemCodeVC = storyboard.instantiateViewController(withIdentifier: "RedeamCodeViewController") as? RedeamCodeViewController

            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(redeemCodeVC!, animated: true)
            }
        }
        else if cellTitle == "Update my App".localized()  {
            AppxorEventHandler.logAppEvent(withName: "UpdateMyAppClicked", info: nil)

            AppHelper.showUpdatePopupFromSetting()
        }
        else if cellTitle == "Settings".localized()  {
            AppxorEventHandler.logAppEvent(withName: "SettingsClicked", info: nil)

            let settingVC = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(settingVC!, animated: true)
            }
        }
        else if cellTitle == "Logout".localized()  {
            
            let permissionPopup = PermissionView(frame: APPDELEGATE.window!.frame)
            APPDELEGATE.window?.addSubview(permissionPopup)
            permissionPopup.showAnimation()
            permissionPopup.updateMessage(title: "Logout".localized(), message: "Are your sure you want to logout?".localized())
            permissionPopup.updateButtonTitle(noButtonTitle: "No".localized(), yesButtonTitle: "Yes".localized())

            permissionPopup.noButtonTappedBlock { (status) in
                
            }
            
            permissionPopup.yesButtonTappedBlock { (status) in
                AppHelper.resetDefaults()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userlogoutNotitification"), object: nil)
                AppHelper.makeLoginAsRootViewController()
            }

        }

    }
    
}
