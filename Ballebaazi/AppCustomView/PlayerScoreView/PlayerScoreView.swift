//  PlayerScoreView.swift
//  Letspick
//  Created by Vikash Rajput on 13/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.


import UIKit

class PlayerScoreView: UIView, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblContainerView: UIView!
    @IBOutlet weak var tblContainerHeightConstraint: NSLayoutConstraint!
    var playerDetails: PlayerDetails?
    var selectedLeagueType = FantasyType.Batting.rawValue
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("PlayerScoreView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.backgroundColor = UIColor.clear
            tblContainerView.layer.borderWidth = 1.5
            
            tblView.register(UINib(nibName: "PlayerNameScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerNameScoreTableViewCell")
            tblView.register(UINib(nibName: "BattingPlayerScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BattingPlayerScoreTableViewCell")
            tblView.register(UINib(nibName: "BowlingPlayerScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BowlingPlayerScoreTableViewCell")
            tblView.register(UINib(nibName: "ExtraPlayerScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "ExtraPlayerScoreTableViewCell")
            tblView.register(UINib(nibName: "CatchPlayerScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "CatchPlayerScoreTableViewCell")
            tblView.register(UINib(nibName: "ExtraPlayerScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "ExtraPlayerScoreTableViewCell")
            tblView.register(UINib(nibName: "RunoutPlayerScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "RunoutPlayerScoreTableViewCell")
            tblView.register(UINib(nibName: "TotalPlayerScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "TotalPlayerScoreTableViewCell")
            
            addSubview(view)
        }
    }
    
    
    func adjustTableContainerViewHeight() {
        
        if selectedLeagueType == FantasyType.Classic.rawValue {
            tblContainerHeightConstraint.constant = 395.0
        }
        else if selectedLeagueType == FantasyType.Batting.rawValue {
            tblContainerHeightConstraint.constant = 260.0
        }
        else if selectedLeagueType == FantasyType.Bowling.rawValue {
            tblContainerHeightConstraint.constant = 285.0
        }

        tblContainerView.layoutIfNeeded()
        tblView.layoutIfNeeded()
    }
    
    //MARK:- Table view Data and Delegats
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if playerDetails == nil {
            return 0;
        }

        if (selectedLeagueType == FantasyType.Classic.rawValue){
            return 7
        }
        else if (selectedLeagueType == FantasyType.Batting.rawValue){
            return 4
        }
        else if (selectedLeagueType == FantasyType.Bowling.rawValue){
            return 5
        }
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
                if indexPath.row == 0 {
            return 80.0
        }
        else if indexPath.row == 1 {
            return 55.0
        }
        else if indexPath.row == 2 {
            if (selectedLeagueType == FantasyType.Classic.rawValue){
                return 55.0
            }
            else if selectedLeagueType == FantasyType.Batting.rawValue{
                return 55.0
            }
        }
        else if indexPath.row == 5 {
            return 55.0
        }

        return 40.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "PlayerNameScoreTableViewCell") as? PlayerNameScoreTableViewCell
            
            if cell == nil {
                cell = PlayerNameScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PlayerNameScoreTableViewCell")
            }
            
            if playerDetails!.isCaption{
                cell?.playerName.text = (playerDetails?.playerName ?? "") + " (C)"
            }
            else if playerDetails!.isViceCaption{
                cell?.playerName.text = (playerDetails?.playerName ?? "") + " (VC)"
            }
            else{
                cell?.playerName.text = playerDetails?.playerName ?? ""
            }
            
            cell?.lblStartingCount.text = playerDetails?.startingPoints ?? "0"

            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            return cell!
        }
        else if indexPath.row == 1 {
            if (selectedLeagueType == FantasyType.Classic.rawValue) || (selectedLeagueType == FantasyType.Batting.rawValue){
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "BattingPlayerScoreTableViewCell") as? BattingPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = BattingPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "BattingPlayerScoreTableViewCell")
                }
                
                cell?.lblFoursCount.text = playerDetails?.battingFoursPoints ?? "0"
                cell?.lblRuns.text = playerDetails?.battingRunsPoints ?? "0"
                cell?.lblSixesCount.text = playerDetails?.battingSixsPoints ?? "0"
                cell?.lblStrickRate.text = playerDetails?.battingStrickRatePoints ?? "0"

                if playerDetails?.seasonalRole == "bowler"{
                    cell?.lblStrickRate.text = "0"
                }
                
                cell?.lblCenturyCount.text = playerDetails?.battingCentryPoints ?? "0"

                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
            else if selectedLeagueType == FantasyType.Bowling.rawValue{
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "BowlingPlayerScoreTableViewCell") as? BowlingPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = BowlingPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "BowlingPlayerScoreTableViewCell")
                }
                
                cell?.lblWicketCount.text = playerDetails?.bowlerWicketPoints ?? "0"
                cell?.lblMaddenCOunt.text = playerDetails?.bowlerMaidenOverPoints ?? "0"
                cell?.lblWEconomyRate.text = playerDetails?.pointsBowlingEconomy ?? "0"
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
        }
        else if indexPath.row == 2 {
            if (selectedLeagueType == FantasyType.Classic.rawValue){
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "BowlingPlayerScoreTableViewCell") as? BowlingPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = BowlingPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "BowlingPlayerScoreTableViewCell")
                }
                
                cell?.lblWicketCount.text = playerDetails?.bowlerWicketPoints ?? "0"
                cell?.lblMaddenCOunt.text = playerDetails?.bowlerMaidenOverPoints ?? "0"
                cell?.lblWEconomyRate.text = playerDetails?.pointsBowlingEconomy ?? "0"

                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
            else if selectedLeagueType == FantasyType.Batting.rawValue{
                var cell = tableView.dequeueReusableCell(withIdentifier: "ExtraPlayerScoreTableViewCell") as? ExtraPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = ExtraPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "ExtraPlayerScoreTableViewCell")
                }
                
                cell?.lblBonus.text = "0"
                cell?.lblNagativeCount.text = "0"

                if playerDetails?.pointsBowlingWicket5 != "0"{
                    cell?.lblBonus.text = playerDetails?.pointsBowlingWicket5
                }
                else if playerDetails?.pointsBowlingWicket4 != "0"{
                    cell?.lblBonus.text = playerDetails?.pointsBowlingWicket4
                }
                
                
                if playerDetails?.playerPlayingRole != PlayerType.Bowler.rawValue{
                    if ((playerDetails?.battingDuckPoints.count)! > 0) && (playerDetails?.battingDuckPoints != "0"){
                        cell?.lblNagativeCount.text = playerDetails?.battingDuckPoints
                    }
                }

                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
            else if selectedLeagueType == FantasyType.Bowling.rawValue{
                var cell = tableView.dequeueReusableCell(withIdentifier: "CatchPlayerScoreTableViewCell") as? CatchPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = CatchPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "CatchPlayerScoreTableViewCell")
                }
                cell?.lblCatchCount.text = "  " + (playerDetails?.catchPoints ?? "0")
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
        }
        else if indexPath.row == 3 {
            
            if (selectedLeagueType == FantasyType.Classic.rawValue){
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "CatchPlayerScoreTableViewCell") as? CatchPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = CatchPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "CatchPlayerScoreTableViewCell")
                }
                cell?.lblCatchCount.text = "  " + (playerDetails?.catchPoints ?? "0")

                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
            else if selectedLeagueType == FantasyType.Batting.rawValue{
                var cell = tableView.dequeueReusableCell(withIdentifier: "TotalPlayerScoreTableViewCell") as? TotalPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = TotalPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TotalPlayerScoreTableViewCell")
                }
                
                if playerDetails!.isCaption{
                    
                    cell?.lblTotal.text = "  " + (playerDetails?.totalBattingCaptainScore ?? "0")
                }
                else if playerDetails!.isViceCaption{
                    cell?.lblTotal.text = "  " + (playerDetails?.totalBattingViceCaptainScore ?? "0")
                }
                else {
                    cell?.lblTotal.text = "  " + (playerDetails?.totalBattingPlayerScore ?? "0")
                }
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
            else if selectedLeagueType == FantasyType.Bowling.rawValue{
                var cell = tableView.dequeueReusableCell(withIdentifier: "RunoutPlayerScoreTableViewCell") as? RunoutPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = RunoutPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "RunoutPlayerScoreTableViewCell")
                }
                cell?.lblRunoutCount.text = "  " + (playerDetails?.runoutPoints ?? "0")
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
        }
        else if indexPath.row == 4 {
            
            if (selectedLeagueType == FantasyType.Classic.rawValue){
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "RunoutPlayerScoreTableViewCell") as? RunoutPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = RunoutPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "RunoutPlayerScoreTableViewCell")
                }
                cell?.lblRunoutCount.text = "  " + (playerDetails?.runoutPoints ?? "0")

                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
            else if selectedLeagueType == FantasyType.Batting.rawValue{
                
            }
            else if selectedLeagueType == FantasyType.Bowling.rawValue{
                var cell = tableView.dequeueReusableCell(withIdentifier: "TotalPlayerScoreTableViewCell") as? TotalPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = TotalPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TotalPlayerScoreTableViewCell")
                }
                
                if playerDetails!.isCaption{
                    cell?.lblTotal.text = "  " + (playerDetails?.totalBowlingCaptainScore ?? "0")
                }
                else if playerDetails!.isViceCaption{
                    cell?.lblTotal.text = "  " + (playerDetails?.totalBowlingViceCaptainScore ?? "0")
                }
                else {
                    cell?.lblTotal.text = "  " + (playerDetails?.totalBowlingPlayerScore ?? "0")
                }
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
        }
        else if indexPath.row == 5 {
            
            if (selectedLeagueType == FantasyType.Classic.rawValue){
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "ExtraPlayerScoreTableViewCell") as? ExtraPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = ExtraPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "ExtraPlayerScoreTableViewCell")
                }
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                
                cell?.lblBonus.text = "0"
                cell?.lblNagativeCount.text = "0"
                
                if playerDetails?.pointsBowlingWicket5 != "0"{
                    cell?.lblBonus.text = playerDetails?.pointsBowlingWicket5 ?? "0"
                }
                else if playerDetails?.pointsBowlingWicket4 != "0"{
                    cell?.lblBonus.text = playerDetails?.pointsBowlingWicket4 ?? "0"
                }
                
                if playerDetails?.playerPlayingRole != PlayerType.Bowler.rawValue{
                    cell?.lblNagativeCount.text = playerDetails?.battingDuckPoints ?? "0"
                }

                return cell!
            }
        }
        else if indexPath.row == 6 {
            
            if (selectedLeagueType == FantasyType.Classic.rawValue){
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "TotalPlayerScoreTableViewCell") as? TotalPlayerScoreTableViewCell
                
                if cell == nil {
                    cell = TotalPlayerScoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TotalPlayerScoreTableViewCell")
                }
                
                if playerDetails!.isCaption{
                    cell?.lblTotal.text = "  " + (playerDetails?.totalClassicCaptainScore ?? "0")
                }
                else if playerDetails!.isViceCaption{
                    cell?.lblTotal.text = "  " + (playerDetails?.totalClassicViceCaptainScore ?? "0")
                }
                else {
                    cell?.lblTotal.text = "  " + (playerDetails?.totalClasscPlayerScore ?? "0")
                }

                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                return cell!
            }
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func hideViewWithAnimation()  {
        weak var weakSelf = self
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            weakSelf?.alpha = 0
        }) { _ in
            weakSelf?.removeFromSuperview()
        }
    }

    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        hideViewWithAnimation()
    }
}
