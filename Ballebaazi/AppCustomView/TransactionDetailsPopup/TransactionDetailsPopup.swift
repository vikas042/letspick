//
//  TransactionDetailsPopup.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class TransactionDetailsPopup: UIView {

    @IBOutlet weak var lblMatchNameTitle: UILabel!
    @IBOutlet weak var imgViewCoin: UIImageView!
    @IBOutlet weak var lblDateTitle: UILabel!
    
    @IBOutlet weak var lblLeagueNameTitle: UILabel!
    @IBOutlet weak var lblTransactionTypeTitle: UILabel!
    
    @IBOutlet weak var lblTransactionAmtTitle: UILabel!
    @IBOutlet weak var lblTransactionDetails: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTransactionMessage: UILabel!
    
    @IBOutlet weak var lblRefundMessage: UILabel!
    @IBOutlet weak var lblTransactionDate: UILabel!
    @IBOutlet weak var leagueName: UILabel!
    @IBOutlet weak var matchName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    var transactionDetails: TransactionsDetails?
    
    @IBOutlet weak var containerView: UIView!
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("TransactionDetailsPopup", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.backgroundColor = UIColor.clear
            containerView.roundViewCorners(corners: .topRight, radius: 10)
            containerView.roundViewCorners(corners: .topRight, radius: 10)
            lblTransactionAmtTitle.text = "Transaction Amount".localized();
            lblTransactionDetails.text = "Transaction details".localized();

            lblDateTitle.text = "Date_trans".localized()
            lblTransactionTypeTitle.text = "Transaction_Type".localized()
            lblLeagueNameTitle.text = "League_name".localized()
            lblMatchNameTitle.text = "Match_name".localized()
            lblRefundMessage.text = "joiningAmountBack".localized()
            addSubview(view)
        }
    }
    
    
    func updateTransactionDetails() {
        imgViewCoin.isHidden = true
        if transactionDetails!.transactionType == "3" {
            lblRefundMessage.isHidden = false
        }
        
        lblTransactionDate.text = transactionDetails!.transactionDate

        if transactionDetails!.leagueName.count == 0 {
            leagueName.text = "N/A"
        }
        else{
            leagueName.text = transactionDetails!.leagueName
        }
        
        if transactionDetails!.matchName.count == 0 {
            matchName.text = "N/A"
        }
        else{
            matchName.text = transactionDetails!.matchName
        }
        
        lblAmount.text =
            AppHelper.makeCommaSeparatedDigitsWithString(digites: transactionDetails!.transactionAmount)
        lblTransactionMessage.text = transactionDetails!.transactionMessage
        
        if transactionDetails!.transactionType == "1" {
            lblTransactionMessage.text = "Contest Won".localized()
        }
        else if transactionDetails!.transactionType == "2" {
            if transactionDetails!.byAdmin == "1"{
                lblTransactionMessage.text = "Deducted by admin".localized()
            }
            else{
                lblTransactionMessage.text = "League Joined".localized()
            }
        }
        else if transactionDetails!.transactionType == "3" {
            lblTransactionMessage.text = "Refund Received".localized()
        }
        else if transactionDetails!.transactionType == "4" {
            lblTransactionMessage.text = "Withdrawal Requested".localized()
        }
        else if transactionDetails!.transactionType == "5" {
            lblTransactionMessage.text = "Promotion recieved".localized()
        }
        else if transactionDetails!.transactionType == "6" {
            lblTransactionMessage.text = "Cash Deposited".localized()
        }
        else if transactionDetails!.transactionType == "7" {
            lblTransactionMessage.text = "Withdrawal Reversed".localized()
        }
        else if transactionDetails!.transactionType == "8" {
            lblTransactionMessage.text = "You have been referred".localized()
        }
        else if transactionDetails!.transactionType == "9" {
            lblTransactionMessage.text = "Referral bonus recived".localized()
        }
        else if transactionDetails!.transactionType == "10" {
            lblTransactionMessage.text = "Signup Bonus".localized()
        }
        else if transactionDetails!.transactionType == "11" {
            lblTransactionMessage.text = "Earned for referring".localized()
        }
        else if transactionDetails!.transactionType == "12" {
            lblTransactionMessage.text = "Cash Redeemed Successfully".localized()
        }else if transactionDetails!.transactionType == "13" {
            lblTransactionMessage.text = "Bonus Redeemed Successfully".localized()
        }
        else if transactionDetails!.transactionType == "14" {
            lblTransactionMessage.text = "Congrats! Winning Received".localized()
        }
        else if transactionDetails!.transactionType == "15" {
            lblTransactionMessage.text = "Joined with ticket".localized()
        }
        else if transactionDetails!.transactionType == "16" {
            lblTransactionMessage.text = "Withdrawal Cancelled".localized()
        }
        else if transactionDetails!.transactionType == "17" {
            lblTransactionMessage.text = "Unused Cash Credited Successfully".localized()
        }
        else if transactionDetails!.transactionType == "24" {
            lblTransactionMessage.text = "Received a season pass".localized()
        }
        else if transactionDetails!.transactionType == "25" {
            lblTransactionMessage.text = "Joined with pass".localized()
        }
        else if transactionDetails!.transactionType == "26" {
            lblTransactionMessage.text = "Joined a quiz".localized()
        }
        else if transactionDetails!.transactionType == "27" {
            lblTransactionMessage.text = "Quiz Refund Received".localized()
        }
        else if transactionDetails!.transactionType == "28" {
            lblTransactionMessage.text = "Won Quiz".localized()
        }
        else{
            lblTransactionMessage.text = transactionDetails!.transactionMessage
        }

        if (transactionDetails!.transactionType == "2") || (transactionDetails!.transactionType == "15"){
            lblAmount.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
            lblAmount.text = "pts" + AppHelper.makeCommaSeparatedDigitsWithString(digites: transactionDetails!.transactionAmount)
        }
        else if transactionDetails?.transactionType == "4" {
            lblAmount.textColor = UIColor(red: 6.0/255, green: 106.0/255, blue: 196.0/255, alpha: 1)
            lblAmount.text = "pts" + AppHelper.makeCommaSeparatedDigitsWithString(digites: transactionDetails!.transactionAmount)
        }
        else{
            
            lblAmount.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
            lblAmount.text = "pts" + AppHelper.makeCommaSeparatedDigitsWithString(digites: transactionDetails!.transactionAmount)
        }
        
        if transactionDetails?.isNegativeTranaction ?? false {
            lblAmount.text = "-" + (lblAmount.text ?? "")
            lblAmount.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        else{
            if transactionDetails?.transactionType == "4"{
                lblAmount.textColor = UIColor(red: 6.0/255, green: 106.0/255, blue: 196.0/255, alpha: 1)
                lblAmount.text = "-" + (lblAmount.text ?? "")
            }
            else{
                lblAmount.text = "+" + (lblAmount.text ?? "")
            }
        }
    }
    
    
    func updateRewardTransactionDetails() {
        imgViewCoin.isHidden = false
        lblTransactionDate.text = transactionDetails!.transactionDate

        if transactionDetails!.leagueName.count == 0 {
            leagueName.text = "N/A"
        }
        else{
            leagueName.text = transactionDetails!.leagueName
        }
        
        if transactionDetails!.matchName.count == 0 {
            matchName.text = "N/A"
        }
        else{
            matchName.text = transactionDetails!.matchName
        }
        
        lblAmount.text =
            AppHelper.makeCommaSeparatedDigitsWithString(digites: transactionDetails!.bbCoins)
                
                
        if transactionDetails!.transactionType == "1" {
            lblTransactionMessage.text = "LP Coins Earned".localized()
        }
        else if transactionDetails!.transactionType == "2" {
            lblTransactionMessage.text = "LP Coins Redeemed".localized()

        }
        else{
            lblTransactionMessage.text = transactionDetails!.transactionMessage
        }
        
        if transactionDetails?.isNegativeTranaction ?? false {
            lblAmount.text = "-" + (lblAmount.text ?? "")
            lblAmount.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        else{
            lblAmount.text = "+" + (lblAmount.text ?? "")
            lblAmount.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
        }
    }
    
    @IBAction func tapGestureTapped(_ sender: Any) {
        hideViewWithAnimation()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        hideViewWithAnimation()
    }
    
    func hideViewWithAnimation() {
        self.bottomConstraint.constant = -420
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    func showAnimation() {
        
        self.bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
            self.layoutIfNeeded()
        })
    }
}
