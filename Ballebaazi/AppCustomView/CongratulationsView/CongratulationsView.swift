//
//  CongratulationsView.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class CongratulationsView: UIView {

    @IBOutlet weak var lblCongratulations: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("CongratulationsView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            lblMessage.text = "Welcome to the partnership program".localized()
            lblCongratulations.text = "Congratulations!".localized()
            
            addSubview(view)
        }
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        removeFromSuperview();
    }
    
}
