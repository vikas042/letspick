//
//  ClaimRewardPopup.swift
//  Letspick
//
//  Created by Vikash Rajput on 30/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class ClaimRewardPopup: UIView {

    @IBOutlet weak var lblCongratulains: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("ClaimRewardPopup", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
             addSubview(view)
        }
    }

}
