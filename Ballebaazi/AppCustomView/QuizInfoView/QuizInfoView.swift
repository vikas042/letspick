//
//  QuizInfoView.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/04/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class QuizInfoView: UIView {

    @IBOutlet weak var lblQuestions: UILabel!
    @IBOutlet weak var lblPlayers: UILabel!
    @IBOutlet weak var lblBonusApplication: UILabel!
    @IBOutlet weak var lblWinningDistribution: UILabel!
    
    @IBOutlet weak var lblSportKnowledge: UILabel!
    // MARK: init
    required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)!
      if self.subviews.count == 0 {
          setup()
      }
    }

    override init(frame: CGRect) {
      super.init(frame: frame)
      setup()
    }

    func setup() {
      
      if let view = Bundle.main.loadNibNamed("QuizInfoView", owner: self, options: nil)?.first as? UIView {
          view.frame = bounds
          view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
          view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
          addSubview(view)
      }
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        removeFromSuperview()
    }
    
    func updateInfo(leagueDetails: LeagueDetails) {
        lblWinningDistribution.text = "DynamicLeagueMessage".localized()
        lblQuestions.text = leagueDetails.totalQuestions + "Questions to answer".localized()
        if leagueDetails.bonusApplicable == "2" && leagueDetails.bounsPercentage != "0" && leagueDetails.bounsPercentage != "" {
            let bonusMessage = leagueDetails.bounsPercentage + "bonusMessage".localized()
            lblBonusApplication.text = bonusMessage
        }
        else{
            lblBonusApplication.text = "Bonus applicable".localized()
        }
        lblSportKnowledge.text = "UseSportKnowledgue".localized()
        
        
        
        if (leagueDetails.dynamicLeague == "2"){
            lblPlayers.text = "Limit of max \(leagueDetails.maxPlayers) players"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    lblPlayers.text = "\(leagueDetails.maxPlayers) खिलाड़ियों तक सीमित।"
                }
            }
        }
        else{
            lblPlayers.text = "Play with \(leagueDetails.maxPlayers) players"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    lblPlayers.text = "\(leagueDetails.maxPlayers) खिलाड़ियों के साथ खेलें"
                }
            }
        }

    }
    
}
