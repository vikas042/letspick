//
//  BasketballPlayerPointsView.swift
//  Letspick
//
//  Created by Vikash Rajput on 20/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class BasketballPlayerPointsView: UIView {
    

    @IBOutlet weak var pointScoredTitle: UILabel!
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var playerRoleImgView: UIImageView!
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var containerBottomConstraintView: NSLayoutConstraint!
    @IBOutlet weak var lblTotalPoints: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblPlayerRole: UILabel!
    @IBOutlet weak var lblScoredPlayerPoints: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
        
    @IBOutlet weak var lblPointScoreTitle: UILabel!
    @IBOutlet weak var lblReboundsTitle: UILabel!
    @IBOutlet weak var lblAssistsTitle: UILabel!
    @IBOutlet weak var lblStealTitle: UILabel!
    @IBOutlet weak var lblBlocksTitle: UILabel!
    @IBOutlet weak var lblTurnoversTitle: UILabel!

    @IBOutlet weak var lblPointScoreActualPoint: UILabel!
    @IBOutlet weak var lblReboundsActualPoint: UILabel!
    @IBOutlet weak var lblAssistsActualPoint: UILabel!
    @IBOutlet weak var lblStealActualPoint: UILabel!
    @IBOutlet weak var lblBlocksActualPoint: UILabel!
    @IBOutlet weak var lblTurnoversActualPoint: UILabel!
    
    @IBOutlet weak var lblPointScorePoint: UILabel!
    @IBOutlet weak var lblReboundsPoint: UILabel!
    @IBOutlet weak var lblAssistsPoint: UILabel!
    @IBOutlet weak var lblStealPoint: UILabel!
    @IBOutlet weak var lblBlocksPoint: UILabel!
    @IBOutlet weak var lblTurnoversPoint: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        if  let entireView =  Bundle.main.loadNibNamed("BasketballPlayerPointsView", owner: self, options: nil)?.first as? UIView{
            entireView.frame = bounds
            entireView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            addSubview(entireView)
            entireView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            lblTotalTitle.text = "Total".localized()
            pointScoredTitle.text = "Points Scored".localized()
            lblPointScoreTitle.text = "Points Scored".localized()
            
            lblReboundsTitle.text = "Rebounds".localized()
            lblAssistsTitle.text = "Assists".localized()
            lblStealTitle.text = "Steals".localized()
            lblBlocksTitle.text = "Blocks".localized()
            lblTurnoversTitle.text = "Turnovers".localized()

            setupView()
        }
    }
    
    func setupView(){
        playerRoleImgView.layer.cornerRadius = 17.0
        let rectShape = CAShapeLayer()
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 25.0, height: 25.0)).cgPath
        containerView.layer.mask = rectShape
    }
    
    func configData(details: PlayerDetails) {
//        if UIScreen.main.bounds.size.height > 667{
//            containerBottomConstraintView.constant = 635
//            containerView.layoutIfNeeded()
//        }
        
        lblPlayerName.text = details.playerName
        var totalPoints = details.totalClasscPlayerScore
        
        lblCaptain.text = ""
        if details.isCaption{
            totalPoints = details.totalClassicCaptainScore
            lblCaptain.text = "X2(" + "Captain".localized() + ")"
            lblPlayerName.text = details.playerName! + "(C)"
            
        }
        else if details.isViceCaption {
            totalPoints = details.totalClassicViceCaptainScore
            lblCaptain.text = "X1.5(" + "subCaptain".localized() + ")"
            lblPlayerName.text = details.playerName! + "(VC)"
        }
 
        lblScoredPlayerPoints.text = details.playerScore
        lblPointScoreActualPoint.text = details.playerScore
        lblReboundsActualPoint.text = details.reboundsActual
        lblAssistsActualPoint.text = details.assistActual
        lblStealActualPoint.text = details.stealsActual
        lblBlocksActualPoint.text = details.blocksActual
        lblTurnoversActualPoint.text = details.turnoversActual
        lblPointScorePoint.text = details.playerScore
        lblReboundsPoint.text = details.rebounds
        lblAssistsPoint.text = details.assist
        lblStealPoint.text = details.steals
        lblBlocksPoint.text = details.blocks
        lblTurnoversPoint.text = details.turnovers
        lblScoredPlayerPoints.text = totalPoints
        lblTotalPoints.text = totalPoints
        
        
        var playerPlayingRole = details.seasonalRole
        if playerPlayingRole.count == 0 {
            playerPlayingRole = details.playerPlayingRole ?? ""
        }

        if playerPlayingRole == PlayerType.PointGuard.rawValue {
            lblPlayerRole.text = "Point-Guard".localized()
        }
        else if playerPlayingRole == PlayerType.ShootingGuard.rawValue {
            lblPlayerRole.text = "Shooting-Guard".localized()
        }
        else if playerPlayingRole == PlayerType.SmallForward.rawValue {
            lblPlayerRole.text = "Small-Forward".localized()
        }
        else if playerPlayingRole == PlayerType.PowerForward.rawValue {
            lblPlayerRole.text = "Power-Forward".localized()
        }
        else if playerPlayingRole == PlayerType.Center.rawValue {
            lblPlayerRole.text = "Center".localized()
        }
        
        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerRoleImgView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerRoleImgView.image = image
                    }
                    else{
                        self?.playerRoleImgView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }
    }
    
    @IBAction func reduceButtonAction(_ sender: Any?) {
        self.containerBottomConstraintView.constant = -500
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    func showAnimation() {
        self.containerBottomConstraintView.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
        })
    }
    
    @IBAction func swipeGestureTapped(_ sender: Any) {
        reduceButtonAction(nil)
    }
    
}
