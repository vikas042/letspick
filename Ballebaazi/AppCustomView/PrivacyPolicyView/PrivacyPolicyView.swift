//
//  PrivacyPolicyView.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/5/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit


class PrivacyPolicyView: UIView {

    var agressTermAndConditionBlock = {(sucess: Bool) -> () in }
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("PrivacyPolicyView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            addSubview(view)
        }
    }
    
 
    @IBAction func agreeButtonTapped(_ sender: Any) {
        removeFromSuperview()
        agressTermAndConditionBlock(true)
    }
    
    @IBAction func donotAgreeButtonTapped(_ sender: Any) {
        removeFromSuperview()
        agressTermAndConditionBlock(false)
    }
    
    @IBAction func termsAndConditionButtonTapped(_ sender: Any) {
        removeViewWithAnimation()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        removeViewWithAnimation()
    }
    
    @IBAction func privacyPolicyButtonTapped(_ sender: Any) {
        removeViewWithAnimation()
    }
    
    func getUserStatusOnPrivacyPolicy(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        agressTermAndConditionBlock = complationBlock
    }
    
    
    func removeViewWithAnimation()  {
        weak var weakSelf = self

        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseIn, animations: {
            weakSelf?.alpha = 0
        }) { _ in
            weakSelf?.removeFromSuperview()
        }
    }
}
