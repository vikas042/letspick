//
//  LetspickCreateTeamHeaderView.swift
//  Letspick
//
//  Created by Vikash Rajput on 03/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LetspickCreateTeamHeaderView: UIView {

    @IBOutlet weak var lblCaptainTitle: UILabel!
    @IBOutlet weak var lblDoublePoint: UILabel!
    @IBOutlet weak var lblViceCaptain: UILabel!
    @IBOutlet weak var lblViceCaptainPoints: UILabel!
    @IBOutlet weak var lblVs: UILabel!
    @IBOutlet weak var lblSelectCaptains: UILabel!
    @IBOutlet weak var secondTeamImgView: UIImageView!
    @IBOutlet weak var firstTeamImgView: UIImageView!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblSecondTeamName: UILabel!
    @IBOutlet weak var lblFirstTeamName: UILabel!
    @IBOutlet weak var lblSelectedPlayerCount: UILabel!
    @IBOutlet weak var lblCreditLeft: UILabel!
    @IBOutlet weak var totalPlayerCount: UILabel!
    @IBOutlet weak var lblSelectedTitle: UILabel!
    @IBOutlet weak var lblCreditLeftitle: UILabel!
    @IBOutlet weak var lblFirstTeamPlayerCount: UILabel!
    @IBOutlet weak var lblSecondTeamPlayerCount: UILabel!
    @IBOutlet weak var createTeamView: UIView!
    @IBOutlet weak var selectCaptainView: UIView!
    @IBOutlet weak var watchIcon: UIImageView!
    @IBOutlet weak var lblViceCaptainName: UILabel!
    @IBOutlet weak var lblCaptainName: UILabel!

    var isViewForSelectCaptain = false

    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("LetspickCreateTeamHeaderView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            lblSelectedTitle.text = "Selected".localized()
            lblCreditLeftitle.text = "Credit".localized()
            
            lblCaptainTitle.text = "Captain".localized()
            lblViceCaptain.text = "subCaptain".localized()
            
            lblDoublePoint.text = "2x" + "Point".localized()
            lblViceCaptainPoints.text = "1.5x" + "Point".localized()
            lblSelectCaptains.text = "SelectedCaptain".localized()    

            addSubview(view)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.popViewController(animated: true)
        }
    }
    
    // MARK:- Timer Handlers

    func updateMatchName(matchDetails: MatchDetails?) {
        
        if isViewForSelectCaptain{
            createTeamView.isHidden = true
            selectCaptainView.isHidden = false
        }
        else{
            createTeamView.isHidden = false
            selectCaptainView.isHidden = true
        }
        
        guard let details = matchDetails else {
            lblTimer.text = ""
            lblFirstTeamName.text = ""
            lblSecondTeamName.text = ""
            return;
        }
        
        lblFirstTeamName.text = details.firstTeamShortName
        lblSecondTeamName.text = details.secondTeamShortName
        lblVs.isHidden = false

        if details.isMatchTourney{
            firstTeamImgView.isHidden = true
            secondTeamImgView.isHidden = true
            lblSelectCaptains.isHidden = false
            lblFirstTeamPlayerCount.isHidden = true
            lblSecondTeamPlayerCount.isHidden = true

            lblVs.isHidden = true
            lblSelectCaptains.text = details.matchShortName
            lblFirstTeamName.text = ""
            lblSecondTeamName.text = ""
        }

        
        if details.isMatchClosed  {
            lblTimer.text = "Leagues Closed".localized()
        }
        else{
            lblTimer.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: details.startDateTimestemp)
        }
    }
    
    func updateData(details: MatchDetails?) {
        lblFirstTeamName.text = details?.firstTeamShortName
        lblSecondTeamName.text = details?.secondTeamShortName
        if details?.isMatchTourney ?? false{
            firstTeamImgView.isHidden = true
            secondTeamImgView.isHidden = true
            lblSelectCaptains.isHidden = false
            lblFirstTeamPlayerCount.isHidden = true
            lblSecondTeamPlayerCount.isHidden = true

            lblVs.isHidden = true
            lblSelectCaptains.text = details?.matchShortName
            lblFirstTeamName.text = ""
            lblSecondTeamName.text = ""
        }
    }
    
    func updateTimerValue(matchDetails: MatchDetails)  {
        
        if matchDetails.isMatchClosed  {
            lblTimer.text = "Leagues Closed".localized()
        }
        else if UInt64((matchDetails.startDateTimestemp)!) != nil{
            lblTimer.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: matchDetails.startDateTimestemp)
        }
    }
    
    func setupDefaultPropertiesForCaptainVicecaptaion(fantasyType: String) {
        lblCaptainName.text = "--"
        lblViceCaptainName.text = "--"
        
        watchIcon.isHidden = true
        lblVs.isHidden = true;
        lblSelectCaptains.isHidden = false

        totalPlayerCount.isHidden = true
        lblFirstTeamPlayerCount.isHidden = true
        lblSecondTeamPlayerCount.isHidden = true
        firstTeamImgView.isHidden = true
        secondTeamImgView.isHidden = true
        lblFirstTeamName.isHidden = true
        lblSecondTeamName.isHidden = true
        lblTimer.isHidden = true
        selectCaptainView.isHidden = false
        createTeamView.isHidden = true
        if fantasyType == "4" {
            lblDoublePoint.text = "0.5x" + "Point".localized()
            lblViceCaptainPoints.text = "0.75x" + "Point".localized()
        }
        else{
            lblDoublePoint.text = "2x" + "Point".localized()
            lblViceCaptainPoints.text = "1.5x" + "Point".localized()
        }
    }
    
    func setupDefaultProperties(matchDetails: MatchDetails?, fantasyType: String, gameType: Int) {
        lblCaptainName.text = "--"
        lblViceCaptainName.text = "--"
        if fantasyType == "4" {
            lblDoublePoint.text = "0.5x" + "Point".localized()
            lblViceCaptainPoints.text = "0.75x" + "Point".localized()
        }
        else{
            lblDoublePoint.text = "2x" + "Point".localized()
            lblViceCaptainPoints.text = "1.5x" + "Point".localized()
        }
        
        if isViewForSelectCaptain {
            selectCaptainView.isHidden = false
            createTeamView.isHidden = true
        }
        else{
            selectCaptainView.isHidden = true
            createTeamView.isHidden = false
        }
        if gameType == GameType.Cricket.rawValue {
            if fantasyType == "1" {
                totalPlayerCount.text = "/11"
            }
            else if fantasyType == "2" {
                totalPlayerCount.text = "/5"
            }
            else if fantasyType == "3" {
                totalPlayerCount.text = "/5"
            }
            else if fantasyType == "4" {
                totalPlayerCount.text = "/11"
            }
            else if fantasyType == "5" {
                totalPlayerCount.text = "/11"
            }
        }
        else if gameType == GameType.Kabaddi.rawValue {
            totalPlayerCount.text = "/7"
        }
        else if gameType == GameType.Football.rawValue {
            totalPlayerCount.text = "/11"
        }
        else if gameType == GameType.Basketball.rawValue {
            totalPlayerCount.text = "/8"
        }
        else if gameType == GameType.Baseball.rawValue {
            totalPlayerCount.text = "/9"
        }

        let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }

        lblFirstTeamPlayerCount.text = String(totalPlayerFromTeamArray.count)
        lblSecondTeamPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count - totalPlayerFromTeamArray.count)
        
        if let url = NSURL(string: matchDetails!.firstTeamImageUrl){
            firstTeamImgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.firstTeamImgView.image = image
                }
                else{
                    self?.firstTeamImgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.firstTeamImgView.image = UIImage(named: "Placeholder")
        }
        
        if let url = NSURL(string: matchDetails!.secondTeamImageUrl){
            secondTeamImgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.secondTeamImgView.image = image
                }
                else{
                    self?.secondTeamImgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.secondTeamImgView.image = UIImage(named: "Placeholder")
        }
        
        lblFirstTeamName.text = matchDetails!.firstTeamShortName
        lblSecondTeamName.text = matchDetails!.secondTeamShortName
        if matchDetails!.isMatchTourney{
            firstTeamImgView.isHidden = true
            secondTeamImgView.isHidden = true
            lblSelectCaptains.isHidden = false
            lblFirstTeamPlayerCount.isHidden = true
            lblSecondTeamPlayerCount.isHidden = true
            lblVs.isHidden = true
            lblSelectCaptains.text = matchDetails!.matchShortName
            lblFirstTeamName.text = ""
            lblSecondTeamName.text = ""
        }
    }
    
    func updatePlayerSelectionData(matchDetails: MatchDetails, fantasyType: String)  {
        
        let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == matchDetails.firstTeamShortName
        }
        
        lblFirstTeamName.text = matchDetails.firstTeamShortName
        lblSecondTeamName.text = matchDetails.secondTeamShortName
        
        if matchDetails.isMatchTourney{
            firstTeamImgView.isHidden = true
            secondTeamImgView.isHidden = true
            lblSelectCaptains.isHidden = false
            lblFirstTeamPlayerCount.isHidden = true
            lblSecondTeamPlayerCount.isHidden = true
            lblVs.isHidden = true
            lblSelectCaptains.text = matchDetails.matchShortName
            lblFirstTeamName.text = ""
            lblSecondTeamName.text = ""
        }

        lblFirstTeamPlayerCount.text = String(totalPlayerFromTeamArray.count)
        lblSecondTeamPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count - totalPlayerFromTeamArray.count)
        
        let totalPoints = UserDetails.sharedInstance.selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            
            creditSum + Float(nextPlayerDetails.credits)!
        })
        
        if fantasyType == "1" {
            lblSelectedPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count)
            let remaingCredit = Float(UserDetails.sharedInstance.maxCreditLimitForClassic) - Float(totalPoints)
            lblCreditLeft.text = String(remaingCredit)
        }
        else if fantasyType == "2" {
            lblSelectedPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count)
            let remaingCredit = Float(UserDetails.sharedInstance.maxCreditLimitForBatting) - Float(totalPoints)
            lblCreditLeft.text = String(remaingCredit)
        }
        else if fantasyType == "4" {
            lblSelectedPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count)
            let remaingCredit = Float(UserDetails.sharedInstance.maxCreditLimitForClassic) - Float(totalPoints)
            lblCreditLeft.text = String(remaingCredit)
        }
        else if fantasyType == "5" {
            lblSelectedPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count)
            let remaingCredit = Float(UserDetails.sharedInstance.maxCreditLimitForClassic) - Float(totalPoints)
            lblCreditLeft.text = String(remaingCredit)
        }
        else{
            lblSelectedPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count)
            let remaingCredit = Float(UserDetails.sharedInstance.maxCreditLimitForBowling) - Float(totalPoints)
            lblCreditLeft.text = String(remaingCredit)
        }
    }
    
    func updateCaptainAndViceCaptainName(captain: PlayerDetails?, viceCaptain: PlayerDetails?) {
        
        if captain != nil {
            lblCaptainName.text = captain?.playerName
        }
        else{
            lblCaptainName.text = "--"
        }
        
        if viceCaptain != nil {
            lblViceCaptainName.text = viceCaptain?.playerName
        }
        else{
            lblViceCaptainName.text = "--"
        }
    }

}
