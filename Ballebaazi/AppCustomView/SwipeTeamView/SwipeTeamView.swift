//
//  SwipeTeamView.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

protocol SwipeTeamViewDelegate {
    func refereshteamData(teamNumebr: String)
}

class SwipeTeamView: UIView, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var confirmButton: SolidButton!
    
    @IBOutlet weak var lblSelectTeamMessage: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!    
    var userTeamsArray:Array<UserTeamDetails>?
    var leagueUserDetails: JoinedLeagueUserList?
    var matchDetails: MatchDetails?
    var leagueDetails: JoinedLeagueDetails?
    var seleactedTeam: UserTeamDetails?
    var deleagte: SwipeTeamViewDelegate?
    var selectedGameType = GameType.Cricket.rawValue
    
    @IBOutlet weak var containerViewBottomConstraint: NSLayoutConstraint!
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("SwipeTeamView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.backgroundColor = UIColor.clear
            collectionView.register(UINib(nibName: "SwapTeamCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SwapTeamCollectionViewCell")
            collectionView.register(UINib(nibName: "SwapeKabaddiCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SwapeKabaddiCollectionViewCell")
            collectionView.register(UINib(nibName: "FootballSwapCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FootballSwapCollectionViewCell")
            collectionView.register(UINib(nibName: "BasketballSwapCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BasketballSwapCollectionViewCell")
            collectionView.register(UINib(nibName: "BaseballSwapCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BaseballSwapCollectionViewCell")

            
            
            confirmButton.setTitle("Confirm".localized(), for: .normal);
            lblSelectTeamMessage.text = "Select Team to Swap".localized()
            addSubview(view)
        }
    }
    
    func updateData(gameType: Int) {
        guard let teamsArray = userTeamsArray else {
            return;
        }
        if teamsArray.count > 0{
            seleactedTeam = teamsArray[0]
        }
        for details in teamsArray {
            if details.teamNumber == leagueUserDetails!.teamNumber!{
                seleactedTeam = details
            }
        }
        selectedGameType = gameType
        collectionView.reloadData()
    }
    
    @IBAction func tapGestureTapped(_ sender: Any) {
        hideViewWithAnimation()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        hideViewWithAnimation()
    }
    
    func hideViewWithAnimation()  {
        self.containerViewBottomConstraint.constant = -290
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    func showAnimation() {
        
        self.containerViewBottomConstraint.constant = -20
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
            self.layoutIfNeeded()
        })
    }

    @IBAction func swipeTeamButtonTapped(_ sender: Any) {
        
        if seleactedTeam == nil {
            return;
        }
        
        callSwipeTeamAPI()
    }
    
    @IBAction func termsAndConditionButtonTapped(_ sender: Any) {
        self.removeFromSuperview()
        hideViewWithAnimation()

        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
            
            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.HowToPlay.rawValue
            navVC.pushViewController(privacyPolicyVC!, animated: true)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    //MARK:- Table view Data and Delegats
    
    //MARK:- Collection View Data Source and Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if userTeamsArray == nil {
            return 0
        }
        return userTeamsArray!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if userTeamsArray == nil {
            return CGSize(width: 0, height: 0)
        }

        if userTeamsArray!.count == 1{
            return CGSize(width: UIScreen.main.bounds.width - 10, height: 125)
        }
        return CGSize(width: 290, height: 125)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if selectedGameType == GameType.Kabaddi.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SwapeKabaddiCollectionViewCell", for: indexPath) as? SwapeKabaddiCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewButtonTeamButton(button:)), for: .touchUpInside)
            
            let details = userTeamsArray![indexPath.row]
            cell?.configData(details, selectedTeam: seleactedTeam)
            return cell!;
        }
        else if selectedGameType == GameType.Football.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FootballSwapCollectionViewCell", for: indexPath) as? FootballSwapCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewButtonTeamButton(button:)), for: .touchUpInside)
            
            let details = userTeamsArray![indexPath.row]
            cell?.configData(details, selectedTeam: seleactedTeam)
            return cell!;
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BasketballSwapCollectionViewCell", for: indexPath) as? BasketballSwapCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewButtonTeamButton(button:)), for: .touchUpInside)
            let details = userTeamsArray![indexPath.row]
            cell?.configData(details, selectedTeam: seleactedTeam)
            return cell!;
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BaseballSwapCollectionViewCell", for: indexPath) as? BaseballSwapCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewButtonTeamButton(button:)), for: .touchUpInside)
            let details = userTeamsArray![indexPath.row]
            cell?.configData(details, selectedTeam: seleactedTeam)
            return cell!;
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SwapTeamCollectionViewCell", for: indexPath) as? SwapTeamCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewButtonTeamButton(button:)), for: .touchUpInside)
            
            let details = userTeamsArray![indexPath.row]
            cell?.configData(details, selectedTeam: seleactedTeam)
            return cell!;
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let details = userTeamsArray![indexPath.row]
        seleactedTeam = details
        collectionView.reloadData();
    }
    
    @objc func selectTeamButton(button: UIButton) {
        if userTeamsArray?.count == 0{
            return;
        }
        let details = userTeamsArray![button.tag]
        seleactedTeam = details
        collectionView.reloadData();
    }
    
    @objc func previewButtonTeamButton(button: UIButton) {
        if userTeamsArray?.count == 0{
            return;
        }
        
        let details = userTeamsArray![button.tag]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if selectedGameType == GameType.Kabaddi.rawValue {
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Kabaddi"])

            let teamPreviewVC = storyboard.instantiateViewController(withIdentifier: "KabaddiTeamPreviewViewController") as! KabaddiTeamPreviewViewController
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.isHideEditButton = true;
            if let playerList = details.playersArray {
                teamPreviewVC.totalPlayerArray = playerList
            }
            else{
                return;
            }
            hideViewWithAnimation()
            teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(teamPreviewVC, animated: true)
            }
        }
        if selectedGameType == GameType.Football.rawValue {
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Football"])

            let teamPreviewVC = storyboard.instantiateViewController(withIdentifier: "FootballTeamPreviewViewController") as! FootballTeamPreviewViewController
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.isHideEditButton = true;
            if let playerList = details.playersArray {
                teamPreviewVC.totalPlayerArray = playerList
            }
            else{
                return;
            }
            hideViewWithAnimation()
            teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            
            teamPreviewVC.firstTeamkey = matchDetails!.firstTeamKey
            teamPreviewVC.secondTeamkey = matchDetails!.secondTeamKey 

            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(teamPreviewVC, animated: true)
            }
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Basketball"])

            let teamPreviewVC = storyboard.instantiateViewController(withIdentifier: "BasketTeamPreviewViewController") as! BasketTeamPreviewViewController
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.isHideEditButton = true;
            if let playerList = details.playersArray {
                teamPreviewVC.totalPlayerArray = playerList
            }
            else{
                return;
            }
            hideViewWithAnimation()
            teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            
            teamPreviewVC.firstTeamkey = matchDetails!.firstTeamKey
            teamPreviewVC.secondTeamkey = matchDetails!.secondTeamKey

            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(teamPreviewVC, animated: true)
            }
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Baseball"])

            let teamPreviewVC = storyboard.instantiateViewController(withIdentifier: "BaseballTeamPreviewViewController") as! BaseballTeamPreviewViewController
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.isHideEditButton = true;
            if let playerList = details.playersArray {
                teamPreviewVC.totalPlayerArray = playerList
            }
            else{
                return;
            }
            hideViewWithAnimation()
            teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            
            teamPreviewVC.firstTeamkey = matchDetails!.firstTeamKey
            teamPreviewVC.secondTeamkey = matchDetails!.secondTeamKey

            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(teamPreviewVC, animated: true)
            }
        }
        else{
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Cricket"])
            let teamPreviewVC = storyboard.instantiateViewController(withIdentifier: "TeamPreviewViewController") as! TeamPreviewViewController
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.isHideEditButton = true;
            teamPreviewVC.matchDetails = matchDetails
            if let playerList = details.playersArray {
                teamPreviewVC.totalPlayerArray = playerList
            }
            else{
                return;
            }
            hideViewWithAnimation()
            if leagueDetails!.fantasyType == "1"{
                teamPreviewVC.selectedFantasy = FantasyType.Classic.rawValue
            }
            else if leagueDetails!.fantasyType == "2"{
                teamPreviewVC.selectedFantasy = FantasyType.Batting.rawValue
            }
            else if leagueDetails!.fantasyType == "3"{
                teamPreviewVC.selectedFantasy = FantasyType.Bowling.rawValue
            }
            else if leagueDetails!.fantasyType == "4"{
                teamPreviewVC.selectedFantasy = FantasyType.Reverse.rawValue
            }
            else if leagueDetails!.fantasyType == "5"{
                teamPreviewVC.selectedFantasy = FantasyType.Wizard.rawValue
            }

            teamPreviewVC.firstTeamkey = matchDetails!.firstTeamKey
            teamPreviewVC.secondTeamkey = matchDetails!.secondTeamKey
            teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(teamPreviewVC, animated: true)
            }
        }
    }

    
    
    
    func callSwipeTeamAPI() {
        hideViewWithAnimation()
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let params = ["option": "swap_team", "match_key": matchDetails!.matchKey, "league_id": leagueDetails!.leagueId!, "user_id": UserDetails.sharedInstance.userID, "old_team": leagueUserDetails!.teamNumber!, "fantasy_type": leagueDetails!.fantasyType!, "new_team": seleactedTeam!.teamNumber!] as [String : Any]
        var urlString = kMatch
        
        if selectedGameType == GameType.Kabaddi.rawValue {
            urlString = kKabaddiMatchURL
        }
        else if selectedGameType == GameType.Football.rawValue {
            urlString = kFootballMatchURL
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            urlString = kBasketballMatchURL
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            urlString = kBaseballMatchURL
        }

        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                
                if statusCode == "200"{
                    self.leagueUserDetails!.teamNumber = self.seleactedTeam!.teamNumber
                    self.deleagte?.refereshteamData(teamNumebr: self.seleactedTeam!.teamNumber ?? "")
                    AppHelper.showToast(message: "You have successfully swapped the team".localized())
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.gestureRecognizers != nil) {
            return false;
        }
        return true;
    }

}
