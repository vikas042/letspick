//
//  JoinLeagueTutorial.swift
//  Letspick
//
//  Created by Vikash Rajput on 12/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class JoinLeagueTutorial: UIView {

    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var containerViewbottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    var selectedGameType = GameType.Cricket.rawValue
    var selectedFansatyType = FantasyType.Classic.rawValue
 
    @IBOutlet weak var txtView: UITextView!
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("JoinLeagueTutorial", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
            
            let rectShape = CAShapeLayer()
            rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 25.0, height: 25.0)).cgPath
            containerView.layer.mask = rectShape

        }
    }
    
    func configData() {
        
        
        var htmlString = ""
        if selectedGameType == GameType.Cricket.rawValue {
            if selectedFansatyType == FantasyType.Classic.rawValue{
                UserDefaults.standard.set(Date(), forKey: "classicBanner")
                imgView.image = UIImage(named: "ClasscicCricketJoinLeagueTutorail")
                lblTitle.text = "Classic Fantasy"
                
                htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>Make a team of 11 players</li> <li>Choose 1-4 Wicket-Keepers, 3-6 Batsmen,\n1-4 All-Rounders & 3-6 Bowlers</li><li>Use upto 100 credits to create team</li> <li>Maximum of 7 players from one team</li> <li>Get points for batting, bowling & fielding performances of your playing 11 </li> </ul>"
                
                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{

                        htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>11 खिलाड़ियों की एक टीम बनायें</li> <li>1-4 विकेट कीपर, 3 से 6 बल्लेबाज़ों, 1 से 4 आल राउंडर व 3 से 6 गेंदबाज़ चुनें</li> <li>1टीम बनाने के लिए 100 क्रेडिट तक का उपयोग करें।</li> <li>एक टीम से अधिकतम 7 खिलड़ी</li> <li>बल्लेबाज़ी , गेंदबाज़ी व फ़ेईल्दिंग प्रदर्शन के अंक ले सकते हैं </li> </ul>"
                    }
                }
            }
            else if selectedFansatyType == FantasyType.Batting.rawValue{
                lblTitle.text = "Batting Fantasy"
                imgView.image = UIImage(named: "BattingCricketJoinLeagueTutorail")
                UserDefaults.standard.set(Date(), forKey: "battingBanner")
                htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>Make a team of 5 Batsmen </li> <li>More runs = More points</li> <li>Use upto 45 credits</li></li> <li>Maximum of 3 players from one team </li></li> <li>Get points only for batting performances </li></li> <li>You can choose more than 1 wicket-keeper </li> </ul>"

                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{
                        htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>5 बल्लेबाज़ों की एक टीम बनायें</li> <li>अधिक रन = अधिक अंक</li> <li>45 क्रेडिट तक का उपयोग कर सकते हैं</li></li> <li>अधिकतम 3 खिलाडी एक टीम बनाते हैं</li></li> <li>बल्लेबाज़ी प्रदर्शन के अंक ले सकते हैं</li></li> <li>एक से अधिक विकेट कीपर चुन सकते हैं</li> </ul>"
                    }
                }
                
            }
            else if selectedFansatyType == FantasyType.Bowling.rawValue{
                UserDefaults.standard.set(Date(), forKey: "bowlingBanner")
                lblTitle.text = "Bowling Fantasy"
                imgView.image = UIImage(named: "BowlingCricketJoinLeagueTutorail")
                
                htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>Make a team of 5 Bowlers </li> <li>More wickets = More points </li> <li>Use upto 45 credits</li> <li>Maximum of 3 players from one team </li> <li>Get points only for bowling & fielding performances</li> <li>You cannot choose wicket-keepers (because, obviously!)</li> </ul>"
                
                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{
                        htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>5 गेंबाजो के टीम बनायें</li> <li>अधिक विकेट = अधिक अंक</li> <li>45 क्रेडिट तक का उपयोग कर सकते हैं</li> <li>अधिकतम 3 खिलाडी एक टीम बनाते हैं</li> <li>केवल गेंदबाज़ी , फील्डिंग प्रदर्शन के अंक ले सकते हैंs</li> <li>विकेट कीपर नहीं चुन सकते</li> </ul>"
                    }
                }
            }
            else if selectedFansatyType == FantasyType.Reverse.rawValue{
                UserDefaults.standard.set(Date(), forKey: "ReverseBanner")
                imgView.image = UIImage(named: "ReverseFantasyTutorial")
                lblTitle.text = "Reverse Fantasy".localized()

                htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>Make a team of 11 players</li> <li>Choose 1-4 Wicket-Keepers, 3-6 Batsmen,\n1-4 All-Rounders & 3-6 Bowlers</li><li>Use upto 100 credits to create team</li> <li>Maximum of 7 players from one team</li> <li>Get points for batting, bowling & fielding performances of your playing 11 </li> <li>Choose your worst 11 players. The team with the least fantasy score will be the winner</li> </ul>"
                
                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{
                        
                        htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>11 खिलाड़ियों की एक टीम बनायें</li> <li>1-4 विकेट कीपर, 3 से 6 बल्लेबाज़ों, 1 से 4 आल राउंडर व 3 से 6 गेंदबाज़ चुनें</li> <li>1टीम बनाने के लिए 100 क्रेडिट तक का उपयोग करें।</li> <li>एक टीम से अधिकतम 7 खिलड़ी</li> <li>बल्लेबाज़ी , गेंदबाज़ी व फ़ेईल्दिंग प्रदर्शन के अंक ले सकते हैं </li> <li>अपने सबसे ख़राब 11 खिलाड़ी चुनें, सबसे कम फैंटसी स्कोर वाली टीम विजेता होगी।</li> </ul>"
                    }
                }
            }
            else if selectedFansatyType == FantasyType.Wizard.rawValue{
                UserDefaults.standard.set(Date(), forKey: "WizardBanner")
                imgView.image = UIImage(named: "WizardFantasyTutorial")
                lblTitle.text = "Wizard Fantasy".localized()
                htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>Make a team of 11 players</li> <li>Choose 1-4 Wicket-Keepers, 3-6 Batsmen,\n1-4 All-Rounders & 3-6 Bowlers</li><li>Use upto 100 credits to create team</li> <li>Maximum of 7 players from one team</li> <li>Get points for batting, bowling & fielding performances of your playing 11 </li> <li>Captain will get 2x Points, V Captain will get 1.5x, Wizard will get 3x, Wizard Captain 5x and Wizard V Captain 4.5x </li> <li>Choose a Wizard whom you think will perform the best in your playing 11</li> </ul>"
                containerViewHeightConstraint.constant = 570
                textFieldHeightContraint.constant = 300
                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{
                        containerViewHeightConstraint.constant = 540
                        textFieldHeightContraint.constant = 250
                        htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>11 खिलाड़ियों की एक टीम बनायें</li> <li>1-4 विकेट कीपर, 3 से 6 बल्लेबाज़ों, 1 से 4 आल राउंडर व 3 से 6 गेंदबाज़ चुनें</li> <li>1टीम बनाने के लिए 100 क्रेडिट तक का उपयोग करें।</li> <li>एक टीम से अधिकतम 7 खिलड़ी</li> <li>बल्लेबाज़ी , गेंदबाज़ी व फ़ेईल्दिंग प्रदर्शन के अंक ले सकते हैं </li> <li>कप्तान को 2x अंक मिलेंगे, उप-कप्तान को 1.5x मिलेंगे, विज़ार्ड को 3x, विज़ार्ड कप्तान 5x और उप-कप्तान 4.5x मिलेंगे। </li> <li>एक विज़ार्ड चुनें, जो आपको लगता है कि आपके खेल रहे 11 में सर्वश्रेष्ठ प्रदर्शन कर सकता है।</li> </ul>"
                    }
                }
            }
        }
        else if selectedGameType == GameType.Kabaddi.rawValue {
            UserDefaults.standard.set(Date(), forKey: "kabaddiBanner")
            imgView.image = UIImage(named: "KabaddiJoinLeagueTutorail")

            lblTitle.text = "Kabaddi Fantasy"
            htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>Make a team of 7 Players </li> <li>Choose Max 5 players from 1 team</li> <li>Use upto 100 credits to create team</li> <li>Choose 2-4 defenders,1-2 all-rounders & 1-3 raiders</li> <li>Get points for every successful tackle, super tackle, raid & push-all out.</li></ul>"
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{

                    htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>7 खिलाडियों की टीम बनायें</li> <li>एक टीम से अधिकतम 5 खिलाड़ि चुनें</li> <li>1टीम बनाने के लिए 100 क्रेडिट तक का उपयोग करें।</li> <li>2 से 4 डिफेंडर , 1 से 2 आल राउंडर व 1 से 3 रेडर चुनें</li> <li>हर सफल टैकल, सुपर टैकल, रेड और पुश-ऑल से अंक प्राप्त करें</li></ul>"
                }
            }
        }
        else if selectedGameType == GameType.Football.rawValue {
            
            UserDefaults.standard.set(Date(), forKey: "footballBanner")
            imgView.image = UIImage(named: "FootballTutorialIcon")
            lblTitle.text = "Football Fantasy".localized()

            htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>Make a team of 11 players</li> <li>Choose 1 Goal-Keeper, 3-5 Defenders, 3-5 Mid-Fielder & 1-3 Strikers</li> <li>Use upto 100 credits to create team to create team</li> <li>Maximum of 7 players from one team</li> <li>Get points for successful tackle, successful assist & shots on target of your playing 11</li></ul>"
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    
                    htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>11 खिलड़ियों की एक टीम बनायें</li> <li>1 गोल कीपर, 3 से 5 डिफेंडर, 1 से 3 स्ट्राइकर व 3 से 5 मिड-फील्डर चुनें</li> <li>1टीम बनाने के लिए 100 क्रेडिट तक का उपयोग करें।</li> <li>एक टीम से अधिकतम 7 खिलाडी</li> <li>अपने प्लेइंग 11 के लक्ष्य पर सफल टैकल, सफल असिस्ट और शॉट्स के लिए अंक प्राप्त करें</li></ul>"
                }
            }
            
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            
            UserDefaults.standard.set(Date(), forKey: "basketballBanner")
            imgView.image = UIImage(named: "BasketballTutorialIcon")
            lblTitle.text = "Basketball Fantasy".localized()

            htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>Make a team of 8 players</li> <li>Choose 1-4 Point-Guard, 1-4 Shooting-Guard, 1-4 Small-Forward, 1-4 Power-Forward & 1-4 Center</li> <li>Use upto 100 credits to create team to create team</li> <li>Maximum of 5 players from one team</li> <li>Get points for the performances of your 8 players</li></ul>"
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    
                    htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>8 खिलड़ियों की एक टीम बनायें</li> <li>1-4 पॉइंट-गार्ड चुनें, 1-4 शूटिंग-गार्ड चुनें,\n1-4 स्मॉल-फॉरवर्ड चुनें,1-4 पावर-फॉरवर्ड चुनें & 1-4 सेंटर</li> <li>1टीम बनाने के लिए 100 क्रेडिट तक का उपयोग करें।</li> <li>एक टीम से अधिकतम 5 खिलाडी</li> <li>सफल टैकल, असिस्ट और अपने खेल रहे 8 के सही निशाने साधने पर अंक पाएं।</li></ul>"
                }
            }
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            
            UserDefaults.standard.set(Date(), forKey: "baseballBanner")
            imgView.image = UIImage(named: "BaseballTutorialIcon")
            lblTitle.text = "Baseball Fantasy".localized()

            htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>Make a team of 9 players</li> <li>Choose 2-5 Outfielder, 2-5 Infielder, 1 Pithcer, 1 Catcher</li> <li>Use upto 100 credits to create team to create team</li> <li>Maximum of 6 players from one team</li> <li>Get points for batting,pitching and fielding performances of your playing 9</li></ul>"
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    
                    htmlString = "<style> ul li{position: relative; list-style-type: none; color: #7F8486; font-size: 11pt; font-family: 'OpenSans';} ul li::before{position: absolute;  content: '';  width: 12px;  height: 12px;  background-color: #288DE9; padding-bottom: 7px; margin: 6px 0px 0px 0px; left: -15px;  border-radius: 50%;} </style> <ul> <li>9 खिलड़ियों की एक टीम बनायें</li> <li>2-5 आउटफील्डर चुनें, 2-5 इनफील्डर, 1 पिचर, 1 कैचर</li> <li>1टीम बनाने के लिए 100 क्रेडिट तक का उपयोग करें।</li> <li>एक टीम से अधिकतम 6 खिलाडी</li> <li>सफल टैकल, असिस्ट और अपने खेल रहे 8 के सही निशाने साधने पर अंक पाएं।</li></ul>"
                }
            }
        }
        
        let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        txtView.attributedText = attributedString

        UserDefaults.standard.synchronize()
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        hideAnimation()
    }
    
    func hideAnimation() {
        self.containerViewbottomConstraint.constant = -620
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }
    
    func showAnimation() {
        self.containerViewbottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
        })
    }

    
    @IBAction func swipeGestureTapped(_ sender: Any) {
        hideAnimation()
    }
    
}
