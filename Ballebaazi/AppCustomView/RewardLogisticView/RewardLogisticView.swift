//
//  RewardLogisticView.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class RewardLogisticView: UIView {

    @IBOutlet weak var lblRewardName: UILabel!
    @IBOutlet weak var lblNeedMoreCoin: UILabel!
    @IBOutlet weak var lblCoin: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var claimButton: ClaimButton!
    var productDetails: RewardProductDetails?
    var addressDetails: RewardAddressDetails?
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("RewardLogisticView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            claimButton.setTitle("Claim".localized(), for: .normal)
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }

    @IBAction func claimButtonTapped(_ sender: Any) {
        
        if !UserDetails.sharedInstance.panVerified || !UserDetails.sharedInstance.emailVerified {
            let overlayMessage = PanNotVerifiedOverLayview(frame: APPDELEGATE.window!.frame)
            APPDELEGATE.window?.addSubview(overlayMessage)
            overlayMessage.showAnimation()
            return
        }
        
        guard let details = productDetails else {
            return
        }
        removeFromSuperview()

        if (Int(details.coins) ?? 0 > Int(UserDetails.sharedInstance.currentBBcoins) ?? 0){
            AppHelper.showAlertView(message: "Insufficient coins in your wallet".localized(), isErrorMessage: true)
            return;
        }
        if details.rewardCategoryId == "3" {
            let permissionPopup = PermissionView(frame: APPDELEGATE.window!.frame)
                APPDELEGATE.window?.addSubview(permissionPopup)
            permissionPopup.showAnimation()
            var message = "Are you sure you want to claim \(details.rewardNameEnglish) and Redeem \(details.coins) LP Coins?"
             
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    message = "क्या आप \(details.rewardNameEnglish) का क्लेम करना चाहते हैं और \(details.coins) LP सिक्के को भुनाना चाहते हैं?"
                }
            }

            permissionPopup.updateMessage(title: "Confirm Claim".localized(), message: message.localized())
            permissionPopup.updateButtonTitle(noButtonTitle: "No".localized(), yesButtonTitle: "Yes".localized())

            permissionPopup.noButtonTappedBlock { (status) in
                
            }
            
            permissionPopup.yesButtonTappedBlock { (status) in
                self.callClaimProductAPI(details: details)
             }
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let addClaimVC = storyboard.instantiateViewController(withIdentifier: "AddClaimViewController") as! AddClaimViewController
            addClaimVC.productArray = [details]
            addClaimVC.addressDetails = addressDetails
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                navVC.pushViewController(addClaimVC, animated: true)
            }
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        removeFromSuperview()
    }
    
    func configData(details: RewardProductDetails) {
        
        lblNeedMoreCoin.isHidden = true
        lblCoin.text = details.coins
        let currentBBcoins = Int(UserDetails.sharedInstance.currentBBcoins) ?? 0
        let coins = Int(details.coins) ?? 0

        if currentBBcoins < coins{
            lblNeedMoreCoin.isHidden = false
            claimButton.isHidden = true
            lblCoin.text = "You have \(UserDetails.sharedInstance.currentBBcoins) coins"
            lblNeedMoreCoin.text = "You need \(coins - currentBBcoins) more coins to claim this product"
        }
        
        if details.isProductClaimed {
            lblNeedMoreCoin.isHidden = false
            claimButton.isHidden = true

             var message = "You have already claimed this item, You will again be eligible to claim this product after \(details.remainingDays) days"

             if details.remainingDays == "1" {
                 message = "You have already claimed this item, You will again be eligible to claim this product after \(details.remainingDays) day"
             }
             
             if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                 if "hi" == lang[0]{
                     message = "आपने इस आइटम पर पहले ही दावा कर दिया है, आप \(details.remainingDays) दिन के बाद फिर से इस उत्पाद का दावा करने के लिए पात्र होंगे|"
                 }
                lblNeedMoreCoin.text = message
             }
             
             claimButton.isSelected = true
         }
         else{
             claimButton.isSelected = false
         }
        
        lblRewardName.text = details.rewardNameEnglish
        lblDescription.text = details.descriptionEnglish.htmlToString
        
        if details.image.count < 10 {
            self.imgView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: details.image){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.imgView.image = UIImage(named: "Placeholder")
        }
    }
    
    func callClaimProductAPI(details: RewardProductDetails)  {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "claim_confirm", "user_id": UserDetails.sharedInstance.userID, "product": details.rewardProdId ] as [String : Any]
        
        WebServiceHandler.performPOSTRequest(urlString: kRewardProgram, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200" {
                    var message = ""
                    if details.rewardCategoryId == "1" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be sent to your address within 7-10 business days. Please check your email"
                    }
                    else if details.rewardCategoryId == "2" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be sent to your email address. Please check your email"
                    }
                    else if details.rewardCategoryId == "3" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be credited in your account in some time. Keep playing on Letspick"
                    }
                    
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            if details.rewardCategoryId == "1" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपको 7 - 10 कार्य दिवस में आपके पते पर भेज दिया जायेगा। कृपया अपना ईमेल अकाउंट चेक करें।"

                            }
                            else if details.rewardCategoryId == "2" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपको आपके ईमेल पर भेज दिया जायेगा । कृपया अपना ईमेल अकाउंट चेक करें।"
                            }
                            else if details.rewardCategoryId == "3" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपके अकाउंट में कुछ देर बाद क्रेडिट कर दिया जायेगा । Letspick पर खेलते रहें।"
                            }
                        }
                    }

                    let confratsView = CongratsRewarsClaimPopup(frame: APPDELEGATE.window!.frame)
                    confratsView.isViewForClaim = true

                    confratsView.configData(mesage: message)
                    APPDELEGATE.window!.addSubview(confratsView)
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized() , isErrorMessage: true)
            }
        }
    }

    @IBAction func fullImageButtonTapped(_ sender: Any) {

        guard let details = productDetails else {
            return
        }
        removeFromSuperview()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let fullImageVC = storyboard.instantiateViewController(withIdentifier: "ShowFullImageViewController") as! ShowFullImageViewController
        fullImageVC.imageUrl = details.image
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(fullImageVC, animated: true)
        }
    }
}
