//
//  PlayerPointInfoView.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/10/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class PlayerPointInfoView: UIView, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var cantainerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblPlayerName: UILabel!
    
    @IBOutlet weak var lblPlayerRole: UILabel!
    @IBOutlet weak var lblCredits: UILabel!
    
    @IBOutlet weak var lblTotalPoints: UILabel!
    @IBOutlet weak var lblTeamName: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    
    var playerInfoArray = Array<PlayerMatchInfo>()
    var playerDetails: PlayerInfoDetails?
    
    var fantasyType = ""
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("PlayerPointInfoView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.backgroundColor = UIColor.clear
            
            tblView.register(UINib(nibName: "PlayerPointInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerPointInfoTableViewCell")
            tblView.tableFooterView = UIView()
            
            addSubview(view)
        }
    }

    func updateData()  {
    
        lblPlayerName.text = "   " + (playerDetails?.playerName ?? "") ;
        lblTeamName.text = playerDetails?.teamShortName;
        let playerRole = playerDetails?.playerPlayingRole ?? ""
        lblPlayerRole.text = ""
        if playerRole.lowercased() == "keeper" {
            lblPlayerRole.text = "Keeper"
        }
        else if playerRole.lowercased() == "batsman" {
            lblPlayerRole.text = "Batsman"
        }
        else if playerRole.lowercased() == "bowler" {
            lblPlayerRole.text = "Bowler"
        }
        else if playerRole.lowercased() == "allrounder" {
            lblPlayerRole.text = "All Rounder"
        }
        else if playerRole.lowercased() == "defender" {
            lblPlayerRole.text = "Defender"
        }
        else if playerRole.lowercased() == "raider" {
            lblPlayerRole.text = "Raider"
        }


        lblCredits.text = playerDetails?.playerCredits;
        
        if fantasyType == "1" {
            lblTotalPoints.text = playerDetails?.seasonalClassicPoints;
        }
        else if fantasyType == "2" {
            lblTotalPoints.text = playerDetails?.seasonalBattingPoints;
        }
        else if fantasyType == "3" {
            lblTotalPoints.text = playerDetails?.seasonalBowlingPoints;
        }
        
        var containerViewHeight:CGFloat = CGFloat(260 + playerInfoArray.count*60);
        
        if containerViewHeight > UIScreen.main.bounds.height - 100{
            containerViewHeight = UIScreen.main.bounds.height - 100;
        }
        
        cantainerViewHeightConstraint.constant = containerViewHeight;
        containerView.layoutIfNeeded()
        tblView.reloadData()
    }
    
    //MARK:- Table view Data and Delegats
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return playerInfoArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "PlayerPointInfoTableViewCell") as? PlayerPointInfoTableViewCell
        
        if cell == nil {
            cell = PlayerPointInfoTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PlayerPointInfoTableViewCell")
        }
        
        let details = playerInfoArray[indexPath.row];
        cell?.configData(details: details, fantacyType: fantasyType)
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func hideViewWithAnimation()  {
        weak var weakSelf = self

        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            weakSelf?.alpha = 0
        }) { _ in
            weakSelf?.removeFromSuperview()
        }
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        hideViewWithAnimation()
    }
}
