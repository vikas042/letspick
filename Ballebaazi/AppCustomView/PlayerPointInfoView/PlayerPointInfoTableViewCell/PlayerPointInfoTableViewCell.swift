//
//  PlayerPointInfoTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/10/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class PlayerPointInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblMatchName: UILabel!
    
    @IBOutlet weak var lblPoints: UILabel!
    
    @IBOutlet weak var lblSelectedBy: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func configData(details: PlayerMatchInfo, fantacyType: String) {
        lblMatchName.text = details.matchShortName
        
//        let tempArray = details.matchShortName.components(separatedBy: "vs ")
//        if tempArray.count > 1 {
//            lblMatchName.text = tempArray[1]
//        }

        lblDate.text = AppHelper.convertDateForPlayerPointsView(dateString: details.matchDate)
        if fantacyType == "1" {
            lblPoints.text = details.playerClassicScore
            lblSelectedBy.text = details.selectedByClassic
        }
        else if fantacyType == "2" {
            lblPoints.text = details.playerScoreBatting
            lblSelectedBy.text = details.selectedByBatting
        }
        else if fantacyType == "3" {
            lblPoints.text = details.playerScoreBowling
            lblSelectedBy.text = details.selectedBybowling
        }
        
    }
}
