//
//  SortMyReferralsView.swift
//  Letspick
//
//  Created by Vikash Rajput on 19/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

protocol SortMyReferralsViewDelegates {
    func refreshRecordsWithType(type: String)
}

class SortMyReferralsView: UIView {
    @IBOutlet weak var lblSortReferals: UILabel!
    
    @IBOutlet var oldestButton: UIButton!
    @IBOutlet var lowToHighButton: UIButton!
    @IBOutlet var highToLowButton: UIButton!
    @IBOutlet var newestButton: UIButton!
    @IBOutlet var templatedBottonConstraint: NSLayoutConstraint!
    @IBOutlet var templateView: UIView!

    var delegate: SortMyReferralsViewDelegates?
    
    var sortingOrder = ""
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("SortMyReferralsView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
             addSubview(view)
        }
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        removeAnimation()
    }
    
    @IBAction func earningLowToHigh(_ sender: Any) {
        sortingOrder = "1"
        lowToHighButton.isSelected = true
        highToLowButton.isSelected = false
        oldestButton.isSelected = false
        newestButton.isSelected = false
        removeAnimation()
    }
    
    @IBAction func earningHighToLow(_ sender: Any) {
        sortingOrder = "2"
        lowToHighButton.isSelected = false
        highToLowButton.isSelected = true
        oldestButton.isSelected = false
        newestButton.isSelected = false
        removeAnimation()
    }
    
    @IBAction func oldesButtonTapped(_ sender: Any) {
        sortingOrder = "3"
        lowToHighButton.isSelected = false
        highToLowButton.isSelected = false
        oldestButton.isSelected = true
        newestButton.isSelected = false
        removeAnimation()
    }
    
    @IBAction func newestButtonTapped(_ sender: Any) {
        sortingOrder = "4"
        lowToHighButton.isSelected = false
        highToLowButton.isSelected = false
        oldestButton.isSelected = false
        newestButton.isSelected = true
        removeAnimation()
    }
    
    func updateOrderType(orderType: String) {
        lblSortReferals.text = "Sort Referrals".localized()
        lowToHighButton.setTitle("Earnings: Low to High".localized(), for: .normal)
        highToLowButton.setTitle("Earnings: High to Low".localized(), for: .normal)
        newestButton.setTitle("Newest".localized(), for: .normal)
        oldestButton.setTitle("Oldest".localized(), for: .normal)

        if orderType == "1" {
            lowToHighButton.isSelected = true
        }
        else if orderType == "2" {
            highToLowButton.isSelected = true
        }
        else if orderType == "3" {
            oldestButton.isSelected = true
        }
        else if orderType == "4" {
            newestButton.isSelected = true
        }
    }
    
    func showAnimation() {
           
       self.templatedBottonConstraint.constant = 0
       UIView.animate(withDuration: 0.5, animations: {
           self.templateView.layoutIfNeeded()
           self.layoutIfNeeded()
       }, completion: {res in
           //Do something
           self.layoutIfNeeded()
       })
    }
    
    func removeAnimation() {
        
        if sortingOrder.count != 0 {
            delegate?.refreshRecordsWithType(type: sortingOrder)
        }
        
        self.templatedBottonConstraint.constant = -260
        UIView.animate(withDuration: 0.5, animations: {
            self.templateView.layoutIfNeeded()
            self.layoutIfNeeded()
        }, completion: {res in
            self.removeFromSuperview()
        })
    }

    @IBAction func swipeGestureTapped(_ sender: Any) {
        removeAnimation()
    }

}
