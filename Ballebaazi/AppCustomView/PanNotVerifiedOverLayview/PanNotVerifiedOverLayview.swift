//
//  PanNotVerifiedOverLayview.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class PanNotVerifiedOverLayview: UIView {
    @IBOutlet weak var lblAccountNotVerified: UILabel!
    
    @IBOutlet weak var lblTapHere: UILabel!
    @IBOutlet weak var lblPanVerification: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    // MARK: init
      required init(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)!
          if self.subviews.count == 0 {
              setup()
          }
      }
      
      override init(frame: CGRect) {
          super.init(frame: frame)
          setup()
      }
      
      func setup() {
          
          if let view = Bundle.main.loadNibNamed("PanNotVerifiedOverLayview", owner: self, options: nil)?.first as? UIView {
              view.frame = bounds
              view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
              view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            lblTapHere.text = "Tap here to complete the verification".localized();
            lblAccountNotVerified.text = "Account not verified".localized();
            lblPanVerification.text = "Account Verification is mandatory for Withdrawals".localized()
            addSubview(view)
          }
      }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        hideAnimation()
    }
    
    @IBAction func verificationButtonTapped(_ sender: Any) {
        removeFromSuperview()
        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let verificationVC = storyboard.instantiateViewController(withIdentifier: "VerifyDocumentsViewController")
            navVC.pushViewController(verificationVC, animated: true)
        }
    }
    
    func hideAnimation() {
          self.bottomConstraint.constant = -200
          UIView.animate(withDuration: 0.5, animations: {
              self.layoutIfNeeded()
          }, completion: {res in
              self.removeFromSuperview()
          })
      }
      
      func showAnimation() {
          self.bottomConstraint.constant = 0
          UIView.animate(withDuration: 0.5, animations: {
              self.layoutIfNeeded()
          }, completion: {res in
              //Do something
          })
      }

    
}
