//
//  ForceUpdateView.swift
//  Letspick
//
//  Created by Vikash Rajput on 12/09/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class UpdateView: UIView {

    @IBOutlet weak var updateButton: UIButton!

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var laterButton: UIButton!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNewUpdate: UILabel!
    
    var isForceUpdateEnable = false

    var versionHistoryArray = Array<VersionDetails>()
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("UpdateView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            tblView.register(UINib(nibName: "VersionDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "VersionDetailsTableViewCell")
            tblView.rowHeight = UITableViewAutomaticDimension
            tblView.estimatedRowHeight = 40
            indicator.startAnimating()
            callGetVersionHistory()
            
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }
    
    @IBAction func updateButtonTapped(_ sender: Any) {
        removeFromSuperview()
        openAppStore()
    }
    
    func openAppStore() {
        if let url = URL(string: UserDetails.sharedInstance.updateAppUrl),
            UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:]) { (opened) in
                    if(opened){
                        print("App Store Opened")
                    }
                }
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    @IBAction func laterButtonTapped(_ sender: Any) {
        hideViewWithAnimation()
    }
    
    @IBAction func tapGestureTapped(_ sender: Any) {
        if !isForceUpdateEnable {
            hideViewWithAnimation()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.gestureRecognizers != nil) {
            return false;
        }
        return true;
    }

    
    func hideViewWithAnimation()  {
        UserDetails.sharedInstance.isForceUpdateViewOpen = false
        weak var weakSelf = self

        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            weakSelf?.alpha = 0
        }) { _ in
            weakSelf?.removeFromSuperview()
        }
    }
    
    
    func configData(result: JSON) {
        let iOSMinVersion = result["ios_version_min"].stringValue
        let iOSMaxVersion = result["ios_version_max"].stringValue
        UserDetails.sharedInstance.iOSMaxVersion = iOSMinVersion
        UserDetails.sharedInstance.iOSMaxVersion = iOSMaxVersion
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String

        
        if UserDetails.sharedInstance.userLoggedIn{
            if !UserDetails.sharedInstance.isForceUpdateViewOpen{
            
                if appVersion < iOSMinVersion{
                    
                }
                else if appVersion < iOSMaxVersion{
                    
                    let serverTime: String = (UserDefaults.standard.value(forKey: "lastForceUpdateTime") ?? "0") as! String
                    if UserDetails.sharedInstance.serverTimeStemp.count == 0{
                        UserDetails.sharedInstance.serverTimeStemp = "0"
                    }
                        
                    let updateDuration = result["ios_update_duration"].stringValue
                    if (Double(UserDetails.sharedInstance.serverTimeStemp)! - Double(serverTime)!) > (Double(updateDuration) ?? 0){
                        let forceUpdateView = UpdateView(frame: APPDELEGATE.window!.frame)
                        forceUpdateView.isForceUpdateEnable = false
                        APPDELEGATE.window?.addSubview(forceUpdateView)
                        UserDetails.sharedInstance.isForceUpdateViewOpen = true
                        UserDefaults.standard.set(UserDetails.sharedInstance.serverTimeStemp, forKey: "lastForceUpdateTime")
                        UserDefaults.standard.synchronize()
                    }
                }
            }
        }
    }
    
    
    func callGetVersionHistory() {

        WebServiceHandler.performPOSTRequest(urlString: kSupport, andParameters: ["option": "update"], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            self.indicator.stopAnimating()
            if result != nil{
                let status = result!["status"]
                if status == "200"{
                    if let dataArray = result!["response"]?.array {
                        
                        self.versionHistoryArray =  VersionDetails.getAlliOSVersionHistory(dataArray: dataArray)
                        self.tblView.reloadData()
                    }
                }
            }
        }
    }
    
}


extension UpdateView: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return versionHistoryArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "VersionDetailsTableViewCell") as? VersionDetailsTableViewCell
        
        if cell == nil {
            cell = VersionDetailsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "VersionDetailsTableViewCell")
        }
        
        let details = versionHistoryArray[indexPath.row]
        cell!.configData(details: details)
        
        return cell!

    }
    

}
