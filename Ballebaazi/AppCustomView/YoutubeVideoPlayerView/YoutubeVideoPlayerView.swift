//
//  YoutubeVideoPlayerView.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class YoutubeVideoPlayerView: UIView, YTPlayerViewDelegate {

    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playerView: YTPlayerView!

    var videoID = ""
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("YoutubeVideoPlayerView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            playerView.delegate = self
            addSubview(view)
        }
    }
    
    func playView() {
        let playerVars = ["playsinline" :1, "autoplay" : 1]


        if videoID.contains("http") {
            let mediaID = self.getYoutubeId(youtubeUrl: videoID) ?? ""
            playerView.load(withVideoId: mediaID, playerVars: playerVars)
        }
        else{
            playerView.load(withVideoId: videoID, playerVars: playerVars)
        }
        
        playerView.delegate = self
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        playerView.pauseVideo()
        playerView.stopVideo()
        removeFromSuperview()
    }
 
    @IBAction func playButtonTapped(_ sender: Any) {
        playButton.isHidden = true
        playerView.isHidden = false
        playerView.playVideo()
    }
    
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.playVideo()
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch state {
        
        case .unstarted:
            break;
        
        case .playing:
            break;
            
        case .paused:
            break;
         
        case .ended:
            playButton.isHidden = false
            playerView.isHidden = true
            break;

        default:
            break
        }
    }

    
}
