//
//  PartnershipQRCode.swift
//  Letspick
//
//  Created by Vikash Rajput on 15/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class PartnershipQRCodeView: UIView {
    @IBOutlet weak var shareButton: SolidButton!
    
    @IBOutlet weak var lblReferalCode: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("PartnershipQRCodeView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            let image = generateQRCode(from: "https://letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!)
            lblName.text = UserDetails.sharedInstance.userName
            lblReferalCode.text = UserDetails.sharedInstance.referralCode
            imgView.image = image
            shareButton.setTitle("SHARE".localized(), for: .normal)
            addSubview(view)
        }
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        removeFromSuperview();
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
                
        let text = "Hey, I'm playing Fantasy Sports on the Letspick App and winning real cash every day. Join now and start making your teams:" + "https://Letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!
        
        let textToShare = [text, containerView.takeScreenshot()] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self
        if let navVC = APPDELEGATE.window!.rootViewController {
            navVC.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func downloadButtonTapped(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(containerView.takeScreenshot(), self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }

    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if error != nil {
            AppHelper.showAlertView(message: "Please allow Letspick to access photos, media, and files on your device", isErrorMessage: true)
        } else {
            AppHelper.showAlertView(message: "QR code downloaded successfully".localized(), isErrorMessage: false)
        }
    }

    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    
    @IBAction func copyReferfalCodeButtonTazpped(_ sender: Any) {
        UIPasteboard.general.string = UserDetails.sharedInstance.referralCode
        AppHelper.showToast(message: "Referral code copied")
    }
    
}
