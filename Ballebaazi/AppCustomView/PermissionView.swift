//
//  PermissionView.swift
//  Letspick
//
//  Created by Vikash Rajput on 20/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class PermissionView: UIView {

    var cancelPopupButton = {(sucess: Bool) -> () in }
    var confirmPopupButton = {(sucess: Bool) -> () in }

    @IBOutlet weak var containerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var noButton: CustomBorderButton!
    
    @IBOutlet weak var yesButton: SolidButton!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("PermissionView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }
    
    @IBAction func noButtonTapped(_ sender: Any) {
        cancelPopupButton(false)
        hideAnimation()
    }
    
    @IBAction func yesButtonTapped(_ sender: Any) {
        confirmPopupButton(true)
        hideAnimation()
    }
    
    func noButtonTappedBlock(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        cancelPopupButton = complationBlock
    }
    
    func yesButtonTappedBlock(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        confirmPopupButton = complationBlock
    }
    
    func updateMessage(title: String, message: String){
        lblTitle.text = title
        lblMessage.text = message
    }
    
    func updateButtonTitle(noButtonTitle: String, yesButtonTitle: String){
        yesButton.setTitle(yesButtonTitle, for: .normal)
        noButton.setTitle(noButtonTitle, for: .normal)
    }
    
    @IBAction func closePopupButtonTapped(_ sender: Any) {
        hideAnimation()
    }
    
    func hideAnimation() {
        
        self.containerViewBottomConstraint.constant = -240
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }, completion: {res in
            //Do something
            self.layoutIfNeeded()
            self.removeFromSuperview()
        })
        
    }
    
    func showAnimation() {
        self.containerViewBottomConstraint.constant = -20
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }, completion: {res in
            self.layoutIfNeeded()
        })
    }
    
    
}
