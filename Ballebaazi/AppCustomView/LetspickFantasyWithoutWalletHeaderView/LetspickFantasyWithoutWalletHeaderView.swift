//
//  LetspickFantasyWithoutWalletHeaderView.swift
//  Letspick
//
//  Created by Vikash Rajput on 20/05/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LetspickFantasyWithoutWalletHeaderView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var titleStr: String?
    var backButtonHiddenStatus = false
    
    @IBOutlet weak var lblVs: UILabel!
    @IBOutlet weak var lblBackButtonTitle: UILabel!

    @IBInspectable var backButtonTitle: String? {
        get {
            return titleStr
        }
        set {
            titleStr = newValue
            lblBackButtonTitle.text = titleStr;
        }
    }
    
    
    @IBInspectable var isBackButtonHidded: Bool {
        get {
            return backButtonHiddenStatus
        }
        set {
            backButtonHiddenStatus = newValue
            backButton.isHidden = backButtonHiddenStatus;
        }
    }

    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var bottomSepratorLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblFirstTeamName: UILabel!
    @IBOutlet weak var lblSecondTeamName: UILabel!
    @IBOutlet weak var lblMatchRemaingTime: UILabel!

    @IBOutlet weak var separatorWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottemSeparatorView: UIView!

    var isFantasyModeEnable = true
    
    var classicBlock = {(sucess: Bool) -> () in }
    var battingBlock = {(sucess: Bool) -> () in }
    var bowlingBlock = {(sucess: Bool) -> () in }
    var reverseFantasyBlock = {(sucess: Bool) -> () in }
    var wizardFantasyBlock = {(sucess: Bool) -> () in }

    func clasicButtonTappedBlock(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        classicBlock = complationBlock
    }
    
    func battingButtonTappedBlock(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        battingBlock = complationBlock
    }
    
    func bowlingButtonTappedBlock(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        bowlingBlock = complationBlock
    }
    
    func reverseFantasyButtonTapped(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        reverseFantasyBlock = complationBlock
    }
    
    func wizardFantasyButtonTapped(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        wizardFantasyBlock = complationBlock
    }
    
    
    lazy var firstTabFantasyType = FantasyType.Classic.rawValue;
    lazy var secondTabFantasyType = FantasyType.Batting.rawValue;
    lazy var thirdTabFantasyType = FantasyType.Bowling.rawValue;
    lazy var fourthTabFantasyType = FantasyType.Reverse.rawValue;
    lazy var fifthTabFantasyType = FantasyType.Wizard.rawValue;
    lazy var tabsArray = Array<Int>()

    lazy var selectedFantasyType = FantasyType.Classic.rawValue;

    lazy var matchFantasyType = ""
    

    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("LetspickFantasyWithoutWalletHeaderView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            addSubview(view)
            collectionView.register(UINib(nibName: "GameTabCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GameTabCollectionViewCell")
        }
    }
    
     func updateTabsData() {
          
        (firstTabFantasyType, secondTabFantasyType, thirdTabFantasyType, fourthTabFantasyType, fifthTabFantasyType) = AppHelper.getFantasyLandingOrder(fantasyType: matchFantasyType)

        selectedFantasyType = firstTabFantasyType
        tabsArray.removeAll()
        if firstTabFantasyType != FantasyType.None.rawValue {
          tabsArray.append(firstTabFantasyType)
        }

        if secondTabFantasyType != FantasyType.None.rawValue {
          tabsArray.append(secondTabFantasyType)
        }

        if thirdTabFantasyType != FantasyType.None.rawValue {
          tabsArray.append(thirdTabFantasyType)
        }

        if fourthTabFantasyType != FantasyType.None.rawValue {
          tabsArray.append(fourthTabFantasyType)
        }

        if fifthTabFantasyType != FantasyType.None.rawValue {
          tabsArray.append(fifthTabFantasyType)
        }

        if tabsArray.count == 0 {
            tabsArray.append(FantasyType.Classic.rawValue)
        }
        let sepratorWidth = Int(UIScreen.main.bounds.width)/tabsArray.count
        separatorWidthConstraint.constant = CGFloat(sepratorWidth)
        self.layoutIfNeeded()
     }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.popViewController(animated: true)
        }
    }

    func updateMatchName(details: MatchDetails?){
        if !isFantasyModeEnable {
            collectionView.isHidden = true
            bottemSeparatorView.isHidden = true
        }
        
        lblFirstTeamName.text = ""
        lblSecondTeamName.text = ""
        lblMatchRemaingTime.text = ""
        lblVs.isHidden = true

        guard let matchDetails = details else {
            return;
        }
        
        if matchDetails.matchStatus == "completed" || matchDetails.matchStatus == "started"  {
            lblFirstTeamName.text = matchDetails.firstTeamShortName
            lblSecondTeamName.text = matchDetails.secondTeamShortName
            lblVs.isHidden = false
            if matchDetails.matchStatus == "started" {
                lblMatchRemaingTime.text = "Live".localized()
            }
            else if matchDetails.matchStatus == "completed" {
                lblMatchRemaingTime.text = "Completed".localized()
            }
            if matchDetails.isMatchTourney && !matchDetails.isMatchClosed{
                lblFirstTeamName.text = ""
                lblSecondTeamName.text = ""
                lblVs.isHidden = true
                lblFirstTeamName.isHidden = true
                lblSecondTeamName.isHidden = false
                lblVs.isHidden = true
                lblSecondTeamName.text = matchDetails.matchShortName
                lblFirstTeamName.text = ""
            }
        }
        else{
            lblFirstTeamName.text = matchDetails.firstTeamShortName
            lblSecondTeamName.text = matchDetails.secondTeamShortName
            lblVs.isHidden = false
            
            if matchDetails.isMatchTourney {
                lblFirstTeamName.isHidden = true
                lblSecondTeamName.isHidden = false
                lblVs.isHidden = true
                lblSecondTeamName.text = matchDetails.matchShortName
                lblFirstTeamName.text = ""
            }

            if matchDetails.isMatchClosed{
                lblMatchRemaingTime.text = "Leagues Closed".localized()
            }
            else{
                lblMatchRemaingTime.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: matchDetails.startDateTimestemp)
            }
        }
        if tabsArray.count > 1 {
            collectionView.isHidden = false
            bottemSeparatorView.isHidden = false
        }
        collectionView.reloadData()
    }
    
    // MARK:- Timer Handlers
    func updateTimerValue(matchDetails: MatchDetails)  {
        
        if matchDetails.isMatchClosed  {
            lblMatchRemaingTime.text = "Leagues Closed".localized()
        }
        if matchDetails.active == "4"  {
            lblMatchRemaingTime.text = "00:00"
        }
        else if UInt64((matchDetails.startDateTimestemp)!) != nil{
            lblMatchRemaingTime.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: matchDetails.startDateTimestemp)
        }
    }
        
    func classicButtonTapped(isNeedToScroll: Bool) {
        
        selectedFantasyType = FantasyType.Classic.rawValue
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
            }
            else if tabsArray.count == 4{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
              bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
               bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }


        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
        
        if isNeedToScroll {
            classicBlock(true)
        }
        else{
            classicBlock(false)
        }

        collectionView.reloadData()
    }
    
//    func classicButtonTapped(isNeedToScroll: Bool) {
//        showClassicTabSelected()
//
//        if isNeedToScroll {
//            classicBlock(true)
//        }
//        else{
//            classicBlock(false)
//        }
//    }

//    func battingButtonTapped(isNeedToScroll: Bool) {
//        showBattingTabSelected()
//        if isNeedToScroll {
//            battingBlock(true)
//        }
//        else{
//            battingBlock(false)
//        }
//    }
    
     
    func battingButtonTapped(isNeedToScroll: Bool) {
        
        selectedFantasyType = FantasyType.Batting.rawValue
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
            }
            else if tabsArray.count == 4{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
              bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
               bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }

        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }

        if isNeedToScroll {
            battingBlock(true)
        }
        else{
            battingBlock(false)
        }

        collectionView.reloadData()
    }

//    func bowlingButtonTapped(isNeedToScroll: Bool) {
//        showBowlingTabSelected();
//        if isNeedToScroll {
//            bowlingBlock(true)
//        }
//        else{
//            bowlingBlock(false)
//        }
//    }
    
    
    
    func bowlingButtonTapped(isNeedToScroll: Bool) {

        selectedFantasyType = FantasyType.Bowling.rawValue
        
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
                        
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }

        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }

        collectionView.reloadData()
        
        if isNeedToScroll {
            bowlingBlock(true)
        }
        else{
            bowlingBlock(false)
        }
    }

    func reverseFantasyButtonTapped(isNeedToScroll: Bool) {

        selectedFantasyType = FantasyType.Reverse.rawValue
        
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
                        
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }

        if isNeedToScroll {
            reverseFantasyBlock(true)
        }
        else{
            reverseFantasyBlock(false)
        }

        collectionView.reloadData()
    }

    
//    func reverseFantasyButtonTapped(isNeedToScroll: Bool) {
//        showBowlingTabSelected();
//        if isNeedToScroll {
//            reverseFantasyBlock(true)
//        }
//        else{
//            reverseFantasyBlock(false)
//        }
//    }

    
    func wizardFantasyButtonTapped(isNeedToScroll: Bool) {
        selectedFantasyType = FantasyType.Wizard.rawValue
        
        if selectedFantasyType == firstTabFantasyType {
            bottomSepratorLeadingConstraint.constant = 0
        }
        else if selectedFantasyType == secondTabFantasyType {
            if tabsArray.count == 6{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0)
            }
            else if tabsArray.count == 5{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0)
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0)
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0)
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/2.0)
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == thirdTabFantasyType {
                        
            if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 2.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 2.0
            }
            else if tabsArray.count == 4{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 2.0
            }
            else if tabsArray.count == 3{
               bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
               bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        else if selectedFantasyType == fourthTabFantasyType {
           if tabsArray.count == 6{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 3.0
           }
           else if tabsArray.count == 5{
              bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 3.0
           }
           else if tabsArray.count == 4{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
           }
           else if tabsArray.count == 3{
             bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
           }
           else if tabsArray.count == 2{
             bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
           }
           else{
              bottomSepratorLeadingConstraint.constant = 0
           }
        }
        else if selectedFantasyType == fifthTabFantasyType {
            if tabsArray.count == 6{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/6.0) * 4.0
            }
            else if tabsArray.count == 5{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/5.0) * 4.0
            }
            else if tabsArray.count == 4{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/4.0) * 3.0
            }
            else if tabsArray.count == 3{
                bottomSepratorLeadingConstraint.constant = (self.frame.size.width/3.0) * 2.0
            }
            else if tabsArray.count == 2{
                bottomSepratorLeadingConstraint.constant = self.frame.size.width/2.0
            }
            else{
                bottomSepratorLeadingConstraint.constant = 0
            }
        }
        
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }

        if isNeedToScroll {
            wizardFantasyBlock(true)
        }
        else{
            wizardFantasyBlock(false)
        }

        collectionView.reloadData()
    }
        

    
    
    //MARK:- CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if tabsArray.count > 1 {
            return tabsArray.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = UIScreen.main.bounds.width/CGFloat(tabsArray.count)
        return CGSize(width: cellWidth, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GameTabCollectionViewCell", for: indexPath) as! GameTabCollectionViewCell
        let fantasyType = tabsArray[indexPath.row]
        cell.lineUpsView.isHidden = true;
        
        let cellWidth = Int(UIScreen.main.bounds.width)/tabsArray.count
        
        if cellWidth <= 55 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 10)
        }
        else if cellWidth <= 64 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 11)
        }
        else if cellWidth <= 69 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 12)
        }
        else if cellWidth <= 75 {
            cell.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 13)
        }

        if fantasyType == FantasyType.Classic.rawValue {
            cell.lblTitle.text = "classic".localized()
        }
        else if fantasyType == FantasyType.Batting.rawValue {
            cell.lblTitle.text = "batting".localized()
        }
        else if fantasyType == FantasyType.Bowling.rawValue {
            cell.lblTitle.text = "bowling".localized()
        }
        else if fantasyType == FantasyType.Reverse.rawValue {
            cell.lblTitle.text = "Reverse".localized()
        }
        else if fantasyType == FantasyType.Wizard.rawValue {
            cell.lblTitle.text = "Wizard".localized()
        }
        else{
            cell.lblTitle.text = ""
        }
        
        cell.lblTitle.textColor = UIColor.white.withAlphaComponent(0.6)

        if fantasyType == selectedFantasyType {
            cell.lblTitle.textColor = UIColor.white.withAlphaComponent(1.0)
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let fantasyType = tabsArray[indexPath.row]
        selectedFantasyType = fantasyType;
        
        if fantasyType == FantasyType.Classic.rawValue {
            classicButtonTapped(isNeedToScroll: true)
        }
        else if fantasyType == FantasyType.Batting.rawValue {
            battingButtonTapped(isNeedToScroll: true)
        }
        else if fantasyType == FantasyType.Bowling.rawValue {
            bowlingButtonTapped(isNeedToScroll: true)
        }
        else if fantasyType == FantasyType.Reverse.rawValue {
            reverseFantasyButtonTapped(isNeedToScroll: true)
        }
        else if fantasyType == FantasyType.Wizard.rawValue {
            wizardFantasyButtonTapped(isNeedToScroll: true)
        }
    }
    

}
