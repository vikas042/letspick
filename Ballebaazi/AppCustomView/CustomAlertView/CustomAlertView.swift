//
//  CustomAlertView.swift
//  Letspick
//
//  Created by Vikash Rajput on 23/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class CustomAlertView: UIView {

    var closePopupViewBlock = {(sucess: Bool) -> () in }
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("CustomAlertView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }

    @IBAction func closeButtonTapped(_ sender: Any) {
        removeFromSuperview()
        closePopupViewBlock(true)
    }
    
    func updateMessage(title: String, message: String){
        lblTitle.text = title
        lblMessage.text = message
    }
    
    func updateMessageForApplyPromode(message: String){
        lblTitle.text = ""
        lblMessage.text = message
        imgView.image = UIImage(named: "VerifyIcon")
        
    }

    
    func closePopupViewBlock(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        closePopupViewBlock = complationBlock
    }
}
