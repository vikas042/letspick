//
//  QuizConfirmationView.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/04/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

protocol QuizConfirmationViewDelegate {
    func updateAutoPopupStatus()
}

class QuizConfirmationView: UIView {

    @IBOutlet weak var lblRealDeductTitle: UILabel!
    @IBOutlet weak var joinButton: SolidButton!
    @IBOutlet weak var lblRealDeductAmount: UILabel!
    @IBOutlet weak var lblConfirmEntry: UILabel!
    @IBOutlet weak var lblBonusDeductTitle: UILabel!
    @IBOutlet weak var lblBonusDeductAmount: UILabel!

    var leagueDetails: LeagueDetails?
    var matchDetails: MatchDetails?
    var delegate:QuizConfirmationViewDelegate?
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)!
      if self.subviews.count == 0 {
          setup()
      }
    }

    override init(frame: CGRect) {
      super.init(frame: frame)
      setup()
    }

    func setup() {
      
      if let view = Bundle.main.loadNibNamed("QuizConfirmationView", owner: self, options: nil)?.first as? UIView {
          view.frame = bounds
          view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
          view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
          addSubview(view)
      }
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        removeFromSuperview()
    }
    
    @IBAction func termsAndConditionButtonTapped(_ sender: Any) {
        delegate?.updateAutoPopupStatus()
        removeFromSuperview()
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        let privacyPolicyVC = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as! PrivacyPolicyWebViewController
        privacyPolicyVC.selectedType = SelectedWebViewType.termsAndConditions.rawValue
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.pushViewController(privacyPolicyVC, animated: true)
        }
    }
    
    @IBAction func joinButtonTapped(_ sender: Any) {
        removeFromSuperview()
        callLeagueValidationAPI(leagueDetails: leagueDetails!)
    }
    
    func updateData(details:LeagueDetails) {
        leagueDetails = details
        let leagueJoiningAmount = (Double(details.joiningAmount) ?? 0)

        var bonusAmount = (Double(details.bounsPercentage) ?? 0)*leagueJoiningAmount/100

        if bonusAmount >= Double(UserDetails.sharedInstance.bonusCash) ?? 0{
            bonusAmount = Double(UserDetails.sharedInstance.bonusCash) ?? 0
        }
        let totalJoiningAmt = Double(leagueJoiningAmount - bonusAmount)
        lblRealDeductAmount.text = "\(totalJoiningAmt)"
        lblBonusDeductAmount.text = "\(bonusAmount)"
        joinButton.setTitle("Play league with".localized() + "pts " + String(leagueJoiningAmount), for: .normal)
        lblConfirmEntry.text = "Confirm Entry".localized()
        lblRealDeductTitle.text = "Balance Deducted".localized()
        lblBonusDeductTitle.text = "Bonus Deducted".localized()
    }
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId]
        WebServiceHandler.performPOSTRequest(urlString: kQuizMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary{
                        if let details = response["league"]?.dictionary{
                            if let quizUrlString = details["quiz_url"]?.string{
                                let quizWebVC = storyboard.instantiateViewController(withIdentifier: "QuizWebViewController") as! QuizWebViewController
                                quizWebVC.matchDetails = self.matchDetails
                                quizWebVC.urlString = quizUrlString
                                quizWebVC.requestId = details["request_id"]?.string ?? ""
                                quizWebVC.leagueId = leagueDetails.leagueId
                                quizWebVC.leagueDetails = leagueDetails
                                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    navVC.pushViewController(quizWebVC, animated: true)
                                }
                            }
                        }
                    }
                }
                else if statusCode == "401"{
                    if let response = result!["response"]?.dictionary {
                        
                        let titleMessage = "Oops! Low Balance".localized()
                        let creditRequired = response["credit_required"]?.stringValue ?? "0"
                        let responseAmt = Float(creditRequired)!

                        let joiningAmount = Float(leagueDetails.joiningAmount)!
                        let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                        let message = String(format: notEnoughPoints, String(roundFigureAmt))
                        
                        let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
                        /*
                        alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
                            
                            let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
                            addCashVC?.leagueDetails = leagueDetails
                            addCashVC?.amount = roundFigureAmt
                            addCashVC?.matchDetails = self.matchDetails
                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                navVC.pushViewController(addCashVC!, animated: true)
                            }
                        }))
                         */
                        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
}
