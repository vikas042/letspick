//
//  CustomNavigationBar.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class CustomNavigationBar: UIView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    var backButtonHiddenStatus = false
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var headerTitleBottonConstraint: NSLayoutConstraint!

    var titleStr: String?
    @IBInspectable var headerTitle: String? {
        get {
            return titleStr
        }
        set {
            titleStr = newValue
            lblTitle.text = titleStr;
        }
    }
    
    @IBInspectable var HideBackButton: Bool {
        get {
            return backButtonHiddenStatus
        }
        set {
            backButtonHiddenStatus = newValue
            backButton.isHidden = newValue;
        }
    }
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("CustomNavigationBar", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            addSubview(view)
        }
    }
 
    func updateHeaderTitles(title: String, subTitle: String) {
        headerTitle = title
        lblSubTitle.isHidden = false
        lblSubTitle.text = subTitle
        headerTitleBottonConstraint.constant = 17
        self.layoutIfNeeded()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        if self.tag == 2000 {
            AppHelper.makeDashbaordAsRootViewController()
        }
        else if self.tag == 6001 {
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                let alert = UIAlertController(title: "Confirm".localized(), message: "LeaveWebViewPageMsg".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in

                    let navArray = navVC.viewControllers
                    var isfoundVC = false
                    for selectPlayerVC in navArray {
                        if selectPlayerVC is QuizPreviewViewController{
                            navVC.popToViewController(selectPlayerVC, animated: true)
                            isfoundVC = true;
                            break;
                        }
                        else if selectPlayerVC is QuizLeaguesViewController{
                            navVC.popToViewController(selectPlayerVC, animated: true)
                            isfoundVC = true;
                            break;
                        }
                    }
                    
                    if !isfoundVC{
                        navVC.popToRootViewController(animated: true)
                    }
                }))
                alert.addAction(UIAlertAction(title: "No".localized(), style: UIAlertActionStyle.default, handler: nil))
                navVC.present(alert, animated: true, completion: nil)
            }
        }
//        else if self.tag == 3000 {
//            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                let navArray = navigationVC.viewControllers
//                var isfoundVC = false
//                for selectPlayerVC in navArray {
//                    if selectPlayerVC is JoinedLeagueViewController{
//                        navigationVC.popToViewController(selectPlayerVC, animated: true)
//                        isfoundVC = true;
//                        break;
//                    }
//                    else if selectPlayerVC is JoinedKabaddiLeagueViewController{
//                        navigationVC.popToViewController(selectPlayerVC, animated: true)
//                        isfoundVC = true;
//                        break;
//                    }
//                    else if selectPlayerVC is FootballJoinedLeagueViewController{
//                        navigationVC.popToViewController(selectPlayerVC, animated: true)
//                        isfoundVC = true;
//                        break;
//                    }
//                }
//
//                if !isfoundVC{
//                    navigationVC.popToRootViewController(animated: true)
//                }
//            }
//        }
        else if self.tag == 4000 {
            
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                let navArray = navigationVC.viewControllers
                var isfoundVC = false
                for selectPlayerVC in navArray {
                    if selectPlayerVC is LeagueViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                    else if selectPlayerVC is KabaddiLeagueViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                    else if selectPlayerVC is FootballLeagueViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                    else if selectPlayerVC is BasketballLeagueViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                    else if selectPlayerVC is BaseballLeagueViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                    else if selectPlayerVC is JoinedLeagueViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                    else if selectPlayerVC is JoinedKabaddiLeagueViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                    else if selectPlayerVC is FootballJoinedLeagueViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                    else if selectPlayerVC is JoinedBasketballLeaguesViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                    else if selectPlayerVC is JoinedBaseballLeaguesViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                }
                
                if !isfoundVC{
                    navigationVC.popToRootViewController(animated: true)
                }
            }
        }
        else if self.tag == 6000 {
            
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                let navArray = navigationVC.viewControllers
                var isfoundVC = false
                for selectPlayerVC in navArray {
                    if selectPlayerVC is RewardStoreViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                }
                
                if !isfoundVC{
                    navigationVC.popToRootViewController(animated: true)
                }
            }
        }
        else if self.tag == 5001 {
            
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.popToRootViewController(animated: true)
            }
        }
        else{
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
                if isUserLoggedIn{
                    if navigationVC.viewControllers.count == 1{
                        AppHelper.makeDashbaordAsRootViewController()
                        return;
                    }
                }
                
                navigationVC.popViewController(animated: true)
            }
        }
    }
    
}
