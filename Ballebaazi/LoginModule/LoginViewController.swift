//
//  LoginViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/30/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class LoginViewController: UIViewController, UITextFieldDelegate {
    
    //    @IBOutlet weak var loginButton: SolidButton!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    //    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    
    //MARK:- Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppHelper.resetDefaults();
        //UITextField.appearance().tintColor = .white
        if view.frame.size.height > 600 {
            //            scrollViewHeightConstraint.constant = view.frame.size.height - 80
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true;
    }
    
    //MARK:- IBAction Methods
    @IBAction func loginButtonTaaped(_ sender: Any) {
        
        if emailTxtField.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterEmailOrUserNameMsg".localized(), isErrorMessage: true)
            return
        }
        else if passwordTxtField.text?.count == 0{
            AppHelper.showAlertView(message: "EnterPasswordMsg".localized(), isErrorMessage: true)
            return
        }
        
        if UserDetails.sharedInstance.guestAccessToken.count == 0 {
            AppHelper.getGuestToken()
            return
        }
        
        var deviceToken: String = UserDetails.sharedInstance.deviceToken
        
        if deviceToken.count == 0 {
            deviceToken = "1111111111";
        }
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        let params:Parameters = ["option": "login_lp","username":emailTxtField.text!, "password":passwordTxtField.text!, "device_type": "1", "device_token": deviceToken, "device_id": deviceID]
        callLoginAPI(params: params)
    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
        AppHelper.showToast(message: "Under Developement")
        return
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        
        UserDetails.sharedInstance.isUserComingFromOnBoarding = true;
        weak var weakSelf = self
        
        FacebookLoginManager.sharedInstance.callLoginMangerWithCompletion { (result, error) in
            
            if (result != nil){
                let responseJSON = JSON(result as AnyObject);
                let statusCode = responseJSON["status"]
                
                if statusCode == "200"{
                    if let allResponse = responseJSON.dictionary!["response"]{
                        if let response = allResponse.dictionary!["user"]{
                            UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                            UserDetails.sharedInstance.userID = response["user_id"].stringValue
                            UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                            UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                            UserDetails.sharedInstance.phoneNumber = response["phone"].stringValue
                            UserDetails.sharedInstance.name = response["name"].stringValue
                            UserDetails.sharedInstance.userName = response["username"].stringValue
                            UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                            UserDetails.sharedInstance.address = response["address"].stringValue
                            UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                            UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                            UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                            UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                            UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                            
                            UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                            UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                            UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                            UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"
                            
                            let userNameEdited = response["username_edited"].stringValue
                            AppHelper.saveUserDetails()
                            if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                            }
                            else{
                                AppHelper.makeDashbaordAsRootViewController()
                            }
                            
                            
                            //                        if userNameEdited == "0"{
                            //                            let enterUserName = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController")
                            //                            weakSelf?.navigationController?.pushViewController(enterUserName!, animated: true)
                            //                        }
                            //                        else{
                            //                            AppHelper.saveUserDetails()
                            //                            if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                            //                            }
                            //                            else{
                            //                                AppHelper.makeDashbaordAsRootViewController()
                            //                            }
                            //                        }
                        }
                    }
                    
                }
                else if statusCode == "650"{
                    
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        let userNameEdited = response["username_edited"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        let facebookID = response["facebook_id"].string ?? ""
                        let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController") as? EnterPhoneNumberViewController
                        //                            enterPhoneVC?.facebookId = facebookID
                        weakSelf?.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                        
                        //                        if userNameEdited == "0"{
                        //                            let enterUserName = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController")
                        //                            weakSelf?.navigationController?.pushViewController(enterUserName!, animated: true)
                        //                        }
                        //                        else{
                        //                            let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController") as? EnterPhoneNumberViewController
                        ////                            enterPhoneVC?.facebookId = facebookID
                        //                            weakSelf?.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                        //                        }
                    }
                }
                else if statusCode == "801"{
                    AppHelper.getGuestToken()
                }
                else{
                    let errorMsg = result!["message"] as! String
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
    
    @IBAction func googleButtonTapped(_ sender: Any) {
        AppHelper.showToast(message: "Under Developement")
        return
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        weak var weakSelf = self
        
        UserDetails.sharedInstance.isUserComingFromOnBoarding = true;
        GoogleLoginManager.sharedInstance.callGoogleSignIn{ (result, error) in
            
            if (result != nil){
                let responseJSON = JSON(result as AnyObject);
                let statusCode = responseJSON["status"]
                let errorMsg = responseJSON["message"].string ?? "kErrorMsg".localized()
                AppHelper.makeDashbaordAsRootViewController()
                if statusCode == "200"{
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        UserDetails.sharedInstance.phoneNumber = response["phone"].stringValue
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        
                        UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                        UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                        UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                        UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"
                        
                        let userNameEdited = response["username_edited"].stringValue
                        AppHelper.saveUserDetails()
                        if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                        }
                        else{
                            AppHelper.makeDashbaordAsRootViewController()
                        }
                        
                        if let configDetailsDict = response["config_details"].dictionary{
                            if let configArray = configDetailsDict["configs"]?.array{
                                if configArray.count > 0{
                                    let sportsLandingOrderTemp = configArray[0]["sports_landing_order"].string ?? "1,2,3"
                                    //                                let sportsLandingOrderTemp = "1,2,3,4,5,6"
                                    let tempArray = sportsLandingOrderTemp.components(separatedBy: ",")
                                    var sportsLandingOrder = ""
                                    
                                    for str in tempArray {
                                        if (str == "1") || (str == "2") || (str == "3") || (str == "4") || (str == "5") || (str == "6"){
                                            if sportsLandingOrder.count == 0 {
                                                sportsLandingOrder = str
                                            }
                                            else{
                                                sportsLandingOrder = sportsLandingOrder + "," + str
                                            }
                                        }
                                    }
                                    UserDefaults.standard.set(sportsLandingOrder, forKey: "sports_landing_order")
                                    UserDefaults.standard.synchronize()
                                    let iOSMinVersion = configArray[0]["iphone_min_version"].string ?? ""
                                    let landingSport = configArray[0]["landing_sport"].string ?? ""
                                    if (landingSport == "1") || (landingSport == "2") || (landingSport == "3") || (landingSport == "4") || (landingSport == "5") || (landingSport == "6") {
                                        UserDefaults.standard.set(landingSport, forKey: "landingSport")
                                    }
                                    else{
                                        UserDefaults.standard.set("1", forKey: "landingSport")
                                    }
                                    
                                    let partnershipProgramStatuslandingSport = configArray[0]["partnership_program_status"].string ?? ""
                                    UserDetails.sharedInstance.isPartnershipProgramStatus = partnershipProgramStatuslandingSport
                                    let iOSMaxVersion = configArray[0]["iphone_max_version"].string ?? ""
                                    let updateDuration = configArray[0]["iphone_duration"].string ?? ""
                                    let partnershipProgramStatus = configArray[0]["partnership_program_status"].stringValue
                                    let rewardStatus = configArray[0]["reward_status"].string ?? ""
                                    let passStatus = configArray[0]["pass_status"].string ?? ""
                                    UserDefaults.standard.set(passStatus, forKey: "passStatus")
                                    UserDefaults.standard.set(rewardStatus, forKey: "rewardStatus")
                                    UserDefaults.standard.set(partnershipProgramStatus, forKey: "partnershipProgramStatus")
                                    UserDefaults.standard.set(iOSMinVersion, forKey: "iOSMinVersion")
                                    UserDefaults.standard.set(iOSMaxVersion, forKey: "iOSMaxVersion")
                                    UserDefaults.standard.synchronize()
                                }
                            }
                            
                            if let userInfo = configDetailsDict["user_info"]?.dictionary{
                                let total_classic = userInfo["total_classic"]?.stringValue
                                let total_classic_kb = userInfo["total_classic_kb"]?.stringValue
                                let total_classic_fb = userInfo["total_classic_fb"]?.stringValue
                                let total_classic_bk = userInfo["total_classic_bk"]?.stringValue
                                let total_classic_bs = userInfo["total_classic_bs"]?.stringValue
                                let total_bowling = userInfo["total_bowling"]?.stringValue
                                let total_batting = userInfo["total_batting"]?.stringValue
                                let total_reverse = userInfo["total_reverse"]?.stringValue
                                let total_wizard = userInfo["total_wizard"]?.stringValue
                                UserDefaults.standard.set(total_classic, forKey: "total_classic")
                                UserDefaults.standard.set(total_classic_kb, forKey: "total_classic_kb")
                                UserDefaults.standard.set(total_classic_fb, forKey: "total_classic_fb")
                                UserDefaults.standard.set(total_classic_bk, forKey: "total_classic_bsk")
                                UserDefaults.standard.set(total_classic_bs, forKey: "total_classic_bsb")
                                
                                UserDefaults.standard.set(total_bowling, forKey: "total_bowling")
                                UserDefaults.standard.set(total_batting, forKey: "total_batting")
                                UserDefaults.standard.set(total_reverse, forKey: "total_reverse")
                                UserDefaults.standard.set(total_wizard, forKey: "total_wizard")
                                
                                let params = [batting_contest: total_batting, bowling_contest: total_bowling, classic_contest: total_classic, kabaddi_contest: total_classic_kb, football_contest: total_classic_fb, basketball_contest: total_classic_bk, baseball_contest: total_classic_bs, device: PLATFORM_iPHONE, user_name: UserDetails.sharedInstance.userName, custome_name: UserDetails.sharedInstance.name]
                                CleverTapEventDetails.updateProfile(updateProfile: params as [String : Any])
                                UserDefaults.standard.synchronize()
                            }
                        }
                        
                        
                        
                        
                        
                        //                        if userNameEdited == "0"{
                        //                            let enterUserName = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController")
                        //                            weakSelf?.navigationController?.pushViewController(enterUserName!, animated: true)
                        //                        }
                        //                        else{
                        //                            AppHelper.saveUserDetails()
                        //                            if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                        //                            }
                        //                            else{
                        //                                AppHelper.makeDashbaordAsRootViewController()
                        //                            }
                        //                        }
                    }
                }
                else if statusCode == "650"{
                    
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        let userNameEdited = response["username_edited"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController")
                        weakSelf?.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                        
                        
                        //                        if userNameEdited == "0"{
                        //                            let enterUserName = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController")
                        //                            weakSelf?.navigationController?.pushViewController(enterUserName!, animated: true)
                        //                        }
                        //                        else{
                        //                            let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController")
                        //                            weakSelf?.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                        //                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
    
    @IBAction func forgotPasswordButtonTapped(_ sender: Any) {
        AppHelper.showToast(message: "Under Developement")
               return
        let forgotPasswordVC = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController")
        navigationController?.pushViewController(forgotPasswordVC!, animated: true)
    }
    
    @IBAction func signupButtonTapped(_ sender: Any) {
        let signupVC = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController")
        navigationController?.pushViewController(signupVC!, animated: true)
    }
    
    @IBAction func infoButtonTapped(_ sender: Any) {
        
        let privacyPolicyVC = storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
        privacyPolicyVC?.selectedType = SelectedWebViewType.HowToPlay.rawValue
        navigationController?.pushViewController(privacyPolicyVC!, animated: true)
    }
    
    
    //MARK:- API Related Methods
    
    func callLoginAPI(params: Parameters)  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        UserDetails.sharedInstance.isUserComingFromOnBoarding = true;
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kSignupURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if (result != nil){
                let statusCode = result!["status"]
                if statusCode == "200"{
                    if let responseAll = result!["response"]?.dictionary{
                        if let response = responseAll["user"]{
                            UserDetails.sharedInstance.accessToken = response["access_token"].string!
                            UserDetails.sharedInstance.userID = response["user_id"].string!
                            UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                            UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].string!
                           // UserDetails.sharedInstance.phoneNumber = response["phone"].string!
                            UserDetails.sharedInstance.name = response["name"].stringValue
                            UserDetails.sharedInstance.userName = response["username"].stringValue
                            UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                            UserDetails.sharedInstance.address = response["address"].stringValue
                            UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                            UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                            UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                            UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                            UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                            UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                            UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                            UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                            UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"
                            
                            AppHelper.saveUserDetails()
                            
                            if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                            }
                            else{
                                AppHelper.makeDashbaordAsRootViewController()
                            }
                            
                            if let configDetailsDict = responseAll["config_details"]?.dictionary{
                                if let configArray = configDetailsDict["configs"]?.array{
                                    if configArray.count > 0{
                                        let sportsLandingOrderTemp = configArray[0]["sports_landing_order"].string ?? "1,2,3"
                                        //                                let sportsLandingOrderTemp = "1,2,3,4,5,6"
                                        let tempArray = sportsLandingOrderTemp.components(separatedBy: ",")
                                        var sportsLandingOrder = ""
                                        
                                        for str in tempArray {
                                            if (str == "1") || (str == "2") || (str == "3") || (str == "4") || (str == "5") || (str == "6"){
                                                if sportsLandingOrder.count == 0 {
                                                    sportsLandingOrder = str
                                                }
                                                else{
                                                    sportsLandingOrder = sportsLandingOrder + "," + str
                                                }
                                            }
                                        }
                                        UserDefaults.standard.set(sportsLandingOrder, forKey: "sports_landing_order")
                                        UserDefaults.standard.synchronize()
                                        let iOSMinVersion = configArray[0]["iphone_min_version"].string ?? ""
                                        let landingSport = configArray[0]["landing_sport"].string ?? ""
                                        if (landingSport == "1") || (landingSport == "2") || (landingSport == "3") || (landingSport == "4") || (landingSport == "5") || (landingSport == "6") {
                                            UserDefaults.standard.set(landingSport, forKey: "landingSport")
                                        }
                                        else{
                                            UserDefaults.standard.set("1", forKey: "landingSport")
                                        }
                                        
                                        let partnershipProgramStatuslandingSport = configArray[0]["partnership_program_status"].string ?? ""
                                        UserDetails.sharedInstance.isPartnershipProgramStatus = partnershipProgramStatuslandingSport
                                        let iOSMaxVersion = configArray[0]["iphone_max_version"].string ?? ""
                                        let updateDuration = configArray[0]["iphone_duration"].string ?? ""
                                        let partnershipProgramStatus = configArray[0]["partnership_program_status"].stringValue
                                        let rewardStatus = configArray[0]["reward_status"].string ?? ""
                                        let passStatus = configArray[0]["pass_status"].string ?? ""
                                        UserDefaults.standard.set(passStatus, forKey: "passStatus")
                                        UserDefaults.standard.set(rewardStatus, forKey: "rewardStatus")
                                        UserDefaults.standard.set(partnershipProgramStatus, forKey: "partnershipProgramStatus")
                                        UserDefaults.standard.set(iOSMinVersion, forKey: "iOSMinVersion")
                                        UserDefaults.standard.set(iOSMaxVersion, forKey: "iOSMaxVersion")
                                        UserDefaults.standard.synchronize()
                                    }
                                }
                                
                                if let userInfo = configDetailsDict["user_info"]?.dictionary{
                                    let total_classic = userInfo["total_classic"]?.stringValue
                                    let total_classic_kb = userInfo["total_classic_kb"]?.stringValue
                                    let total_classic_fb = userInfo["total_classic_fb"]?.stringValue
                                    let total_classic_bk = userInfo["total_classic_bk"]?.stringValue
                                    let total_classic_bs = userInfo["total_classic_bs"]?.stringValue
                                    let total_bowling = userInfo["total_bowling"]?.stringValue
                                    let total_batting = userInfo["total_batting"]?.stringValue
                                    let total_reverse = userInfo["total_reverse"]?.stringValue
                                    let total_wizard = userInfo["total_wizard"]?.stringValue
                                    UserDefaults.standard.set(total_classic, forKey: "total_classic")
                                    UserDefaults.standard.set(total_classic_kb, forKey: "total_classic_kb")
                                    UserDefaults.standard.set(total_classic_fb, forKey: "total_classic_fb")
                                    UserDefaults.standard.set(total_classic_bk, forKey: "total_classic_bsk")
                                    UserDefaults.standard.set(total_classic_bs, forKey: "total_classic_bsb")
                                    
                                    UserDefaults.standard.set(total_bowling, forKey: "total_bowling")
                                    UserDefaults.standard.set(total_batting, forKey: "total_batting")
                                    UserDefaults.standard.set(total_reverse, forKey: "total_reverse")
                                    UserDefaults.standard.set(total_wizard, forKey: "total_wizard")
                                    
                                    let params = [batting_contest: total_batting, bowling_contest: total_bowling, classic_contest: total_classic, kabaddi_contest: total_classic_kb, football_contest: total_classic_fb, basketball_contest: total_classic_bk, baseball_contest: total_classic_bs, device: PLATFORM_iPHONE, user_name: UserDetails.sharedInstance.userName, custome_name: UserDetails.sharedInstance.name]
                                    CleverTapEventDetails.updateProfile(updateProfile: params as [String : Any])
                                    UserDefaults.standard.synchronize()
                                }
                            }
                            
                        }
                    }
                    
                }
                else if statusCode == "650"{
                    if let response = result!["response"]{
                        UserDetails.sharedInstance.accessToken = response["access_token"].string!
                        UserDetails.sharedInstance.userID = response["user_id"].string!
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].string!
                        let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController")
                        weakSelf?.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                    }
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!, isErrorMessage: true)
                }
            }
        }
        
    }
    
    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if string == " " {
            return false
        }
        
        return true
    }
    
    
    // MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
