//
//  WebViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!

    var urlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let URL = URL(string: urlString) {
            webView.load(URLRequest(url: URL))
            AppHelper.sharedInstance.displaySpinner()
        }
        else{
            let trimmedString = urlString.trimmingCharacters(in: .whitespaces)
            if let URL = URL(string: trimmedString) {
                webView.load(URLRequest(url: URL))
                AppHelper.sharedInstance.displaySpinner()
            }
        }
        webView.navigationDelegate = self
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        AppHelper.sharedInstance.removeSpinner()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        AppHelper.sharedInstance.removeSpinner()
    }
    
}
