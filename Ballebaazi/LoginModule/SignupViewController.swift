//
//  SignupViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/30/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SignupViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: LoginCustomTextField!
    
    @IBOutlet weak var passwordTextField: LoginCustomTextField!
    
    @IBOutlet weak var promocodeTextField: UITextField!
    
    @IBOutlet weak var txtFieldUserName: LoginCustomTextField!
    //MARK:- View Life Cycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
//        MixPanelEventsDetails.updateBranchEvent()
        if APPDELEGATE.invitationCode != kNoValue{
            promocodeTextField.text = APPDELEGATE.invitationCode;
        }
        navigationController?.navigationBar.isHidden = false;
        promocodeTextField.attributedPlaceholder = NSAttributedString(string: promocodeTextField.placeholder!, attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 208.0/255, green: 190.0/255, blue: 190.0/255, alpha: 1)])
        
        AppHelper.addBackButtonOnNavigationbar(title: "  Register", target: self, isNeedToShowShadow: false)
    }
    

    // MARK:- IBAction Methods
    @IBAction func googleButtonTapped(_ sender: Any) {
        if UserDetails.sharedInstance.guestAccessToken.count == 0 {
            AppHelper.getGuestToken()
        }
        weak var weakSelf = self

        GoogleLoginManager.sharedInstance.callGoogleSignIn{ (result, error) in
            
            if (result != nil){
                let responseJSON = JSON(result as AnyObject);
                let statusCode = responseJSON["status"]
                
                if statusCode == "200"{
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        UserDetails.sharedInstance.phoneNumber = response["phone"].stringValue
                        let userNameEdited = response["username_edited"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].string ?? ""
                        UserDetails.sharedInstance.name = response["name"].string ?? ""
                        let enterUserName = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController")
                        weakSelf!.navigationController?.pushViewController(enterUserName!, animated: true)

//                        if userNameEdited == "0"{
//                            let enterUserName = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController")
//                            weakSelf!.navigationController?.pushViewController(enterUserName!, animated: true)
//
//                        }
//                        else{
//                            AppHelper.saveUserDetails()
//                            if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
//                            }
//                            else{
//                                AppHelper.makeDashbaordAsRootViewController()
//                            }
//                        }
                    }
                }
                else if statusCode == "650"{
                    
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        let userNameEdited = response["username_edited"].stringValue
                        let enterPhoneVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController")
                        weakSelf!.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                        
//                        if userNameEdited == "0"{
//                            let enterUserName = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController")
//                            weakSelf!.navigationController?.pushViewController(enterUserName!, animated: true)
//                        }
//                        else{
//                            let enterPhoneVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController")
//                            weakSelf!.navigationController?.pushViewController(enterPhoneVC!, animated: true)
//                        }
                    }
                }
                else{
                    let errorMsg = result!["message"] as! String
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
        if UserDetails.sharedInstance.guestAccessToken.count == 0 {
            AppHelper.getGuestToken()
            return
        }
        weak var weakSelf = self

        FacebookLoginManager.sharedInstance.callLoginMangerWithCompletion { (result, error) in
            
            if (result != nil){
                let responseJSON = JSON(result as AnyObject);
                let statusCode = responseJSON["status"]
                
                if statusCode == "200"{
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        UserDetails.sharedInstance.phoneNumber = response["phone"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].string ?? ""
                        UserDetails.sharedInstance.name = response["name"].string ?? ""
                        
                        let userNameEdited = response["username_edited"].stringValue
                        AppHelper.saveUserDetails()
                        if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                        }
                        else{
                            AppHelper.makeDashbaordAsRootViewController()
                        }

//                        if userNameEdited == "0"{
//                            let enterUserName = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController")
//                            weakSelf!.navigationController?.pushViewController(enterUserName!, animated: true)
//                        }
//                        else{
//                            AppHelper.saveUserDetails()
//                            if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
//                            }
//                            else{
//                                AppHelper.makeDashbaordAsRootViewController()
//                            }
//                        }
                    }
                }
                else if statusCode == "650"{
                    
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        
                        let userNameEdited = response["username_edited"].stringValue
                        let enterPhoneVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController")
                        weakSelf!.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                        
//                        if userNameEdited == "0"{
//                            let enterUserName = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController")
//                            weakSelf!.navigationController?.pushViewController(enterUserName!, animated: true)
//                        }
//                        else{
//                            let enterPhoneVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController")
//                            weakSelf!.navigationController?.pushViewController(enterPhoneVC!, animated: true)
//                        }
                    }
                }
                else{
                    let errorMsg = result!["message"] as! String
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
    
    @IBAction func registerButtonTapped(_ sender: Any) {
        
        if UserDetails.sharedInstance.guestAccessToken.count == 0 {
            AppHelper.getGuestToken()
            return
        }
        
        if txtFieldUserName.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter user name.", isErrorMessage: true)
            return
        }
        else if passwordTextField.text?.count == 0{
            AppHelper.showAlertView(message: "EnterPasswordMsg".localized(), isErrorMessage: true)
            return
        }
        else if (passwordTextField.text?.count)! < 6{
            AppHelper.showAlertView(message: "MinPasswordLenghtMsg".localized(), isErrorMessage: true)
            return
        }
        
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        promocodeTextField.resignFirstResponder()
        weak var weakSelf = self

        let privacyView = AppHelper.showPrivacyPolicyView()
        privacyView.getUserStatusOnPrivacyPolicy { (sucess) in
            if sucess{
                
                var deviceToken: String = UserDetails.sharedInstance.deviceToken
                
                if deviceToken.count == 0 {
                    deviceToken = "1111111111";
                }
                let deviceID = UIDevice.current.identifierForVendor!.uuidString
                
                let parameters = ["option": "register_mobile", "email": (weakSelf!.emailTextField.text ?? ""), "password": weakSelf!.passwordTextField.text!, "promocode": (weakSelf!.promocodeTextField.text ?? ""), "account_type": "1", "confirm_password": weakSelf!.passwordTextField.text!,"phone": "", "device_type": "1", "device_token": deviceToken, "username": weakSelf!.txtFieldUserName.text!, "device_id": deviceID]
                
                UserDetails.sharedInstance.emailAdress = weakSelf!.emailTextField.text!
                weakSelf!.callSignupAPI(params: parameters)
            }
        }
    }
    
    //MARK:- API Related Methods
    func callSignupAPI(params : Parameters)  {
        
        AppHelper.sharedInstance.displaySpinner()
        UserDetails.sharedInstance.isUserComingFromOnBoarding = true;
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kSignupURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if (result != nil){
                let statusCode = result!["status"]
                let errorMsg = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200"{
                    
                }
                else if statusCode == "650"{
                    
                    if let response = result!["response"]{
                        UserDetails.sharedInstance.accessToken = response["access_token"].string!
                        UserDetails.sharedInstance.userID = response["user_id"].string!
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.userName = response["username"].string ?? ""
                        UserDetails.sharedInstance.name = response["name"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].string!
                        UserDetails.sharedInstance.signupCode = weakSelf!.promocodeTextField.text ?? "N/A"

                        let enterPhoneVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController")
                        weakSelf!.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                    }                    
                }
                else{
                    let errorMsg = errorMsg
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
    
    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if string == " " {
            return false
        }
        return true
    }
    
    // MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension UIViewController{
    
    @objc func backButtonTapped() {

        if navigationController?.navigationBar.tag == 2000 {
            AppHelper.makeDashbaordAsRootViewController()
        }
        else if navigationController?.navigationBar.tag == 3000 {

            let alert = UIAlertController(title: kAlert, message: "ExitMessage".localized(), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action -> Void in
                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    let navArray = navigationVC.viewControllers
                    var isfoundVC = false
                    for selectPlayerVC in navArray {
                        if selectPlayerVC is JoinedLeagueViewController{
                            navigationVC.popToViewController(selectPlayerVC, animated: true)
                            isfoundVC = true;
                            break;
                        }
                    }
                    
                    if !isfoundVC{
                        navigationVC.popToRootViewController(animated: true)
                    }
                }
                
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action -> Void in
            }))
            
            APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        else if navigationController?.navigationBar.tag == 4000 {
            
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                let navArray = navigationVC.viewControllers
                var isfoundVC = false
                for selectPlayerVC in navArray {
                    if selectPlayerVC is LeagueViewController{
                        navigationVC.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                }
                
                if !isfoundVC{
                    navigationVC.popToRootViewController(animated: true)
                }
            }
            
            /*
            let alert = UIAlertController(title: kAlert, message: "ExitMessage".localized(), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action -> Void in
                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    let navArray = navigationVC.viewControllers
                    var isfoundVC = false
                    for selectPlayerVC in navArray {
                        if selectPlayerVC is LeagueViewController{
                            navigationVC.popToViewController(selectPlayerVC, animated: true)
                            isfoundVC = true;
                            break;
                        }
                    }
                    
                    if !isfoundVC{
                        navigationVC.popToRootViewController(animated: true)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action -> Void in
            }))
            
            APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
            */
        }
        else{
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func floatingButtonTapped() {
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
        privacyPolicyVC?.selectedType = SelectedWebViewType.Floating.rawValue
        navigationController?.pushViewController(privacyPolicyVC!, animated: true)
        
    }

}
