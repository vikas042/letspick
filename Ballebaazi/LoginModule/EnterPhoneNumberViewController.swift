//
//  EnterPhoneNumberViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/4/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire



class EnterPhoneNumberViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var sendOtpButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    var isNeedToUpdateUserName = false
    var isNeedToShowJoinLeagueScreen = false
    var facebookId = ""
    var googleId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true;
        view.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        sendOtpButton.removebackgroundGrident()
        sendOtpButton.backgroundGrident()
    }

    //MARK:- IBAction Methods
    @IBAction func sendOtpButtonTapped(_ sender: Any) {
        if phoneNumberTextField.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter your mobile number", isErrorMessage: true)
            return
        }
        else if (phoneNumberTextField.text?.count)! < 10{
            AppHelper.showAlertView(message: "Please enter valid mobile number", isErrorMessage: true)
            return
        }
        
//        let parameters: Parameters = ["option": "verify_phone_request", "mobile": phoneNumberTextField.text!, "user_id": UserDetails.sharedInstance.userID]
        
        callLoginAPI()
    }
    
    //MARK:- API Related Methods
    
        
    func callLoginAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        var deviceToken: String = UserDetails.sharedInstance.deviceToken
        
        if deviceToken.count == 0 {
            deviceToken = "1111111111";
        }

        let params = ["option": "signup_otp_v1", "mobile" : phoneNumberTextField.text!, "device_type": "1", "device_id":deviceID, "device_token": deviceToken, "facebook_id": facebookId, "google_id": googleId]
        AppHelper.sharedInstance.displaySpinner()
        UserDetails.sharedInstance.isUserComingFromOnBoarding = true;
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kSignupURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
                        
            CleverTapEventDetails.sendEventToCleverTap(eventName: otp_requested, params: [otp_requested_date: Date(), PLATFORM: PLATFORM_iPHONE, mobile_number: self.phoneNumberTextField.text ?? ""])

            if (result != nil){
                let statusCode = result!["status"]
                if statusCode == "200"{
                    if let response = result!["response"]{
                        UserDetails.sharedInstance.accessToken = response["access_token"].string!
                        UserDetails.sharedInstance.userID = response["user_id"].string!
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].string!
                        UserDetails.sharedInstance.phoneNumber = response["phone"].string!
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                        UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                        UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                        UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"
                        AppHelper.saveUserDetails()
                        
                        let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as? EnterOTPViewController
                        enterPhoneVC?.facebookId = self.facebookId
                        enterPhoneVC?.googleId = self.googleId
                        enterPhoneVC?.isNeedToUpdateUserName = true;
                        enterPhoneVC?.signupMode = "Phone Number"
                        if APPDELEGATE.leagueCode.count > 0{
                            enterPhoneVC?.isNeedToUpdateUserName = true;
                        }
                            
                        weakSelf?.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                    }
                }
                else if statusCode == "205"{
                    let enterOTPVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as? EnterOTPViewController
                    enterOTPVC?.googleId = self.googleId
                    enterOTPVC?.facebookId = self.facebookId

                    UserDetails.sharedInstance.phoneNumber = weakSelf!.phoneNumberTextField.text!
                    enterOTPVC?.signupMode = "Phone Number"
                    enterOTPVC?.isNeedToUpdateUserName = true; weakSelf?.navigationController?.pushViewController(enterOTPVC!, animated: true)
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!, isErrorMessage: true)
                }
            }
        }
    }
    
//    func callOtpAPI(params: Parameters)  {
//        if !AppHelper.isInterNetConnectionAvailable() {
//            return
//        }
//        weak var weakSelf = self
//
//        AppHelper.sharedInstance.displaySpinner()
//        WebServiceHandler.performPOSTRequest(urlString: kVerifyPhone, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
//
//            AppHelper.sharedInstance.removeSpinner()
//            CleverTapEventDetails.sendEventToCleverTap(eventName: otp_requested, params: [otp_requested_date: Date(), PLATFORM: PLATFORM_iPHONE, mobile_number: self.phoneNumberTextField.text ?? ""])
//
//            if (result != nil){
//                let statusCode = result!["status"]
//                let errorMsg = result!["message"]?.string ?? "kErrorMsg".localized()
//
//                if statusCode == "200"{
//                    UserDetails.sharedInstance.phoneNumber = weakSelf!.phoneNumberTextField.text!
//                    let enterOtpVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController")
//                    UserDetails.sharedInstance.phoneNumber = weakSelf!.phoneNumberTextField.text!
//                    weakSelf!.navigationController?.pushViewController(enterOtpVC!, animated: true)
//                }
//                else{
//                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
//                }
//            }
//        }
//    }

    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if string == "" {
            return true;
        }
        if string == " " {
            return false
        }
        
        let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
        if (textField.text?.count)! < 10 {
            return isNumber
        }
        
        return false
    }
        
    // MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
