
//
//  EnterOTPViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/4/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EnterOTPViewController: UIViewController, UITextFieldDelegate, LoginCustomTextFieldDelegate {

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblResndMesssage: UILabel!
    @IBOutlet weak var resendButton: SolidButton!
    @IBOutlet var textFieldsArray: [LoginCustomTextField]!
    var signupMode = ""
    
    var otpStr = ""
    var facebookId = ""
    var googleId = ""

    var isNeedToUpdateUserName = false
    var isNeedToShowJoinLeagueScreen = false
    
    var isNewUser = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        AppHelper.callConfigAPI();

        resendButton.isHidden = true;
        lblResndMesssage.isHidden = true;
        lblMobileNumber.text = "OTP sent to " + UserDetails.sharedInstance.phoneNumber
        
        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            Timer.scheduledTimer(withTimeInterval: 30.0, repeats: false){_ in
                weakSelf?.showResendButton()
            }
        }
        else {
            Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.showResendButton), userInfo: nil, repeats: false)
        }

    }
    
    override func viewDidLayoutSubviews() {
        nextButton.removebackgroundGrident()
        nextButton.backgroundGrident()
    }
    
    override func viewDidAppear(_ animated: Bool) {
                
        let txtField = textFieldsArray[0]
        txtField.becomeFirstResponder()
    }
    
    @objc func showResendButton() {
        resendButton.isHidden = false;
        lblResndMesssage.isHidden = false;
    }
    
    func showTimer() {
        resendButton.isHidden = true;
        lblResndMesssage.isHidden = true;
        
        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            Timer.scheduledTimer(withTimeInterval: 60.0, repeats: false){_ in
                weakSelf?.showResendButton()
            }
        }
        else {
            Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.showResendButton), userInfo: nil, repeats: false)
        }

    }
    
    
    // MARK:- IBAction Methods
    @IBAction func submitButtonTapped(_ sender: Any) {
        otpStr = ""

        for txtField in textFieldsArray {
            if txtField.text != ""{
                otpStr = otpStr + txtField.text!
            }
            else{
                AppHelper.showAlertView(message: "Please enter OTP.", isErrorMessage: true)
                return;
            }
        }
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        var deviceToken: String = UserDetails.sharedInstance.deviceToken

        if deviceToken.count == 0 {
            deviceToken = "";
        }

        AppxorEventHandler.logAppEvent(withName: "VerfiyOTPClicked", info: nil)

//        if UserDetails.sharedInstance.userID.count > 0{
//            let parameters: Parameters = ["option": "verify_phone", "user_id": UserDetails.sharedInstance.userID, "otp": otpStr]
//            callVerifySocialOtpAPI(params: parameters)
//        }
//        else{
//
//            let parameters: Parameters = ["option": "signup_otp", "user_id": UserDetails.sharedInstance.userID, "otp": otpStr, "mobile": UserDetails.sharedInstance.phoneNumber, "verify": "1", "device_type": "1", "device_id": deviceID, "device_token": deviceToken, "facebook_id": facebookId, "google_id": googleId]
//            callVerifyPhoneOtpAPI(params: parameters)
//        }
        
        let parameters: Parameters = ["option": "signup_otp_v1", "user_id": UserDetails.sharedInstance.userID, "otp": otpStr, "mobile": UserDetails.sharedInstance.phoneNumber, "verify": "1", "device_type": "1", "device_id": deviceID, "device_token": deviceToken, "facebook_id": facebookId, "google_id": googleId]
        callVerifyPhoneOtpAPI(params: parameters)
    }
    
    @IBAction func changePhoneNumberButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "EditNumberButtonClicked", info: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendOtpButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "ResendOTPClicked", info: nil)

        if UserDetails.sharedInstance.userID.count > 0 {
            let parameters: Parameters = ["option": "verify_phone_request", "mobile": UserDetails.sharedInstance.phoneNumber, "user_id": UserDetails.sharedInstance.userID]
            callOtpAPI(params: parameters, urlString: kVerifyPhone, accessToken: UserDetails.sharedInstance.accessToken)
        }
        else{
            let params = ["option": "signup_otp_v1", "mobile" : UserDetails.sharedInstance.phoneNumber]
            callOtpAPI(params: params, urlString: kSignupURL, accessToken: UserDetails.sharedInstance.guestAccessToken)
        }
    }
    
    //MARK:- API Related Methods
    
    func callVerifySocialOtpAPI(params: Parameters)  {
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        WebServiceHandler.performPOSTRequest(urlString: kVerifyPhone, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if (result != nil){
                let statusCode = result!["status"]
                let errorMsg = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200"{
                    let responseJSON = JSON(result as AnyObject);

                    if let response = responseJSON.dictionary!["response"]{
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        UserDetails.sharedInstance.phoneNumber = response["phone"].stringValue
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        
                        UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                        UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                        UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                        UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"
                        AppHelper.saveUserDetails()

                        let phoneNumber = "+91" + UserDetails.sharedInstance.phoneNumber

                        var profileUpdate = [name : UserDetails.sharedInstance.name, user_id: UserDetails.sharedInstance.userID, identity: UserDetails.sharedInstance.userID, last_login_date: Date(), phone_number: phoneNumber, referral_code_used: APPDELEGATE.invitationCode, PLATFORM: PLATFORM_iPHONE] as [String : Any]
                        
                        profileUpdate[registration_date] = AppHelper.getUserRegistrationDate(dateString: UserDetails.sharedInstance.registeredDate)

                        if UserDetails.sharedInstance.emailAdress.count > 0{
                            profileUpdate[email_address] = UserDetails.sharedInstance.emailAdress;
                        }
                        CleverTapEventDetails.updateLoginEvent(params: profileUpdate)
//                        MixPanelEventsDetails.updateBranchEvent(registrationMode: "Social")
                        MixPanelEventsDetails.completeLogin();
                        
                        let userNameEdited = response["username_edited"].stringValue
                        
                        CleverTapEventDetails.sendEventToCleverTap(eventName: login, params: [login_date: Date(), mobile_number: UserDetails.sharedInstance.phoneNumber, onBoarding_medium: PLATFORM_iPHONE])
                        AppHelper.saveUserDetails()
                        MixPanelEventsDetails.intialiseMixpanel()
                        MixPanelEventsDetails.setupSuperProperties()
                        MixPanelEventsDetails.setPeopleProperties(isFromEdit: false)
                        if (APPDELEGATE.leagueCode.count > 0) && (APPDELEGATE.leagueCode != kNoValue){                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                        }
                        else{
                            AppHelper.makeDashbaordAsRootViewController()
                        }
                        
//                        if userNameEdited == "0"{
//
//                            let enterUserName = self.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController") as! EnterUserNameViewController
//                            enterUserName.signupMode = self.signupMode;
//                            self.navigationController?.pushViewController(enterUserName, animated: true)
//                        }
//                        else{
//
//                            CleverTapEventDetails.sendEventToCleverTap(eventName: login, params: [login_date: Date(), mobile_number: UserDetails.sharedInstance.phoneNumber, onBoarding_medium: PLATFORM_iPHONE])
//                            AppHelper.saveUserDetails()
//                            MixPanelEventsDetails.intialiseMixpanel()
//                            MixPanelEventsDetails.setupSuperProperties()
//                            MixPanelEventsDetails.setPeopleProperties(isFromEdit: false)
//                            if (APPDELEGATE.leagueCode.count > 0) && (APPDELEGATE.leagueCode != kNoValue){                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
//                            }
//                            else{
//                                AppHelper.makeDashbaordAsRootViewController()
//                            }
//                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
    
    
    func callVerifyPhoneOtpAPI(params: Parameters)  {
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        WebServiceHandler.performPOSTRequest(urlString: kSignupURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            DispatchQueue.main.async {
                AppHelper.sharedInstance.removeSpinner()
            }
            
            if (result != nil){
                let statusCode = result!["status"]
                let errorMsg = result!["message"]?.string ?? "kErrorMsg".localized()
                
                if statusCode == "200"{
                    let responseJSON = JSON(result as AnyObject);
                    if let response = responseJSON.dictionary!["response"]{
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        UserDetails.sharedInstance.phoneNumber = response["phone"].stringValue
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].stringValue
                        
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        
                        UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                        UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                        UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                        UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"                        
                        AppHelper.saveUserDetails()
                        let phoneNumber = "+91" + UserDetails.sharedInstance.phoneNumber
                        
                        var profileUpdate = [name : UserDetails.sharedInstance.name, user_id: UserDetails.sharedInstance.userID, user_name : UserDetails.sharedInstance.userName, identity: UserDetails.sharedInstance.userID, last_login_date: Date(), phone_number: phoneNumber, referral_code_used: APPDELEGATE.invitationCode, PLATFORM: PLATFORM_iPHONE] as [String : Any]
                        
                        profileUpdate[registration_date] = AppHelper.getUserRegistrationDate(dateString: UserDetails.sharedInstance.registeredDate)

                        
                        if UserDetails.sharedInstance.emailAdress.count > 0{
                            profileUpdate[email_address] = UserDetails.sharedInstance.emailAdress;
                        }
                        CleverTapEventDetails.updateLoginEvent(params: profileUpdate)
                        let userNameEdited = response["username_edited"].stringValue
                        MixPanelEventsDetails.loginBranchIdentity()
                        CleverTapEventDetails.sendEventToCleverTap(eventName: login, params: [last_login_date: Date(), mobile_number: UserDetails.sharedInstance.phoneNumber, onBoarding_medium: PLATFORM_iPHONE])

                        AppHelper.saveUserDetails()
                        MixPanelEventsDetails.intialiseMixpanel()
                        MixPanelEventsDetails.setupSuperProperties()
                        MixPanelEventsDetails.setPeopleProperties(isFromEdit: false)
                        MixPanelEventsDetails.completeLogin();

                        if self.isNewUser {
                            var chanelName = UserDetails.sharedInstance.channel
                            
                            if chanelName.count == 0 {
                                chanelName = kDirect
                            }

                            var params = [String: Any]()
                            params[verification] = "Yes"
                            params[sign_up_code] = APPDELEGATE.invitationCode
                            params[signup_channel] = chanelName
                            params[PLATFORM] = PLATFORM_iPHONE
                            params[onBoarding_medium] = PLATFORM_iPHONE
                            params[user_name] = UserDetails.sharedInstance.userName
                            params[custome_name] = UserDetails.sharedInstance.name
                            params[mode] = self.signupMode
                            params[registration_date] = AppHelper.getUserRegistrationDate(dateString: UserDetails.sharedInstance.registeredDate)
                            CleverTapEventDetails.sendEventToCleverTap(eventName: sign_up, params: params)
                            CleverTapEventDetails.updateProfile(updateProfile: [signup_channel: chanelName])
                        }
                        
                        if (APPDELEGATE.leagueCode.count > 0) && (APPDELEGATE.leagueCode != kNoValue){                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                        }
                        else{
                            AppHelper.makeDashbaordAsRootViewController()
                        }
                        
                        
                        if let configDetailsDict = response["config_details"].dictionary{
                            if let configArray = configDetailsDict["configs"]?.array{
                                if configArray.count > 0{
                                    let sportsLandingOrderTemp = configArray[0]["sports_landing_order"].string ?? "1,2,3"
    //                                let sportsLandingOrderTemp = "1,2,3,4,5,6"
                                    let tempArray = sportsLandingOrderTemp.components(separatedBy: ",")
                                    var sportsLandingOrder = ""
                                    
                                    for str in tempArray {
                                        if (str == "1") || (str == "2") || (str == "3") || (str == "4") || (str == "5") || (str == "6"){
                                            if sportsLandingOrder.count == 0 {
                                                sportsLandingOrder = str
                                            }
                                            else{
                                                sportsLandingOrder = sportsLandingOrder + "," + str
                                            }
                                        }
                                    }
                                    UserDefaults.standard.set(sportsLandingOrder, forKey: "sports_landing_order")
                                    UserDefaults.standard.synchronize()
                                    let iOSMinVersion = configArray[0]["iphone_min_version"].string ?? ""
                                    let landingSport = configArray[0]["landing_sport"].string ?? ""
                                    if (landingSport == "1") || (landingSport == "2") || (landingSport == "3") || (landingSport == "4") || (landingSport == "5") || (landingSport == "6") {
                                        UserDefaults.standard.set(landingSport, forKey: "landingSport")
                                    }
                                    else{
                                        UserDefaults.standard.set("1", forKey: "landingSport")
                                    }

                                    let partnershipProgramStatuslandingSport = configArray[0]["partnership_program_status"].string ?? ""
                                    UserDetails.sharedInstance.isPartnershipProgramStatus = partnershipProgramStatuslandingSport
                                    let iOSMaxVersion = configArray[0]["iphone_max_version"].string ?? ""
                                    let updateDuration = configArray[0]["iphone_duration"].string ?? ""
                                    let partnershipProgramStatus = configArray[0]["partnership_program_status"].stringValue
                                    let rewardStatus = configArray[0]["reward_status"].string ?? ""
                                    let passStatus = configArray[0]["pass_status"].string ?? ""
                                    UserDefaults.standard.set(passStatus, forKey: "passStatus")
                                    UserDefaults.standard.set(rewardStatus, forKey: "rewardStatus")
                                    UserDefaults.standard.set(partnershipProgramStatus, forKey: "partnershipProgramStatus")
                                    UserDefaults.standard.set(iOSMinVersion, forKey: "iOSMinVersion")
                                    UserDefaults.standard.set(iOSMaxVersion, forKey: "iOSMaxVersion")
                                    UserDefaults.standard.synchronize()
                                }
                            }
                            
                            if let userInfo = configDetailsDict["user_info"]?.dictionary{
                                let total_classic = userInfo["total_classic"]?.stringValue
                                let total_classic_kb = userInfo["total_classic_kb"]?.stringValue
                                let total_classic_fb = userInfo["total_classic_fb"]?.stringValue
                                let total_classic_bk = userInfo["total_classic_bk"]?.stringValue
                                let total_classic_bs = userInfo["total_classic_bs"]?.stringValue
                                let total_bowling = userInfo["total_bowling"]?.stringValue
                                let total_batting = userInfo["total_batting"]?.stringValue
                                let total_reverse = userInfo["total_reverse"]?.stringValue
                                let total_wizard = userInfo["total_wizard"]?.stringValue
                                UserDefaults.standard.set(total_classic, forKey: "total_classic")
                                UserDefaults.standard.set(total_classic_kb, forKey: "total_classic_kb")
                                UserDefaults.standard.set(total_classic_fb, forKey: "total_classic_fb")
                                UserDefaults.standard.set(total_classic_bk, forKey: "total_classic_bsk")
                                UserDefaults.standard.set(total_classic_bs, forKey: "total_classic_bsb")

                                UserDefaults.standard.set(total_bowling, forKey: "total_bowling")
                                UserDefaults.standard.set(total_batting, forKey: "total_batting")
                                UserDefaults.standard.set(total_reverse, forKey: "total_reverse")
                                UserDefaults.standard.set(total_wizard, forKey: "total_wizard")

                                let params = [batting_contest: total_batting, bowling_contest: total_bowling, classic_contest: total_classic, kabaddi_contest: total_classic_kb, football_contest: total_classic_fb, basketball_contest: total_classic_bk, baseball_contest: total_classic_bs, device: PLATFORM_iPHONE, user_name: UserDetails.sharedInstance.userName, custome_name: UserDetails.sharedInstance.name]
                                CleverTapEventDetails.updateProfile(updateProfile: params as [String : Any])
                                UserDefaults.standard.synchronize()
                            }
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
    
    func callOtpAPI(params: Parameters, urlString: String, accessToken: String)  {
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if (result != nil){
                let statusCode = result!["status"]
                let errorMsg = result!["message"]?.string ?? "kErrorMsg".localized()
                
                if (statusCode == "200") || statusCode == "205"{
                    CleverTapEventDetails.sendEventToCleverTap(eventName: otp_requested, params: [otp_requested_date: Date(), PLATFORM: PLATFORM_iPHONE, mobile_number: UserDetails.sharedInstance.phoneNumber])
                    AppHelper.showAlertView(message: "OTP has been sent to your mobile number", isErrorMessage: false)
                    self.showTimer()
                }
                else{
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
    
    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if string.count != 0 {
            if string == " " {
                return false
            }

            let lenght = range.location
            if (string.count == 1) && (lenght == 0) {
                fillTheOtp(textField: textField, string: string)
            }
            else{
                if lenght < textFieldsArray.count{
                   textFieldsArray[lenght].text = string
                }
            }
        }
        if string == UIPasteboard.general.string{
            print("Print")
        }

        return true
    }

    func textField(_ textField: UITextField, didDeleteBackwardAnd wasEmpty: Bool) {
        if wasEmpty {

            let index = textField.tag - 11
            if index >= 0{
                textFieldsArray[index].becomeFirstResponder()
            }
        }
    }

    func autofillTheOtp(clipboardString: String)  {
        if clipboardString.count > 6 {
            return
        }
        
        for index in 0..<clipboardString.count {
            textFieldsArray[index].text = clipboardString[index]
        }
    }
    
    func fillTheOtp(textField: UITextField, string: String)  {
        weak var weakSelf = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
            
            if textField == weakSelf!.textFieldsArray[0]{
                if string == ""{
                    
                }else{
                    if (weakSelf!.textFieldsArray[1].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[1].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[0].text = string
                        weakSelf!.textFieldsArray[0].resignFirstResponder()
                    }
                }
            }else if textField == weakSelf!.textFieldsArray[1]{
                if string == ""{
                    weakSelf!.textFieldsArray[0].becomeFirstResponder()
                    
                }else{
                    if (weakSelf!.textFieldsArray[2].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[2].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[1].text = string
                        weakSelf!.textFieldsArray[1].resignFirstResponder()
                    }
                }
            }else if textField == weakSelf!.textFieldsArray[2]{
                if string == ""{
                    weakSelf!.textFieldsArray[1].becomeFirstResponder()
                    
                }else{
                    if (weakSelf!.textFieldsArray[3].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[3].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[2].text = string
                        weakSelf!.textFieldsArray[2].resignFirstResponder()
                    }
                }
                
            }else if textField == weakSelf!.textFieldsArray[3]{
                if string == ""{
                    weakSelf!.textFieldsArray[2].becomeFirstResponder()
                    
                }else{
                    if (weakSelf!.textFieldsArray[4].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[4].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[3].text = string
                        weakSelf!.textFieldsArray[3].resignFirstResponder()
                    }
                }
                
            }else if textField == weakSelf!.textFieldsArray[4]{
                if string == ""{
                    weakSelf!.textFieldsArray[3].becomeFirstResponder()
                    
                }else{
                    if (weakSelf!.textFieldsArray[5].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[5].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[4].text = string
                        weakSelf!.textFieldsArray[4].resignFirstResponder()
                    }
                }
                
            }else if textField == weakSelf!.textFieldsArray[5]{
                if string == ""{
                    weakSelf!.textFieldsArray[4].becomeFirstResponder()
                }else{
                    textField.text = string
                    textField.resignFirstResponder()
                }
            }
        }
    }
    
    // MARK:- Memory Management Method

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension String {
    
    var length: Int {
        return count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
}
