//
//  FacebookLoginManager.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/30/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import Alamofire


class FacebookLoginManager: NSObject {

    var facebookLoginCompletion = { (result: AnyObject?, error: Error?) -> () in }

    static let sharedInstance = FacebookLoginManager()
    
    func callLoginMangerWithCompletion(completionblock: @escaping(_ result: AnyObject?, _ error: Error?) -> Void) {
        
        let loginManager = LoginManager()
        facebookLoginCompletion = completionblock
        if AccessToken.isCurrentAccessTokenActive {
            AppHelper.sharedInstance.displaySpinner()
            getFBUserData()
        }
        else{
            weak var weakSelf = self

            loginManager.logIn(permissions: [.publicProfile, .email], viewController : nil) { loginResult in
                switch loginResult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login")
                case .success( _, _, _):
                    
                    AppHelper.sharedInstance.displaySpinner()
                    weakSelf!.getFBUserData()
                }
            }
        }
    }
    
    // MARK: Get User Data From Facebook
    
    func getFBUserData(){
        weak var weakSelf = self

        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let dict = result as! [String : AnyObject]
                    weakSelf!.callFacebookLoginApi(dict: dict)
                }
                else{
                    AppHelper.sharedInstance.removeSpinner()
                }
            })
        }
    }
    
    func callFacebookLoginApi(dict :Dictionary<String, AnyObject>)  {
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        var deviceToken: String = UserDetails.sharedInstance.deviceToken
        
        if deviceToken.count == 0 {
            deviceToken = "1111111111";
        }
        
        let facebookId = dict["id"] as! String
        let userName = dict["name"] as? String
        let userEmail = dict["email"] as? String
        
        UserDetails.sharedInstance.name = userName!
        UserDetails.sharedInstance.emailAdress = userEmail ?? ""
        
//        let parameters: Parameters = ["option": "register","name": UserDetails.sharedInstance.name, "email": UserDetails.sharedInstance.emailAdress, "facebook_id": facebookId, "account_type": "2", "device_type": "1", "device_id": deviceID, "device_token": deviceToken]
        weak var weakSelf = self



        let parameters = ["option": "social", "device_type": "1", "device_id":deviceID, "device_token": deviceToken, "facebook_id": facebookId, "name": UserDetails.sharedInstance.name, "email": UserDetails.sharedInstance.emailAdress]

        
        WebServiceHandler.performPOSTRequest(urlString: kSignupURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                weakSelf!.facebookLoginCompletion(result as AnyObject,error)
            }
        }
    }
}
