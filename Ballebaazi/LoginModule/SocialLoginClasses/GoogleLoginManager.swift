
//
//  GoogleLoginManager.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/30/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import GoogleSignIn
import Alamofire

class GoogleLoginManager: NSObject, GIDSignInUIDelegate, GIDSignInDelegate {

    static let sharedInstance = GoogleLoginManager()
    var isGoogleLoginSilently = false
    var googleLoginCompletion = { (result: AnyObject?, error: Error?) -> () in }

    
    func callGoogleSignIn(completionBlock: @escaping(_ result: AnyObject?, _ error: Error?) -> Void)
    {
        googleLoginCompletion = completionBlock
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        if GIDSignIn.sharedInstance().hasAuthInKeychain() == true{
            AppHelper.sharedInstance.displaySpinner()
            isGoogleLoginSilently = true;
            GIDSignIn.sharedInstance().signInSilently()
        }
        else{
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
//            AppHelper.showAlertView(message: error.localizedDescription, isErrorMessage: true)
        } else {
            
            if !isGoogleLoginSilently{
                AppHelper.sharedInstance.displaySpinner()
            }
            
            let userId = user.userID
            let fullName = user.profile.name
            let email = user.profile.email
            
            UserDetails.sharedInstance.emailAdress = email ?? ""
            UserDetails.sharedInstance.name = fullName!
            
            let deviceID = UIDevice.current.identifierForVendor!.uuidString
            var deviceToken: String = UserDetails.sharedInstance.deviceToken
            
            if deviceToken.count == 0 {
                deviceToken = "1111111111";
            }
            
            let parameters: Parameters = ["option": "social","name": UserDetails.sharedInstance.name, "email": UserDetails.sharedInstance.emailAdress, "google_id": userId!, "account_type": "3", "device_type": "1", "device_id": deviceID, "device_token": deviceToken]
            
            callGoogleLoginApi(params: parameters)
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        print(error)
    }
    
    
    
    private func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        
        print(error)
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {

        
        APPDELEGATE.window?.rootViewController?.present(viewController, animated: true, completion: nil)
//        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    private func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        APPDELEGATE.window?.rootViewController?.dismiss(animated: true, completion: nil)
//        self.dismiss(animated: true, completion: nil)
    }
    
    
    func callGoogleLoginApi(params :Parameters)  {
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kSignupURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                weakSelf?.googleLoginCompletion(result as AnyObject,error)
            }
        }
    }    
}
