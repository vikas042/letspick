//
//  RegisterUsingPhoneNumberViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 26/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class RegisterUsingPhoneNumberViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var googleButtonView: UIView!
    @IBOutlet weak var facebookButtonView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var lblSepratorLine: UILabel!
    @IBOutlet weak var lblOr: UILabel!
    
    @IBOutlet weak var loginFieldsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblJoinNow: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblOr.layer.cornerRadius = 16
        lblOr.layer.borderWidth = 1
        lblOr.layer.borderColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 134.0/255, alpha: 1).cgColor
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateGuestToken), name: NSNotification.Name(rawValue: "updateGuestToken"), object: nil)
        if !UserDetails.sharedInstance.isFacebookViewHidden {
            facebookButtonView.isHidden = false
            googleButtonView.isHidden = false
            lblOr.isHidden = false
            lblSepratorLine.isHidden = false
            lblJoinNow.isHidden = false
        }
    }
    
    @objc func updateGuestToken(notification: Notification) {
        if !UserDetails.sharedInstance.isFacebookViewHidden {
            facebookButtonView.isHidden = false
            googleButtonView.isHidden = false
            lblOr.isHidden = false
            lblSepratorLine.isHidden = false
            lblJoinNow.isHidden = false
            loginFieldsHeightConstraint.constant = 340
            view.layoutIfNeeded()
        }
    }
        
    @IBAction func enterPromocodeButtonTapped(_ sender: Any) {
        let enterReferalCodeVC = storyboard?.instantiateViewController(withIdentifier: "LoginWithReferalCodeViewController") as? LoginWithReferalCodeViewController
        enterReferalCodeVC?.phoneNumber = txtFieldPhoneNumber.text ?? ""
        navigationController?.pushViewController(enterReferalCodeVC!, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        nextButton.removebackgroundGrident()
        nextButton.backgroundGrident()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppHelper.resetDefaults()
        if !UserDetails.sharedInstance.isFacebookViewHidden {
            loginFieldsHeightConstraint.constant = 340
            view.layoutIfNeeded()
        }
    }

    @IBAction func termsAndConditionsButtonTapped(_ sender: Any) {
        let privacyPolicyVC = storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
        privacyPolicyVC?.selectedType = SelectedWebViewType.termsAndConditions.rawValue
        navigationController?.pushViewController(privacyPolicyVC!, animated: true)
    }

    @IBAction func loginButtonTapped(_ sender: Any) {
        if txtFieldPhoneNumber.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter mobile number.", isErrorMessage: true)
            return;
        }
        else if txtFieldPhoneNumber.text!.count < 9 {
            AppHelper.showAlertView(message: "Please enter valid mobile number.", isErrorMessage: true)
            return;
        }
        if UserDetails.sharedInstance.guestAccessToken.count == 0{
            AppHelper.getGuestToken()
            return
        }
        
        AppxorEventHandler.logAppEvent(withName: "LoginViaMobileClicked", info: nil)
        callLoginAPI()
    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        
        if UserDetails.sharedInstance.guestAccessToken.count == 0{
            AppHelper.getGuestToken()
            return
        }
        
        AppxorEventHandler.logAppEvent(withName: "SignInWithFacebookClicked", info: nil)
        UserDetails.sharedInstance.isUserComingFromOnBoarding = true;
        weak var weakSelf = self
        
        FacebookLoginManager.sharedInstance.callLoginMangerWithCompletion { (result, error) in
            
            if (result != nil){
                let responseJSON = JSON(result as AnyObject);
                let statusCode = responseJSON["status"]
                
                if statusCode == "200"{
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        UserDetails.sharedInstance.phoneNumber = response["phone"].stringValue
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                        UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                        UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                        UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"
                        
                        let userNameEdited = response["username_edited"].stringValue
                        AppHelper.saveUserDetails()
                        if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                        }
                        else{
                            AppHelper.makeDashbaordAsRootViewController()
                        }

//                        if userNameEdited == "0"{
//                            let enterUserName = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController") as! EnterUserNameViewController
//                            enterUserName.signupMode = "Facebook";
//                            weakSelf?.navigationController?.pushViewController(enterUserName, animated: true)
//                        }
//                        else{
//                            AppHelper.saveUserDetails()
//                            if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
//                            }
//                            else{
//                                AppHelper.makeDashbaordAsRootViewController()
//                            }
//                        }
                    }
                }
                else if statusCode == "650"{
                    
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        let userNameEdited = response["username_edited"].stringValue
                        

                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        let facebookID = response["facebook_id"].string ?? ""
                        let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController") as! EnterPhoneNumberViewController
                        enterPhoneVC.facebookId = facebookID
                        if userNameEdited == "0"{
                            enterPhoneVC.isNeedToUpdateUserName = true
                        }

                        weakSelf?.navigationController?.pushViewController(enterPhoneVC, animated: true)
                    }
                }
                else if statusCode == "801"{
                    AppHelper.getGuestToken()
                }
                else{
                    let errorMsg = result!["message"] as! String
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
    
    @IBAction func googleButtonTapped(_ sender: Any) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        weak var weakSelf = self
        
        if UserDetails.sharedInstance.guestAccessToken.count == 0{
            AppHelper.getGuestToken()
            return
        }
        
        AppxorEventHandler.logAppEvent(withName: "SignInWithGoogleClicked", info: nil)

        UserDetails.sharedInstance.isUserComingFromOnBoarding = true;
        GoogleLoginManager.sharedInstance.callGoogleSignIn{ (result, error) in
            
            if (result != nil){
                let responseJSON = JSON(result as AnyObject);
                let statusCode = responseJSON["status"]
                let errorMsg = responseJSON["message"].string ?? "kErrorMsg".localized()
                
                if statusCode == "200"{
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        UserDetails.sharedInstance.phoneNumber = response["phone"].stringValue
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        
                        UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                        UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                        UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                        UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"
                        
                        let userNameEdited = response["username_edited"].stringValue
                        AppHelper.saveUserDetails()
                        
                        if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                        }
                        else{
                            AppHelper.makeDashbaordAsRootViewController()
                        }

//                        if userNameEdited == "0"{
//                            let enterUserName = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterUserNameViewController") as! EnterUserNameViewController
//                            enterUserName.signupMode = "Google";
//                            weakSelf?.navigationController?.pushViewController(enterUserName, animated: true)
//                        }
//                        else{
//                            AppHelper.saveUserDetails()
//                            
//                            if APPDELEGATE.leagueCode.count > 0{                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
//                            }
//                            else{
//                                AppHelper.makeDashbaordAsRootViewController()
//                            }
//                        }
                    }
                }
                else if statusCode == "650"{
                    
                    if let response = responseJSON.dictionary!["response"]{
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].stringValue
                        UserDetails.sharedInstance.userID = response["user_id"].stringValue
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].stringValue
                        let userNameEdited = response["username_edited"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        let googleId = response["google_id"].string ?? ""

                        let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterPhoneNumberViewController") as! EnterPhoneNumberViewController
                        enterPhoneVC.googleId = googleId
                        if userNameEdited == "0"{
                            enterPhoneVC.isNeedToUpdateUserName = true
                        }
                        
                        weakSelf?.navigationController?.pushViewController(enterPhoneVC, animated: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
    }
        
    func callLoginAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        var deviceToken: String = UserDetails.sharedInstance.deviceToken
        
        if deviceToken.count == 0 {
            deviceToken = "1111111111";
        }

        
        let params = ["option": "signup_otp_v1", "mobile" : txtFieldPhoneNumber.text!, "device_type": "1", "device_id":deviceID, "device_token": deviceToken]
        AppHelper.sharedInstance.displaySpinner()
        UserDetails.sharedInstance.isUserComingFromOnBoarding = true;
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kSignupURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            CleverTapEventDetails.sendEventToCleverTap(eventName: otp_requested, params: [otp_requested_date: Date(), PLATFORM: PLATFORM_iPHONE, mobile_number: self.txtFieldPhoneNumber.text ?? ""])
           
            if (result != nil){
                let statusCode = result!["status"]
                if statusCode == "200"{
                    if let response = result!["response"]{
                        UserDetails.sharedInstance.accessToken = response["access_token"].string!
                        UserDetails.sharedInstance.userID = response["user_id"].string!
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].string!
                        UserDetails.sharedInstance.phoneNumber = response["phone"].string!
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                        UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                        UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                        UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"
                        AppHelper.saveUserDetails()
                        
                        let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as? EnterOTPViewController
                        enterPhoneVC?.isNeedToUpdateUserName = true;
                        enterPhoneVC?.signupMode = "Phone Number"
                        if APPDELEGATE.leagueCode.count > 0{
                            enterPhoneVC?.isNeedToUpdateUserName = true;
                        }
                        weakSelf?.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                    }
                }
                else if statusCode == "205"{
                    let enterOTPVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as? EnterOTPViewController

                    if let response = result!["response"]{
                        let type = response["type"].string
                        if type == "register"{
                            enterOTPVC?.isNewUser = true
                        }
                    }
                    UserDetails.sharedInstance.phoneNumber = weakSelf!.txtFieldPhoneNumber.text!
                    enterOTPVC?.signupMode = "Phone Number"
                    enterOTPVC?.isNeedToUpdateUserName = true; weakSelf?.navigationController?.pushViewController(enterOTPVC!, animated: true)
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!, isErrorMessage: true)
                }
            }
        }
    }
    
    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if string == "" {
            return true;
        }
        if string == " " {
            return false
        }
        
        let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
        if (textField.text?.count)! < 10 {
            return isNumber
        }
        
        return false
    }
}

extension UIButton{
    
    func backgroundGrident() {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor(red: 31.0/255, green: 0.0/255, blue: 104.0/255, alpha: 1).cgColor, UIColor(red: 37.0/255, green: 95.0/255, blue: 176.0/255, alpha: 1).cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.cornerRadius = 5.0
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func removebackgroundGrident() {
         self.layer.sublayers = self.layer.sublayers?.filter { theLayer in
               !theLayer.isKind(of: CAGradientLayer.classForCoder())
         }
     }
}
