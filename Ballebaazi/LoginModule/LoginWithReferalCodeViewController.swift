//
//  LoginWithReferalCodeViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 27/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class LoginWithReferalCodeViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtFieldRedeemCode: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFieldPhoneNumber.text = phoneNumber
        txtFieldRedeemCode.text = UserDetails.sharedInstance.channel
    }
    
    override func viewDidLayoutSubviews() {
        nextButton.removebackgroundGrident()
        nextButton.backgroundGrident()
    }


    @IBAction func getOtpButtonTapped(_ sender: Any) {
        if txtFieldPhoneNumber.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter mobile number.", isErrorMessage: true)
            return;
        }
        else if txtFieldPhoneNumber.text!.count < 9 {
            AppHelper.showAlertView(message: "Please enter valid mobile number.", isErrorMessage: true)
            return;
        }
        if UserDetails.sharedInstance.guestAccessToken.count == 0{
            AppHelper.getGuestToken()
            return
        }
        
        AppxorEventHandler.logAppEvent(withName: "LoginViaMobileClicked", info: nil)
        callLoginAPI()
    }
   
    @IBAction func loginButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func callLoginAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        var deviceToken: String = UserDetails.sharedInstance.deviceToken
        
        if deviceToken.count == 0 {
            deviceToken = "1111111111";
        }

        
        let params = ["option": "signup_otp_v1", "mobile" : txtFieldPhoneNumber.text!, "device_type": "1", "device_id":deviceID, "device_token": deviceToken, "promocode": txtFieldRedeemCode.text ?? ""]
        
        AppHelper.sharedInstance.displaySpinner()
        UserDetails.sharedInstance.isUserComingFromOnBoarding = true;
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kSignupURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            CleverTapEventDetails.sendEventToCleverTap(eventName: otp_requested, params: [otp_requested_date: Date(), PLATFORM: PLATFORM_iPHONE, mobile_number: self.txtFieldPhoneNumber.text ?? ""])
           
            if (result != nil){
                let statusCode = result!["status"]
                if statusCode == "200"{
                    if let response = result!["response"]{
                        UserDetails.sharedInstance.accessToken = response["access_token"].string!
                        UserDetails.sharedInstance.userID = response["user_id"].string!
                        UserDetails.sharedInstance.emailAdress = response["email"].string ?? ""
                        UserDetails.sharedInstance.apiSecretKey = response["api_secret_key"].string!
                        UserDetails.sharedInstance.phoneNumber = response["phone"].string!
                        UserDetails.sharedInstance.name = response["name"].stringValue
                        UserDetails.sharedInstance.userName = response["username"].stringValue
                        UserDetails.sharedInstance.dob = response["dob"].string ?? ""
                        UserDetails.sharedInstance.address = response["address"].stringValue
                        UserDetails.sharedInstance.referralAmount = response["referral_amount"].stringValue
                        UserDetails.sharedInstance.bonusCash = response["bonus_cash"].stringValue
                        
                        UserDetails.sharedInstance.signupBonus = response["signup_bonus"].stringValue
                        UserDetails.sharedInstance.totalCredits = response["total_cash"].stringValue
                        UserDetails.sharedInstance.unusedCash = response["unused_amount"].stringValue
                        UserDetails.sharedInstance.registeredDate = response["registered_date"].string ?? "N/A"
                        UserDetails.sharedInstance.state = response["state"].string ?? "N/A"
                        UserDetails.sharedInstance.city = response["city"].string ?? "N/A"
                        UserDetails.sharedInstance.referralCode = response["referral_code"].string ?? "N/A"
                        AppHelper.saveUserDetails()
                        
                        let enterPhoneVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as? EnterOTPViewController
                        enterPhoneVC?.isNeedToUpdateUserName = true;
                        enterPhoneVC?.signupMode = "Phone Number"
                        if APPDELEGATE.leagueCode.count > 0{
                            enterPhoneVC?.isNeedToUpdateUserName = true;
                        }
                            weakSelf?.navigationController?.pushViewController(enterPhoneVC!, animated: true)
                    }
                }
                else if statusCode == "403"{
                                                        
                    let message = result!["message"]?.string ?? ""

                    let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "CHANGE NUMBER".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
                        self.txtFieldPhoneNumber.text = ""
                        
                    }))
                    alert.addAction(UIAlertAction(title: "CONTINUE LOGIN".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
                        self.txtFieldRedeemCode.text = ""
                        weakSelf?.callLoginAPI()
                    }))
                    self.navigationController?.present(alert, animated: true, completion: nil)
                }
                else if statusCode == "205"{
                    let enterOTPVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController") as? EnterOTPViewController
                    UserDetails.sharedInstance.phoneNumber = weakSelf!.txtFieldPhoneNumber.text!
                    enterOTPVC?.signupMode = "Phone Number"
                    enterOTPVC?.isNeedToUpdateUserName = true;
                    weakSelf?.navigationController?.pushViewController(enterOTPVC!, animated: true)
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!, isErrorMessage: true)
                }
            }
        }
    }
    
    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == txtFieldPhoneNumber {
            if string == "" {
                return true;
            }
            if string == " " {
                return false
            }
            
            let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
            if (textField.text?.count)! < 10 {
                return isNumber
            }
            return false
        }
        else{
            return true
        }
    }

}
