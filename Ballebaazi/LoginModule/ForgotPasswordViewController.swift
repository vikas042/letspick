//
//  ForgotPasswordViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/30/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire


class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: LoginCustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false;
        AppHelper.addBackButtonOnNavigationbar(title: "  Forgot Password", target: self, isNeedToShowShadow: false)
    }

    @IBAction func submitButtonTapped(_ sender: Any) {
        if emailTextField.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter your email ID", isErrorMessage: true)
            return
        }
        else if AppHelper.isNotValidEmail(email: emailTextField.text!){
            AppHelper.showAlertView(message: "Please enter valid email ID", isErrorMessage: true)
            return
        }
        
        let params: Parameters = ["option": "send", "email": emailTextField.text!]
        callForgotPasswordAPI(params: params)
    }
    
    func callForgotPasswordAPI(params: Parameters)  {
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        weak var weakSelf = self

        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(urlString: kForgotPasswordURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if (result != nil){
                let message = result!["message"]?.string

                let statusCode = result!["status"]
                if statusCode == "200"{
                    let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
                        weakSelf!.navigationController?.popViewController(animated: true)
                    }))
                    weakSelf!.present(alert, animated: true, completion: nil)
                }
                else{
                    AppHelper.showAlertView(message: message!, isErrorMessage: true)
                }
            }
        }
    }
    
    
    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if string == " " {
            return false
        }
        return true
    }
    
    
    // MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
