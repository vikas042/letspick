//
//  EnterUserNameViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/10/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire

class EnterUserNameViewController: UIViewController {

    @IBOutlet weak var lblChooseUserName: UILabel!
    @IBOutlet weak var checkboxButton: UIButton!
    @IBOutlet weak var txtFiledPromocode: LoginCustomTextField!
    var signupMode = "Phone Number"
    var selectedLanguage = "en"
    
    @IBOutlet weak var hindiSelectedImgView: UIImageView!
    @IBOutlet weak var englishSelectedImgView: UIImageView!
    @IBOutlet weak var hindiLanguegeView: UIView!
    @IBOutlet weak var englishLanguagueView: UIView!
    @IBOutlet weak var lblHindi: UILabel!
    @IBOutlet weak var lblEnglish: UILabel!
    @IBOutlet weak var hindiSelectBgImgView: UIImageView!
    @IBOutlet weak var englishSelectBgImgView: UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: "isUserOnUserName")
        UserDefaults.standard.synchronize()
        checkboxButton.isSelected = true
        englishLanguagueView.layer.borderColor = UIColor(red: 198.0/255, green: 198.0/255, blue: 198.0/255, alpha: 1).cgColor
        englishLanguagueView.layer.borderWidth = 1
        
        hindiLanguegeView.layer.borderColor = UIColor(red: 198.0/255, green: 198.0/255, blue: 198.0/255, alpha: 1).cgColor
        hindiLanguegeView.layer.borderWidth = 1
        englishButtonTapped(nil)

        
        if (APPDELEGATE.invitationCode.count > 0) && (APPDELEGATE.invitationCode != kNoValue){
            txtFiledPromocode.text = APPDELEGATE.invitationCode
        }
    }
    
    @IBAction func privacyPolicyButtonTapped(_ sender: Any) {
        let privacyPolicyVC = storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
        privacyPolicyVC?.selectedType = SelectedWebViewType.PrivacyPolicy.rawValue
        navigationController?.pushViewController(privacyPolicyVC!, animated: true)
    }
    
    @IBAction func englishButtonTapped(_ sender: Any?) {
        hindiSelectedImgView.isHidden = true
        hindiLanguegeView.backgroundColor = UIColor.white
        englishLanguagueView.backgroundColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)
        lblEnglish.textColor = UIColor.white;
        lblHindi.textColor = UIColor(red: 198.0/255, green: 198.0/255, blue: 198.0/255, alpha: 1);
        englishSelectedImgView.isHidden = false
        englishLanguagueView.layer.borderWidth = 0
        hindiLanguegeView.layer.borderWidth = 1
        
        englishSelectBgImgView.image = UIImage(named: "UnselectedLanguague")
        hindiSelectBgImgView.image = UIImage(named: "SelectedLanguague")
        
        selectedLanguage = "en"
    }
    
    @IBAction func hindiButtonTapped(_ sender: Any?) {
        englishSelectedImgView.isHidden = true
        hindiSelectedImgView.isHidden = false
        hindiLanguegeView.backgroundColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)
        englishLanguagueView.backgroundColor = UIColor.white
        
        lblHindi.textColor = UIColor.white;
        lblEnglish.textColor = UIColor(red: 198.0/255, green: 198.0/255, blue: 198.0/255, alpha: 1);
        
        selectedLanguage = "hi"
        hindiLanguegeView.layer.borderWidth = 0
        englishLanguagueView.layer.borderWidth = 1
        
        hindiSelectBgImgView.image = UIImage(named: "UnselectedLanguague")
        englishSelectBgImgView.image = UIImage(named: "SelectedLanguague")
        
    }
    
    @IBAction func termsAndConditaionsButtonTapped(_ sender: Any) {
        let privacyPolicyVC = storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
        privacyPolicyVC?.selectedType = SelectedWebViewType.termsAndConditions.rawValue
        navigationController?.pushViewController(privacyPolicyVC!, animated: true)
    }
    
    @IBAction func checkBoxButtonTapped(_ sender: Any) {
        return
        if checkboxButton.isSelected{
            checkboxButton.isSelected = false
        }
        else{
            checkboxButton.isSelected = true
        }
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if !checkboxButton.isSelected {
            AppHelper.showAlertView(message: "Please accept our Terms & Conditions to proceed", isErrorMessage: true)
            return;
        }

        var chanelName = UserDetails.sharedInstance.channel
        
        if chanelName.count == 0 {
            chanelName = kDirect
        }
        
        if selectedLanguage.count != 0 {
            let arr = NSArray(objects: selectedLanguage)
            UserDefaults.standard.set(arr, forKey: kAppLanguague)
        }

        AppxorEventHandler.logAppEvent(withName: "SIgnUpNextButtonClicked", info: nil)

        let params:Parameters = ["option": "update_username_v1","username": UserDetails.sharedInstance.userName, "user_id": UserDetails.sharedInstance.userID, "promocode": txtFiledPromocode.text ?? "", "channel": chanelName]
        callUpdateUserName(params: params)
    }    
    
    func callUpdateUserName(params: Parameters) {
       
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if (result != nil){
                let statusCode = result!["status"]
                let errorMsg = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200"{
                    
                    APPDELEGATE.isNewUserRegister = true;

                    MixPanelEventsDetails.intialiseMixpanel()
                    MixPanelEventsDetails.setupSuperProperties()
                    MixPanelEventsDetails.setPeopleProperties(isFromEdit: false)
                    MixPanelEventsDetails.loginBranchIdentity()
                    MixPanelEventsDetails.updateBranchEvent(registrationMode: self.signupMode)

                    if let response = result!["response"]?.dictionary {
                        
                        if (response["code_benifits"]?.dictionary) != nil{
                            let popupView = RedeemCodePopupView(frame: APPDELEGATE.window!.frame)
                            popupView.isNewUser = true
                            let codeDetails = RedeemCodeDetails.parseRedeemCodeDetails(details: response["code_benifits"]!)
                            codeDetails.redeemCode = self.txtFiledPromocode.text ?? ""
                            popupView.updateData(details: codeDetails)
                            APPDELEGATE.window!.addSubview(popupView)
                        }
                        else{
                            if (APPDELEGATE.leagueCode.count > 0) && (APPDELEGATE.leagueCode != kNoValue){                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                            }
                            else{
                                AppHelper.makeDashbaordAsRootViewController()
                            }
                        }
                    }
                    else{
                        if (APPDELEGATE.leagueCode.count > 0) && (APPDELEGATE.leagueCode != kNoValue){                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: APPDELEGATE.leagueCode)
                        }
                        else{
                            AppHelper.makeDashbaordAsRootViewController()
                        }
                    }
                    
                    
                    var emailAddress = UserDetails.sharedInstance.emailAdress
                    if emailAddress.count == 0{
                        emailAddress = kNoValue
                    }
                    
                    var name = UserDetails.sharedInstance.name
                    if name.count == 0{
                        name = kNoValue
                    }
                    
                    if APPDELEGATE.invitationCode.count == 0{
                        APPDELEGATE.invitationCode = kNoValue
                    }
                    
                    if APPDELEGATE.leagueCode.count == 0{
                        APPDELEGATE.leagueCode = kNoValue
                    }
                                        
                    var chanelName = UserDetails.sharedInstance.channel
                    
                    if chanelName.count == 0 {
                        chanelName = kDirect
                    }
                    
                    var params = [String: Any]()
                    params[verification] = "Yes"
                    params[sign_up_code] = APPDELEGATE.invitationCode
                    params[signup_channel] = chanelName
                    params[PLATFORM] = PLATFORM_iPHONE
                    params[onBoarding_medium] = PLATFORM_iPHONE
                    params[user_name] = UserDetails.sharedInstance.userName
                    params[custome_name] = UserDetails.sharedInstance.name
                    params[mode] = self.signupMode
                    params[registration_date] = AppHelper.getUserRegistrationDate(dateString: UserDetails.sharedInstance.registeredDate)
                    CleverTapEventDetails.sendEventToCleverTap(eventName: sign_up, params: params)
                    CleverTapEventDetails.updateProfile(updateProfile: [signup_channel: chanelName])
                }
                else{
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
        }
        
    }

}
