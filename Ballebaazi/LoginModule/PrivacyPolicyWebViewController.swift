//
//  PrivacyPolicyWebViewController.swift
//  Letspick
//
//  Created by Mads Technologies on 27/06/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import WebKit

enum SelectedWebViewType: Int {
    
    case AboutUs = 100
    case ContactUs
    case HowToPlay
    case PointSysytem
    case FAQ
    case PrivacyPolicy
    case Promotions
    case termsAndConditions
    case Winners
    case Floating
    case WithdrawalPolicies
    case ReferralPolicies
    case partnershipProgram
    case Blog
    case RewardProgram
    case Quiz

}


class PrivacyPolicyWebViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var containerView: UIView!
    var privacyPolicyWebivew: WKWebView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    var htmlString = ""
    
    var selectedType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
        let wkWebConfig = WKWebViewConfiguration()
        wkWebConfig.userContentController = wkUController
        privacyPolicyWebivew = WKWebView(frame: CGRect(x: 0, y: 6, width: containerView.frame.width, height: UIScreen.main.bounds.height - 169), configuration: wkWebConfig)
        containerView.addSubview(privacyPolicyWebivew)
        privacyPolicyWebivew.backgroundColor = UIColor.clear
        privacyPolicyWebivew.isOpaque = false
        navigationController?.navigationBar.isHidden = true;
        privacyPolicyWebivew.scrollView.showsVerticalScrollIndicator = false
        privacyPolicyWebivew.scrollView.showsHorizontalScrollIndicator = false

        AppHelper.showShodowOnCellsView(innerView: containerView)
        containerView.clipsToBounds = true
        var webPageBaseurl = "https://Letspick.com/lang/"

        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                webPageBaseurl = webPageBaseurl + "hi/"
            }
            else{
                webPageBaseurl = webPageBaseurl + "en/"
            }
        }
        else{
            webPageBaseurl = webPageBaseurl + "en/"
        }
        
        if selectedType == SelectedWebViewType.AboutUs.rawValue {
            lblTitle.text = "About Us".localized()
            getHtmlContentAPI(type: "1")
        }
        else if selectedType == SelectedWebViewType.ContactUs.rawValue {
            DispatchQueue.main.async {
                AppHelper.sharedInstance.displaySpinner()
            }
            lblTitle.text = "Contact Us".localized()
            let baseUrl = webPageBaseurl + "contact.html"
            let request = URLRequest(url: URL(string: baseUrl)!)
            privacyPolicyWebivew.load(request)
        }
        else if selectedType == SelectedWebViewType.Blog.rawValue {
            DispatchQueue.main.async {
                AppHelper.sharedInstance.displaySpinner()
            }
            lblTitle.text = "Blog".localized()
            let request = URLRequest(url: URL(string: "https://www.Letspick.com/blog")!)
            privacyPolicyWebivew.load(request)
        }

        else if selectedType == SelectedWebViewType.HowToPlay.rawValue {
            DispatchQueue.main.async {
                AppHelper.sharedInstance.displaySpinner()
            }
            let title = "How To Play".localized()
            lblTitle.text = title
            let baseUrl = webPageBaseurl + "howtoplay.html"
            let request = URLRequest(url: URL(string: baseUrl)!)
            privacyPolicyWebivew.load(request)
        }
        else if selectedType == SelectedWebViewType.termsAndConditions.rawValue {
            lblTitle.text = "Terms & Conditions".localized()
            getHtmlContentAPI(type: "3")
        }
        else if selectedType == SelectedWebViewType.PointSysytem.rawValue {
            DispatchQueue.main.async {
                AppHelper.sharedInstance.displaySpinner()
            }
            lblTitle.text = "Point System".localized()
            let baseUrl = webPageBaseurl + "pointssystem.html"
            let request = URLRequest(url: URL(string: baseUrl)!)
            privacyPolicyWebivew.load(request)
        }
        else if selectedType == SelectedWebViewType.FAQ.rawValue {
            lblTitle.text = "FAQ".localized()
            getHtmlContentAPI(type: "4")
        }
        else if selectedType == SelectedWebViewType.ReferralPolicies.rawValue {
            lblTitle.text = "Referral Policies".localized()
            getHtmlContentAPI(type: "8")
        }
        else if selectedType == SelectedWebViewType.PrivacyPolicy.rawValue{
            lblTitle.text = "Privacy Policy & Legalities".localized()
            getHtmlContentAPI(type: "2")
        }
        else if selectedType == SelectedWebViewType.partnershipProgram.rawValue{
            lblTitle.text = "Partnership Program".localized()
            getHtmlContentAPI(type: "11")
        }
        else if selectedType == SelectedWebViewType.RewardProgram.rawValue{
            lblTitle.text = "How Rewards Work?".localized()
            getHtmlContentAPI(type: "12")
        }
        else if selectedType == SelectedWebViewType.Promotions.rawValue {
            lblTitle.text = "Promotions".localized()
            let baseUrl = webPageBaseurl + "promotions.html"
            let request = URLRequest(url: URL(string: baseUrl)!)
            privacyPolicyWebivew.load(request)
        }
        else if selectedType == SelectedWebViewType.Winners.rawValue {
            lblTitle.text = "Winners".localized()

            AppHelper.sharedInstance.displaySpinner()
            let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
            let options = [NSAttributedString.DocumentReadingOptionKey.documentType:
                NSAttributedString.DocumentType.html]
            let attributedString = try? NSMutableAttributedString(data: htmlData ?? Data(), options: options, documentAttributes: nil)
            let dataStr = attributedString?.string ?? ""
            privacyPolicyWebivew.loadHTMLString(dataStr, baseURL: nil)
        }
        else if selectedType == SelectedWebViewType.Floating.rawValue {
            lblTitle.text = "Leaderboard Info".localized()
            getHtmlContentAPI(type: "6")
        }
        else if selectedType == SelectedWebViewType.WithdrawalPolicies.rawValue{
            lblTitle.text = "Withdrawal Policies".localized()
            getHtmlContentAPI(type: "7")
        }
        else if selectedType == SelectedWebViewType.Quiz.rawValue{
            lblTitle.text = "Quiz Policies".localized()
            getHtmlContentAPI(type: "13")
        }
        
        privacyPolicyWebivew.navigationDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getHtmlContentAPI(type: String) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        var selectedLanguage = "en"
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                selectedLanguage = "hi"
            }
        }
        AppHelper.sharedInstance.displaySpinner()
        let params = ["option": "acknowledgement", "type": type, "lang": selectedLanguage]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kSupport, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                
                if let response = result!["response"]?.dictionary{
                    if let htmlString = response["body"]?.string{
                        weakSelf?.privacyPolicyWebivew.loadHTMLString( htmlString, baseURL: nil)
                    }
                    else{
                        AppHelper.sharedInstance.removeSpinner()
                    }
                }
                else{
                    AppHelper.sharedInstance.removeSpinner()
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        AppHelper.sharedInstance.removeSpinner()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        AppHelper.sharedInstance.removeSpinner()
    }

//    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
//        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
//        webView.evaluateJavaScript(jscript)
//    }


}
