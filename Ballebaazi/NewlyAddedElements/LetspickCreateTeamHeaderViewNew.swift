//
//  LetspickNewCreateTeamHeaderView.swift
//  Letspick
//
//  Created by anurag singh on 25/06/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit


class LetspickCreateTeamHeaderViewNew: UIView {
    /*
    @IBOutlet weak var lblCaptainTitle: UILabel!
    
    @IBOutlet weak var lblDoublePoint: UILabel!
    @IBOutlet weak var lblViceCaptain: UILabel!
    @IBOutlet weak var lblViceCaptainPoints: UILabel!
    
    @IBOutlet weak var lblVs: UILabel!
    @IBOutlet weak var lblSelectCaptains: UILabel!
    */
    
    @IBOutlet weak var secondTeamImgView: UIImageView!
    @IBOutlet weak var firstTeamImgView: UIImageView!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblSecondTeamName: UILabel!
    @IBOutlet weak var lblFirstTeamName: UILabel!
    @IBOutlet weak var lblSelectedPlayerCount: UILabel!
    @IBOutlet weak var lblCreditLeft: UILabel!
    
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var announcement: AnnouncementView!
    
   // @IBOutlet weak var totalPlayerCount: UILabel!
    /*
    @IBOutlet weak var lblSelectedTitle: UILabel!
    @IBOutlet weak var lblCreditLeftitle: UILabel!
    */
    @IBOutlet weak var lblFirstTeamPlayerCount: UILabel!
    @IBOutlet weak var lblSecondTeamPlayerCount: UILabel!
    /*
    @IBOutlet weak var createTeamView: UIView!
    
    @IBOutlet weak var selectCaptainView: UIView!
    
    @IBOutlet weak var watchIcon: UIImageView!
    */
    
    var totalPlayerCount = "11"
    var isViewForSelectCaptain = false
    var isSegmentLoaded = false
    var progress: KDCircularProgress!
    /*
    @IBOutlet weak var lblViceCaptainName: UILabel!
    @IBOutlet weak var lblCaptainName: UILabel!
    */
    @IBOutlet weak var creditView: UIView!

   // var arcCircularView: ArcaProgressView!
    var duration: TimeInterval!
  
    @IBOutlet weak var segmentedProgressView: UIView!
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setupCircularProgress(){
        progress = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        progress.angle = 360
        progress.startAngle = -90
        progress.progressThickness = 0.4
        progress.trackThickness = 0.4
        progress.clockwise = true
        progress.gradientRotateSpeed = 2
        progress.roundedCorners = false
        progress.glowMode = .forward
        progress.glowAmount = 0.9
        progress.trackColor = UIColor.white
        progress.set(colors: UIColor.green)
       // progress.center = CGPoint(x: creditView.center.x, y: creditView.center.y)
        creditView.addSubview(progress)
        
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("LetspickCreateTeamHeaderViewNew", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            lblFirstTeamPlayerCount.layer.cornerRadius = 11
            lblSecondTeamPlayerCount.layer.cornerRadius = 11
            lblFirstTeamPlayerCount.clipsToBounds = true
            lblSecondTeamPlayerCount.clipsToBounds = true
            lblFirstTeamPlayerCount.layer.borderColor = UIColor.systemYellow.cgColor
            lblFirstTeamPlayerCount.layer.borderWidth = 1
            lblSecondTeamPlayerCount.layer.borderColor = UIColor.systemYellow.cgColor
            lblSecondTeamPlayerCount.layer.borderWidth = 1
            //addCircularView()
            setupCircularProgress()
            isSegmentLoaded = true
           // segmentedCircular()
           // circularView.progress = 0//CGFloat(UserDetails.sharedInstance.maxCreditLimitForClassic)
           
            /*
             lblSelectedTitle.text = "Selected".localized()
             lblCreditLeftitle.text = "CreditLeft".localized()
             
             lblCaptainTitle.text = "Captain".localized()
             lblViceCaptain.text = "subCaptain".localized()
             lblDoublePoint.text = "2x" + "Point".localized()
             lblViceCaptainPoints.text = "1.5x" + "Point".localized()
             lblSelectCaptains.text = "SelectedCaptain".localized()
             */
            addSubview(view)
        }
    }
    
    func segmentedCircular(){
        let circlePath = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: segmentedProgressView.frame.width, height: segmentedProgressView.frame.height))
        let overLayPath = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: segmentedProgressView.frame.width, height: segmentedProgressView.frame.height))
        
        var segments: [CAShapeLayer] = []
        //let segmentAngle: CGFloat = (360 * 0.125) / 360 // For 8
        let segmentAngle: CGFloat =  (360 * (CGFloat(360.0/Double(totalPlayerCount)!/360.0))) / 360
     //   let segmentAngle: CGFloat = (360 * 0.090909090909091) / 360 // For 11
        
        var playerNumber = UserDetails.sharedInstance.selectedPlayerList.count
        
        for  i in 0..<Int(totalPlayerCount)! {
            let circleLayer = CAShapeLayer()
            circleLayer.path = overLayPath.cgPath

            // start angle is number of segments * the segment angle
            circleLayer.strokeStart = segmentAngle * CGFloat(i)

            // end angle is the start plus one segment, minus a little to make a gap
            // you'll have to play with this value to get it to look right at the size you need
            let gapSize: CGFloat = 0.008
            circleLayer.strokeEnd = circleLayer.strokeStart + segmentAngle - gapSize

            circleLayer.lineWidth = 8
            circleLayer.strokeColor = UIColor.white.cgColor
            circleLayer.fillColor = UIColor.clear.cgColor

            // add the segment to the segments array and to the view
            segments.insert(circleLayer, at: i)
            circleLayer.frame = segmentedProgressView.bounds
            segmentedProgressView.layer.addSublayer(segments[i])
        }

        for  i in 0..<playerNumber {
            let circleLayer = CAShapeLayer()
            circleLayer.path = circlePath.cgPath

            // start angle is number of segments * the segment angle
            circleLayer.strokeStart = segmentAngle * CGFloat(i)

            // end angle is the start plus one segment, minus a little to make a gap
            // you'll have to play with this value to get it to look right at the size you need
            let gapSize: CGFloat = 0.008
            circleLayer.strokeEnd = circleLayer.strokeStart + segmentAngle - gapSize

            circleLayer.lineWidth = 8
            circleLayer.strokeColor = UIColor.green.cgColor
            circleLayer.fillColor = UIColor.clear.cgColor

            // add the segment to the segments array and to the view
            segments.insert(circleLayer, at: i)
            circleLayer.frame = segmentedProgressView.bounds
            segmentedProgressView.layer.addSublayer(segments[i])
        }
    }
    
  
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.popViewController(animated: true)
        }
    }
    
    // MARK:- Timer Handlers
    
    func updateMatchName(matchDetails: MatchDetails?) {
        
        if isViewForSelectCaptain{
         //   createTeamView.isHidden = true
        //    selectCaptainView.isHidden = false
        }
        else{
         //   createTeamView.isHidden = false
         //   selectCaptainView.isHidden = true
        }
        
        guard let details = matchDetails else {
            lblTimer.text = ""
            lblFirstTeamName.text = ""
            lblSecondTeamName.text = ""
            return;
        }
        
        lblFirstTeamName.text = details.firstTeamShortName
        lblSecondTeamName.text = details.secondTeamShortName
        
        if details.isMatchClosed  {
            lblTimer.text = "Leagues Closed"
        }
        else{
            lblTimer.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: details.startDateTimestemp)
        }
        
    }
    
    func updateData(details: MatchDetails?) {
        lblFirstTeamName.text = details?.firstTeamShortName
        lblSecondTeamName.text = details?.secondTeamShortName
    }
    
    func updateTimerValue(matchDetails: MatchDetails)  {
        
        if matchDetails.isMatchClosed  {
            lblTimer.text = "Leagues Closed"
        }
        else if UInt64((matchDetails.startDateTimestemp)!) != nil{
            lblTimer.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: matchDetails.startDateTimestemp)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails.startDateTimestemp!)
            if remainingTime <= 0{
                AppHelper.sharedInstance.removeSpinner()
                AppHelper.showMatchCLosedAlertAndPopToRootViewController()
                matchDetails.isMatchClosed = true
            }
        }
    }
    
    func setupDefaultPropertiesForCaptainVicecaptaion() {
        /*
        lblCaptainName.text = "--"
        lblViceCaptainName.text = "--"
        
        watchIcon.isHidden = true
        lblVs.isHidden = true;
        lblSelectCaptains.isHidden = false
        
        totalPlayerCount.isHidden = true
        */
        lblFirstTeamPlayerCount.isHidden = true
        lblSecondTeamPlayerCount.isHidden = true
        firstTeamImgView.isHidden = true
        secondTeamImgView.isHidden = true
        lblFirstTeamName.isHidden = true
        lblSecondTeamName.isHidden = true
        lblTimer.isHidden = true
        /*
        selectCaptainView.isHidden = false
        createTeamView.isHidden = true
        */
    }
    
    func setupDefaultProperties(matchDetails: MatchDetails?, fantasyType: String, gameType: Int) {
        
        // lblCaptainName.text = "--"
        // lblViceCaptainName.text = "--"
         
         if isViewForSelectCaptain {
       //  selectCaptainView.isHidden = false
      //   createTeamView.isHidden = true
         }
         else{
      //   selectCaptainView.isHidden = true
       //  createTeamView.isHidden = false
         }
         
         if gameType == GameType.Cricket.rawValue {
         if fantasyType == "1" {
         totalPlayerCount = "11"
         }
         else if fantasyType == "2" {
         totalPlayerCount = "5"
         }
         else if fantasyType == "3" {
         totalPlayerCount = "5"
         }
         }
         else if gameType == GameType.Kabaddi.rawValue {
         totalPlayerCount = "7"
         }
         else if gameType == GameType.Football.rawValue {
         totalPlayerCount = "11"
         }
         else if gameType == GameType.Basketball.rawValue {
         totalPlayerCount = "8"
         }
         else if gameType == GameType.Baseball.rawValue {
          totalPlayerCount = "9"
        }
        
        segmentedCircular()
        
        let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        lblFirstTeamPlayerCount.text = String(totalPlayerFromTeamArray.count)
        lblSecondTeamPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count - totalPlayerFromTeamArray.count)
        
        
        if let url = NSURL(string: matchDetails!.firstTeamImageUrl){
            firstTeamImgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.firstTeamImgView.image = image
                }
                else{
                    self?.firstTeamImgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.firstTeamImgView.image = UIImage(named: "Placeholder")
        }
        
        if let url = NSURL(string: matchDetails!.secondTeamImageUrl){
            secondTeamImgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.secondTeamImgView.image = image
                }
                else{
                    self?.secondTeamImgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.secondTeamImgView.image = UIImage(named: "Placeholder")
        }
        
        lblFirstTeamName.text = matchDetails!.firstTeamShortName
        lblSecondTeamName.text = matchDetails!.secondTeamShortName
        
    }
    
    func updatePlayerSelectionData(matchDetails: MatchDetails, fantasyType: String)  {
        
        let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == matchDetails.firstTeamShortName
        }
        
        lblFirstTeamName.text = matchDetails.firstTeamShortName
        lblSecondTeamName.text = matchDetails.secondTeamShortName
        lblFirstTeamPlayerCount.text = String(totalPlayerFromTeamArray.count)
        lblSecondTeamPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count - totalPlayerFromTeamArray.count)
        
        let totalPoints = UserDetails.sharedInstance.selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            
            creditSum + Float(nextPlayerDetails.credits)!
        })
        
        if fantasyType == "1" {
            
             lblSelectedPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count) + "/" + totalPlayerCount
             let remaingCredit = Float(UserDetails.sharedInstance.maxCreditLimitForClassic) - Float(totalPoints)
             lblCreditLeft.text = String(remaingCredit)
           // circularView.setProgressWithAnimation(duration: 1, value: remaingCredit)
            progress.angle = Double(remaingCredit) * 360/100
            segmentedCircular()
             
        }
        else if fantasyType == "2" {
            lblSelectedPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count) + "/" + totalPlayerCount
            let remaingCredit = Float(UserDetails.sharedInstance.maxCreditLimitForBatting) - Float(totalPoints)
            lblCreditLeft.text = String(remaingCredit)
            segmentedCircular()
        }
        else{
            lblSelectedPlayerCount.text = String(UserDetails.sharedInstance.selectedPlayerList.count) + "/" + totalPlayerCount
            let remaingCredit = Float(UserDetails.sharedInstance.maxCreditLimitForBowling) - Float(totalPoints)
            lblCreditLeft.text = String(remaingCredit)
            segmentedCircular()
        }
        
    }
    
    func updateCaptainAndViceCaptainName(captain: PlayerDetails?, viceCaptain: PlayerDetails?) {
        /*
        if captain != nil {
            lblCaptainName.text = captain?.playerName
        }
        else{
            lblCaptainName.text = "--"
        }
        
        if viceCaptain != nil {
            lblViceCaptainName.text = viceCaptain?.playerName
        }
        else{
            lblViceCaptainName.text = "--"
        }
        */
    }
    
    
}

fileprivate extension UIImage {
    static func gradientImage(with bounds: CGRect,
                            colors: [CGColor],
                            locations: [NSNumber]?) -> UIImage? {

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = colors
        // This makes it horizontal
        gradientLayer.startPoint = CGPoint(x: 0.0,
                                        y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0,
                                        y: 0.5)

        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return image
    }
}
