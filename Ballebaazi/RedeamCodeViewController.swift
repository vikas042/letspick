//
//  RedeamCodeViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 06/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class RedeamCodeViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var submittButton: UIButton!
    @IBOutlet weak var txtFieldRedeemCode: UITextField!
    @IBOutlet weak var txtFieldContainerView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblEnterRedeemCode: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFieldRedeemCode.delegate = self
        lblTitle.text = "Redeem Code".localized()
        lblMessage.text = "RedeemCodeMsg".localized()
        lblEnterRedeemCode.text = "Enter Redeem Code".localized()
        txtFieldContainerView.layer.cornerRadius = 4.0
        txtFieldContainerView.layer.borderWidth = 1.0
        txtFieldContainerView.layer.borderColor = UIColor(red: 127.0/255.0, green: 130.0/255.0, blue: 132.0/255.0, alpha: 1).cgColor
        submittButton.setTitle("Submit".localized(), for: .normal)
        AppHelper.showShodowOnCellsView(innerView: containerView)
        submittButton.backgroundColor = UIColor(red: 0/255, green: 136.0/255, blue: 64.0/255, alpha: 1)

    }
    
    @IBAction func submittedButtonnTapped(_ sender: Any) {

        if txtFieldRedeemCode.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter redeem code", isErrorMessage: true)
            return;
        }
        else if txtFieldRedeemCode.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter redeem code", isErrorMessage: true)
            return;
        }

        
        callGetRedeemCodeAPI(redeemCode: txtFieldRedeemCode.text!)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if (textField.text!.count > 9) && (string != ""){
//            return false
//        }
        
        return true
    }
    
    
    func callGetRedeemCodeAPI(redeemCode: String) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        txtFieldRedeemCode.resignFirstResponder()
        AppHelper.sharedInstance.displaySpinner()
        let params = ["option": "campaign_code", "code": redeemCode, "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let status = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()

                if status == "200"{
                    if (result!["response"]?.dictionary) != nil {
                        let popupView = RedeemCodePopupView(frame: APPDELEGATE.window!.frame)
                        let codeDetails = RedeemCodeDetails.parseRedeemCodeDetails(details: result!["response"]!)
                        codeDetails.redeemCode = weakSelf?.txtFieldRedeemCode.text ?? ""
                        popupView.updateData(details: codeDetails)
                        APPDELEGATE.window!.addSubview(popupView)
                        weakSelf?.txtFieldRedeemCode.text = ""
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
}

