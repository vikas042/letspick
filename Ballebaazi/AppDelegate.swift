//
//  AppDelegate.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/28/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import CoreData
import FBSDKLoginKit
import GoogleSignIn
import Alamofire
import IQKeyboardManagerSwift
import UserNotifications
import Fabric
import Crashlytics
import Branch
import CleverTapSDK
import AVFoundation
import HyperSDK
import MapleBacon

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var invitationCode = ""
    var leagueCode = ""
    lazy var isNewUserRegister = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        

        Settings.isAutoLogAppEventsEnabled = true
        Settings.isAutoInitEnabled = true
        ApplicationDelegate.initializeSDK(nil)
        AppEvents.activateApp()
        Branch.getInstance().delayInitToCheckForSearchAds()
       
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }
        catch {
            // report for an error
        }

        UIApplication.shared.applicationIconBadgeNumber = 0;
        Branch.setUseTestBranchKey(false)
        AppHelper.getUserDetails()
        weak var weakSelf = self
        CleverTap.autoIntegrate()
        CleverTap.sharedInstance()?.enableDeviceNetworkInfoReporting(true)

        Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
            if let dict = params as? [String: AnyObject]{
                let clickedBranchLink = (dict["+clicked_branch_link"] as? Bool) ?? false
                
                if  clickedBranchLink {
                    if let completeLink = dict["~referring_link"] as? String{
                        let linkComponentArray = completeLink.components(separatedBy: "refer_code=")
                        if linkComponentArray.count > 1{
                            UserDefaults.standard.setValue("User Referral", forKey: "marketing_title")
                            UserDefaults.standard.synchronize()
                            UserDetails.sharedInstance.channel = "User Referral"
                            let leagueCode = linkComponentArray.last
                            let tempArray = leagueCode?.components(separatedBy: "&")
                            
                            if tempArray?.count ?? 0 > 1{
                                weakSelf?.invitationCode = tempArray?.first ?? "";
                            }
                            else{
                                weakSelf?.invitationCode = leagueCode ?? "";
                            }
                        }else{
                            
                            var marketingTitle = ""
                                
                            if let title = dict["$marketing_title"]  as? String{
                                marketingTitle = title
                            }
                            if marketingTitle.count == 0 {
                                if let title = dict["$link_title"]  as? String{
                                    marketingTitle = title
                                }
                            }
                            
                            if marketingTitle.count == 0 {
                                marketingTitle = kDirect
                            }
                            
                                
                            UserDefaults.standard.setValue(marketingTitle, forKey: "marketing_title")
                            UserDefaults.standard.synchronize()
                            UserDetails.sharedInstance.channel = marketingTitle
                        }
                    }
                    else{

                        var marketingTitle = ""
                            
                        if let title = dict["$marketing_title"]  as? String{
                            marketingTitle = title
                        }
                        if marketingTitle.count == 0 {
                            if let title = dict["$link_title"]  as? String{
                                marketingTitle = title
                            }
                        }
                        
                        if marketingTitle.count == 0 {
                            marketingTitle = kDirect
                        }

                        UserDefaults.standard.setValue(marketingTitle, forKey: "marketing_title")
                        UserDefaults.standard.synchronize()
                        UserDetails.sharedInstance.channel = marketingTitle
                    }
                }
                
                if let leagueCode = dict["league_code"] as? String{
                    let userID = UserDetails.sharedInstance.userID
                    if userID.count == 0 {
                        weakSelf!.leagueCode = leagueCode;
                        UserDefaults.standard.setValue("User Referral", forKey: "marketing_title")
                        UserDefaults.standard.synchronize()
                        UserDetails.sharedInstance.channel = "User Referral"
                        
                    }else if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                        if let joinLeagueVC = navVC.viewControllers.last as? JoinPrivateLeagueViewController{
                            joinLeagueVC.leagueCode = leagueCode
                            joinLeagueVC.updateLeagueCode()
                            UserDefaults.standard.setValue(kPrivateLeague, forKey: "marketing_title")
                            UserDefaults.standard.synchronize()
                            UserDetails.sharedInstance.channel = kPrivateLeague
                        }
                    }
                }
            }
        }
        Branch.getInstance().setRequestMetadataKey("$clevertap_attribution_id", value:CleverTap.sharedInstance()?.profileGetAttributionIdentifier());

        IQKeyboardManager.shared.enable = true
        Fabric.with([Crashlytics.self])
        GIDSignIn.sharedInstance().clientID = "971369828514-6j0u5tqbeebv1a1ive6efj6kg3c40fi2.apps.googleusercontent.com"
        
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        if !isUserLoggedIn {
            AppHelper.getGuestToken()
            
            if (launchOptions != nil){
                if let activityDic = launchOptions?[UIApplicationLaunchOptionsKey.userActivityDictionary] as? [String : AnyObject]{
                    
                    let key = "UIApplicationLaunchOptionsUserActivityKey"
                    let userActivity = activityDic[key] as? NSUserActivity
                    Branch.getInstance().continue(userActivity)

                    if let completeLink = userActivity?.webpageURL{
                        let linkComponentArray = completeLink.absoluteString.components(separatedBy: "league_code=")
                        if linkComponentArray.count > 1{
                            let leagueCode = linkComponentArray.last
                            weakSelf?.leagueCode = leagueCode ?? "";
                            
                            UserDefaults.standard.setValue(kPrivateLeague, forKey: "marketing_title")
                            UserDefaults.standard.synchronize()
                            UserDetails.sharedInstance.channel = kPrivateLeague

                        }
                        else{
                            let linkComponentArray = completeLink.absoluteString.components(separatedBy: "refer_code=")
                            if linkComponentArray.count > 1{
                                let leagueCode = linkComponentArray.last
                                weakSelf?.invitationCode = leagueCode ?? "";
                                
                                UserDefaults.standard.setValue("User Referral", forKey: "marketing_title")
                                UserDefaults.standard.synchronize()
                                UserDetails.sharedInstance.channel = "User Referral"
                            }
                        }
                    }
                }
            }
            
            let isUserOnUserName = UserDefaults.standard.bool(forKey: "isUserOnUserName")
            if isUserOnUserName{
                AppHelper.getUserDetails()
                AppHelper.makeUserNameAsRootViewController()
            }
            else{
                //AppHelper.makeLoginAsRootViewController()
                AppHelper.callLoginViewController()
            }
        }else{

            if (launchOptions != nil){
                if let activityDic = launchOptions?[UIApplicationLaunchOptionsKey.userActivityDictionary] as? [String : AnyObject]{
                   
                    let key = "UIApplicationLaunchOptionsUserActivityKey"
                    let userActivity = activityDic[key] as? NSUserActivity
                    if let completeLink = userActivity?.webpageURL{
                        let linkComponentArray = completeLink.absoluteString.components(separatedBy: "league_code=")
                        if linkComponentArray.count > 1{
                            UserDefaults.standard.setValue(kPrivateLeague, forKey: "marketing_title")
                            UserDefaults.standard.synchronize()
                            UserDetails.sharedInstance.channel = kPrivateLeague
                            
                            let leagueCode = linkComponentArray.last
                            AppHelper.makeJoinPrivateLeagueViewController(leagueCode: leagueCode)
                        }
                        else{
                            UserDefaults.standard.setValue("User Referral", forKey: "marketing_title")
                            UserDefaults.standard.synchronize()
                            UserDetails.sharedInstance.channel = "User Referral"
                            
                            let linkComponentArray = completeLink.absoluteString.components(separatedBy: "refer_code=")
                            if linkComponentArray.count > 1{
                                let invitationCode = linkComponentArray.last
                                weakSelf?.invitationCode = invitationCode ?? "";
                            }
                        }
                    }
                }
                else if let userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [String : AnyObject]{
                    
                    if let userInfoDict = userInfo["data"] as? NSDictionary {
                        if let params = userInfoDict["params"] as? NSDictionary {
                            
                            let type = params["push_type"]
                            if type != nil{
                                let notificationType = String(format: "%@", type as! CVarArg)
                               
                                if notificationType == "3"{
                                    let matchKey = params["push_match"] as? String ?? ""
                                    let seasonKey = params["push_season"] as? String ?? ""
                                    openScreenAccordingToNotification(notificationType: notificationType, matchKey: matchKey, seasonKey: seasonKey, gametype: "")
                                }
                                else{
                                    openScreenAccordingToNotification(notificationType: notificationType, matchKey: "", seasonKey: "", gametype: "")
                                }
                            }
                        }
                    }
                }
                else{
                    AppHelper.makeDashbaordAsRootViewController()
               
                }
            }
            else{
                AppHelper.makeDashbaordAsRootViewController()
              
                
            }
        }        
           
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.badge, .alert , .sound]) { (granted, error) in
            if granted {
                DispatchQueue.main.async {                
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        center.delegate = self

        return ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        CleverTap.sharedInstance()?.setPushToken(deviceToken)
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("DEVICE TOKEN: ", deviceTokenString)
        UserDetails.sharedInstance.deviceToken = deviceTokenString
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo)
        
        CleverTap.sharedInstance()?.isCleverTapNotification(userInfo)
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let userInfoDict = userInfo["data"] as? NSDictionary {
            if let params = userInfoDict["params"] as? NSDictionary {

                let type = params["push_type"]

                if type != nil{
                    let notificationType = String(format: "%@", type as! CVarArg)
                    
                    if (UIApplication.shared.applicationState == UIApplicationState.inactive) || (UIApplication.shared.applicationState == UIApplicationState.background) || (UIApplication.shared.applicationState == UIApplicationState.active) {
                        let playType = params["sport_type"] as? String ?? ""

                        if notificationType == "0" {
                            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                if let dashboardVC = navigationVC.viewControllers.first as? DashboardViewController{
                                    
                                    if playType == "1" {
                                        dashboardVC.headerView.cricketButtonTapped(nil)
                                    }
                                    else if playType == "2" {
                                        dashboardVC.headerView.kabaddiButtonTapped(nil)
                                    }
                                    else if playType == "3" {
                                        dashboardVC.headerView.footballButtonTapped(nil)
                                    }
                                    else if playType == "4" {
                                        dashboardVC.headerView.quizButtonTapped()
                                    }
                                    else if playType == "5" {
                                        dashboardVC.headerView.basketballButtonTapped()
                                    }
                                    else if playType == "6" {
                                        dashboardVC.headerView.baseballButtonTapped()
                                    }
                                    else{
                                        dashboardVC.headerView.cricketButtonTapped(nil)
                                    }
                                }
                            }
                        }
                        else if notificationType == "1" {
                            let promotionsVC = storyboard.instantiateViewController(withIdentifier: "PromotionsViewController") as? PromotionsViewController
                            promotionsVC?.isFromSetting = true
                            promotionsVC!.isFromNotification = true
                            
                            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                navigationVC.pushViewController(promotionsVC!, animated: true)
                            }
                            else{
                                let navVC = UINavigationController.init(rootViewController: promotionsVC!)
                                navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
                                navVC.navigationBar.isTranslucent = false;
                                APPDELEGATE.window?.rootViewController = navVC;
                            }
                        }
                        else if notificationType == "2" {
                            let promotionsVC = storyboard.instantiateViewController(withIdentifier: "LeaderboardViewController") as? LeaderboardViewController
                            promotionsVC!.isFromNotification = true
                            
                            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                navigationVC.pushViewController(promotionsVC!, animated: true)
                            }
                            else{
                                let navVC = UINavigationController.init(rootViewController: promotionsVC!)
                                navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
                                navVC.navigationBar.isTranslucent = false;
                                APPDELEGATE.window?.rootViewController = navVC;
                            }                        }
                        else if notificationType == "3" {
                            if playType == "1" {
                                let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as? LeagueViewController
                                leagueVC!.isFormNotification = true
                                leagueVC?.matchKey = params["push_match"] as? String ?? ""
                                leagueVC?.seasonKey = params["push_season"] as? String ?? ""
                                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    navigationVC.pushViewController(leagueVC!, animated: true)
                                }
                            }
                            else if playType == "2" {
                                let leagueVC = storyboard.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as? KabaddiLeagueViewController
                                leagueVC!.isFormNotification = true
                                leagueVC?.matchKey = params["push_match"] as? String ?? ""
                                leagueVC?.seasonKey = params["push_season"] as? String ?? ""
                                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    navigationVC.pushViewController(leagueVC!, animated: true)
                                }

                            }
                            else if playType == "3" {
                                
                                let leagueVC = storyboard.instantiateViewController(withIdentifier: "FootballLeagueViewController") as? FootballLeagueViewController
                                leagueVC!.isFormNotification = true
                                leagueVC?.matchKey = params["push_match"] as? String ?? ""
                                leagueVC?.seasonKey = params["push_season"] as? String ?? ""
                                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    navigationVC.pushViewController(leagueVC!, animated: true)
                                }

                            }
                            else if playType == "4" {
                                
//                                let leagueVC = storyboard.instantiateViewController(withIdentifier: "QuizLeaguesViewController") as? QuizLeaguesViewController
//                                leagueVC!.isFormNotification = true
//                                leagueVC?.matchKey = params["push_match"] as? String ?? ""
//                                leagueVC?.seasonKey = params["push_season"] as? String ?? ""
//                                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                    navigationVC.pushViewController(leagueVC!, animated: true)
//                                }
                            }
                            else if playType == "5" {
                                
                                let leagueVC = storyboard.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as? BasketballLeagueViewController
                                leagueVC!.isFormNotification = true
                                leagueVC?.matchKey = params["push_match"] as? String ?? ""
                                leagueVC?.seasonKey = params["push_season"] as? String ?? ""
                                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    navigationVC.pushViewController(leagueVC!, animated: true)
                                }

                            }
                            else if playType == "6" {
                                
                                let leagueVC = storyboard.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as? BasketballLeagueViewController
                                leagueVC!.isFormNotification = true
                                leagueVC?.matchKey = params["push_match"] as? String ?? ""
                                leagueVC?.seasonKey = params["push_season"] as? String ?? ""
                                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    navigationVC.pushViewController(leagueVC!, animated: true)
                                }
                            }
                            else{
                                
                                let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as? LeagueViewController
                                leagueVC!.isFormNotification = true
                                leagueVC?.matchKey = params["push_match"] as? String ?? ""
                                leagueVC?.seasonKey = params["push_season"] as? String ?? ""
                                if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    navigationVC.pushViewController(leagueVC!, animated: true)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func openScreenAccordingToNotification(notificationType: String, matchKey: String, seasonKey: String, gametype: String) {
        if notificationType == "0" {
            // HOME Screen
            AppHelper.makeDashbaordAsRootViewController()
        }
        else if notificationType == "1" {
            AppHelper.makePromotionViewControllerAsRootViewController()
        }
//        else if notificationType == "2" {
//            AppHelper.makeDashbaordAsRootViewControllerWithPreSelectedLeaderboardTab()
//        }
        else if notificationType == "3" {
            AppHelper.makeLeagueViewControllerAsRootViewController(matchKey: matchKey, seasonKey: seasonKey)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applicationFoesIntoBackground"), object: nil)

        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)
        UserDetails.sharedInstance.isLiveServerUpdate = false;
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
//    // MARK:- Social Login Related Methods
//    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
//        let handled = ApplicationDelegate.shared.application(app, open: url, options: options)
//      
//      return handled
//    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation){
            return true
        }
        else if url.absoluteString.contains("com.googleusercontent.apps"){
            return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        }
        
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        weak var weakSelf = self

        if let completeLink = userActivity.webpageURL{
            let linkComponentArray = completeLink.absoluteString.components(separatedBy: "league_code=")
            if linkComponentArray.count > 1{
                let leagueCode = linkComponentArray.last

                let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
                UserDefaults.standard.setValue(kPrivateLeague, forKey: "marketing_title")
                UserDefaults.standard.synchronize()
                UserDetails.sharedInstance.channel = kPrivateLeague

                if !isUserLoggedIn {
                    weakSelf?.leagueCode = leagueCode ?? ""
                }
                else{
                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let joinPrivateLagueVC = storyboard.instantiateViewController(withIdentifier: "JoinPrivateLeagueViewController") as! JoinPrivateLeagueViewController
                        joinPrivateLagueVC.leagueCode = leagueCode
                        navVC.pushViewController(joinPrivateLagueVC, animated: true)
                    }
                    else{
                        AppHelper.makeJoinPrivateLeagueViewController(leagueCode: leagueCode)
                    }
                }
            }
            else{
                let linkComponentArray = completeLink.absoluteString.components(separatedBy: "refer_code=")
                if linkComponentArray.count > 1{
                    UserDefaults.standard.setValue("User Referral", forKey: "marketing_title")
                    UserDefaults.standard.synchronize()
                    UserDetails.sharedInstance.channel = "User Referral"
                    let leagueCode = linkComponentArray.last
                    let tempArray = leagueCode?.components(separatedBy: "&")
                    if tempArray?.count ?? 0 > 1{
                        weakSelf?.invitationCode = tempArray?.first ?? "";
                    }
                    else{
                        weakSelf?.invitationCode = leagueCode ?? "";
                    }
                }
            }
        }
        
        Branch.getInstance().continue(userActivity)
        return true
    }

    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print(error)        
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Ballebaazi")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } else {

            if managedObjectContext.hasChanges {
                do {
                    try managedObjectContext.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                    abort()
                }
            }
        }
    }

    
    
    // iOS 9 and below
    lazy var applicationDocumentsDirectory: URL = {
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "Ballebaazi", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Ballebaazi")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    
}
