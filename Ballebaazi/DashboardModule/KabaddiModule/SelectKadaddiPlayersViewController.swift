//
//  SelectKadaddiPlayersViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 31/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class SelectKadaddiPlayersViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var headerView: LetspickCreateTeamHeaderViewNew!
    @IBOutlet weak var sliderLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var pointsButton: UIButton!
    @IBOutlet weak var creditButton: UIButton!
    @IBOutlet weak var defenderIcon: UIImageView!
    @IBOutlet weak var raiderIcon: UIImageView!
    @IBOutlet weak var allRounderIcon: UIImageView!
    @IBOutlet weak var secondTeamButton: UIButton!
    @IBOutlet weak var firstTeamButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var previewButton: CustomBorderButton!
    @IBOutlet weak var lblAllRounderTitle: UILabel!
    @IBOutlet weak var lblRaiderTitle: UILabel!
    @IBOutlet weak var lblDefenderTitle: UILabel!
    @IBOutlet weak var nextButton: SolidButton!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var lblDefenderCount: UILabel!
    @IBOutlet weak var lblAllRounderCount: UILabel!
    @IBOutlet weak var lblRaiderCount: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottonView: UIView!
    @IBOutlet weak var lblPickMessage: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var inOutButton: UIButton!

    lazy var selectedPlayerType = PlayerType.Defender.rawValue
    var totalPlayerArray: Array<PlayerDetails>?
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var userTeamDetails: UserTeamDetails?
    var timer: Timer?
    lazy var isEditPlayerTeam = false
    lazy var isPlaying22 = ""
    var ticketDetails: TicketDetails?
    
    lazy var totalDefenderArray = Array<PlayerDetails>()
    lazy var totalAllRounderArray = Array<PlayerDetails>()
    lazy var totalRaiderArray = Array<PlayerDetails>()
    
    lazy var playerSortedType = false
    lazy var teamSortedType = false
    lazy var pointsSortedType = true
    lazy var creditsSortedType = true
    lazy var selectedLeagueType = 0
    lazy var isComeFromEditPlayerTeamScreen = false
    lazy var isPlayerPlaying = false
    var isMatchClosingTimeRefereshing = false

    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.isViewForSelectCaptain = false
        setupDefaultProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails?.fantasyType ?? "1", gameType: GameType.Kabaddi.rawValue)
        self.defenderButtonTapped(nil)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerListContainerCollectionViewCell", for: indexPath) as? PlayerListContainerCollectionViewCell
        var playerList: Array<PlayerDetails>?
        
        if indexPath.row == 0 {
            playerList = totalDefenderArray
        }
        else if indexPath.row == 1 {
            playerList = totalAllRounderArray
        }
        else if indexPath.row == 2 {
            playerList = totalRaiderArray
        }
        
        cell?.configData(playerList: playerList!, leagueType: FantasyType.Classic.rawValue, gameType: GameType.Kabaddi.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        
        return cell!;
    }
    
    // MARK:- API Related Method
    
    func callGetPlayerListAPI(urlString: String, isNeedToShowLoader: Bool)  {
        
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        
        weak var waekSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
                    DispatchQueue.main.async {
                        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
                        let response = savedResponse?.dictionary!["response"]
                        if let flagDict = response?.dictionary!["team_flags"]{
                            UserDetails.sharedInstance.teamFlag = flagDict;
                        }
                        
                        if let selectedmatch = response!.dictionary!["selected_match"]?.dictionary{
                            waekSelf?.isPlaying22 = selectedmatch["show_playing22"]?.string ?? "0"
                            if waekSelf?.isPlaying22 != "1"{
                                waekSelf?.inOutButton.isHidden = true
                            }
                            else{
                                waekSelf?.inOutButton.isHidden = false
                            }
                            
                            if waekSelf?.matchDetails?.firstTeamShortName?.count == 0{
                                let teamFirstName = selectedmatch["team_a_short_name"]?.string ?? ""
                                let teamSecondShortName = selectedmatch["team_b_short_name"]?.string ?? ""
                                if let firstTeamImageName = selectedmatch["team_a_flag"]?.string{
                                    waekSelf?.matchDetails?.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                                }
                                   
                                if let secondTeamImageName = selectedmatch["team_b_flag"]?.string{
                                    waekSelf?.matchDetails?.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                                }

                                waekSelf?.matchDetails?.firstTeamShortName = teamFirstName
                                waekSelf?.matchDetails?.secondTeamShortName = teamSecondShortName
                                waekSelf?.firstTeamButton.setTitle(teamFirstName, for: .normal)
                                waekSelf?.secondTeamButton.setTitle(teamSecondShortName, for: .normal)
                            }

                        }
                        
                        let playerList = response?.dictionary!["match_players"]?.array
                        waekSelf?.totalPlayerArray = PlayerDetails.getPlayerDetailsArray(responseArray: playerList!, matchDetails: waekSelf?.matchDetails)
                        
                        if waekSelf?.matchDetails?.playersGender == "F"{
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: waekSelf!.totalPlayerArray!)
                        }
                        if waekSelf?.matchDetails?.isMatchTourney ?? false{
                            waekSelf?.allButton.isHidden = true
                            waekSelf?.firstTeamButton.isHidden = true
                            waekSelf?.secondTeamButton.isHidden = true
                        }
                        waekSelf?.modifySelectedPlayerDetails()
                        waekSelf?.updateTimerVlaue()
                        
                        waekSelf?.totalDefenderArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 0)
                        waekSelf?.totalRaiderArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 2)
                        waekSelf?.totalAllRounderArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 1)
                        waekSelf?.collectionView.reloadData()
                        waekSelf?.sortPlayerBasedOnLineupsOut()
                        waekSelf?.upperView.isHidden = false
                        waekSelf?.bottonView.isHidden = false
                        waekSelf?.collectionView.isHidden = false
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    //MARK:- Custom Methods
    
    func setupDefaultProperties() {
        
        
        lblAllRounderTitle.text = "ALR".localized()
        lblDefenderTitle.text = "DEF".localized()
        lblRaiderTitle.text = "RAI".localized()
        
        previewButton.setTitle("TeamPreview".localized(), for: .normal)
        nextButton.setTitle("Next".localized(), for: .normal)
        previewButton.setTitle("TeamPreview".localized(), for: .selected)
        nextButton.setTitle("Next".localized(), for: .selected)
        pointsButton.setTitle("Points".localized(), for: .normal)
        pointsButton.setTitle("Points".localized(), for: .selected)
        
        creditButton.setTitle("Credits".localized(), for: .normal)
        creditButton.setTitle("Credits".localized(), for: .selected)
        
        
        lblDefenderCount.text = "0"
        lblAllRounderCount.text = "0"
        lblRaiderCount.text = "0"
        upperView.isHidden = true
        bottonView.isHidden = true
        collectionView.isHidden = true
        isPlayerPlaying = true

        if matchDetails?.isMatchTourney ?? false{
            allButton.isHidden = true
            firstTeamButton.isHidden = true
            secondTeamButton.isHidden = true
        }

        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        
        firstTeamButton.setTitle(matchDetails!.firstTeamShortName, for: .normal)
        secondTeamButton.setTitle(matchDetails!.secondTeamShortName, for: .normal)
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails?.fantasyType ?? "1", gameType: GameType.Kabaddi.rawValue)
        
        AppHelper.designBottomTabDesing(bottonView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerSelectionUpdateNotification), name: NSNotification.Name(rawValue: "PlayerSelectionUpdateNotification"), object: nil)
        
        collectionView.register(UINib(nibName: "PlayerListContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayerListContainerCollectionViewCell")
        
        let urlString = kKabaddiMatchURL + "?option=match_players&match_key=" + matchDetails!.matchKey
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.callGetPlayerListAPI(urlString: urlString, isNeedToShowLoader: true)
        }
        
        playerSelectionUpdateNotification()
        
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        for selectedDetails in UserDetails.sharedInstance.selectedPlayerList{
            for details in totalPlayerArray!{
                
                if details.playerKey == selectedDetails.playerKey{
                    details.isSelected = selectedDetails.isSelected
                    selectedDetails.playerPlayingRole = details.playerPlayingRole
                    selectedDetails.bowlingPoints = details.bowlingPoints
                    selectedDetails.isPlayerPlaying = details.isPlayerPlaying
                    selectedDetails.battingPoints = details.battingPoints
                    selectedDetails.classicPoints = details.classicPoints
                    selectedDetails.reversePoints = details.reversePoints
                    selectedDetails.wizardPoints = details.wizardPoints

                    selectedDetails.teamShortName = details.teamShortName
                    break;
                }
            }
        }
        
        if selectedRow == 0{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Raider.rawValue
            })
        }
        if playersArray != nil{
            return playersArray!
        }
        return []
    }
    
    @objc func playerSelectionUpdateNotification()  {
        
        headerView.updatePlayerSelectionData(matchDetails: matchDetails!, fantasyType: leagueDetails?.fantasyType ?? "1")
        if UserDetails.sharedInstance.selectedPlayerList.count == 7 {
            nextButton.isEnabled = true
            nextButton.updateLayerProperties()
        }
        else{
            nextButton.isEnabled = false
            nextButton.updateLayerProperties()
        }
        getSelectedPlayerCounts()
    }
    
    
    func getSelectedPlayerCounts(){
        
        let totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        if totalPlayerArray.count == 1 {
            AppxorEventHandler.logAppEvent(withName: "AddPlayer", info: ["SportType": "Kabaddi"])
        }
        
        let defenderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
        })

        let allRounderPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
        })
        
        let raiderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Raider.rawValue
        })
        
        lblDefenderCount.text = "(" + String(defenderPlayersArray.count) + ")"
        lblAllRounderCount.text = "(" + String(allRounderPlayerArray.count) + ")"
        lblRaiderCount.text = "(" + String(raiderPlayersArray.count) + ")"
    }
    
    func modifySelectedPlayerDetails()  {
        
        if isEditPlayerTeam{
            for selectedPlayermDetails in UserDetails.sharedInstance.selectedPlayerList{
                for playermDetails in totalPlayerArray!{
                    if selectedPlayermDetails.playerKey == playermDetails.playerKey{
                        
                        selectedPlayermDetails.bowlingPoints = playermDetails.bowlingPoints
                        selectedPlayermDetails.battingPoints = playermDetails.battingPoints
                        selectedPlayermDetails.classicPoints = playermDetails.classicPoints
                        selectedPlayermDetails.reversePoints = playermDetails.reversePoints
                        selectedPlayermDetails.wizardPoints = playermDetails.wizardPoints

                        selectedPlayermDetails.teamShortName = playermDetails.teamShortName
                        break
                    }
                }
            }
        }
        playerSelectionUpdateNotification()
    }
    
    //MARK:- -IBAction Methods
    @IBAction func defenderButtonTapped(_ sender: Any?) {

        showDefenderSelected()
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func allRounderButtonTapped(_ sender: Any?) {
        
        showAllRounderSelected()
        let indexPath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func raiderButtonTapped(_ sender: Any?) {
        showRaiderSelected()
        let indexPath = IndexPath(item: 2, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    func showDefenderSelected()  {
        lblPickMessage.text = "Pick 2-4 Defender".localized()
        selectedPlayerType = PlayerType.Defender.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Kabaddi", "Type": "DEF"])

        defenderIcon.image = UIImage(named: "defenderIconSelected")
        allRounderIcon.image = UIImage(named: "AllRounderKabaddiIcon")
        raiderIcon.image = UIImage(named: "RaiderIcon")
        
        lblDefenderTitle.textColor = kPlayerTypeSelectedColor
        lblRaiderTitle.textColor = kPlayerTypeUnselectedColor
        lblAllRounderTitle.textColor = kPlayerTypeUnselectedColor
        lblDefenderCount.textColor = kPlayerTypeSelectedColor
        lblRaiderCount.textColor = kPlayerTypeUnselectedColor
        lblAllRounderCount.textColor = kPlayerTypeUnselectedColor
        
        sliderLeadingConstraint.constant = 0.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    func showAllRounderSelected()  {
    
        lblPickMessage.text = "Pick 1-2 All Rounders".localized()
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Kabaddi", "Type": "ALR"])

        selectedPlayerType = PlayerType.AllRounder.rawValue
        allRounderIcon.layer.borderColor = kPlayerSelectionMaxLimitColor.cgColor
        
        lblDefenderTitle.textColor = kPlayerTypeUnselectedColor
        lblRaiderTitle.textColor = kPlayerTypeUnselectedColor
        lblAllRounderTitle.textColor = kPlayerTypeSelectedColor
        lblDefenderCount.textColor = kPlayerTypeUnselectedColor
        lblRaiderCount.textColor = kPlayerTypeUnselectedColor
        lblAllRounderCount.textColor = kPlayerTypeSelectedColor
        
        
        defenderIcon.image = UIImage(named: "defenderIcon")
        allRounderIcon.image = UIImage(named: "AllRounderKabaddiIconSelected")
        raiderIcon.image = UIImage(named: "RaiderIcon")
        
        
        sliderLeadingConstraint.constant = UIScreen.main.bounds.width/3
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }

    func showRaiderSelected()  {
        
        lblPickMessage.text = "Pick 1-3 Raiders".localized()
        selectedPlayerType = PlayerType.Raider.rawValue
        lblDefenderTitle.textColor = kPlayerTypeUnselectedColor
        lblRaiderTitle.textColor = kPlayerTypeUnselectedColor
        lblAllRounderTitle.textColor = kPlayerTypeUnselectedColor
        lblDefenderCount.textColor = kPlayerTypeUnselectedColor
        lblRaiderCount.textColor = kPlayerTypeUnselectedColor
        lblAllRounderCount.textColor = kPlayerTypeUnselectedColor
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Kabaddi", "Type": "RAI"])

        defenderIcon.image = UIImage(named: "defenderIcon")
        allRounderIcon.image = UIImage(named: "AllRounderKabaddiIcon")
        raiderIcon.image = UIImage(named: "RaiderIconSelected")
        
        sliderLeadingConstraint.constant = UIScreen.main.bounds.width*2/3
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    @IBAction func teamPreviewButtonTapped(_ sender: Any) {
        
        if UserDetails.sharedInstance.selectedPlayerList.count == 0 {
            AppHelper.showAlertView(message: "Please select atleast one player", isErrorMessage: true)
            return
        }
        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Kabaddi"])

        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "KabaddiTeamPreviewViewController") as! KabaddiTeamPreviewViewController
        teamPreviewVC.totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        teamPreviewVC.isShowPlayingRole = true;
        teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
        teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    @IBAction func creditButtonTapped(_ sender: Any){
        
        let defenderPlayerListArray = totalDefenderArray
        let allRounderPlayerListArray = totalAllRounderArray
        let raiderPlayerListArray = totalRaiderArray
        
        let sortedDefenderArray =  defenderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
                
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedAllrounderArray =  allRounderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedRaiderArray =  raiderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        
        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        if creditsSortedType {
            creditButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }
        else{
            creditButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }
        
        creditsSortedType = !creditsSortedType
        totalDefenderArray = sortedDefenderArray
        totalRaiderArray = sortedRaiderArray
        totalAllRounderArray = sortedAllrounderArray
        
        collectionView.reloadData()
    }
    
    @IBAction func pointsButtonTapped(_ sender: Any){
        
        let defenderPlayerListArray = totalDefenderArray
        let allRounderPlayerListArray = totalAllRounderArray
        let raiderPlayerListArray = totalRaiderArray
        
        let sortedDefenderArray =  defenderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
                
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
        })
        
        let sortedAllrounderArray =  allRounderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
        })
        
        let sortedRaiderArray =  raiderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
        })
        
        
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        if pointsSortedType {
            pointsButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }
        else{
            pointsButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }
        
        creditsSortedType = !creditsSortedType
        totalDefenderArray = sortedDefenderArray
        totalRaiderArray = sortedRaiderArray
        totalAllRounderArray = sortedAllrounderArray
        
        collectionView.reloadData()
    }
    
    @IBAction func allTeamButtonTapped(_ sender: Any?) {
        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        totalDefenderArray = getSelectedTypePlayerList(selectedRow: 0)
        totalAllRounderArray = getSelectedTypePlayerList(selectedRow: 1)
        totalRaiderArray = getSelectedTypePlayerList(selectedRow: 2)
        collectionView.reloadData()
    }
    
    @IBAction func firstTeamButtonTapped(_ sender: Any) {
        
        firstTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        
        let defenderPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let allRounderPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let raiderPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        
        let filteredDefenderArray = defenderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredAllRounderArray = allRounderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredRaiderArray = raiderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        
        totalDefenderArray = filteredDefenderArray
        totalAllRounderArray = filteredAllRounderArray
        totalRaiderArray = filteredRaiderArray
        collectionView.reloadData()
        sortPlayerBasedOnLineupsOut()
    }
    
    @IBAction func secondTeamButtonTapped(_ sender: Any) {
        
        secondTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        
        let defenderPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let allRounderPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let raiderListArray = getSelectedTypePlayerList(selectedRow: 2)
        
        
        let filteredDefenderArray = defenderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredAllRounderArray = allRounderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredRiderArray = raiderListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        totalDefenderArray = filteredDefenderArray
        totalAllRounderArray = filteredAllRounderArray
        totalRaiderArray = filteredRiderArray
        collectionView.reloadData()
        sortPlayerBasedOnLineupsOut()
    }
    
    @IBAction func inOutButtonTapped(_ sender: Any?) {
//        isPlayerPlaying = !isPlayerPlaying
        sortPlayerBasedOnLineupsOut()
    }

    func sortPlayerBasedOnLineupsOut() {
//        if isPlayerPlaying{
//            inOutButton.setTitleColor(UIColor(red: 60.0/255, green: 196.0/255, blue: 266.0/255, alpha: 1), for: .normal)
//        }
//        else{
//            inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
//        }
        
        if isPlaying22 != "1"{
            allTeamButtonTapped(nil)
            return
        }

        inOutButton.setTitleColor(UIColor(red: 60.0/255, green: 196.0/255, blue: 266.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        let defenderPlayerListArray = totalDefenderArray
        let allRounderPlayerListArray = totalAllRounderArray
        let raiderPlayerListArray = totalRaiderArray
        
        let sortedDefenderArray = defenderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }
        
        let sortedAllrounderArray = allRounderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }

        let sortedRaiderArray = raiderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }

        
        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        totalDefenderArray = sortedDefenderArray
        totalRaiderArray = sortedRaiderArray
        totalAllRounderArray = sortedAllrounderArray
        
        collectionView.reloadData()
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "NextButtonClicked", info: ["SportType": "Kabaddi"])

        if UserDetails.sharedInstance.selectedPlayerList.count < 7{
            AppHelper.showAlertView(message: "Please select 7 players", isErrorMessage: true)
            return;
        }
        
        let selectCaptionVC = storyboard?.instantiateViewController(withIdentifier: "SelectKabaddiCaptionsViewController") as! SelectKabaddiCaptionsViewController
        selectCaptionVC.matchDetails = matchDetails
        selectCaptionVC.leagueDetails = leagueDetails
        selectCaptionVC.isEditPlayerTeam = isEditPlayerTeam
        selectCaptionVC.userTeamDetails = userTeamDetails
        selectCaptionVC.ticketDetails = ticketDetails

        navigationController?.pushViewController(selectCaptionVC, animated: true);
    }
    
    // MARK: - Timer Handler
    @objc func updateTimerVlaue()  {
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kKabaddiMatchURL, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Scroll View Delegates
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if collectionView == scrollView{
            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
            if Int(currentPage) == 0{
                showDefenderSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 1{
                showAllRounderSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 2{
                showRaiderSelected()
                collectionView.reloadData()
            }
        }
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
