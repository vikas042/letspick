//
//  KabaddiTeamPreviewViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 31/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class KabaddiTeamPreviewViewController: UIViewController {

    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var defenderView: UIView!
    @IBOutlet weak var raiderView: UIView!
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var totalPointValue: UILabel!
    @IBOutlet weak var lblTotalPointTitle: UILabel!
    @IBOutlet weak var allRounderView: UIView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var containerView: UIView!
//    @IBOutlet weak var bottomSepView: UIView!

    @IBOutlet weak var lblRaiders: UILabel!
    @IBOutlet weak var lblDefenders: UILabel!
    
    @IBOutlet weak var lblAllRounders: UILabel!
    var totalPlayerArray = Array<PlayerDetails>()
    var isMatchClosed = false
    var isShowPlayingRole = false
    var firstTeamName = ""
    var secondTeamName = ""
    var teamNumber = ""
    var userName = ""
    var isHideEditButton = false
    
    @IBOutlet weak var lblTeamNameTwo: UILabel!
    @IBOutlet weak var lblTeamNameOne: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTeamNameOne.text = firstTeamName
        lblTeamNameTwo.text = secondTeamName

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isHideEditButton {
            editButton.isHidden = true
        }
        if isMatchClosed {
            lblTotalPointTitle.text = "Total Points".localized()
            editButton.isHidden = true
            pointsView.isHidden = false
        }
        else{
            lblTotalPointTitle.text = "Credit Points".localized()
            pointsView.isHidden = true
        }
        
        if teamNumber.count > 0{
            lblTeamName.text = "Team".localized() + " " + teamNumber
            if userName.count > 0{
                lblUserName.text = userName
            }
            else{
                lblUserName.text = UserDetails.sharedInstance.userName
            }
        }
        else{
            lblTeamName.text = "TeamPreview".localized()
        }

        showPlayerList()
    }

    func showPlayerList() {

        let defenderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
        })
        
        let allRounderPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
        })
        
        let raiderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Raider.rawValue
        })

        let selectedFantasy = FantasyType.Classic.rawValue
        
        lblRaiders.text = "RAIDER".localized()
        if raiderPlayersArray.count > 1 {
            lblRaiders.text = "RAIDERS".localized()
        }
        
        lblDefenders.text = "DEFENDER".localized()
        if defenderPlayersArray.count > 1 {
            lblDefenders.text = "DEFENDERS".localized()
        }
        
        lblAllRounders.text = "ALL ROUNDER".localized()
        if allRounderPlayerArray.count > 1 {
            lblAllRounders.text = "ALL ROUNDERS".localized()
        }

        let viewWidth = UIScreen.main.bounds.width/5 - 9;
        var totalPoint: Float = 0
        
        if defenderPlayersArray.count == 0{
            defenderView.isHidden = true
        }
        else if defenderPlayersArray.count == 1{
            let playerInfoView = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            let deatails = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            defenderView.addSubview(playerInfoView)
        }
        else if defenderPlayersArray.count == 2{
            
            let playerInfoView1 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            defenderView.addSubview(playerInfoView1)
            defenderView.addSubview(playerInfoView2)
        }
        else if defenderPlayersArray.count == 3{
            
            let playerInfoView1 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails3 = defenderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            defenderView.addSubview(playerInfoView1)
            defenderView.addSubview(playerInfoView2)
            defenderView.addSubview(playerInfoView3)
        }
        else if defenderPlayersArray.count == 4{
            
            let playerInfoView1 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerKabaddiPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails3 = defenderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails4 = defenderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            defenderView.addSubview(playerInfoView1)
            defenderView.addSubview(playerInfoView2)
            defenderView.addSubview(playerInfoView3)
            defenderView.addSubview(playerInfoView4)
        }
        
        if allRounderPlayerArray.count == 0{
            allRounderView.isHidden = true
        }
        else if allRounderPlayerArray.count == 1{
            
            let playerInfoView = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            allRounderView.addSubview(playerInfoView)
        }
        else if allRounderPlayerArray.count == 2{
            
            let playerInfoView1 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let details2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
        }
        else if allRounderPlayerArray.count == 3{
            
            let playerInfoView1 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: deatails1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails3 = allRounderPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
        }
        
        if raiderPlayersArray.count == 0{
            raiderView.isHidden = true
        }
        else if raiderPlayersArray.count == 1{
            
            let playerInfoView = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = raiderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            raiderView.addSubview(playerInfoView)
        }
        else if raiderPlayersArray.count == 2{
            
            let playerInfoView1 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = raiderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails2 = raiderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            raiderView.addSubview(playerInfoView1)
            raiderView.addSubview(playerInfoView2)
        }
        else if raiderPlayersArray.count == 3{
            
            let playerInfoView1 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerKabaddiPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))

            let details1 = raiderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails2 = raiderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            let deatails3 = raiderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamName: firstTeamName)
            
            raiderView.addSubview(playerInfoView1)
            raiderView.addSubview(playerInfoView2)
            raiderView.addSubview(playerInfoView3)

        }
        totalPointValue.text = String(totalPoint)
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let teamName = firstTeamName + " vs " + secondTeamName
        
        let text = String(format: "Check out my team for %@. Create your team on Letspick. Click here https://Letspick.app.link", teamName)
        
        // set up activity view controller
        let textToShare = [text, containerView.takeScreenshot()] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
}
