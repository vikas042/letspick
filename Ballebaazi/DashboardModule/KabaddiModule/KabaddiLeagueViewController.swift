//
//  KabaddiLeagueViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 31/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class KabaddiLeagueViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate{
    var userTicketsArray = Array<TicketDetails>()

    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var annuncementView: AnnouncementView!
    @IBOutlet weak var headerView: LetspickFantasyHeaderView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var myTeamButton: CustomBorderButton!
    @IBOutlet weak var myLeaguesButton: CustomBorderButton!
    
    @IBOutlet weak var lblJoinedLeagueTitle: UILabel!
    @IBOutlet weak var lblMyTeamTitle: UILabel!
    @IBOutlet weak var leagueCollectionView: UICollectionView!
    @IBOutlet weak var lblMyTeam: UILabel!
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var lblJoinLeagues: UILabel!
    @IBOutlet weak var placeholderImgView: UIImageView!
    var isFromJoiningConfirmation = false

    lazy var isPullToRefresh = false
    lazy var isUserValidatingToJoinLeague = false
    
    var timer: Timer?
    var matchDetails: MatchDetails?
    
    lazy var leagueArray:Array<LeagueDetails> = []
    lazy var classicLeagueArray:Array<LeagueDetails> = []
    lazy var battingLeagueArray:Array<LeagueDetails> = []
    lazy var bowlingLeagueArray:Array<LeagueDetails> = []
    lazy var classicCategoryArray:Array<LeagueCategoryDetails> = []
    lazy var battingCategoryArray:Array<LeagueCategoryDetails> = []
    lazy var bowlingCategoryArray:Array<LeagueCategoryDetails> = []
    var isMatchClosingTimeRefereshing = false

    lazy var isJoiningPop = false
    lazy var isFormNotification = false
    lazy var totalClassicTeam = 0
    lazy var totalBattingTeam = 0
    lazy var totalBowlingTeam = 0
    lazy var seasonKey = ""
    lazy var matchKey = ""
    
    lazy var selectedFantasy = FantasyType.Classic.rawValue
    lazy var totalClassicJoinedLeague = "0"
    lazy var totalBattingJoinedLeague = "0"
    lazy var totalBowlingJoinedLeague = "0"
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProperties()
    }
    
    //MARK:- Custom Methods
    func setupProperties() {
        
        
        if isFromJoiningConfirmation {
            headerView.tag = 1001
        }

        lblNoRecordFound.text = "Stay_Tuned_Message".localized()
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationComesInForground), name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)

        headerView.isFantasyModeEnable = false
        self.leagueCollectionView.isHidden = true
        self.bottomView.isHidden = true
        lblNoRecordFound.isHidden = true
        lblMyTeam.text = String(totalClassicTeam)
        lblJoinLeagues.text = totalClassicJoinedLeague
        headerView.updateMatchName(matchDetails: matchDetails)
        headerView.updateData(details: matchDetails)
        leagueCollectionView.register(UINib(nibName: "LeagueContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueContainerCollectionViewCell")
        
        if isFormNotification {
            headerView?.tag = 2000
        }
        
        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerValue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerValue), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UserDetails.sharedInstance.selectedPlayerList.removeAll();
        navigationController?.navigationBar.isHidden = true
        
        if matchDetails != nil {
            seasonKey = matchDetails!.seasonKey ?? ""
            matchKey = matchDetails!.matchKey 
        }

        let urlString = kKabaddiMatchURL + "?option=match_leagues&screen_msg=1&season_key=" + seasonKey + "&match_key=" + matchKey

        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
        if (savedResponse != nil) {
            if leagueArray.count == 0{
                
                let templateID = matchDetails?.templateID
                let isTicketAvailable = matchDetails?.isTicketAvailable ?? false
                
                matchDetails = MatchDetails.getSelectedMatchDetails(responseResult: savedResponse!)
                matchDetails?.templateID = templateID ?? ""
                matchDetails?.isTicketAvailable = isTicketAvailable

                
                leagueArray = LeagueDetails.getAllLeagueDetails(responseResult: savedResponse!, matchDetails: matchDetails!)
                
                if let response = savedResponse!["response"].dictionary{
                    var recomandedArray = Array<JSON>()
                    let leagueEecommendationString = response["league_recommendation"]?.string ?? ""
                    if leagueEecommendationString.count > 0 {
                        let json = JSON(parseJSON: leagueEecommendationString)
                        if (json.array != nil){
                            recomandedArray = json.array!
                        }
                    }
                    
                    if  response["categorisation"]?.dictionary != nil{
                        
                        (classicCategoryArray, battingCategoryArray, bowlingCategoryArray,_,_) = LeagueCategoryDetails.parseCategoryDetails(categoryDetails: response["categorisation"]!, leagueArray: leagueArray, recomnadedLeaguesArray: recomandedArray)
                    }
                }
                
                (totalClassicJoinedLeague, totalBattingJoinedLeague, totalBowlingJoinedLeague,_,_) = LeagueDetails.getJoinedLeagueCounts(responseResult: savedResponse!)
            }
            
            updateLeaguesArray()
            weak var weakSelf = self
            
            DispatchQueue.main.async {
                weakSelf?.leagueCollectionView.isHidden = false
                weakSelf?.bottomView.isHidden = false
                weakSelf?.leagueCollectionView.reloadData()
            }
            DispatchQueue.global().async {
                weakSelf?.callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: false)
            }
        }
        else{
            callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {

        if let totalClassic = UserDefaults.standard.value(forKey: "total_classic_kb") as? String{
            if Int(totalClassic) ?? 0 <= 0{
                if let kabaddiBanner = UserDefaults.standard.value(forKey: "kabaddiBanner") as? Date{
                    let days = getDayOfWeek(kabaddiBanner)
                    if days >= 1{
                        showClassicTutorial()
                    }
                }
                else{
                    showClassicTutorial()
                }
            }
        }
    }
    
    func getDayOfWeek(_ todayDate: Date) -> Int {
        let diffInDays = Calendar.current.dateComponents([.day], from: Date(), to: todayDate).day
        return diffInDays ?? 0
    }
    
    
    func showClassicTutorial(){
        let classicTutView = JoinLeagueTutorial(frame: APPDELEGATE.window!.frame)
        classicTutView.selectedFansatyType = FantasyType.Classic.rawValue
        classicTutView.selectedGameType = GameType.Kabaddi.rawValue
        classicTutView.configData()
        APPDELEGATE.window?.addSubview(classicTutView)
        classicTutView.showAnimation()
    }

    
    func updateLeagueJoinedStatus(joinedLeague: LeagueDetails, isticketUsed: Bool, fantasyType: String) {
        
        for leagueDetails in leagueArray {
            if leagueDetails.leagueId == joinedLeague.leagueId{
                leagueDetails.joinedLeagueCount += 1
                break;
            }
        }
        if isticketUsed {
            matchDetails?.isTicketAvailable = false
            matchDetails?.templateID = ""
        }
        updateLeaguesArray()
        leagueCollectionView.reloadData()
    }
    
    @objc func applicationComesInForground(notification: Notification)  {
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            if ((navVC.visibleViewController as? KabaddiLeagueViewController) != nil) {
                let urlString = kKabaddiMatchURL + "?option=match_leagues&screen_msg=1&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey
                callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: false)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)
    }
    
    @objc func refresh(sender:AnyObject) {
        let urlString = kKabaddiMatchURL + "?option=match_leagues&screen_msg=1&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey
        callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: false)
    }
    
    func updateLeaguesArray()  {
        
        let classicTeams = UserDetails.sharedInstance.userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "1"
        }
        
        let battingTeams = UserDetails.sharedInstance.userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "2"
        }
        
        let bowingTeams = UserDetails.sharedInstance.userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "3"
        }
        
        totalClassicTeam = classicTeams.count
        totalBattingTeam = battingTeams.count
        totalBowlingTeam = bowingTeams.count
        
        if selectedFantasy == FantasyType.Classic.rawValue {
            lblMyTeam.text = String(totalClassicTeam)
            lblJoinLeagues.text = String(totalClassicJoinedLeague)
            
            if totalClassicTeam > 1{
                lblMyTeamTitle.text = "Teams".localized()
            }
            else{
                lblMyTeamTitle.text = "Team".localized()
            }
            
            if Int(totalClassicJoinedLeague)! > 1{
                lblJoinedLeagueTitle.text = "Leagues".localized()
            }
            else{
                lblJoinedLeagueTitle.text = "League".localized()
            }
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            lblMyTeam.text = String(totalBattingTeam)
            lblJoinLeagues.text = String(totalBattingJoinedLeague)
            
            if totalBattingTeam > 1{
                lblMyTeamTitle.text = "Teams".localized()
            }
            else{
                lblMyTeamTitle.text = "Team".localized()
            }
            
            if Int(totalBattingJoinedLeague)! > 1{
                lblJoinedLeagueTitle.text = "Leagues".localized()
            }
            else{
                lblJoinedLeagueTitle.text = "League".localized()
            }
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            lblMyTeam.text = String(totalBowlingTeam)
            lblJoinLeagues.text = String(totalBowlingJoinedLeague)
            
            if totalBowlingTeam > 1{
                lblMyTeamTitle.text = "Teams".localized()
            }
            else{
                lblMyTeamTitle.text = "Team".localized()
            }
            
            if Int(totalBowlingJoinedLeague)! > 1{
                lblJoinedLeagueTitle.text = "Leagues".localized()
            }
            else{
                lblJoinedLeagueTitle.text = "League".localized()
            }
        }
        
        classicLeagueArray = leagueArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "1"
        }
        
        battingLeagueArray = leagueArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "2"
        }
        
        bowlingLeagueArray = leagueArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "3"
        }
        
        if selectedFantasy == FantasyType.Classic.rawValue {
            if classicLeagueArray.count == 0 {
                lblNoRecordFound.isHidden = false
                placeholderImgView.isHidden = false
            }
            else{
                lblNoRecordFound.isHidden = true
                placeholderImgView.isHidden = true
            }
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            if battingLeagueArray.count == 0 {
                lblNoRecordFound.isHidden = false
                placeholderImgView.isHidden = false
            }
            else{
                lblNoRecordFound.isHidden = true
                placeholderImgView.isHidden = true
            }
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            if bowlingLeagueArray.count == 0 {
                lblNoRecordFound.isHidden = false
                placeholderImgView.isHidden = false
            }
            else{
                lblNoRecordFound.isHidden = true
                placeholderImgView.isHidden = true
            }
        }
        
        headerView.updateMatchName(matchDetails: matchDetails)
        updateTimerValue()
        if matchDetails!.isMatchClosed{
            lblNoRecordFound.isHidden = true
            placeholderImgView.isHidden = true
            AppHelper.showMatchCLosedAlertAndPopToRootViewController()
        }
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if collectionView.tag == 100{
            return 1
        }
        else if selectedFantasy == FantasyType.Classic.rawValue {
            return classicCategoryArray.count
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            return battingCategoryArray.count
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            return bowlingCategoryArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 100{
            return 3
        }
        else{
            
            if selectedFantasy == FantasyType.Classic.rawValue {
                let details = classicCategoryArray[section]
                return details.visiableLeagueCount
            }
            else if selectedFantasy == FantasyType.Batting.rawValue {
                let details = battingCategoryArray[section]
                return details.visiableLeagueCount
            }
            else if selectedFantasy == FantasyType.Bowling.rawValue {
                let details = bowlingCategoryArray[section]
                return details.visiableLeagueCount
            }
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView.tag == 100 {
            return CGSize(width: 0, height: 0)
        }
        else{
            return CGSize(width: collectionView.frame.size.width, height: 48)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if collectionView.tag == 100 {
            return CGSize(width: 0, height: 0)
        }
        else{
            
            var details: LeagueCategoryDetails!
            if selectedFantasy == FantasyType.Classic.rawValue {
                details = classicCategoryArray[section]
                
                if classicCategoryArray.count == section+1{
                    if details.leaguesArray.count > details.visiableLeagueCount{
                        return CGSize(width: collectionView.frame.size.width, height: 80)
                    }
                    else{
                        return CGSize(width: collectionView.frame.size.width, height: 60)
                    }
                }
                else if details.leaguesArray.count > details.visiableLeagueCount{
                    return CGSize(width: collectionView.frame.size.width, height: 20)
                }
            }
            else if selectedFantasy == FantasyType.Batting.rawValue {
                details = battingCategoryArray[section]
                
                if battingCategoryArray.count == section+1{
                    if details.leaguesArray.count > details.visiableLeagueCount{
                        return CGSize(width: collectionView.frame.size.width, height: 80)
                    }
                    else{
                        return CGSize(width: collectionView.frame.size.width, height: 60)
                    }
                }
                else if details.leaguesArray.count > details.visiableLeagueCount{
                    return CGSize(width: collectionView.frame.size.width, height: 38)
                }
            }
            else if selectedFantasy == FantasyType.Bowling.rawValue {
                details = bowlingCategoryArray[section]
                
                if bowlingCategoryArray.count == section+1{
                    if details.leaguesArray.count > details.visiableLeagueCount{
                        return CGSize(width: collectionView.frame.size.width, height: 80)
                    }
                    else{
                        return CGSize(width: collectionView.frame.size.width, height: 60)
                    }
                }
                else if details.leaguesArray.count > details.visiableLeagueCount{
                    return CGSize(width: collectionView.frame.size.width, height: 38)
                }
            }
            
            
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 100 {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height-10)
        }
        else{
            
            var details: LeagueCategoryDetails!
            if selectedFantasy == FantasyType.Classic.rawValue {
                details = classicCategoryArray[indexPath.section]
            }
            else if selectedFantasy == FantasyType.Batting.rawValue {
                details = battingCategoryArray[indexPath.section]
            }
            else if selectedFantasy == FantasyType.Bowling.rawValue {
                details = bowlingCategoryArray[indexPath.section]
            }
            
            let reminder = details.visiableLeagueCount % 2
            if (reminder == 1) && (indexPath.row == details.visiableLeagueCount - 1){
                return CGSize(width: collectionView.frame.width , height: 137)
            }
            else{
                return CGSize(width: collectionView.frame.width/2 , height: 166)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            
            if selectedFantasy == FantasyType.Classic.rawValue {
                let details = classicCategoryArray[indexPath.section]
                headerView.lblCategoryName.text = details.categoryName.uppercased()
                headerView.lblMessage.text = details.categoryMessage
                showCategoryIcon(headerView: headerView, details: details)

            }
            else if selectedFantasy == FantasyType.Batting.rawValue {
                let details = battingCategoryArray[indexPath.section]
                headerView.lblCategoryName.text = details.categoryName.uppercased()
                headerView.lblMessage.text = details.categoryMessage
                showCategoryIcon(headerView: headerView, details: details)

            }
            else if selectedFantasy == FantasyType.Bowling.rawValue {
                let details = bowlingCategoryArray[indexPath.section]
                headerView.lblCategoryName.text = details.categoryName.uppercased()
                headerView.lblMessage.text = details.categoryMessage
                showCategoryIcon(headerView: headerView, details: details)

            }
            
            return headerView
            
        case UICollectionElementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
            
            if selectedFantasy == FantasyType.Classic.rawValue {
                let details = classicCategoryArray[indexPath.section]
                footerView.lblViewMore.text = "VIEW MORE(".localized() + String(details.leaguesArray.count - details.visiableLeagueCount) + ")"
                footerView.viewMoreButton.isHidden = false
                footerView.lblViewMore.isHidden = false
                if classicCategoryArray.count == indexPath.section+1{
                    if details.leaguesArray.count > details.visiableLeagueCount{
                        footerView.viewMoreButton.isHidden = false
                        footerView.lblViewMore.isHidden = false
                    }
                    else{
                        footerView.viewMoreButton.isHidden = true
                        footerView.lblViewMore.isHidden = true
                    }
                }
            }
            else if selectedFantasy == FantasyType.Batting.rawValue {
                let details = battingCategoryArray[indexPath.section]
                footerView.lblViewMore.text = "VIEW MORE(".localized() + String(details.leaguesArray.count - details.visiableLeagueCount) + ")"
                footerView.viewMoreButton.isHidden = false
                footerView.lblViewMore.isHidden = false
                if battingCategoryArray.count == indexPath.section+1{
                    if details.leaguesArray.count > details.visiableLeagueCount{
                        footerView.viewMoreButton.isHidden = false
                        footerView.lblViewMore.isHidden = false
                    }
                    else{
                        footerView.viewMoreButton.isHidden = true
                        footerView.lblViewMore.isHidden = true
                    }
                }
            }
            else if selectedFantasy == FantasyType.Bowling.rawValue {
                let details = bowlingCategoryArray[indexPath.section]
                footerView.lblViewMore.text = "VIEW MORE(".localized() + String(details.leaguesArray.count - details.visiableLeagueCount) + ")"
                footerView.viewMoreButton.isHidden = false
                footerView.lblViewMore.isHidden = false
                if bowlingCategoryArray.count == indexPath.section+1{
                    if details.leaguesArray.count > details.visiableLeagueCount{
                        footerView.viewMoreButton.isHidden = false
                        footerView.lblViewMore.isHidden = false
                    }
                    else{
                        footerView.viewMoreButton.isHidden = true
                        footerView.lblViewMore.isHidden = true
                    }
                }
            }
            footerView.viewMoreButton.tag = indexPath.section;
            footerView.viewMoreButton.addTarget(self, action: #selector(viewMoreButtonTapped(button:)), for: .touchUpInside)
            return footerView
            
        default:
            return UICollectionReusableView()
        }
    }
    
    @objc func viewMoreButtonTapped(button: UIButton) {
        let moreLeagueVC = storyboard?.instantiateViewController(withIdentifier: "MoreKabaddiViewController") as! MoreKabaddiViewController
        AppxorEventHandler.logAppEvent(withName: "ViewMoreClicked", info: ["SportType": "Kabaddi"])

        if selectedFantasy == FantasyType.Classic.rawValue {
            let details = classicCategoryArray[button.tag]
            moreLeagueVC.selectedFantasy = FantasyType.Classic.rawValue
            moreLeagueVC.categoryName = details.categoryName
            moreLeagueVC.categoryID = details.categoryID
            moreLeagueVC.totalJoinedLeague = Int(totalClassicJoinedLeague) ?? 0;
            moreLeagueVC.totalTeams = totalClassicTeam
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            let details = battingCategoryArray[button.tag]
            moreLeagueVC.selectedFantasy = FantasyType.Batting.rawValue
            moreLeagueVC.categoryName = details.categoryName
            moreLeagueVC.categoryID = details.categoryID
            moreLeagueVC.totalJoinedLeague = Int(totalBattingJoinedLeague) ?? 0;
            moreLeagueVC.totalTeams = totalBattingTeam
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            let details = bowlingCategoryArray[button.tag]
            moreLeagueVC.selectedFantasy = FantasyType.Bowling.rawValue
            moreLeagueVC.categoryName = details.categoryName
            moreLeagueVC.categoryID = details.categoryID
            moreLeagueVC.totalJoinedLeague = Int(totalBattingJoinedLeague) ?? 0;
            moreLeagueVC.totalTeams = totalBattingTeam
        }
        
        moreLeagueVC.matchDetails = matchDetails
        navigationController?.pushViewController(moreLeagueVC, animated: true)
    }
    
    func showCategoryIcon(headerView: HeaderCollectionReusableView, details: LeagueCategoryDetails) {
        
        if details.categoryImg.count != 0{
            if let url = NSURL(string: UserDetails.sharedInstance.teamImageUrl + details.categoryImg){
                headerView.imgView.setImage(with: url as URL, placeholder: UIImage(named: "LeagueDefaultIcon"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        headerView.imgView.image = image
                    }
                    else{
                        headerView.imgView.image = UIImage(named: "LeagueDefaultIcon")
                    }
                })
            }
        }
        else{
            headerView.imgView.image = UIImage(named: "LeagueDefaultIcon")
        }

        headerView.layoutIfNeeded()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 100{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueContainerCollectionViewCell", for: indexPath) as? LeagueContainerCollectionViewCell
            cell!.leagueCollectionView.register(UINib(nibName: "LeagueCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueCollectionViewCell")
            cell!.leagueCollectionView.register(UINib(nibName: "SingleLeagueCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SingleLeagueCollectionViewCell")
            
            cell!.leagueCollectionView.delegate = self
            cell!.leagueCollectionView.dataSource = self
            
            cell!.leagueCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
            cell!.leagueCollectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionReusableView")
            cell!.leagueCollectionView.reloadData()
            
            return cell!;
        }
        else{
            
            var details: LeagueCategoryDetails!
            if selectedFantasy == FantasyType.Classic.rawValue {
                details = classicCategoryArray[indexPath.section]
            }
            else if selectedFantasy == FantasyType.Batting.rawValue {
                details = battingCategoryArray[indexPath.section]
            }
            else if selectedFantasy == FantasyType.Bowling.rawValue {
                details = bowlingCategoryArray[indexPath.section]
            }
            
            let reminder = details.visiableLeagueCount % 2
            if (reminder == 1) && (indexPath.row == details.visiableLeagueCount - 1){
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleLeagueCollectionViewCell", for: indexPath) as! SingleLeagueCollectionViewCell
                cell.joinButton.tag = indexPath.row
                cell.joinButton.addTarget(self, action: #selector(self.joinLeagueButtonTapped(button:)), for: .touchUpInside)
                cell.configData(details: details.leaguesArray[indexPath.row], matchDetails: matchDetails!, gameType: GameType.Kabaddi.rawValue)
                return cell
            }
            else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueCollectionViewCell", for: indexPath) as! LeagueCollectionViewCell
                cell.joinButton.tag = indexPath.row
                cell.joinButton.addTarget(self, action: #selector(self.joinLeagueButtonTapped(button:)), for: .touchUpInside)
                cell.configData(details: details.leaguesArray[indexPath.row], matchDetails: matchDetails!, gameType: GameType.Kabaddi.rawValue)
                return cell
            }            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var categoryDetails: LeagueCategoryDetails!
        if selectedFantasy == FantasyType.Classic.rawValue {
            categoryDetails = classicCategoryArray[indexPath.section]
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            categoryDetails = battingCategoryArray[indexPath.section]
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            categoryDetails = bowlingCategoryArray[indexPath.section]
        }
        
        let leagueDetails = categoryDetails.leaguesArray[indexPath.row]
        if leagueDetails.leagueType == "2" {
            return
        }
        
        let leaguePreview = storyboard?.instantiateViewController(withIdentifier: "KabaddiLeaguePreviewViewController") as! KabaddiLeaguePreviewViewController
        leaguePreview.userTicketsArray = userTicketsArray
        leaguePreview.leagueDetails = leagueDetails
        leaguePreview.matchDetails = matchDetails
//        leaguePreview.catgegoryName = categoryDetails.categoryName
        navigationController?.pushViewController(leaguePreview, animated: true)
    }
    
    @objc func joinLeagueButtonTapped(button: SolidButton)  {
        
        var containerCell: LeagueContainerCollectionViewCell!
        if selectedFantasy == FantasyType.Classic.rawValue {
            guard let classicLeague = leagueCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? LeagueContainerCollectionViewCell else{
                return;
            }
            containerCell = classicLeague
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            guard let battingLeague = leagueCollectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? LeagueContainerCollectionViewCell else{
                return;
            }
            containerCell = battingLeague
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            guard let bowlingLeague = leagueCollectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? LeagueContainerCollectionViewCell else{
                return;
            }
            containerCell = bowlingLeague
        }
        
        var leagueCell: LeagueCollectionViewCell?
        var singleCell: SingleLeagueCollectionViewCell?
        
        if let cell = button.superview?.superview?.superview?.superview?.superview as? LeagueCollectionViewCell {
            leagueCell = cell
        }
        
        if let cell = button.superview?.superview?.superview?.superview?.superview as? SingleLeagueCollectionViewCell {
            singleCell = cell
        }
        
        var cellIndexPath: IndexPath!
        
        if leagueCell != nil{
            guard let indexPath = containerCell.leagueCollectionView.indexPath(for: leagueCell!) else{
                return;
            }
            cellIndexPath = indexPath
        }
        
        if singleCell != nil{
            guard let indexPath = containerCell.leagueCollectionView.indexPath(for: singleCell!) else{
                return;
            }
            cellIndexPath = indexPath
        }
        
        var categoryDetails: LeagueCategoryDetails!
        if selectedFantasy == FantasyType.Classic.rawValue {
            categoryDetails = classicCategoryArray[cellIndexPath.section]
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            categoryDetails = battingCategoryArray[cellIndexPath.section]
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            categoryDetails = bowlingCategoryArray[cellIndexPath.section]
        }
        
        let leagueDetails = categoryDetails.leaguesArray[cellIndexPath.row]
        
        if (leagueDetails.teamType == "1") {
            if leagueDetails.joinedLeagueCount == UserDetails.sharedInstance.maxTeamAllowedForKabaddi{
                return;
            }
        }
        else if (leagueDetails.teamType == "1") {
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.confirmedLeague == "2"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.teamType == "2"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.teamType == "3"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        
        if isUserValidatingToJoinLeague {
            return;
        }
        
        AppxorEventHandler.logAppEvent(withName: "JoinNowButtonClicked", info: ["ContestType": categoryDetails.categoryName, "ContestID": leagueDetails.leagueId, "SportType": "Kabaddi"])
        
        callLeagueValidationAPI(leagueDetails: leagueDetails, leagueCategoryName: categoryDetails.categoryName)
    }
    
    // MARK:- -IBAction Methods
    
    @IBAction func myLeagueButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "TeamTabClicked", info: ["SportType": "Kabaddi"])

        let myTeamVC = storyboard?.instantiateViewController(withIdentifier: "MyKabaddiTeamViewController") as! MyKabaddiTeamViewController
        myTeamVC.matchDetails = matchDetails
        myTeamVC.userTeamsArray = UserDetails.sharedInstance.userTeamsArray
        
        if selectedFantasy == FantasyType.Classic.rawValue {
            myTeamVC.selectedLeagueType = FantasyType.Classic.rawValue
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            myTeamVC.selectedLeagueType = FantasyType.Batting.rawValue
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            myTeamVC.selectedLeagueType = FantasyType.Bowling.rawValue
        }
        navigationController?.pushViewController(myTeamVC, animated: true)
        
    }
    
    @IBAction func joinedLeagueButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "LeagueTabClicked", info: ["SportType": "Kabaddi"])

        let joinedWinnerRankVC = storyboard?.instantiateViewController(withIdentifier: "JoinedKabaddiLeagueViewController") as! JoinedKabaddiLeagueViewController
        joinedWinnerRankVC.matchDetails = matchDetails
        self.navigationController?.pushViewController(joinedWinnerRankVC, animated: true)
    }
    
    func classicButtonTapped(isAnimation: Bool) {
        selectedFantasy = FantasyType.Classic.rawValue
        lblMyTeam.text = String(totalClassicTeam)
        lblJoinLeagues.text = String(totalClassicJoinedLeague)
        
        if classicLeagueArray.count == 0 {
            lblNoRecordFound.isHidden = false
            placeholderImgView.isHidden = false
        }
        else{
            lblNoRecordFound.isHidden = true
            placeholderImgView.isHidden = true
        }
        
        if totalClassicTeam > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalClassicJoinedLeague)! > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
        
        let indexPath = IndexPath(item: 0, section: 0)
        weak var weakSelf = self
        DispatchQueue.main.async {
            if isAnimation{
                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
            }
            else{
                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
            }
            weakSelf?.leagueCollectionView.reloadData()
        }
    }
    
    func battingButtonTapped(isAnimation: Bool) {
        selectedFantasy = FantasyType.Batting.rawValue
        lblMyTeam.text = String(totalBattingTeam)
        lblJoinLeagues.text = String(totalBattingJoinedLeague)
        if battingLeagueArray.count == 0 {
            lblNoRecordFound.isHidden = false
            placeholderImgView.isHidden = false
        }
        else{
            lblNoRecordFound.isHidden = true
            placeholderImgView.isHidden = true
            
        }
        
        if totalBattingTeam > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalBattingJoinedLeague)! > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
        
        
        let indexPath = IndexPath(item: 1, section: 0)
        weak var weakSelf = self
        
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    func bowlingButtonTapped(isAnimation: Bool) {
        selectedFantasy = FantasyType.Bowling.rawValue
        lblMyTeam.text = String(totalBowlingTeam)
        lblJoinLeagues.text = String(totalBowlingJoinedLeague)
        
        if bowlingLeagueArray.count == 0 {
            lblNoRecordFound.isHidden = false
            placeholderImgView.isHidden = false
            
        }
        else{
            lblNoRecordFound.isHidden = true
            placeholderImgView.isHidden = true
        }
        
        if totalBowlingTeam > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalBowlingJoinedLeague)! > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
        
        let indexPath = IndexPath(item: 2, section: 0)
        weak var weakSelf = self
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    // MARK:- API Related Method
    
    func callGetLeagueAPI(urlString: String, isNeedToShowLoader: Bool)  {
        //        return
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                isPullToRefresh = false
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        weak var weakSelf = self

        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            self.isPullToRefresh = false
            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
//                    DispatchQueue.global().async {
//                        self.callMyTicketsAPI()
//                    }
                    
                    DispatchQueue.main.async {
                        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
                        let templateID = self.matchDetails?.templateID
                        let isTicketAvailable = self.matchDetails?.isTicketAvailable ?? false
                        
                        self.matchDetails = MatchDetails.getSelectedMatchDetails(responseResult: savedResponse!)
                        self.matchDetails?.templateID = templateID ?? ""
                        self.matchDetails?.isTicketAvailable = isTicketAvailable;
                        self.leagueArray = LeagueDetails.getAllLeagueDetails(responseResult: savedResponse!, matchDetails: self.matchDetails!)
                        
                        (self.totalClassicJoinedLeague, self.totalBattingJoinedLeague, self.totalBowlingJoinedLeague,_,_) = LeagueDetails.getJoinedLeagueCounts(responseResult: savedResponse!)
                        if let response = savedResponse!["response"].dictionary{
                            var recomandedArray = Array<JSON>()
                            let leagueEecommendationString = response["league_recommendation"]?.string ?? ""
                            if leagueEecommendationString.count > 0 {
                                let json = JSON(parseJSON: leagueEecommendationString)
                                if (json.array != nil){
                                    recomandedArray = json.array!
                                }
                            }
                            if let announcement = response["announcement"]?.dictionary{
                                let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                                
                                self.announcementViewHeightConstraint.constant = 50.0
                                self.view.layoutIfNeeded()
                                self.annuncementView.showAnnouncementMessage(message: details.message)
                            }
                            else{
                                self.announcementViewHeightConstraint.constant = 0.0
                                self.view.layoutIfNeeded()
                                self.annuncementView.showAnnouncementMessage(message: "")
                            }
                            
                            if  response["categorisation"]?.dictionary != nil{
                                (self.classicCategoryArray, self.battingCategoryArray, self.bowlingCategoryArray,_,_) = LeagueCategoryDetails.parseCategoryDetails(categoryDetails: response["categorisation"]!, leagueArray: self.leagueArray, recomnadedLeaguesArray: recomandedArray)
                            }
                            
                            if let tempArray = response["active_tickets"]?.array {
                                (weakSelf!.userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                                
                                for ticketDetails in weakSelf!.userTicketsArray{
                                    for details in weakSelf!.leagueArray {
                                        if ticketDetails.ticketType != "2"{
                                            if (details.categoryId == ticketDetails.leagueCategory) && (ticketDetails.matchKey == weakSelf!.matchKey) && (ticketDetails.joiningAmount == details.joiningAmount){
                                                details.isTicketAvailable = true
                                                if ticketDetails.ticketType == "3" {
                                                    details.isPassAvailable = true
                                                }
                                            }
                                        }
                                        else{
                                            if (details.categoryId == ticketDetails.leagueCategory) && (ticketDetails.joiningAmount == details.joiningAmount){
                                                details.isTicketAvailable = true
                                            }
                                        }
                                    }
                                }
                                weakSelf!.leagueCollectionView.reloadData()
                            }
                        }
                        
                        self.lblMyTeam.isHidden = false
                        self.lblJoinLeagues.isHidden = false
                        if self.leagueArray.count == 0{
                            self.leagueCollectionView.isHidden = true
                            self.bottomView.isHidden = true
                        }
                        else{
                            self.leagueCollectionView.isHidden = false
                            self.bottomView.isHidden = false
                        }
                        
                        DispatchQueue.main.async {
                            self.headerView.updateMatchName(matchDetails: self.matchDetails)
                            
//                            if self.selectedFantasy == FantasyType.Classic.rawValue {
//                                if self.classicLeagueArray.count == 0 {
//                                    self.lblNoRecordFound.isHidden = false
//                                    self.placeholderImgView.isHidden = false
//                                    
//                                }
//                                else{
//                                    self.lblNoRecordFound.isHidden = true
//                                    self.placeholderImgView.isHidden = true
//                                }
//                            }
//                            else if self.selectedFantasy == FantasyType.Classic.rawValue {
//                                if self.battingLeagueArray.count == 0 {
//                                    self.lblNoRecordFound.isHidden = false
//                                    self.placeholderImgView.isHidden = false
//                                }
//                                else{
//                                    self.lblNoRecordFound.isHidden = true
//                                    self.placeholderImgView.isHidden = true
//                                }
//                            }
//                            else if self.selectedFantasy == FantasyType.Classic.rawValue {
//                                if self.bowlingLeagueArray.count == 0 {
//                                    self.lblNoRecordFound.isHidden = false
//                                    self.placeholderImgView.isHidden = false
//                                }
//                                else{
//                                    self.lblNoRecordFound.isHidden = true
//                                    self.placeholderImgView.isHidden = true
//                                }
//                            }
                            
                            self.leagueCollectionView.reloadData()
                        }

                        self.updateLeaguesArray()
                        AppHelper.sharedInstance.removeSpinner()
                    }
                }
                else{
                    AppHelper.sharedInstance.removeSpinner()
                }
            }
            else{
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
    
    func callMyTicketsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        weak var weakSelf = self

        let params = ["option": "user_tickets", "user_id": UserDetails.sharedInstance.userID]     
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200"{
                    
                    if let response = result!["response"]?.dictionary{
                        if let tempArray = response["active_tickets"]?.array {
                            (weakSelf!.userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                            
                            for ticketDetails in weakSelf!.userTicketsArray{
                                for details in self.leagueArray {
                                                                        
                                    if (details.templateID == ticketDetails.templateID) && (ticketDetails.matchKey == weakSelf!.matchDetails!.matchKey){
                                        details.isTicketAvailable = true
                                    }
                                }
                            }
                            self.leagueCollectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails, leagueCategoryName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        isUserValidatingToJoinLeague = true
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league_preview_v1","check_ticket": "1", "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kKabaddiMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            weakSelf?.isUserValidatingToJoinLeague = false
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
                                    /*
                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
                                        
                                        let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
                                        addCashVC?.leagueDetails = leagueDetails
                                        addCashVC?.amount = roundFigureAmt
                                        addCashVC?.matchDetails = weakSelf!.matchDetails
                                        addCashVC?.userTeamArray = teamArray
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                            navVC.pushViewController(addCashVC!, animated: true)
                                        }
                                    }))
                                     */
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                    playerVC.matchDetails = weakSelf?.matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails, userTeamArray: teamArray, leagueCategoryName: "", ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                playerVC.matchDetails = weakSelf?.matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                
                            }
                        }
                        else{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, leagueCategoryName: String, ticketDetails: TicketDetails?)  {
        
        let joinedLeagueConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
        joinedLeagueConfirmVC?.leagueDetails = leagueDetails
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails
        joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
        joinedLeagueConfirmVC?.matchDetails = self.matchDetails
        joinedLeagueConfirmVC?.selectedGameType = GameType.Kabaddi.rawValue
       joinedLeagueConfirmVC?.leagueCategoryName = leagueCategoryName
        navigationController?.pushViewController(joinedLeagueConfirmVC!, animated: true)
    }
    
    // MARK:- Timer Handlers
    @objc func updateTimerValue()  {
        
        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
        
        leagueCollectionView.reloadData()
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kKabaddiMatchURL, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                            AppHelper.showMatchCLosedAlertAndPopToRootViewController()
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SelectKadaddiPlayersViewController" {
            
            let playerVC = segue.destination as! SelectKadaddiPlayersViewController
            playerVC.matchDetails = matchDetails
            playerVC.leagueDetails = (sender as! LeagueDetails)
        }
    }
    
    //MARK:- Scroll View Delegates
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if !isPullToRefresh{
            if scrollView.contentOffset.y < -120{
                isPullToRefresh = true
                let urlString = kKabaddiMatchURL + "?option=match_leagues&screen_msg=1&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey
                callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: true)
            }
        }
    }
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
