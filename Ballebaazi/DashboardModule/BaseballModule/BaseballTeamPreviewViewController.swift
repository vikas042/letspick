//
//  BaseballTeamPreviewViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class BaseballTeamPreviewViewController: UIViewController {
    
    @IBOutlet weak var outFielderAspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet weak var inFielderViewCenterVertivacalConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var bottomSepView: UIView!

    @IBOutlet weak var lblInFielder: UILabel!
    @IBOutlet weak var lblCatcher: UILabel!
    @IBOutlet weak var lblPitcher: UILabel!
    @IBOutlet weak var lblOffFielder: UILabel!

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var inFielderView: UIView!
    @IBOutlet weak var outFielderView: UIView!
    @IBOutlet weak var pitcherView: UIView!
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var totalPointValue: UILabel!
    @IBOutlet weak var lblTotalPointTitle: UILabel!
    @IBOutlet weak var catcherView: UIView!
    var totalPlayerArray = Array<PlayerDetails>()
    var selectedFantasy = FantasyType.Classic.rawValue
    
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblAnnounced: UILabel!

    var isMatchClosed = false
    var isShowPlayingRole = false
    var firstTeamName = ""
    var secondTeamName = ""
    
    var firstTeamkey = ""
    var secondTeamkey = ""

    var teamNumber = ""
    var userName = ""
    var isHideEditButton = false
    
    @IBOutlet weak var lblTeamNameTwo: UILabel!
    @IBOutlet weak var lblTeamNameOne: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTeamNameOne.text = firstTeamName
        lblTeamNameTwo.text = secondTeamName
        lblAnnounced.text = "ANNOUNCED".localized()        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isHideEditButton {
            editButton.isHidden = true
        }
        if isMatchClosed {
            lblTotalPointTitle.text = "Total Points".localized()
            editButton.isHidden = true
            lblTotalPointTitle.isHidden = false
            totalPointValue.isHidden = false

            if AppHelper.isApplicationRunningOnIphoneX() {
                bottomViewHeightConstraint.constant = 110
                inFielderViewCenterVertivacalConstraint.constant = -40
                outFielderAspectRatioConstraint.constant = -98
                pointsView.layoutIfNeeded()
            }
            else{
                bottomViewHeightConstraint.constant = 70
            }
        }
        else{
            lblTotalPointTitle.text = "Credit Points".localized()
            if AppHelper.isApplicationRunningOnIphoneX() {
                bottomViewHeightConstraint.constant = 50
                inFielderViewCenterVertivacalConstraint.constant = -40
                outFielderAspectRatioConstraint.constant = -98
                pointsView.layoutIfNeeded()
            }
            lblTotalPointTitle.isHidden = true
            totalPointValue.isHidden = true
            bottomSepView.isHidden = true
            view.layoutIfNeeded()
        }
        
        if selectedFantasy == FantasyType.Classic.rawValue {
            if teamNumber.count > 0{
                lblTeamName.text = "Team".localized() + " " + teamNumber
                if userName.count > 0{
                    lblUserName.text = userName
                }
                else{
                    lblUserName.text = UserDetails.sharedInstance.userName
                }
            }
            else{
                lblTeamName.text = "TeamPreview".localized()
            }
        }
        showPlayerBeforeMakingTeam()
    }
    
    func showPlayerBeforeMakingTeam() {
        let offFielderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Outfielders.rawValue
        })
        
        let inFielderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Infielder.rawValue
        })
        
        let pitcherPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Pitcher.rawValue
        })
        
        let catcherPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Catcher.rawValue
        })
        
        lblInFielder.text = "Infielder".localized()
        lblPitcher.text = "Pitcher".localized()
        lblCatcher.text = "Catcher".localized()
        lblOffFielder.text = "Outfielder".localized()
        
        let viewWidth = UIScreen.main.bounds.width/5 - 8;
        var totalPoint: Float = 0
        
        if offFielderPlayersArray.count == 0{
            outFielderView.isHidden = true
        }
        else if offFielderPlayersArray.count == 1{
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            let details = offFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: details, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView.selectedGameType = GameType.Baseball.rawValue
            outFielderView.addSubview(playerInfoView)
        }
        else if offFielderPlayersArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = offFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = offFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue

            outFielderView.addSubview(playerInfoView1)
            outFielderView.addSubview(playerInfoView2)
        }
        else if offFielderPlayersArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = offFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = offFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = offFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue

            outFielderView.addSubview(playerInfoView1)
            outFielderView.addSubview(playerInfoView2)
            outFielderView.addSubview(playerInfoView3)
        }
        else if offFielderPlayersArray.count == 4{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = offFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = offFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = offFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = offFielderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue
            playerInfoView4.selectedGameType = GameType.Baseball.rawValue

            outFielderView.addSubview(playerInfoView1)
            outFielderView.addSubview(playerInfoView2)
            outFielderView.addSubview(playerInfoView3)
            outFielderView.addSubview(playerInfoView4)
        }
        else if offFielderPlayersArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = offFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = offFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = offFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = offFielderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = offFielderPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue
            playerInfoView4.selectedGameType = GameType.Baseball.rawValue
            playerInfoView5.selectedGameType = GameType.Baseball.rawValue


            outFielderView.addSubview(playerInfoView1)
            outFielderView.addSubview(playerInfoView2)
            outFielderView.addSubview(playerInfoView3)
            outFielderView.addSubview(playerInfoView4)
            outFielderView.addSubview(playerInfoView5)
        }
        
        if inFielderPlayersArray.count == 0{
            inFielderView.isHidden = true
        }
        else if inFielderPlayersArray.count == 1{
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let deatails = inFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView.selectedGameType = GameType.Baseball.rawValue

            inFielderView.addSubview(playerInfoView)
        }
        else if inFielderPlayersArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = inFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = inFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue

            inFielderView.addSubview(playerInfoView1)
            inFielderView.addSubview(playerInfoView2)
        }
        else if inFielderPlayersArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = inFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = inFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = inFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue

            inFielderView.addSubview(playerInfoView1)
            inFielderView.addSubview(playerInfoView2)
            inFielderView.addSubview(playerInfoView3)
        }
        else if inFielderPlayersArray.count == 4{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = inFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = inFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = inFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = inFielderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue
            playerInfoView4.selectedGameType = GameType.Baseball.rawValue

            inFielderView.addSubview(playerInfoView1)
            inFielderView.addSubview(playerInfoView2)
            inFielderView.addSubview(playerInfoView3)
            inFielderView.addSubview(playerInfoView4)
        }
        else if inFielderPlayersArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = inFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = inFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = inFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = inFielderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = inFielderPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue
            playerInfoView4.selectedGameType = GameType.Baseball.rawValue
            playerInfoView5.selectedGameType = GameType.Baseball.rawValue

            inFielderView.addSubview(playerInfoView1)
            inFielderView.addSubview(playerInfoView2)
            inFielderView.addSubview(playerInfoView3)
            inFielderView.addSubview(playerInfoView4)
            inFielderView.addSubview(playerInfoView5)
        }
        
        if pitcherPlayersArray.count == 0{
            pitcherView.isHidden = true
        }
        else if pitcherPlayersArray.count == 1{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let deatails = pitcherPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView.selectedGameType = GameType.Baseball.rawValue

            pitcherView.addSubview(playerInfoView)
        }
        else if pitcherPlayersArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = pitcherPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = pitcherPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue

            pitcherView.addSubview(playerInfoView1)
            pitcherView.addSubview(playerInfoView2)
        }
        else if pitcherPlayersArray.count == 3{
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = pitcherPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = pitcherPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = pitcherPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue

            pitcherView.addSubview(playerInfoView1)
            pitcherView.addSubview(playerInfoView2)
            pitcherView.addSubview(playerInfoView3)
        }
        else if pitcherPlayersArray.count == 4{
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = pitcherPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = pitcherPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = pitcherPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = pitcherPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue
            playerInfoView4.selectedGameType = GameType.Baseball.rawValue

            pitcherView.addSubview(playerInfoView1)
            pitcherView.addSubview(playerInfoView2)
            pitcherView.addSubview(playerInfoView3)
            pitcherView.addSubview(playerInfoView4)
        }
        else if pitcherPlayersArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = pitcherPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = pitcherPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = pitcherPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = pitcherPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = pitcherPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue
            playerInfoView4.selectedGameType = GameType.Baseball.rawValue
            playerInfoView5.selectedGameType = GameType.Baseball.rawValue

            pitcherView.addSubview(playerInfoView1)
            pitcherView.addSubview(playerInfoView2)
            pitcherView.addSubview(playerInfoView3)
            pitcherView.addSubview(playerInfoView4)
            pitcherView.addSubview(playerInfoView5)
        }
        
        if catcherPlayerArray.count == 0{
            catcherView.isHidden = true
        }
        else if catcherPlayerArray.count == 1{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let deatails = catcherPlayerArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView.selectedGameType = GameType.Baseball.rawValue
            catcherView.addSubview(playerInfoView)
        }
        else if catcherPlayerArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let details1 = catcherPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let details2 = catcherPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue

            catcherView.addSubview(playerInfoView1)
            catcherView.addSubview(playerInfoView2)
            
        }
        else if catcherPlayerArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 18, width: viewWidth, height: viewWidth + 9))
            
            let deatails1 = catcherPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: deatails1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = catcherPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = catcherPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Baseball.rawValue
            playerInfoView2.selectedGameType = GameType.Baseball.rawValue
            playerInfoView3.selectedGameType = GameType.Baseball.rawValue

            catcherView.addSubview(playerInfoView1)
            catcherView.addSubview(playerInfoView2)
            catcherView.addSubview(playerInfoView3)
        }
        
        totalPointValue.text = String(totalPoint)
        
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let teamName = firstTeamName + " vs " + secondTeamName
        
        let text = String(format: "Check out my team for %@. Create your team on Letspick. Click here https://Letspick.app.link", teamName)
        
        // set up activity view controller
        let textToShare = [text, containerView.takeScreenshot()] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
}
