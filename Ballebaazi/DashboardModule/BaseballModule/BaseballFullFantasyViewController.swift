//
//  BaseballFullFantasyViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 28/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class BaseballFullFantasyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var headerView: CustomNavigationBar!
    
    lazy var totalOutFieldersArray = Array<PlayerDetails>()
    lazy var totalInFieldersArray = Array<PlayerDetails>()
    lazy var totalPitcherArray = Array<PlayerDetails>()
    lazy var totalCatcherArray = Array<PlayerDetails>()

    lazy var playerListArray = Array<PlayerDetails>()
    
    var fantasyType = ""
    var matchKey = ""
    var firstTeamName = ""
    var secondTeamName = ""
    var playersGender = ""
    var matchDetails: MatchDetails?

    @IBOutlet weak var teamTwoButton: UIButton!
    @IBOutlet weak var teamOneButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "FullFantasyTableViewCell", bundle: nil), forCellReuseIdentifier: "FullFantasyTableViewCell")
        headerView.headerTitle = firstTeamName + " vs " + secondTeamName
        teamTwoButton.setTitle(secondTeamName, for: .normal)
        teamOneButton.setTitle(firstTeamName, for: .normal)

        var subTitle = ""
        if matchDetails?.isMatchClosed ?? false {
            if matchDetails!.matchStatus == "completed"{
                subTitle = "Completed".localized()
            }
            else if matchDetails!.matchStatus == "started"{
                subTitle = "Live".localized()
            }
            else{
                subTitle = "Leagues Closed".localized()
            }
        }
        headerView.updateHeaderTitles(title: firstTeamName + " vs " + secondTeamName, subTitle: subTitle)
        callGetTeamPlayersDetails()
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        
        if selectedRow == 0{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Outfielders.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Infielder.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Pitcher.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Catcher.rawValue
            })
        }
        
        if playersArray != nil{
            return playersArray!
        }
        
        return []
    }
    
    
    private func callGetTeamPlayersDetails()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "full_scoreboard", "match_key": matchKey, "team_number": "1", "user_id": UserDetails.sharedInstance.userID, "type": "", "fantasy_type": fantasyType]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kBaseballSocrescardUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let playerArray = result!["response"]?.array{
                        
                        weakSelf?.playerListArray = PlayerDetails.getPlayerDetailsForScoreArray(responseArray: playerArray)
                        if self.playersGender == "F" {
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: weakSelf!.playerListArray)
                        }
                        weakSelf?.totalOutFieldersArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 0)
                        weakSelf?.totalInFieldersArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 1)
                        weakSelf?.totalPitcherArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 2)
                        weakSelf?.totalCatcherArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 3)
                        weakSelf?.tblView.reloadData()
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (totalOutFieldersArray.count > 0) && totalCatcherArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            return 4
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 && totalInFieldersArray.count > 0{
            return 3
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 && totalPitcherArray.count > 0{
            return 3
        }
        else if totalCatcherArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            return 3
        }
        else if totalInFieldersArray.count > 0 && totalOutFieldersArray.count > 0 && totalPitcherArray.count > 0{
            return 3
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 {
            return 2
        }
        else if totalOutFieldersArray.count > 0 && totalInFieldersArray.count > 0 {
            return 2
        }
        else if totalOutFieldersArray.count > 0 && totalPitcherArray.count > 0 {
            return 2
        }
        else if totalCatcherArray.count > 0 && totalInFieldersArray.count > 0{
            return 2
        }
        else if totalCatcherArray.count > 0 && totalPitcherArray.count > 0{
            return 2
        }
        else if totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            return 2
        }
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        label.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255, blue: 240.0/255, alpha: 1)
        label.text = getPlayerType(section: section)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.textColor = UIColor(red: 127.0/255.0, green: 132.0/255, blue: 134.0/255, alpha: 1)
        label.font = UIFont(name: "OpenSans-Semibold", size: 12)
//        label.backgroundColor = UIColor.white
        return label;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (totalOutFieldersArray.count > 0) && totalCatcherArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalOutFieldersArray.count
            }
            else if section == 1 {
                return totalInFieldersArray.count
            }
            else if section == 2 {
                return totalPitcherArray.count
            }
            else if section == 3 {
                return totalCatcherArray.count
            }
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 && totalInFieldersArray.count > 0{
            if section == 0 {
                return totalOutFieldersArray.count
            }
            else if section == 1 {
                return totalInFieldersArray.count
            }
            else if section == 2 {
                return totalCatcherArray.count
            }
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalOutFieldersArray.count
            }
            else if section == 1 {
                return totalPitcherArray.count
            }
            else if section == 3 {
                return totalCatcherArray.count
            }
        }
        else if totalOutFieldersArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalOutFieldersArray.count
            }
            else if section == 1 {
                return totalInFieldersArray.count
            }
            else if section == 2 {
                return totalPitcherArray.count
            }
        }
        else if totalCatcherArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalInFieldersArray.count
            }
            else if section == 1 {
                return totalPitcherArray.count
            }
            else if section == 2 {
                return totalCatcherArray.count
            }
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 {
            
            if section == 0 {
                return totalOutFieldersArray.count
            }
            else if section == 1 {
                return totalCatcherArray.count
            }
        }
        else if totalOutFieldersArray.count > 0 && totalInFieldersArray.count > 0 {
            
            if section == 0 {
                return totalOutFieldersArray.count
            }
            else if section == 1 {
                return totalInFieldersArray.count
            }
        }
        else if totalOutFieldersArray.count > 0 && totalPitcherArray.count > 0 {
            if section == 0 {
                return totalOutFieldersArray.count
            }
            else if section == 1 {
                return totalPitcherArray.count
            }
            
        }
        else if totalCatcherArray.count > 0 && totalInFieldersArray.count > 0{
            if section == 0 {
                return totalInFieldersArray.count
            }
            else if section == 1 {
                return totalCatcherArray.count
            }
        }
        else if totalCatcherArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalPitcherArray.count
            }
            else if section == 1 {
                return totalCatcherArray.count

            }
        }
        else if totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalInFieldersArray.count
            }
            else if section == 1 {
                return totalPitcherArray.count
            }
        }
        else if totalOutFieldersArray.count > 0{
            return totalOutFieldersArray.count
        }
        else if totalCatcherArray.count > 0{
            return totalCatcherArray.count
        }
        else if totalInFieldersArray.count > 0{
            return totalInFieldersArray.count
        }
        else if totalPitcherArray.count > 0{
            return totalPitcherArray.count
        }
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "FullFantasyTableViewCell") as? FullFantasyTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "FullFantasyTableViewCell") as? FullFantasyTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.contentView.backgroundColor = UIColor.white
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        cell?.configData(details: playerInfo, legueType: FantasyType.Classic.rawValue, gameType: GameType.Football.rawValue)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        let teamScoresView = BaseballPlayerPointsView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
        teamScoresView.configData(details: playerInfo)
        APPDELEGATE.window!.addSubview(teamScoresView)
        
        DispatchQueue.main.async {
            teamScoresView.showAnimation()
        }

    }
    
    func getPlayerType(section: Int) -> String {
        if (totalOutFieldersArray.count > 0) && totalCatcherArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                if totalOutFieldersArray.count > 1{
                    return "Outfielder".localized()
                }
                return "Outfielder".localized()
            }
            else if section == 1 {
                if totalInFieldersArray.count > 1{
                    return "Infielder".localized()
                }
                
                return "Infielder".localized()
            }
            else if section == 2 {
                return "Pitcher".localized()
            }
            else if section == 3 {
                return "Catcher".localized()
            }
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 && totalInFieldersArray.count > 0{
            if section == 0 {
                if totalOutFieldersArray.count > 1{
                    return "Outfielder".localized()
                }
                return "Outfielder".localized()
            }
            else if section == 1 {
                if totalInFieldersArray.count > 1{
                    return "Infielder".localized()
                }
                return "Infielder".localized()
            }
            else if section == 2 {
                if totalCatcherArray.count > 1{
                    return "Catcher".localized()
                }
                return "Catcher".localized()
            }
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                if totalOutFieldersArray.count > 1{
                    return "Outfielder".localized()
                }
                return "Outfielder".localized()
            }
            else if section == 1 {
                if totalInFieldersArray.count > 1{
                    return "Infielder".localized()
                }
                
                return "Infielder".localized()
            }
            else if section == 2 {
                if totalCatcherArray.count > 1{
                    return "Catcher".localized()
                }
                return "Catcher".localized()
            }
        }
        else if totalOutFieldersArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                if totalOutFieldersArray.count > 1{
                    return "Outfielder".localized()
                }
                return "Outfielder".localized()
            }
            else if section == 1 {
                if totalInFieldersArray.count > 1{
                    return "Infielder".localized()
                }
                
                return "Infielder".localized()
            }
            else if section == 2 {
                if totalPitcherArray.count > 1{
                    return "Pitcher".localized()
                }
                
                return "Pitcher".localized()
            }
        }
        else if totalCatcherArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                if totalInFieldersArray.count > 1{
                    return "Infielder".localized()
                }
                return "Infielder".localized()
            }
            else if section == 1 {
                if totalPitcherArray.count > 1{
                    return "Pitcher".localized()
                }
                return "Pitcher".localized()
            }
            else if section == 2 {
                if totalCatcherArray.count > 1{
                    return "Catcher".localized()
                }
                
                return "Catcher".localized()
            }

        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 {
            
            if section == 0 {
                if totalOutFieldersArray.count > 1{
                    return "Outfielder".localized()
                }
                
                return "Outfielder".localized()
            }
            else if section == 1 {
                if totalCatcherArray.count > 1{
                    return "Catcher".localized()
                }
                
                return "Catcher".localized()
            }
        }
        else if totalOutFieldersArray.count > 0 && totalInFieldersArray.count > 0 {
            
            if section == 0 {
                if totalOutFieldersArray.count > 1{
                    return "Outfielder".localized()
                }
                
                return "Outfielder".localized()
            }
            else if section == 1 {
                if totalInFieldersArray.count > 1{
                    return "Infielder".localized()
                }
                return "Infielder".localized()
            }
        }
        else if totalOutFieldersArray.count > 0 && totalPitcherArray.count > 0 {
            if section == 0 {
                if totalOutFieldersArray.count > 1{
                    return "Outfielder".localized()
                }
                
                return "Outfielder".localized()
            }
            else if section == 1 {
                if totalPitcherArray.count > 1{
                    return "Pitcher".localized()
                }
                
                return "Pitcher".localized()
            }
        }
        else if totalCatcherArray.count > 0 && totalInFieldersArray.count > 0{
            if section == 0 {
                if totalInFieldersArray.count > 1{
                    return "Infielder".localized()
                }
                
                return "Infielder".localized()

            }
            else if section == 1 {
                if totalCatcherArray.count > 1{
                    return "Catcher".localized()
                }
                
                return "Catcher".localized()
            }
        }
        else if totalCatcherArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                if totalPitcherArray.count > 1{
                    return "Pitcher".localized()
                }
                return "Pitcher".localized()
            }
            else if section == 1 {
                if totalCatcherArray.count > 1{
                    return "Catcher".localized()
                }
                
                return "Catcher".localized()
            }

        }
        else if totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                if totalInFieldersArray.count > 1{
                    return "Infielder".localized()
                }
                
                return "Infielder".localized()
            }
            else if section == 1 {
                if totalPitcherArray.count > 1{
                    return "Pitcher".localized()
                }
                return "Pitcher".localized()
            }
        }
        else if totalOutFieldersArray.count > 0{
            if totalOutFieldersArray.count > 1{
                return "Outfielder".localized()
            }
            
            return "Outfielder".localized()
        }
        else if totalCatcherArray.count > 0{
            if totalCatcherArray.count > 1{
                return "Catcher".localized()
            }
            return "Catcher".localized()
        }
        else if totalInFieldersArray.count > 0{
            if totalInFieldersArray.count > 1{
                return "Infielder".localized()
            }
            return "Infielder".localized()
        }
        else if totalPitcherArray.count > 0{
            if totalPitcherArray.count > 1{
                return "Pitcher".localized()
            }
            return "Pitcher".localized()
        }
        return ""
    }
    
    
    func getPlayerDetails(section: Int, index: Int) -> PlayerDetails {
        if (totalOutFieldersArray.count > 0) && totalCatcherArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalOutFieldersArray[index]
            }
            else if section == 1 {
                return totalInFieldersArray[index]
            }
            else if section == 2 {
                return totalPitcherArray[index]
            }
            else if section == 3 {
                return totalCatcherArray[index]
            }
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 && totalInFieldersArray.count > 0{
            if section == 0 {
                return totalOutFieldersArray[index]
            }
            else if section == 1 {
                return totalInFieldersArray[index]
            }
            else if section == 2 {
                return totalCatcherArray[index]
            }
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalOutFieldersArray[index]
            }
            else if section == 1 {
                return totalPitcherArray[index]
            }
            else if section == 2 {
                return totalCatcherArray[index]
            }

        }
        else if totalOutFieldersArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalOutFieldersArray[index]
            }
            else if section == 1 {
                return totalInFieldersArray[index]
            }
            else if section == 2 {
                return totalPitcherArray[index]
            }
        }
        else if totalCatcherArray.count > 0 && totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalInFieldersArray[index]
            }
            else if section == 1 {
                return totalPitcherArray[index]
            }
            else if section == 2 {
                return totalCatcherArray[index]
            }
        }
        else if totalOutFieldersArray.count > 0 && totalCatcherArray.count > 0 {
            
            if section == 0 {
                return totalOutFieldersArray[index]
            }
            else if section == 1 {
                return totalCatcherArray[index]
            }
        }
        else if totalOutFieldersArray.count > 0 && totalInFieldersArray.count > 0 {
            
            if section == 0 {
                return totalOutFieldersArray[index]
            }
            else if section == 1 {
                return totalInFieldersArray[index]
            }
        }
        else if totalOutFieldersArray.count > 0 && totalPitcherArray.count > 0 {
            if section == 0 {
                return totalOutFieldersArray[index]
            }
            else if section == 1 {
                return totalPitcherArray[index]
            }
            
        }
        else if totalCatcherArray.count > 0 && totalInFieldersArray.count > 0{
            if section == 0 {
                return totalInFieldersArray[index]
            }
            else if section == 1 {
                return totalCatcherArray[index]
            }
        }
        else if totalCatcherArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalPitcherArray[index]
            }
            else if section == 1 {
                return totalCatcherArray[index]
            }
        }
        else if totalInFieldersArray.count > 0 && totalPitcherArray.count > 0{
            if section == 0 {
                return totalInFieldersArray[index]
            }
            else if section == 1 {
                return totalPitcherArray[index]
            }
        }
        else if totalOutFieldersArray.count > 0{
            return totalOutFieldersArray[index]
        }
        else if totalCatcherArray.count > 0{
            return totalCatcherArray[index]
        }
        else if totalInFieldersArray.count > 0{
            return totalInFieldersArray[index]
        }
        else if totalPitcherArray.count > 0{
            return totalPitcherArray[index]
        }
        return PlayerDetails()
    }
}
