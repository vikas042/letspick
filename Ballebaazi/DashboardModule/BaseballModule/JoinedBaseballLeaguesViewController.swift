//
//  JoinedBaseballLeaguesViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class JoinedBaseballLeaguesViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var scoreViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var annuncementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var annuncementView: AnnouncementView!
    @IBOutlet weak var scoreView: TeamScoreView!
    @IBOutlet weak var headerView: LetspickFantasyWithoutWalletHeaderView!
    @IBOutlet weak var lblMyTeamTitle: UILabel!
    @IBOutlet weak var myTeamButton: CustomBorderButton!
    @IBOutlet weak var privateLeagueButton: SolidButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    @IBOutlet weak var lblTeamCount: UILabel!
    @IBOutlet weak var placeholderimgView: UIImageView!
    @IBOutlet weak var tblViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var leagueCollectionView: UICollectionView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    lazy var isPullToRefresh = false
    lazy var isViewForLeagueFlow = false
    lazy var userTeamsArray:Array<UserTeamDetails> = []
    lazy var selectedRow = 0
    lazy var classicTeamCount = 0
    lazy var battingTeamCount = 0
    lazy var bowlingTeamCount = 0
    
    var matchDetails: MatchDetails?
    var timer: Timer?
    var joinedLeagueDetailsArray: Array<JoinedLeagueDetails>?
    var joinedClassicLeagueDetailsArray: Array<JoinedLeagueDetails>?
    
    // MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.isFantasyModeEnable = false
        setupDefaultProperties()
    }
    
    //MARK:- Custom Methods
    
    func setupDefaultProperties() {
        
        headerView.updateMatchName(details: matchDetails)
        classicButtonTapped(isNeedToAnimate: false)
        leagueCollectionView.register(UINib(nibName: "JoinedLeagueContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JoinedLeagueContainerCollectionViewCell")
        UserDefaults.standard.set(Date().timeIntervalSince1970 - 65, forKey: "lastRefreshedMatchDetails")
        UserDefaults.standard.synchronize()
        callGetJoinedLeagueAPI(isNeedToShowLoader: true)
        lblNoRecordsFound.text = "NoJoinedLeague".localized()
        headerView.backButtonTitle = "My League".localized();
        bottomView.isHidden = false
        if isViewForLeagueFlow{
            bottomView.isHidden = false
        }
        else{
            leagueCollectionView.layoutIfNeeded()
        }
        
        if matchDetails!.isMatchClosed {
            lblTeamCount.isHidden = false
            lblMyTeamTitle.isHidden = false
            privateLeagueButton.isHidden = true
            myTeamButton.isHidden = false
        }
        else{
            lblTeamCount.isHidden = true
            lblMyTeamTitle.isHidden = true
            privateLeagueButton.isHidden = false
            myTeamButton.isHidden = true
            scoreViewHeightConstraint.constant = 0;
            scoreView.isHidden = true
        }

        scoreView.layoutIfNeeded()
        updateTimerVlaue()
        
        if #available(iOS 10.0, *) {
            weak var weakSelf = self;
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        if joinedLeagueDetailsArray != nil{
            if joinedLeagueDetailsArray!.count > 0{
                weak var weakSelf = self;
                DispatchQueue.global().async {
                    weakSelf?.callGetJoinedLeagueAPI(isNeedToShowLoader: false)
                }
            }
        }
    }
        
    @objc func refereshRecordsInBackgrouns() {
        
    }
    
    @objc func removeLoader()  {
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.isPullToRefresh = false
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    // MARK:- -IBAction Methods
    
    func classicButtonTapped(isNeedToAnimate: Bool) {
        
        
        lblNoRecordsFound.isHidden = true
        placeholderimgView.isHidden = true
        if joinedClassicLeagueDetailsArray?.count == 0{
            lblNoRecordsFound.isHidden = false
            placeholderimgView.isHidden = false
            placeholderimgView.image = UIImage(named: "Placeholdermyleagueclassicmage")
        }
        
        self.lblTeamCount.text = String(self.classicTeamCount)
        
        if self.classicTeamCount > 1 {
            lblMyTeamTitle.text = "My Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "My Team".localized()
        }
    }
    
    @IBAction func myTeamButtonTapped(_ sender: Any) {
        
        if matchDetails!.isMatchClosed {
            let myTeamVC = storyboard?.instantiateViewController(withIdentifier: "MyBaseballTeamsViewController") as! MyBaseballTeamsViewController
            myTeamVC.matchDetails = matchDetails
            myTeamVC.userTeamsArray = userTeamsArray
            myTeamVC.isFromJoinedLeague = true
            navigationController?.pushViewController(myTeamVC, animated: true)
        }
        else{

            let createPrivateLeague = storyboard?.instantiateViewController(withIdentifier: "CreatePrivateLeagueViewController") as! CreatePrivateLeagueViewController
            createPrivateLeague.matchDetails = matchDetails
            createPrivateLeague.userTeamsArray = userTeamsArray
            createPrivateLeague.fantasyType = "1"
            createPrivateLeague.gameType = GameType.Baseball.rawValue
            navigationController?.pushViewController(createPrivateLeague, animated: true)
        }
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60;
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let blankView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        blankView.backgroundColor = UIColor.clear
        return blankView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = joinedClassicLeagueDetailsArray?.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "JoinedLeagueTableViewCell") as? JoinedLeagueTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "JoinedLeagueTableViewCell") as? JoinedLeagueTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        let count = indexPath.row
        cell?.viewRankingButton.tag = count
        cell?.viewRankingButton.addTarget(self, action: #selector(viewRankingButtonTapped(button:)), for: .touchUpInside)
//        cell?.myTeamButton.tag = count
//        cell?.myTeamButton.addTarget(self, action: #selector(viewRankingButtonTapped(button:)), for: .touchUpInside)
        let details = joinedClassicLeagueDetailsArray![count]
        
        if (matchDetails!.isMatchClosed)  {
            cell?.lblTeamCount.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.totalJoined ?? "0"))
        }
        else{
            if details.isInfinity == "1"{
                cell?.lblTeamCount.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.totalJoined ?? "0")) + "/" + "∞"
            }
            else{
                cell?.lblTeamCount.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.totalJoined ?? "0")) + "/" + AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.maxPlayers ?? "0"))
            }
        }
        
        cell?.configData(details: details, matchDetails: matchDetails!, teamsArray: userTeamsArray, gameType: GameType.Baseball.rawValue)
        
        return cell!
    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        var cell = tableView.dequeueReusableCell(withIdentifier: "FootballJoinedLeagueTableViewCell") as? FootballJoinedLeagueTableViewCell
//        if cell == nil {
//            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "FootballJoinedLeagueTableViewCell") as? FootballJoinedLeagueTableViewCell
//        }
//
//        cell?.selectionStyle = UITableViewCellSelectionStyle.none
//        let count = indexPath.row
//        cell?.viewRankingButton.tag = count
//        cell?.viewRankingButton.addTarget(self, action: #selector(viewRankingButtonTapped(button:)), for: .touchUpInside)
//        cell?.myTeamButton.tag = count
//        cell?.myTeamButton.addTarget(self, action: #selector(viewRankingButtonTapped(button:)), for: .touchUpInside)
//        let details = joinedClassicLeagueDetailsArray![count]
//
//        if (matchDetails!.isMatchClosed)  {
//            cell?.lblTeamCount.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.totalJoined ?? "0"))
//        }
//        else{
//            if details.isInfinity == "1"{
//                cell?.lblTeamCount.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.totalJoined ?? "0")) + "/" + "∞"
//            }
//            else{
//                cell?.lblTeamCount.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.totalJoined ?? "0")) + "/" + AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.maxPlayers ?? "0"))
//            }
//        }
//
//        cell?.configData(details: details, matchDetails: matchDetails!, teamsArray: userTeamsArray, gameType: GameType.Baseball.rawValue)
//
//        return cell!
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var details: JoinedLeagueDetails?
        let count = indexPath.row
        let joinedLeagueTeamVC = storyboard?.instantiateViewController(withIdentifier: "JoinedBaseballLeaguesTeamsViewController") as! JoinedBaseballLeaguesTeamsViewController
        
        details = joinedClassicLeagueDetailsArray![count]
        let classicTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
            teamDetails.fantasyType == "1"
        })
        joinedLeagueTeamVC.userTeamsArray = classicTeamArray
        
        guard let leagueDetails = details else {
            return;
        }
        
        if leagueDetails.isPrivateLeague  && !leagueDetails.isPrivateLeagueJoined{
            return;
        }
        
        if leagueDetails.isPrivateLeague && !leagueDetails.isPrivateLeagueJoined && !matchDetails!.isMatchClosed && (leagueDetails.totalJoined != leagueDetails.maxPlayers){
            
            let privateLeagueDetails = LeagueDetails()
            privateLeagueDetails.leagueId = leagueDetails.leagueId!
            privateLeagueDetails.joiningAmount = leagueDetails.joiningAmount
            privateLeagueDetails.winAmount = leagueDetails.winAmount
            privateLeagueDetails.winAmount = leagueDetails.winAmount
            privateLeagueDetails.fantasyType = leagueDetails.fantasyType!
            privateLeagueDetails.confirmedLeague = leagueDetails.confirmedLeague
            privateLeagueDetails.matchKey = matchDetails!.matchKey
            privateLeagueDetails.teamType = "2"
            privateLeagueDetails.bonusApplicable = "1"
            privateLeagueDetails.isPrivateLeague = true
            callLeagueValidationAPI(leagueDetails: privateLeagueDetails, categoryName: kPrivateLeague)
            return;
        }
        else{
            joinedLeagueTeamVC.joinedLeagueDetails = details
            joinedLeagueTeamVC.selectedMatchDetails = matchDetails
            navigationController?.pushViewController(joinedLeagueTeamVC, animated: true)
        }
    }
    
    @objc func viewRankingButtonTapped(button: UIButton){
        
        let count = button.tag
        let joinedLeagueTeamVC = storyboard?.instantiateViewController(withIdentifier: "JoinedBaseballLeaguesTeamsViewController") as! JoinedBaseballLeaguesTeamsViewController
        
        let leagueDetails = joinedClassicLeagueDetailsArray![count]
        let classicTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
            teamDetails.fantasyType == "1"
        })
        joinedLeagueTeamVC.userTeamsArray = classicTeamArray
        
        if leagueDetails.isPrivateLeague && !leagueDetails.isPrivateLeagueJoined && !matchDetails!.isMatchClosed && (leagueDetails.totalJoined != leagueDetails.maxPlayers){
            
            let privateLeagueDetails = LeagueDetails()
            privateLeagueDetails.leagueId = leagueDetails.leagueId!
            privateLeagueDetails.joiningAmount = leagueDetails.joiningAmount
            privateLeagueDetails.winAmount = leagueDetails.winAmount
            privateLeagueDetails.winAmount = leagueDetails.winAmount
            privateLeagueDetails.fantasyType = leagueDetails.fantasyType!
            privateLeagueDetails.confirmedLeague = leagueDetails.confirmedLeague
            privateLeagueDetails.matchKey = matchDetails!.matchKey
            privateLeagueDetails.teamType = "2"
            privateLeagueDetails.bonusApplicable = "1"
            privateLeagueDetails.isPrivateLeague = true
            callLeagueValidationAPI(leagueDetails: privateLeagueDetails, categoryName: kPrivateLeague)
            return;
        }
        else if leagueDetails.isPrivateLeague && leagueDetails.isPrivateLeagueJoined && !matchDetails!.isMatchClosed {
            
            var userName = UserDetails.sharedInstance.name
            if userName.count == 0 {
                userName = UserDetails.sharedInstance.userName
            }
            
            
            let text = String(format: "%@ has invited you to join the private league %@ on Letspick. Use code %@ to join.\nhttps://Letspick.app.link?league_code=%@", userName, (leagueDetails.leagueName ?? ""), leagueDetails.leagueCode, leagueDetails.leagueCode)
            
            // set up activity view controller
            let textToShare = [ text ]
            let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
            
            // present the view controller
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.present(activityViewController, animated: true, completion: nil)
            }
        }
        else{
            joinedLeagueTeamVC.joinedLeagueDetails = leagueDetails
            joinedLeagueTeamVC.selectedMatchDetails = matchDetails
            navigationController?.pushViewController(joinedLeagueTeamVC, animated: true)
        }
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height-8)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JoinedLeagueContainerCollectionViewCell", for: indexPath) as? JoinedLeagueContainerCollectionViewCell
        
        cell!.tblView.register(UINib(nibName: "FootballJoinedLeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "FootballJoinedLeagueTableViewCell")
        cell!.tblView.register(UINib(nibName: "JoinedLeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "JoinedLeagueTableViewCell")

        cell!.tblView.register(UINib(nibName: "CreatePrivateLeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "CreatePrivateLeagueTableViewCell")
        cell!.tblView.estimatedRowHeight = 70;
        cell!.tblView.rowHeight = UITableViewAutomaticDimension
        cell!.tblView.delegate = self
        cell!.tblView.dataSource = self
        cell!.tblView.reloadData()
        
        return cell!;
    }
    
    //MARK:-  API Releated Method
    func callGetJoinedLeagueAPI(isNeedToShowLoader: Bool){
        
        if matchDetails?.matchStatus == "started"  {
            let lastRefreshedMatchDetails: TimeInterval  = UserDefaults.standard.value(forKey: "lastRefreshedMatchDetails") as? TimeInterval ?? Date().timeIntervalSince1970
            let currentTimetemp = Date().timeIntervalSince1970
            
            let timeDefference = currentTimetemp - lastRefreshedMatchDetails
            
            if timeDefference < 60 {
                AppHelper.sharedInstance.displaySpinner()
                let delayInSec = 3.0
                weak var weakSelf = self
                DispatchQueue.main.asyncAfter(deadline: .now() + delayInSec) {
                    weakSelf?.removeLoader()
                }
                return;
            }
        }
        
        
        if isNeedToShowLoader{
            if !AppHelper.isInterNetConnectionAvailable(){
                isPullToRefresh = false
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let parameters = ["option": "get_contest", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kBaseballSocresUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            DispatchQueue.main.async {
                if weakSelf?.matchDetails?.matchStatus == "started"  {
                    UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "lastRefreshedMatchDetails")
                    UserDefaults.standard.synchronize()
                }
                
                weakSelf?.isPullToRefresh = false
                if result != nil{
                    
                    let statusCode = result!["status"]?.string
                    if statusCode == "200"{
                        
                        if let response = result!["response"]?.dictionary{
                            if let scoreDetailsString = response["match_innings"]?.string{
                                
                                var firstTeamName = ""
                                var firstTeamScore = ""
                                var secondTeamName = ""
                                var secondTeamScore = ""
                                var thirdTeamScore = ""
                                var fourthTeamScore = ""
                                
                                var firstTeamOver = ""
                                var secondTeamOver = ""
                                var thirdTeamOver = ""
                                var fourthTeamOver = ""
                                
                                let json = JSON(parseJSON: scoreDetailsString)
                                if (json.array != nil){
                                    let scoreDetailsArray = json.array!
                                    for index in 0 ..< scoreDetailsArray.count{
                                        let scoreDetails = scoreDetailsArray[index]
                                        
                                        if index == 3{
                                            let teamName = scoreDetails["team_short_name"].string ?? ""
                                            let run = scoreDetails["runs"].stringValue
                                            let wicket = scoreDetails["wickets"].stringValue
                                            let overs = scoreDetails["overs"].stringValue
                                            //                                            let teamScore = run + "/" + wicket + "(" + overs + " ov)"
                                            let teamScore = run + "-" + wicket
                                            fourthTeamOver = overs
                                            if teamName == firstTeamName{
                                                thirdTeamScore = teamScore
                                            }
                                            else{
                                                fourthTeamScore = teamScore
                                            }
                                            
                                            weakSelf?.matchDetails?.isViewForTestMatch = true
                                        }
                                        else if index == 2{
                                            let teamName = scoreDetails["team_short_name"].string ?? ""
                                            let run = scoreDetails["runs"].stringValue
                                            let wicket = scoreDetails["wickets"].stringValue
                                            let overs = scoreDetails["overs"].stringValue
                                            //                                            let teamScore = run + "/" + wicket + "(" + overs + " ov)"
                                            let teamScore = run + "-" + wicket
                                            thirdTeamOver = overs
                                            
                                            if teamName == firstTeamName{
                                                thirdTeamScore = teamScore
                                            }
                                            else{
                                                fourthTeamScore = teamScore
                                            }
                                            weakSelf?.matchDetails?.isViewForTestMatch = true
                                            weakSelf?.headerView?.layoutIfNeeded()
                                        }
                                        else if index == 1{
                                            firstTeamName = scoreDetails["team_short_name"].string ?? ""
                                            let run = scoreDetails["runs"].stringValue
                                            let wicket = scoreDetails["wickets"].stringValue
                                            let overs = scoreDetails["overs"].stringValue
                                            //                                            firstTeamScore = run + "/" + wicket + "(" + overs + " ov)"
                                            //                                            firstTeamScore = run + "/" + wicket + "(" + overs + " ov)"
                                            firstTeamScore = run + "-" + wicket
                                            firstTeamOver = overs
                                        }
                                        else if index == 0{
                                            let run = scoreDetails["runs"].stringValue
                                            let wicket = scoreDetails["wickets"].stringValue
                                            let overs = scoreDetails["overs"].stringValue
                                            if  Double(overs)! > 0.0 {
                                                secondTeamName = scoreDetails["team_short_name"].string ?? ""
                                                //                                                secondTeamScore = run + "/" + wicket + "(" + overs + " ov)"
                                                secondTeamScore = run + "-" + wicket
                                                secondTeamOver = overs
                                            }
                                        }
                                    }
                                    
                                    weakSelf?.matchDetails?.firstTeamOver = firstTeamOver
                                    weakSelf?.matchDetails?.secondTeamOver = secondTeamOver
                                    weakSelf?.matchDetails?.thirdTeamOver = thirdTeamOver
                                    weakSelf?.matchDetails?.fourthTeamOver = fourthTeamOver
                                    weakSelf?.matchDetails?.thirdTeamLiveScore = thirdTeamScore
                                    weakSelf?.matchDetails?.fourthTeamLiveScore = fourthTeamScore
                                    weakSelf?.matchDetails?.firstTeamLiveScoreShortName = firstTeamName
                                    weakSelf?.matchDetails?.secondTeamLiveScoreShortName = secondTeamName
                                    weakSelf?.matchDetails?.thirdTeamLiveScore = thirdTeamScore
                                    weakSelf?.matchDetails?.fourthTeamLiveScore = fourthTeamScore
                                    
                                    weakSelf?.matchDetails?.firstTeamLiveScore = firstTeamScore
                                    weakSelf?.matchDetails?.secondTeamLiveScore = secondTeamScore
                                    weakSelf?.scoreView.showTeamScore(details: self.matchDetails!)
                                }
                            }
                        }
                        self.showScoreView()
                        weakSelf?.joinedLeagueDetailsArray = JoinedLeagueDetails.getJoinedLeageList(result: result!)
                        weakSelf?.userTeamsArray = JoinedLeagueDetails.getUserTeamsList(result: result!, matchDetails: weakSelf!.matchDetails!)
                        
                        if weakSelf!.isViewForLeagueFlow{
                            weakSelf!.classicTeamCount = weakSelf!.userTeamsArray.count
                            weakSelf!.lblTeamCount.text = String(weakSelf!.classicTeamCount)
                        }
                        
                        weakSelf?.joinedClassicLeagueDetailsArray = weakSelf?.joinedLeagueDetailsArray?.filter({ (details) -> Bool in
                            details.fantasyType == "1"
                        })
                        
                        weakSelf?.leagueCollectionView.reloadData()
                        AppHelper.sharedInstance.removeSpinner()
                        self.classicButtonTapped(isNeedToAnimate: false)
                    }
                    else{
                        self.placeholderimgView.isHidden = false
                        self.lblNoRecordsFound.isHidden = false
                        AppHelper.sharedInstance.removeSpinner()
                        let message = result!["message"]?.string ?? "kErrorMsg".localized()
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
                else{
                    self.placeholderimgView.isHidden = false
                    self.lblNoRecordsFound.isHidden = false
                    AppHelper.sharedInstance.removeSpinner()
                    AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                }
            }
        }
    }
    
    //MARK:- ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if !isPullToRefresh{
            if scrollView.contentOffset.y < -120{
                isPullToRefresh = true
                callGetJoinedLeagueAPI(isNeedToShowLoader: true)
            }
        }
    }
    
    //MARK:- Timer Handler
    func startRecordsRefershTimer() {
        
        if (self.matchDetails!.isMatchClosed) && (self.matchDetails?.matchStatus == "notstarted"){
            self.updateTimerVlaue()
        }
        else{
            self.refereshRecordsInBackgrouns()
        }
    }
    
    @objc func updateTimerVlaue(){
        
        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(details: matchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            if matchDetails?.active != "4" {
                if remainingTime <= 0{
                    matchDetails!.isMatchClosed = true
                    callGetJoinedLeagueAPI(isNeedToShowLoader: true)
                }
            }
        }
    }
    
    
    
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails, categoryName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league_preview_v1","check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType, "is_private": "1"]
        //        isUserValidatingToJoinLeague = true
        
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kBaseballMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                                        
//                                        let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = weakSelf!.matchDetails
//                                        addCashVC?.userTeamArray = teamArray
//                                        addCashVC?.categoryName = categoryName
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                    playerVC.matchDetails = weakSelf?.matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails, userTeamArray: teamArray, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                playerVC.matchDetails = weakSelf?.matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            }
                        }
                        else{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, ticketDetails: TicketDetails?)  {
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let joinedLeagueConfirmVC = storyboard.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
            joinedLeagueConfirmVC?.leagueDetails = leagueDetails
            joinedLeagueConfirmVC?.ticketDetails = ticketDetails

            joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
            joinedLeagueConfirmVC?.matchDetails = self.matchDetails
            joinedLeagueConfirmVC?.isNeedToShowMatchClosePopup = true
            joinedLeagueConfirmVC?.selectedGameType = GameType.Baseball.rawValue
            joinedLeagueConfirmVC?.leagueCategoryName = kPrivateLeague
            navigationVC.pushViewController(joinedLeagueConfirmVC!, animated: true)
        }
    }
    
    func showScoreView() {
        if (matchDetails?.firstTeamLiveScore.count != 0) && (matchDetails?.secondTeamLiveScore.count != 0) && (matchDetails?.thirdTeamLiveScore.count != 0){
            self.scoreViewHeightConstraint.constant = 110
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        else  if (matchDetails?.firstTeamLiveScore.count != 0) && (matchDetails?.secondTeamLiveScore.count != 0) && (matchDetails?.fourthTeamLiveScore.count != 0){
            self.scoreViewHeightConstraint.constant = 110
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        else if (matchDetails?.firstTeamLiveScoreShortName.count == 0) && (matchDetails?.secondTeamLiveScoreShortName.count == 0){
            self.scoreViewHeightConstraint.constant = 0
            self.scoreView.isHidden = true
            self.scoreView.layoutIfNeeded()
        }
        else{
            self.scoreViewHeightConstraint.constant = 50.0
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
