//
//  SelectBaseballPlayersViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 06/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class SelectBaseballPlayersViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var outfieldersIcon: UIImageView!
    @IBOutlet weak var pitcherIcon: UIImageView!
    @IBOutlet weak var catcherIcon: UIImageView!
    @IBOutlet weak var infielderIcon: UIImageView!
    
    @IBOutlet weak var lblOutfieldersTitle: UILabel!
    @IBOutlet weak var lblOutfieldersCount: UILabel!
    @IBOutlet weak var lblCatcherTitle: UILabel!
    @IBOutlet weak var lblCatcherCount: UILabel!
    @IBOutlet weak var lblPitcherTitle: UILabel!
    @IBOutlet weak var lblPitcherCount: UILabel!
    @IBOutlet weak var lblInfielderTitle: UILabel!
    @IBOutlet weak var lblInfielderCount: UILabel!
    
    @IBOutlet weak var headerView: LetspickCreateTeamHeaderViewNew!
    @IBOutlet weak var sliderLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var pointsButton: UIButton!
    @IBOutlet weak var creditButton: UIButton!
    @IBOutlet weak var secondTeamButton: UIButton!
    @IBOutlet weak var firstTeamButton: UIButton!
    @IBOutlet weak var allButton: UIButton!

    @IBOutlet weak var previewButton: CustomBorderButton!
    @IBOutlet weak var nextButton: SolidButton!

    @IBOutlet weak var upperView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottonView: UIView!
    @IBOutlet weak var lblPickMessage: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var inOutButton: UIButton!

    
    lazy var selectedPlayerType = PlayerType.Outfielders.rawValue
    var totalPlayerArray: Array<PlayerDetails>?
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var userTeamDetails: UserTeamDetails?
    var timer: Timer?
    lazy var isEditPlayerTeam = false
    lazy var isPlaying22 = ""
    lazy var totalOutFieldersArray = Array<PlayerDetails>()
    lazy var totalPitcherArray = Array<PlayerDetails>()
    lazy var totalCatcherArray = Array<PlayerDetails>()
    lazy var totalInFieldersArray = Array<PlayerDetails>()
    
    lazy var isMatchClosingTimeRefereshing = false
    lazy var playerSortedType = false
    lazy var teamSortedType = false
    lazy var pointsSortedType = true
    lazy var creditsSortedType = true
    lazy var isComeFromEditPlayerTeamScreen = false
    lazy var isPlayerPlaying = false
    var ticketDetails: TicketDetails?
    
    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.isViewForSelectCaptain = false
        setupDefaultProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails!.fantasyType, gameType: GameType.Baseball.rawValue)
        inOutButton.isHidden = true

        
        if self.selectedPlayerType == PlayerType.Pitcher.rawValue{
            self.pitcherButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.Catcher.rawValue{
            self.catcherButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.Infielder.rawValue{
            self.infielderButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.Outfielders.rawValue{
            self.outfieldersButtonTapped(nil)
            
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerListContainerCollectionViewCell", for: indexPath) as? PlayerListContainerCollectionViewCell
        var playerList: Array<PlayerDetails>?
        
        if indexPath.row == 0 {
            playerList = totalOutFieldersArray
        }
        else if indexPath.row == 1 {
            playerList = totalCatcherArray
        }
        else if indexPath.row == 2 {
            playerList = totalPitcherArray
        }
        else if indexPath.row == 3 {
            playerList = totalInFieldersArray
        }
        
        if leagueDetails?.fantasyType == "1" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Classic.rawValue, gameType: GameType.Baseball.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        else if leagueDetails?.fantasyType == "2" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Batting.rawValue, gameType: GameType.Baseball.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        else if leagueDetails?.fantasyType == "3" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Bowling.rawValue, gameType: GameType.Baseball.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        
        return cell!;
    }
    
    // MARK:- API Related Method
    
    func callGetPlayerListAPI(urlString: String, isNeedToShowLoader: Bool)  {
        
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        
        weak var waekSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
                    DispatchQueue.main.async {
                        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
                        let response = savedResponse?.dictionary!["response"]
                        if let flagDict = response?.dictionary!["team_flags"]{
                            UserDetails.sharedInstance.teamFlag = flagDict;
                        }
                        
                        if let selectedmatch = response!.dictionary!["selected_match"]?.dictionary{
                            waekSelf?.isPlaying22 = selectedmatch["show_playing22"]?.string ?? "0"
                            if waekSelf?.isPlaying22 != "1"{
                                waekSelf?.inOutButton.isHidden = true
                            }
                            else{
                                waekSelf?.inOutButton.isHidden = false
                            }
                            
                            if waekSelf?.matchDetails?.firstTeamShortName?.count == 0{
                                let teamFirstName = selectedmatch["team_a_short_name"]?.string ?? ""
                                let teamSecondShortName = selectedmatch["team_b_short_name"]?.string ?? ""
                                waekSelf?.matchDetails?.firstTeamShortName = teamFirstName
                                waekSelf?.matchDetails?.secondTeamShortName = teamSecondShortName
                                waekSelf?.firstTeamButton.setTitle(teamFirstName, for: .normal)
                                waekSelf?.secondTeamButton.setTitle(teamSecondShortName, for: .normal)
                                
                                if let firstTeamImageName = selectedmatch["team_a_flag"]?.string{
                                    waekSelf?.matchDetails?.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                                }
                                   
                                if let secondTeamImageName = selectedmatch["team_b_flag"]?.string{
                                    waekSelf?.matchDetails?.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                                }

                                if let startTime = selectedmatch["start_date_unix"]?.string{
                                    var closingTime = selectedmatch["closing_ts"]?.intValue ?? 0
                                    if closingTime == 0{
                                        closingTime = UserDetails.sharedInstance.closingTimeForMatch
                                    }
                                    let calcultedTime = Int(startTime)! - closingTime
                                    waekSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                                }

                            }

                        }
                        
                        let playerList = response?.dictionary!["match_players"]?.array
                        waekSelf?.totalPlayerArray = PlayerDetails.getPlayerDetailsArray(responseArray: playerList!, matchDetails: self.matchDetails)
                        if waekSelf?.matchDetails?.playersGender == "F"{
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: waekSelf!.totalPlayerArray!)
                        }
                        if waekSelf?.matchDetails?.isMatchTourney ?? false{
                            waekSelf?.allButton.isHidden = true
                            waekSelf?.firstTeamButton.isHidden = true
                            waekSelf?.secondTeamButton.isHidden = true
                        }
                        waekSelf?.modifySelectedPlayerDetails()
                        waekSelf?.updateTimerVlaue()
                        
                        waekSelf?.totalOutFieldersArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 0)
                        waekSelf?.totalCatcherArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 3)
                        waekSelf?.totalPitcherArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 2)
                        waekSelf?.totalInFieldersArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 1)
                        waekSelf?.collectionView.reloadData()
                        waekSelf?.upperView.isHidden = false
                        waekSelf?.bottonView.isHidden = false
                        waekSelf?.collectionView.isHidden = false
                        waekSelf?.inOutButtonTapped(nil)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    //MARK:- Custom Methods
    
    func setupDefaultProperties() {
        
        lblOutfieldersTitle.text = "OF".localized()
        lblCatcherTitle.text = "CAT".localized()
        lblPitcherTitle.text = "PIT".localized()
        lblInfielderTitle.text = "IF".localized()
        
        previewButton.setTitle("TeamPreview".localized(), for: .normal)
        nextButton.setTitle("Next".localized(), for: .normal)
        previewButton.setTitle("TeamPreview".localized(), for: .selected)
        nextButton.setTitle("Next".localized(), for: .selected)
        pointsButton.setTitle("Points".localized(), for: .normal)
        pointsButton.setTitle("Points".localized(), for: .selected)
        
        creditButton.setTitle("Credits".localized(), for: .normal)
        creditButton.setTitle("Credits".localized(), for: .selected)
        
        lblOutfieldersCount.text = "0"
        lblInfielderCount.text = "0"
        lblPitcherCount.text = "0"
        lblCatcherCount.text = "0"
        upperView.isHidden = true
        bottonView.isHidden = true
        collectionView.isHidden = true
        
        if matchDetails?.isMatchTourney ?? false{
            allButton.isHidden = true
            firstTeamButton.isHidden = true
            secondTeamButton.isHidden = true
        }

        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        
        firstTeamButton.setTitle(matchDetails!.firstTeamShortName, for: .normal)
        secondTeamButton.setTitle(matchDetails!.secondTeamShortName, for: .normal)
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails!.fantasyType, gameType: GameType.Baseball.rawValue)
        
        selectedPlayerType = PlayerType.Outfielders.rawValue
        AppHelper.designBottomTabDesing(bottonView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerSelectionUpdateNotification), name: NSNotification.Name(rawValue: "PlayerSelectionUpdateNotification"), object: nil)
        
        collectionView.register(UINib(nibName: "PlayerListContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayerListContainerCollectionViewCell")
        
        let urlString = kBaseballMatchURL + "?option=match_players&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.callGetPlayerListAPI(urlString: urlString, isNeedToShowLoader: true)
        }
        
        playerSelectionUpdateNotification()
        
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        for selectedDetails in UserDetails.sharedInstance.selectedPlayerList{
            for details in totalPlayerArray!{
                
                if details.playerKey == selectedDetails.playerKey{
                    details.isSelected = selectedDetails.isSelected
                    selectedDetails.playerPlayingRole = details.playerPlayingRole
                    selectedDetails.bowlingPoints = details.bowlingPoints
                    selectedDetails.isPlayerPlaying = details.isPlayerPlaying
                    selectedDetails.battingPoints = details.battingPoints
                    selectedDetails.classicPoints = details.classicPoints
                    selectedDetails.teamShortName = details.teamShortName
                    break;
                }
            }
        }
        
        if selectedRow == 0{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Outfielders.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Catcher.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Pitcher.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Infielder.rawValue
            })
        }
        if playersArray != nil{
            return playersArray!
        }
        return []
    }
    
    @objc func playerSelectionUpdateNotification()  {
        
        headerView.updatePlayerSelectionData(matchDetails: matchDetails!, fantasyType: leagueDetails!.fantasyType)
        if leagueDetails?.fantasyType == "1" {
            if UserDetails.sharedInstance.selectedPlayerList.count == 9 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }

        getSelectedPlayerCounts()
    }

    
    func getSelectedPlayerCounts(){
        
        let totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        if totalPlayerArray.count == 1 {
            AppxorEventHandler.logAppEvent(withName: "AddPlayer", info: ["SportType": "Baseball"])
        }

        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Outfielders.rawValue
        })
        
        let MidFielderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Pitcher.rawValue
        })
        
        let DefenderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Catcher.rawValue
        })
        
        let ForwardPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Infielder.rawValue
        })
        
        
        lblOutfieldersCount.text = "(" + String(wicketPlayersArray.count) + ")"
        lblInfielderCount.text = "(" + String(ForwardPlayerArray.count) + ")"
        lblPitcherCount.text = "(" + String(MidFielderPlayersArray.count) + ")"
        lblCatcherCount.text = "(" + String(DefenderPlayersArray.count) + ")"
    }
    
    func modifySelectedPlayerDetails()  {
        
        if isEditPlayerTeam{
            for selectedPlayermDetails in UserDetails.sharedInstance.selectedPlayerList{
                for playermDetails in totalPlayerArray!{
                    if selectedPlayermDetails.playerKey == playermDetails.playerKey{
                        
                        selectedPlayermDetails.bowlingPoints = playermDetails.bowlingPoints
                        selectedPlayermDetails.battingPoints = playermDetails.battingPoints
                        selectedPlayermDetails.classicPoints = playermDetails.classicPoints
                        selectedPlayermDetails.teamShortName = playermDetails.teamShortName
                        break
                    }
                }
            }
        }
        playerSelectionUpdateNotification()
    }
    
    //MARK:- -IBAction Methods
    @IBAction func outfieldersButtonTapped(_ sender: Any?) {
        
        outfieldersSelected()
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func catcherButtonTapped(_ sender: Any?) {
        
        catcherSelected()
        let indexPath = IndexPath(item: 3, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func pitcherButtonTapped(_ sender: Any?) {
        
        pitcherSelected()
        let indexPath = IndexPath(item: 2, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    
    @IBAction func infielderButtonTapped(_ sender: Any?) {
        
        infielderSelected()
        
        let indexPath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    func outfieldersSelected()  {
        lblPickMessage.text = "Pick 2-5 Outfielder".localized()

        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Baseball", "Type": "GK"])

        selectedPlayerType = PlayerType.Outfielders.rawValue
        outfieldersIcon.image = UIImage(named: "GoalkeeperSelected")
        pitcherIcon.image = UIImage(named: "MidfielderUnselected")
        infielderIcon.image = UIImage(named: "ForwardUnselected")
        catcherIcon.image = UIImage(named: "FootDefenderUnselected")
        
        lblOutfieldersTitle.textColor = kPlayerTypeSelectedColor
        lblPitcherTitle.textColor = kPlayerTypeUnselectedColor
        lblCatcherTitle.textColor = kPlayerTypeUnselectedColor
        lblInfielderTitle.textColor = kPlayerTypeUnselectedColor
        
        lblOutfieldersCount.textColor = kPlayerTypeSelectedColor
        lblPitcherCount.textColor = kPlayerTypeUnselectedColor
        lblCatcherCount.textColor = kPlayerTypeUnselectedColor
        lblInfielderCount.textColor = kPlayerTypeUnselectedColor
        
        sliderLeadingConstraint.constant = 0.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    func infielderSelected() {

      lblPickMessage.text = "Pick 2-5 Infielder".localized()
      selectedPlayerType = PlayerType.Infielder.rawValue
      AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Baseball", "Type": "IF"])

      infielderIcon.layer.borderColor = kPlayerSelectionMaxLimitColor.cgColor
      lblOutfieldersTitle.textColor = kPlayerTypeUnselectedColor
      lblPitcherTitle.textColor = kPlayerTypeUnselectedColor
      lblCatcherTitle.textColor = kPlayerTypeUnselectedColor
      lblInfielderTitle.textColor = kPlayerTypeSelectedColor
      
      lblOutfieldersCount.textColor = kPlayerTypeUnselectedColor
      lblPitcherCount.textColor = kPlayerTypeUnselectedColor
      lblCatcherCount.textColor = kPlayerTypeUnselectedColor
      lblInfielderCount.textColor = kPlayerTypeSelectedColor
      
      outfieldersIcon.image = UIImage(named: "GoalkeeperUnselected")
      pitcherIcon.image = UIImage(named: "MidfielderUnselected")
      infielderIcon.image = UIImage(named: "ForwardSelected")
      catcherIcon.image = UIImage(named: "FootDefenderUnselected")
      
      sliderLeadingConstraint.constant = infielderIcon.frame.origin.x - 9.0
      UIView.animate(withDuration: 0.2) {
          self.upperView.layoutIfNeeded()
      }
    }
    
    func pitcherSelected()  {

        lblPickMessage.text = "Pick 1 Pitcher".localized()

        selectedPlayerType = PlayerType.Pitcher.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Baseball", "Type": "PIT"])

        lblOutfieldersTitle.textColor = kPlayerTypeUnselectedColor
        lblPitcherTitle.textColor = kPlayerTypeSelectedColor
        lblCatcherTitle.textColor = kPlayerTypeUnselectedColor
        lblInfielderTitle.textColor = kPlayerTypeUnselectedColor
        
        lblOutfieldersCount.textColor = kPlayerTypeUnselectedColor
        lblPitcherCount.textColor = kPlayerTypeSelectedColor
        lblInfielderCount.textColor = kPlayerTypeUnselectedColor
        lblCatcherCount.textColor = kPlayerTypeUnselectedColor
        
        outfieldersIcon.image = UIImage(named: "GoalkeeperUnselected")
        pitcherIcon.image = UIImage(named: "MidfielderSelected")
        infielderIcon.image = UIImage(named: "ForwardUnselected")
        catcherIcon.image = UIImage(named: "FootDefenderUnselected")
        
        sliderLeadingConstraint.constant = pitcherIcon.frame.origin.x - 9.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
  
    func catcherSelected() {
        lblPickMessage.text = "Pick 1 Catcher".localized()
        selectedPlayerType = PlayerType.Catcher.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Baseball", "Type": "CAT"])

        lblOutfieldersTitle.textColor = kPlayerTypeUnselectedColor
        lblPitcherTitle.textColor = kPlayerTypeUnselectedColor
        lblCatcherTitle.textColor = kPlayerTypeSelectedColor
        lblInfielderTitle.textColor = kPlayerTypeUnselectedColor
        
        lblOutfieldersCount.textColor = kPlayerTypeUnselectedColor
        lblPitcherCount.textColor = kPlayerTypeUnselectedColor
        lblInfielderCount.textColor = kPlayerTypeUnselectedColor
        lblCatcherCount.textColor = kPlayerTypeSelectedColor
        
        outfieldersIcon.image = UIImage(named: "GoalkeeperUnselected")
        pitcherIcon.image = UIImage(named: "MidfielderUnselected")
        infielderIcon.image = UIImage(named: "ForwardUnselected")
        catcherIcon.image = UIImage(named: "FootDefenderSelected")
        
        sliderLeadingConstraint.constant = catcherIcon.frame.origin.x - 9.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    @IBAction func teamPreviewButtonTapped(_ sender: Any) {
        
        if UserDetails.sharedInstance.selectedPlayerList.count == 0 {
            AppHelper.showAlertView(message: "Please select atleast one player", isErrorMessage: true)
            return
        }
        
        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Baseball"])
        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "BaseballTeamPreviewViewController") as! BaseballTeamPreviewViewController
        teamPreviewVC.totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        teamPreviewVC.isShowPlayingRole = true;
        teamPreviewVC.selectedFantasy = FantasyType.Classic.rawValue
        teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
        teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
        teamPreviewVC.firstTeamkey = self.matchDetails!.firstTeamKey
        teamPreviewVC.secondTeamkey = self.matchDetails!.secondTeamKey

        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    @IBAction func creditButtonTapped(_ sender: Any){
        
        let OutFieldersPlayerListArray = totalOutFieldersArray
        let MidFielderPlayerListArray = totalPitcherArray
        let ForwardPlayerListArray = totalInFieldersArray
        let DefenderPlayerListArray = totalCatcherArray
        
        let sortedKeeperArray =  OutFieldersPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
                
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedPitcherArray =  MidFielderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedInFieldersArray =  ForwardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedCatcherArray =  DefenderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        if creditsSortedType {
            creditButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }
        else{
            creditButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }
        
        creditsSortedType = !creditsSortedType
        totalOutFieldersArray = sortedKeeperArray
        totalPitcherArray = sortedPitcherArray
        totalCatcherArray = sortedCatcherArray
        totalInFieldersArray = sortedInFieldersArray
        
        collectionView.reloadData()
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

    }
    
    @IBAction func pointsButtonTapped(_ sender: Any){
        
        let OutFieldersPlayerListArray = totalOutFieldersArray
        let MidFielderPlayerListArray = totalPitcherArray
        let ForwardPlayerListArray = totalInFieldersArray
        let DefenderPlayerListArray = totalCatcherArray
        
        let sortedKeeperArray =  OutFieldersPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        let sortedPitcherArray =  MidFielderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        let sortedInFieldersArray =  ForwardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        let sortedCatcherArray =  DefenderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        if pointsSortedType {
            pointsButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }
        else{
            pointsButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }
        
        pointsSortedType = !pointsSortedType
        totalOutFieldersArray = sortedKeeperArray
        totalPitcherArray = sortedPitcherArray
        totalCatcherArray = sortedCatcherArray
        totalInFieldersArray = sortedInFieldersArray
        
        collectionView.reloadData()
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

    }
    
    @IBAction func allTeamButtonTapped(_ sender: Any?) {
        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        sortPlayerBasedOnLineupsOut(isFromAll: true)

        
//        totalOutFieldersArray = getSelectedTypePlayerList(selectedRow: 0)
//        totalCatcherArray = getSelectedTypePlayerList(selectedRow: 3)
//        totalPitcherArray = getSelectedTypePlayerList(selectedRow: 2)
//        totalInFieldersArray = getSelectedTypePlayerList(selectedRow: 1)
//
//        collectionView.reloadData()
    }
    
    @IBAction func firstTeamButtonTapped(_ sender: Any) {
        firstTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        let outFieldersPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let inFieldersPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let pitcherPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let catcherPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)

        let filteredOutFieldersArray = outFieldersPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredPitcherArray = pitcherPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredCatcherArray = catcherPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredInfielderArray = inFieldersPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        totalOutFieldersArray = filteredOutFieldersArray
        totalInFieldersArray = filteredInfielderArray
        totalPitcherArray = filteredPitcherArray
        totalCatcherArray = filteredCatcherArray
        
        collectionView.reloadData()
    }
    
    @IBAction func secondTeamButtonTapped(_ sender: Any) {
        
        secondTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        
        
        let outFieldersPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let inFieldersPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let pitcherPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let catcherPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)

        let filteredOutFieldersArray = outFieldersPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredPitcherArray = pitcherPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredCatcherArray = catcherPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredInfielderArray = inFieldersPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        totalOutFieldersArray = filteredOutFieldersArray
        totalInFieldersArray = filteredInfielderArray
        totalPitcherArray = filteredPitcherArray
        totalCatcherArray = filteredCatcherArray
    }

    
    @IBAction func nextButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "NextButtonClicked", info: ["SportType": "Baseball"])
        if UserDetails.sharedInstance.selectedPlayerList.count != 9{
            AppHelper.showAlertView(message: "Please select 9 players", isErrorMessage: true)
            return;
        }
        
        let selectCaptionVC = storyboard?.instantiateViewController(withIdentifier: "SelectBaseballCaptainsViewController") as! SelectBaseballCaptainsViewController
        selectCaptionVC.matchDetails = matchDetails
        selectCaptionVC.leagueDetails = leagueDetails
        selectCaptionVC.isEditPlayerTeam = isEditPlayerTeam
        selectCaptionVC.userTeamDetails = userTeamDetails
        selectCaptionVC.ticketDetails = ticketDetails

        navigationController?.pushViewController(selectCaptionVC, animated: true)
    }
    
    @IBAction func inOutButtonTapped(_ sender: Any?) {
        sortPlayerBasedOnLineupsOut(isFromAll: false)
    }
    
    func sortPlayerBasedOnLineupsOut(isFromAll: Bool) {
        
        if isFromAll {
            allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
            firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        }
        else{
            inOutButton.setTitleColor(UIColor(red: 60.0/255, green: 196.0/255, blue: 266.0/255, alpha: 1), for: .normal)
            secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        }
                
        let outFieldersPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let inFieldersPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let pitcherPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let catcherPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)

        let sortedOutFielderArray = outFieldersPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }
        
        let sortedInFieldersArray = inFieldersPlayerListArray.sorted{ (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }

        let sortedPitcherArray = pitcherPlayerListArray.sorted{ (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }
        
        let sortedCatcherArray = catcherPlayerListArray.sorted{ (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }
        
        
        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        totalOutFieldersArray = sortedOutFielderArray
        totalInFieldersArray = sortedInFieldersArray
        totalPitcherArray = sortedPitcherArray
        totalCatcherArray = sortedCatcherArray

        collectionView.reloadData()
    }
    
    // MARK: - Timer Handler
    @objc func updateTimerVlaue()  {
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kBaseballMatchURL, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    
    //MARK:- Scroll View Delegates
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if collectionView == scrollView{
            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
            if Int(currentPage) == 0{
                outfieldersSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 1{
                 infielderSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 2{
                pitcherSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 3{
                catcherSelected()
                collectionView.reloadData()
            }
        }
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
