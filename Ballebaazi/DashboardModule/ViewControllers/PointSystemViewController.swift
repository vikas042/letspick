//
//  PointSystemViewController.swift
//  Letspick
//
//  Created by anurag singh on 16/07/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class PointSystemViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var sliderViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cricketBtn: UIButton!
    @IBOutlet weak var kabaddiBtn: UIButton!
    @IBOutlet weak var footballBtn: UIButton!
    @IBOutlet weak var basketballBtn: UIButton!
    @IBOutlet weak var baseballBtn: UIButton!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var containerView: UIView!

    var index = 0
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        containerView.backgroundColor = UIColor(red: 0.0/255.0, green: 136.0/255.0, blue: 64.0/255.0, alpha: 1)
        headerView.lblTitle.text = "Point System".localized()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "PointSystemWebCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PointSystemWebCollectionViewCell")
    }
    
    func moveAnimatedView(xPosition: CGFloat){
        self.sliderViewLeadingConstraint.constant = xPosition
        UIView.animate(withDuration: 0.2) {
            self.sliderView.layoutIfNeeded()
        }
    }
    
    @IBAction func cricketBtnAction(_ sender: Any) {
        let nextItem: IndexPath = IndexPath(item: 0, section: 0)
        self.collectionView.scrollToItem(at: nextItem, at: .centeredHorizontally, animated: true)
        moveAnimatedView(xPosition: sliderView.frame.width * 0)
        
        cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)
        kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        footballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)

    }
    
    @IBAction func kabaddiBtnAction(_ sender: Any) {
        let nextItem: IndexPath = IndexPath(item: 1, section: 0)
        self.collectionView.scrollToItem(at: nextItem, at: .centeredHorizontally, animated: true)
        moveAnimatedView(xPosition: sliderView.frame.width * 1)
        
        cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)
        footballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)

    }
    
    @IBAction func footballBtnAction(_ sender: Any) {
        let nextItem: IndexPath = IndexPath(item: 1, section: 0)
        self.collectionView.scrollToItem(at: nextItem, at: .centeredHorizontally, animated: true)
        moveAnimatedView(xPosition: sliderView.frame.width * 1)
        
        cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        footballBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)
        basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
    }
    
    @IBAction func basketballBtnAction(_ sender: Any) {
        let nextItem: IndexPath = IndexPath(item: 3, section: 0)
        self.collectionView.scrollToItem(at: nextItem, at: .centeredHorizontally, animated: true)
        moveAnimatedView(xPosition: sliderView.frame.width * 3)
        
        cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        footballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)
        baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
    }
    
    @IBAction func baseballBtnAction(_ sender: Any) {
        let nextItem: IndexPath = IndexPath(item: 4, section: 0)
        self.collectionView.scrollToItem(at: nextItem, at: .centeredHorizontally, animated: true)
        moveAnimatedView(xPosition: sliderView.frame.width * 4)
        
        cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        footballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
        baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PointSystemWebCollectionViewCell", for: indexPath) as? PointSystemWebCollectionViewCell
        selectedIndex = indexPath.row
        
        var language = "en"
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                language = "hi"
            }
        }
        
        if indexPath.row == 0{
            let urlString = POINTSYSTEMURL + "lang/\(language)/cricket.html"
            cell?.webkitView.load(URLRequest(url: URL(string: urlString)!))
            sliderView.bounds.origin.x = sliderView.frame.width * 0
        }else if indexPath.row == 1{
            let urlString = POINTSYSTEMURL + "lang/\(language)/football.html"
            cell?.webkitView.load(URLRequest(url: URL(string: urlString)!))
            sliderView.bounds.origin.x = sliderView.frame.width * 1
        }
        /*
        else if indexPath.row == 2{
            let urlString = POINTSYSTEMURL + "lang/\(language)/football.html"
            cell?.webkitView.load(URLRequest(url: URL(string: urlString)!))
            sliderView.bounds.origin.x = sliderView.frame.width * 2
        }else if indexPath.row == 3{
            let urlString = POINTSYSTEMURL + "lang/\(language)/basketball.html"
            cell?.webkitView.load(URLRequest(url: URL(string: urlString)!))
            sliderView.bounds.origin.x = sliderView.frame.width * 3
        }else if indexPath.row == 4{
            let urlString = POINTSYSTEMURL + "lang/\(language)/baseball.html"
            cell?.webkitView.load(URLRequest(url: URL(string: urlString)!))
            sliderView.bounds.origin.x = sliderView.frame.width * 4
        }
        */
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        index = Int(scrollView.contentOffset.x / scrollView.bounds.size.width)
        if index == 0{
            moveAnimatedView(xPosition: sliderView.frame.width * 0)
            cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)
            kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            footballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)

        }else if index == 1{
            moveAnimatedView(xPosition: sliderView.frame.width * 1)
            cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
           // kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)
            footballBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)
            basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)

        } /* else if index == 2{
            moveAnimatedView(xPosition: sliderView.frame.width * 2)
            cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            footballBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)
            basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)

        }else if index == 3{
            moveAnimatedView(xPosition: sliderView.frame.width * 3)
            cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            footballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)
            baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)

        }else if index == 4{
            moveAnimatedView(xPosition: sliderView.frame.width * 4)
            cricketBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            kabaddiBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            footballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            basketballBtn.setTitleColor(UIColor.white.withAlphaComponent(0.6), for: .normal)
            baseballBtn.setTitleColor(UIColor.white.withAlphaComponent(1.0), for: .normal)

        }
        */
    }
    
    
    
}
