//
//  QuizLeaguesViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 30/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class QuizLeaguesViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, QuizConfirmationViewDelegate {

    @IBOutlet weak var headerView: LetspickFantasyHeaderView!
    @IBOutlet weak var collectionView: UICollectionView!
  
    var matchDetails: MatchDetails?
    
    @IBOutlet weak var noDataImgView: UIImageView!
    @IBOutlet weak var lblStayTuned: UILabel!
    
    lazy var leagueArray:Array<LeagueDetails> = []
    lazy var selctedGameType = GameType.Cricket.rawValue
    var matchKey = ""
    lazy var isPullToRefresh = false

    var isAutoShowPricebreakupPopup = false
    var selectedLeagueforAutoPopup: LeagueDetails?
    var timer: Timer?

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProperties()
    }
    
    //MARK:- Custom Methods
    func setupProperties() {
        collectionView.register(UINib(nibName: "QuizLeagueCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "QuizLeagueCollectionViewCell")
        lblStayTuned.text = "Stay_TunedQuiz_Message".localized()
        weak var weakSelf = self

        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.updateTimerValue()
        }

    }
    
    func updateAutoPopupStatus() {
        isAutoShowPricebreakupPopup = true
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        headerView.isFantasyModeEnable = false
        headerView.updateData(details: matchDetails)
        UserDetails.sharedInstance.selectedPlayerList.removeAll();
        navigationController?.navigationBar.isHidden = true
        
        if matchDetails != nil {
            matchKey = matchDetails!.matchKey
        }

        if isAutoShowPricebreakupPopup {
            isAutoShowPricebreakupPopup = false
            let confirmationVC = QuizConfirmationView(frame: APPDELEGATE.window!.frame)
            confirmationVC.matchDetails = matchDetails
            confirmationVC.delegate = self
            confirmationVC.updateData(details: selectedLeagueforAutoPopup!)
            APPDELEGATE.window?.addSubview(confirmationVC)
        }
        
        let urlString = BASEURLKABADDI + "quiz/match?option=get_leagues&user_id=" + UserDetails.sharedInstance.userID + "&match_key=" + matchKey
        callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: true)
    }
   
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)
    }


    @objc func refresh(sender:AnyObject) {
        let urlString = BASEURLKABADDI + "quiz/match?option=get_leagues&user_id=" + UserDetails.sharedInstance.userID + "&match_key=" + matchKey
        callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: false)
    }
    
    
    //MARK:- Collection View Data Source and Delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return leagueArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width , height: 108)
    }
    
    func showCategoryIcon(headerView: HeaderCollectionReusableView, details: LeagueCategoryDetails) {
        
        if details.categoryImg.count != 0{
            if let url = NSURL(string: UserDetails.sharedInstance.teamImageUrl + details.categoryImg){
                headerView.imgView.setImage(with: url as URL, placeholder: UIImage(named: "LeagueDefaultIcon"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        headerView.imgView.image = image
                    }
                    else{
                        headerView.imgView.image = UIImage(named: "LeagueDefaultIcon")
                    }
                })
            }
            else{
                headerView.imgView.image = UIImage(named: "LeagueDefaultIcon")
            }
        }
        else{
            headerView.imgView.image = UIImage(named: "LeagueDefaultIcon")
        }

        headerView.layoutIfNeeded()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizLeagueCollectionViewCell", for: indexPath) as! QuizLeagueCollectionViewCell
        cell.playerButton.tag = indexPath.row
        cell.playerButton.addTarget(self, action: #selector(self.playLeagueButtonTapped(button:)), for: .touchUpInside)
        cell.configData(details: leagueArray[indexPath.row])

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let details = leagueArray[indexPath.row]
        if details.leagueType == "2" {
            return
        }

        let leaguePreview = storyboard?.instantiateViewController(withIdentifier: "QuizPreviewViewController") as! QuizPreviewViewController
        leaguePreview.leagueDetails = details
        leaguePreview.matchDetails = matchDetails
        navigationController?.pushViewController(leaguePreview, animated: true)
    }
    
    @objc func playLeagueButtonTapped(button: SolidButton)  {
        let leagueDetails = leagueArray[button.tag]
        selectedLeagueforAutoPopup = leagueDetails
        let confirmationVC = QuizConfirmationView(frame: APPDELEGATE.window!.frame)
        confirmationVC.matchDetails = matchDetails
        confirmationVC.delegate = self
        confirmationVC.updateData(details: leagueDetails)
        APPDELEGATE.window?.addSubview(confirmationVC)
    }

    
    // MARK:- API Related Method
    
    func callGetLeagueAPI(urlString: String, isNeedToShowLoader: Bool)  {
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                isPullToRefresh = false
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        
        weak var weakSelf = self

        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            self.isPullToRefresh = false
            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"].dictionary {
                        if let dataArray = response["leaguess"]?.array {
                            weakSelf?.leagueArray = LeagueDetails.getAllQuizLeagueDetails(dataArray: dataArray)
                            weakSelf?.noDataImgView.isHidden = true
                            weakSelf?.lblStayTuned.isHidden = true

                            if dataArray.count == 0 {
                                weakSelf?.noDataImgView.isHidden = false
                                weakSelf?.lblStayTuned.isHidden = false
                                weakSelf?.collectionView.isHidden = true
                            }
                        }
                        else{
                            weakSelf?.noDataImgView.isHidden = false
                            weakSelf?.lblStayTuned.isHidden = false
                            weakSelf?.collectionView.isHidden = true
                        }
                        if (response["selected_match"]?.dictionary) != nil {
                            weakSelf?.matchDetails = MatchDetails.parseQuizDetails(details: response["selected_match"]!)
                            weakSelf?.headerView.updateData(details: weakSelf?.matchDetails)
                        }
                        weakSelf?.collectionView.reloadData()
                    }
                }
                else{
                    AppHelper.sharedInstance.removeSpinner()
                }
            }
            else{
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }

    //MARK:- Scroll View Delegates
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if !isPullToRefresh{
            if scrollView.contentOffset.y < -120{
                isPullToRefresh = true
                let urlString = BASEURLKABADDI + "quiz/match?option=get_leagues&user_id=" + UserDetails.sharedInstance.userID + "&match_key=" + matchKey
                callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: true)
            }
        }
    }
    
    
    @objc func updateTimerValue()  {
        
        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateData(details: matchDetails)
        }
        else {
            headerView.updateData(details: matchDetails)
//            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
//            weak var weakSelf = self
//            if remainingTime <= 5 {
//                DispatchQueue.main.async {
//                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
//                }
//            }
        }
    }
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
