//
//  RewardStoreViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 30/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class RewardStoreViewController: UIViewController {
    
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var announcementView: AnnouncementView!
    var urlString = kRewardProgram + "?option=get_product_list&user_id="
    
    @IBOutlet weak var bannerCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var rewardsCollectionView: UICollectionView!
    @IBOutlet weak var lblHelloTitle: UILabel!
    @IBOutlet weak var claimButton: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCoins: UILabel!
    @IBOutlet weak var headerView: CustomNavigationBar!
    lazy var isFromTabBar = true
    lazy var bannersArray = Array<BannerDetails>()
    lazy var productsArray = Array<RewardProductDetails>()
    var addressDetails: RewardAddressDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.isHidden = true
        infoButton.isHidden = true
        rewardsCollectionView.register(UINib(nibName: "RewardStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RewardStoreCollectionViewCell")
        bannerCollectionView.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell")
        headerView.headerTitle = "Rewards Store".localized()
        lblCoins.text = UserDetails.sharedInstance.currentBBcoins
        lblName.text = UserDetails.sharedInstance.userName
        urlString = urlString + UserDetails.sharedInstance.userID
        
        NotificationCenter.default.addObserver(self, selector: #selector(rewardButtonTapped(notification:)), name: NSNotification.Name(rawValue: "rewardButtonTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userlogoutNotitification(notification:)), name: NSNotification.Name(rawValue: "userlogoutNotitification"), object: nil)

        
        if !isFromTabBar{
            callGetRewardStoreAPI(isNeedToShowLoader: true)
        }
    }

    @objc func userlogoutNotitification(notification: Notification)  {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func rewardButtonTapped(notification: Notification)  {
        lblCoins.text = UserDetails.sharedInstance.currentBBcoins + " " + "LP Coins".localized()
        callGetRewardStoreAPI(isNeedToShowLoader: true)
    }

    override func viewWillAppear(_ animated: Bool) {
                
        lblHelloTitle.text = "Hello".localized()
        claimButton.setTitle("Claims".localized(), for: .normal)
        lblCoins.text = UserDetails.sharedInstance.currentBBcoins + " " + "LP Coins".localized()
        lblName.text = UserDetails.sharedInstance.userName

        infoButton.isHidden = false

        if !isFromTabBar{
            headerView.isHidden = false
            infoButton.isHidden = false
            headerHeightConstraint.constant = 60
            headerView.layoutIfNeeded()
        }
        if let dashboardVC = navigationController?.viewControllers.first as? DashboardViewController{
            if dashboardVC.preselectedTab == 600{
                callGetRewardStoreAPI(isNeedToShowLoader: true)
            }
        }
    }
    
    @IBAction func rewardInfoButtonTapped(_ sender: Any) {
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
        privacyPolicyVC?.selectedType = SelectedWebViewType.RewardProgram.rawValue
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.pushViewController(privacyPolicyVC!, animated: true)
        }
    }

    @IBAction func claimButtonTapped(_ sender: Any) {
        let myClaimVC = storyboard?.instantiateViewController(withIdentifier: "MyClaimViewController") as! MyClaimViewController
        navigationController?.pushViewController(myClaimVC, animated: true)
    }
    
    func callGetRewardStoreAPI(isNeedToShowLoader: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }

        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let statusCode = result!["status"].string
                let message = result!["message"].string ?? "kErrorMsg".localized()
                weakSelf?.lblCoins.text = UserDetails.sharedInstance.currentBBcoins + " " + "LP Coins".localized()

                if statusCode == "200" {
                    if let response = result!["response"].dictionary {
                        if let announcement = response["announcement"]?.dictionary{
                            let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                            
                            self.announcementViewHeightConstraint.constant = 50.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: details.message)
                        }
                        else{
                            self.announcementViewHeightConstraint.constant = 0.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: "")
                        }
                        
                        
                        if let banners = response["banner"]?.array {
                            weakSelf?.bannersArray = BannerDetails.getPromoBannerDetails(dataArray: banners)
                            if weakSelf?.bannersArray.count == 0 {
                                weakSelf?.bannerCollectionViewHeightConstraint.constant = 0
                                weakSelf?.view.layoutIfNeeded()
                            }
                        }
                        
                        if let productlist = response["productlist"]?.array {
                            if let claimedArray = response["lastClaimedProduct"]?.array {
                                weakSelf?.productsArray = RewardProductDetails.getAllProductList(dataArray: productlist, claimedProduct: claimedArray)
                            }
                            else{
                                weakSelf?.productsArray = RewardProductDetails.getAllProductList(dataArray: productlist, claimedProduct: Array<JSON>())
                            }
                        }
                        
                        if let address = response["saveAddress"] {
                            weakSelf?.addressDetails = RewardAddressDetails.parseAddressDetails(details: address)
                        }
                        
                        weakSelf?.rewardsCollectionView.reloadData()
                        weakSelf?.bannerCollectionView.reloadData()
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callClaimProductAPI(details: RewardProductDetails)  {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "claim_confirm", "user_id": UserDetails.sharedInstance.userID, "product": details.rewardProdId ] as [String : Any]
        
        WebServiceHandler.performPOSTRequest(urlString: kRewardProgram, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200" {
                    var message = ""
                    if details.rewardCategoryId == "1" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be sent to your address within 7-10 business days. Please check your email"
                    }
                    else if details.rewardCategoryId == "2" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be sent to your email address. Please check your email"
                    }
                    else if details.rewardCategoryId == "3" {
                        if details.rewardId == "6" {
                            message = "Congratulations you have successfully donated for the cause. Thank You!"
                        }
                        else{
                            message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be credited in your account in some time. Keep playing on Letspick"
                        }
                    }
                    
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            if details.rewardCategoryId == "1" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपको 7 - 10 कार्य दिवस में आपके पते पर भेज दिया जायेगा। कृपया अपना ईमेल अकाउंट चेक करें।"

                            }
                            else if details.rewardCategoryId == "2" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपको आपके ईमेल पर भेज दिया जायेगा । कृपया अपना ईमेल अकाउंट चेक करें।"
                            }
                            else if details.rewardCategoryId == "3" {
                                if details.rewardId == "6" {
                                    message = "बधाई हो। आप सफतापूर्वक डोनेट कर चुके हैं, धन्यवाद् ।"
                                }
                                else{
                                    message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपके अकाउंट में कुछ देर बाद क्रेडिट कर दिया जायेगा । Letspick पर खेलते रहें।"
                                }
                            }
                        }
                    }

                    let confratsView = CongratsRewarsClaimPopup(frame: APPDELEGATE.window!.frame)
                    confratsView.isViewForClaim = true

                    confratsView.configData(mesage: message)
                    APPDELEGATE.window!.addSubview(confratsView)
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized() , isErrorMessage: true)
            }
        }
    }
}

extension RewardStoreViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100 {
            return bannersArray.count
        }
        
        return productsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 100 {
            return CGSize(width: UIScreen.main.bounds.width - 20, height: 100)
        }
        return CGSize(width: (UIScreen.main.bounds.width - 10)/2, height: (UIScreen.main.bounds.width - 10)/2 + 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 100 {
                    
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
            let details = bannersArray[indexPath.item]
            cell.configBannerData(details: details)
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RewardStoreCollectionViewCell", for: indexPath) as! RewardStoreCollectionViewCell
            let details = productsArray[indexPath.item]
            cell.claimButton.tag = indexPath.row
            cell.claimButton.backgroundColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
            cell.claimButton.addTarget(self, action: #selector(cellClaimButtonTapped(button:)), for: .touchUpInside)
            cell.configData(details: details)
            if UserDetails.sharedInstance.currentBBcoins < details.coins{
                cell.claimButton.backgroundColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 0.4)
            }else{
                cell.claimButton.backgroundColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag != 100 {
            let details = productsArray[indexPath.item]
            let rewardView = RewardLogisticView(frame: APPDELEGATE.window!.frame)
            rewardView.configData(details: details)
            rewardView.addressDetails = addressDetails
            rewardView.productDetails = details

            APPDELEGATE.window?.addSubview(rewardView)
        }
        else{
            
            let details = bannersArray[indexPath.item]
            var sportType = ""
            if details.gameType == "1" {
                sportType = "Cricket"
            }
            else if details.gameType == "1" {
                sportType = "Kabaddi"
            }
            else if details.gameType == "1" {
                sportType = "Football"
            }
            
            AppxorEventHandler.logAppEvent(withName: "BannerClicked", info: ["BannerRedirectionType": details.redirectType, "SportType": sportType, "BannerName": details.title])
        
            if details.redirectType == "1" {
                if details.gameType == "1" {
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navigationController?.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "2" {
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navigationController?.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "3" {
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navigationController?.pushViewController(leagueVC, animated: true)
                }
            }
            else if details.redirectType == "2" {
                let leagueVC = storyboard?.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
                navigationController?.pushViewController(leagueVC, animated: true)
            }
            else if details.redirectType == "3" {
                
                if details.websiteUrl.count == 0{
                    return;
                }
                let webviewVC = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webviewVC.urlString = details.websiteUrl
                navigationController?.pushViewController(webviewVC, animated: true)
            }
            else if details.redirectType == "4" {
                let playerView = YoutubeVideoPlayerView(frame: APPDELEGATE.window!.frame)
                playerView.videoID = details.videoUrl
                playerView.playView()
                APPDELEGATE.window!.addSubview(playerView)
            }
            else if details.redirectType == "5" {
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                    if let dashboardVC = navVC.viewControllers.first as? DashboardViewController{
                        if details.redirectSportType == "1"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Cricket.rawValue)
                        }
                        else if details.redirectSportType == "2"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Kabaddi.rawValue)
                        }
                        else if details.redirectSportType == "3"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Football.rawValue)
                        }
                    }
                }
            }
            else if details.redirectType == "6" {
                let promoCodeVC = storyboard?.instantiateViewController(withIdentifier: "PromotionsViewController") as! PromotionsViewController
                promoCodeVC.isFromSetting = true
                navigationController?.pushViewController(promoCodeVC, animated: true)
            }
            else if details.redirectType == "7" {
                let addCashVC = storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
                navigationController?.pushViewController(addCashVC, animated: true)
            }
            else if details.redirectType == "8" {
                
                let howToPlayVC = storyboard?.instantiateViewController(withIdentifier: "HowToPlayViewController") as! HowToPlayViewController
                navigationController?.pushViewController(howToPlayVC, animated: true)
            }
            else if details.redirectType == "9" {

                let becomePartnerVC = storyboard?.instantiateViewController(withIdentifier: "BecomePartnerViewController") as! BecomePartnerViewController
                navigationController?.pushViewController(becomePartnerVC, animated: true)
            }
            else if details.redirectType == "10" {
                // Batting fantasy
                if details.gameType == "1" {
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.matchKey = details.matchKey
                    leagueVC.selectedFantasy = FantasyType.Batting.rawValue
                    navigationController?.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "2" {
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navigationController!.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "3" {
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navigationController?.pushViewController(leagueVC, animated: true)
                }
            }
            else if details.redirectType == "11" {
                // Bowling fantasy
                if details.gameType == "1" {
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.matchKey = details.matchKey
                    leagueVC.selectedFantasy = FantasyType.Bowling.rawValue
                    navigationController?.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "2" {
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navigationController?.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "3" {
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navigationController?.pushViewController(leagueVC, animated: true)
                }
            }
        }
    }
    
    @objc func cellClaimButtonTapped(button: UIButton)  {
        if !UserDetails.sharedInstance.panVerified || !UserDetails.sharedInstance.emailVerified {
            let overlayMessage = PanNotVerifiedOverLayview(frame: APPDELEGATE.window!.frame)
            APPDELEGATE.window?.addSubview(overlayMessage)
            overlayMessage.showAnimation()
            return
        }

        let details = productsArray[button.tag]
        if details.isProductClaimed {
            
            var message = "You have already claimed this item, You will again be eligible to claim this product after \(details.remainingDays) days"

            if details.remainingDays == "1" {
                message = "You have already claimed this item, You will again be eligible to claim this product after \(details.remainingDays) day"
            }
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    message = "आपने इस आइटम पर पहले ही दावा कर दिया है, आप \(details.remainingDays) दिन के बाद फिर से इस उत्पाद का दावा करने के लिए पात्र होंगे|"
                }
            }
            AppHelper.showAlertView(message: message, isErrorMessage: true)
            return
        }
        else if (Int(details.coins) ?? 0 > Int(UserDetails.sharedInstance.currentBBcoins) ?? 0){
            AppHelper.showAlertView(message: "Insufficient coins in your wallet".localized(), isErrorMessage: true)
            return;
        }
        if details.rewardCategoryId == "3" {
            
            let permissionPopup = PermissionView(frame: APPDELEGATE.window!.frame)
                APPDELEGATE.window?.addSubview(permissionPopup)
            var message = "Are you sure you want to claim \(details.rewardNameEnglish) and Redeem \(details.coins) LP Coins?"
             if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                 if "hi" == lang[0]{
                     message = "क्या आप \(details.rewardNameEnglish) का क्लेम करना चाहते हैं और \(details.coins) LP सिक्के को भुनाना चाहते हैं?"
                 }
             }

            permissionPopup.updateMessage(title: "Confirm Claim".localized(), message: message.localized())
            permissionPopup.updateButtonTitle(noButtonTitle: "No".localized(), yesButtonTitle: "Yes".localized())

            DispatchQueue.main.async {
                permissionPopup.showAnimation()
            }

            permissionPopup.noButtonTappedBlock { (status) in
                
            }
            
            permissionPopup.yesButtonTappedBlock { (status) in
                self.callClaimProductAPI(details: details)
             }
        }
        else{
            let addClaimVC = storyboard?.instantiateViewController(withIdentifier: "AddClaimViewController") as! AddClaimViewController
            addClaimVC.productArray = [details]
            addClaimVC.addressDetails = addressDetails
            navigationController?.pushViewController(addClaimVC, animated: true)
        }
    }
}
