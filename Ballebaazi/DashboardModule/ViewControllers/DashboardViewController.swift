
//
//  DashboardViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/5/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import FacebookCore

enum GameType: Int {
    case Cricket = 1000
    case Kabaddi = 2000
    case Football = 3000
    case Quiz = 5000
    case Basketball = 6000
    case Baseball = 7000
    case None = 1000000
}


enum FantasyType: Int {
    case Classic = 1000
    case Batting = 2000
    case Bowling = 3000
    case Reverse = 4000
    case Wizard = 5000
    case None = 1000000
}


class DashboardViewController: UIViewController {
    
    func updateHeaderOnscroll(gameType: Int) {
        if gameType == GameType.Cricket.rawValue{
            headerView.cricketButtonTapped(nil);
        }
        else if gameType == GameType.Kabaddi.rawValue{
            headerView.kabaddiButtonTapped(nil)
        }
        else if gameType == GameType.Football.rawValue{
            headerView.footballButtonTapped(nil)
        }
        else if gameType == GameType.Quiz.rawValue{
            headerView.quizButtonTapped()
        }
        else if gameType == GameType.Basketball.rawValue{
            headerView.basketballButtonTapped()
        }
        else if gameType == GameType.Baseball.rawValue{
            headerView.baseballButtonTapped()
        }
    }
    
    @IBOutlet weak var homeContainerView: UIView!

    @IBOutlet weak var rewardContainerView: UIView!
    @IBOutlet weak var myLeagueContainerView: UIView!
    @IBOutlet weak var leagueBottomView: UIView!
    @IBOutlet weak var leagueBottomView1: UIView!
    
    @IBOutlet weak var myAccountBottomView: UIView!
    @IBOutlet weak var myAccountBottomView1: UIView!

    @IBOutlet weak var myAccountTabView: UIView!

    @IBOutlet weak var moreBottomView: UIView!
    @IBOutlet weak var moreBottomView1: UIView!

    @IBOutlet weak var moreTabView: MoreTabDetailsView!

    @IBOutlet weak var homeBottomView: UIView!
    @IBOutlet weak var homeBottomView1: UIView!

    @IBOutlet weak var homeTabIcon: UIImageView!
    @IBOutlet weak var leagueTabIcon: UIImageView!
    @IBOutlet weak var myAccountTabIcon: UIImageView!
    @IBOutlet weak var moreTabIcon: UIImageView!
    
    @IBOutlet weak var homeTabIcon1: UIImageView!
    @IBOutlet weak var leagueTabIcon1: UIImageView!
    @IBOutlet weak var myAccountTabIcon1: UIImageView!
    @IBOutlet weak var moreTabIcon1: UIImageView!
    
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblLeague: UILabel!
    @IBOutlet weak var lblMyAccount: UILabel!
    @IBOutlet weak var lblMore: UILabel!
    
    @IBOutlet weak var lblHome1: UILabel!
    @IBOutlet weak var lblLeague1: UILabel!
    @IBOutlet weak var lblMyAccount1: UILabel!
    @IBOutlet weak var lblMore1: UILabel!
    
    @IBOutlet weak var headerView: LetspickHomeHeaderView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomView1: UIView!

    @IBOutlet weak var BottomTabHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomTabHeightConstraintWithCoinView: NSLayoutConstraint!

    
    
    @IBOutlet weak var lblRewards: UILabel!
    @IBOutlet weak var rewardImgView: UIImageView!
    
    
    var rewardVisiableCount = 0
        
    var isApplicationLaunchedFirstTime = true
    var preselectedTab = 0
    var selectedGameType = GameType.Cricket.rawValue
    
    lazy var firstTabGameType = GameType.Cricket.rawValue;
    lazy var secondTabGameType = GameType.Kabaddi.rawValue;
    lazy var thirdTabGameType = GameType.Football.rawValue;
    lazy var fourthTabGameType = GameType.Quiz.rawValue;
    lazy var fifthTabGameType = GameType.Basketball.rawValue;
    lazy var sixthTabGameType = GameType.Baseball.rawValue;

    // MARK:- View Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDetails.sharedInstance.startTimer()
        UserDetails.sharedInstance.userLoggedIn = true;
        
        (firstTabGameType, secondTabGameType, thirdTabGameType, fourthTabGameType, fifthTabGameType, sixthTabGameType) = AppHelper.getLandingOrder()
        
        if let savedUserJson = AppHelper.getValueFromCoreData(urlString: userDetailsUrl){
            UserDetails.sharedInstance.getAllUserDetails(details: savedUserJson)
        }
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFantasyNotification), name: NSNotification.Name(rawValue: "updateFantasyNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationComesInForground), name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userlogoutNotitification(notification:)), name: NSNotification.Name(rawValue: "userlogoutNotitification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.playNowFromPartnership), name: NSNotification.Name(rawValue: "playNowFromPartnership"), object: nil)

        headerView.cricketHomeButtonTapped { (selectedTab) in
            self.selectedGameType = GameType.Cricket.rawValue
            if self.preselectedTab == 100{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  selectedTab)
            }
            else if self.preselectedTab == 200{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  selectedTab)
            }
        }
        
        headerView.kabaddiButtonTapped { (selectedTab) in
            self.selectedGameType = GameType.Kabaddi.rawValue
            if self.preselectedTab == 100{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  selectedTab)
            }
            else if self.preselectedTab == 200{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  selectedTab)
            }
        }

        headerView.footballHomeButtonTapped { (selectedTab) in
            self.selectedGameType = GameType.Football.rawValue
            if self.preselectedTab == 100{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  selectedTab)
            }
            else if self.preselectedTab == 200{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  selectedTab)
            }
        }
        
        headerView.quizHomeButtonTapped { (selectedTab) in
            self.selectedGameType = GameType.Quiz.rawValue
            if self.preselectedTab == 100{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  selectedTab)
            }
            else if self.preselectedTab == 200{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  selectedTab)
            }
        }
        
        headerView.basketballHomeButtonTapped { (selectedTab) in
            self.selectedGameType = GameType.Basketball.rawValue
            if self.preselectedTab == 100{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  selectedTab)
            }
            else if self.preselectedTab == 200{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  selectedTab)
            }
        }
        
        headerView.baseballHomeButtonTapped { (selectedTab) in
            self.selectedGameType = GameType.Baseball.rawValue
            if self.preselectedTab == 100{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  selectedTab)
            }
            else if self.preselectedTab == 200{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  selectedTab)
            }
        }


        bottomView1.isHidden = true
        bottomView.isHidden = false
        
        if let passStatus = UserDefaults.standard.value(forKey: "passStatus") as? String {
            if passStatus == "1"{
                DispatchQueue.main.async {
                    self.headerView.passView.isHidden = false
                }
            }
        }
        
        if let rewardStatus = UserDefaults.standard.value(forKey: "rewardStatus") as? String {
            if rewardStatus == "1" {
                bottomView1.isHidden = false
                bottomView.isHidden = true
                if let appOpenCountForReward = UserDefaults.standard.value(forKey: "AppOpenCountForReward") as? String {
                    let count = Int(appOpenCountForReward) ?? 0
                    UserDefaults.standard.setValue(String(count + 1), forKey: "AppOpenCountForReward")
                    rewardVisiableCount = count
                }
                else{
                    UserDefaults.standard.setValue("1", forKey: "AppOpenCountForReward")
                }
            }
        }
        
        if let landingSport = UserDefaults.standard.value(forKey: "landingSport") as? String {
            DispatchQueue.main.async {
                
                if landingSport == "1" {
                    self.headerView.cricketButtonTapped(nil)
                }
                else if landingSport == "2" {
                    self.headerView.kabaddiButtonTapped(nil)
                }
                else if landingSport == "3" {
                    self.headerView.footballButtonTapped(nil)
                }
                else if landingSport == "4" {
                    self.headerView.quizButtonTapped()
                }
                else if landingSport == "5" {
                    self.headerView.basketballButtonTapped()
                }
                else if landingSport == "6" {
                    self.headerView.baseballButtonTapped()
                }
                else{
                    self.headerView.cricketButtonTapped(nil)
                }
            }
        }
        else{
            DispatchQueue.main.async {
                
                if self.firstTabGameType == GameType.Cricket.rawValue {
                    self.headerView.cricketButtonTapped(nil)
                }
                else if self.firstTabGameType == GameType.Kabaddi.rawValue {
                    self.headerView.kabaddiButtonTapped(nil)
                }
                else if self.firstTabGameType == GameType.Football.rawValue {
                    self.headerView.footballButtonTapped(nil)
                }
                else if self.firstTabGameType == GameType.Quiz.rawValue {
                    self.headerView.quizButtonTapped()
                }
                else if self.firstTabGameType == GameType.Basketball.rawValue {
                    self.headerView.basketballButtonTapped()
                }
                else if self.firstTabGameType == GameType.Baseball.rawValue {
                    self.headerView.baseballButtonTapped()
                }
                else{
                    self.headerView.cricketButtonTapped(nil)
                }
            }
        }
        

        homeButtonSelected()
        AppHelper.showShodowOnBottomTab(bottomView: bottomView)
        AppHelper.showShodowOnBottomTab(bottomView: bottomView1)
    }
    
    @objc func userlogoutNotitification(notification: Notification)  {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func applicationComesInForground(notification: Notification)  {
        
        if !isApplicationLaunchedFirstTime {
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                if ((navVC.visibleViewController as? DashboardViewController) != nil) {
                }
                else{
                    return;
                }
            }

            if selectedGameType == GameType.Cricket.rawValue{
                if self.preselectedTab == 100{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Cricket.rawValue)
                }
                else if self.preselectedTab == 200{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  GameType.Cricket.rawValue)
                }
            }
            else if selectedGameType == GameType.Kabaddi.rawValue{
                if self.preselectedTab == 100{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Kabaddi.rawValue)
                }
                else if self.preselectedTab == 200{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  GameType.Kabaddi.rawValue)
                }
            }
            else if selectedGameType == GameType.Football.rawValue{
                if self.preselectedTab == 100{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Football.rawValue)
                }
                else if self.preselectedTab == 200{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  GameType.Football.rawValue)
                }
            }
            else if selectedGameType == GameType.Basketball.rawValue{
                if self.preselectedTab == 100{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Basketball.rawValue)
                }
                else if self.preselectedTab == 200{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  GameType.Basketball.rawValue)
                }
            }
            else if selectedGameType == GameType.Baseball.rawValue{
                if self.preselectedTab == 100{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Baseball.rawValue)
                }
                else if self.preselectedTab == 200{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object:  GameType.Baseball.rawValue)
                }
            }
        }
        
        isApplicationLaunchedFirstTime = false
    }

    @objc func playNowFromPartnership(notification: Notification)  {
        if selectedGameType == GameType.Cricket.rawValue{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Cricket.rawValue)
            homeButtonSelected()
        }
        else if selectedGameType == GameType.Kabaddi.rawValue{
                            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Kabaddi.rawValue)
            homeButtonSelected()
        }
        else if selectedGameType == GameType.Football.rawValue{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Football.rawValue)
            homeButtonSelected()
        }
        else if selectedGameType == GameType.Quiz.rawValue{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Quiz.rawValue)
            homeButtonSelected()
        }
        else if selectedGameType == GameType.Basketball.rawValue{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Basketball.rawValue)
            homeButtonSelected()
        }
        else if selectedGameType == GameType.Baseball.rawValue{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object:  GameType.Baseball.rawValue)
            homeButtonSelected()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateDataOnLanguageChange()
        navigationController?.navigationBar.isHidden = true
        UserDetails.sharedInstance.selectedPlayerList.removeAll()
        UserDetails.sharedInstance.userTeamsArray.removeAll()
        
        if AppHelper.isApplicationRunningOnIphoneX() {
            BottomTabHeightConstraint.constant = 75.0
            bottomTabHeightConstraintWithCoinView.constant = 75.0
            
            bottomView1.layoutIfNeeded()
            bottomView.layoutIfNeeded()
            view.layoutIfNeeded()
        }
        updateWalletAmount()
    }
    
    func updateDataOnLanguageChange() {

        lblHome.text = "MATCHES".localized()
        lblLeague.text = "MY LEAGUES".localized()
        lblMyAccount.text = "ACCOUNT".localized()
        lblMore.text = "MORE".localized()
        lblRewards.text = "REWARDS".localized()

        lblHome1.text = "MATCHES".localized()
        lblLeague1.text = "MY LEAGUES".localized()
        lblMyAccount1.text = "ACCOUNT".localized()
        lblMore1.text = "MORE".localized()
        
        moreTabView.updateDataArray()
        headerView.updateLanguage()
        if preselectedTab == 200{
            headerView.showGameChangeOption(title: "My Leagues")
        }
        else if preselectedTab == 300{
            headerView.showGameChangeOption(title: "My Account")
        }
        else if preselectedTab == 500{
            headerView.showGameChangeOption(title: "More")
        }
        else if preselectedTab == 600{
            headerView.showGameChangeOption(title: "Rewards Store")
        }
    }
    
    @objc func updateWalletAmount()  {
        headerView.updateWalletAmount()
    }
    
    
    func homeButtonSelected()  {
        
        homeContainerView.isHidden = false
        myLeagueContainerView.isHidden = true
        moreTabView.isHidden = true
        myAccountTabView.isHidden = true
        preselectedTab = 100
        homeBottomView.backgroundColor = selectedTabColor
        lblHome.textColor = selectedTabColor
        lblLeague.textColor = UnselectedTabColor
        lblMyAccount.textColor = UnselectedTabColor
        lblMore.textColor = UnselectedTabColor
        
        lblHome1.textColor = selectedTabColor
        lblLeague1.textColor = UnselectedTabColor
        lblMyAccount1.textColor = UnselectedTabColor
        lblMore1.textColor = UnselectedTabColor
        lblRewards.textColor = UnselectedTabColor

        
        homeBottomView.isHidden = true
        leagueBottomView.isHidden = true
        myAccountBottomView.isHidden = true
        moreBottomView.isHidden = true
        rewardContainerView.isHidden = true

        
        homeBottomView1.isHidden = true
        leagueBottomView1.isHidden = true
        myAccountBottomView1.isHidden = true
        moreBottomView1.isHidden = true

        
        homeTabIcon.image = UIImage.init(named: "HomeTabIconSelected")
        homeTabIcon.tintColor = selectedTabColor
        
        leagueTabIcon.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon.image = UIImage.init(named: "MoreTabUnselected")
        
        
        homeTabIcon1.image = UIImage.init(named: "HomeTabIconSelected")
        homeTabIcon1.tintColor = selectedTabColor
        leagueTabIcon1.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon1.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon1.image = UIImage.init(named: "MoreTabUnselected")
        if rewardVisiableCount > 10 {
            rewardImgView.image = UIImage.init(named: "UnselectedCoinTab")
        }
        else{
            rewardImgView.image = UIImage.init(named: "SelectedCoinTab")
        }

        headerView.showGameChangeOption(title: "")
    }
    
    func leagueButtonSelected()  {
        homeContainerView.isHidden = true
        myLeagueContainerView.isHidden = false
        moreTabView.isHidden = true
        myAccountTabView.isHidden = true
        preselectedTab = 200
        leagueBottomView.backgroundColor = selectedTabColor
        rewardContainerView.isHidden = true

        lblLeague.textColor = selectedTabColor
        lblHome.textColor = UnselectedTabColor
        lblMyAccount.textColor = UnselectedTabColor
        lblMore.textColor = UnselectedTabColor
        
        lblLeague1.textColor = selectedTabColor
        lblHome1.textColor = UnselectedTabColor
        lblMyAccount1.textColor = UnselectedTabColor
        lblMore1.textColor = UnselectedTabColor

        
        headerView.showGameChangeOption(title: "My Leagues")

        homeBottomView.isHidden = true
        leagueBottomView.isHidden = true
        myAccountBottomView.isHidden = true
        moreBottomView.isHidden = true
        
        homeBottomView1.isHidden = true
        leagueBottomView1.isHidden = true
        myAccountBottomView1.isHidden = true
        moreBottomView1.isHidden = true
        lblRewards.textColor = UnselectedTabColor

        
        homeTabIcon.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon.image = UIImage.init(named: "LeagueTabSelected")
        myAccountTabIcon.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon.image = UIImage.init(named: "MoreTabUnselected")
        
        homeTabIcon1.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon1.image = UIImage.init(named: "LeagueTabSelected")
        myAccountTabIcon1.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon1.image = UIImage.init(named: "MoreTabUnselected")
        
        if rewardVisiableCount > 10 {
            rewardImgView.image = UIImage.init(named: "UnselectedCoinTab")
        }
        else{
            rewardImgView.image = UIImage.init(named: "SelectedCoinTab")
        }
    }

    func leaderboardButtonSelected()  {

        homeContainerView.isHidden = true
        myLeagueContainerView.isHidden = true
        moreTabView.isHidden = true
        myAccountTabView.isHidden = true
        preselectedTab = 300
        rewardContainerView.isHidden = true

        myAccountBottomView.backgroundColor = selectedTabColor
        lblHome.textColor = UnselectedTabColor
        lblLeague.textColor = UnselectedTabColor
        lblMyAccount.textColor = UnselectedTabColor
        lblMore.textColor = UnselectedTabColor
        
        lblHome1.textColor = UnselectedTabColor
        lblLeague1.textColor = UnselectedTabColor
        lblMyAccount1.textColor = UnselectedTabColor
        lblMore1.textColor = UnselectedTabColor
        
        homeBottomView.isHidden = true
        leagueBottomView.isHidden = true
        myAccountBottomView.isHidden = true
        moreBottomView.isHidden = true
        lblRewards.textColor = UnselectedTabColor

        homeBottomView1.isHidden = true
        leagueBottomView1.isHidden = true
        myAccountBottomView1.isHidden = true
        moreBottomView1.isHidden = true

        
        homeTabIcon.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon.image = UIImage.init(named: "MoreTabUnselected")
        
        homeTabIcon1.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon1.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon1.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon1.image = UIImage.init(named: "MoreTabUnselected")
        if rewardVisiableCount > 10 {
            rewardImgView.image = UIImage.init(named: "UnselectedCoinTab")
        }
        else{
            rewardImgView.image = UIImage.init(named: "SelectedCoinTab")
        }
    }

    func myAccountButtonSelected()  {
        
        homeContainerView.isHidden = true
        myLeagueContainerView.isHidden = true
        moreTabView.isHidden = true
        myAccountTabView.isHidden = false
        rewardContainerView.isHidden = true

        preselectedTab = 400
        
        lblMyAccount.textColor = selectedTabColor
        myAccountBottomView.backgroundColor = selectedTabColor
        lblHome.textColor = UnselectedTabColor
        lblLeague.textColor = UnselectedTabColor
        lblMore.textColor = UnselectedTabColor
        
        lblRewards.textColor = UnselectedTabColor

        lblMyAccount1.textColor = selectedTabColor
        lblHome1.textColor = UnselectedTabColor
        lblLeague1.textColor = UnselectedTabColor
        lblMore1.textColor = UnselectedTabColor

        
        homeBottomView.isHidden = true
        leagueBottomView.isHidden = true
        myAccountBottomView.isHidden = true
        moreBottomView.isHidden = true
        
        homeBottomView1.isHidden = true
        leagueBottomView1.isHidden = true
        myAccountBottomView1.isHidden = true
        moreBottomView1.isHidden = true

        
        headerView.showGameChangeOption(title: "My Account")

        homeTabIcon.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon.image = UIImage.init(named: "MyAccountSelectedTab")
        moreTabIcon.image = UIImage.init(named: "MoreTabUnselected")
        
        homeTabIcon1.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon1.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon1.image = UIImage.init(named: "MyAccountSelectedTab")
        moreTabIcon1.image = UIImage.init(named: "MoreTabUnselected")
        if rewardVisiableCount > 10 {
            rewardImgView.image = UIImage.init(named: "UnselectedCoinTab")
        }
        else{
            rewardImgView.image = UIImage.init(named: "SelectedCoinTab")
        }
    }

    
    func rewardButtonSelected()  {
        
        homeContainerView.isHidden = true
        myLeagueContainerView.isHidden = true
        rewardContainerView.isHidden = false

        moreTabView.isHidden = true
        myAccountTabView.isHidden = true
        myAccountTabView.isHidden = true

        preselectedTab = 600
       
        lblMore.textColor = UnselectedTabColor
        lblHome.textColor = UnselectedTabColor
        lblLeague.textColor = UnselectedTabColor
        lblMyAccount.textColor = UnselectedTabColor

        lblRewards.textColor = selectedTabColor

        
        lblMore1.textColor = UnselectedTabColor
        lblHome1.textColor = UnselectedTabColor
        lblLeague1.textColor = UnselectedTabColor
        lblMyAccount1.textColor = UnselectedTabColor
        lblRewards.textColor = selectedTabColor

        
        headerView.showGameChangeOption(title: "Rewards Store")

        homeBottomView.isHidden = true
        leagueBottomView.isHidden = true
        myAccountBottomView.isHidden = true
        moreBottomView.isHidden = true
        
        homeBottomView1.isHidden = true
        leagueBottomView1.isHidden = true
        myAccountBottomView1.isHidden = true
        moreBottomView1.isHidden = true

        
        homeTabIcon.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon.image = UIImage.init(named: "MoreTabUnselected")
        
        
        homeTabIcon1.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon1.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon1.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon1.image = UIImage.init(named: "MoreTabUnselected")
        
        rewardImgView.image = UIImage.init(named: "SelectedCoinTab")
    }
    
    func moreButtonSelected()  {
        
        homeContainerView.isHidden = true
        myLeagueContainerView.isHidden = true
        moreTabView.isHidden = false
        myAccountTabView.isHidden = true
        preselectedTab = 500
        rewardContainerView.isHidden = true

        moreBottomView.backgroundColor = selectedTabColor
       
        lblMore.textColor = selectedTabColor
        lblHome.textColor = UnselectedTabColor
        lblLeague.textColor = UnselectedTabColor
        lblMyAccount.textColor = UnselectedTabColor
        
        lblRewards.textColor = UnselectedTabColor

        
        lblMore1.textColor = selectedTabColor
        lblHome1.textColor = UnselectedTabColor
        lblLeague1.textColor = UnselectedTabColor
        lblMyAccount1.textColor = UnselectedTabColor

        
        headerView.showGameChangeOption(title: "More")

        homeBottomView  .isHidden = true
        leagueBottomView.isHidden = true
        myAccountBottomView.isHidden = true
        moreBottomView.isHidden = true
        
        homeBottomView1.isHidden = true
        leagueBottomView1.isHidden = true
        myAccountBottomView1.isHidden = true
        moreBottomView1.isHidden = true

        
        homeTabIcon.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon.image = UIImage.init(named: "MoreTabSelected")
        homeTabIcon1.image = UIImage.init(named: "HomeTabIconUnselected")
        leagueTabIcon1.image = UIImage.init(named: "LeagueTabUnselected")
        myAccountTabIcon1.image = UIImage.init(named: "MyAccountUnselectedTab")
        moreTabIcon1.image = UIImage.init(named: "MoreTabSelected")
        if rewardVisiableCount > 10 {
            rewardImgView.image = UIImage.init(named: "UnselectedCoinTab")
        }
        else{
            rewardImgView.image = UIImage.init(named: "SelectedCoinTab")
        }
    }
    
    @IBAction func homeButtonTapped(_ sender: Any) {

        AppxorEventHandler.logAppEvent(withName: "MatchesTabClicked", info: nil)
        if preselectedTab != 100 {
            homeContainerView.isHidden = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeButtonTapped"), object:  selectedGameType)
            homeButtonSelected()
        }
    }
    
    
    @IBAction func leagueButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "MyLeaguesTabClicked", info: nil)

        if preselectedTab != 200 {
            homeContainerView.isHidden = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leagueButtonTapped"), object: selectedGameType)
            leagueButtonSelected()
        }
    }
     
     @IBAction func rewardButtonTapped(_ sender: Any) {
        preselectedTab = 600
        updateSelectedTab()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "rewardButtonTapped"), object: SelectedTab.RewardTab.rawValue)
     }

    @IBAction func leaderboardButtonTapped(_ sender: Any?) {
        preselectedTab = 300
        updateSelectedTab()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leaderboardButtonTapped"), object:  SelectedTab.Leaderboard.rawValue)
    }
    
    @IBAction func myAccountButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "AccountTabClicked", info: nil)

        preselectedTab = 400
        updateSelectedTab()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserInfoNotification"), object: nil)
    }
    
    @IBAction func moreButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "MoreTabClicked", info: nil)

        preselectedTab = 500
        moreTabView.isHidden = false
        moreButtonSelected()
    }
    
    func updateSelectedTab() {
        
        if preselectedTab == 100{
            homeButtonSelected()
        }
        else if preselectedTab == 200{
            leagueButtonSelected()
        }
        else if preselectedTab == 300{
            leaderboardButtonSelected()
        }
        else if preselectedTab == 400{
            myAccountButtonSelected()
        }
        else if preselectedTab == 500{
            moreButtonSelected()
        }
        else if preselectedTab == 600{
            rewardButtonSelected()
        }

    }

    @objc func updateFantasyNotification(notification: Notification?)  {

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
