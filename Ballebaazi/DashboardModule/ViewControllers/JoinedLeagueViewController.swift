//
//  JoinedLeagueViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 03/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class JoinedLeagueViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, JoinedLeagueContainerCollectionViewCellDelegate {

    @IBOutlet weak var scoreViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var annuncementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var annuncementView: AnnouncementView!
    @IBOutlet weak var scoreView: TeamScoreView!
    @IBOutlet weak var headerView: LetspickFantasyWithoutWalletHeaderView!
    @IBOutlet weak var lblMyTeamTitle: UILabel!
    @IBOutlet weak var myTeamButton: CustomBorderButton!
    @IBOutlet weak var privateLeagueButton: SolidButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lblTeamCount: UILabel!
    @IBOutlet weak var tblViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var leagueCollectionView: UICollectionView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!

    lazy var isPullToRefresh = false
    lazy var isViewForLeagueFlow = false
    lazy var isNeedToRefreshLeaues = false

    lazy var userTeamsArray:Array<UserTeamDetails> = []
    lazy var selectedRow = 0
    lazy var classicTeamCount = 0
    lazy var battingTeamCount = 0
    lazy var bowlingTeamCount = 0
    lazy var reverseTeamCount = 0
    lazy var wizardTeamCount = 0
    
    lazy var firstTabFantasyType = FantasyType.Classic.rawValue;
    lazy var secondTabFantasyType = FantasyType.Batting.rawValue;
    lazy var thirdTabFantasyType = FantasyType.Bowling.rawValue;
    lazy var fourthTabFantasyType = FantasyType.Reverse.rawValue;
    lazy var fifthTabFantasyType = FantasyType.Wizard.rawValue;
    lazy var tabsArray = Array<Int>()
    var selectedFantasyType = FantasyType.Classic.rawValue
    
    
    var matchDetails: MatchDetails?
    var timer: Timer?
    var joinedLeagueDetailsArray: Array<JoinedLeagueDetails>?
    var joinedClassicLeagueDetailsArray: Array<JoinedLeagueDetails>?
    var joinedBattingLeagueDetailsArray: Array<JoinedLeagueDetails>?
    var joinedBowlingLeagueDetailsArray: Array<JoinedLeagueDetails>?
    var joinedReverseLeagueDetailsArray: Array<JoinedLeagueDetails>?
    var joinedWizardLeagueDetailsArray: Array<JoinedLeagueDetails>?

    
    
    // MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaultProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true

        if selectedFantasyType == FantasyType.Classic.rawValue {
            headerView.classicButtonTapped(isNeedToScroll: false)
        }
        else if selectedFantasyType == FantasyType.Batting.rawValue {
            headerView.battingButtonTapped(isNeedToScroll: false)
        }
        else if selectedFantasyType == FantasyType.Bowling.rawValue {
            headerView.bowlingButtonTapped(isNeedToScroll: false)
        }
        else if selectedFantasyType == FantasyType.Reverse.rawValue {
            headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
        }
        else if selectedFantasyType == FantasyType.Wizard.rawValue {
            headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
        }

        if AppHelper.isApplicationRunningOnIphoneX() {
//            bottomViewHeightConstraint.constant = 65.0;
//            bottomView.layoutIfNeeded()
        }
        else{
            view.layoutIfNeeded()
        }
        
        if isNeedToRefreshLeaues{
            callGetJoinedLeagueAPI(isNeedToShowLoader: true)
        }
        else{
            if joinedLeagueDetailsArray != nil{
                if joinedLeagueDetailsArray!.count > 0{
                    weak var weakSelf = self;
                    DispatchQueue.global().async {
                        weakSelf?.callGetJoinedLeagueAPI(isNeedToShowLoader: false)
                    }
                }
            }
        }
    }
    
    
    //MARK:- Custom Methods
    
    func setupDefaultProperties() {
        updateTabsData()

        weak var weakSelf = self
        headerView.clasicButtonTappedBlock { (status) in
            weakSelf?.classicButtonTapped(isAnimation: status)
        }
        
        headerView.battingButtonTappedBlock { (status) in
            weakSelf?.battingButtonTapped(isAnimation: status)
        }
        
        headerView.bowlingButtonTappedBlock { (status) in
            weakSelf?.bowlingButtonTapped(isAnimation: status)
        }
        
        headerView.reverseFantasyButtonTapped { (status) in
            weakSelf?.reverseButtonTapped(isAnimation: true)
        }

        headerView.wizardFantasyButtonTapped { (status) in
            weakSelf?.wizardButtonTapped(isAnimation: true)
        }

        headerView.updateMatchName(details: matchDetails)
        leagueCollectionView.register(UINib(nibName: "JoinedLeagueContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JoinedLeagueContainerCollectionViewCell")
        UserDefaults.standard.set(Date().timeIntervalSince1970 - 65, forKey: "lastRefreshedMatchDetails")
        UserDefaults.standard.synchronize()
        callGetJoinedLeagueAPI(isNeedToShowLoader: true)
        privateLeagueButton.setTitle("Create Private League".localized(), for: .normal)

        bottomView.isHidden = false
        if isViewForLeagueFlow{
            bottomView.isHidden = false
        }
        else{
            leagueCollectionView.layoutIfNeeded()
        }
        
        if matchDetails!.isMatchClosed {
            lblTeamCount.isHidden = false
            lblMyTeamTitle.isHidden = false
            privateLeagueButton.isHidden = true
            myTeamButton.isHidden = false
        }
        else{
            lblTeamCount.isHidden = true
            lblMyTeamTitle.isHidden = true
            privateLeagueButton.isHidden = false
            myTeamButton.isHidden = true
            scoreViewHeightConstraint.constant = 0;
            scoreView.isHidden = true
        }
        scoreView.layoutIfNeeded()
        
        updateTimerVlaue()
        headerView.backButtonTitle = "My Leagues".localized();
        if #available(iOS 10.0, *) {
            weak var weakSelf = self;
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    
    @objc func refereshRecordsInBackgrouns() {
        
    }
    
    @objc func removeLoader()  {
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.isPullToRefresh = false
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    // MARK:- -IBAction Methods

    func classicButtonTapped(isAnimation: Bool) {

        showDataForClassic()
        if tabsArray.count == 0{
            return
        }
        var indexPathCount = 0

        if selectedFantasyType == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)
        weak var weakSelf = self
        DispatchQueue.main.async {
            if isAnimation{
                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
            }
            else{
                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
            }
            weakSelf?.leagueCollectionView.reloadData()
        }
    }
    
    func battingButtonTapped(isAnimation: Bool) {
        showDataForBatting()
        if tabsArray.count == 0{
            return
        }

        var indexPathCount = 0

        if selectedFantasyType == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)
        weak var weakSelf = self

        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    func bowlingButtonTapped(isAnimation: Bool) {
        showDataForBowling();
        if tabsArray.count == 0{
            return
        }

        var indexPathCount = 0

        if selectedFantasyType == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)

        weak var weakSelf = self
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    func reverseButtonTapped(isAnimation: Bool) {
        showDataForReverse();
        if tabsArray.count == 0{
            return
        }

        var indexPathCount = 0

        if selectedFantasyType == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
            indexPathCount = 4
        }


        let indexPath = IndexPath(item: indexPathCount, section: 0)
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    func wizardButtonTapped(isAnimation: Bool) {
        showDataForWizard();
        var indexPathCount = 0
        if tabsArray.count == 0{
            return
        }

        if selectedFantasyType == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)
        weak var weakSelf = self
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    
//    func classicButtonTapped(isNeedToAnimate: Bool) {
//        showDataForClassic()
//        let indexPath = IndexPath(item: 0, section: 0)
//
//        weak var weakSelf = self
//        DispatchQueue.main.async {
//            if isNeedToAnimate{
//                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//            }
//            else{
//                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
//            }
//            weakSelf?.leagueCollectionView.reloadData()
//        }
//    }
//
//    func battingButtonTapped(isNeedToAnimate: Bool) {
//
//        showDataForBatting()
//
//        let indexPath = IndexPath(item: 1, section: 0)
//        weak var weakSelf = self
//        DispatchQueue.main.async {
//            if isNeedToAnimate{
//                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//            }
//            else{
//                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
//            }
//            weakSelf?.leagueCollectionView.reloadData()
//        }
//    }
//
//
//
//    func bowlingButtonTapped(isNeedToAnimate: Bool) {
//        showDataForBowling()
//
//        weak var weakSelf = self
//        let indexPath = IndexPath(item: 2, section: 0)
//        DispatchQueue.main.async {
//            if isNeedToAnimate{
//                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//            }
//            else{
//                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
//            }
//            weakSelf?.leagueCollectionView.reloadData()
//        }
//    }
//
    
    func showDataForClassic() {
        selectedFantasyType = FantasyType.Classic.rawValue
        self.lblTeamCount.text = String(self.classicTeamCount)
        if self.classicTeamCount > 1 {
            lblMyTeamTitle.text = "My Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "My Team".localized()
        }
    }
    
    
    func showDataForBatting() {
         
        selectedFantasyType = FantasyType.Batting.rawValue
        self.lblTeamCount.text = String(self.battingTeamCount)
        if self.battingTeamCount > 1 {
            lblMyTeamTitle.text = "My Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "My Team".localized()
        }
    }
    
    
    func showDataForBowling() {
        
        selectedFantasyType = FantasyType.Bowling.rawValue
        self.lblTeamCount.text = String(self.bowlingTeamCount)
        
        if self.bowlingTeamCount > 1 {
            lblMyTeamTitle.text = "My Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "My Team".localized()
        }
    }
    
    func showDataForReverse() {
        
        selectedFantasyType = FantasyType.Reverse.rawValue
        self.lblTeamCount.text = String(self.reverseTeamCount)
        
        if self.reverseTeamCount > 1 {
            lblMyTeamTitle.text = "My Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "My Team".localized()
        }
    }

    
    func showDataForWizard() {
        
        selectedFantasyType = FantasyType.Wizard.rawValue
        self.lblTeamCount.text = String(self.wizardTeamCount)
        
        if self.wizardTeamCount > 1 {
            lblMyTeamTitle.text = "My Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "My Team".localized()
        }
    }

    
    @IBAction func myTeamButtonTapped(_ sender: Any) {
       
        if matchDetails!.isMatchClosed {
            let myTeamVC = storyboard?.instantiateViewController(withIdentifier: "MyTeamsViewController") as! MyTeamsViewController
            myTeamVC.matchDetails = matchDetails
            myTeamVC.userTeamsArray = userTeamsArray
            myTeamVC.isFromJoinedLeague = true
            if selectedFantasyType == FantasyType.Classic.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Classic.rawValue
            }
            else if selectedFantasyType == FantasyType.Batting.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Batting.rawValue
            }
            else if selectedFantasyType == FantasyType.Bowling.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Bowling.rawValue
            }
            else if selectedFantasyType == FantasyType.Reverse.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Reverse.rawValue
            }
            else if selectedFantasyType == FantasyType.Wizard.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Wizard.rawValue
            }

            navigationController?.pushViewController(myTeamVC, animated: true)
        }
        else{
            
            let createPrivateLeague = storyboard?.instantiateViewController(withIdentifier: "CreatePrivateLeagueViewController") as! CreatePrivateLeagueViewController
            createPrivateLeague.matchDetails = matchDetails
            createPrivateLeague.userTeamsArray = userTeamsArray
            if self.selectedFantasyType == FantasyType.Classic.rawValue{
                createPrivateLeague.fantasyType = "1"
            }
            else if self.selectedFantasyType == FantasyType.Batting.rawValue{
                createPrivateLeague.fantasyType = "2"
            }
            else if self.selectedFantasyType == FantasyType.Bowling.rawValue{
                createPrivateLeague.fantasyType = "3"
            }
            else if self.selectedFantasyType == FantasyType.Reverse.rawValue{
                createPrivateLeague.fantasyType = "4"
            }
            else if self.selectedFantasyType == FantasyType.Wizard.rawValue{
                createPrivateLeague.fantasyType = "5"
            }
            createPrivateLeague.gameType = GameType.Cricket.rawValue
            navigationController?.pushViewController(createPrivateLeague, animated: true)
        }
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {        
        return tabsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height-8)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JoinedLeagueContainerCollectionViewCell", for: indexPath) as? JoinedLeagueContainerCollectionViewCell
        
        cell!.placeholderimgView.isHidden = true
        cell!.lblNoRecordsFound.isHidden = true
        cell!.delegate = self;
            
        
        let fantasyType = tabsArray[indexPath.row]
        
        if fantasyType == FantasyType.Classic.rawValue {
            cell?.configData(gameType: GameType.Cricket.rawValue, leagueType: FantasyType.Classic.rawValue, selectedMatchDetails: matchDetails, leagueArray: joinedClassicLeagueDetailsArray, teamsArray: userTeamsArray)
            if joinedClassicLeagueDetailsArray?.count == 0{
                cell!.lblNoRecordsFound.text = "NoJoinedClassicLeague".localized()
                cell!.placeholderimgView.isHidden = false
                cell!.lblNoRecordsFound.isHidden = false
                cell!.placeholderimgView.image = UIImage(named: "Placeholdermyleagueclassicmage")
            }
        }
        else if fantasyType == FantasyType.Batting.rawValue {
            cell?.configData(gameType: GameType.Cricket.rawValue, leagueType: FantasyType.Batting.rawValue, selectedMatchDetails: matchDetails, leagueArray: joinedBattingLeagueDetailsArray, teamsArray: userTeamsArray)

            if joinedBattingLeagueDetailsArray?.count == 0{
                cell!.lblNoRecordsFound.text = "NoJoinedBattingLeague".localized()
                cell!.placeholderimgView.isHidden = false
                cell!.lblNoRecordsFound.isHidden = false
                cell!.placeholderimgView.image = UIImage(named: "PlaceholdermyleagueBattingImage")
            }
        }
        else if fantasyType == FantasyType.Bowling.rawValue {
            cell?.configData(gameType: GameType.Cricket.rawValue, leagueType: FantasyType.Bowling.rawValue, selectedMatchDetails: matchDetails, leagueArray: joinedBowlingLeagueDetailsArray, teamsArray: userTeamsArray)

            if joinedBowlingLeagueDetailsArray?.count == 0{
                cell!.lblNoRecordsFound.text = "NoJoinedBowlingLeague".localized()
                cell!.placeholderimgView.isHidden = false
                cell!.lblNoRecordsFound.isHidden = false
                cell!.placeholderimgView.image = UIImage(named: "BowlingPlaceholder")
            }
        }
        else if fantasyType == FantasyType.Reverse.rawValue {
            cell?.configData(gameType: GameType.Cricket.rawValue, leagueType: FantasyType.Reverse.rawValue, selectedMatchDetails: matchDetails, leagueArray: joinedReverseLeagueDetailsArray, teamsArray: userTeamsArray)

            if joinedReverseLeagueDetailsArray?.count == 0{
                cell!.lblNoRecordsFound.text = "NoJoinedReverseLeague".localized()
                cell!.placeholderimgView.isHidden = false
                cell!.lblNoRecordsFound.isHidden = false
                cell!.placeholderimgView.image = UIImage(named: "Placeholdermyleagueclassicmage")
            }
        }
        else if fantasyType == FantasyType.Wizard.rawValue {
            cell?.configData(gameType: GameType.Cricket.rawValue, leagueType: FantasyType.Wizard.rawValue, selectedMatchDetails: matchDetails, leagueArray: joinedWizardLeagueDetailsArray, teamsArray: userTeamsArray)

            if joinedWizardLeagueDetailsArray?.count == 0{
                cell!.lblNoRecordsFound.text = "NoJoinedWizardLeague".localized()
                cell!.placeholderimgView.isHidden = false
                cell!.lblNoRecordsFound.isHidden = false
                cell!.placeholderimgView.image = UIImage(named: "Placeholdermyleagueclassicmage")
            }

        }
        
        return cell!;
    }
    
    //MARK:-  API Releated Method
    func callGetJoinedLeagueAPI(isNeedToShowLoader: Bool){        
        
        if matchDetails?.matchStatus == "started"  {
            let lastRefreshedMatchDetails: TimeInterval  = UserDefaults.standard.value(forKey: "lastRefreshedMatchDetails") as? TimeInterval ?? Date().timeIntervalSince1970
            let currentTimetemp = Date().timeIntervalSince1970
            let timeDefference = currentTimetemp - lastRefreshedMatchDetails
            
            if timeDefference < 60 {
                AppHelper.sharedInstance.displaySpinner()
                let delayInSec = 3.0
                weak var weakSelf = self
                DispatchQueue.main.asyncAfter(deadline: .now() + delayInSec) {
                    weakSelf?.removeLoader()
                }
                return;
            }
        }

        
        if isNeedToShowLoader{
            if !AppHelper.isInterNetConnectionAvailable(){
                isPullToRefresh = false
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let parameters: Parameters = ["option": "get_contest", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kSocresUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            DispatchQueue.main.async {
                if weakSelf?.matchDetails?.matchStatus == "started"  {
                    UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "lastRefreshedMatchDetails")
                    UserDefaults.standard.synchronize()
                }

                weakSelf?.isPullToRefresh = false
                if result != nil{
                    
                    let statusCode = result!["status"]?.string
                    if statusCode == "200"{
                        
                        if let response = result!["response"]?.dictionary{
                            if let scoreDetailsString = response["match_innings"]?.string{
                                
                                var firstTeamName = ""
                                var firstTeamScore = ""
                                var secondTeamName = ""
                                var secondTeamScore = ""
                                var thirdTeamScore = ""
                                var fourthTeamScore = ""
                                
                                var firstTeamOver = ""
                                var secondTeamOver = ""
                                var thirdTeamOver = ""
                                var fourthTeamOver = ""
                                
                                let json = JSON(parseJSON: scoreDetailsString)
                                if (json.array != nil){
                                    let scoreDetailsArray = json.array!
                                    for index in 0 ..< scoreDetailsArray.count{
                                        let scoreDetails = scoreDetailsArray[index]

                                        if index == 3{
                                            let teamName = scoreDetails["team_short_name"].string ?? ""
                                            let run = scoreDetails["runs"].stringValue
                                            let wicket = scoreDetails["wickets"].stringValue
                                            let overs = scoreDetails["overs"].stringValue
                                            let teamScore = run + "-" + wicket
                                            fourthTeamOver = overs
                                            if teamName == firstTeamName{
                                                thirdTeamScore = teamScore
                                            }
                                            else{
                                                fourthTeamScore = teamScore
                                            }

                                            weakSelf?.matchDetails?.isViewForTestMatch = true
                                        }
                                        else if index == 2{
                                            let teamName = scoreDetails["team_short_name"].string ?? ""
                                            let run = scoreDetails["runs"].stringValue
                                            let wicket = scoreDetails["wickets"].stringValue
                                            let overs = scoreDetails["overs"].stringValue
                                            let teamScore = run + "-" + wicket
                                            thirdTeamOver = overs

                                            if teamName == firstTeamName{
                                                thirdTeamScore = teamScore
                                            }
                                            else{
                                                fourthTeamScore = teamScore
                                            }
                                            weakSelf?.matchDetails?.isViewForTestMatch = true
                                            weakSelf?.headerView?.layoutIfNeeded()
                                        }
                                        else if index == 1{
                                            firstTeamName = scoreDetails["team_short_name"].string ?? ""
                                            let run = scoreDetails["runs"].stringValue
                                            let wicket = scoreDetails["wickets"].stringValue
                                            let overs = scoreDetails["overs"].stringValue
                                            firstTeamScore = run + "-" + wicket
                                            firstTeamOver = overs
                                        }
                                        else if index == 0{
                                            let run = scoreDetails["runs"].stringValue
                                            let wicket = scoreDetails["wickets"].stringValue
                                            let overs = scoreDetails["overs"].stringValue
                                            if  Double(overs)! > 0.0 {
                                                secondTeamName = scoreDetails["team_short_name"].string ?? ""
                                                secondTeamScore = run + "-" + wicket
                                                secondTeamOver = overs
                                            }
                                        }
                                    }
                                    
                                    weakSelf?.matchDetails?.firstTeamOver = firstTeamOver
                                    weakSelf?.matchDetails?.secondTeamOver = secondTeamOver
                                    weakSelf?.matchDetails?.thirdTeamOver = thirdTeamOver
                                    weakSelf?.matchDetails?.fourthTeamOver = fourthTeamOver
                                    weakSelf?.matchDetails?.thirdTeamLiveScore = thirdTeamScore
                                    weakSelf?.matchDetails?.fourthTeamLiveScore = fourthTeamScore
                                    weakSelf?.matchDetails?.firstTeamLiveScoreShortName = firstTeamName
                                    weakSelf?.matchDetails?.secondTeamLiveScoreShortName = secondTeamName
                                    weakSelf?.matchDetails?.thirdTeamLiveScore = thirdTeamScore
                                    weakSelf?.matchDetails?.fourthTeamLiveScore = fourthTeamScore

                                    weakSelf?.matchDetails?.firstTeamLiveScore = firstTeamScore
                                    weakSelf?.matchDetails?.secondTeamLiveScore = secondTeamScore
                                    weakSelf?.scoreView.showTeamScore(details: self.matchDetails!)
                                }
                            }
                            
                            if let selectedmatch = response["selected_match"]?.dictionary{
                                let matchFantasyType = selectedmatch["match_fantasy_type"]?.string ?? ""
                                if matchFantasyType != "" {
                                    weakSelf?.matchDetails?.matchFantasyType = matchFantasyType
                                }
                            }
                        }

                        self.updateTabsData()
                        self.showScoreView()
                        weakSelf?.joinedLeagueDetailsArray = JoinedLeagueDetails.getJoinedLeageList(result: result!)
                        weakSelf?.userTeamsArray = JoinedLeagueDetails.getUserTeamsList(result: result!, matchDetails: weakSelf!.matchDetails!)

                        if weakSelf!.isViewForLeagueFlow{
                            weakSelf!.lblTeamCount.text = String(weakSelf!.userTeamsArray.count)
                            let classicTeamArray = weakSelf!.userTeamsArray.filter({ (teamDetails) -> Bool in
                                teamDetails.fantasyType == "1"
                            })
                            
                            let battingTeamArray = weakSelf!.userTeamsArray.filter({ (teamDetails) -> Bool in
                                teamDetails.fantasyType == "2"
                            })
                            
                            let bowlingTeamArray = weakSelf!.userTeamsArray.filter({ (teamDetails) -> Bool in
                                teamDetails.fantasyType == "3"
                            })
                            
                            let reverseTeamArray = weakSelf!.userTeamsArray.filter({ (teamDetails) -> Bool in
                                teamDetails.fantasyType == "4"
                            })
                            
                            let wizardTeamArray = weakSelf!.userTeamsArray.filter({ (teamDetails) -> Bool in
                                teamDetails.fantasyType == "5"
                            })
                            
                            weakSelf!.classicTeamCount = classicTeamArray.count
                            weakSelf!.battingTeamCount = battingTeamArray.count
                            weakSelf!.bowlingTeamCount = bowlingTeamArray.count
                            weakSelf!.reverseTeamCount = reverseTeamArray.count
                            weakSelf!.wizardTeamCount = wizardTeamArray.count

                            weakSelf!.lblTeamCount.text = String(weakSelf!.classicTeamCount)
                        }
                        
                        weakSelf?.joinedClassicLeagueDetailsArray = weakSelf?.joinedLeagueDetailsArray?.filter({ (details) -> Bool in
                            details.fantasyType == "1"
                        })
                        weakSelf?.joinedBattingLeagueDetailsArray = weakSelf?.joinedLeagueDetailsArray?.filter({ (details) -> Bool in
                            details.fantasyType == "2"
                        })
                        
                        weakSelf?.joinedBowlingLeagueDetailsArray = weakSelf?.joinedLeagueDetailsArray?.filter({ (details) -> Bool in
                            details.fantasyType == "3"
                        })
                        
                        weakSelf?.joinedReverseLeagueDetailsArray = weakSelf?.joinedLeagueDetailsArray?.filter({ (details) -> Bool in
                            details.fantasyType == "4"
                        })

                        weakSelf?.joinedWizardLeagueDetailsArray = weakSelf?.joinedLeagueDetailsArray?.filter({ (details) -> Bool in
                            details.fantasyType == "5"
                        })

                    
                        DispatchQueue.main.async {
                            if weakSelf?.selectedFantasyType == FantasyType.Classic.rawValue{
                                weakSelf?.headerView.classicButtonTapped(isNeedToScroll: false)
                            }
                            else if weakSelf?.selectedFantasyType == FantasyType.Batting.rawValue{
                                weakSelf?.headerView.battingButtonTapped(isNeedToScroll: false)
                            }
                            else if weakSelf?.selectedFantasyType == FantasyType.Bowling.rawValue{
                                weakSelf?.headerView.bowlingButtonTapped(isNeedToScroll: false)
                            }
                            else if weakSelf?.selectedFantasyType == FantasyType.Reverse.rawValue{
                                weakSelf?.headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
                            }
                            else if weakSelf?.selectedFantasyType == FantasyType.Wizard.rawValue{
                                weakSelf?.headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
                            }

                            weakSelf?.leagueCollectionView.reloadData()
                        }
                    
                        AppHelper.sharedInstance.removeSpinner()
                    }
                    else{
                        AppHelper.sharedInstance.removeSpinner()
                        let message = result!["message"]?.string ?? "kErrorMsg".localized()
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
                else{
                    AppHelper.sharedInstance.removeSpinner()
                    AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                }
            }
        }
    }
    
    //MARK:- ScrollView Delegate
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
                
        if leagueCollectionView == scrollView{
            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
            if Int(currentPage) == 0{
                let fansatyType = tabsArray[0]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 1{
                let fansatyType = tabsArray[1]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 2{
                let fansatyType = tabsArray[2]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 3{
                let fansatyType = tabsArray[3]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 4{
                let fansatyType = tabsArray[4]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 5{
                let fansatyType = tabsArray[5]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }

            updateHeaderOnscroll(gameType: selectedFantasyType)
        }
    }
    
    func updateHeaderOnscroll(gameType: Int) {
        if gameType == FantasyType.Classic.rawValue{
            headerView.classicButtonTapped(isNeedToScroll: false)
        }
        else if gameType == FantasyType.Batting.rawValue{
            headerView.battingButtonTapped(isNeedToScroll: false)
        }
        else if gameType == FantasyType.Bowling.rawValue{
            headerView.bowlingButtonTapped(isNeedToScroll: false)
        }
        else if gameType == FantasyType.Reverse.rawValue{
            headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
        }
        else if gameType == FantasyType.Wizard.rawValue{
            headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
        }
    }
    
    func updateTabsData() {
        headerView.matchFantasyType = matchDetails?.matchFantasyType ?? ""
        headerView.isFantasyModeEnable = false
        headerView.updateTabsData()
        (firstTabFantasyType, secondTabFantasyType, thirdTabFantasyType, fourthTabFantasyType, fifthTabFantasyType) = AppHelper.getFantasyLandingOrder(fantasyType: matchDetails?.matchFantasyType ?? "")

        tabsArray.removeAll()
        if firstTabFantasyType != FantasyType.None.rawValue {
            tabsArray.append(firstTabFantasyType)
        }

        if secondTabFantasyType != FantasyType.None.rawValue {
            tabsArray.append(secondTabFantasyType)
        }

        if thirdTabFantasyType != FantasyType.None.rawValue {
            tabsArray.append(thirdTabFantasyType)
        }

        if fourthTabFantasyType != FantasyType.None.rawValue {
            tabsArray.append(fourthTabFantasyType)
        }

        if fifthTabFantasyType != FantasyType.None.rawValue {
            tabsArray.append(fifthTabFantasyType)
        }

        if tabsArray.count > 1 {
            headerViewHeightConstraint.constant = 90
            headerView.layoutIfNeeded()
            headerView.isFantasyModeEnable = true
        }

        headerView.updateMatchName(details: matchDetails)
        leagueCollectionView.reloadData()
    }

    
    func refereshLeaguesData() {
        if !isPullToRefresh{
            isPullToRefresh = true
            callGetJoinedLeagueAPI(isNeedToShowLoader: true)
        }
    }
    
    //MARK:- Timer Handler
    func startRecordsRefershTimer() {
        
        if (self.matchDetails!.isMatchClosed) && (self.matchDetails?.matchStatus == "notstarted"){
            self.updateTimerVlaue()
        }
        else{
            self.refereshRecordsInBackgrouns()
        }
    }

    @objc func updateTimerVlaue(){
        
        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(details: matchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            if matchDetails?.active != "4" {
                if remainingTime <= 0{
                    matchDetails!.isMatchClosed = true
                    callGetJoinedLeagueAPI(isNeedToShowLoader: true)
                }
            }
        }
    }

    func callLeagueValidationAPI(leagueDetails: LeagueDetails, categoryName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "league_prev_data_v2","check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType, "is_private": "1"]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                                        
//                                        let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = weakSelf!.matchDetails
//                                        addCashVC?.userTeamArray = teamArray
//                                        addCashVC?.categoryName = categoryName
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                    playerVC.matchDetails = weakSelf?.matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails, userTeamArray: teamArray, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                playerVC.matchDetails = weakSelf?.matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                
                            }
                        }
                        else{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, ticketDetails: TicketDetails?)  {
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let joinedLeagueConfirmVC = storyboard.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
            joinedLeagueConfirmVC?.leagueDetails = leagueDetails
            joinedLeagueConfirmVC?.ticketDetails = ticketDetails
            joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
            joinedLeagueConfirmVC?.matchDetails = self.matchDetails
            joinedLeagueConfirmVC?.leagueCategoryName = kPrivateLeague
            joinedLeagueConfirmVC?.isNeedToShowMatchClosePopup = true
            navigationVC.pushViewController(joinedLeagueConfirmVC!, animated: true)
        }
    }

    func showScoreView() {
        if (matchDetails?.firstTeamLiveScore.count != 0) && (matchDetails?.secondTeamLiveScore.count != 0) && (matchDetails?.thirdTeamLiveScore.count != 0){
            self.scoreViewHeightConstraint.constant = 110
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        else  if (matchDetails?.firstTeamLiveScore.count != 0) && (matchDetails?.secondTeamLiveScore.count != 0) && (matchDetails?.fourthTeamLiveScore.count != 0){
            self.scoreViewHeightConstraint.constant = 110
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        else if (matchDetails?.firstTeamLiveScoreShortName.count == 0) && (matchDetails?.secondTeamLiveScoreShortName.count == 0){
            self.scoreViewHeightConstraint.constant = 0
            self.scoreView.isHidden = true
            self.scoreView.layoutIfNeeded()
        }
        else{
            self.scoreViewHeightConstraint.constant = 50.0
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }

    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
