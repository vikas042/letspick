//
//  SessionPassesViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class SessionPassesViewController: UIViewController {

    @IBOutlet weak var placeholderImgView: UIImageView!
    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblNoDatafound: UILabel!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var topMessageView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!

    var passesArray = Array<PassesDetails>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topMessageView.isHidden = true
        lblNoDatafound.text = "no_passes_found".localized()
        headerView.headerTitle = "Pass Store".localized()
        collectionView.register(UINib(nibName: "SeasonPassCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SeasonPassCollectionViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callGetPassesStoreAPI(isNeedToShowLoader: true)
    }
    
    func callGetPassesStoreAPI(isNeedToShowLoader: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }

        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }

        let params = ["option":"get_passes", "user_id": UserDetails.sharedInstance.userID]
        
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kPassesURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary{
                        if let message = response["passStoreHeaderMessage"]?.string {
                            weakSelf?.lblMessage.text = message
                            AppHelper.showShodowOnCellsView(innerView: weakSelf!.topMessageView)
                        }
                        if let userPassArray = response["user_pass"]?.array {
                            if userPassArray.count > 0 {
                                weakSelf?.topMessageView.isHidden = false
                            }
                            else{
                                weakSelf?.topMessageView.isHidden = true
                                weakSelf?.collectionViewTopConstraint.constant = 10
                                weakSelf?.collectionView.layoutIfNeeded()
                            }
                        }
                        else{
                            weakSelf?.topMessageView.isHidden = true
                            weakSelf?.collectionViewTopConstraint.constant = 10
                            weakSelf?.collectionView.layoutIfNeeded()
                        }

                        if let dataArray = response["pass_list"]?.array {
                            weakSelf?.passesArray = PassesDetails.getAllPasesDetails(dataArray: dataArray)
                            if dataArray.count > 0 {
                                weakSelf?.lblNoDatafound.isHidden = true
                                weakSelf?.placeholderImgView.isHidden = true
                                weakSelf?.collectionView.isHidden = false
                            }
                            else{
                                weakSelf?.lblNoDatafound.isHidden = false
                                weakSelf?.placeholderImgView.isHidden = false
                                weakSelf?.collectionView.isHidden = true
                            }
                            weakSelf?.collectionView.reloadData()
                        }
                        else{
                            weakSelf?.passesArray.removeAll()
                            weakSelf?.lblNoDatafound.isHidden = false
                            weakSelf?.placeholderImgView.isHidden = false
                            weakSelf?.collectionView.isHidden = true
                            weakSelf?.collectionView.reloadData()
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }

}

extension SessionPassesViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return passesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.frame.size.width/2
        return CGSize(width: cellWidth, height: cellWidth + 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeasonPassCollectionViewCell", for: indexPath) as! SeasonPassCollectionViewCell
        let details = passesArray[indexPath.item]
        cell.purchaseButton.tag = indexPath.row
        cell.purchaseButton.addTarget(self, action: #selector(purchasePassButtonTapped), for: .touchUpInside)
        cell.configData(details: details, isPurchased: false)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let details = passesArray[indexPath.item]

        let passDetailsVC = storyboard?.instantiateViewController(withIdentifier: "PassDetailsViewController") as! PassDetailsViewController
        passDetailsVC.passesDetails = details
        navigationController?.pushViewController(passDetailsVC, animated: true)
    }
    
    @objc func purchasePassButtonTapped(button: UIButton)  {
        
        let alert = UIAlertController(title: kAlert, message: "Are you sure, you want to purchase this pass?".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
           
            let details = self.passesArray[button.tag]
            let amount = Float(details.passPrice) ?? 0
            if amount <= (Float(UserDetails.sharedInstance.totalCredits) ?? 0) {
                self.callPurchasePassAPI(details: details)
            }
            else{
                self.callAddAmountAPI(paymentType: "1", addCashAmount: details.passPrice)
            }
        }))
        alert.addAction(UIAlertAction(title: "No".localized(), style: UIAlertActionStyle.default, handler: nil))
        navigationController?.present(alert, animated: true, completion: nil)
    }
    
    func callPurchasePassAPI(details: PassesDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "purchase_pass", "user_id": UserDetails.sharedInstance.userID, "pass_id": details.passId]
        
        let weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kPassesURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let status = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if status == "200"{
                    var message = "You have successfully received a Season Pass with \(details.totalLeagueEntries) Entries"
                    if details.totalLeagueEntries == "1" {
                        message = "You have successfully received a Season Pass with 1 Single Entry"
                    }
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            if details.totalLeagueEntries == "1" {
                                message = "You have successfully received a Season Pass with 1 Single Entry"
                            }
                            else{
                                message = "आपको \(details.totalLeagueEntries) एंट्रीज के साथ सीज़न पास सफलतापूर्वक मिला है"
                            }
                        }
                    }

                    AppHelper.showAlertView(message: message, isErrorMessage: false)
                    DispatchQueue.main.async {
                        weakSelf.callGetPassesStoreAPI(isNeedToShowLoader: false)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                    AppHelper.sharedInstance.removeSpinner()
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
    
    //MARK: API Related Methods
    func callAddAmountAPI(paymentType: String, addCashAmount: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "add_cash", "user_id": UserDetails.sharedInstance.userID, "pg_type": paymentType, "amount": addCashAmount, "url": "home", "promo": "", "is_mobile": "1", "gateway_version": "1"]
        if paymentType == "1" {
            MixPanelEventsDetails.depositInitiated(amountInitiated: addCashAmount, paymentMethod: "PAYU")
        }
        else if paymentType == "2"{
            MixPanelEventsDetails.depositInitiated(amountInitiated: addCashAmount, paymentMethod: "PAYTM")
        }
        
        let weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let status = result!["status"]?.string
                if status == "200"{
                    if paymentType == "2"{
//                        weakSelf.sharePayTmPaymentDetails(response: result!["response"]!)
                    }
                    else{
                        weakSelf.genrateRequiredHtml(response: (result!["response"]?.dictionary!)!, paymentType: paymentType, addCashAmount: addCashAmount)
                    }
                }
                else{
                    
                    if let message = result!["message"]?.string{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func genrateRequiredHtml(response: [String: JSON], paymentType: String, addCashAmount: String) {
        var paramsString = ""
        var actionString = ""
        for (key, value) in response {
            if key == "gateway_type"{
                continue
            }
            if key == "action"{
                actionString = value.string!
                continue
            }

            if let keyValue = value.string{
                paramsString += "<input  type=\u{22}hidden\u{22} name=\u{22}" + key + "\u{22} value=\u{22}" + keyValue + "\u{22}>\n"
            }
        }
        
        let htmlString = "<body OnLoad=\u{22}document.payForm.submit();\u{22} ><form method=\u{22}post\u{22} action=\u{22}" + actionString + "\u{22} name=\u{22}payForm\u{22}>" + paramsString + "</form></body>"
        let loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let paymentWebVC = loginStoryboard.instantiateViewController(withIdentifier: "PaymentWebViewController") as? PaymentWebViewController
        paymentWebVC?.htmlString = htmlString

        if paymentType == "2" {
            paymentWebVC?.isWebViewForPaytm = true
        }
        
        paymentWebVC!.paymentAmount = addCashAmount
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            DispatchQueue.main.async {
                navigationVC.pushViewController(paymentWebVC!, animated: true)
            }
        }
    }
    

}
