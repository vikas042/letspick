//
//  FloatingViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/03/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class FloatingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        AppHelper.addBackButtonOnNavigationbar(title: "Leaderboard Info", target: self, isNeedToShowShadow: true)
    }
}
