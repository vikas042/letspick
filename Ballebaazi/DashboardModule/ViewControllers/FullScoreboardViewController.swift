//
//  FullScoreboardViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class FullScoreboardViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var headerView: CustomNavigationBar!
    var matchArray = Array<LiveScoreDetails>()
    var matchKey = ""
    var selectedCell = VisiableTeam.FirstTeam.rawValue
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerView.headerTitle = "Scorecard".localized()
        
        navigationController?.navigationBar.isHidden = true
        tblView.register(UINib(nibName: "FullScorcardTableViewCell", bundle: nil), forCellReuseIdentifier: "FullScorcardTableViewCell")
        tblView.register(UINib(nibName: "TotalFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "TotalFullscoreTableViewCell")
        refreshControl.addTarget(self, action: #selector(refreshScrore), for: UIControlEvents.valueChanged)
        tblView.addSubview(refreshControl)
        
        callGetFullScorecardAPI(isNeedToShowLoader: true)
    }
    
    @objc func refreshScrore() {
        callGetFullScorecardAPI(isNeedToShowLoader: false)
        
    }
    func callGetFullScorecardAPI(isNeedToShowLoader: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader  {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let params = ["option": "match_scoreboard", "match_key": matchKey, "user_id": UserDetails.sharedInstance.userID]
        
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kSocrescardUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            weakSelf!.refreshControl.endRefreshing()

            if result != nil{
                
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let matchDetails = result!["response"]{
                        weakSelf!.matchArray = LiveScoreDetails.getLiveScoreDetails(liveScoreArray: [matchDetails])
                        weakSelf!.tblView.reloadData()
                    }
                }
                else{
                    let message = result!["message"]?.string ?? "kErrorMsg".localized()
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
        
    }
    
    //MARK:- Table view Data and Delegats
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if matchArray.count > 0 {
            let liveScoreDetails = matchArray[0]
            if (liveScoreDetails.firstTeamFullName.count != 0) && (liveScoreDetails.secondTeamFullName.count != 0) && (liveScoreDetails.thirdTeamFullName.count != 0) && (liveScoreDetails.fourthTeamFullName.count != 0){
                return 4
            }
            else if (liveScoreDetails.firstTeamFullName.count != 0) && (liveScoreDetails.secondTeamFullName.count != 0) && (liveScoreDetails.thirdTeamFullName.count != 0){
                return 3
            }
            else if (liveScoreDetails.firstTeamFullName.count != 0) && (liveScoreDetails.secondTeamFullName.count != 0){
                return 2
            }
        }
        
        return matchArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let liveScoreDetails = matchArray[0]

        if indexPath.row == 0 {
            if selectedCell == VisiableTeam.FirstTeam.rawValue {
            
                let deviceWidth = view.frame.size.width
                let textHeight = liveScoreDetails.firstTeamDoNotBats.height(withConstrainedWidth: deviceWidth - 123, font: UIFont(name: "OpenSans", size: 15)!)
                let fallofWicketTextHeight = liveScoreDetails.firstTeamFallOfWickets.height(withConstrainedWidth: deviceWidth - 40, font: UIFont(name: "OpenSans", size: 15)!) + 38
                
                let height = 220 + CGFloat(liveScoreDetails.firstTeamBatsmanArray.count * 50) + fallofWicketTextHeight +  CGFloat(liveScoreDetails.firstTeamBowlerArray.count * 34) + textHeight

                return height + 30 + 27 + 22
            }
            else{
                return 65
            }
        }
        else if indexPath.row == 1{
            
            if selectedCell == VisiableTeam.SecondTeam.rawValue {
                
                let deviceWidth = view.frame.size.width
                let textHeight = liveScoreDetails.secondTeamDoNotBats.height(withConstrainedWidth: deviceWidth - 123, font: UIFont(name: "OpenSans", size: 15)!)
                let fallofWicketTextHeight = liveScoreDetails.secondTeamFallOfWickets.height(withConstrainedWidth: deviceWidth - 40, font: UIFont(name: "OpenSans", size: 15)!) + 38
                let height = 220 + CGFloat(liveScoreDetails.secodsTeamBatsmanArray.count * 50) + fallofWicketTextHeight +  CGFloat(liveScoreDetails.secondTeamBowlerArray.count * 34) + textHeight
                
                return height + 30 + 27 + 22
            }
            else{
                return 65
            }
        }
        else if indexPath.row == 2{
            
            if selectedCell == VisiableTeam.ThirdTeam.rawValue {
                
                let deviceWidth = view.frame.size.width
                let textHeight = liveScoreDetails.thirdTeamDoNotBats.height(withConstrainedWidth: deviceWidth - 123, font: UIFont(name: "OpenSans", size: 15)!)
                let fallofWicketTextHeight = liveScoreDetails.thirdTeamFallOfWickets.height(withConstrainedWidth: deviceWidth - 40, font: UIFont(name: "OpenSans", size: 15)!) + 38
                let height = 220 + CGFloat(liveScoreDetails.thirdTeamBatsmanArray.count * 50) + fallofWicketTextHeight +  CGFloat(liveScoreDetails.thirdTeamBowlerArray.count * 34) + textHeight
                
                return height + 30 + 27 + 22
            }
            else{
                return 65
            }
        }
        else if indexPath.row == 3{
            
            if selectedCell == VisiableTeam.FourthTeam.rawValue {
                
                let deviceWidth = view.frame.size.width
                let textHeight = liveScoreDetails.fourthTeamDoNotBats.height(withConstrainedWidth: deviceWidth - 123, font: UIFont(name: "OpenSans", size: 15)!)
                let fallofWicketTextHeight = liveScoreDetails.fourthTeamFallOfWickets.height(withConstrainedWidth: deviceWidth - 40, font: UIFont(name: "OpenSans", size: 15)!) + 38
                let height = 220 + CGFloat(liveScoreDetails.fourthTeamBatsmanArray.count * 50) + fallofWicketTextHeight +  CGFloat(liveScoreDetails.fourthTeamBowlerArray.count * 34) + textHeight
                
                return height + 30 + 27 + 22
            }
            else{
                return 65
            }
        }
        
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "FullScorcardTableViewCell") as? FullScorcardTableViewCell
        
        if cell == nil {
            cell = FullScorcardTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "FullScorcardTableViewCell")
        }
        cell!.dropDownButton.tag = indexPath.row
        cell!.dropDownButton.addTarget(self, action: #selector(self.dropDownButtonTapped(button:)), for: .touchUpInside)

        if indexPath.row == 0 {
            if selectedCell == VisiableTeam.FirstTeam.rawValue {
                cell!.dropdownArrow.image = #imageLiteral(resourceName: "UpArraowForPlayerSelection")
                cell!.fallOfWicketView.isHidden = false
                cell!.batsmanView.isHidden = false
                cell!.bowlerView.isHidden = false
            }
            else{
                cell!.dropdownArrow.image = #imageLiteral(resourceName: "DownArraowForPlayerSelection")

                cell!.fallOfWicketView.isHidden = true
                cell!.batsmanView.isHidden = true
                cell!.bowlerView.isHidden = true
            }
        }
        else if indexPath.row == 1{
            
            if selectedCell == VisiableTeam.SecondTeam.rawValue {
                cell!.dropdownArrow.image = #imageLiteral(resourceName: "UpArraowForPlayerSelection")
                cell!.fallOfWicketView.isHidden = false
                cell!.batsmanView.isHidden = false
                cell!.bowlerView.isHidden = false
                
            }
            else{
                cell!.dropdownArrow.image = #imageLiteral(resourceName: "DownArraowForPlayerSelection")
                cell!.fallOfWicketView.isHidden = true
                cell!.batsmanView.isHidden = true
                cell!.bowlerView.isHidden = true
            }
        }
        else if indexPath.row == 2{
            
            if selectedCell == VisiableTeam.ThirdTeam.rawValue {
                cell!.dropdownArrow.image = #imageLiteral(resourceName: "UpArraowForPlayerSelection")
                cell!.fallOfWicketView.isHidden = false
                cell!.batsmanView.isHidden = false
                cell!.bowlerView.isHidden = false
                
            }
            else{
                cell!.dropdownArrow.image = #imageLiteral(resourceName: "DownArraowForPlayerSelection")
                cell!.fallOfWicketView.isHidden = true
                cell!.batsmanView.isHidden = true
                cell!.bowlerView.isHidden = true
            }
        }
        else if indexPath.row == 3{
            
            if selectedCell == VisiableTeam.FourthTeam.rawValue {
                cell!.dropdownArrow.image = #imageLiteral(resourceName: "UpArraowForPlayerSelection")
                cell!.fallOfWicketView.isHidden = false
                cell!.batsmanView.isHidden = false
                cell!.bowlerView.isHidden = false
                
            }
            else{
                cell!.dropdownArrow.image = #imageLiteral(resourceName: "DownArraowForPlayerSelection")
                cell!.fallOfWicketView.isHidden = true
                cell!.batsmanView.isHidden = true
                cell!.bowlerView.isHidden = true
            }
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        let liveScoreDetails = matchArray[0]
        
        if indexPath.row == 0 {
            cell?.firstTeamData(liveScoreDetails: liveScoreDetails)
        }
        else if indexPath.row == 1{
            cell?.secondTeamData(liveScoreDetails: liveScoreDetails)
        }
        else if indexPath.row == 2{
            cell?.thirdTeamData(liveScoreDetails: liveScoreDetails)
        }
        else if indexPath.row == 3{
            cell?.fourthTeamData(liveScoreDetails: liveScoreDetails)
        }

        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    @objc func dropDownButtonTapped(button: UIButton)  {
        weak var weakSelf = self

        if button.tag == 0 {
            if (selectedCell == VisiableTeam.FirstTeam.rawValue) {
                
                let currentIndexPath = IndexPath(row: 0, section: 0)
                
                selectedCell = 10000000
                tblView.beginUpdates()
                tblView.reloadRows(at: [currentIndexPath], with: .none)
                tblView.endUpdates()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    weakSelf!.tblView.reloadData()
                }
                
                return
            }
            
            selectedCell = VisiableTeam.FirstTeam.rawValue
            let currentIndexPath = IndexPath(row: 0, section: 0)
            
            tblView.beginUpdates()
            tblView.reloadRows(at: [currentIndexPath], with: .none)
            tblView.endUpdates()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                weakSelf?.tblView.reloadData()
            }
            
        }
        else if button.tag == 1 {
            if (selectedCell == VisiableTeam.SecondTeam.rawValue){
                
                let currentIndexPath = IndexPath(row: 1, section: 0)
                
                selectedCell = 10000000
                tblView.beginUpdates()
                tblView.reloadRows(at: [currentIndexPath], with: .none)
                tblView.endUpdates()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    weakSelf?.tblView.reloadData()
                }
                return
            }
            
            selectedCell = VisiableTeam.SecondTeam.rawValue
            
            let currentIndexPath = IndexPath(row: 1, section: 0)
            
            tblView.beginUpdates()
            tblView.reloadRows(at: [currentIndexPath], with: .none)
            tblView.endUpdates()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                weakSelf?.tblView.reloadData()
            }
        }
        else if button.tag == 2 {
            if (selectedCell == VisiableTeam.ThirdTeam.rawValue){
                
                let currentIndexPath = IndexPath(row: 2, section: 0)
                
                selectedCell = 10000000
                tblView.beginUpdates()
                tblView.reloadRows(at: [currentIndexPath], with: .none)
                tblView.endUpdates()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    weakSelf?.tblView.reloadData()
                }
                return
            }
            
            selectedCell = VisiableTeam.ThirdTeam.rawValue
            
            let currentIndexPath = IndexPath(row: 2, section: 0)
            
            tblView.beginUpdates()
            tblView.reloadRows(at: [currentIndexPath], with: .none)
            tblView.endUpdates()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                weakSelf?.tblView.reloadData()
            }
        }
        else if button.tag == 3 {
            if (selectedCell == VisiableTeam.FourthTeam.rawValue){
                
                let currentIndexPath = IndexPath(row: 3, section: 0)
                
                selectedCell = 10000000
                tblView.beginUpdates()
                tblView.reloadRows(at: [currentIndexPath], with: .none)
                tblView.endUpdates()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    weakSelf?.tblView.reloadData()
                }
                return
            }
            
            selectedCell = VisiableTeam.FourthTeam.rawValue
            
            let currentIndexPath = IndexPath(row: 3, section: 0)
            
            tblView.beginUpdates()
            tblView.reloadRows(at: [currentIndexPath], with: .none)
            tblView.endUpdates()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                weakSelf?.tblView.reloadData()
            }
        }
        tblView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
