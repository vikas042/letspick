//
//  SelectPlayerViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/8/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit


let kPlayerConditionNotSatisfyColor = UIColor(red: 215.0/255, green: 51.0/255 , blue: 97.0/255, alpha: 1)

let kPlayerConditionSatisfyColor = UIColor(red: 226.0/255, green: 144.0/255 , blue: 36.0/255, alpha: 1)

let kPlayerSelectionMaxLimitColor = UIColor(red: 21.0/255, green: 143.0/255 , blue: 255.0/255, alpha: 1)

let kPlayerTypeSelectedColor = UIColor(red: 0/255, green: 167/255, blue: 80/255, alpha: 1)

let kPlayerTypeUnselectedColor = UIColor(red: 127.0/255, green: 132.0/255 , blue: 134.0/255, alpha: 1)

class SelectPlayerViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

  
    @IBOutlet weak var headerView: LetspickCreateTeamHeaderViewNew!
    @IBOutlet weak var sliderLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var pointsButton: UIButton!
    @IBOutlet weak var creditButton: UIButton!
    
    @IBOutlet weak var wicketKeeperIcon: UIImageView!
    @IBOutlet weak var batsmanIcon: UIImageView!
    @IBOutlet weak var bowlerIcon: UIImageView!
    @IBOutlet weak var allRounderIcon: UIImageView!
    
    @IBOutlet weak var secondTeamButton: UIButton!
    @IBOutlet weak var firstTeamButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    
    @IBOutlet weak var previewButton: CustomBorderButton!
    
    @IBOutlet weak var lblAllRounderTitle: UILabel!
    @IBOutlet weak var lblBowlerTitle: UILabel!
    @IBOutlet weak var lblBatsmanTitle: UILabel!
    @IBOutlet weak var lblWicketTitle: UILabel!
    
    @IBOutlet weak var nextButton: SolidButton!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var lblWicketCount: UILabel!
    @IBOutlet weak var lblAllRounderCount: UILabel!
    @IBOutlet weak var lblBatsmanCount: UILabel!
    @IBOutlet weak var lblBowlCount: UILabel!
  
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var announcementView: AnnouncementView!
    @IBOutlet weak var inOutButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottonView: UIView!
    @IBOutlet weak var lblPickMessage: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var fantasyTypeLbl: UILabel!
    lazy var selectedPlayerType = PlayerType.WicketKeeper.rawValue
    var totalPlayerArray: Array<PlayerDetails>?
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var userTeamDetails: UserTeamDetails?

    var ticketDetails: TicketDetails?

    var timer: Timer?
    lazy var isEditPlayerTeam = false
    lazy var isPlaying22 = ""
    lazy var totalWicketKeeperArray = Array<PlayerDetails>()
    lazy var totalBatsmanArray = Array<PlayerDetails>()
    lazy var totalBowlerArray = Array<PlayerDetails>()
    lazy var totalAllRounderArray = Array<PlayerDetails>()
    
    lazy var playerSortedType = false
    lazy var teamSortedType = false
    lazy var pointsSortedType = true
    lazy var creditsSortedType = true
    lazy var selectedLeagueType = 0
    lazy var isComeFromEditPlayerTeamScreen = false
    lazy var isPlayerPlaying = false
    lazy var isMatchClosingTimeRefereshing = false

    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        inOutButton.isHidden = true
        headerView.isViewForSelectCaptain = false
        setupDefaultProperties()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails!.fantasyType, gameType: GameType.Cricket.rawValue)
        
        lblAllRounderTitle.text = "ALR".localized()
        lblBowlerTitle.text = "BOW".localized()
        lblBatsmanTitle.text = "BAT".localized()
        lblWicketTitle.text = "WK".localized()
        previewButton.setTitle("TeamPreview".localized(), for: .normal)
        nextButton.setTitle("Next".localized(), for: .normal)
        previewButton.setTitle("TeamPreview".localized(), for: .selected)
        nextButton.setTitle("Next".localized(), for: .selected)
        
        pointsButton.setTitle("Points".localized(), for: .normal)
        pointsButton.setTitle("Points".localized(), for: .selected)

        creditButton.setTitle("Credits".localized(), for: .normal)
        creditButton.setTitle("Credits".localized(), for: .selected)

        var index = 0
        
        if self.selectedPlayerType == PlayerType.Batsman.rawValue{
            self.batsmanButtonTapped(nil)
            index = 1
        }
        else if self.selectedPlayerType == PlayerType.Bowler.rawValue{
            self.bowlerButtonTapped(nil)
            index = 3
        }
        else if self.selectedPlayerType == PlayerType.AllRounder.rawValue{
            self.allRounderButtonTapped(nil)
            index = 2
        }
        else if self.selectedPlayerType == PlayerType.WicketKeeper.rawValue{
            self.wicketButtonTapped(nil)
            index = 0
        }
        
        let indexPath = IndexPath(item: index, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)

    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerListContainerCollectionViewCell", for: indexPath) as? PlayerListContainerCollectionViewCell
        var playerList: Array<PlayerDetails>?
        
        if indexPath.row == 0 {
            playerList = totalWicketKeeperArray
        }
        else if indexPath.row == 1 {
            playerList = totalBatsmanArray
        }
        else if indexPath.row == 2 {
            playerList = totalAllRounderArray
        }
        else if indexPath.row == 3 {
            playerList = totalBowlerArray
        }

        if leagueDetails?.fantasyType == "1" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Classic.rawValue, gameType: GameType.Cricket.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        else if leagueDetails?.fantasyType == "2" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Batting.rawValue, gameType: GameType.Cricket.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        else if leagueDetails?.fantasyType == "3" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Bowling.rawValue, gameType: GameType.Cricket.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        else if leagueDetails?.fantasyType == "4" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Reverse.rawValue, gameType: GameType.Cricket.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        else if leagueDetails?.fantasyType == "5" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Wizard.rawValue, gameType: GameType.Cricket.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }

        return cell!;
    }
    
    // MARK:- API Related Method
    
    func callGetPlayerListAPI(urlString: String, isNeedToShowLoader: Bool)  {
        
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                return;
            }
        }
        AppHelper.sharedInstance.displaySpinner()
        
        weak var waekSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in

            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
                    DispatchQueue.main.async {
                        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
                        let response = savedResponse?.dictionary!["response"]
                        if let flagDict = response?.dictionary!["team_flags"]{
                            UserDetails.sharedInstance.teamFlag = flagDict;
                        }

                        if let selectedmatch = response!.dictionary!["selected_match"]?.dictionary{
                            waekSelf?.isPlaying22 = selectedmatch["show_playing22"]?.string ?? "0"
                            if waekSelf?.isPlaying22 != "1"{
                                waekSelf?.inOutButton.isHidden = true
                            }
                            else{
                                waekSelf?.inOutButton.isHidden = false
                            }
                            
                            if waekSelf?.matchDetails?.firstTeamShortName?.count == 0{
                                let teamFirstName = selectedmatch["team_a_short_name"]?.string ?? ""
                                let teamSecondShortName = selectedmatch["team_b_short_name"]?.string ?? ""
                                waekSelf?.matchDetails?.firstTeamShortName = teamFirstName
                                waekSelf?.matchDetails?.secondTeamShortName = teamSecondShortName
                                
                                if let firstTeamImageName = selectedmatch["team_a_flag"]?.string{
                                    waekSelf?.matchDetails?.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                                }
                                   
                                if let secondTeamImageName = selectedmatch["team_b_flag"]?.string{
                                    waekSelf?.matchDetails?.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                                }

                                if let startTime = selectedmatch["start_date_unix"]?.string{
                                    var closingTime = selectedmatch["closing_ts"]?.intValue ?? 0
                                    if closingTime == 0{
                                        closingTime = UserDetails.sharedInstance.closingTimeForMatch
                                    }
                                    let calcultedTime = Int(startTime)! - closingTime * 60
                                    waekSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                                }
                    
                                waekSelf?.firstTeamButton.setTitle(teamFirstName, for: .normal)
                                waekSelf?.secondTeamButton.setTitle(teamSecondShortName, for: .normal)
                            }
                        }
                        if waekSelf?.matchDetails?.isMatchTourney ?? false{
                            waekSelf?.allButton.isHidden = true
                            waekSelf?.firstTeamButton.isHidden = true
                            waekSelf?.secondTeamButton.isHidden = true
                        }
                        let playerList = response?.dictionary!["match_players"]?.array
                        waekSelf?.totalPlayerArray = PlayerDetails.getPlayerDetailsArray(responseArray: playerList!, matchDetails: waekSelf?.matchDetails)
                        if waekSelf?.matchDetails?.playersGender == "F"{
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: waekSelf!.totalPlayerArray!)
                        }

                        waekSelf?.modifySelectedPlayerDetails()
                        waekSelf?.updateTimerVlaue()
                        waekSelf?.totalWicketKeeperArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 0)
                        waekSelf?.totalBatsmanArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 1)
                        waekSelf?.totalBowlerArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 2)
                        waekSelf?.totalAllRounderArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 3)
                        waekSelf?.sortPlayerBasedOnLineupsOut(isFromAll: false)
                        waekSelf?.upperView.isHidden = false
                        waekSelf?.bottonView.isHidden = false
                        waekSelf?.collectionView.isHidden = false

                        if waekSelf?.selectedLeagueType == FantasyType.Batting.rawValue{
                            waekSelf?.batsmanButtonTapped(nil)
                        }
                        else if waekSelf?.selectedLeagueType == FantasyType.Bowling.rawValue{
                            waekSelf?.bowlerButtonTapped(nil)
                        }

                        waekSelf?.inOutButtonTapped(nil)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    //MARK:- Custom Methods
    
    func setupDefaultProperties() {
        
        lblWicketCount.text = "0"
        lblAllRounderCount.text = "0"
        lblBatsmanCount.text = "0"
        lblBowlCount.text = "0"
        upperView.isHidden = true
        bottonView.isHidden = true
        collectionView.isHidden = true
        isPlayerPlaying = true
        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        fantasyTypeLbl.layer.cornerRadius = fantasyTypeLbl.frame.height/2

        firstTeamButton.setTitle(matchDetails!.firstTeamShortName, for: .normal)
        secondTeamButton.setTitle(matchDetails!.secondTeamShortName, for: .normal)
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails!.fantasyType, gameType: GameType.Cricket.rawValue)

        if leagueDetails?.fantasyType == "1" {
            selectedLeagueType = FantasyType.Classic.rawValue;
            selectedPlayerType = PlayerType.WicketKeeper.rawValue
        }
        else if leagueDetails?.fantasyType == "2" {
            selectedLeagueType = FantasyType.Batting.rawValue;
            selectedPlayerType = PlayerType.Batsman.rawValue
        }
        else if leagueDetails?.fantasyType == "3" {
            selectedLeagueType = FantasyType.Bowling.rawValue;
            selectedPlayerType = PlayerType.Bowler.rawValue
        }
        else if leagueDetails?.fantasyType == "4" {
            selectedLeagueType = FantasyType.Reverse.rawValue;
            selectedPlayerType = PlayerType.WicketKeeper.rawValue
        }
        else if leagueDetails?.fantasyType == "5" {
            selectedLeagueType = FantasyType.Wizard.rawValue;
            selectedPlayerType = PlayerType.WicketKeeper.rawValue
        }
        
        if matchDetails?.isMatchTourney ?? false{
            allButton.isHidden = true
            firstTeamButton.isHidden = true
            secondTeamButton.isHidden = true
        }

        
        AppHelper.designBottomTabDesing(bottonView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerSelectionUpdateNotification), name: NSNotification.Name(rawValue: "PlayerSelectionUpdateNotification"), object: nil)
        collectionView.register(UINib(nibName: "PlayerListContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayerListContainerCollectionViewCell")
        let urlString = kMatch + "?option=match_details&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.callGetPlayerListAPI(urlString: urlString, isNeedToShowLoader: true)
        }
        
        playerSelectionUpdateNotification()

        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        for selectedDetails in UserDetails.sharedInstance.selectedPlayerList{
            for details in totalPlayerArray!{
                
                if details.playerKey == selectedDetails.playerKey{
                    details.isSelected = selectedDetails.isSelected
                    selectedDetails.playerPlayingRole = details.playerPlayingRole
                    selectedDetails.bowlingPoints = details.bowlingPoints
                    selectedDetails.isPlayerPlaying = details.isPlayerPlaying
                    selectedDetails.battingPoints = details.battingPoints
                    selectedDetails.classicPoints = details.classicPoints
                    selectedDetails.reversePoints = details.reversePoints
                    selectedDetails.wizardPoints = details.wizardPoints
                    selectedDetails.teamShortName = details.teamShortName
                    
                    selectedDetails.viceCaptainRoleSelected = details.viceCaptainRoleSelected
                    selectedDetails.captainRoleSelected = details.captainRoleSelected
                    selectedDetails.wizardRoleSelected = details.wizardRoleSelected
                    selectedDetails.classicSelected = details.classicSelected
                    selectedDetails.battingSelected = details.battingSelected
                    selectedDetails.bowlingSelected = details.bowlingSelected
                    selectedDetails.wizardSelected = details.wizardSelected
                    selectedDetails.reverseSelected = details.reverseSelected

                    break;
                }
            }
        }
        
        if selectedRow == 0{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
            })
        }
        if playersArray != nil{
            return playersArray!
        }
        return []
    }
    
    @objc func playerSelectionUpdateNotification()  {
        
        headerView.updatePlayerSelectionData(matchDetails: matchDetails!, fantasyType: leagueDetails!.fantasyType)
        if leagueDetails?.fantasyType == "1" {
            if UserDetails.sharedInstance.selectedPlayerList.count == 11 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }
        else if leagueDetails?.fantasyType == "2" {
            if UserDetails.sharedInstance.selectedPlayerList.count == 5 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }
        else if leagueDetails?.fantasyType == "4" {
            if UserDetails.sharedInstance.selectedPlayerList.count == 11 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }
        else if leagueDetails?.fantasyType == "5" {
            if UserDetails.sharedInstance.selectedPlayerList.count == 11 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }
        else{
            if UserDetails.sharedInstance.selectedPlayerList.count == 5 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }

        if leagueDetails?.fantasyType == "1" {
            getSelectedPlayerCounts()
        }
        else{
            if isComeFromEditPlayerTeamScreen{
                getSelectedPlayerCountsForBatsManAndBowlerFantasy()
            }
            else{
                getSelectedPlayerCounts()
            }
        }
    }
    
    func getSelectedPlayerCountsForBatsManAndBowlerFantasy(){
        
        let totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        if totalPlayerArray.count == 1 {
            AppxorEventHandler.logAppEvent(withName: "AddPlayer", info: ["SportType": "Kabaddi"])
        }

        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.WicketKeeper.rawValue
        })
        
        let batsmanPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.Batsman.rawValue
        })
        
        let bowlerPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.Bowler.rawValue
        })
        
        let allRounderPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.AllRounder.rawValue
        })
        
        lblWicketCount.text = "(" + String(wicketPlayersArray.count) + ")"
        lblAllRounderCount.text = "(" + String(allRounderPlayerArray.count) + ")"
        lblBatsmanCount.text = "(" + String(batsmanPlayersArray.count) + ")"
        lblBowlCount.text = "(" + String(bowlerPlayersArray.count) + ")"
    }
    
    func getSelectedPlayerCounts(){
        
        let totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        if totalPlayerArray.count == 1 {
            AppxorEventHandler.logAppEvent(withName: "AddPlayer", info: ["SportType": "Kabaddi"])
        }

        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
        })
        
        let batsmanPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
        })
        
        let bowlerPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
        })
        
        let allRounderPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
        })
        
        
        lblWicketCount.text = "(" + String(wicketPlayersArray.count) + ")"
        lblAllRounderCount.text = "(" + String(allRounderPlayerArray.count) + ")"
        lblBatsmanCount.text = "(" + String(batsmanPlayersArray.count) + ")"
        lblBowlCount.text = "(" + String(bowlerPlayersArray.count) + ")"
    }
    
    func modifySelectedPlayerDetails()  {
        
        if isEditPlayerTeam{
            for selectedPlayermDetails in UserDetails.sharedInstance.selectedPlayerList{
                for playermDetails in totalPlayerArray!{
                    if selectedPlayermDetails.playerKey == playermDetails.playerKey{
                        
                        selectedPlayermDetails.bowlingPoints = playermDetails.bowlingPoints
                        selectedPlayermDetails.battingPoints = playermDetails.battingPoints
                        selectedPlayermDetails.classicPoints = playermDetails.classicPoints
                        selectedPlayermDetails.reversePoints = playermDetails.reversePoints
                        selectedPlayermDetails.wizardPoints = playermDetails.wizardPoints
                        selectedPlayermDetails.teamShortName = playermDetails.teamShortName
                        break
                    }
                }
            }
        }
        playerSelectionUpdateNotification()
    }
    
    //MARK:- -IBAction Methods
    @IBAction func wicketButtonTapped(_ sender: Any?) {
        showWicketSelectedTab()
        
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func batsmanButtonTapped(_ sender: Any?) {
        
        showBattingSelectedTab()
        
        let indexPath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func allRounderButtonTapped(_ sender: Any?) {
        showAllRounderSelectedTab()
        let indexPath = IndexPath(item: 2, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func bowlerButtonTapped(_ sender: Any?) {
        showBowlerSelectedTab()
        let indexPath = IndexPath(item: 3, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    
    func showWicketSelectedTab() {
        if (selectedLeagueType == FantasyType.Classic.rawValue) || (selectedLeagueType == FantasyType.Reverse.rawValue) || (selectedLeagueType == FantasyType.Wizard.rawValue){
            lblPickMessage.text = "Pick 1-4 Wicket-Keeper".localized()
            if selectedLeagueType == FantasyType.Classic.rawValue{
                fantasyTypeLbl.text = " Classic Fantasy "
            }else if selectedLeagueType == FantasyType.Reverse.rawValue{
                fantasyTypeLbl.text = " Reverse Fantasy "
            }else{
                fantasyTypeLbl.text = " Wizard Fantasy "
            }
            
        }
        else if selectedLeagueType == FantasyType.Batting.rawValue{
            lblPickMessage.text = "Pick 1-5 Wicket-Keepers".localized()
            fantasyTypeLbl.text = " Batting Fantasy "
        }
        else if selectedLeagueType == FantasyType.Bowling.rawValue {
            lblPickMessage.text = "Not Required".localized()
            fantasyTypeLbl.text = " Bowling Fantasy "
        }
        
        selectedPlayerType = PlayerType.WicketKeeper.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Cricket", "Type": "WK"])

        wicketKeeperIcon.image = UIImage(named: "WicketKeeperSelectedIcon")
        batsmanIcon.image = UIImage(named: "BatsmanIcon")
        allRounderIcon.image = UIImage(named: "AllRounderPlayerIcon")
        bowlerIcon.image = UIImage(named: "BowlerIcon")
        
        
        lblWicketTitle.textColor = kPlayerTypeSelectedColor
        lblBatsmanTitle.textColor = kPlayerTypeUnselectedColor
        lblBowlerTitle.textColor = kPlayerTypeUnselectedColor
        lblAllRounderTitle.textColor = kPlayerTypeUnselectedColor
        
        lblWicketCount.textColor = kPlayerTypeSelectedColor
        lblBatsmanCount.textColor = kPlayerTypeUnselectedColor
        lblBowlCount.textColor = kPlayerTypeUnselectedColor
        lblAllRounderCount.textColor = kPlayerTypeUnselectedColor
        
        sliderLeadingConstraint.constant = 0.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }

    func showBattingSelectedTab() {
        if (selectedLeagueType == FantasyType.Classic.rawValue) || (selectedLeagueType == FantasyType.Reverse.rawValue) || (selectedLeagueType == FantasyType.Wizard.rawValue){
            lblPickMessage.text = "Pick 3-6 Batsmen".localized()
        }
        else if (selectedLeagueType == FantasyType.Batting.rawValue) || (selectedLeagueType == FantasyType.Bowling.rawValue){
            lblPickMessage.text = "Pick 1-5 Batsmen".localized()
        }
        
        selectedPlayerType = PlayerType.Batsman.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Cricket", "Type": "BAT"])

        lblWicketTitle.textColor = kPlayerTypeUnselectedColor
        lblBatsmanTitle.textColor = kPlayerTypeSelectedColor
        lblBowlerTitle.textColor = kPlayerTypeUnselectedColor
        lblAllRounderTitle.textColor = kPlayerTypeUnselectedColor
        
        lblWicketCount.textColor = kPlayerTypeUnselectedColor
        lblBatsmanCount.textColor = kPlayerTypeSelectedColor
        lblBowlCount.textColor = kPlayerTypeUnselectedColor
        lblAllRounderCount.textColor = kPlayerTypeUnselectedColor
        
        wicketKeeperIcon.image = UIImage(named: "WicketKeeperIcon")
        batsmanIcon.image = UIImage(named: "BatsmanSelectedIcon")
        allRounderIcon.image = UIImage(named: "AllRounderPlayerIcon")
        bowlerIcon.image = UIImage(named: "BowlerIcon")
        
        sliderLeadingConstraint.constant = batsmanIcon.frame.origin.x - 10.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    func showAllRounderSelectedTab() {
        if (selectedLeagueType == FantasyType.Classic.rawValue) || (selectedLeagueType == FantasyType.Reverse.rawValue) || (selectedLeagueType == FantasyType.Wizard.rawValue){
            lblPickMessage.text = "Pick 1-4 All Rounders".localized()
        }
        else if (selectedLeagueType == FantasyType.Batting.rawValue) || (selectedLeagueType == FantasyType.Bowling.rawValue){
            lblPickMessage.text = "Pick 1-5 All Rounders".localized()
        }
        
        selectedPlayerType = PlayerType.AllRounder.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Cricket", "Type": "ALR"])

        allRounderIcon.layer.borderColor = kPlayerSelectionMaxLimitColor.cgColor
        
        lblWicketTitle.textColor = kPlayerTypeUnselectedColor
        lblBatsmanTitle.textColor = kPlayerTypeUnselectedColor
        lblBowlerTitle.textColor = kPlayerTypeUnselectedColor
        lblAllRounderTitle.textColor = kPlayerTypeSelectedColor
        
        lblWicketCount.textColor = kPlayerTypeUnselectedColor
        lblBatsmanCount.textColor = kPlayerTypeUnselectedColor
        lblBowlCount.textColor = kPlayerTypeUnselectedColor
        lblAllRounderCount.textColor = kPlayerTypeSelectedColor
        
        wicketKeeperIcon.image = UIImage(named: "WicketKeeperIcon")
        batsmanIcon.image = UIImage(named: "BatsmanIcon")
        allRounderIcon.image = UIImage(named: "AllRounderPlayerSelectedIcon")
        bowlerIcon.image = UIImage(named: "BowlerIcon")
        
        sliderLeadingConstraint.constant = allRounderIcon.frame.origin.x - 10.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    func showBowlerSelectedTab() {
        if (selectedLeagueType == FantasyType.Classic.rawValue) || (selectedLeagueType == FantasyType.Reverse.rawValue) || (selectedLeagueType == FantasyType.Wizard.rawValue){
            lblPickMessage.text = "Pick 3-6 Bowlers".localized()
        }
        else if (selectedLeagueType == FantasyType.Batting.rawValue) || (selectedLeagueType == FantasyType.Bowling.rawValue){
            lblPickMessage.text = "Pick 1-5 Bowlers".localized()
        }
        
        selectedPlayerType = PlayerType.Bowler.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Cricket", "Type": "BOW"])

        lblWicketTitle.textColor = kPlayerTypeUnselectedColor
        lblBatsmanTitle.textColor = kPlayerTypeUnselectedColor
        lblBowlerTitle.textColor = kPlayerTypeSelectedColor
        lblAllRounderTitle.textColor = kPlayerTypeUnselectedColor
        
        lblWicketCount.textColor = kPlayerTypeUnselectedColor
        lblBatsmanCount.textColor = kPlayerTypeUnselectedColor
        lblBowlCount.textColor = kPlayerTypeSelectedColor
        lblAllRounderCount.textColor = kPlayerTypeUnselectedColor
        
        wicketKeeperIcon.image = UIImage(named: "WicketKeeperIcon")
        batsmanIcon.image = UIImage(named: "BatsmanIcon")
        allRounderIcon.image = UIImage(named: "AllRounderPlayerIcon")
        bowlerIcon.image = UIImage(named: "BowlerSelectedIcon")
        
        sliderLeadingConstraint.constant = UIScreen.main.bounds.size.width - 90.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
   
    @IBAction func teamPreviewButtonTapped(_ sender: Any) {
        
        if UserDetails.sharedInstance.selectedPlayerList.count == 0 {
            AppHelper.showAlertView(message: "Please select atleast one player", isErrorMessage: true)
            return
        }
        
        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Cricket"])

        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "TeamPreviewViewController") as! TeamPreviewViewController
        teamPreviewVC.totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        teamPreviewVC.matchDetails = matchDetails
        teamPreviewVC.selectedFantasy = selectedLeagueType
        teamPreviewVC.isShowPlayingRole = true;

        if leagueDetails!.fantasyType == "1"{
            teamPreviewVC.selectedFantasy = FantasyType.Classic.rawValue
        }
        else if leagueDetails!.fantasyType == "2"{
            teamPreviewVC.selectedFantasy = FantasyType.Batting.rawValue
        }
        else if leagueDetails!.fantasyType == "3"{
            teamPreviewVC.selectedFantasy = FantasyType.Bowling.rawValue
        }
        else if leagueDetails!.fantasyType == "4"{
            teamPreviewVC.selectedFantasy = FantasyType.Reverse.rawValue
        }
        else if leagueDetails!.fantasyType == "5"{
            teamPreviewVC.selectedFantasy = FantasyType.Wizard.rawValue
        }

        teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
        teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
        teamPreviewVC.firstTeamkey = matchDetails!.firstTeamKey
        teamPreviewVC.secondTeamkey = matchDetails!.secondTeamKey

        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    @IBAction func creditButtonTapped(_ sender: Any){
        
        let wicketKeeperPlayerListArray = totalWicketKeeperArray
        let batsmanPlayerListArray = totalBatsmanArray
        let allRounderPlayerListArray = totalAllRounderArray
        let bowlerPlayerListArray = totalBowlerArray
        
        let sortedKeeperArray =  wicketKeeperPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
                
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedBatsmanArray =  batsmanPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedAllrounderArray =  allRounderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedBowlerArray =  bowlerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        if creditsSortedType {
            creditButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }
        else{
            creditButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }

        creditsSortedType = !creditsSortedType
        totalWicketKeeperArray = sortedKeeperArray
        totalBatsmanArray = sortedBatsmanArray
        totalBowlerArray = sortedBowlerArray
        totalAllRounderArray = sortedAllrounderArray
        
        collectionView.reloadData()
        
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        
    }

    @IBAction func pointsButtonTapped(_ sender: Any){
        
        let wicketKeeperPlayerListArray = totalWicketKeeperArray
        let batsmanPlayerListArray = totalBatsmanArray
        let allRounderPlayerListArray = totalAllRounderArray
        let bowlerPlayerListArray = totalBowlerArray
        
        let sortedKeeperArray =  wicketKeeperPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! > Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.bowlingPoints)!
                }
                else if selectedLeagueType == FantasyType.Reverse.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.reversePoints)!
                }
                else if selectedLeagueType == FantasyType.Wizard.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.wizardPoints)!
                }

            }
            else{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! < Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! < Float(playerDetails.bowlingPoints)!
                }
                else if selectedLeagueType == FantasyType.Reverse.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.reversePoints)!
                }
                else if selectedLeagueType == FantasyType.Wizard.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.wizardPoints)!
                }

            }
            
            return false
        })
        
        let sortedBatsmanArray =  batsmanPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! > Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.bowlingPoints)!
                }
                else if selectedLeagueType == FantasyType.Reverse.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.reversePoints)!
                }
                else if selectedLeagueType == FantasyType.Wizard.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.wizardPoints)!
                }

            }
            else{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! < Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! < Float(playerDetails.bowlingPoints)!
                }
                else if selectedLeagueType == FantasyType.Reverse.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.reversePoints)!
                }
                else if selectedLeagueType == FantasyType.Wizard.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.wizardPoints)!
                }
            }
            
            return false
        })
        
        
        let sortedAllrounderArray =  allRounderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! > Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.bowlingPoints)!
                }
                else if selectedLeagueType == FantasyType.Reverse.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.reversePoints)!
                }
                else if selectedLeagueType == FantasyType.Wizard.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.wizardPoints)!
                }

            }
            else{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! < Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! < Float(playerDetails.bowlingPoints)!
                }
                else if selectedLeagueType == FantasyType.Reverse.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.reversePoints)!
                }
                else if selectedLeagueType == FantasyType.Wizard.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.wizardPoints)!
                }
            }
            
            return false
        })
        
        let sortedBowlerArray =  bowlerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! > Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.bowlingPoints)!
                }
                else if selectedLeagueType == FantasyType.Reverse.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.reversePoints)!
                }
                else if selectedLeagueType == FantasyType.Wizard.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.wizardPoints)!
                }
            }
            else{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! < Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! < Float(playerDetails.bowlingPoints)!
                }
                else if selectedLeagueType == FantasyType.Reverse.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.reversePoints)!
                }
                else if selectedLeagueType == FantasyType.Wizard.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.wizardPoints)!
                }

            }
            
            return false
        })
        
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        if pointsSortedType {
            pointsButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }
        else{
            pointsButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }

        pointsSortedType = !pointsSortedType
        totalWicketKeeperArray = sortedKeeperArray
        totalBatsmanArray = sortedBatsmanArray
        totalBowlerArray = sortedBowlerArray
        totalAllRounderArray = sortedAllrounderArray
        collectionView.reloadData()
        
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
    }
    
    @IBAction func allTeamButtonTapped(_ sender: Any?) {

        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        totalWicketKeeperArray = getSelectedTypePlayerList(selectedRow: 0)
        totalBatsmanArray = getSelectedTypePlayerList(selectedRow: 1)
        totalBowlerArray = getSelectedTypePlayerList(selectedRow: 2)
        totalAllRounderArray = getSelectedTypePlayerList(selectedRow: 3)
        sortPlayerBasedOnLineupsOut(isFromAll: true)
//        collectionView.reloadData()
    }
    
    @IBAction func firstTeamButtonTapped(_ sender: Any) {
    
        firstTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        
        let wicketKeeperPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let batsmanPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let bowlerPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let allRounderPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)

        let filteredKeeperArray = wicketKeeperPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredBatsmanArray = batsmanPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }

        let filteredBowlerArray = bowlerPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }

        let filteredAllRounderArray = allRounderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }

        totalWicketKeeperArray = filteredKeeperArray
        totalBatsmanArray = filteredBatsmanArray
        totalBowlerArray = filteredBowlerArray
        totalAllRounderArray = filteredAllRounderArray
        collectionView.reloadData()

//        sortPlayerBasedOnLineupsOut()
    }
    
    
    @IBAction func secondTeamButtonTapped(_ sender: Any) {

        secondTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        
        let wicketKeeperPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let batsmanPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let bowlerPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let allRounderPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)

        let filteredKeeperArray = wicketKeeperPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredBatsmanArray = batsmanPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredBowlerArray = bowlerPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredAllRounderArray = allRounderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        totalWicketKeeperArray = filteredKeeperArray
        totalBatsmanArray = filteredBatsmanArray
        totalBowlerArray = filteredBowlerArray
        totalAllRounderArray = filteredAllRounderArray
        collectionView.reloadData()
//        sortPlayerBasedOnLineupsOut()
    }
 
    @IBAction func inOutButtonTapped(_ sender: Any?) {
        sortPlayerBasedOnLineupsOut(isFromAll: false)
    }
    
    func sortPlayerBasedOnLineupsOut(isFromAll: Bool) {
        
//        if isPlaying22 != "1"{
//            allTeamButtonTapped(nil)
//            return
//        }
                
        if !isFromAll {
            secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            inOutButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        }
        else{
            allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
            firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        }
        
        let wicketKeeperPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let batsmanPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let bowlerPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let allRounderPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)

        
//        let sortedKeeperArray = wicketKeeperPlayerListArray.filter { (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedBatsmanArray = batsmanPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedAllrounderArray =  allRounderPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedBowlerArray =  bowlerPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
        
        
//        let sortedKeeperArray = wicketKeeperPlayerListArray.filter { (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedBatsmanArray = batsmanPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedAllrounderArray =  allRounderPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedBowlerArray =  bowlerPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
        
        let sortedKeeperArray = wicketKeeperPlayerListArray.sorted(by: { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        })

        let sortedBatsmanArray = batsmanPlayerListArray.sorted(by: { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        })

        let sortedAllrounderArray = allRounderPlayerListArray.sorted(by: { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        })

        let sortedBowlerArray = bowlerPlayerListArray.sorted(by: { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        })


        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        totalWicketKeeperArray = sortedKeeperArray
        totalBatsmanArray = sortedBatsmanArray
        totalBowlerArray = sortedBowlerArray
        totalAllRounderArray = sortedAllrounderArray
        
        collectionView.reloadData()
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "NextButtonClicked", info: ["SportType": "Cricket"])

        if leagueDetails?.fantasyType == "1" {
            if UserDetails.sharedInstance.selectedPlayerList.count < 11{
                AppHelper.showAlertView(message: "Please select 11 players", isErrorMessage: true)
                return;
            }
        }
        else if leagueDetails?.fantasyType == "2"{
            if UserDetails.sharedInstance.selectedPlayerList.count < 5{
                AppHelper.showAlertView(message: "Please select 5 players", isErrorMessage: true)
                return;
            }
        }
        else if leagueDetails?.fantasyType == "3"{
            if UserDetails.sharedInstance.selectedPlayerList.count < 5{
                AppHelper.showAlertView(message: "Please select 5 players", isErrorMessage: true)
                return;
            }
        }
        else if leagueDetails?.fantasyType == "4" {
            if UserDetails.sharedInstance.selectedPlayerList.count < 11{
                AppHelper.showAlertView(message: "Please select 11 players", isErrorMessage: true)
                return;
            }
        }
        else if leagueDetails?.fantasyType == "5" {
            if UserDetails.sharedInstance.selectedPlayerList.count < 11{
                AppHelper.showAlertView(message: "Please select 11 players", isErrorMessage: true)
                return;
            }
        }

        self.performSegue(withIdentifier: "SelectCaptainViewController", sender: nil)
    }
    
    // MARK: - Timer Handler
    @objc func updateTimerVlaue()  {
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
        
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
//                            AppHelper.showMatchCLosedAlertAndPopToRootViewController()
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectCaptainViewController" {
            let selectCaptionVC = segue.destination as! SelectCaptainViewController
            selectCaptionVC.matchDetails = matchDetails
            selectCaptionVC.leagueDetails = leagueDetails
            selectCaptionVC.isEditPlayerTeam = isEditPlayerTeam
            selectCaptionVC.userTeamDetails = userTeamDetails
            selectCaptionVC.tickeDtails = ticketDetails

        }
    }
    
    //MARK:- Scroll View Delegates
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if collectionView == scrollView{
            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
            if Int(currentPage) == 0{
                showWicketSelectedTab()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 1{
                showBattingSelectedTab()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 2{
                showAllRounderSelectedTab()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 3{
                showBowlerSelectedTab()
                collectionView.reloadData()
            }
        }
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
