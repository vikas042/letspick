//
//  JoinLeagueConfirmationViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/11/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire


class JoinLeagueConfirmationViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    @IBOutlet weak var lblBonusDeductTitle: UILabel!
    
    @IBOutlet weak var lblBalanceDeductAmount: UILabel!
    @IBOutlet weak var ticketImgView: UIImageView!
    @IBOutlet weak var lblBonusDeductAmount: UILabel!
    @IBOutlet weak var lblBalanceDeductTitle: UILabel!
    @IBOutlet weak var ticketButton: UIButton!
    @IBOutlet weak var lblSelectAll: UILabel!
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var lblYourTeam: UILabel!
    @IBOutlet weak var lblEntryCostTitle: UILabel!
    @IBOutlet weak var joinButton: SolidButton!
    @IBOutlet weak var lblTicketName: UILabel!
    @IBOutlet weak var lblTicketTitle: UILabel!
    @IBOutlet weak var ticketView: UIView!
    @IBOutlet weak var lblTicketAmount: UILabel!
    @IBOutlet weak var lblEntryFee: UILabel!
    @IBOutlet weak var joinLeagueViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var joinLeagueView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var ticketViewHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var termsAndConditionsButton: UIButton!
    @IBOutlet weak var lblTermsAndConditions: UILabel!
    
    @IBOutlet weak var lblRealDuctionCollen: UILabel!
    @IBOutlet weak var lblBonusDeductionCollen: UILabel!
    @IBOutlet weak var lblEntryCostCollen: UILabel!
    @IBOutlet weak var lblTicketColen: UILabel!
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    
    var ticketDetails: TicketDetails?

    var timer: Timer?
    var teamPickerDataArray: Array<UserTeamDetails>?
    lazy var userTeamsArray = Array<UserTeamDetails>()
    lazy var selectedTeamsArray = Array<UserTeamDetails>()
    lazy var isMultiJoiningAllowed = true

    lazy var ticket_applied = 0
    lazy var selectedGameType = GameType.Cricket.rawValue
    lazy var leagueCategoryName = ""
    lazy var isMatchClosingTimeRefereshing = false
    lazy var isNeedToShowMatchClosePopup = false
    lazy var isNeedToShowMatch = true

    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        lblBalanceDeductTitle.text = "Balance Deducted".localized()
        lblBonusDeductTitle.text = "Bonus Deducted".localized()
        collectionView.register(UINib(nibName: "SwapTeamCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SwapTeamCollectionViewCell")
        collectionView.register(UINib(nibName: "SwapeKabaddiCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SwapeKabaddiCollectionViewCell")
        collectionView.register(UINib(nibName: "FootballSwapCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FootballSwapCollectionViewCell")
        collectionView.register(UINib(nibName: "BaseballSwapCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BaseballSwapCollectionViewCell")
        collectionView.register(UINib(nibName: "BasketballSwapCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BasketballSwapCollectionViewCell")
        
        lblTicketTitle.isHidden = true
        lblTicketColen.isHidden = true
        lblTicketAmount.isHidden = true
        ticketButton.isSelected = true
        ticketView.isHidden = true
        setupDefaultProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true;
        
        headerView.tag = 4000;
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.tag = 0;
    }
    
    // MARK:- IBAction Methods
    @IBAction func joinLeagueButtonTapped(_ sender: Any) {
        callJoinLeagueAPI()
    }

    @IBAction func termsAndConditionButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        let privacyPolicyVC = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as! PrivacyPolicyWebViewController
        privacyPolicyVC.selectedType = SelectedWebViewType.termsAndConditions.rawValue
        navigationController?.pushViewController(privacyPolicyVC, animated: true)
    }
    
    //MARK:- Custom Methods
    func setupDefaultProperties() {
        if (UserDetails.sharedInstance.isMultiJoiningAllow == 1) && (leagueDetails?.teamType == "1"){
            isMultiJoiningAllowed = true
        }
        else{
            isMultiJoiningAllowed = false
        }
        
        if !isMultiJoiningAllowed {
            selectAllButton.isHidden = true
            lblSelectAll.isHidden = true
        }
        lblSelectAll.text = "SelectAll".localized()
        lblYourTeam.text = "YourTeam".localized()
        lblEntryCostTitle.text = "EntryCost".localized()
        headerView.headerTitle = "JoiningConfirmation".localized()
        joinButton.setTitle("JoinLeague".localized(), for: .normal)
        lblTicketTitle.text = "Ticket".localized()
        ticketImgView.image = UIImage(named: "TicketHomeIcon")

        if ticketDetails?.ticketType == "3" {
            lblTicketTitle.text = "Pass".localized()
            ticketImgView.image = UIImage(named: "PassIcon")
        }
        
        joinLeagueView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        joinLeagueView.layer.shadowOffset = CGSize(width: 0, height: 0)
        joinLeagueView.layer.shadowOpacity = 0.33
        joinLeagueView.layer.shadowRadius = 5.0
        joinLeagueView.layer.cornerRadius = 10
        joinLeagueView.layer.masksToBounds = false

        if UserDetails.sharedInstance.ticketApplied != 0 {
            lblTicketName.text = ticketDetails?.ticketTitle
            ticket_applied = 1
            ticketView.isHidden = false
            lblTicketTitle.isHidden = false
            lblTicketColen.isHidden = false
            lblTicketAmount.isHidden = false
            ticketViewHeightConstaint.constant = 20
            joinLeagueViewHeightConstraint.constant = 260
            joinLeagueView.layoutIfNeeded()
        }
        
        teamPickerDataArray = userTeamsArray.filter({ (teamDetails) -> Bool in
            teamDetails.fantasyType == leagueDetails?.fantasyType
        })

        
        if userTeamsArray.count > 0{
            selectedTeamsArray.append(userTeamsArray[0])
            addAndRemoveTeam(details: nil, isNeedToAddRemove: false)
        }
        
        if teamPickerDataArray?.count ?? 0 > 1 {
            lblYourTeam.text = "YourTeams".localized()
        }
        else{
            lblYourTeam.text = "YourTeam".localized()
        }
        

        if UserDetails.sharedInstance.ticketApplied == 2{
            ticketButton.isUserInteractionEnabled = false
        }
        
        weak var weakSelf = self
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            if (weakSelf?.leagueDetails?.bonusApplicable == "2") && (weakSelf?.leagueDetails?.timeBasedBonusArray.count != 0){
                weakSelf?.addAndRemoveTeam(details: nil, isNeedToAddRemove: false)
            }

            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: weakSelf!.matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
    }
        
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        var urlString = kMatch
        if selectedGameType == GameType.Cricket.rawValue {
            urlString = kMatch
        }
        else if selectedGameType == GameType.Kabaddi.rawValue {
            urlString = kKabaddiMatchURL
        }
        else if selectedGameType == GameType.Cricket.rawValue {
            urlString = kFootballMatchURL
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            urlString = kBasketballMatchURL
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            urlString = kBaseballMatchURL
        }
        
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                            if weakSelf?.isNeedToShowMatchClosePopup ?? false {
                                AppHelper.showMatchCLosedAlertAndPopToRootViewController()
                            }
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    
    
    // MARK:- API Related Methods
    func callJoinLeagueAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        var teamNumbers = ""
        
        for teamDetails in selectedTeamsArray {
            if teamNumbers.count == 0 {
                teamNumbers = teamDetails.teamNumber ?? ""
            }
            else{
                if teamDetails.teamNumber?.count != 0 {
                    teamNumbers = teamNumbers + "," + teamDetails.teamNumber!
                }
            }
        }
        
        if teamNumbers.count == 0 {
            AppHelper.showAlertView(message: "Please select at least one team", isErrorMessage: true)
            return;
        }
        var option = "save_league_v2"
        var urlString = kMatch
        var sportType = "Cricket"

        if selectedGameType == GameType.Kabaddi.rawValue {
            option = "join_league_v1"
            urlString = kKabaddiMatchURL
            sportType = "Kabaddi"
        }
        else if selectedGameType == GameType.Football.rawValue {
            option = "join_league_v1"
            urlString = kFootballMatchURL
            sportType = "Football"
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            option = "join_league_v1"
            urlString = kBasketballMatchURL
            sportType = "Basketball"
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            option = "join_league_v1"
            urlString = kBaseballMatchURL
            sportType = "Baseball"
        }

        
        AppHelper.sharedInstance.displaySpinner()
        
        AppxorEventHandler.logAppEvent(withName: "JoinLeagueButtonClicked", info: ["SportType": sportType])
        
        weak var weakSelf = self
        
        var isFromTicketScreen = "0"
        
        if ticketDetails?.isFromTicketScreen ?? false{
            isFromTicketScreen = "1"
        }
        
        let parameters: Parameters = ["option": option,"fantasy_type": leagueDetails!.fantasyType, "match_key": leagueDetails!.matchKey,"league_id": leagueDetails!.leagueId,"teams": teamNumbers, "user_id": UserDetails.sharedInstance.userID, "ticket_applied":ticket_applied, "ticket_screen": isFromTicketScreen]
        
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200"{
                    
                    weakSelf?.updateLeagueJoinedEventToCleverTap()
                    AppHelper.showToast(message: "League joined successfully".localized())
                    if weakSelf?.selectedGameType == GameType.Cricket.rawValue {
                        let foundRecord = weakSelf?.redirectForCricketLaague() ?? false
                        if foundRecord{
                            return;
                        }
                    }
                    else if weakSelf?.selectedGameType == GameType.Kabaddi.rawValue {
                        let foundRecord = weakSelf?.redirectForKabaddiLaague() ?? false
                        if foundRecord{
                            return;
                        }
                    }
                    else if weakSelf?.selectedGameType == GameType.Football.rawValue {
                        let foundRecord = weakSelf?.redirectForFootballLeague() ?? false
                        if foundRecord{
                            return;
                        }
                    }
                    else if weakSelf?.selectedGameType == GameType.Basketball.rawValue {
                        let foundRecord = weakSelf?.redirectForBasketballLeague() ?? false
                        if foundRecord{
                            return;
                        }
                    }
                    else if weakSelf?.selectedGameType == GameType.Baseball.rawValue {
                        let foundRecord = weakSelf?.redirectForBaseballLeague() ?? false
                        if foundRecord{
                            return;
                        }
                    }

                    
                    weakSelf?.navigationController?.popToRootViewController(animated: true)
                }
                else if statusCode == "401"{
                    let titleMessage = "Oops! Low Balance".localized()
                    
                    if let response = result!["response"]?.dictionary{
                        
                        let is_multi_joining = response["is_multi_joining"]?.intValue
                        UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0

                        let creditRequired = response["credit_required"]?.stringValue ?? "0"
                        let responseAmt = Float(creditRequired)!

                        let joiningAmount = Float(self.leagueDetails!.joiningAmount)!
                        let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                        let message = String(format: "LowBalanceMessage".localized(), String(roundFigureAmt))

                        let alert = UIAlertController(title: titleMessage, message: "Not enough points to join this league", preferredStyle: UIAlertControllerStyle.alert)
                        /*
                        alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
                            
                            let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
                            addCashVC?.leagueDetails = self.leagueDetails!
                            addCashVC?.amount = roundFigureAmt
                            addCashVC?.matchDetails = self.matchDetails
                            addCashVC?.userTeamArray = self.userTeamsArray
                            addCashVC?.categoryName = self.leagueCategoryName
                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                navVC.pushViewController(addCashVC!, animated: true)
                            }
                        }))
                         */
                        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message , isErrorMessage: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true )
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func updateLeagueJoinedEventToCleverTap()  {
        var params = [String: Any]()
        if selectedGameType == GameType.Cricket.rawValue{
            params[game_type] = "Cricket"
        }
        else if selectedGameType == GameType.Cricket.rawValue{
            params[game_type] = "Kabaddi"
        }
        else if selectedGameType == GameType.Cricket.rawValue{
            params[game_type] = "Football"
        }
        else if selectedGameType == GameType.Basketball.rawValue{
            params[game_type] = "Basketball"
        }
        else if selectedGameType == GameType.Baseball.rawValue{
            params[game_type] = "Baseball"
        }

        params[match_name] = matchDetails?.matchShortName ?? ""
        params[id] = matchDetails?.matchKey ?? ""
        var fantasyName = "Classic"
        
        if leagueDetails?.fantasyType == "1"{
            fantasyName = "Classic"
        }
        else if leagueDetails?.fantasyType == "2"{
            fantasyName = "Batting"
        }
        else if leagueDetails?.fantasyType == "3"{
            fantasyName = "Bowling"
        }
        
        params[fantasy_type] = fantasyName
        if ticket_applied == 0 {
            params[buy_in] = leagueDetails?.joiningAmount ?? ""
            params[joined_mode] = "Cash"
        }
        else{
            params[buy_in] = "0"
            params[joined_mode] = "Ticket"
        }
        
        params[max_team] = leagueDetails?.maxPlayers
        params[league_type] = leagueDetails?.leagueType
        params[prize_pool] = leagueDetails?.totalWinners
        
        if leagueDetails?.teamType == "1"{
            params[entry_type] = "Multiple"
        }
        else{
            params[entry_type] = "Single"
        }

        params[PLATFORM] = PLATFORM_iPHONE

        
        if leagueDetails?.bonusApplicable == "2"{
            params[bonus_allowed] = "Yes"
        }
        else{
            params[bonus_allowed] = "No"
        }
        
        params[series_name] = matchDetails?.seasonShortName ?? ""
        params[league_category] = leagueCategoryName
        params[PLATFORM] = PLATFORM_iPHONE

        CleverTapEventDetails.sendEventToCleverTap(eventName: joined_league, params: params)
    }
    
    func redirectForCricketLaague() -> Bool  {
        let navArray = navigationController?.viewControllers
        for index in stride(from: navArray!.count - 1, to: 0, by: -1) {
            let selectPlayerVC = navArray![index]

            if leagueDetails!.isPrivateLeague{
                if selectPlayerVC is LeagueViewController{
                    let leagueVC = selectPlayerVC as! LeagueViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    if self.ticket_applied == 1{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: true, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    else{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: false, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    return true
                }
                else if selectPlayerVC is JoinedLeagueViewController{
                    let leagueVC = selectPlayerVC as! JoinedLeagueViewController
                    leagueVC.isNeedToRefreshLeaues = true
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    return true
                }
            }
            else{
                if selectPlayerVC is MoreLeagueViewController{
                    let leagueVC = selectPlayerVC as! MoreLeagueViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!)
                    return true
                }
                else if selectPlayerVC is LeagueViewController{
                    let leagueVC = selectPlayerVC as! LeagueViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    if self.ticket_applied == 1{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: true, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    else{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: false, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    return true
                }
                else if selectPlayerVC is MyTicketViewController{
                    
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.isFromJoiningConfirmation = true
                    leagueVC.matchDetails = matchDetails
                    navigationController?.pushViewController(leagueVC, animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserInfoNotification"), object: nil)
                    return true
                }
                else if selectPlayerVC is JoinedLeagueViewController{
                    let leagueVC = selectPlayerVC as! JoinedLeagueViewController
                    leagueVC.isNeedToRefreshLeaues = true
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    return true
                }

            }
        }
        return false
    }
    
    func redirectForKabaddiLaague() -> Bool  {
        let navArray = navigationController?.viewControllers
        for index in stride(from: navArray!.count - 1, to: 0, by: -1) {
            let selectPlayerVC = navArray![index]

            if leagueDetails!.isPrivateLeague{
                if selectPlayerVC is JoinedKabaddiLeagueViewController{
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    return true
                }
            }
            else{
                if selectPlayerVC is MoreKabaddiViewController{
                    let leagueVC = selectPlayerVC as! MoreKabaddiViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!)
                    return true
                }
                else if selectPlayerVC is KabaddiLeagueViewController{
                    let leagueVC = selectPlayerVC as! KabaddiLeagueViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    if self.ticket_applied == 1{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: true, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    else{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: false, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    return true
                }
                else if selectPlayerVC is MyTicketViewController{
                    
                    let kabaddiLeagueVC = storyboard?.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    kabaddiLeagueVC.matchDetails = matchDetails
                    navigationController?.pushViewController(kabaddiLeagueVC, animated: true)
                    kabaddiLeagueVC.isFromJoiningConfirmation = true

                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserInfoNotification"), object: nil)
//                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    
                    return true
                }
            }
        }
        return false
    }
    
    func redirectForFootballLeague() -> Bool  {
        let navArray = navigationController?.viewControllers
        for index in stride(from: navArray!.count - 1, to: 0, by: -1) {

            let selectPlayerVC = navArray![index]            
            if leagueDetails!.isPrivateLeague{
                if selectPlayerVC is FootballJoinedLeagueViewController{
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    return true
                }
            }
            else{
                if selectPlayerVC is FootballMoreLeagueViewController{
                    let leagueVC = selectPlayerVC as! FootballMoreLeagueViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!)
                    return true
                }
                else if selectPlayerVC is FootballLeagueViewController{
                    let leagueVC = selectPlayerVC as! FootballLeagueViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    if self.ticket_applied == 1{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: true, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    else{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: false, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    return true
                }
                else if selectPlayerVC is MyTicketViewController{
                    
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchDetails = matchDetails
                    navigationController?.pushViewController(leagueVC, animated: true)
                    leagueVC.isFromJoiningConfirmation = true

                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserInfoNotification"), object: nil)
//                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    return true
                }
            }
        }

        return false
    }
    
    func redirectForBasketballLeague() -> Bool  {
        let navArray = navigationController?.viewControllers
        
        for index in stride(from: navArray!.count - 1, to: 0, by: -1) {

            let selectPlayerVC = navArray![index]
            if leagueDetails!.isPrivateLeague{
                if selectPlayerVC is JoinedBasketballLeaguesViewController{
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    return true
                }
            }
            else{
                if selectPlayerVC is BasketballMoreLeagueViewController{
                    let leagueVC = selectPlayerVC as! BasketballMoreLeagueViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!)
                    return true
                }
                else if selectPlayerVC is BasketballLeagueViewController{
                    let leagueVC = selectPlayerVC as! BasketballLeagueViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    if self.ticket_applied == 1{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: true, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    else{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: false, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    return true
                }
                else if selectPlayerVC is MyTicketViewController{
                    let leagueVC = storyboard?.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as! BasketballLeagueViewController
                    leagueVC.matchDetails = matchDetails
                    navigationController?.pushViewController(leagueVC, animated: true)
                    leagueVC.isFromJoiningConfirmation = true

                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserInfoNotification"), object: nil)
//                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    return true
                }
            }
        }

        return false
    }
    
    func redirectForBaseballLeague() -> Bool  {
        let navArray = navigationController?.viewControllers
        for index in stride(from: navArray!.count - 1, to: 0, by: -1) {
            let selectPlayerVC = navArray![index]
            if leagueDetails!.isPrivateLeague{
                if selectPlayerVC is JoinedBaseballLeaguesViewController{
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    return true
                }
            }
            else{
                if selectPlayerVC is BaseballMoreLeaguesViewController{
                    let leagueVC = selectPlayerVC as! BaseballMoreLeaguesViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!)
                    return true
                }
                else if selectPlayerVC is BaseballLeagueViewController{
                    let leagueVC = selectPlayerVC as! BaseballLeagueViewController
                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    if self.ticket_applied == 1{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: true, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    else{
                        leagueVC.updateLeagueJoinedStatus(joinedLeague: leagueDetails!, isticketUsed: false, fantasyType: self.leagueDetails!.fantasyType)
                    }
                    return true
                }
                else if selectPlayerVC is MyTicketViewController{
                    
                    let leagueVC = storyboard!.instantiateViewController(withIdentifier: "BaseballLeagueViewController") as! BaseballLeagueViewController
                    leagueVC.matchDetails = matchDetails
                    navigationController?.pushViewController(leagueVC, animated: true)
                    leagueVC.isFromJoiningConfirmation = true
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserInfoNotification"), object: nil)
//                    navigationController?.popToViewController(selectPlayerVC, animated: true)
                    return true
                }
            }
        }
        return false
    }
    
    @IBAction func selectAllButtonTapped(_ sender: Any) {
        if !isMultiJoiningAllowed {
            return;
        }
        else if selectAllButton.isSelected {
            selectAllButton.isSelected = false
            selectedTeamsArray.removeAll()
        }
        else{
            selectAllButton.isSelected = true
            if teamPickerDataArray != nil  {
                selectedTeamsArray.removeAll()
                selectedTeamsArray.append(contentsOf: teamPickerDataArray!)
            }
        }
        
        var sportType = "Cricket"

        if selectedGameType == GameType.Kabaddi.rawValue {
            sportType = "Kabaddi"
        }
        else if selectedGameType == GameType.Football.rawValue {
            sportType = "Football"
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            sportType = "Basketball"
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            sportType = "Baseball"
        }

        AppxorEventHandler.logAppEvent(withName: "SelectAllTeamButtonClicked", info: ["SportType": sportType])
        collectionView.reloadData()
        addAndRemoveTeam(details: nil, isNeedToAddRemove: false)
    }
    

    //MARK:- Collection View Data Source and Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if teamPickerDataArray == nil {
            return 0;
        }
        
        return teamPickerDataArray!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if leagueDetails?.fantasyType == "5" {
            return CGSize(width: UIScreen.main.bounds.width - 10, height: 155)
        }
        return CGSize(width: UIScreen.main.bounds.width - 10, height: 125)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
        if selectedGameType == GameType.Kabaddi.rawValue{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SwapeKabaddiCollectionViewCell", for: indexPath) as? SwapeKabaddiCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.teamPreviewButton.tag = indexPath.row
            
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewTeamButton(button:)), for: .touchUpInside)
            cell?.showViewForUnselected()

            let details = teamPickerDataArray![indexPath.row]
            cell?.configData(details, selectedTeam: nil)

            for teamDetails in selectedTeamsArray{
                if teamDetails.teamNumber == details.teamNumber {
                    cell?.showViewForSelected()
                }
            }
            return cell!;
        }
        else if selectedGameType == GameType.Football.rawValue{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FootballSwapCollectionViewCell", for: indexPath) as? FootballSwapCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.teamPreviewButton.tag = indexPath.row
            
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewTeamButton(button:)), for: .touchUpInside)
            
            let details = teamPickerDataArray![indexPath.row]
            cell?.configData(details, selectedTeam: nil)
            for teamDetails in selectedTeamsArray{
                if teamDetails.teamNumber == details.teamNumber {
                    cell?.showViewForSelected()
                }
            }
            return cell!;
        }
        else if selectedGameType == GameType.Basketball.rawValue{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BasketballSwapCollectionViewCell", for: indexPath) as? BasketballSwapCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.teamPreviewButton.tag = indexPath.row
            
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewTeamButton(button:)), for: .touchUpInside)
            
            let details = teamPickerDataArray![indexPath.row]
            cell?.configData(details, selectedTeam: nil)
            for teamDetails in selectedTeamsArray{
                if teamDetails.teamNumber == details.teamNumber {
                    cell?.showViewForSelected()
                }
            }
            return cell!;
        }
        else if selectedGameType == GameType.Baseball.rawValue{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BaseballSwapCollectionViewCell", for: indexPath) as? BaseballSwapCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.teamPreviewButton.tag = indexPath.row
            
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewTeamButton(button:)), for: .touchUpInside)
            
            let details = teamPickerDataArray![indexPath.row]
            cell?.configData(details, selectedTeam: nil)
            for teamDetails in selectedTeamsArray{
                if teamDetails.teamNumber == details.teamNumber {
                    cell?.showViewForSelected()
                }
            }
            return cell!;
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SwapTeamCollectionViewCell", for: indexPath) as? SwapTeamCollectionViewCell
            cell?.selectButton.tag = indexPath.row
            cell?.teamPreviewButton.tag = indexPath.row
           // cell?.lblViceCaptain.textColor = UIColor.black
            //cell?.lblVicecaptainTitle.textColor = UIColor.gray
            
            cell?.selectButton.addTarget(self, action: #selector(selectTeamButton(button:)), for: .touchUpInside)
            cell?.teamPreviewButton.addTarget(self, action: #selector(previewTeamButton(button:)), for: .touchUpInside)
            
            let details = teamPickerDataArray![indexPath.row]
            cell?.configData(details, selectedTeam: nil)
            for teamDetails in selectedTeamsArray{
                if teamDetails.teamNumber == details.teamNumber {
                    cell?.showViewForSelected()
                }
            }
            return cell!;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
   
        let details = teamPickerDataArray![indexPath.row]
        addAndRemoveTeam(details: details, isNeedToAddRemove: true)
    }
    
    @objc func selectTeamButton(button: UIButton) {
        if teamPickerDataArray?.count == 0{
            return;
        }
        let details = teamPickerDataArray![button.tag]
        addAndRemoveTeam(details: details, isNeedToAddRemove: true)
        
        var sportType = "Cricket"

        if selectedGameType == GameType.Kabaddi.rawValue {
            sportType = "Kabaddi"
        }
        else if selectedGameType == GameType.Football.rawValue {
            sportType = "Football"
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            sportType = "Basketball"
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            sportType = "Baseball"
        }

        
        AppxorEventHandler.logAppEvent(withName: "SelectTeamButtonClicked", info: ["SportType": sportType, "TeamNumber": details.teamNumber ?? "", "CaptainID": "", "ViceCaptainID": ""])
    }
    
    func addAndRemoveTeam(details: UserTeamDetails?, isNeedToAddRemove: Bool) {
        
        if isNeedToAddRemove {
            if isMultiJoiningAllowed {
                var isRecordFound = false
                for index in 0 ..< selectedTeamsArray.count{
                    let teamDetails = selectedTeamsArray[index]
                    if teamDetails.teamNumber == details!.teamNumber {
                        isRecordFound = true
                        selectedTeamsArray.remove(at: index)
                        break;
                    }
                }
                
                if !isRecordFound {
                    selectedTeamsArray.append(details!)
                }
            }
            else{
                selectedTeamsArray.removeAll()
                selectedTeamsArray.append(details!)
            }
        }
                
        lblEntryFee.text = String(selectedTeamsArray.count) + "x pts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: leagueDetails!.joiningAmount)
        let leagueJoiningAmount = (Double(leagueDetails!.joiningAmount) ?? 0) * Double(selectedTeamsArray.count)
        var usedUnusedAmount: Double = leagueJoiningAmount
        var totalJoiningAmount = "pts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: String(leagueJoiningAmount))

        if (ticket_applied != 0) {
            let joiningAmount = Float(leagueDetails?.joiningAmount ?? "0") ?? 0
            let totalAmount = joiningAmount * Float(selectedTeamsArray.count)
            usedUnusedAmount = Double(totalAmount)
            var totalPassApplied = (ticketDetails?.totalPass ?? 0)
            if ticketDetails?.ticketType != "3" {
                totalPassApplied = 1
            }
            if totalPassApplied > Float(selectedTeamsArray.count) {
                totalPassApplied = Float(selectedTeamsArray.count)
            }
            
            let ticketPrice = (Float(leagueDetails!.joiningAmount) ?? 0) * totalPassApplied
            totalJoiningAmount = "pts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: String(totalAmount - ticketPrice))

            lblTicketAmount.text =  "-\(String(Int(totalPassApplied)))xpts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: String(ticketPrice))
            
            if ticketDetails?.ticketType != "3" {
                lblTicketAmount.text = "-1xpts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: String(ticketPrice))
            }
            
            let bonusAppliedPercentage = self.getAppliedBonusPercentage()
            if bonusAppliedPercentage == "0" {
                lblBonusDeductAmount.text = "pts 0"
            }
            else{
                var bonusAmount = (Float(bonusAppliedPercentage) ?? 0) * totalAmount / 100
                if bonusAmount >= Float(UserDetails.sharedInstance.bonusCash) ?? 0{
                    bonusAmount = Float(UserDetails.sharedInstance.bonusCash) ?? 0
                }
                usedUnusedAmount = Double(totalAmount - bonusAmount)
//                lblBonusDeductAmount.text = "pts \(bonusAmount)"
                lblBonusDeductAmount.text = String(format: "pts%.2f", bonusAmount)
            }
        }
        else{
            lblTicketAmount.text = "pts0"
            let bonusAppliedPercentage = self.getAppliedBonusPercentage()
            if bonusAppliedPercentage == "0" {
                lblBonusDeductAmount.text = "pts0"
            }
            else{
                var bonusAmount = (Double(bonusAppliedPercentage) ?? 0) * leagueJoiningAmount / 100
                if bonusAmount >= Double(UserDetails.sharedInstance.bonusCash) ?? 0{
                    bonusAmount = Double(UserDetails.sharedInstance.bonusCash) ?? 0
                }
                usedUnusedAmount = leagueJoiningAmount - bonusAmount
//                lblBonusDeductAmount.text = "pts\(bonusAmount)"
                lblBonusDeductAmount.text = String(format: "pts%.2f", bonusAmount)

            }
        }
        
        let avilableBalance = (Float(UserDetails.sharedInstance.unusedCash) ?? 0) + (Float(UserDetails.sharedInstance.withdrawableCredits) ?? 0)
        
        if usedUnusedAmount >= Double(avilableBalance) {
            usedUnusedAmount = Double(avilableBalance)
        }

//        lblBalanceDeductAmount.text = "pts\(usedUnusedAmount)"
        lblBalanceDeductAmount.text = String(format: "pts%.2f", usedUnusedAmount)

        joinButton.setTitle("Join league with".localized() + totalJoiningAmount, for: .normal)

        if teamPickerDataArray != nil {
            if selectedTeamsArray.count >= teamPickerDataArray!.count  {
                selectAllButton.isSelected = true
            }
            else{
                selectAllButton.isSelected = false
            }
        }
        collectionView.reloadData();
    }
    func getAppliedBonusPercentage() ->String {
                
        let bounsPercentage = leagueDetails!.bounsPercentage

        if (leagueDetails!.timeBasedBonusArray.count != 0) && (leagueDetails!.bonusApplicable == "2"){
                    
            let serverUnixTimestamp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
            let dateUnixTimestamp = Double(matchDetails!.startDateTimestemp ?? "0") ?? 0
            
            var matchClosingSeconds = 0.0
            
            if dateUnixTimestamp > serverUnixTimestamp {
                matchClosingSeconds = dateUnixTimestamp - serverUnixTimestamp
            }
                        
            for bonusDetails in leagueDetails!.timeBasedBonusArray{
                let defaultBouns = bonusDetails["tbm"].string ?? ""
                if(defaultBouns == "default")   {
                    continue
                }
                let remaingTimeStr = bonusDetails["tbm"].stringValue
                let remainingTime = (Double(remaingTimeStr) ?? 0)*60
                if matchClosingSeconds > remainingTime {
                    let bounsPercentage = bonusDetails["bp"].stringValue
                    if (bounsPercentage != "0") && (bounsPercentage != "default")  {
                        if bounsPercentage.count == 0 {
                            return "0"
                        }
                        return bounsPercentage
                    }
                    break;
                }
            }
        }
        if bounsPercentage.count == 0 {
            return "0"
        }
        return bounsPercentage
    }
    
    @objc func previewTeamButton(button: UIButton) {
        if teamPickerDataArray?.count == 0{
            return;
        }
        let details = teamPickerDataArray![button.tag]
        guard let playersArray = details.playersArray else {
            return;
        }
        
        
        if selectedGameType == GameType.Cricket.rawValue {
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Cricket"])
            let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "TeamPreviewViewController") as! TeamPreviewViewController
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.isHideEditButton = true;
            teamPreviewVC.totalPlayerArray = playersArray;
            teamPreviewVC.matchDetails = matchDetails
            if leagueDetails!.fantasyType == "1"{
                teamPreviewVC.selectedFantasy = FantasyType.Classic.rawValue
            }
            else if leagueDetails!.fantasyType == "2"{
                teamPreviewVC.selectedFantasy = FantasyType.Batting.rawValue
            }
            else if leagueDetails!.fantasyType == "3"{
                teamPreviewVC.selectedFantasy = FantasyType.Bowling.rawValue
            }
            else if leagueDetails!.fantasyType == "4"{
                teamPreviewVC.selectedFantasy = FantasyType.Reverse.rawValue
            }
            else if leagueDetails!.fantasyType == "5"{
                teamPreviewVC.selectedFantasy = FantasyType.Wizard.rawValue
            }
            
            teamPreviewVC.firstTeamkey = matchDetails!.firstTeamKey
            teamPreviewVC.secondTeamkey = matchDetails!.secondTeamKey

            teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""

            navigationController?.pushViewController(teamPreviewVC, animated: true)
        }
        else if selectedGameType == GameType.Kabaddi.rawValue {
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Kabaddi"])

            let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "KabaddiTeamPreviewViewController") as! KabaddiTeamPreviewViewController
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.totalPlayerArray = playersArray;            teamPreviewVC.isShowPlayingRole = true;
            teamPreviewVC.isHideEditButton = true;
            teamPreviewVC.firstTeamName = matchDetails!.firstTeamKey
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamKey
            navigationController?.pushViewController(teamPreviewVC, animated: true)
        }
        else if selectedGameType == GameType.Football.rawValue {
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Football"])

            let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "FootballTeamPreviewViewController") as! FootballTeamPreviewViewController
            teamPreviewVC.totalPlayerArray = playersArray;            teamPreviewVC.isShowPlayingRole = true;
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.isHideEditButton = true;
            teamPreviewVC.firstTeamkey = self.matchDetails!.firstTeamKey
            teamPreviewVC.secondTeamkey = self.matchDetails!.secondTeamKey

            teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            navigationController?.pushViewController(teamPreviewVC, animated: true)
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Basketball"])

            let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "BasketTeamPreviewViewController") as! BasketTeamPreviewViewController
            teamPreviewVC.totalPlayerArray = playersArray;            teamPreviewVC.isShowPlayingRole = true;
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.isHideEditButton = true;
            teamPreviewVC.firstTeamkey = self.matchDetails!.firstTeamKey
            teamPreviewVC.secondTeamkey = self.matchDetails!.secondTeamKey

            teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            navigationController?.pushViewController(teamPreviewVC, animated: true)
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Baseball"])

            let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "BaseballTeamPreviewViewController") as! BaseballTeamPreviewViewController
            teamPreviewVC.totalPlayerArray = playersArray;            teamPreviewVC.isShowPlayingRole = true;
            teamPreviewVC.teamNumber = details.teamNumber ?? ""
            teamPreviewVC.isHideEditButton = true;
            teamPreviewVC.firstTeamkey = self.matchDetails!.firstTeamKey
            teamPreviewVC.secondTeamkey = self.matchDetails!.secondTeamKey
            teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            navigationController?.pushViewController(teamPreviewVC, animated: true)
        }
        
    }

    @IBAction func ticketButtonTapped(_ sender: Any) {
        if (UserDetails.sharedInstance.ticketApplied == 2){
            return
        }
        
        if ticketButton.isSelected{
            ticket_applied = 0
            ticketButton.isSelected = false
            lblTicketTitle.isHidden = true
            lblTicketColen.isHidden = true
            lblTicketAmount.isHidden = true
        }
        else{
            ticket_applied = 1
            ticketButton.isSelected = true
            lblTicketTitle.isHidden = false
            lblTicketColen.isHidden = false
            lblTicketAmount.isHidden = false
        }
        
        addAndRemoveTeam(details: nil, isNeedToAddRemove: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}
