//
//  PartnershipProgramViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import FBSDKShareKit


class PartnershipProgramViewController: UIViewController, SharingDelegate, PartnershipWalletViewControllerDelegate {
    @IBOutlet weak var applicationClosedButtton: UIButton!
    
    @IBOutlet weak var lblWillBackDes: UILabel!
    @IBOutlet weak var lblWillBackTitle: UILabel!
    @IBOutlet weak var shoutdownView: UIView!
    @IBOutlet weak var lblCopy: UILabel!
    @IBOutlet weak var bonanzaCollection: UICollectionView!
    @IBOutlet weak var bonanazaViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblPPTitle: UILabel!
    @IBOutlet weak var ppTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblLifeTimeEarning: UILabel!
    @IBOutlet weak var lblEarningAmount: UILabel!
    @IBOutlet weak var lblPatnerNow: UILabel!
    @IBOutlet weak var lblReferalPolicies: UILabel!
    @IBOutlet weak var inviteCodeView: UIView!
    @IBOutlet weak var lifeTimeEarningView: UIView!
    
    lazy var currentAffilateAmount = "0";
    lazy var bannersArray = Array<BannerDetails>()
    lazy var bonanazaBannersArray = Array<BannerDetails>()

    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.isHidden = true
        collectionView.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell")
        bonanzaCollection.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell")

        NotificationCenter.default.addObserver(self, selector: #selector(removeBonanazeBanner(notification:)), name: NSNotification.Name("removeBonanazeBanner"), object: nil)
        
        lblEarningAmount.text = "pts 0.0"
        lblCode.text = UserDetails.sharedInstance.referralCode
        headerView.headerTitle = "Partnership Program".localized()
        lblLifeTimeEarning.text = "Life time earning".localized()
        applicationClosedButtton.setTitle("Application Closed".localized(), for: .normal)
        lblWillBackTitle.text = "We will be Right Back!".localized()
        lblWillBackDes.text = "WillRighBackPP".localized()

        lblPatnerNow.text = "You are now a partner. Let's invite friends".localized()
        lblCopy.text = "Copy".localized()
        headerView.tag = 5001
        self.lblReferalPolicies.text = "Referral Policies".localized()
        showShadow(containerView: lifeTimeEarningView);
        showShadow(containerView: inviteCodeView);
        callGetBannerDetailsAPI()
    }
    
    @objc func removeBonanazeBanner(notification: Notification) {
        
        let bannerID = notification.object as? String ?? ""
        
        for index in 0 ..< bonanazaBannersArray.count {
            let details = bonanazaBannersArray[index]
            if details.promotionID == bannerID{
                bonanazaBannersArray.remove(at: index)
                break;
            }
        }
        
        bonanzaCollection.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let isWatingForApproval = UserDefaults.standard.bool(forKey: "pp_isWatingForApproval")
        if isWatingForApproval {
            UserDefaults.standard.set(false, forKey: "pp_isWatingForApproval")
            UserDefaults.standard.synchronize()

            let congratsView = CongratulationsView(frame: APPDELEGATE.window!.frame)
            APPDELEGATE.window?.addSubview(congratsView)
        }
    }
    
    @IBAction func copyButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "ReferalCodeClicked", info: nil)
        UIPasteboard.general.string = UserDetails.sharedInstance.referralCode
        AppHelper.showToast(message: "Referral code copied")
    }
    
    //MARK:- API Related Methods
    func callGetBannerDetailsAPI() {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "get_banner", "user_id": UserDetails.sharedInstance.userID, "referals": "1"]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kPartnershipURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary {
                        let partnership_program_status = response["partnership_program_status"]?.stringValue

                        if let partnershipDict = response["what_is_pp"]?.dictionary {
                            let title = partnershipDict["title"]?.string ?? ""
                            weakSelf?.lblPPTitle.text = title
                            let partnershipDescription = partnershipDict["body"]?.string ?? ""
                            weakSelf?.lblMessage.showHTML(htmlString: partnershipDescription, isCenterAligment: true)
                        }
                                                
                        if let bannersArray = response["bonanaza_banners"]?.array {
                            weakSelf?.bonanazaBannersArray = BannerDetails.getPromoBannerDetails(dataArray: bannersArray)
                        }
                        
                        if weakSelf?.bonanazaBannersArray.count == 0 {
                            weakSelf?.bonanzaCollection.isHidden = true
                            weakSelf?.bonanazaViewHeightConstraint.constant = 30
                            weakSelf?.view.layoutIfNeeded()
                        }
                        else{
                            weakSelf?.bonanzaCollection.isHidden = false
                        }
                        
                        weakSelf?.bonanzaCollection.reloadData()
                        
                        if let bannersArray = response["banners"]?.array {
                            weakSelf?.bannersArray = BannerDetails.getPromoBannerDetails(dataArray: bannersArray)
                            weakSelf?.collectionView.reloadData()
                        }
                        if weakSelf?.bannersArray.count == 0 {
                            weakSelf?.ppTitleTopConstraint.constant = 10
                            weakSelf?.collectionView.isHidden = true
                            weakSelf?.containerView.layoutIfNeeded()
                        }
                        
                        DispatchQueue.main.async {
                                                        
                            weakSelf!.lblMessage.sizeToFit()
                            weakSelf!.scrollViewHeightConstraint.constant = weakSelf!.lblMessage.frame.height + 480 + weakSelf!.bonanazaViewHeightConstraint.constant
                            weakSelf!.containerView.layoutIfNeeded()
                        }

                        if let affiliateStatsDict = response["affiliateStats"]?.dictionary {
                            
                            let totalAffilateAmount = affiliateStatsDict["total_affiliate_amount"]?.stringValue ?? "0"
                            weakSelf?.currentAffilateAmount = affiliateStatsDict["current_affiliate_amount"]?.stringValue ?? "0"
                            weakSelf?.lblEarningAmount.text = "pts" + totalAffilateAmount
                        }
                        
                        var isWaiting = false
                        if let leaguesDict = response["leagues"]?.dictionary {
                            isWaiting = leaguesDict["is_waiting"]?.boolValue ?? false
                        }
                        
                        weakSelf?.shoutdownView.isHidden = true
                        if ((partnership_program_status == "0") && !isWaiting) && !((UserDetails.sharedInstance.userAffilate == "1") || (UserDetails.sharedInstance.userAffilate == "2")) {
                            weakSelf?.shoutdownView.isHidden = false
                            weakSelf?.inviteCodeView.isHidden = true
                            weakSelf?.lifeTimeEarningView.isHidden = true
                        }
                    }
                    weakSelf?.containerView.isHidden = false
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    //MARK:- -IBAction Methods
    @IBAction func facebookButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "FacebookClicked", info: nil)
        shareTextOnFaceBook();
    }
    
    @IBAction func telegramButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "TelegramClicked", info: nil)

        let linkShare = "https://Letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!
        let urlString = "tg://msg?text=Hey, I'm playing Fantasy Sports on the Letspick App and winning real cash every day. Join now and start making your teams:\n\(linkShare)"
        guard let tgUrl = URL(string:urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) else { return }
        
        if UIApplication.shared.canOpenURL(tgUrl){
            UIApplication.shared.open(tgUrl, options: [:], completionHandler: nil)
        }
        else{
            AppHelper.showAlertView(message: "Telegram not available", isErrorMessage: true)
        }
    }

    @IBAction func whatsAppButtonTapped(_ sender: Any) {
    
        AppxorEventHandler.logAppEvent(withName: "WhatsappClicked", info: nil)

        let linkShare = "https://Letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!
        let urlString = "whatsapp://send?text=Hey, I'm playing Fantasy Sports on the Letspick App and winning real cash every day. Join now and start making your teams:\n\(linkShare)"
        guard let tgUrl = URL(string:urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) else { return }
        
        if UIApplication.shared.canOpenURL(tgUrl){
            UIApplication.shared.open(tgUrl, options: [:], completionHandler: nil)
        }
        else{
            AppHelper.showAlertView(message: "Whats app not available", isErrorMessage: true)
        }
    }

    @IBAction func sharingButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "ShareButtonClicked", info: nil)

        let sharingText = "Hey, I'm playing Fantasy Sports on the Letspick App and winning real cash every day. Join now and start making your teams:\n" + "https://Letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!

        let textToShare = [sharingText]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func shareTextOnFaceBook() {
        let shareContent = ShareLinkContent()
        shareContent.contentURL = URL(string: "https://Letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!)!
        shareContent.quote = "Hey, I'm playing Fantasy Sports on the Letspick App and winning real cash every day. Join now and start making your teams."
        ShareDialog(fromViewController: self, content: shareContent, delegate: self).show()
    }

    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        if sharer.shareContent.pageID != nil {
            print("Share: Success")
        }
    }
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print("Share: Fail")
    }
    func sharerDidCancel(_ sharer: Sharing) {
        print("Share: Cancel")
    }

    @IBAction func referalPolicyButtonTapped(_ sender: Any?) {
        AppxorEventHandler.logAppEvent(withName: "ReferralPoliciesClicked", info: nil)

        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
        privacyPolicyVC?.selectedType = SelectedWebViewType.ReferralPolicies.rawValue
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.pushViewController(privacyPolicyVC!, animated: true)
        }
    }
    
    func showShadow(containerView: UIView) {
                
        containerView.layer.cornerRadius = 6.0
        containerView.layer.shadowColor = UIColor(red: 78.0/255, green: 84.0/255, blue: 90.0/255, alpha: 0.5).cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        containerView.layer.shadowOpacity = 1.35
        containerView.layer.shadowRadius = 2.5
        containerView.layer.masksToBounds = false
    }
    
    @IBAction func lifeTimeEarningTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "LifeTimeEarningClicked", info: nil)

        let partnershipWalletVC = storyboard?.instantiateViewController(withIdentifier: "PartnershipWalletViewController") as! PartnershipWalletViewController
        partnershipWalletVC.bannersArray = bannersArray
        partnershipWalletVC.delegates = self
        partnershipWalletVC.currentAffilateAmount = currentAffilateAmount;
        navigationController?.pushViewController(partnershipWalletVC, animated: true)
    }
    
    func updateAffilateAmount(newAmount: String, totalEarning: String) {
        currentAffilateAmount = newAmount
        if totalEarning.count != 0{
            lblEarningAmount.text = "pts" + totalEarning
        }
    }
}


extension PartnershipProgramViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100 {
            return bonanazaBannersArray.count
        }
        return bannersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 100 {
            return CGSize(width: UIScreen.main.bounds.width - 20, height: 90)
        }
        return CGSize(width: UIScreen.main.bounds.width - 20, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 100 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
            let details = bonanazaBannersArray[indexPath.row]
            cell.configBannerDataForBonanaza(details: details)
            cell.lblTimer.isHidden = false
            cell.imgViewClock.isHidden = true

            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
            let details = bannersArray[indexPath.row]
            cell.configBannerData(details: details)
            cell.lblTimer.isHidden = true
            cell.imgViewClock.isHidden = true

            return cell
        }
    }
    
//    if collectionView.tag == 100 {
//        referalPolicyButtonTapped(nil)
//    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if collectionView.tag == 100 {
            referalPolicyButtonTapped(nil)
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                let details = bannersArray[indexPath.item]
                
                if details.redirectType == "2" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.redirectType == "3" {
                    
                    if details.websiteUrl.count == 0{
                        return;
                    }
                    let webviewVC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                    webviewVC.urlString = details.websiteUrl
                    navVC.pushViewController(webviewVC, animated: true)
                }
                else if details.redirectType == "4" {
                    let playerView = YoutubeVideoPlayerView(frame: APPDELEGATE.window!.frame)
                    playerView.videoID = details.videoUrl
                    playerView.playView()
                    APPDELEGATE.window!.addSubview(playerView)
                }
                else if details.redirectType == "6" {
                    let promoCodeVC = storyboard.instantiateViewController(withIdentifier: "PromotionsViewController") as! PromotionsViewController
                    promoCodeVC.isFromSetting = true
                    navVC.pushViewController(promoCodeVC, animated: true)
                }
                else if details.redirectType == "7" {
                    let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
                    navVC.pushViewController(addCashVC, animated: true)
                }
                else if details.redirectType == "8" {
                    
                    let howToPlayVC = storyboard.instantiateViewController(withIdentifier: "HowToPlayViewController") as! HowToPlayViewController
                    navVC.pushViewController(howToPlayVC, animated: true)
                }
            }
        }
    }
    
}

