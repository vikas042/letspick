//
//  CreatePrivateLeagueViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 21/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire

class CreatePrivateLeagueViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var swicthButton: UISwitch!
    @IBOutlet weak var bottomTabHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var createButton: SolidButton!
    
    @IBOutlet weak var setButton: SolidButton!
    @IBOutlet weak var lblMultipleWinnerTitle: UILabel!
    @IBOutlet weak var pricePoolView: UIView!

    @IBOutlet weak var lblLeagueNameTitle: UILabel!
    @IBOutlet weak var lblLeagueTypeTitle: UILabel!
    @IBOutlet weak var lblLeagueSize: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblDivideThePrize: UILabel!
    @IBOutlet weak var noOfWinnerView: UIView!
    @IBOutlet weak var createLeagueButton: UIButton!
    @IBOutlet weak var checkBoxButton: UIButton!
    
    @IBOutlet weak var lblPricePoolTitle: UILabel!
    @IBOutlet weak var lblPricePool: UILabel!
    @IBOutlet weak var lblJoiningAmountTitle: UILabel!
    @IBOutlet weak var txtFieldWinningAmount: UITextField!
    @IBOutlet weak var txtFieldLeagueType: UITextField!
    @IBOutlet weak var txtFieldLeagueName: UITextField!
    @IBOutlet weak var txtFieldSize: UITextField!
    @IBOutlet weak var txtFieldNumberOfWinners: UITextField!

    var userTeamsArray = Array<UserTeamDetails>()
    var matchDetails: MatchDetails?
    var rankCount = 0
    var teamPickerView = UIPickerView()
    var leagueTypeArray = ["Classic League".localized(), "Batting League".localized(), "Bowling League".localized()]
    

    
    var fantasyType = "1"
    var gameType = GameType.Cricket.rawValue
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppHelper.designBottomTabDesing(bottomView);
        headerView.headerTitle = "Create Private League".localized()
        createButton.setTitle("Create Private League".localized(), for: .normal)
        updateTabsData()
        lblLeagueTypeTitle.text = "League Type".localized()
        lblMultipleWinnerTitle.text = "Multiple Winners".localized()
        lblJoiningAmountTitle.text = "Joining Amount".localized()
        lblPricePoolTitle.text = "Prize pool".localized()
        lblLeagueNameTitle.text = "League Name".localized()
        setButton.setTitle("SET".localized(), for: .normal)
        txtFieldNumberOfWinners.placeholder = "Number of Winner".localized()
        lblDivideThePrize.text = "DividePrizes".localized()
        
        if fantasyType == "1" {
            txtFieldLeagueType.text = "Classic League".localized()
            lblLeagueSize.text = "League Size (Maximum ".localized() + "1000)"
        }
        else if fantasyType == "2" {
            txtFieldLeagueType.text = "Batting League".localized()
            lblLeagueSize.text = "League Size (Maximum ".localized() + "4)"
        }
        else if fantasyType == "3" {
            txtFieldLeagueType.text = "Bowling League".localized()
            lblLeagueSize.text = "League Size (Maximum ".localized() + "4)"
        }
        else if fantasyType == "4" {
            txtFieldLeagueType.text = "Reverse League".localized()
            lblLeagueSize.text = "League Size (Maximum ".localized() + "1000)"
        }
        else if fantasyType == "5" {
            txtFieldLeagueType.text = "Wizard League".localized()
            lblLeagueSize.text = "League Size (Maximum ".localized() + "1000)"
        }

        lblPricePool.text = "pts 0"
        navigationController?.navigationBar.isHidden = true
        noOfWinnerView.isHidden = true
        teamPickerView.delegate = self;
        txtFieldLeagueType.inputView = teamPickerView
        tblView.register(UINib(nibName: "InputFiledTableViewCell", bundle: nil), forCellReuseIdentifier: "InputFiledTableViewCell")
        showShowOnCreateLeagueButton()
    }
    
    func backgroundGrident() {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor(red: 92.0/255, green: 221.0/255, blue: 225.0/255, alpha: 1).cgColor, UIColor(red: 72.0/255, green: 140.0/255, blue: 255.0/255, alpha: 1).cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: pricePoolView.frame.size.width, height: pricePoolView.frame.size.height)
        gradient.cornerRadius = 5.0
        pricePoolView.layer.insertSublayer(gradient, at: 0)
        pricePoolView.layer.cornerRadius = 5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if AppHelper.isApplicationRunningOnIphoneX() {
            bottomTabHeightConstraint.constant = 78.0
        }
    }
    
    override func viewDidLayoutSubviews() {
        removebackgroundGrident()
        backgroundGrident()
    }
    
    func removebackgroundGrident() {
         pricePoolView.layer.sublayers = pricePoolView.layer.sublayers?.filter { theLayer in
               !theLayer.isKind(of: CAGradientLayer.classForCoder())
         }
     }

    @IBAction func switchButtonTapped(_ sender: Any) {

    }
        
    func showShowOnCreateLeagueButton() {
        createLeagueButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        createLeagueButton.layer.shadowOffset = CGSize(width: 0, height: 3)
        createLeagueButton.layer.shadowOpacity = 2.0
        createLeagueButton.layer.shadowRadius = 10.0
        createLeagueButton.layer.masksToBounds = false
        createLeagueButton.backgroundColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
    }

    @IBAction func createPrivateLeagueButtonTapped(_ sender: Any) {
        
        if txtFieldLeagueType.text?.count == 0{
            AppHelper.showAlertView(message: "Please select the league Type".localized(), isErrorMessage: true)
            return;
        }
        else if txtFieldLeagueName.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter the league name".localized(), isErrorMessage: true)
            return;
        }
        else if txtFieldSize.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter the league size".localized(), isErrorMessage: true)
            return;
        }
        else if txtFieldWinningAmount.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter the joining amount".localized(), isErrorMessage: true)
            return;
        }
        else if Int(txtFieldSize!.text!) ?? 0 < 2{
            AppHelper.showAlertView(message: "League size atleast should be 2".localized(), isErrorMessage: true)
            return;
        }
        else if Int(txtFieldWinningAmount!.text!) ?? 0 < 10{
            AppHelper.showAlertView(message: "Joining Amount can not be less the 10 rupee".localized(), isErrorMessage: true)
            return;
        }
        
        if (fantasyType == "2") || (fantasyType == "3"){
            if Int(txtFieldSize.text!) ?? 0 > 4{
                AppHelper.showAlertView(message: "Not more than 4 League size".localized(), isErrorMessage: true)
                return;
            }
        }
        else{
            if Int(txtFieldSize.text!) ?? 0 > 1000{
                AppHelper.showAlertView(message: "Not more than 1000 League size".localized(), isErrorMessage: true)
                return;
            }
        }
                
        let visibleCellsArray = tblView.visibleCells
        var rankAmount = 0
        var rankAmountArray = Array<String>()
        
        let size = Float(txtFieldSize!.text!) ?? 0
        let joiningAmount = Float(txtFieldWinningAmount!.text!) ?? 0
//        let pricePool = Int((joiningAmount / (1 + UserDetails.sharedInstance.balleBaaziCommission/100))*size)
        let pricePool = Int((joiningAmount*size) - (joiningAmount*size)*UserDetails.sharedInstance.balleBaaziCommission/100)

        if checkBoxButton.isSelected {
            if visibleCellsArray.count == 0{
                return
            }
            
            for cell in visibleCellsArray {
                let visibleCell = cell as! InputFiledTableViewCell
                if visibleCell.txtFieldRank!.text?.count == 0{
                    let indexPath = tblView.indexPath(for: visibleCell)
                    AppHelper.showAlertView(message: "Please enter rank " + String(indexPath!.row + 1), isErrorMessage: true)
                    return;
                }
                rankAmountArray.append(visibleCell.txtFieldRank!.text!)
                rankAmount = rankAmount + (Int(visibleCell.txtFieldRank!.text!) ?? 0)
            }
            
            if rankAmount != pricePool{
                AppHelper.showAlertView(message: "Please distribute the prize amount correctly.", isErrorMessage: true)
                return;
            }
        }
        else{
            rankAmountArray.append(txtFieldWinningAmount!.text!)
        }
        
        callVerifyInvitationCodeAPI(rankArray: rankAmountArray, pricePool: pricePool)
    }
    
    
    @IBAction func setButtonTapped(_ sender: Any) {
        
        if txtFieldNumberOfWinners!.text?.count == 0{
            return
        }
        let fieldSize = txtFieldSize.text?.convertStringToInt()
        if fieldSize?.count == 0{
            AppHelper.showAlertView(message: "Please enter the league size", isErrorMessage: true)
            return;
        }
        else if Int(fieldSize!)! < 2{
            AppHelper.showAlertView(message: "League size atleast should be 2", isErrorMessage: true)
            return;
        }
        
        txtFieldNumberOfWinners.resignFirstResponder()
        rankCount = Int(txtFieldNumberOfWinners!.text!) ?? 0

        AppxorEventHandler.logAppEvent(withName: "SelectMultipleWinnerOptions", info: ["SportType": "Cricket", "Number": String(rankCount)])

        if rankCount > Int(fieldSize!)! {
            AppHelper.showAlertView(message: "Winner can not be more than league size.", isErrorMessage: true)
            return;
        }
//        lblDivideThePrize.isHidden = false
        scrollContentViewHeightConstraint.constant = CGFloat(570.0 + Float(rankCount) * 64.0)
        tblView.reloadData()
    }
    
    @IBAction func checkBoxButtonTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected{
            tblView.isHidden = false
            noOfWinnerView.isHidden = false
            if rankCount > 0{
//                lblDivideThePrize.isHidden = false
                scrollContentViewHeightConstraint.constant = CGFloat(520.0 + Float(rankCount) * 64.0)
            }
            view.layoutIfNeeded()
        }
        else{
            tblView.isHidden = true
            lblDivideThePrize.isHidden = true
            noOfWinnerView.isHidden = true
            scrollContentViewHeightConstraint.constant = 520.0
            view.layoutIfNeeded()
        }
    }
    
    func showJoiningFee() {
        txtFieldSize.text = txtFieldSize.text?.convertStringToInt()
        txtFieldWinningAmount.text = txtFieldWinningAmount.text?.convertStringToInt()
        
        if (txtFieldSize.text?.count == 0) || (txtFieldWinningAmount.text?.count == 0){
            lblPricePool.text = "pts 0"
            return;
        }
        
        let size = Float(txtFieldSize!.text!) ?? 0
        let joiningAmount = Float(txtFieldWinningAmount!.text!) ?? 0
//        let pricePool = (joiningAmount / (1 + UserDetails.sharedInstance.balleBaaziCommission/100))*size
        
        let pricePool = Int((joiningAmount*size) - (joiningAmount*size)*UserDetails.sharedInstance.balleBaaziCommission/100)

        lblPricePool.text = "pts" + String(Int(pricePool))
    }
    
    func callVerifyInvitationCodeAPI(rankArray: Array<String>, pricePool: Int)  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var fantasyType = ""
        
        var fantasyName = "Classic"
 
        if txtFieldLeagueType.text == "Classic League".localized() {
            fantasyType = "1"
            fantasyName = "Classic"
        }
        else if txtFieldLeagueType.text == "Batting League".localized() {
            fantasyType = "2"
            fantasyName = "Batting"
        }
        else if txtFieldLeagueType.text == "Bowling League".localized() {
            fantasyType = "3"
            fantasyName = "Bowling"
        }
        else if txtFieldLeagueType.text == "Reverse League".localized() {
            fantasyType = "4"
            fantasyName = "Reverse"
        }
        else if txtFieldLeagueType.text == "Wizard League".localized() {
            fantasyType = "5"
            fantasyName = "Wizard"
        }


        AppxorEventHandler.logAppEvent(withName: "CreateLeagueClicked", info: ["SportType": "Cricket", "FANTASY_TYPE": fantasyName])

        if fantasyType.count == 0{
            AppHelper.showAlertView(message: "Please select the league type", isErrorMessage: true)
            return
        }
        

        var isMultiJoiningAllow = "0"
        
        if swicthButton.isOn {
            isMultiJoiningAllow = "1"
        }
        
        var urlString = ""
        
        if gameType == GameType.Cricket.rawValue{
            urlString = kMatch
        }
        else if gameType == GameType.Kabaddi.rawValue{
            urlString = kKabaddiMatchURL
        }
        else if gameType == GameType.Football.rawValue{
            urlString = kFootballMatchURL
        }
        else if gameType == GameType.Basketball.rawValue{
            urlString = kBasketballMatchURL
        }
        else if gameType == GameType.Baseball.rawValue{
            urlString = kBaseballMatchURL
        }
        
        let parameters: Parameters = ["option": "create_league","match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID, "fantasy_type": fantasyType, "league_name": txtFieldLeagueName!.text!, "size": txtFieldSize!.text!, "winning_amount": String(pricePool), "multiple_join": isMultiJoiningAllow, "multiple_winner": "1","total_winners": String(rankCount), "ranks": rankArray]
        weak var weakSelf = self;
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                
                if let response = result!["response"]?.dictionary{
                    let leagueDetails = LeagueDetails.getLeagueDetails(details: response)
                    leagueDetails.isPrivateLeague = true;
                    if statusCode == "200"{
                        weakSelf?.callLeagueValidationAPI(details: leagueDetails)
                    }
                    else{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                        AppHelper.sharedInstance.removeSpinner()
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                    AppHelper.sharedInstance.removeSpinner()
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
    
    
    func callLeagueValidationAPI(details: LeagueDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        var optionValue = ""
        
        weak var weakSelf = self

        var urlString = kMatch
        
        if gameType == GameType.Cricket.rawValue{
            urlString = kMatch
            optionValue = "league_prev_data_v2"
        }
        else if gameType == GameType.Kabaddi.rawValue{
            urlString = kKabaddiMatchURL
            optionValue = "join_league_preview_v1"
        }
        else if gameType == GameType.Football.rawValue{
            urlString = kFootballMatchURL
            optionValue = "join_league_preview_v1"
        }
        else if gameType == GameType.Basketball.rawValue{
            urlString = kBasketballMatchURL
            optionValue = "join_league_preview_v1"
        }
        else if gameType == GameType.Baseball.rawValue{
            urlString = kBaseballMatchURL
            optionValue = "join_league_preview_v1"
        }
        
        let params = ["option": optionValue,"check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": details.matchKey, "league_id": details.leagueId, "fantasy_type": details.fantasyType]

        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string
                if let response = result!["response"]?.dictionary {
                    if let tempArray = response["user_teams"]?.array{
                        UserDetails.sharedInstance.userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: tempArray, matchDetails: weakSelf!.matchDetails!)
                    }

                }

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == details.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(details.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                                        
//                                        let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = details
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = weakSelf!.matchDetails
//                                        addCashVC?.userTeamArray = teamArray
//                                        addCashVC?.categoryName = "Private League"
//                                        addCashVC?.selectedGameType = weakSelf!.gameType
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    
                                    
                                    if weakSelf!.gameType == GameType.Cricket.rawValue{
                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                        playerVC.matchDetails = weakSelf?.matchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                    else if weakSelf!.gameType == GameType.Kabaddi.rawValue{

                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                        playerVC.matchDetails = weakSelf?.matchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                    else if weakSelf!.gameType == GameType.Football.rawValue{
                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                        playerVC.matchDetails = weakSelf?.matchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                    else if weakSelf!.gameType == GameType.Basketball.rawValue{
                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                        playerVC.matchDetails = weakSelf?.matchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                    else if weakSelf!.gameType == GameType.Baseball.rawValue{
                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                        playerVC.matchDetails = weakSelf?.matchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{

                            if weakSelf!.gameType == GameType.Cricket.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.gameType == GameType.Kabaddi.rawValue{

                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.gameType == GameType.Football.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.gameType == GameType.Basketball.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.gameType == GameType.Baseball.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                            
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == details.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: details, userTeamArray: teamArray, ticketDetails: ticketDetais)
                            }
                            else{
                                if weakSelf!.gameType == GameType.Cricket.rawValue{
                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                     playerVC.matchDetails = weakSelf?.matchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }
                                 else if weakSelf!.gameType == GameType.Kabaddi.rawValue{

                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                     playerVC.matchDetails = weakSelf?.matchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }
                                 else if weakSelf!.gameType == GameType.Football.rawValue{
                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                     playerVC.matchDetails = weakSelf?.matchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }
                                 else if weakSelf!.gameType == GameType.Basketball.rawValue{
                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                     playerVC.matchDetails = weakSelf?.matchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }
                                 else if weakSelf!.gameType == GameType.Baseball.rawValue{
                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                     playerVC.matchDetails = weakSelf?.matchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }                            }
                        }
                        else{
                                    
                            if weakSelf!.gameType == GameType.Cricket.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.gameType == GameType.Kabaddi.rawValue{

                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.gameType == GameType.Football.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.gameType == GameType.Basketball.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.gameType == GameType.Baseball.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.matchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    func updateTabsData() {
        leagueTypeArray.removeAll()
        if gameType == GameType.Cricket.rawValue {

            let matchFantasyType = matchDetails?.matchFantasyType ?? "1"

            let tabsArray = matchFantasyType.components(separatedBy: ",")
            for str in tabsArray{
                if str == "1" {
                    leagueTypeArray.append("Classic League".localized())
                }
                else if str == "2" {
                    leagueTypeArray.append("Batting League".localized())
                }
                else if str == "3" {
                    leagueTypeArray.append("Bowling League".localized())
                }
                else if str == "4" {
                    leagueTypeArray.append("Reverse League".localized())
                }
                else if str == "5" {
                    leagueTypeArray.append("Wizard League".localized())
                }
            }
        }
        else{
            leagueTypeArray.append("Classic League".localized())
        }
    }
    
//    func callLeagueValidationAPI(leagueDetails: LeagueDetails)  {
//
//        if !AppHelper.isInterNetConnectionAvailable(){
//            return;
//        }
//        weak var weakSelf = self
//        var urlString = kMatch
//        var optionValue = ""
//
//        if gameType == GameType.Cricket.rawValue{
//           urlString = kMatch
//           optionValue = "league_prev_data_v2"
//        }
//        else if gameType == GameType.Kabaddi.rawValue{
//           urlString = kKabaddiMatchURL
//           optionValue = "join_league_preview_v1"
//        }
//        else if gameType == GameType.Football.rawValue{
//           urlString = kFootballMatchURL
//           optionValue = "join_league_preview_v1"
//        }
//        else if gameType == GameType.Basketball.rawValue{
//           urlString = kBasketballMatchURL
//           optionValue = "join_league_preview_v1"
//        }
//        else if gameType == GameType.Baseball.rawValue{
//           urlString = kBaseballMatchURL
//           optionValue = "join_league_preview_v1"
//        }
//
//        let params = ["option": optionValue,"check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType]
//
//        //        let params = ["option": "league_prev_data_v2","check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType]
//
//
//        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
//            AppHelper.sharedInstance.removeSpinner()
//
//            if result != nil{
//                let statusCode = result!["status"]?.string
//                let message = result!["message"]?.string
//
//                DispatchQueue.main.async {
//                    let response = result!["response"]?.dictionary
//                    let is_multi_joining = response?["is_multi_joining"]?.intValue
//                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
//                    var ticketDetais: TicketDetails?
//                    if let applied_ticket = response?["ticket_applied"]?.intValue{
//                        UserDetails.sharedInstance.ticketApplied = applied_ticket
//
//                        if (response?["ticket"]?.dictionary) != nil{
//                            if (response?["ticket"]?.dictionary) != nil{
//                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
//                            }
//                        }
//                    }
//
//                    if statusCode == "401"{
//                        let titleMessage = "Oops! Low Balance".localized()
//
//                        if let response = result!["response"]?.dictionary{
//                            if let teamsArray = response["teams"]?.array{
//                                if teamsArray.count != 0{
//                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
//                                        teamDetails.fantasyType == leagueDetails.fantasyType
//                                    })
//
//                                    var teamArray = Array<UserTeamDetails>()
//
//                                    for teamDetails in fantacyArray{
//                                        for validTeamNumber in teamsArray{
//                                            if let teamNumber = validTeamNumber.string{
//                                                if teamNumber == teamDetails.teamNumber{
//                                                    teamArray.append(teamDetails)
//                                                    break;
//                                                }
//                                            }
//                                        }
//                                    }
//
//                                    let is_multi_joining = response["is_multi_joining"]?.intValue
//                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
//                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
//                                    let responseAmt = Float(creditRequired)!
//
//                                    let joiningAmount = Float(leagueDetails.joiningAmount)!
//                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
//                                    let message = String(format: "LowBalanceMessage".localized(), String(roundFigureAmt))
//
//                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//
//                                        let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = weakSelf!.matchDetails
//                                        addCashVC?.userTeamArray = teamArray
//                                        addCashVC?.selectedGameType = weakSelf!.gameType
//
//                                        addCashVC?.categoryName = kPrivateLeague
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
//                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
//                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                        navVC.present(alert, animated: true, completion: nil)
//                                    }
//                                }
//                                else{
//                                    let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
//                                    playerVC.matchDetails = weakSelf?.matchDetails
//                                    playerVC.leagueDetails = leagueDetails
//                                    playerVC.ticketDetails = ticketDetais
//                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
//                                }
//                            }
//                            else{
//                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
//                            }
//                        }
//                        else{
//                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
//                        }
//                    }
//                    else if (statusCode == "400") || (statusCode == "402"){
//                        let titleStr = result!["title"]?.string ?? ""
//                        if titleStr == "no_teams"{
//                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
//                            playerVC.matchDetails = weakSelf?.matchDetails
//                            playerVC.leagueDetails = leagueDetails
//                            playerVC.ticketDetails = ticketDetais
//                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
//                        }
//                        else
//                        {
//                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
//                        }
//                    }
//                    else if statusCode == "200"{
//                        let response = result!["response"]?.dictionary
//
//                        if let teamsArray = response?["teams"]?.array{
//                            if teamsArray.count != 0{
//                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
//                                    teamDetails.fantasyType == leagueDetails.fantasyType
//                                })
//
//                                var teamArray = Array<UserTeamDetails>()
//
//                                for teamDetails in fantacyArray{
//
//                                    for validTeamNumber in teamsArray{
//                                        if let teamNumber = validTeamNumber.string{
//                                            if teamNumber == teamDetails.teamNumber{
//                                                teamArray.append(teamDetails)
//                                                break;
//                                            }
//                                        }
//                                    }
//                                }
//
//                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails, userTeamArray: teamArray, ticketDetails: ticketDetais)
//                            }
//                            else{
//                                let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
//                                playerVC.matchDetails = weakSelf?.matchDetails
//                                playerVC.leagueDetails = leagueDetails
//                                playerVC.ticketDetails = ticketDetais
//                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
//
//                            }
//                        }
//                        else{
//                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
//                            playerVC.matchDetails = weakSelf?.matchDetails
//                            playerVC.leagueDetails = leagueDetails
//                            playerVC.ticketDetails = ticketDetais
//                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
//
//                        }
//                    }
//                    else{
//                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
//                    }
//                }
//            }
//            else{
//                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
//            }
//        }
//    }
    
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, ticketDetails: TicketDetails?)  {

        let joinedLeagueConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
        joinedLeagueConfirmVC?.leagueDetails = leagueDetails
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails
        joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
        joinedLeagueConfirmVC?.isNeedToShowMatchClosePopup = true
        joinedLeagueConfirmVC?.leagueCategoryName = kPrivateLeague
        joinedLeagueConfirmVC?.matchDetails = matchDetails
        joinedLeagueConfirmVC?.selectedGameType = gameType
        navigationController?.pushViewController(joinedLeagueConfirmVC!, animated: true)
    }

    //MARK:- Picker View Data Source and Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
        return leagueTypeArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let leagueType = leagueTypeArray[row]
        return leagueType
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        let leagueType = leagueTypeArray[row]

        if leagueType == "Classic League".localized() {
            fantasyType = "1"
        }
        else if leagueType == "Batting League".localized() {
            fantasyType = "2"
        }
        else if leagueType == "Bowling League".localized() {
            fantasyType = "3"
        }
        else if leagueType == "Reverse League".localized() {
            fantasyType = "4"
        }
        else if leagueType == "Wizard League".localized() {
            fantasyType = "5"
        }

        if fantasyType == "1" {
            txtFieldLeagueType.text = leagueType
            lblLeagueSize.text = "League Size (Maximum ".localized() + "1000)"
        }
        else if fantasyType == "2" {
            txtFieldLeagueType.text = leagueType
            lblLeagueSize.text = "League Size (Maximum ".localized() + "4)"
        }
        else if fantasyType == "3" {
            txtFieldLeagueType.text = leagueType
            lblLeagueSize.text = "League Size (Maximum ".localized() + "4)"
        }
        else if fantasyType == "4" {
            txtFieldLeagueType.text = leagueType
            lblLeagueSize.text = "League Size (Maximum ".localized() + "4)"
        }
        else if fantasyType == "5" {
            txtFieldLeagueType.text = leagueType
            lblLeagueSize.text = "League Size (Maximum ".localized() + "4)"
        }

        txtFieldLeagueType.text = leagueType
    }
    
    //MARK:- Table view Data and Delegats
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return rankCount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 64.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "InputFiledTableViewCell") as? InputFiledTableViewCell
        
        if cell == nil {
            cell = InputFiledTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "InputFiledTableViewCell")
        }
        cell?.selectionStyle = .none
        cell?.lblRowNumber.text = String(indexPath.row + 1)
        cell?.cupImgView.isHidden = false
        if indexPath.row > 2{
            cell?.cupImgView.isHidden = true
        }
        cell?.txtFieldRank.placeholder = "Rank".localized() + " " + String(indexPath.row + 1)
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK:- Textfield Delegate
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        
        if textField == txtFieldLeagueType{
            return false
        }
        else if textField == txtFieldLeagueName{
            if (textField.text?.count)! < 15 {
                return true
            }
            else{
                return false
            }
        }

        if (textField == txtFieldSize) ||  (textField == txtFieldWinningAmount){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showJoiningFee()
            }
        }
        
        return true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
