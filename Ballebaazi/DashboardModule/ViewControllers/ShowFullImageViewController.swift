//
//  ShowFullImageViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 13/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class ShowFullImageViewController: UIViewController, UIScrollViewDelegate  {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgView: UIImageView!
    var imageUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = URL(string: imageUrl){
            imgView.setImage(with: url , placeholder: UIImage(named: ""), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: "")
                }
            })
        }
        else{
            imgView.image = UIImage(named: "")
        }
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        scrollView.alwaysBounceVertical = false
        scrollView.alwaysBounceHorizontal = false
//        scrollView.showsVerticalScrollIndicator = true
        scrollView.flashScrollIndicators()
        imgView.isUserInteractionEnabled = true;
        scrollView.isUserInteractionEnabled=true;

        return imgView;
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
    
        navigationController?.popViewController(animated: true)
    }
}
