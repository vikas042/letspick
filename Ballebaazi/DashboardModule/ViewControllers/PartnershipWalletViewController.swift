//
//  PartnershipWalletViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 28/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import FBSDKShareKit


protocol PartnershipWalletViewControllerDelegate {
    func updateAffilateAmount(newAmount: String, totalEarning: String)
}

class PartnershipWalletViewController: UIViewController, SharingDelegate, AffiliateWalletViewDelegate, MyEarningsViewControllerDelegates  {

    @IBOutlet weak var walletTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var inviteCodeView: UIView!
    
    @IBOutlet weak var lblInviteFriends: UILabel!
    @IBOutlet weak var lblWallet: UILabel!
    @IBOutlet weak var lblAffiletWallet: UILabel!
   
    @IBOutlet weak var lblTotalWalletAmount: UILabel!
    @IBOutlet weak var lblTotalAffiletWalletAmount: UILabel!

    @IBOutlet weak var lblMyReferrals: UILabel!
    
    @IBOutlet weak var lblMyTransactions: UILabel!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var affilateView: UIView!
    lazy var bannersArray = Array<BannerDetails>()
    lazy var currentAffilateAmount = "0";
    var delegates: PartnershipWalletViewControllerDelegate?
    
    @IBOutlet weak var lblCopy: UILabel!
    var isNeedToCallAPI = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell")
        showShadow(containerView: affilateView)
        showShadow(containerView: walletView)
        showShadow(containerView: inviteCodeView)
        lblCode.text = UserDetails.sharedInstance.referralCode
        headerView.headerTitle = "My Earning".localized()
        lblWallet.text = "My Wallet".localized()
        lblAffiletWallet.text = "My Affiliation".localized()
        lblCopy.text = "Copy".localized()
        lblInviteFriends.text = "INVITE FRIENDS".localized()
        lblMyReferrals.text = "My Referrals".localized()
        lblMyTransactions.text = "My Transactions".localized()
        lblTotalWalletAmount.text = "pts\(UserDetails.sharedInstance.totalCredits)"
        lblTotalAffiletWalletAmount.text = "pts" + AppHelper.formatDecimalNumberString(digites: currentAffilateAmount);
        
        if isNeedToCallAPI {
            callGetBannerDetailsAPI()
        }
        else{
            if bannersArray.count == 0 {
                walletTopConstraint.constant = 10
                collectionView.isHidden = true
                containerView.layoutIfNeeded()
            }
        }
    }
    
    func showShadow(containerView: UIView) {
                
        containerView.layer.cornerRadius = 6.0
        containerView.layer.shadowColor = UIColor(red: 78.0/255, green: 84.0/255, blue: 90.0/255, alpha: 0.5).cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        containerView.layer.shadowOpacity = 1.35
        containerView.layer.shadowRadius = 2
        containerView.layer.masksToBounds = false
    }

    // MARK: -IBAction Methods
    @IBAction func facebookButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "FacebookClicked", info: nil)
        shareTextOnFaceBook();
    }
    
    @IBAction func telegramButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "TelegramClicked", info: nil)

        let linkShare = "https://Letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!
        let urlString = "tg://msg?text=Hey, I'm playing Fantasy Sports on the Letspick App and winning real cash every day. Join now and start making your teams:\n\(linkShare)"
        guard let tgUrl = URL(string:urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) else { return }
        
        if UIApplication.shared.canOpenURL(tgUrl){
            UIApplication.shared.open(tgUrl, options: [:], completionHandler: nil)
        }
        else{
            AppHelper.showAlertView(message: "Telegram not available", isErrorMessage: true)
        }
    }

    @IBAction func whatsAppButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "WhatsappClicked", info: nil)

        let linkShare = "https://Letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!
        let urlString = "whatsapp://send?text=Hey, I'm playing Fantasy Sports on the Letspick App and winning real cash every day. Join now and start making your teams:\n\(linkShare)"
        guard let tgUrl = URL(string:urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) else { return }
        
        if UIApplication.shared.canOpenURL(tgUrl){
            UIApplication.shared.open(tgUrl, options: [:], completionHandler: nil)
        }
        else{
            AppHelper.showAlertView(message: "Whats app not available", isErrorMessage: true)
        }
    }

    @IBAction func sharingButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "ShareButtonClicked", info: nil)

        let sharingText = "Hey, I'm playing Fantasy Sports on the Letspick App and winning real cash every day. Join now and start making your teams:\n" + "https://Letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!

        let textToShare = [sharingText]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func shareTextOnFaceBook() {
        let shareContent = ShareLinkContent()
        shareContent.contentURL = URL(string: "https://Letspick.app.link/refer?refer_code=" + UserDetails.sharedInstance.referralCode!)!
        shareContent.quote = "Hey, I'm playing Fantasy Sports on the Letspick App and winning real cash every day. Join now and start making your teams."
        ShareDialog(fromViewController: self, content: shareContent, delegate: self).show()
    }

    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        if sharer.shareContent.pageID != nil {
            print("Share: Success")
        }
    }
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print("Share: Fail")
    }
    func sharerDidCancel(_ sharer: Sharing) {
        print("Share: Cancel")
    }


    
    @IBAction func QRCodeButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "QRCodeClicked", info: nil)

        let qrView = PartnershipQRCodeView(frame: APPDELEGATE.window!.frame)
        APPDELEGATE.window!.addSubview(qrView)
    }
    
    @IBAction func myReferalsButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "MyReferralsClicked", info: nil)

        let myEarningsVC = storyboard?.instantiateViewController(withIdentifier: "MyEarningsViewController") as! MyEarningsViewController
        myEarningsVC.selectedTab = SelectedEarningTab.MyReferrals.rawValue
        myEarningsVC.delegates = self
        navigationController?.pushViewController(myEarningsVC, animated: true)
    }
    
    @IBAction func myTransactionButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "MyTransactionClicked", info: nil)

        let myEarningsVC = storyboard?.instantiateViewController(withIdentifier: "MyEarningsViewController") as! MyEarningsViewController
        myEarningsVC.delegates = self
        myEarningsVC.selectedTab = SelectedEarningTab.MyTransactions.rawValue
        navigationController?.pushViewController(myEarningsVC, animated: true)
    }
    
    
    @IBAction func myWalletButtonTapped(_ sender: Any) {
        
        AppxorEventHandler.logAppEvent(withName: "MyWalletClicked", info: nil)

        let walletVC = storyboard?.instantiateViewController(withIdentifier: "WalletViewController")
        navigationController?.pushViewController(walletVC!, animated: true)
    }
    
    @IBAction func copyButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "ReferalCodeClicked", info: nil)

        UIPasteboard.general.string = UserDetails.sharedInstance.referralCode
        AppHelper.showToast(message: "Referral code copied")
    }
    
    @IBAction func myAffilateWalletButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "MyAffiliationClicked", info: nil)

        let affilateView = AffiliateWalletView(frame: APPDELEGATE.window!.frame)
        affilateView.currentAffilateAmount = currentAffilateAmount
        affilateView.delegate = self
        affilateView.updateConfig()
        APPDELEGATE.window?.addSubview(affilateView)
    }

    func updateAffilateAmount(newAmount: String, totalEarning: String) {
        currentAffilateAmount = newAmount
        lblTotalAffiletWalletAmount.text = "pts" + AppHelper.formatDecimalNumberString(digites: currentAffilateAmount);
        delegates?.updateAffilateAmount(newAmount: currentAffilateAmount, totalEarning: totalEarning)
    }
    
    //MARK:- API Related Methods
    func callGetBannerDetailsAPI() {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "get_banner", "user_id": UserDetails.sharedInstance.userID, "referals": "1"]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kPartnershipURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary {
    
                        
                        weakSelf?.collectionView.reloadData()
                        
                        if let bannersArray = response["banners"]?.array {
                            weakSelf?.bannersArray = BannerDetails.getPromoBannerDetails(dataArray: bannersArray)
                            weakSelf?.collectionView.reloadData()
                        }
                        
                        
                        if let affiliateStatsDict = response["affiliateStats"]?.dictionary {
                            
                            weakSelf?.currentAffilateAmount = affiliateStatsDict["current_affiliate_amount"]?.stringValue ?? "0"
                            
                            weakSelf?.lblTotalWalletAmount.text = "pts\(UserDetails.sharedInstance.totalCredits)"
                            weakSelf?.lblTotalAffiletWalletAmount.text = "pts" + AppHelper.formatDecimalNumberString(digites: weakSelf!.currentAffilateAmount);

                            if weakSelf?.bannersArray.count == 0 {
                                weakSelf?.walletTopConstraint.constant = 10
                                weakSelf?.collectionView.isHidden = true
                                weakSelf?.containerView.layoutIfNeeded()
                            }
                        }
                    }
                    weakSelf?.containerView.isHidden = false
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
        
}


extension PartnershipWalletViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 20, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
        let details = bannersArray[indexPath.row]
        cell.configBannerData(details: details)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            let details = bannersArray[indexPath.item]
            
            if details.redirectType == "2" {
                let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
                navVC.pushViewController(leagueVC, animated: true)
            }
            else if details.redirectType == "3" {
                
                if details.websiteUrl.count == 0{
                    return;
                }
                let webviewVC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webviewVC.urlString = details.websiteUrl
                navVC.pushViewController(webviewVC, animated: true)
            }
            else if details.redirectType == "4" {
                let playerView = YoutubeVideoPlayerView(frame: APPDELEGATE.window!.frame)
                playerView.videoID = details.videoUrl
                playerView.playView()
                APPDELEGATE.window!.addSubview(playerView)
            }
            else if details.redirectType == "6" {
                let promoCodeVC = storyboard.instantiateViewController(withIdentifier: "PromotionsViewController") as! PromotionsViewController
                promoCodeVC.isFromSetting = true
                navVC.pushViewController(promoCodeVC, animated: true)
            }
            else if details.redirectType == "7" {
                let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
                navVC.pushViewController(addCashVC, animated: true)
            }
            else if details.redirectType == "8" {
                
                let howToPlayVC = storyboard.instantiateViewController(withIdentifier: "HowToPlayViewController") as! HowToPlayViewController
                navVC.pushViewController(howToPlayVC, animated: true)
            }
        }
    }
    
}
