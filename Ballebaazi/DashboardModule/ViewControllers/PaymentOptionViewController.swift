//
//  PaymentOptionViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 20/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class PaymentOptionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PGTransactionDelegate {
   
    @IBOutlet weak var headerView: CustomNavigationBar!
    
    @IBOutlet weak var lblTotalAmtTitle: UILabel!
    @IBOutlet weak var lblChooseMethod: UILabel!
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var announcementView: AnnouncementView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    lazy var userTeamArray = Array<UserTeamDetails>()
    lazy var categoryName = ""
    var leagueDetails: LeagueDetails?
    var matchDetails: MatchDetails?
    var selectedGameType = GameType.Cricket.rawValue
    var ticketDetails: TicketDetails?
    
    
//    var dataArray = ["Paytm".localized(), "Debit/Credit Cards".localized(), "Net Banking".localized()]
    var dataArray = ["Debit/Credit Cards".localized(), "Paytm".localized(), "Net Banking".localized()]

    var promoCode = ""
    var addCashAmount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
        lblTotalAmtTitle.text = "Total Amount".localized()
        lblChooseMethod.text = "Choose Payment Method".localized()
        headerView.headerTitle = "Payment".localized()
        lblAmount.text = "pts" + addCashAmount
        callUpatePersonalDetailsAPI(isNeedToreturn: false)
    }
    
    // MARK: Paytm Delegate methods.
    
    func sharePayTmPaymentDetails(response: JSON) {
        
        let merchantConfig = PGMerchantConfiguration.default();
        var odrDict = Parameters()
        odrDict["CALLBACK_URL"] = response["CALLBACK_URL"].string ?? ""
        odrDict["CHANNEL_ID"] = response["CHANNEL_ID"].string ?? ""
        odrDict["CHECKSUMHASH"] = response["CHECKSUMHASH"].string ?? ""
        odrDict["CUST_ID"] = response["CUST_ID"].string ?? ""
        odrDict["INDUSTRY_TYPE_ID"] = response["INDUSTRY_TYPE_ID"].string ?? ""
        odrDict["MID"] = response["MID"].string ?? ""
        odrDict["ORDER_ID"] = response["ORDER_ID"].string ?? ""
        odrDict["TXN_AMOUNT"] = response["TXN_AMOUNT"].string ?? ""
        odrDict["WEBSITE"] = response["WEBSITE"].string ?? ""
        
        let order: PGOrder = PGOrder(params: odrDict)
        let transactionController = PGTransactionViewController.init(transactionFor: order)
        transactionController?.serverType = eServerTypeProduction
        transactionController?.merchant = merchantConfig
        transactionController?.delegate = self

        self.navigationController?.pushViewController(transactionController!, animated: true)
    }
    
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        print(responseString)
        if responseString.count == 0 {
            AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            self.navigationController?.popViewController(animated: true)
        }
        else if responseString.contains("Txn Success"){
            showAlertViewForPaymentSuccess();
        }
        else if responseString.contains("User has not completed transaction"){
            showAlertViewForCancel();
        }
        else{

            let deadlineTime = DispatchTime.now() + .milliseconds(100)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        showAlertViewForError()
    }
    
    func didFinishCASTransaction(controller: PGTransactionViewController, response: [NSObject : AnyObject]) {
        print("Transaction::::::::::::")
        print(response)
        showAlertViewForPaymentSuccess();
    }
    
    //MARK: Custom Methods
    func showAlertViewForError()  {
        AppxorEventHandler.logAppEvent(withName: "PaymentFailed", info: ["Option": "PAYTM"])

        weak var weakSelf = self
        let alert = UIAlertController(title: kAlert, message: "kErrorMsg".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
            weakSelf?.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showAlertViewForPaymentSuccess()  {
        AppxorEventHandler.logAppEvent(withName: "PaymentComplete", info: ["Option": "PAYTM"])

        weak var weakSelf = self
        var params = [String : Any]()
        params[amount] = Float(addCashAmount) ?? 0.0
        params[promo_code] = promoCode
        params[payment_method] = "PAYTM"
        params[PLATFORM] = PLATFORM_iPHONE
        CleverTapEventDetails.sendEventToCleverTap(eventName: deposite_success, params: params)
        MixPanelEventsDetails.customDepositSuccessFull(amount: addCashAmount, paymentMethod: "PAYTM")
        MixPanelEventsDetails.customDepositSuccessFullEvent(amountDeposit: addCashAmount, promoCode: promoCode, paymentMethod: "PAYTM")

        let alert = UIAlertController(title: kAlert, message: "PaymentSuccessMsg".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
            if weakSelf!.userTeamArray.count != 0 && (weakSelf!.leagueDetails != nil){
                weakSelf?.goToConfirmLeagueScreen(leagueDetails: weakSelf!.leagueDetails!, userTeamArray: weakSelf!.userTeamArray, categoryName: weakSelf!.categoryName)
            }
            else if weakSelf?.matchDetails?.isQuizMatch ?? false{
                weakSelf?.callLeagueValidationAPI(leagueDetails: weakSelf!.leagueDetails!)
            }
            else{
                weakSelf!.callUpatePersonalDetailsAPI(isNeedToreturn: true)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId]
        WebServiceHandler.performPOSTRequest(urlString: kQuizMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary{
                        if let details = response["league"]?.dictionary{
                            if let quizUrlString = details["quiz_url"]?.string{
                                let quizWebVC = storyboard.instantiateViewController(withIdentifier: "QuizWebViewController") as! QuizWebViewController
                                quizWebVC.matchDetails = self.matchDetails
                                quizWebVC.urlString = quizUrlString
                                quizWebVC.requestId = details["request_id"]?.string ?? ""
                                quizWebVC.leagueId = leagueDetails.leagueId
                                quizWebVC.leagueDetails = leagueDetails
                                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    navVC.pushViewController(quizWebVC, animated: true)
                                }

                            }
                        }
                    }
                }
                else if statusCode == "401"{
                    if let response = result!["response"]?.dictionary {
                        
                        let titleMessage = "Oops! Low Balance".localized()
                        let creditRequired = response["credit_required"]?.stringValue ?? "0"
                        let responseAmt = Float(creditRequired)!

                        let joiningAmount = Float(leagueDetails.joiningAmount)!
                        let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                        let message = String(format: notEnoughPoints, String(roundFigureAmt))
                        
                        let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                        alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                            
//                            let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                            addCashVC?.leagueDetails = leagueDetails
//                            addCashVC?.amount = roundFigureAmt
//                            addCashVC?.matchDetails = self.matchDetails
//                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                navVC.pushViewController(addCashVC!, animated: true)
//                            }
//                        }))
                        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func showAlertViewForCancel()  {
        
        weak var weakSelf = self
        
        let alert = UIAlertController(title: kAlert, message: "PaymentCancelMsg".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
            weakSelf?.navigationController?.popViewController(animated: true)
            
        }))
        self.present(alert, animated: true, completion: nil)
    }

    //MARK: API Related Methods
    func callAddAmountAPI(paymentType: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "add_cash", "user_id": UserDetails.sharedInstance.userID, "pg_type": paymentType, "amount": addCashAmount, "url": "home", "promo": promoCode, "is_mobile": "1", "gateway_version": "1"]
        if paymentType == "1" {
            MixPanelEventsDetails.depositInitiated(amountInitiated: addCashAmount, paymentMethod: "PAYU")
        }
        else if paymentType == "2"{
            MixPanelEventsDetails.depositInitiated(amountInitiated: addCashAmount, paymentMethod: "PAYTM")

        }
        let weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let status = result!["status"]?.string
                if status == "200"{
                    if paymentType == "2"{
                        weakSelf.sharePayTmPaymentDetails(response: result!["response"]!)
                    }
                    else{
                        weakSelf.genrateRequiredHtml(response: (result!["response"]?.dictionary!)!, paymentType: paymentType)
                    }
                }
                else{
                    
                    if let message = result!["message"]?.string{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func genrateRequiredHtml(response: [String: JSON], paymentType: String) {
        var paramsString = ""
        var actionString = ""
        for (key, value) in response {
            if key == "gateway_type"{
                continue
            }
            if key == "action"{
                actionString = value.string!
                continue
            }

            if let keyValue = value.string{
                paramsString += "<input  type=\u{22}hidden\u{22} name=\u{22}" + key + "\u{22} value=\u{22}" + keyValue + "\u{22}>\n"
            }
        }
        
        let htmlString = "<body OnLoad=\u{22}document.payForm.submit();\u{22} ><form method=\u{22}post\u{22} action=\u{22}" + actionString + "\u{22} name=\u{22}payForm\u{22}>" + paramsString + "</form></body>"
        let loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let paymentWebVC = loginStoryboard.instantiateViewController(withIdentifier: "PaymentWebViewController") as? PaymentWebViewController
        paymentWebVC?.ticketDetails = ticketDetails
        paymentWebVC?.htmlString = htmlString
        paymentWebVC?.selectedGameType = selectedGameType
        paymentWebVC?.matchDetails = self.matchDetails
        paymentWebVC?.userTeamArray = userTeamArray
        paymentWebVC?.categoryName = categoryName
        paymentWebVC?.leagueDetails = leagueDetails
        if paymentType == "2" {
            paymentWebVC?.isWebViewForPaytm = true
        }
        
        paymentWebVC!.paymentAmount = addCashAmount
        paymentWebVC!.promoCode = promoCode
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            DispatchQueue.main.async {
                navigationVC.pushViewController(paymentWebVC!, animated: true)
            }
        }
    }
    
    
    func callUpatePersonalDetailsAPI(isNeedToreturn: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            if isNeedToreturn{
                self.navigationController?.popToRootViewController(animated: true)
            }
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "get_profile", "screen_msg": "1" ,"user_id": UserDetails.sharedInstance.userID]
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if !isNeedToreturn {
                if result != nil{
                    let statusCode = result!["status"]
                    if statusCode == "200"{
                        if let response = result!["response"]{
                            
                            if let announcementDict = response["announcement"].dictionary{
                                if let announcement = announcementDict["2"]?.dictionary{
                                    let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                                    self.announcementViewHeightConstraint.constant = 50.0
                                    self.view.layoutIfNeeded()
                                    self.announcementView.showAnnouncementMessage(message: details.message)
                                }
                            }
                            else{
                                self.announcementViewHeightConstraint.constant = 0.0
                                self.view.layoutIfNeeded()
                                self.announcementView.showAnnouncementMessage(message: "")
                            }
                            
                            if let active_ticket = response.dictionary!["active_tickets"]?.stringValue{
                                UserDetails.sharedInstance.active_tickets = active_ticket
                            }
                            else {
                                UserDetails.sharedInstance.active_tickets = "0"
                            }
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                }
            }
            else{
                self.navigationController?.popToRootViewController(animated: true)
            }
                        
            AppHelper.sharedInstance.removeSpinner()
        }
    }

    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, categoryName: String)  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let joinedLeagueConfirmVC = storyboard.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
        joinedLeagueConfirmVC?.leagueDetails = leagueDetails
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails
        joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
        joinedLeagueConfirmVC?.matchDetails = self.matchDetails
        joinedLeagueConfirmVC?.leagueCategoryName = categoryName
        joinedLeagueConfirmVC?.selectedGameType = selectedGameType
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails

        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.pushViewController(joinedLeagueConfirmVC!, animated: true)
        }
    }
    
    //MARK:- Table view Data and Delegats

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as? SettingsTableViewCell
        
        if cell == nil {
            cell = SettingsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "SettingsTableViewCell")
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none

        let title = dataArray[indexPath.row]
        cell?.lblTitle.text = title
        cell?.lblTitle.font = UIFont(name: "OpenSans-Semibold", size: 17)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var paymentType = ""
        
        if indexPath.row == 1{
            callAddAmountAPI(paymentType: "2")
            paymentType = "PAYTM"
        }
        else{
            callAddAmountAPI(paymentType: "1")
            paymentType = "PAYU"
        }
        
        AppxorEventHandler.logAppEvent(withName: "ChoosePaymentClicked", info: ["Option": paymentType])

    }
    
}
