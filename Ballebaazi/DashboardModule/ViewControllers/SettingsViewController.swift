//
//  SettingsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 27/12/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var tblView: UITableView!
    var dataArray = Array<String>()
    var isNotificationAllowedInSetting = false
    var switchButton: UISwitch?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationComesInForground), name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)

        tblView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        headerView.headerTitle = "Settings".localized()                
        dataArray = [ "Language Preference".localized(),
                                 "Notifications".localized(),
                                 "Update my App".localized(),
                                 "Logout".localized()]
        tblView.reloadData()
    }
    
    @objc func applicationComesInForground(notification: Notification)  {
        getNotificationStatus();
    }
    
    func getNotificationStatus() {
        let center = UNUserNotificationCenter.current()
            center.getNotificationSettings { (settings) in
                print(settings.authorizationStatus)
                switch (settings.authorizationStatus) {
                case .authorized:
                    self.isNotificationAllowedInSetting = true
                    break;
                default:
                    break;
            }
        }
    }
    
    //MARK:- Table view Data and Delegats
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as? SettingsTableViewCell
        
        if cell == nil {
            cell = SettingsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "SettingsTableViewCell")
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.switchButton.isHidden = true
        cell?.rightArrow.isHidden = true
        if indexPath.row == 0 {
            cell?.rightArrow.isHidden = false
        }
        else if indexPath.row == 1 {
            cell?.switchButton.isHidden = false
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                cell?.switchButton.isOn = true
            }
            else{
                cell?.switchButton.isOn = false
            }
        }
        cell?.switchButton.addTarget(self, action: #selector(switchButtonTapped(button:)), for: .valueChanged)
        cell?.lblTitle.text = dataArray[indexPath.row]
        cell?.lblTitle.textAlignment = .left
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.row >= dataArray.count {
            return;
        }
        
        let cellTitle = dataArray[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if cellTitle == "Language Preference".localized() {
            let languagueVC = storyboard.instantiateViewController(withIdentifier: "LanguageViewController") as? LanguageViewController
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(languagueVC!, animated: true)
            }
        }
        else if cellTitle == "Notifications".localized()  {
            AppxorEventHandler.logAppEvent(withName: "NotificationToggleClicked", info: nil)

            if UIApplication.shared.isRegisteredForRemoteNotifications {
                UIApplication.shared.unregisterForRemoteNotifications()
            }
            else{
                UIApplication.shared.registerForRemoteNotifications()
            }
        }

        else if cellTitle == "Update my App".localized()  {
            AppHelper.showUpdatePopupFromSetting()
        }
        else if cellTitle == "Logout".localized()  {
            AppxorEventHandler.logAppEvent(withName: "LogoutClicked", info: nil)

            let permissionPopup = PermissionView(frame: APPDELEGATE.window!.frame)
            APPDELEGATE.window?.addSubview(permissionPopup)
            permissionPopup.showAnimation()
            permissionPopup.updateMessage(title: "Logout".localized(), message: "Are your sure you want to logout?".localized())
            permissionPopup.updateButtonTitle(noButtonTitle: "No".localized(), yesButtonTitle: "Yes".localized())

            permissionPopup.noButtonTappedBlock { (status) in
                
            }
            
            permissionPopup.yesButtonTappedBlock { (status) in
                AppHelper.resetDefaults()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userlogoutNotitification"), object: nil)
                //AppHelper.makeLoginAsRootViewController()
                AppHelper.callLoginViewController()
            }

        }
    }

    @objc func switchButtonTapped(button: UISwitch) {
        if isNotificationAllowedInSetting {
            AppHelper.showAlertView(message: "Please allow notification in phone settings", isErrorMessage: true)
        }
        else if UIApplication.shared.isRegisteredForRemoteNotifications {
            UIApplication.shared.unregisterForRemoteNotifications()
            button.isOn = false

        }
        else{
            if !AppHelper.isInterNetConnectionAvailable() {
                return;
            }

            UIApplication.shared.registerForRemoteNotifications()
            button.isOn = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
               self.calllUpdateDeviceTokenAPI()
            })
        }
    }
    
    func calllUpdateDeviceTokenAPI() {
        AppHelper.sharedInstance.displaySpinner()
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let deviceToken: String = UserDetails.sharedInstance.deviceToken

        let params = ["user_id": UserDetails.sharedInstance.userID, "option": "ios_device_token", "device_id": deviceID, "token": deviceToken]
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]?.stringValue
                if statusCode == "200" {
                    
                }
                else{
                    AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                    UIApplication.shared.unregisterForRemoteNotifications()
                    self.switchButton?.isOn = false
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                UIApplication.shared.unregisterForRemoteNotifications()
                self.switchButton?.isOn = false
            }
        }
    }
    
}
