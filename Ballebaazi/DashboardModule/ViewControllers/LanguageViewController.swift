
//
//  LanguageViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 12/09/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit


class LanguageViewController: UIViewController {
    var selectedLanguage = ""

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblSelectLanguague: UILabel!
    @IBOutlet weak var hindiSelectedImgView: UIImageView!
    @IBOutlet weak var englishSelectedImgView: UIImageView!
    @IBOutlet weak var saveButton: SolidButton!
    @IBOutlet weak var hindiLanguegeView: UIView!
    @IBOutlet weak var englishLanguagueView: UIView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var lblHindi: UILabel!
    @IBOutlet weak var lblEnglish: UILabel!
    @IBOutlet weak var hindiSelectBgImgView: UIImageView!
    @IBOutlet weak var englishSelectBgImgView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        englishLanguagueView.layer.borderColor = UIColor(red: 198.0/255, green: 198.0/255, blue: 198.0/255, alpha: 1).cgColor
        englishLanguagueView.layer.borderWidth = 1
        headerView.headerTitle = "Language".localized()
        hindiLanguegeView.layer.borderColor = UIColor(red: 198.0/255, green: 198.0/255, blue: 198.0/255, alpha: 1).cgColor
        hindiLanguegeView.layer.borderWidth = 1
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                hindiButtonTapped(nil)
            }
            else{
                englishButtonTapped(nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblSelectLanguague.text = "selectLanguage".localized()
        lblMessage.text = "LanguageMessage".localized()
        saveButton.setTitle("Save".localized(), for: .normal)
    }
    
    @IBAction func englishButtonTapped(_ sender: Any?) {
        AppxorEventHandler.logAppEvent(withName: "LanguageClicked", info: nil)

        hindiSelectedImgView.isHidden = true
        hindiLanguegeView.backgroundColor = UIColor.white
        englishLanguagueView.backgroundColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)
        lblEnglish.textColor = UIColor.white;
        lblHindi.textColor = UIColor(red: 198.0/255, green: 198.0/255, blue: 198.0/255, alpha: 1);
        englishSelectedImgView.isHidden = false
        englishLanguagueView.layer.borderWidth = 0
        hindiLanguegeView.layer.borderWidth = 1

        englishSelectBgImgView.image = UIImage(named: "UnselectedLanguague")
        hindiSelectBgImgView.image = UIImage(named: "SelectedLanguague")
        selectedLanguage = "en"
    }
    
    @IBAction func hindiButtonTapped(_ sender: Any?) {
        
        AppxorEventHandler.logAppEvent(withName: "LanguageClicked", info: nil)

        englishSelectedImgView.isHidden = true
        hindiSelectedImgView.isHidden = false
        hindiLanguegeView.backgroundColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)
        englishLanguagueView.backgroundColor = UIColor.white
        
        lblHindi.textColor = UIColor.white;
        lblEnglish.textColor = UIColor(red: 198.0/255, green: 198.0/255, blue: 198.0/255, alpha: 1);

        selectedLanguage = "hi"
        hindiLanguegeView.layer.borderWidth = 0
        englishLanguagueView.layer.borderWidth = 1

        hindiSelectBgImgView.image = UIImage(named: "UnselectedLanguague")
        englishSelectBgImgView.image = UIImage(named: "SelectedLanguague")
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        if selectedLanguage.count == 0 {
            return
        }
        var language = "English"
        
        if selectedLanguage == "hi" {
            language = "Hindi"
        }
        
        AppxorEventHandler.logAppEvent(withName: "ChangeLanguage", info: ["Language": language])
        callUpdateUserLanguage(language: selectedLanguage)
    }
    
    func callUpdateUserLanguage(language: String)  {
                
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }

        AppHelper.sharedInstance.displaySpinner()

        let params = ["option": "change_language", "user_id": UserDetails.sharedInstance.userID, "lang": language]

        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    let arr = NSArray(objects: self.selectedLanguage)
                    UserDefaults.standard.set(arr, forKey: kAppLanguague)
                    self.navigationController?.popViewController(animated: true)
                    let profileData = ["Language": self.selectedLanguage] as [String: Any]
                     CleverTapEventDetails.updateProfile(updateProfile: profileData)
                }
            }
        }
    }
    
    
}
