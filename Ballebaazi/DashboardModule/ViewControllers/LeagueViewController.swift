//
//  LeagueViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/7/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class LeagueViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, LeagueContainerCollectionViewCellDelegates, LeagueFilterViewControllerDelegate {
    
    func applyLeagueFilter(rangArray: Array<Any>, poolArray: Array<Any>, teamsArray: Array<Any>, leaguesArray: Array<String>, totalLeagueTypeArray: Array<String>) {
        entryRangeArray = rangArray
        teamsfilterArray = teamsArray
        poolRangeArray = poolArray
        leagueTypeArray = leaguesArray
        allleagueTypeArray = totalLeagueTypeArray
        isFilterApplied = true
        isNeedtoShowLoaderOncomeBack = true;
        filterImgView.image = UIImage(named: "FilterSelected")
        lblFilter.textColor = UIColor(red: 34/255, green: 114/255, blue: 70/255, alpha: 1)
    }
    
    func resetLeagueFilter() {
        entryRangeArray.removeAll()
        teamsfilterArray.removeAll()
        poolRangeArray.removeAll()
        leagueTypeArray.removeAll()
        allleagueTypeArray.removeAll()
        isFilterApplied = false
        lblFilter.textColor = UIColor(red: 130.0/255, green: 131.0/255, blue: 132.0/255, alpha: 1)
        isNeedtoShowLoaderOncomeBack = true;
        filterImgView.image = UIImage(named: "FilterUnselected")
    }
    
    var isFilterApplied = false
    var entryRangeArray = Array<Any>()
    var teamsfilterArray = Array<Any>()
    var poolRangeArray = Array<Any>()
    var leagueTypeArray = Array<String>()
    var allleagueTypeArray = Array<String>()

    var isFromJoiningConfirmation = false

        
    lazy var firstTabFantasyType = FantasyType.Classic.rawValue;
    lazy var secondTabFantasyType = FantasyType.Batting.rawValue;
    lazy var thirdTabFantasyType = FantasyType.Bowling.rawValue;
    lazy var fourthTabFantasyType = FantasyType.Reverse.rawValue;
    lazy var fifthTabFantasyType = FantasyType.Wizard.rawValue;
    lazy var tabsArray = Array<Int>()
    var selectedFantasyType = FantasyType.Classic.rawValue

    
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var createLeagueButton: UIButton!
    
    @IBOutlet weak var filterImgView: UIImageView!
    @IBOutlet weak var leagueFilterButton: UIButton!
    @IBOutlet weak var lblFilter: UILabel!
    
    var userTicketsArray = Array<TicketDetails>()

    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var annuncementView: AnnouncementView!
    @IBOutlet weak var headerView: LetspickFantasyHeaderView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var myTeamButton: CustomBorderButton!
    @IBOutlet weak var myLeaguesButton: CustomBorderButton!
    @IBOutlet weak var lblJoinedLeagueTitle: UILabel!
    @IBOutlet weak var lblMyTeamTitle: UILabel!
    @IBOutlet weak var leagueCollectionView: UICollectionView!
    @IBOutlet weak var lblMyTeam: UILabel!
    @IBOutlet weak var lblJoinLeagues: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!

    lazy var isPullToRefresh = false
    lazy var isUserValidatingToJoinLeague = false
    lazy var isMatchClosingTimeRefereshing = false
    var timer: Timer?
    var matchDetails: MatchDetails?
    
    lazy var leagueArray:Array<LeagueDetails> = []
    lazy var classicLeagueArray:Array<LeagueDetails> = []
    lazy var battingLeagueArray:Array<LeagueDetails> = []
    lazy var bowlingLeagueArray:Array<LeagueDetails> = []
    lazy var reverseLeagueArray:Array<LeagueDetails> = []
    lazy var wizardLeagueArray:Array<LeagueDetails> = []

    lazy var classicCategoryArray:Array<LeagueCategoryDetails> = []
    lazy var battingCategoryArray:Array<LeagueCategoryDetails> = []
    lazy var bowlingCategoryArray:Array<LeagueCategoryDetails> = []
    lazy var reverseCategoryArray:Array<LeagueCategoryDetails> = []
    lazy var wizardCategoryArray:Array<LeagueCategoryDetails> = []

    lazy var isJoiningPop = false
    lazy var isNeedtoShowLoaderOncomeBack = false

    lazy var isFormNotification = false
    lazy var totalClassicTeam = 0
    lazy var totalBattingTeam = 0
    lazy var totalBowlingTeam = 0
    lazy var totalReverseTeam = 0
    lazy var totalWizardTeam = 0

    lazy var seasonKey = ""
    lazy var matchKey = ""

    lazy var selectedFantasy = FantasyType.Classic.rawValue
    lazy var totalClassicJoinedLeague = "0"
    lazy var totalBattingJoinedLeague = "0"
    lazy var totalBowlingJoinedLeague = "0"
    lazy var totalReverseJoinedLeague = "0"
    lazy var totalWizardJoinedLeague = "0"

    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProperties()
    }
        
    //MARK:- Custom Methods
    func setupProperties() {
        
        if isFromJoiningConfirmation {
            headerView.tag = 1001
        }
        leagueCollectionView.isHidden = true
        bottomView.isHidden = true
        filterView.isHidden = true
        lblMyTeam.text = String(totalClassicTeam)
        lblJoinLeagues.text = totalClassicJoinedLeague
        headerView.updateMatchName(matchDetails: matchDetails)
        lblFilter.text = "Filter".localized()
        createLeagueButton.setTitle("Create League+".localized(), for: .normal)

        weak var weakSelf = self
        headerView.clasicButtonTapped { (status) in
            weakSelf?.classicButtonTapped(isAnimation: true)
        }
        
        headerView.battingButtonTapped { (status) in
            weakSelf?.battingButtonTapped(isAnimation: true)
        }
        
        headerView.bowlingButtonTapped { (status) in
            weakSelf?.bowlingButtonTapped(isAnimation: true)
        }
        
        headerView.reverseFantasyButtonTapped { (status) in
            weakSelf?.reverseButtonTapped(isAnimation: true)
        }

        headerView.wizardFantasyButtonTapped { (status) in
            weakSelf?.wizardButtonTapped(isAnimation: true)
        }


        leagueCollectionView.register(UINib(nibName: "LeagueContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueContainerCollectionViewCell")
        NotificationCenter.default.addObserver(self, selector: #selector(weakSelf?.applicationComesInForground), name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)

        if isFormNotification {
            headerView?.tag = 2000
        }
        
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerValue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(weakSelf?.updateTimerValue), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if AppHelper.isApplicationRunningOnIphoneX(){
            bottomViewHeightConstraint.constant = 65;
            bottomView.layoutIfNeeded()
        }
        else{
            view.layoutIfNeeded()
        }
        
        UserDetails.sharedInstance.selectedPlayerList.removeAll();
        navigationController?.navigationBar.isHidden = true
        
        if matchDetails != nil {
            seasonKey = matchDetails!.seasonKey ?? ""
            matchKey = matchDetails!.matchKey 
        }
        
        let urlString = kMatch + "?option=get_match_v1&screen_msg=1&season_key=" + seasonKey + "&match_key=" + matchKey
        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
        if (savedResponse != nil) {
            if leagueArray.count == 0{
                
                let templateID = matchDetails?.templateID
                let isTicketAvailable = matchDetails?.isTicketAvailable ?? false
                matchDetails = MatchDetails.getSelectedMatchDetails(responseResult: savedResponse!)
                updateTabsData()
                matchDetails?.templateID = templateID ?? ""
                matchDetails?.isTicketAvailable = isTicketAvailable
                leagueArray = LeagueDetails.getAllLeagueDetails(responseResult: savedResponse!, matchDetails: matchDetails!)
                if self.leagueArray.count != 0 {
                    self.filterView.isHidden = false
                }

                for ticketDetails in userTicketsArray{
                    for details in leagueArray {
                        if details.templateID == ticketDetails.templateID {
                            details.isTicketAvailable = true
                        }
                    }
                }

                if let response = savedResponse!["response"].dictionary{
                    
                    var recomandedArray = Array<JSON>()
                    let leagueEecommendationString = response["league_recommendation"]?.string ?? ""
                    if leagueEecommendationString.count > 0 {
                        let json = JSON(parseJSON: leagueEecommendationString)
                        if (json.array != nil){
                            recomandedArray = json.array!
                        }
                    }
                    
                    if  response["categorisation"]?.dictionary != nil{
                        if isFilterApplied{
                            (classicCategoryArray, battingCategoryArray, bowlingCategoryArray, reverseCategoryArray, wizardCategoryArray) = LeagueCategoryDetails.parseCategoryDetailsWithFilter(categoryDetails: response["categorisation"]!, leagueArray: leagueArray, recomnadedLeaguesArray: recomandedArray, rangArray: entryRangeArray, poolArray: poolRangeArray, teamsArray: teamsfilterArray, leaguesTypeArray: leagueTypeArray)
                        }
                        else{
                            (classicCategoryArray, battingCategoryArray, bowlingCategoryArray, reverseCategoryArray, wizardCategoryArray) = LeagueCategoryDetails.parseCategoryDetails(categoryDetails: response["categorisation"]!, leagueArray: leagueArray, recomnadedLeaguesArray: recomandedArray)
                        }
                    }
                }

                (totalClassicJoinedLeague, totalBattingJoinedLeague, totalBowlingJoinedLeague, totalReverseJoinedLeague, totalWizardJoinedLeague) = LeagueDetails.getJoinedLeagueCounts(responseResult: savedResponse!)
            }
            
            updateLeaguesArray()
            weak var weakSelf = self

            DispatchQueue.main.async {
                weakSelf?.leagueCollectionView.isHidden = false
                weakSelf?.bottomView.isHidden = false
                weakSelf?.leagueCollectionView.reloadData()
            }
            DispatchQueue.global().async {
                weakSelf?.callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: weakSelf!.isNeedtoShowLoaderOncomeBack)
            }
        }
        else{
            callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {

//        if let totalClassic = UserDefaults.standard.value(forKey: "total_classic") as? String{
//            if Int(totalClassic) ?? 0 <= 0{
//                if let classicBanner = UserDefaults.standard.value(forKey: "classicBanner") as? Date{
//                    let days = getDayOfWeek(classicBanner)
//                    if days >= 1{
//                        showClassicTutorial()
//                    }
//                }
//                else{
//                    showClassicTutorial()
//                }
//            }
//        }
    }
    
    func getDayOfWeek(_ todayDate: Date) -> Int {
        let diffInDays = Calendar.current.dateComponents([.day], from: Date(), to: todayDate).day
        return diffInDays ?? 0
    }

    
    func showClassicTutorial(){
        let classicTutView = JoinLeagueTutorial(frame: APPDELEGATE.window!.frame)
        classicTutView.selectedFansatyType = FantasyType.Classic.rawValue
        classicTutView.selectedGameType = GameType.Cricket.rawValue
        classicTutView.configData()
        APPDELEGATE.window?.addSubview(classicTutView)
        classicTutView.showAnimation()
    }
    
    func showBattingTutorial(){
        let classicTutView = JoinLeagueTutorial(frame: APPDELEGATE.window!.frame)
        classicTutView.selectedFansatyType = FantasyType.Batting.rawValue
        classicTutView.selectedGameType = GameType.Cricket.rawValue
        classicTutView.configData()
        APPDELEGATE.window?.addSubview(classicTutView)
        classicTutView.showAnimation()
    }

    func showBowlingTutorial(){
        let classicTutView = JoinLeagueTutorial(frame: APPDELEGATE.window!.frame)
        classicTutView.selectedFansatyType = FantasyType.Bowling.rawValue
        classicTutView.selectedGameType = GameType.Cricket.rawValue
        classicTutView.configData()
        APPDELEGATE.window?.addSubview(classicTutView)
        classicTutView.showAnimation()
    }

    func showReverseTutorial(){
        let classicTutView = JoinLeagueTutorial(frame: APPDELEGATE.window!.frame)
        classicTutView.selectedFansatyType = FantasyType.Reverse.rawValue
        classicTutView.selectedGameType = GameType.Cricket.rawValue
        classicTutView.configData()
        APPDELEGATE.window?.addSubview(classicTutView)
        classicTutView.showAnimation()
    }

    func showWizardTutorial(){
        let classicTutView = JoinLeagueTutorial(frame: APPDELEGATE.window!.frame)
        classicTutView.selectedFansatyType = FantasyType.Wizard.rawValue
        classicTutView.selectedGameType = GameType.Cricket.rawValue
        classicTutView.configData()
        APPDELEGATE.window?.addSubview(classicTutView)
        classicTutView.showAnimation()
    }

    
    func updateLeagueJoinedStatus(joinedLeague: LeagueDetails, isticketUsed: Bool, fantasyType: String) {
        
        for leagueDetails in leagueArray {
            if leagueDetails.leagueId == joinedLeague.leagueId{
                leagueDetails.joinedLeagueCount += 1
                break;
            }
        }
        if isticketUsed {
            matchDetails?.isTicketAvailable = false
            matchDetails?.templateID = ""
        }
        updateLeaguesArray()
        leagueCollectionView.reloadData()
        
        if fantasyType == "1" {
            headerView.classicButtonTapped(isNeedToScroll: false)
        }
        else if fantasyType == "2" {
            headerView.battingButtonTapped(isNeedToScroll: false)
        }
        else if fantasyType == "3" {
            headerView.bowlingButtonTapped(isNeedToScroll: false)
        }
        else if fantasyType == "4" {
            headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
        }
        else if fantasyType == "5" {
            headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)
    }
    
    @objc func applicationComesInForground(notification: Notification)  {
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            if ((navVC.visibleViewController as? LeagueViewController) != nil) {
                leagueCollectionView.reloadData()
                let urlString = kMatch + "?option=get_match_v1&screen_msg=1&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey
                callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: false)
            }
        }
    }

    @objc func refresh(sender:AnyObject) {
        let urlString = kMatch + "?option=get_match_v1&screen_msg=1&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey
        callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: false)
    }
    
    func updateLeaguesArray()  {
        
        let classicTeams = UserDetails.sharedInstance.userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "1"
        }
        
        let battingTeams = UserDetails.sharedInstance.userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "2"
        }
        
        let bowingTeams = UserDetails.sharedInstance.userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "3"
        }
        
        let reverseTeams = UserDetails.sharedInstance.userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "4"
        }

        
        let wizardTeams = UserDetails.sharedInstance.userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "5"
        }

        
        totalClassicTeam = classicTeams.count
        totalBattingTeam = battingTeams.count
        totalBowlingTeam = bowingTeams.count
        totalReverseTeam = reverseTeams.count
        totalWizardTeam = wizardTeams.count

        if selectedFantasy == FantasyType.Classic.rawValue {
            lblMyTeam.text = String(totalClassicTeam)
            lblJoinLeagues.text = String(totalClassicJoinedLeague)
            
            if totalClassicTeam > 1{
                lblMyTeamTitle.text = "Teams".localized()
            }
            else{
                lblMyTeamTitle.text = "Team".localized()
            }
            
            if Int(totalClassicJoinedLeague)! > 1{
                lblJoinedLeagueTitle.text = "Leagues".localized()
            }
            else{
                lblJoinedLeagueTitle.text = "League".localized()
            }
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            lblMyTeam.text = String(totalBattingTeam)
            lblJoinLeagues.text = String(totalBattingJoinedLeague)
            
            if totalBattingTeam > 1{
                lblMyTeamTitle.text = "Teams".localized()
            }
            else{
                lblMyTeamTitle.text = "Team".localized()
            }
            
            if Int(totalBattingJoinedLeague)! > 1{
                lblJoinedLeagueTitle.text = "Leagues".localized()
            }
            else{
                lblJoinedLeagueTitle.text = "League".localized()
            }
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            lblMyTeam.text = String(totalBowlingTeam)
            lblJoinLeagues.text = String(totalBowlingJoinedLeague)
            
            if totalBowlingTeam > 1{
                lblMyTeamTitle.text = "Teams".localized()
            }
            else{
                lblMyTeamTitle.text = "Team".localized()
            }
            
            if Int(totalBowlingJoinedLeague)! > 1{
                lblJoinedLeagueTitle.text = "Leagues".localized()
            }
            else{
                lblJoinedLeagueTitle.text = "League".localized()
            }
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue {
            lblMyTeam.text = String(totalReverseTeam)
            lblJoinLeagues.text = String(totalReverseJoinedLeague)
            
            if totalReverseTeam > 1{
                lblMyTeamTitle.text = "Teams".localized()
            }
            else{
                lblMyTeamTitle.text = "Team".localized()
            }
            
            if Int(totalReverseJoinedLeague)! > 1{
                lblJoinedLeagueTitle.text = "Leagues".localized()
            }
            else{
                lblJoinedLeagueTitle.text = "League".localized()
            }
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue {
            lblMyTeam.text = String(totalWizardTeam)
            lblJoinLeagues.text = String(totalWizardJoinedLeague)
            
            if totalWizardTeam > 1{
                lblMyTeamTitle.text = "Teams".localized()
            }
            else{
                lblMyTeamTitle.text = "Team".localized()
            }
            
            if Int(totalWizardJoinedLeague)! > 1{
                lblJoinedLeagueTitle.text = "Leagues".localized()
            }
            else{
                lblJoinedLeagueTitle.text = "League".localized()
            }
        }
        classicLeagueArray = leagueArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "1"
        }
        
        battingLeagueArray = leagueArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "2"
        }
        
        bowlingLeagueArray = leagueArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "3"
        }
        
        reverseLeagueArray = leagueArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "4"
        }

        wizardLeagueArray = leagueArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "5"
        }
        
        headerView.updateMatchName(matchDetails: matchDetails)
        updateTimerValue()
        
        if matchDetails!.isMatchClosed{
            AppHelper.showMatchCLosedAlertAndPopToRootViewController()
        }
    }
    
    //MARK:- Collection View Data Source and Delegate

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height-10)
    }
    
    @objc func viewMoreButtonTapped(button: UIButton) {
        let moreLeagueVC = storyboard?.instantiateViewController(withIdentifier: "MoreLeagueViewController") as! MoreLeagueViewController
        AppxorEventHandler.logAppEvent(withName: "ViewMoreClicked", info: ["SportType": "Cricket"])

        if selectedFantasy == FantasyType.Classic.rawValue {
            let details = classicCategoryArray[button.tag]
            moreLeagueVC.selectedFantasy = FantasyType.Classic.rawValue
            moreLeagueVC.categoryName = details.categoryName
            moreLeagueVC.categoryID = details.categoryID
            moreLeagueVC.totalJoinedLeague = Int(totalClassicJoinedLeague) ?? 0;
            moreLeagueVC.totalTeams = totalClassicTeam
            moreLeagueVC.categoryImg = details.categoryImg
            moreLeagueVC.categoryMessage = details.categoryMessage
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            let details = battingCategoryArray[button.tag]
            moreLeagueVC.selectedFantasy = FantasyType.Batting.rawValue
            moreLeagueVC.categoryName = details.categoryName
            moreLeagueVC.categoryID = details.categoryID
            moreLeagueVC.totalJoinedLeague = Int(totalBattingJoinedLeague) ?? 0;
            moreLeagueVC.totalTeams = totalBattingTeam
            moreLeagueVC.categoryImg = details.categoryImg
            moreLeagueVC.categoryMessage = details.categoryMessage
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            let details = bowlingCategoryArray[button.tag]
            moreLeagueVC.selectedFantasy = FantasyType.Bowling.rawValue
            moreLeagueVC.categoryName = details.categoryName
            moreLeagueVC.categoryID = details.categoryID
            moreLeagueVC.totalJoinedLeague = Int(totalBowlingJoinedLeague) ?? 0;
            moreLeagueVC.totalTeams = totalBattingTeam
            moreLeagueVC.categoryImg = details.categoryImg
            moreLeagueVC.categoryMessage = details.categoryMessage
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue {
            let details = reverseCategoryArray[button.tag]
            moreLeagueVC.selectedFantasy = FantasyType.Reverse.rawValue
            moreLeagueVC.categoryName = details.categoryName
            moreLeagueVC.categoryID = details.categoryID
            moreLeagueVC.totalJoinedLeague = Int(totalReverseJoinedLeague) ?? 0;
            moreLeagueVC.totalTeams = totalReverseTeam
            moreLeagueVC.categoryImg = details.categoryImg
            moreLeagueVC.categoryMessage = details.categoryMessage
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue {
            let details = wizardCategoryArray[button.tag]
            moreLeagueVC.selectedFantasy = FantasyType.Wizard.rawValue
            moreLeagueVC.categoryName = details.categoryName
            moreLeagueVC.categoryID = details.categoryID
            moreLeagueVC.totalJoinedLeague = Int(totalWizardJoinedLeague) ?? 0;
            moreLeagueVC.totalTeams = totalWizardTeam
            moreLeagueVC.categoryImg = details.categoryImg
            moreLeagueVC.categoryMessage = details.categoryMessage
        }

        moreLeagueVC.userTicketsArray = userTicketsArray
        isNeedtoShowLoaderOncomeBack = false;

        moreLeagueVC.matchDetails = matchDetails
        navigationController?.pushViewController(moreLeagueVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueContainerCollectionViewCell", for: indexPath) as? LeagueContainerCollectionViewCell
        cell?.delegate = self
        let fantasyType = tabsArray[indexPath.row]
        
        if fantasyType == FantasyType.Classic.rawValue {
            cell!.configData(catArray: classicCategoryArray, selectedMatchDetails: matchDetails, fantasyType: FantasyType.Classic.rawValue, ticketsArray: userTicketsArray, isFilterApplied: isFilterApplied, gameType: GameType.Cricket.rawValue)
        }
        else if fantasyType == FantasyType.Batting.rawValue {
            cell!.configData(catArray: battingCategoryArray, selectedMatchDetails: matchDetails, fantasyType: FantasyType.Batting.rawValue, ticketsArray: userTicketsArray, isFilterApplied: isFilterApplied, gameType: GameType.Cricket.rawValue)
        }
        else if fantasyType == FantasyType.Bowling.rawValue {
            cell!.configData(catArray: bowlingCategoryArray, selectedMatchDetails: matchDetails, fantasyType: FantasyType.Bowling.rawValue, ticketsArray: userTicketsArray, isFilterApplied: isFilterApplied, gameType: GameType.Cricket.rawValue)
        }
        else if fantasyType == FantasyType.Reverse.rawValue {
            cell!.configData(catArray: reverseCategoryArray, selectedMatchDetails: matchDetails, fantasyType: FantasyType.Reverse.rawValue, ticketsArray: userTicketsArray, isFilterApplied: isFilterApplied, gameType: GameType.Cricket.rawValue)
        }
        else if fantasyType == FantasyType.Wizard.rawValue {
            cell!.configData(catArray: wizardCategoryArray, selectedMatchDetails: matchDetails, fantasyType: FantasyType.Wizard.rawValue, ticketsArray: userTicketsArray, isFilterApplied: isFilterApplied, gameType: GameType.Cricket.rawValue)
        }
        else{
            cell!.configData(catArray: Array<LeagueCategoryDetails>() , selectedMatchDetails: matchDetails, fantasyType: FantasyType.None.rawValue, ticketsArray: userTicketsArray, isFilterApplied: isFilterApplied, gameType: GameType.Cricket.rawValue)
        }

        return cell!;
    }

    
    @objc func joinLeagueButtonTapped(button: SolidButton)  {

        var containerCell: LeagueContainerCollectionViewCell!
        if selectedFantasy == FantasyType.Classic.rawValue {
            guard let classicLeague = leagueCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? LeagueContainerCollectionViewCell else{
                return;
            }
            containerCell = classicLeague
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            guard let battingLeague = leagueCollectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? LeagueContainerCollectionViewCell else{
                return;
            }
            containerCell = battingLeague
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            guard let bowlingLeague = leagueCollectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? LeagueContainerCollectionViewCell else{
                return;
            }
            containerCell = bowlingLeague
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue {
            guard let reverseLeague = leagueCollectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? LeagueContainerCollectionViewCell else{
                return;
            }
            containerCell = reverseLeague
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue {
            guard let wizardLeague = leagueCollectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? LeagueContainerCollectionViewCell else{
                return;
            }
            containerCell = wizardLeague
        }

        var leagueCell: LeagueCollectionViewCell?
        var singleCell: SingleLeagueCollectionViewCell?

        if let cell = button.superview?.superview?.superview?.superview?.superview as? LeagueCollectionViewCell {
            leagueCell = cell
        }
        
        if let cell = button.superview?.superview?.superview?.superview?.superview as? SingleLeagueCollectionViewCell {
            singleCell = cell
        }

        var cellIndexPath: IndexPath!

        if leagueCell != nil{
            guard let indexPath = containerCell.leagueCollectionView.indexPath(for: leagueCell!) else{
                return;
            }
            cellIndexPath = indexPath
        }
        
        if singleCell != nil{
            guard let indexPath = containerCell.leagueCollectionView.indexPath(for: singleCell!) else{
                return;
            }
            cellIndexPath = indexPath
        }

        var categoryDetails: LeagueCategoryDetails!
        if selectedFantasy == FantasyType.Classic.rawValue {
            categoryDetails = classicCategoryArray[cellIndexPath.section]
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            categoryDetails = battingCategoryArray[cellIndexPath.section]
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            categoryDetails = bowlingCategoryArray[cellIndexPath.section]
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue {
            categoryDetails = reverseCategoryArray[cellIndexPath.section]
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue {
            categoryDetails = wizardCategoryArray[cellIndexPath.section]
        }

        let leagueDetails = categoryDetails.leaguesArray[cellIndexPath.row]

        if (leagueDetails.teamType == "1") {
            var maxTeamAllow = 0;
            
            if leagueDetails.fantasyType == "1"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForClassic;
            }
            else if leagueDetails.fantasyType == "2"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBatting;
            }
            else if leagueDetails.fantasyType == "3"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBowling;
            }
            else if leagueDetails.fantasyType == "4"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForReverse;
            }
            else if leagueDetails.fantasyType == "5"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForWizard;
            }

            if leagueDetails.joinedLeagueCount == maxTeamAllow{
                return;
            }
        }
        else if (leagueDetails.teamType == "1") {
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.confirmedLeague == "2"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.teamType == "2"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.teamType == "3"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        
        if isUserValidatingToJoinLeague {
            return;
        }
        
        var fantasyType = ""
        if leagueDetails.fantasyType == "1"{
            fantasyType = "Classic";
        }
        else if leagueDetails.fantasyType == "2"{
            fantasyType = "Batting";
        }
        else if leagueDetails.fantasyType == "3"{
            fantasyType = "Bowling";
        }
        else if leagueDetails.fantasyType == "4"{
            fantasyType = "Reverse";
        }
        else if leagueDetails.fantasyType == "5"{
            fantasyType = "Wizard";
        }

        AppxorEventHandler.logAppEvent(withName: "JoinNowButtonClicked", info: ["ContestType": categoryDetails.categoryName, "ContestID": leagueDetails.leagueId, "Type": fantasyType, "SportType": "Cricket"])

        callLeagueValidationAPI(leagueDetails: leagueDetails, categoryName: categoryDetails.categoryName)
    }
    
    // MARK:- -IBAction Methods
    
    @IBAction func myLeagueButtonTapped(_ sender: Any) {
        isNeedtoShowLoaderOncomeBack = false;
        performSegue(withIdentifier: "MyTeamsViewController", sender: nil)
        
        AppxorEventHandler.logAppEvent(withName: "TeamTabClicked", info: ["SportType": "Cricket"])

    }
    
    @IBAction func createPrivateLeagueButtonTapped(_ sender: Any) {
        let createPrivateLeague = storyboard?.instantiateViewController(withIdentifier: "CreatePrivateLeagueViewController") as! CreatePrivateLeagueViewController
        createPrivateLeague.matchDetails = matchDetails
        
        if selectedFantasy == FantasyType.Classic.rawValue{
            let userTeamsArray =  UserDetails.sharedInstance.userTeamsArray.filter { (details) -> Bool in
                return details.fantasyType == "1"
            }
            createPrivateLeague.userTeamsArray = userTeamsArray
            createPrivateLeague.fantasyType = "1"
        }
        else if selectedFantasy == FantasyType.Batting.rawValue{
            createPrivateLeague.fantasyType = "2"
            let userTeamsArray =  UserDetails.sharedInstance.userTeamsArray.filter { (details) -> Bool in
                return details.fantasyType == "2"
            }
            createPrivateLeague.userTeamsArray = userTeamsArray
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue{
            createPrivateLeague.fantasyType = "3"
            let userTeamsArray =  UserDetails.sharedInstance.userTeamsArray.filter { (details) -> Bool in
                return details.fantasyType == "3"
            }
            createPrivateLeague.userTeamsArray = userTeamsArray
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue{
            createPrivateLeague.fantasyType = "4"
            let userTeamsArray =  UserDetails.sharedInstance.userTeamsArray.filter { (details) -> Bool in
                return details.fantasyType == "4"
            }
            createPrivateLeague.userTeamsArray = userTeamsArray
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue{
            createPrivateLeague.fantasyType = "5"
            let userTeamsArray =  UserDetails.sharedInstance.userTeamsArray.filter { (details) -> Bool in
                return details.fantasyType == "5"
            }
            createPrivateLeague.userTeamsArray = userTeamsArray
        }
        createPrivateLeague.gameType = GameType.Cricket.rawValue

        navigationController?.pushViewController(createPrivateLeague, animated: true)
    }
    
    @IBAction func filterButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "FilterClicked", info: ["SportType": "Cricket"])

        let filterVC = storyboard?.instantiateViewController(withIdentifier: "LeagueFilterViewController") as! LeagueFilterViewController
        filterVC.delegate = self
        filterVC.showSelectedLeagueFilter(rangArray: entryRangeArray, poolArray: poolRangeArray, teamsArray: teamsfilterArray, leaguesArray: allleagueTypeArray)
        navigationController?.pushViewController(filterVC, animated: true)
    }
    
    @IBAction func joinedLeagueButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "LeagueTabClicked", info: ["SportType": "Cricket"])

        isNeedtoShowLoaderOncomeBack = false;
        performSegue(withIdentifier: "JoinedLeagueViewController", sender: nil)
    }

    func classicButtonTapped(isAnimation: Bool) {

        showDataForClassic()

        var indexPathCount = 0

        if selectedFantasy == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasy == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasy == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasy == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasy == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)
        weak var weakSelf = self
        DispatchQueue.main.async {
            if isAnimation{
                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
            }
            else{
                weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
            }
            weakSelf?.leagueCollectionView.reloadData()
        }
    }
    
    func battingButtonTapped(isAnimation: Bool) {
        showDataForBatting()

        var indexPathCount = 0

        if selectedFantasy == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasy == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasy == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasy == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasy == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)
        weak var weakSelf = self

        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    func bowlingButtonTapped(isAnimation: Bool) {
        showDataForBowling();

        var indexPathCount = 0

        if selectedFantasy == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasy == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasy == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasy == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasy == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)

        weak var weakSelf = self
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    func reverseButtonTapped(isAnimation: Bool) {
        showDataForReverse();

        var indexPathCount = 0

        if selectedFantasy == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasy == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasy == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasy == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasy == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    func wizardButtonTapped(isAnimation: Bool) {
        showDataForWizard();
        var indexPathCount = 0

        if selectedFantasy == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasy == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasy == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasy == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasy == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)

        //        headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                if isAnimation{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                else{
                    weakSelf?.leagueCollectionView?.scrollToItem(at: indexPath, at: .right, animated: false)
                }
                weakSelf?.leagueCollectionView.reloadData()
            }
        }
    }
    
    func showDataForClassic() {
        if let totalClassic = UserDefaults.standard.value(forKey: "total_classic") as? String{
            if Int(totalClassic) ?? 0 <= 0{
                if let classicBanner = UserDefaults.standard.value(forKey: "classicBanner") as? Date{
                    let days = getDayOfWeek(classicBanner)
                    if days >= 1{
                        showClassicTutorial()
                    }
                }
                else{
                    showClassicTutorial()
                }
            }
        }
        selectedFantasy = FantasyType.Classic.rawValue
        lblMyTeam.text = String(totalClassicTeam)
        lblJoinLeagues.text = String(totalClassicJoinedLeague)
        
        if totalClassicTeam > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalClassicJoinedLeague)! > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
    }

    func showDataForBatting() {
        
        if let totalBatting = UserDefaults.standard.value(forKey: "total_batting") as? String{
            if Int(totalBatting) ?? 0 <= 0{
                if let battingBanner = UserDefaults.standard.value(forKey: "battingBanner") as? Date{
                    let days = getDayOfWeek(battingBanner)
                    if days >= 1{
                        showBattingTutorial()
                    }
                }
                else{
                    showBattingTutorial()
                }
            }
        }
        
        selectedFantasy = FantasyType.Batting.rawValue
        lblMyTeam.text = String(totalBattingTeam)
        lblJoinLeagues.text = String(totalBattingJoinedLeague)
        
        if totalBattingTeam > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalBattingJoinedLeague)! > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
    }
    
    
    func showDataForBowling() {
        
        if let totalBowling = UserDefaults.standard.value(forKey: "total_bowling") as? String{
            if Int(totalBowling) ?? 0 <= 0{
                if let bowlingBanner = UserDefaults.standard.value(forKey: "bowlingBanner") as? Date{
                    let days = getDayOfWeek(bowlingBanner)
                    if days >= 1{
                        showBowlingTutorial()
                    }
                }
                else{
                    showBowlingTutorial()
                }
            }
        }
        
        selectedFantasy = FantasyType.Bowling.rawValue
        lblMyTeam.text = String(totalBowlingTeam)
        lblJoinLeagues.text = String(totalBowlingJoinedLeague)
        
        if totalBowlingTeam > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalBowlingJoinedLeague)! > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
        
    }
    
    func showDataForReverse() {

        if let totalReverse = UserDefaults.standard.value(forKey: "total_reverse") as? String{
            if Int(totalReverse) ?? 0 <= 0{
                if let bowlingBanner = UserDefaults.standard.value(forKey: "ReverseBanner") as? Date{
                    let days = getDayOfWeek(bowlingBanner)
                    if days >= 1{
                        showReverseTutorial()
                    }
                }
                else{
                    showReverseTutorial()
                }
            }
        }
        
        selectedFantasy = FantasyType.Reverse.rawValue
        lblMyTeam.text = String(totalReverseTeam)
        lblJoinLeagues.text = String(totalReverseJoinedLeague)
        
        if totalReverseTeam > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalReverseJoinedLeague)! > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
    }
    
    func showDataForWizard() {

        if let totalBowling = UserDefaults.standard.value(forKey: "total_wizard") as? String{
            if Int(totalBowling) ?? 0 <= 0{
                if let bowlingBanner = UserDefaults.standard.value(forKey: "WizardBanner") as? Date{
                    let days = getDayOfWeek(bowlingBanner)
                    if days >= 1{
                        showWizardTutorial()
                    }
                }
                else{
                    showWizardTutorial()
                }
            }
        }

        selectedFantasy = FantasyType.Wizard.rawValue
        lblMyTeam.text = String(totalWizardTeam)
        lblJoinLeagues.text = String(totalWizardJoinedLeague)
        
        if totalWizardTeam > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalWizardJoinedLeague)! > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
        
    }
    // MARK:- API Related Method
    
    func callGetLeagueAPI(urlString: String, isNeedToShowLoader: Bool)  {

        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                isPullToRefresh = false
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            weakSelf?.isPullToRefresh = false
            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
                    DispatchQueue.main.async {
                        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
                        let templateID = weakSelf?.matchDetails?.templateID
                        let isTicketAvailable = weakSelf?.matchDetails?.isTicketAvailable ?? false

                        weakSelf?.matchDetails = MatchDetails.getSelectedMatchDetails(responseResult: savedResponse!)
                        weakSelf?.matchDetails?.templateID = templateID ?? ""
                        weakSelf?.updateTabsData()
                        weakSelf?.matchDetails?.isTicketAvailable = isTicketAvailable;
                        weakSelf?.leagueArray = LeagueDetails.getAllLeagueDetails(responseResult: savedResponse!, matchDetails: weakSelf!.matchDetails!)
                        
                        if self.leagueArray.count != 0 {
                            self.filterView.isHidden = false
                        }

                        (weakSelf!.totalClassicJoinedLeague, weakSelf!.totalBattingJoinedLeague, weakSelf!.totalBowlingJoinedLeague, weakSelf!.totalReverseJoinedLeague, weakSelf!.totalWizardJoinedLeague) = LeagueDetails.getJoinedLeagueCounts(responseResult: savedResponse!)
                        if let response = savedResponse!["response"].dictionary{
                            var recomandedArray = Array<JSON>()
                            let leagueEecommendationString = response["league_recommendation"]?.string ?? ""
                            if leagueEecommendationString.count > 0 {
                                let json = JSON(parseJSON: leagueEecommendationString)
                                if (json.array != nil){
                                    recomandedArray = json.array!
                                }
                            }

                            if let announcement = response["announcement"]?.dictionary{
                                let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                                
                                weakSelf?.announcementViewHeightConstraint.constant = 50.0
                                weakSelf?.view.layoutIfNeeded()
                                weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                            }
                            else{
                                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                                weakSelf?.view.layoutIfNeeded()
                                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                            }

                            if  response["categorisation"]?.dictionary != nil{
                                if weakSelf!.isFilterApplied{
                                    (weakSelf!.classicCategoryArray, weakSelf!.battingCategoryArray, weakSelf!.bowlingCategoryArray, weakSelf!.reverseCategoryArray, weakSelf!.wizardCategoryArray) = LeagueCategoryDetails.parseCategoryDetailsWithFilter(categoryDetails: response["categorisation"]!, leagueArray: weakSelf!.leagueArray, recomnadedLeaguesArray: recomandedArray, rangArray: weakSelf!.entryRangeArray, poolArray: weakSelf!.poolRangeArray, teamsArray: weakSelf!.teamsfilterArray, leaguesTypeArray: weakSelf!.leagueTypeArray)
                                }
                                else{
                                    (weakSelf!.classicCategoryArray, weakSelf!.battingCategoryArray, weakSelf!.bowlingCategoryArray,weakSelf!.reverseCategoryArray, weakSelf!.wizardCategoryArray) = LeagueCategoryDetails.parseCategoryDetails(categoryDetails: response["categorisation"]!, leagueArray: weakSelf!.leagueArray, recomnadedLeaguesArray: recomandedArray)
                                }
                            }
                            
                            if let tempArray = response["active_tickets"]?.array {
                                (weakSelf!.userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                                
                                for ticketDetails in weakSelf!.userTicketsArray{
                                    for details in weakSelf!.leagueArray {
                                        if ticketDetails.ticketType != "2"{
                                            if (details.categoryId == ticketDetails.leagueCategory) && (ticketDetails.matchKey == weakSelf!.matchKey) && (ticketDetails.joiningAmount == details.joiningAmount){
                                                details.isTicketAvailable = true
                                                if ticketDetails.ticketType == "3" {
                                                    details.isPassAvailable = true
                                                }
                                            }
                                        }
                                        else{
                                            if (details.categoryId == ticketDetails.leagueCategory) && (ticketDetails.joiningAmount == details.joiningAmount){
                                                details.isTicketAvailable = true
                                            }
                                        }
                                    }
                                }
                                weakSelf!.leagueCollectionView.reloadData()
                            }
                        }

                        weakSelf!.lblMyTeam.isHidden = false
                        weakSelf!.lblJoinLeagues.isHidden = false
                        weakSelf!.updateLeaguesArray()
                        if weakSelf!.leagueArray.count == 0{
                            weakSelf!.leagueCollectionView.isHidden = true
                            weakSelf!.bottomView.isHidden = true
                        }
                        else{
                            weakSelf!.leagueCollectionView.isHidden = false
                            weakSelf!.bottomView.isHidden = false
                        }
                        
                        DispatchQueue.main.async {
                            weakSelf!.headerView.updateMatchName(matchDetails: weakSelf!.matchDetails)
                            weakSelf!.leagueCollectionView.reloadData()
                        }
                        
                        if weakSelf!.selectedFantasy == FantasyType.Classic.rawValue{
                            weakSelf!.headerView.classicButtonTapped(isNeedToScroll: false)
                        }
                        else if weakSelf!.selectedFantasy == FantasyType.Batting.rawValue{
                            weakSelf!.headerView.battingButtonTapped(isNeedToScroll: false)
                        }
                        else if weakSelf!.selectedFantasy == FantasyType.Bowling.rawValue{
                            weakSelf!.headerView.bowlingButtonTapped(isNeedToScroll: false)
                        }
                        else if weakSelf!.selectedFantasy == FantasyType.Reverse.rawValue{
                            weakSelf!.headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
                        }
                        else if weakSelf!.selectedFantasy == FantasyType.Wizard.rawValue{
                            weakSelf!.headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
                        }

                        AppHelper.sharedInstance.removeSpinner()
                    }
                }
                else{
                    AppHelper.sharedInstance.removeSpinner()
                }
            }
            else{
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
    
    func callMyTicketsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        let params = ["option": "user_tickets", "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200"{
                    
                    if let response = result!["response"]?.dictionary{
                        if let tempArray = response["active_tickets"]?.array {
                            (weakSelf!.userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                            
                            for ticketDetails in weakSelf!.userTicketsArray{
                                for details in weakSelf!.leagueArray {
                                    if ticketDetails.ticketType == "2"{
                                        if (details.categoryId == ticketDetails.leagueCategory) && (ticketDetails.matchKey == weakSelf!.matchKey) && (ticketDetails.joiningAmount == details.joiningAmount){
                                            details.isTicketAvailable = true
                                        }
                                    }
                                    else{
                                        if (details.templateID == ticketDetails.leagueCategory) && (ticketDetails.matchKey == weakSelf!.matchKey){
                                            details.isTicketAvailable = true
                                        }
                                    }
                                }
                            }
                            weakSelf!.leagueCollectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails, categoryName: String)  {
       
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        isUserValidatingToJoinLeague = true
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "league_prev_data_v2","check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            weakSelf?.isUserValidatingToJoinLeague = false
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                                        
//                                        let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = weakSelf!.matchDetails
//                                        addCashVC?.userTeamArray = teamArray
//                                        addCashVC?.categoryName = categoryName
//                                        weakSelf!.isNeedtoShowLoaderOncomeBack = false;
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    weakSelf!.isNeedtoShowLoaderOncomeBack = false;
                                    let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                    playerVC.matchDetails = weakSelf?.matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            weakSelf?.isNeedtoShowLoaderOncomeBack = false;
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails, userTeamArray: teamArray, categoryName: categoryName, ticketDetails: ticketDetais)
                            }
                            else{
                                weakSelf?.isNeedtoShowLoaderOncomeBack = false;
                                let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                playerVC.matchDetails = weakSelf?.matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                
                            }
                        }
                        else{
                            weakSelf?.isNeedtoShowLoaderOncomeBack = false;
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, categoryName: String, ticketDetails: TicketDetails? )  {
        isNeedtoShowLoaderOncomeBack = false;
        let joinedLeagueConfirmVC = storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
        joinedLeagueConfirmVC?.leagueDetails = leagueDetails
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails
        joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
        joinedLeagueConfirmVC?.matchDetails = matchDetails
        joinedLeagueConfirmVC?.leagueCategoryName = categoryName
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails
        navigationController?.pushViewController(joinedLeagueConfirmVC!, animated: true)
    }
    
    // MARK:- Timer Handlers
    @objc func updateTimerValue()  {

        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
        leagueCollectionView.reloadData()
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                            AppHelper.showMatchCLosedAlertAndPopToRootViewController()
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
        
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SelectPlayerViewController" {
            
            let playerVC = segue.destination as! SelectPlayerViewController
            playerVC.matchDetails = matchDetails
            playerVC.leagueDetails = (sender as! LeagueDetails)
        }
        else if segue.identifier == "LeagueWinnersRankPriceViewController" {
            
            let leagueWinnerRankVC = segue.destination as! LeagueWinnersRankPriceViewController
            leagueWinnerRankVC.matchDetails = matchDetails
            let leagueDetails = (sender as! LeagueDetails)
            
            leagueWinnerRankVC.leagueID = leagueDetails.leagueId
            leagueWinnerRankVC.leagueWinningAmount = leagueDetails.winAmount!
        }
        else if segue.identifier == "JoinedLeagueViewController" {
            
            let joinedWinnerRankVC = segue.destination as! JoinedLeagueViewController
            joinedWinnerRankVC.matchDetails = matchDetails
            if selectedFantasy == FantasyType.Classic.rawValue {
                joinedWinnerRankVC.selectedFantasyType = FantasyType.Classic.rawValue
            }
            else if selectedFantasy == FantasyType.Batting.rawValue {
                joinedWinnerRankVC.selectedFantasyType = FantasyType.Batting.rawValue
            }
            else if selectedFantasy == FantasyType.Bowling.rawValue {
                joinedWinnerRankVC.selectedFantasyType = FantasyType.Bowling.rawValue
            }
            else if selectedFantasy == FantasyType.Reverse.rawValue {
                joinedWinnerRankVC.selectedFantasyType = FantasyType.Reverse.rawValue
            }
            else if selectedFantasy == FantasyType.Wizard.rawValue {
                joinedWinnerRankVC.selectedFantasyType = FantasyType.Wizard.rawValue
            }
        }
        else if segue.identifier == "MyTeamsViewController" {
            let myTeamVC = segue.destination as! MyTeamsViewController
            myTeamVC.matchDetails = matchDetails
            myTeamVC.userTeamsArray = UserDetails.sharedInstance.userTeamsArray
            
            if selectedFantasy == FantasyType.Classic.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Classic.rawValue
            }
            else if selectedFantasy == FantasyType.Batting.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Batting.rawValue
            }
            else if selectedFantasy == FantasyType.Bowling.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Bowling.rawValue
            }
            else if selectedFantasy == FantasyType.Reverse.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Reverse.rawValue
            }
            else if selectedFantasy == FantasyType.Wizard.rawValue {
                myTeamVC.selectedFantasyType = FantasyType.Wizard.rawValue
            }

        }
    }
    
    //MARK:- Scroll View Delegates
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
                
        if leagueCollectionView == scrollView{
            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
            if Int(currentPage) == 0{
                let fansatyType = tabsArray[0]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 1{
                let fansatyType = tabsArray[1]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 2{
                let fansatyType = tabsArray[2]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 3{
                let fansatyType = tabsArray[3]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 4{
                let fansatyType = tabsArray[4]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }
            else if Int(currentPage) == 5{
                let fansatyType = tabsArray[5]
                if selectedFantasyType == fansatyType{
                    return;
                }
                selectedFantasyType = fansatyType
            }

            updateHeaderOnscroll(gameType: selectedFantasyType)
        }
    }
    
    func updateHeaderOnscroll(gameType: Int) {
        if gameType == FantasyType.Classic.rawValue{
            headerView.classicButtonTapped(isNeedToScroll: false)
        }
        else if gameType == FantasyType.Batting.rawValue{
            headerView.battingButtonTapped(isNeedToScroll: false)
        }
        else if gameType == FantasyType.Bowling.rawValue{
            headerView.bowlingButtonTapped(isNeedToScroll: false)
        }
        else if gameType == FantasyType.Reverse.rawValue{
            headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
        }
        else if gameType == FantasyType.Wizard.rawValue{
            headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
        }
    }
    
    
    func updateTabsData() {
        headerView.matchFantasyType = matchDetails?.matchFantasyType ?? ""
        headerView.isFantasyModeEnable = false
        headerView.updateTabsData()
        (firstTabFantasyType, secondTabFantasyType, thirdTabFantasyType, fourthTabFantasyType, fifthTabFantasyType) = AppHelper.getFantasyLandingOrder(fantasyType: matchDetails?.matchFantasyType ?? "")
        
         selectedFantasyType = firstTabFantasyType
        tabsArray.removeAll()
         if firstTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(firstTabFantasyType)
         }
         
         if secondTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(secondTabFantasyType)
         }

         if thirdTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(thirdTabFantasyType)
         }
         
         if fourthTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(fourthTabFantasyType)
         }
         
         if fifthTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(fifthTabFantasyType)
         }
        
        if tabsArray.count > 1 {
            headerViewHeightConstraint.constant = 90
            headerView.layoutIfNeeded()
            headerView.isFantasyModeEnable = true
        }
        
        headerView.updateData(details: matchDetails)
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if leagueCollectionView == scrollView{
//            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
//            if Int(currentPage) == 0{
//                headerView.showClassicTabSelected()
//                showDataForClassic()
//                leagueCollectionView.reloadData()
//            }
//            else if Int(currentPage) == 1{
//                headerView.showBattingTabSelected()
//                showDataForBatting()
//                leagueCollectionView.reloadData()
//            }
//            else if Int(currentPage) == 2{
//                headerView.showBowlingTabSelected()
//                showDataForBowling()
//                leagueCollectionView.reloadData()
//            }
//        }
//
//    }

    
    func pullToRefereshLeagues() {
        if !isPullToRefresh{
            isPullToRefresh = true
            let urlString = kMatch + "?option=get_match_v1&screen_msg=1&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey + ""
            callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: true)
        }
    }

    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 

}
