//
//  MyLeaguesViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class MyLeaguesViewController: UIViewController {
    
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var annuncementView: AnnouncementView!
    @IBOutlet weak var collectionView: UICollectionView!

    private var indexOfCellBeforeDragging = 0
    
    lazy var cricketUpcomingMatchsArray = Array<MatchDetails>()
    lazy var cricketLiveMatchsArray = Array<MatchDetails>()
    lazy var cricketCompletedMatchsArray = Array<MatchDetails>()
    
    lazy var kabaddiUpcomingMatchsArray = Array<MatchDetails>()
    lazy var kabaddiLiveMatchsArray = Array<MatchDetails>()
    lazy var kabaddiCompletedMatchsArray = Array<MatchDetails>()

    lazy var footballUpcomingMatchsArray = Array<MatchDetails>()
    lazy var footballLiveMatchsArray = Array<MatchDetails>()
    lazy var footballCompletedMatchsArray = Array<MatchDetails>()
        
    lazy var basketballUpcomingMatchsArray = Array<MatchDetails>()
    lazy var basketballLiveMatchsArray = Array<MatchDetails>()
    lazy var basketballCompletedMatchsArray = Array<MatchDetails>()

    lazy var baseballUpcomingMatchsArray = Array<MatchDetails>()
    lazy var baseballLiveMatchsArray = Array<MatchDetails>()
    lazy var baseballCompletedMatchsArray = Array<MatchDetails>()

    var bannerHeight: CGFloat = 90
    var tabsArray = Array<Int>()

    var cricketUrlString = kMatch + "?option=get_home_joined_new&screen_msg=1&season_key=&user_id="
    var kabaddiUrlString = kKabaddiMatchURL + "?option=get_home_joined&screen_msg=1&season_key=&user_id="
    var footballUrlString = kFootballMatchURL + "?option=get_home_joined&screen_msg=1&season_key=&user_id="

    var basketballUrlString = kBasketballMatchURL + "?option=get_home_joined&screen_msg=1&season_key=&user_id="
    var baseballUrlString = kBaseballMatchURL + "?option=get_home_joined&screen_msg=1&season_key=&user_id="

    lazy var selectedGameType = GameType.Cricket.rawValue
    lazy var userTicketsArray = Array<TicketDetails>()
    
    var firstTabGameType = GameType.Cricket.rawValue;
    var secondTabGameType = GameType.Kabaddi.rawValue;
    var thirdTabGameType = GameType.Football.rawValue;
    var fourthTabGameType = GameType.Quiz.rawValue;
    var fifthTabGameType = GameType.Basketball.rawValue;
    var sixthTabGameType = GameType.Baseball.rawValue;

    //MARK:- View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        (firstTabGameType, secondTabGameType, thirdTabGameType, fourthTabGameType, fifthTabGameType, sixthTabGameType) = AppHelper.getLandingOrder()
        
        if firstTabGameType != GameType.None.rawValue {
            tabsArray.append(firstTabGameType)
        }
        
        if secondTabGameType != GameType.None.rawValue {
            tabsArray.append(secondTabGameType)
        }

        if thirdTabGameType != GameType.None.rawValue {
            tabsArray.append(thirdTabGameType)
        }
        
        if fourthTabGameType != GameType.None.rawValue {
            tabsArray.append(fourthTabGameType)
        }
        
        if fifthTabGameType != GameType.None.rawValue {
            tabsArray.append(fifthTabGameType)
        }
        
        if sixthTabGameType != GameType.None.rawValue {
            tabsArray.append(sixthTabGameType)
        }

        setupViewProperties()
    }
    
    func setupViewProperties() {
        
        cricketUrlString = cricketUrlString + UserDetails.sharedInstance.userID
        kabaddiUrlString = kabaddiUrlString + UserDetails.sharedInstance.userID
        footballUrlString = footballUrlString + UserDetails.sharedInstance.userID
        basketballUrlString = basketballUrlString + UserDetails.sharedInstance.userID
        baseballUrlString = baseballUrlString + UserDetails.sharedInstance.userID

        NotificationCenter.default.addObserver(self, selector: #selector(self.leagueButtonTapped), name: NSNotification.Name(rawValue: "leagueButtonTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.leagueGameTypeChangeNotification(notification:)), name: NSNotification.Name(rawValue: "leagueGameTypeChangeNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMyLeaguesOnPullToRefresh(notification:)), name: NSNotification.Name(rawValue: "updateMyLeaguesOnPullToRefresh"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.userlogoutNotitification(notification:)), name: NSNotification.Name(rawValue: "userlogoutNotitification"), object: nil)

        collectionView.register(UINib(nibName: "MyLeagueMatchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MyLeagueMatchCollectionViewCell")
        
        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
        UserDefaults.standard.set(false, forKey: "isUserOnUserName")
        UserDefaults.standard.synchronize()
    }
    
    @objc func userlogoutNotitification(notification: Notification)  {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func removeUpcomingMatchFromLocal(notification: Notification) {
        
        guard let matchDetails = notification.object as? MatchDetails else {
            return
        }
        
        if selectedGameType == GameType.Cricket.rawValue {
            cricketUpcomingMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
        }
        else if selectedGameType == GameType.Kabaddi.rawValue {
            kabaddiUpcomingMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
            
        }
        else if selectedGameType == GameType.Football.rawValue {
            footballUpcomingMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            basketballUpcomingMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            baseballUpcomingMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
        }

        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        showTableDataData(isTabClicked: false)
    }
    
    // MARK:- Custom Methods
    
    func showTableDataData(isTabClicked: Bool) {
        
        weak var weakSelf = self
        if selectedGameType == GameType.Cricket.rawValue {
            let savedResponse = AppHelper.getValueFromCoreData(urlString: cricketUrlString)
            if (savedResponse != nil) {
                let (tempLiveArray, tempUpcomingArray, tempCompletedArray) = MatchDetails.getAllMatchDetailsForLeague(responseResult: savedResponse!)
                
                if let response = savedResponse?.dictionary!["response"]{
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }
 
                cricketLiveMatchsArray = tempLiveArray
                cricketUpcomingMatchsArray = tempUpcomingArray
                cricketCompletedMatchsArray = tempCompletedArray

                DispatchQueue.global().async {
                    weakSelf?.callGetLeagueMatchesAPI(urlString: weakSelf?.cricketUrlString ?? "", isNeedToShowLoader: false)
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }

            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")

                callGetLeagueMatchesAPI(urlString: cricketUrlString, isNeedToShowLoader: true)
            }
        }
        else if selectedGameType == GameType.Kabaddi.rawValue{
            let savedResponse = AppHelper.getValueFromCoreData(urlString: kabaddiUrlString)
            if (savedResponse != nil) {
                let (tempLiveArray, tempUpcomingArray, tempCompletedArray) = MatchDetails.getAllMatchDetailsForLeague(responseResult: savedResponse!)
                
                if let response = savedResponse?.dictionary!["response"]{
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }
  
                kabaddiLiveMatchsArray = tempLiveArray
                kabaddiUpcomingMatchsArray = tempUpcomingArray
                kabaddiCompletedMatchsArray = tempCompletedArray
                
                DispatchQueue.global().async {
                    weakSelf?.callGetLeagueMatchesAPI(urlString: weakSelf?.kabaddiUrlString ?? "", isNeedToShowLoader: false)
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }

            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")

                callGetLeagueMatchesAPI(urlString: kabaddiUrlString, isNeedToShowLoader: true)
            }
        }
        else if selectedGameType == GameType.Football.rawValue{
            let savedResponse = AppHelper.getValueFromCoreData(urlString: footballUrlString)
            if (savedResponse != nil) {
                let (tempLiveArray, tempUpcomingArray, tempCompletedArray) = MatchDetails.getAllMatchDetailsForLeague(responseResult: savedResponse!)
                
                if let response = savedResponse?.dictionary!["response"]{
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }
                
                footballLiveMatchsArray = tempLiveArray
                footballUpcomingMatchsArray = tempUpcomingArray
                footballCompletedMatchsArray = tempCompletedArray

                DispatchQueue.global().async {
                    weakSelf?.callGetLeagueMatchesAPI(urlString: weakSelf?.footballUrlString ?? "", isNeedToShowLoader: false)
                }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }

            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")

                callGetLeagueMatchesAPI(urlString: footballUrlString, isNeedToShowLoader: true)
            }
        }
        else if selectedGameType == GameType.Basketball.rawValue{
            let savedResponse = AppHelper.getValueFromCoreData(urlString: basketballUrlString)
            if (savedResponse != nil) {
                let (tempLiveArray, tempUpcomingArray, tempCompletedArray) = MatchDetails.getAllMatchDetailsForLeague(responseResult: savedResponse!)
                
                if let response = savedResponse?.dictionary!["response"]{
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }
                
                basketballLiveMatchsArray = tempLiveArray
                basketballUpcomingMatchsArray = tempUpcomingArray
                basketballCompletedMatchsArray = tempCompletedArray
                
                DispatchQueue.global().async {
                    weakSelf?.callGetLeagueMatchesAPI(urlString: weakSelf?.basketballUrlString ?? "", isNeedToShowLoader: false)
                }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }

            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                callGetLeagueMatchesAPI(urlString: basketballUrlString, isNeedToShowLoader: true)
            }
        }
        else if selectedGameType == GameType.Baseball.rawValue{
            let savedResponse = AppHelper.getValueFromCoreData(urlString: baseballUrlString)
            if (savedResponse != nil) {
                let (tempLiveArray, tempUpcomingArray, tempCompletedArray) = MatchDetails.getAllMatchDetailsForLeague(responseResult: savedResponse!)
                
                if let response = savedResponse?.dictionary!["response"]{
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }
                
                baseballLiveMatchsArray = tempLiveArray
                baseballUpcomingMatchsArray = tempUpcomingArray
                baseballCompletedMatchsArray = tempCompletedArray

                DispatchQueue.global().async {
                    weakSelf?.callGetLeagueMatchesAPI(urlString: weakSelf?.baseballUrlString ?? "", isNeedToShowLoader: false)
                }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                callGetLeagueMatchesAPI(urlString: baseballUrlString, isNeedToShowLoader: true)
            }
        }
        
    }
    
    @objc func leagueButtonTapped(notification: Notification)  {
        selectedGameType =  notification.object as! Int
        showTableDataData(isTabClicked: true)
        if selectedGameType == firstTabGameType{
            let indexpath = IndexPath(item: 0, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == secondTabGameType{
            let indexpath = IndexPath(item: 1, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == thirdTabGameType{
            let indexpath = IndexPath(item: 2, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == fourthTabGameType{
            let indexpath = IndexPath(item: 3, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == fifthTabGameType{
            let indexpath = IndexPath(item: 4, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == sixthTabGameType{
            let indexpath = IndexPath(item: 5, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }

        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    @objc func updateMyLeaguesOnPullToRefresh(notification: Notification)  {
        if selectedGameType == GameType.Cricket.rawValue {
            callGetLeagueMatchesAPI(urlString: cricketUrlString, isNeedToShowLoader: true)
        }
        else if selectedGameType == GameType.Kabaddi.rawValue {
            callGetLeagueMatchesAPI(urlString: kabaddiUrlString, isNeedToShowLoader: true)
        }
        else if selectedGameType == GameType.Football.rawValue {
            callGetLeagueMatchesAPI(urlString: footballUrlString, isNeedToShowLoader: true)
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            callGetLeagueMatchesAPI(urlString: basketballUrlString, isNeedToShowLoader: true)
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            callGetLeagueMatchesAPI(urlString: baseballUrlString, isNeedToShowLoader: true)
        }

    }
    @objc func leagueGameTypeChangeNotification(notification: Notification)  {
        selectedGameType =  notification.object as! Int
        showTableDataData(isTabClicked: false)
        if selectedGameType == firstTabGameType{
            let indexpath = IndexPath(item: 0, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == secondTabGameType{
            let indexpath = IndexPath(item: 1, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == thirdTabGameType{
            let indexpath = IndexPath(item: 2, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == fourthTabGameType{
            let indexpath = IndexPath(item: 3, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == fifthTabGameType{
            let indexpath = IndexPath(item: 4, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == sixthTabGameType{
            let indexpath = IndexPath(item: 5, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }

        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    
    func callGetLeagueMatchesAPI(urlString: String, isNeedToShowLoader: Bool)  {
        if urlString.count == 0 {
            return;
        }
        
        weak var weakSelf = self
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
                    DispatchQueue.main.async {
                        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
                        let (tempLiveArray, tempUpcomingArray, tempCompletedArray) = MatchDetails.getAllMatchDetailsForLeague(responseResult: savedResponse!)
                        
                        if let response = savedResponse?.dictionary!["response"]{
                            if let announcement = response["announcement"].dictionary{
                                weakSelf?.announcementViewHeightConstraint.constant = 50.0
                                weakSelf?.view.layoutIfNeeded()
                                let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                                weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                            }
                            else{
                                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                                weakSelf?.view.layoutIfNeeded()
                                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                            }
                        }
                                                
                        if weakSelf?.cricketUrlString == urlString{
                            weakSelf?.cricketLiveMatchsArray = tempLiveArray
                            weakSelf?.cricketUpcomingMatchsArray = tempUpcomingArray
                            weakSelf?.cricketCompletedMatchsArray = tempCompletedArray
                        }
                        else if weakSelf?.kabaddiUrlString == urlString{
                            weakSelf?.kabaddiLiveMatchsArray = tempLiveArray
                            weakSelf?.kabaddiUpcomingMatchsArray = tempUpcomingArray
                            weakSelf?.kabaddiCompletedMatchsArray = tempCompletedArray
                        }
                        else if weakSelf?.footballUrlString == urlString{
                            weakSelf?.footballLiveMatchsArray = tempLiveArray
                            weakSelf?.footballUpcomingMatchsArray = tempUpcomingArray
                            weakSelf?.footballCompletedMatchsArray = tempCompletedArray
                        }
                        else if weakSelf?.basketballUrlString == urlString{
                            weakSelf?.basketballLiveMatchsArray = tempLiveArray
                            weakSelf?.basketballUpcomingMatchsArray = tempUpcomingArray
                            weakSelf?.basketballCompletedMatchsArray = tempCompletedArray
                        }
                        else if weakSelf?.baseballUrlString == urlString{
                            weakSelf?.baseballLiveMatchsArray = tempLiveArray
                            weakSelf?.baseballUpcomingMatchsArray = tempUpcomingArray
                            weakSelf?.baseballCompletedMatchsArray = tempCompletedArray
                        }
                        weakSelf?.collectionView.reloadData()
                    }
                }
            }
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
//    func callGetBannerDetailsAPI() {
//        if !AppHelper.isInterNetConnectionAvailable(){
//            return;
//        }
//
//        var isNewSignup = "0"
//        if APPDELEGATE.isNewUserRegister{
//            isNewSignup = "1"
//        }
//
//        let params = ["option": "promo_banners","signup": isNewSignup]
//        WebServiceHandler.performPOSTRequest(urlString: kPmomotionUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
//
//            if result != nil{
//                let statusCode = result!["status"]
//                if statusCode == "200"{
//                    if let response = result!["response"]?.dictionary{
//                        if let dataArray = response["banners"]?.array{
//                            let bannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
//                            if bannerArray.count > 0{
//                                DispatchQueue.main.async {
//                                    if !APPDELEGATE.isNewUserRegister{
//                                        let timestamp = NSDate().timeIntervalSince1970
//                                        APPDELEGATE.isNewUserRegister = false
//                                        UserDefaults.standard.set(timestamp, forKey: "bannerTimestamp")
//                                        UserDefaults.standard.synchronize()
//                                    }
//                                    let bannerView = BannerView(frame: APPDELEGATE.window!.frame)
//                                    bannerView.bannerDetails = bannerArray[0]
//
//                                    if let filePathDict = result!["file_path"]?.dictionary{
//                                        bannerView.filePath = filePathDict["promotion_images"]?.string ?? ""
//                                    }
//
//                                    bannerView.updateData()
//                                    APPDELEGATE.window?.addSubview(bannerView)
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
 
    // MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let dashboardVC = self.navigationController?.viewControllers.first as? DashboardViewController{
            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
            if Int(currentPage) == 0{
                let gameType = tabsArray[0]
                
                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 1{
                let gameType = tabsArray[1]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 2{
                let gameType = tabsArray[2]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 3{
                let gameType = tabsArray[3]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 4{
                let gameType = tabsArray[4]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 5{
                let gameType = tabsArray[5]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
//            else if Int(currentPage) == 6{
//                let gameType = tabsArray[6]
//
//                if selectedGameType == gameType{
//                    return;
//                }
//                selectedGameType = gameType
//            }

            dashboardVC.updateHeaderOnscroll(gameType: selectedGameType)
        }
    }
}


extension MyLeaguesViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyLeagueMatchCollectionViewCell", for: indexPath) as! MyLeagueMatchCollectionViewCell
        
        let gameType = tabsArray[indexPath.row]
        
        if gameType == GameType.Cricket.rawValue {
            cell.configData(liveArray: cricketLiveMatchsArray, upcomingArray: cricketUpcomingMatchsArray, completedArray: cricketCompletedMatchsArray, gameType: GameType.Cricket.rawValue)
        }
        else if gameType == GameType.Kabaddi.rawValue {
            cell.configData(liveArray: kabaddiLiveMatchsArray, upcomingArray: kabaddiUpcomingMatchsArray, completedArray: kabaddiCompletedMatchsArray, gameType: GameType.Kabaddi.rawValue)
        }
        else if gameType == GameType.Football.rawValue {
            cell.configData(liveArray: footballLiveMatchsArray, upcomingArray: footballUpcomingMatchsArray, completedArray: footballCompletedMatchsArray, gameType: GameType.Football.rawValue)
        }
        else if gameType == GameType.Quiz.rawValue {
            cell.configData(liveArray:nil, upcomingArray: nil, completedArray: nil, gameType: GameType.Quiz.rawValue)
        }
        else if gameType == GameType.Basketball.rawValue {
            cell.configData(liveArray: basketballLiveMatchsArray, upcomingArray: basketballUpcomingMatchsArray, completedArray: basketballCompletedMatchsArray, gameType: GameType.Basketball.rawValue)
        }
        else if gameType == GameType.Baseball.rawValue {
            cell.configData(liveArray: baseballLiveMatchsArray, upcomingArray: baseballUpcomingMatchsArray, completedArray: baseballCompletedMatchsArray, gameType: GameType.Baseball.rawValue)
        }

        else if gameType == GameType.None.rawValue {
            cell.configData(liveArray:nil, upcomingArray: nil, completedArray: nil, gameType: selectedGameType)
        }
        else{
            cell.configData(liveArray:nil, upcomingArray: nil, completedArray: nil, gameType: selectedGameType)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
