//
//  QuizPreviewViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 30/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class QuizPreviewViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, QuizConfirmationViewDelegate {
    
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblEntry: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var ticketIcon: UIImageView!
    @IBOutlet weak var joinedViewWidthConstaint: NSLayoutConstraint!
    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var headerView: LetspickFantasyHeaderView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var joinButtonBackgroundView: UIView!
    @IBOutlet weak var lblEntryFee: UILabel!
    @IBOutlet var lblJoinedText: UILabel!
    @IBOutlet weak var lblTicketAvailable: UILabel!
    var isCoinsAvailable = false

    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    lazy var winnersArray = Array<LeagueWinnersRank>()
    lazy var bannerArray = Array<String>()

    var timer: Timer?
    lazy var isUserValidatingToJoinLeague = false
    var categoryName = ""
    lazy var isMatchClosingTimeRefereshing = false
    var userTicketsArray = Array<TicketDetails>()

    lazy var isViewForPrivateLeague = false
    var userTeamsArray = Array<UserTeamDetails>()
    var isAutoShowPricebreakupPopup = false

    //MARK: View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppxorEventHandler.logAppEvent(withName: "ContestClicked", info: nil)
        setupDefaultProperties()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        if AppHelper.isApplicationRunningOnIphoneX() {
            bottomViewHeightConstraint.constant = 85.0
        }
        
        if isAutoShowPricebreakupPopup {
            isAutoShowPricebreakupPopup = false
            let confirmationVC = QuizConfirmationView(frame: APPDELEGATE.window!.frame)
            confirmationVC.matchDetails = matchDetails
            confirmationVC.delegate = self
            confirmationVC.updateData(details: leagueDetails!)
            APPDELEGATE.window?.addSubview(confirmationVC)
        }

    }
    
    func updateAutoPopupStatus() {
        isAutoShowPricebreakupPopup = true
    }

    // MARK:- Custom Methods
    private func setupDefaultProperties() {
        navigationController?.navigationBar.isHidden = true
        lblJoinedText.text = "Play".localized()

        headerView.isFantasyModeEnable = false
        headerView.updateData(details: matchDetails)

        lblEntry.text = "Entry".localized()
        AppHelper.designBottomTabDesing(bottomView)
        lblEntryFee.text = "pts" + AppHelper.makeCommaSeparatedDigitsWithString(digites: leagueDetails!.joiningAmount)
        collectionView.register(UINib(nibName: "LeagueWinneraListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueWinneraListCollectionViewCell")
        collectionView.register(UINib(nibName: "QuizPreviewDetailsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "QuizPreviewDetailsCollectionViewCell")
        collectionView.register(UINib(nibName: "JackpotBannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JackpotBannerCollectionViewCell")

        callTotalWinnersRankAPI()
        changeShadowAndBackgroundButtonView(color: UIColor(red: 56.0/255, green: 154.0/255, blue: 254.0/255, alpha: 1));
        if !isViewForPrivateLeague{
            userTeamsArray = UserDetails.sharedInstance.userTeamsArray
        }

        if leagueDetails != nil {
            bannerArray = leagueDetails!.bannerImages
        }
    }
    
    func changeShadowAndBackgroundButtonView(color: UIColor)  {
        joinButtonBackgroundView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        joinButtonBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 3)
        joinButtonBackgroundView.layer.shadowOpacity = 1.0
        joinButtonBackgroundView.layer.shadowRadius = 10.0
        joinButtonBackgroundView.layer.masksToBounds = false
        joinButtonBackgroundView.layer.cornerRadius = 5.0;
        joinButtonBackgroundView.backgroundColor = color
    }

    //MARK:- -IBAction Methods
    @IBAction func joinButtonTapped(_ sender: Any) {
        
        if isUserValidatingToJoinLeague {
            return;
        }
                
        let confirmationVC = QuizConfirmationView(frame: APPDELEGATE.window!.frame)
        confirmationVC.matchDetails = matchDetails
        confirmationVC.updateData(details: leagueDetails!)
        confirmationVC.delegate = self
        APPDELEGATE.window?.addSubview(confirmationVC)
    }
       
    // MARK:- API Related Method
    
    func callTotalWinnersRankAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
       
        let urlString = BASEURLKABADDI + "quiz/match?option=get_league_winners&user_id=" + UserDetails.sharedInstance.userID + "&league_id=" + leagueDetails!.leagueId

        AppHelper.sharedInstance.displaySpinner()
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                
                if statusCode == "200"{

                    weakSelf?.winnersArray = LeagueWinnersRank.getAllRankDetails(response: result!.dictionary!)
                    
                    if weakSelf!.leagueDetails?.isInfinity == "1"{
                        if weakSelf!.leagueDetails?.leagueWinnerType == "dynamic_winner" {
                            let leagueRank = LeagueWinnersRank()
                            leagueRank.rankCount = "Top " + weakSelf!.leagueDetails!.totalWinnersPercent + "%"
                            leagueRank.winAmount = weakSelf!.leagueDetails!.winPerUser
                            weakSelf!.winnersArray.append(leagueRank)
                        }
                    }
                    else{
                        if weakSelf?.winnersArray.count == 0{
                            let winnerRank = LeagueWinnersRank()
                            winnerRank.winAmount = weakSelf?.leagueDetails?.winAmount
                            winnerRank.rankCount = "Rank".localized() + " : 1"
                            weakSelf?.winnersArray.append(winnerRank)
                        }
                    }

                    weakSelf?.collectionView.reloadData()
                }
                else{
                    let message = result!["message"].string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId]
        isUserValidatingToJoinLeague = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kQuizMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            weakSelf?.isUserValidatingToJoinLeague = false
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary{
                        if let leagueDetails = response["league"]?.dictionary{
                            if let quizUrlString = leagueDetails["quiz_url"]?.string{
                                let quizWebVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "QuizWebViewController") as! QuizWebViewController
                                quizWebVC.matchDetails = weakSelf?.matchDetails
                                quizWebVC.leagueDetails = weakSelf?.leagueDetails
                                quizWebVC.urlString = quizUrlString
                                weakSelf?.navigationController?.pushViewController(quizWebVC, animated: true)
                            }
                        }
                    }
                }
                else if statusCode == "401"{
                    if let response = result!["response"]?.dictionary {
                        
                        let titleMessage = "Oops! Low Balance".localized()
                        let creditRequired = response["credit_required"]?.stringValue ?? "0"
                        let responseAmt = Float(creditRequired)!

                        let joiningAmount = Float(leagueDetails.joiningAmount)!
                        let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                        let message = String(format: notEnoughPoints, String(roundFigureAmt))
                        
                        let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                        alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                            
//                            let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                            addCashVC?.leagueDetails = leagueDetails
//                            addCashVC?.amount = roundFigureAmt
//                            addCashVC?.matchDetails = weakSelf!.matchDetails
//                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                navVC.pushViewController(addCashVC!, animated: true)
//                            }
//                        }))
                        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
        
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }

    //MARK:- CollectionView Data Source and Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if leagueDetails == nil{
            return 0
        }
        
        if bannerArray.count > 0 {
            return 3
        }
        
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        if indexPath.row == 0 {
            var height = 205;
            if (leagueDetails?.dynamicLeague == "2") && (leagueDetails?.bonusApplicable == "2"){
                height = 195
            }
            else if (leagueDetails?.dynamicLeague == "2"){
                height = 170
            }
            else if (leagueDetails?.bonusApplicable == "2"){
                height = 160
            }
            else{
                height = 135
            }

            let size = CGSize(width: UIScreen.main.bounds.width, height: CGFloat(height))
            return size
        }
        else if (indexPath.row == 1) && (bannerArray.count != 0){
            let size = CGSize(width: UIScreen.main.bounds.width, height: 90)
            return size
        }
        else{

            var height = 135.0;
            if leagueDetails?.dynamicLeague == "2" {
                height = 195.0
            }
            if leagueDetails != nil {
            if (leagueDetails!.leagueMsg.count > 0){
                    height = height + 50.0;
                }
            }
            height = height + Double(winnersArray.count*50)

            let size = CGSize(width: UIScreen.main.bounds.width, height: CGFloat(height))
            return size
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizPreviewDetailsCollectionViewCell", for: indexPath) as? QuizPreviewDetailsCollectionViewCell
            if leagueDetails != nil {            
                cell?.configData(leagueDetails: leagueDetails!)
            }
            return cell!;
        }
        else if (indexPath.row == 1) && (bannerArray.count != 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JackpotBannerCollectionViewCell", for: indexPath) as? JackpotBannerCollectionViewCell
            cell?.configData(dataArray: bannerArray)
            return cell!;
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueWinneraListCollectionViewCell", for: indexPath) as? LeagueWinneraListCollectionViewCell
            
            cell?.configData(details: leagueDetails!, dataArray: winnersArray, isPreview: false, isQuiz: true)
            return cell!;
        }
    }
    
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}
