//
//  PassDetailsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class PassDetailsViewController: UIViewController {

    @IBOutlet weak var lblSeasonFullName: UILabel!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var scrollViewContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblSessionName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var lblLeagueCategoryTitle: UILabel!
    @IBOutlet weak var lblLeagueCategory: UILabel!
    @IBOutlet weak var lblNoOfEntriesTitle: UILabel!
    @IBOutlet weak var lblNoOfEntries: UILabel!
    @IBOutlet weak var lblNoOfMatchesTitle: UILabel!
    @IBOutlet weak var lblNoOfMatches: UILabel!
    @IBOutlet weak var lblEntryAmountTitle: UILabel!
    @IBOutlet weak var lblEntryAmount: UILabel!
    @IBOutlet weak var lblPassPurchasedPrice: UILabel!
    @IBOutlet weak var lblPassPurchasedWithTite: UILabel!
    @IBOutlet weak var lblPassPurchasedOldPrice: UILabel!
    
    @IBOutlet weak var lblPassPrice: UILabel!
    @IBOutlet weak var lblBuyWithTite: UILabel!
    @IBOutlet weak var lblPassActualPrice: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    
    @IBOutlet weak var purchasedWithView: UIView!
    @IBOutlet weak var buyNowPriceView: UIView!
    @IBOutlet weak var byNowView: UIView!
    var passesDetails: PassesDetails?
    
    @IBOutlet weak var detailsView: UIView!
    var isPassPurchases = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "Pass Details".localized()

        collectionView.register(UINib(nibName: "SeasonPassImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SeasonPassImageCollectionViewCell")
        
        lblBuyWithTite.text = "Buy With".localized()
        buyButton.setTitle("Buy Now".localized(), for: .normal)
        lblPassPurchasedWithTite.text = "Purchased With".localized()

        lblDescription.text = passesDetails?.descriptionEnglish.htmlToString
        lblSessionName.text = passesDetails?.seasonShortName
        lblSeasonFullName.text = passesDetails?.titleEnglish
        
        detailsView.layer.borderColor = UIColor(red: 222.0/235, green: 222.0/235, blue: 222.0/235, alpha: 1).cgColor
        detailsView.layer.borderWidth = 2
        detailsView.layer.cornerRadius = 7.0
        detailsView.clipsToBounds = true
        
        lblLeagueCategory.text = passesDetails?.categoryName
        lblNoOfEntries.text = passesDetails?.totalLeagueEntries
        lblNoOfMatches.text = passesDetails?.totalMatches
        lblEntryAmount.text = "pts" + (passesDetails?.leagueBuyInAmount ?? "0")
          
        
        lblLeagueCategoryTitle.text = "League Category".localized()
        lblNoOfEntriesTitle.text = "No. of Entries".localized()
        lblNoOfMatchesTitle.text = "No. of Matches".localized()
        lblEntryAmountTitle.text = "Entry Amount".localized()
        lblDescriptionTitle.text = "Description".localized()
        
        lblPassPurchasedPrice.text = "pts" + (passesDetails?.passPrice ?? "0")
        lblPassPrice.text = "pts" + (passesDetails?.passPrice ?? "0")
        
        lblPassActualPrice.text = "pts" + (passesDetails?.actualAmount ?? "0")
        lblPassPurchasedOldPrice.text = "pts" + (passesDetails?.actualAmount ?? "0")
        
        if isPassPurchases {
            purchasedWithView.isHidden = false
            buyNowPriceView.isHidden = true
            byNowView.isHidden = true
        }
        else{
            purchasedWithView.isHidden = true
            buyNowPriceView.isHidden = false
            byNowView.isHidden = false
        }
    }
    
    @IBAction func buyNowButtonTapped(_ sender: Any) {
        guard let details = passesDetails else {
            return
        }
        
        let alert = UIAlertController(title: kAlert, message: "Are you sure, you want to purchase this pass?".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
            let amount = Float(details.passPrice) ?? 0
           if amount <= (Float(UserDetails.sharedInstance.totalCredits) ?? 0) {
                self.callPurchasePassAPI(details: details)
           }
           else{
               self.callAddAmountAPI(paymentType: "1", addCashAmount: details.passPrice)
           }
        }))
        alert.addAction(UIAlertAction(title: "No".localized(), style: UIAlertActionStyle.default, handler: nil))
        navigationController?.present(alert, animated: true, completion: nil)
    }
    
    func callPurchasePassAPI(details: PassesDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "purchase_pass", "user_id": UserDetails.sharedInstance.userID, "pass_id": details.passId]
        
        let weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kPassesURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let status = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                
                if status == "200"{
                    
                    var message = "You have successfully received a Season Pass with \(details.totalLeagueEntries) Entries"
                    if details.totalLeagueEntries == "1" {
                        message = "You have successfully received a Season Pass with 1 Single Entry"
                    }
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            if details.totalLeagueEntries == "1" {
                                message = "You have successfully received a Season Pass with 1 Single Entry"
                            }
                            else{
                                message = "आपको \(details.totalLeagueEntries) एंट्रीज के साथ सीज़न पास सफलतापूर्वक मिला है"
                            }
                        }
                    }

                    AppHelper.showAlertView(message: message, isErrorMessage: false)
                    DispatchQueue.main.async {
                        weakSelf.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                    AppHelper.sharedInstance.removeSpinner()
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
    
    //MARK: API Related Methods
    func callAddAmountAPI(paymentType: String, addCashAmount: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "add_cash", "user_id": UserDetails.sharedInstance.userID, "pg_type": paymentType, "amount": addCashAmount, "url": "home", "promo": "", "is_mobile": "1", "gateway_version": "1"]
        if paymentType == "1" {
            MixPanelEventsDetails.depositInitiated(amountInitiated: addCashAmount, paymentMethod: "PAYU")
        }
        else if paymentType == "2"{
            MixPanelEventsDetails.depositInitiated(amountInitiated: addCashAmount, paymentMethod: "PAYTM")

        }
        
        let weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let status = result!["status"]?.string
                if status == "200"{
                    if paymentType == "2"{
//                        weakSelf.sharePayTmPaymentDetails(response: result!["response"]!)
                    }
                    else{
                        weakSelf.genrateRequiredHtml(response: (result!["response"]?.dictionary!)!, paymentType: paymentType, addCashAmount: addCashAmount)
                    }
                }
                else{
                    
                    if let message = result!["message"]?.string{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func genrateRequiredHtml(response: [String: JSON], paymentType: String, addCashAmount: String) {
        var paramsString = ""
        var actionString = ""
        for (key, value) in response {
            if key == "gateway_type"{
                continue
            }
            if key == "action"{
                actionString = value.string!
                continue
            }

            if let keyValue = value.string{
                paramsString += "<input  type=\u{22}hidden\u{22} name=\u{22}" + key + "\u{22} value=\u{22}" + keyValue + "\u{22}>\n"
            }
        }
        
        let htmlString = "<body OnLoad=\u{22}document.payForm.submit();\u{22} ><form method=\u{22}post\u{22} action=\u{22}" + actionString + "\u{22} name=\u{22}payForm\u{22}>" + paramsString + "</form></body>"
        let loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let paymentWebVC = loginStoryboard.instantiateViewController(withIdentifier: "PaymentWebViewController") as? PaymentWebViewController
        paymentWebVC?.htmlString = htmlString

        if paymentType == "2" {
            paymentWebVC?.isWebViewForPaytm = true
        }
        
        paymentWebVC!.paymentAmount = addCashAmount
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            DispatchQueue.main.async {
                navigationVC.pushViewController(paymentWebVC!, animated: true)
            }
        }
    }
}

extension PassDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let details = passesDetails else {
            return 0
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: collectionView.frame.size.width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeasonPassImageCollectionViewCell", for: indexPath) as! SeasonPassImageCollectionViewCell
        cell.configData(details: passesDetails!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    @objc func cellClaimButtonTapped(button: UIButton)  {
    
    }
}
