//
//  WithDrawHistoryViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 19/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class WithDrawHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, WithdrawCashViewControllerDeleagte {
    
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var announcementView: AnnouncementView!
    
    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var withdrawButton: SolidButton!
    @IBOutlet weak var lblWithdrawRequested: UILabel!
    @IBOutlet weak var lblAvailableAmtTitle: UILabel!
    
    @IBOutlet weak var lblInProcess: UILabel!
    @IBOutlet weak var headerView: CustomNavigationBar!
    
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblWithdrawHistory: UILabel!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblAvailableBalance: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var pendingRequestView: UIView!
    @IBOutlet weak var pendingRequestViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var tblBottomConstraint: NSLayoutConstraint!
    
    var pendingRequestDetails: PendingRequest?
    var transactionDetailsArray = Array<[String: Any]>()
    var isNeedToShowLoadMore = false
    var pageNumber = 1
    var pageLimit = 50
    var isRefereshWallet = true
    var withdrawOptionArray = Array<WithdrawOptionDetails>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelButton.layer.borderWidth = 1.0;
        cancelButton.layer.borderColor = UIColor(red: 255.0/255.0, green: 71.0/255, blue: 49.0/255, alpha: 1).cgColor
        cancelButton.layer.cornerRadius = 4.0
        cancelButton.clipsToBounds = true;
        headerView.headerTitle = "Withdraw history".localized()
        lblAvailableAmtTitle.text = "Available Points".localized()
        lblWithdrawRequested.text = "Withdraw Requested".localized();
        withdrawButton.setTitle("Withdraw".localized(), for: .normal)
        cancelButton.setTitle("Cancel Request".localized(), for: .normal)
        lblWithdrawHistory.isHidden = true
        lblBankName.text = "Requested Withdrawal amount".localized()
//        lblWithdrawHistory.text = "Withdraw history".localized()
        lblWithdrawHistory.text = "No History".localized()
        lblInProcess.text = "In Process".localized();
        
        lblAvailableBalance.text = "pts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: UserDetails.sharedInstance.withdrawableCredits);
        tblView.register(UINib(nibName: "TransactionDateTableViewCell", bundle: nil), forCellReuseIdentifier: "TransactionDateTableViewCell")
        tblView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        AppHelper.designBottomTabDesing(bottomView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblAvailableBalance.text = "pts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: UserDetails.sharedInstance.withdrawableCredits);

        if isRefereshWallet {
            callUpatePersonalDetailsAPI()
        }
        isRefereshWallet = true
        
        if AppHelper.isApplicationRunningOnIphoneX() {
            bottomHeightConstraint.constant = 78
            view.layoutIfNeeded()
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        guard let details = pendingRequestDetails else {
            return
        }
        let permissionPopup = PermissionView(frame: APPDELEGATE.window!.frame)
        APPDELEGATE.window?.addSubview(permissionPopup)
        permissionPopup.updateMessage(title: "Cancel Request".localized(), message: "Are your sure you want to revert your withdrawal request for pts" + details.amount)
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                permissionPopup.updateMessage(title: "Cancel Request".localized(), message: "क्या आप वाकई pts " + details.amount + " के लिए अपने निकासी अनुरोध को वापस लेना चाहते हैं" )
            }
        }

        permissionPopup.updateButtonTitle(noButtonTitle: "Cancel".localized(), yesButtonTitle: "Confirm".localized())
        permissionPopup.showAnimation()
        permissionPopup.yesButtonTappedBlock { (status) in
            self.callCancelWithdrawRequestAPI()
        }
    }
    
    @IBAction func withdrawButtonTapped(_ sender: Any) {

        let withdrawableCredits = Double(UserDetails.sharedInstance.withdrawableCredits) ?? 0.0
        
        if withdrawableCredits <= 0.0{
            AppHelper.showAlertViewWithoutMessage(titleMessage: "You have no balance in wallet".localized(), isErrorMessage: true)
            return;
        }
        
        let withdrawCashVC = storyboard?.instantiateViewController(withIdentifier: "WithdrawCashViewController") as! WithdrawCashViewController
        withdrawCashVC.delegate = self;
        withdrawCashVC.withdrawOptionArray = withdrawOptionArray
        navigationController?.pushViewController(withdrawCashVC, animated: true)
    }
    
    // MARK: Table View Delegates and Data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return transactionDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let transAction = transactionDetailsArray[section]
        let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>
        
        if isNeedToShowLoadMore && (section == transactionDetailsArray.count - 1) {
            return detailsArray.count + 1;
        }
        
        return detailsArray.count;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38.0;
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 38))
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 4, width: tableView.bounds.size.width, height: 34))
        titleLabel.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        titleLabel.font = UIFont(name: "OpenSans-Semibold", size: 14)
        headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        let transAction = transactionDetailsArray[section]
        titleLabel.text = transAction["date"] as? String
        titleLabel.textAlignment = .center
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let transAction = transactionDetailsArray[indexPath.section]
        let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>
        
        if  (indexPath.section == transactionDetailsArray.count - 1) && detailsArray.count == indexPath.row{
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            if isNeedToShowLoadMore{
                isNeedToShowLoadMore = false
                callGetTransactionDetailsAPI(isNeedToShowLoader: false)
            }
            
            return cell!
        }
        else {
            var cell = tableView.dequeueReusableCell(withIdentifier: "TransactionDateTableViewCell") as? TransactionDateTableViewCell
            
            if cell == nil {
                cell = TransactionDateTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TransactionDateTableViewCell")
            }

            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            let details = detailsArray[indexPath.row]
            cell!.configData(details: details)
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let transAction = transactionDetailsArray[indexPath.section]
        let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>
        let details = detailsArray[indexPath.row]
        
        let transactionPopup = TransactionDetailsPopup(frame: (APPDELEGATE.window?.frame)!)
        transactionPopup.transactionDetails = details
        transactionPopup.updateTransactionDetails()

        APPDELEGATE.window?.addSubview(transactionPopup)
        DispatchQueue.main.async {
            transactionPopup.showAnimation();
        }
    }
        
    func callGetTransactionDetailsAPI(isNeedToShowLoader: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }

        var transactionID = ""
        
        if let transAction = transactionDetailsArray.last{
            let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>
            let details = detailsArray.last
            transactionID = details?.transactionID ?? ""
        }

        
        let params = ["option": "credit_stats", "user_id": UserDetails.sharedInstance.userID, "page": String(pageNumber), "limit": String(pageLimit), "last_id": transactionID, "is_withdrawal": "1", "version": "v2"]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                
                if statusCode == "200"{
                    
                    if let response = result!["response"]?.dictionary {
                        if let withdrawOptionArray = response["withdraw_option"]?.array {
                            weakSelf?.withdrawOptionArray = WithdrawOptionDetails.getAllPaymentMethods(detailsArray: withdrawOptionArray)
                        }
                    }
                    
                    if weakSelf?.pageNumber == 1{
                        weakSelf?.pageNumber += 1
                        weakSelf?.transactionDetailsArray.removeAll();
                    }
                    else{
                        weakSelf?.pageNumber += 1
                    }
                    
                    var txtTypes = ""

                    if let thisUser = result!["this_user"]?.dictionary {
                        txtTypes = thisUser["negative_txn_types"]?.string ?? ""
                    }

                    let detailsTempArray = TransactionsDetails.getAllTransactionsArray(result: result!, negativeTxnTypes: txtTypes, transactionKey: "data")
                    if detailsTempArray.count >= weakSelf!.pageLimit{
                        weakSelf?.isNeedToShowLoadMore = true;
                    }
                    
                    
                    let tempArray = detailsTempArray.enumerated().map { (index,element) in
                        element.transactionDate
                    }
                    
                    let tempSetArray = Array(Set(tempArray))
                    let setArray = AppHelper.getTheSortedArray(dataArray: tempSetArray)

                    let lastObj = weakSelf?.transactionDetailsArray.last
                    
                    for dateStr in setArray{
                        let transactionArray = detailsTempArray.filter({ (details) -> Bool in
                            details.transactionDate == dateStr
                        })
                        if lastObj != nil{
                            let dateTitle = lastObj!["date"] as! String
                            
                            if dateTitle == dateStr{
                                let detailsArray = lastObj!["details_list"] as! Array<TransactionsDetails>
                                
                                let newTransactionArray = detailsArray + transactionArray
                                weakSelf?.transactionDetailsArray.removeLast()
                                let tempDetails = ["date": dateStr, "details_list": newTransactionArray] as [String : Any]
                                weakSelf?.transactionDetailsArray.append(tempDetails)
                            }
                            else{
                                let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                                weakSelf?.transactionDetailsArray.append(tempDetails)
                                
                            }
                        }
                        else{
                            let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                            weakSelf?.transactionDetailsArray.append(tempDetails)
                        }
                    }
                    if weakSelf!.transactionDetailsArray.count == 0{
                        weakSelf!.tblView.isHidden = true
                        weakSelf!.lblWithdrawHistory.isHidden = false
                        weakSelf!.lblWithdrawHistory.text = "No History".localized()
                    }
                    else{
                        weakSelf!.lblWithdrawHistory.isHidden = true
                        weakSelf!.tblView.isHidden = false
//                        weakSelf!.lblWithdrawHistory.text = "Withdraw history".localized()
                    }
                    
                    DispatchQueue.main.async {
                        weakSelf!.tblView.reloadData()
                    }
                }
                else{
                    let message = result!["message"]?.string ?? ""
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                weakSelf!.isNeedToShowLoadMore = true;
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    func callUpatePersonalDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "get_profile", "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    if let response = result!["response"]{
                        if let announcement = response["announcement"].dictionary{
                            let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                            
                            self.announcementViewHeightConstraint.constant = 0.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: details.message)
                        }
                        else{
                            self.announcementViewHeightConstraint.constant = 0.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: "")
                        }
                        
                        
                        weakSelf?.lblAvailableBalance.text = "pts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: UserDetails.sharedInstance.withdrawableCredits);

                        if let pendingRequest = response.dictionary!["pending_withdraw"]?.dictionary{
                            weakSelf?.pendingRequestDetails = PendingRequest.parsePendingRequest(withdrawData: pendingRequest);
                            weakSelf?.pendingRequestView.isHidden = false
                            weakSelf?.pendingRequestViewHeightConstraint.constant = 130.0
                            weakSelf?.view.layoutIfNeeded()
                            weakSelf?.tblBottomConstraint.constant = 0.0
                            weakSelf?.bottomView.isHidden = true;
                            weakSelf?.lblAmount.text = "pts" +  AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: weakSelf!.pendingRequestDetails!.amount)
                        }
                        else{
                            weakSelf?.pendingRequestView.isHidden = true
                            weakSelf?.pendingRequestViewHeightConstraint.constant = 0.0
                            weakSelf?.view.layoutIfNeeded()
                            weakSelf?.pendingRequestDetails = nil
                        }
                        
                    }
                }
            }
            weakSelf?.callGetTransactionDetailsAPI(isNeedToShowLoader: false);
        }
    }
    
    
    
    func callCancelWithdrawRequestAPI() {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "cancel_withdraw", "user_id": UserDetails.sharedInstance.userID, "withdraw_id": pendingRequestDetails!.withdrawID]

        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    self.pendingRequestView.isHidden = true
                    self.pendingRequestViewHeightConstraint.constant = 0.0
                    self.tblBottomConstraint.constant = 58;
                    self.bottomView.isHidden = false
                    self.view.layoutIfNeeded()
                    
                    let customAlertView = CustomAlertView(frame: APPDELEGATE.window!.frame)
                    customAlertView.updateMessage(title: "request_cancelled".localized(), message: "request_cancelled_msg".localized())
                    APPDELEGATE.window!.addSubview(customAlertView)
                    customAlertView.closePopupViewBlock(complationBlock: { (status) in
                        self.refreshList()
                    })
                }
                else{
                    let errorMsg = result!["message"]?.string ?? "kErrorMsg".localized()
                    AppHelper.showAlertView(message: errorMsg, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func refreshList() {
        DispatchQueue.main.async {
            self.pageNumber = 1
            self.isNeedToShowLoadMore = false
            self.isRefereshWallet = false
            self.transactionDetailsArray.removeAll()
            self.tblView.reloadData()
            self.callUpatePersonalDetailsAPI()
//            self.callGetTransactionDetailsAPI(isNeedToShowLoader: true)
        }
    }

}
