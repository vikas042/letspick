//
//  RewardTransactionViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 26/02/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class RewardTransactionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var transactionsArray = Array<TransactionsDetails>()
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var tblView: UITableView!
    
    var pageNumber = 1
        var pageLimit = 50
        var isNeedToShowLoadMore = false
        var pendingRequestDetails: PendingRequest?
        var transactionDetailsArray = Array<[String: Any]>()
        
        var selectedTypeTab = SelectedProfileTab.profileTab.rawValue
        
        override func viewDidLoad() {
            super.viewDidLoad()
            headerView.headerTitle = "Rewards Transactions".localized()
//            self.lblNoRecordFound.isHidden = true
//            self.placeholderimgView.isHidden = true
            tblView.register(UINib(nibName: "RewardTransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "RewardTransactionTableViewCell")
            tblView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
            
            callGetTransactionDetailsAPI(isShowLoader: true)
            navigationController?.navigationBar.isHidden = true
        }
            

        
        // MARK: Table View Delegates and Data source
        
        func numberOfSections(in tableView: UITableView) -> Int {
            
            return transactionDetailsArray.count
        }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            let transAction = transactionDetailsArray[section]
            let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>
            
            if isNeedToShowLoadMore && (section == transactionDetailsArray.count - 1) {
                return detailsArray.count + 1;
            }

            return detailsArray.count;
        }

        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 38
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 38))
            let titleLabel = UILabel(frame: CGRect(x: 0, y: 4, width: tableView.bounds.size.width, height: 34))
            titleLabel.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
            titleLabel.font = UIFont(name: "OpenSans-Semibold", size: 14)
            headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
            titleLabel.clipsToBounds = true;
            headerView.addSubview(titleLabel)
            headerView.clipsToBounds = true;
            let transAction = transactionDetailsArray[section]
            titleLabel.text = transAction["date"] as? String
            titleLabel.textAlignment = .center
            return headerView
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         
            return 60.0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let transAction = transactionDetailsArray[indexPath.section]
            let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>

            if  (indexPath.section == transactionDetailsArray.count - 1) && detailsArray.count == indexPath.row{

                var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell

                if cell == nil {
                    cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.none

                if isNeedToShowLoadMore{
                    isNeedToShowLoadMore = false
                    callGetTransactionDetailsAPI(isShowLoader: false)
                }

                return cell!
            }
            else {
                var cell = tableView.dequeueReusableCell(withIdentifier: "RewardTransactionTableViewCell") as? RewardTransactionTableViewCell
                
                if cell == nil {
                    cell = RewardTransactionTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "RewardTransactionTableViewCell")
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                let details = detailsArray[indexPath.row]
                cell!.configRewardData(details: details)
                
                return cell!
            }
        }

        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           let transAction = transactionDetailsArray[indexPath.section]
           let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>
           let details = detailsArray[indexPath.row]
            
            AppxorEventHandler.logAppEvent(withName: "TransactionDetailsClicked", info: ["TXN_DATE": details.transactionDate, "TXN_TYPE": details.transactionType, "TXN_AMOUNT": details.transactionAmount])


            if details.isNegativeTranaction {
                let transactionPopup = RewardTransactionPopup(frame: (APPDELEGATE.window?.frame)!)
                transactionPopup.transactionDetails = details
                APPDELEGATE.window?.addSubview(transactionPopup)
                transactionPopup.updateTransactionDetails()
                DispatchQueue.main.async {
                    transactionPopup.showAnimation();
                }
            }
            else{
                let transactionPopup = TransactionDetailsPopup(frame: (APPDELEGATE.window?.frame)!)
                transactionPopup.transactionDetails = details
                transactionPopup.updateRewardTransactionDetails()

                APPDELEGATE.window?.addSubview(transactionPopup)
                DispatchQueue.main.async {
                    transactionPopup.showAnimation();
                }
            }
        }
        
        func callGetTransactionDetailsAPI(isShowLoader: Bool)  {
            
            if !AppHelper.isInterNetConnectionAvailable(){
                return;
            }
            
            if isShowLoader {
                AppHelper.sharedInstance.displaySpinner()
            }
            
            var transactionID = ""
            
            if let transAction = transactionDetailsArray.last{
                let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>
                let details = detailsArray.last
                transactionID = details?.transactionID ?? ""
            }

            let params = ["option": "claim_stats", "user_id": UserDetails.sharedInstance.userID, "page": String(pageNumber), "limit": String(pageLimit), "last_id": transactionID]
            weak var weakSelf = self

            WebServiceHandler.performPOSTRequest(urlString: kRewardProgram, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
                AppHelper.sharedInstance.removeSpinner()
                
                if result != nil{
                    let statusCode = result!["status"]
                    
                    if statusCode == "200"{
                        
                        if weakSelf?.pageNumber == 1{
                            weakSelf?.pageNumber += 1
                            weakSelf?.transactionDetailsArray.removeAll();
                        }
                        else{
                            weakSelf?.pageNumber += 1
                        }
                        
                        var txtTypes = ""

                        if let response = result!["response"]?.dictionary {
                            txtTypes = response["negative_txn_types"]?.string ?? ""
                        }
                        
                        let detailsTempArray = TransactionsDetails.getAllTransactionsArray(result: result!, negativeTxnTypes: txtTypes, transactionKey: "transactions")
                        if detailsTempArray.count >= weakSelf!.pageLimit{
                            weakSelf?.isNeedToShowLoadMore = true;
                        }

                        let tempArray = detailsTempArray.enumerated().map { (index,element) in
                            element.transactionDate
                        }

                        let tempSetArray = Array(Set(tempArray))
                        let setArray = AppHelper.getTheSortedArray(dataArray: tempSetArray)
                        let lastObj = weakSelf?.transactionDetailsArray.last
                        
                        for dateStr in setArray{
                            let transactionArray = detailsTempArray.filter({ (details) -> Bool in
                                details.transactionDate == dateStr
                            })
                            if lastObj != nil{
                                let dateTitle = lastObj!["date"] as! String
                                
                                if dateTitle == dateStr{
                                    let detailsArray = lastObj!["details_list"] as! Array<TransactionsDetails>
                                    let newTransactionArray = detailsArray + transactionArray
                                    weakSelf?.transactionDetailsArray.removeLast()
                                    let tempDetails = ["date": dateStr, "details_list": newTransactionArray] as [String : Any]
                                    weakSelf?.transactionDetailsArray.append(tempDetails)
                                }
                                else{
                                    let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                                    weakSelf?.transactionDetailsArray.append(tempDetails)
                                }
                            }
                            else{
                                let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                                weakSelf?.transactionDetailsArray.append(tempDetails)
                            }
                        }
                        
                        if weakSelf!.transactionDetailsArray.count == 0{
//                            weakSelf!.lblNoRecordFound.isHidden = false
//                            weakSelf!.placeholderimgView.isHidden = false
                            weakSelf!.tblView.isHidden = true
                        }
                        else{
                            weakSelf!.tblView.isHidden = false
//                            weakSelf!.lblNoRecordFound.isHidden = true
//                            weakSelf!.placeholderimgView.isHidden = true
                        }
                        
                        DispatchQueue.main.async {
                            weakSelf!.tblView.reloadData()
                        }
                    }
                    else{
                        let message = result!["message"]?.string ?? ""
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
                else{
                    weakSelf!.isNeedToShowLoadMore = true;
                    AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                }
            }
        }
    
}

