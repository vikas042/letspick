//
//  MyEarningsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 15/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


enum SelectedEarningTab: Int {
    case MyReferrals = 100
    case MyTransactions
}

protocol MyEarningsViewControllerDelegates {
    func updateAffilateAmount(newAmount: String, totalEarning: String)
}

class MyEarningsViewController: UIViewController, SortMyReferralsViewDelegates {
    
    @IBOutlet weak var bottomViewHeightConstaint: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewBottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblMyTransactions: UILabel!
    @IBOutlet weak var lblMyReferrals: UILabel!
    @IBOutlet weak var bottomViewLeadingConstraints: NSLayoutConstraint!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var lblReferalsCount: UILabel!

    var myTransactionArray = Array<[String: Any]>()
    var myEarningsArray = Array<ReferalDetails>()

    lazy var selectedTab = SelectedEarningTab.MyReferrals.rawValue
    lazy var myEarningPageNumber = 1;
    lazy var transactionPageNumber = 1;

    lazy var isNeedToShowMyEarningsLoadMore = false
    lazy var isNeedToShowTransactionLoadMore = false
    lazy var isNeedToMoveOnTranaction = false

    lazy var myEarningPageLimit = 20;
    lazy var transactionsPageLimit = 20;
    lazy var orderType = "4"
    
    var delegates: MyEarningsViewControllerDelegates?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "My Earning".localized()
        
        collectionView.register(UINib(nibName: "MyEarningCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MyEarningCollectionViewCell")
        lblMyReferrals.text = "My Referrals".localized()
        lblMyTransactions.text = "My Transactions".localized()
        
        if selectedTab ==  SelectedEarningTab.MyReferrals.rawValue{
            myReferalsButtonTapped(nil)
        }
        else if selectedTab ==  SelectedEarningTab.MyTransactions.rawValue{
            myTransactionsButtonTapped(nil)
            isNeedToMoveOnTranaction = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isNeedToMoveOnTranaction {            
            DispatchQueue.main.async {
                self.collectionView.scrollToItem(at: IndexPath(row: 1, section: 0), at: .centeredHorizontally, animated: true)
            }

        }
    }
    @IBAction func myReferalsButtonTapped(_ sender: Any?) {
        bottomViewLeadingConstraints.constant = 0
        bottomView.isHidden = false
        sortButton.isHidden = false
        if AppHelper.isApplicationRunningOnIphoneX(){
            bottomViewHeightConstaint.constant = 65
        }
        else{
            bottomViewHeightConstaint.constant = 45
        }

        selectedTab = SelectedEarningTab.MyReferrals.rawValue
        if self.myEarningsArray.count == 0 {
            lblNoDataFound.isHidden = false
        }
        else{
            lblNoDataFound.isHidden = true
        }

        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
        view.layoutIfNeeded()
        if myEarningsArray.count == 0 {
            callGetTransactionAPI(isReferal: true, isNeedLeader: true)
        }
    }
    
    @IBAction func myTransactionsButtonTapped(_ sender: Any?) {
        bottomViewLeadingConstraints.constant = UIScreen.main.bounds.width/2
        bottomView.isHidden = true
        sortButton.isHidden = true
        bottomViewHeightConstaint.constant = 0

        selectedTab = SelectedEarningTab.MyTransactions.rawValue
        collectionView.reloadData()
        view.layoutIfNeeded()
        if self.myTransactionArray.count == 0 {
            lblNoDataFound.isHidden = false
        }
        else{
            lblNoDataFound.isHidden = true
        }
        
        collectionView.scrollToItem(at: IndexPath(row: 1, section: 0), at: .centeredHorizontally, animated: true)

        if myTransactionArray.count == 0 {
            callGetTransactionAPI(isReferal: false, isNeedLeader: true)
        }
    }
    
    @IBAction func sortingButtonTapped(_ sender: Any?) {
    
        AppxorEventHandler.logAppEvent(withName: "TransationSortingClicked", info: nil)
        let sortingView = SortMyReferralsView(frame: APPDELEGATE.window!.frame)
        sortingView.updateOrderType(orderType: orderType)
        sortingView.delegate = self
        APPDELEGATE.window!.addSubview(sortingView)
    }
    
    func refreshRecordsWithType(type: String) {
        orderType = type
        myEarningPageNumber = 1
        callGetTransactionAPI(isReferal: true, isNeedLeader: true)
    }
    func callGetTransactionAPI(isReferal: Bool, isNeedLeader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        if isNeedLeader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        var type = "3"
        var pageNumber = String(transactionPageNumber)
        var pageLimit = String(transactionsPageLimit)

        if isReferal {
            type = "2"
            pageNumber = String(myEarningPageNumber)
            pageLimit = String(myEarningPageLimit)
        }
        
        let parameters = ["option": "get_affiliate_stats", "page": pageNumber, "limit": pageLimit, "user_id": UserDetails.sharedInstance.userID, "type": type, "order_type": orderType] as [String : Any]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kPartnershipURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary {
                        let totalAffiliateAmount = response["total_affiliate_amount"]?.stringValue
                        let currentAffiliateAmount = response["current_affiliate_amount"]?.stringValue
                        weakSelf?.delegates?.updateAffilateAmount(newAmount: currentAffiliateAmount ?? "0", totalEarning: totalAffiliateAmount ?? "0")
                        if isReferal {
                            if self.myEarningPageNumber == 1 {
                                let referalCount = response["referal_count"]?.stringValue
                                weakSelf!.lblReferalsCount.text = (referalCount ?? "0") + " " + "Referrals".localized()
                            }

                            if let dataArray = response["referals"]?.array{
                                weakSelf?.parseMyEarningsData(dataArray: dataArray)
                            }
                            weakSelf?.lblNoDataFound.text = "Oops ! No Referrals Found".localized()

                            if self.myEarningsArray.count == 0 {
                                weakSelf?.lblNoDataFound.isHidden = false
                            }
                            else{
                                weakSelf?.lblNoDataFound.isHidden = true
                            }
                        }
                        else{
                            if let dataArray = response["affiliates"]?.array{
                                weakSelf?.parseTransactionData(dataArray: dataArray)
                            }
                            
                            weakSelf?.lblNoDataFound.text = "Oops ! No Transactions Found".localized()
                            
                            if self.myTransactionArray.count == 0 {
                                weakSelf?.lblNoDataFound.isHidden = false
                            }
                            else{
                                weakSelf?.lblNoDataFound.isHidden = true
                            }
                        }
                    }
                    weakSelf?.collectionView.reloadData()
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    func parseMyEarningsData(dataArray: Array<JSON>) {
        if myEarningPageNumber == 1{
            myEarningPageNumber += 1
            myEarningsArray.removeAll();
            myEarningsArray = ReferalDetails.getAllReferalDetails(dataArray: dataArray)
            if myEarningsArray.count >= myEarningPageLimit{
                isNeedToShowMyEarningsLoadMore = true;
            }
        }
        else{
            myEarningPageNumber += 1
            let detailsTempArray = ReferalDetails.getAllReferalDetails(dataArray: dataArray)
            myEarningsArray.append(contentsOf: detailsTempArray)
            if detailsTempArray.count >= myEarningPageLimit{
                isNeedToShowMyEarningsLoadMore = true;
            }
            else{
                isNeedToShowMyEarningsLoadMore = false;
            }
        }
    }
    
    func parseTransactionData(dataArray: Array<JSON>) {
        
        let detailsTempArray = TransactionsDetails.getAllPartnershipTransactionsArray(responseArray: dataArray)

        if transactionPageNumber == 1{
            transactionPageNumber += 1
        }
        else{
            transactionPageNumber += 1
        }

        if detailsTempArray.count >= transactionsPageLimit{
            isNeedToShowTransactionLoadMore = true;
        } else{
            isNeedToShowTransactionLoadMore = false;
        }

        let tempArray = detailsTempArray.enumerated().map { (index,element) in
            element.transactionDate
        }

        let tempSetArray = Array(Set(tempArray))
        let setArray = AppHelper.getTheSortedArray(dataArray: tempSetArray)
        
        let lastObj = myTransactionArray.last
        
        for dateStr in setArray{
            let transactionArray = detailsTempArray.filter({ (details) -> Bool in
                details.transactionDate == dateStr
            })
            
            if lastObj != nil{
                let dateTitle = lastObj!["date"] as! String
                
                if dateTitle == dateStr{
                    let detailsArray = lastObj!["details_list"] as! Array<TransactionsDetails>

                    let newTransactionArray = detailsArray + transactionArray
                    myTransactionArray.removeLast()
//                    myTransactionArray.removeAll()
                    let tempDetails = ["date": dateStr, "details_list": newTransactionArray] as [String : Any]
                    myTransactionArray.append(tempDetails)
                }
                else{
                    let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                    myTransactionArray.append(tempDetails)
                }
            }
            else{
                let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                myTransactionArray.append(tempDetails)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
        if Int(currentPage) == 0{
            if selectedTab != SelectedEarningTab.MyReferrals.rawValue {
                myReferalsButtonTapped(nil)
            }
        }
        else if Int(currentPage) == 1{
            
            if selectedTab != SelectedEarningTab.MyTransactions.rawValue {
                myTransactionsButtonTapped(nil)
            }

        }
    }
}


extension MyEarningsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyEarningCollectionViewCell", for: indexPath) as! MyEarningCollectionViewCell
        
        
        if indexPath.row == 0 {
            cell.configData(transactions: myTransactionArray, earningArray: myEarningsArray, earningsLoadMore: isNeedToShowMyEarningsLoadMore, transactionLoadMore: isNeedToShowTransactionLoadMore, tabType: SelectedEarningTab.MyReferrals.rawValue)
        }
        else if indexPath.row == 1{
            cell.configData(transactions: myTransactionArray, earningArray: myEarningsArray, earningsLoadMore: isNeedToShowMyEarningsLoadMore, transactionLoadMore: isNeedToShowTransactionLoadMore, tabType: SelectedEarningTab.MyTransactions.rawValue)
        }
        
        
        cell.loadMoreBlock { (status) in
            if status && (self.selectedTab == SelectedEarningTab.MyReferrals.rawValue){
                if self.isNeedToShowMyEarningsLoadMore {
                    self.isNeedToShowMyEarningsLoadMore = false
                    self.callGetTransactionAPI(isReferal: true, isNeedLeader: false)
                }

            }else if status && (self.selectedTab == SelectedEarningTab.MyTransactions.rawValue){
                if self.isNeedToShowTransactionLoadMore {
                    self.isNeedToShowTransactionLoadMore = false
                    self.callGetTransactionAPI(isReferal: false, isNeedLeader: false)
                }
            }
        }
        return cell
    }
}
