
//
//  LeaderboardViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/01/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LeaderboardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var headerView: CustomNavigationBar!
    
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var announcementView: AnnouncementView!
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var placeholderLeaderboardImgView: UIImageView!
    @IBOutlet weak var placeholderLeaderboardLbl: UILabel!
    
    lazy var pageNumber = 1
    lazy var leaderboardsArray = Array<Leaderboards>()
    lazy var leaderboardPath = ""
    var isFromNotification = false
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "Leaderboards".localized()
//        NotificationCenter.default.addObserver(self, selector: #selector(leaderboardNotificationReceived(notification:)), name: NSNotification.Name(rawValue: "leaderboardButtonTapped"), object: nil)
        collectionView.register(UINib(nibName: "LeaderboardsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeaderboardsCollectionViewCell")
        lblNoRecordFound.isHidden = true
        if isFromNotification {
            navigationController?.navigationBar.tag = 2000
        }

        callGetLeaderboardsAPI()

    }
    
    //MARK:- API Related Method
    func callGetLeaderboardsAPI() {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let params = ["option": "get_leaderboards", "fetch_type": "1","screen_msg": "1", "page": String(pageNumber)]
        weak var weakSelf = self;
        
        WebServiceHandler.performPOSTRequest(urlString: kLeaderboardURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    if let filePaths = result!["file_path"]?.dictionary{
                        weakSelf?.leaderboardPath = filePaths["leaderboard_images"]?.string ?? ""
                    }
                    
                    if let response = result!["response"]?.dictionary{
                        if let announcement = response["announcement"]?.dictionary{
                            let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                            
                            self.announcementViewHeightConstraint.constant = 50.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: details.message)
                        }
                        else{
                            self.announcementViewHeightConstraint.constant = 0.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: "")
                        }
                        
                        if let dataArray = response["leaderboards"]?.array{
                            weakSelf?.leaderboardsArray = Leaderboards.getAllLeaderboards(dataArray: dataArray)
                            weakSelf?.collectionView.isHidden = false
                            weakSelf?.placeholderLeaderboardLbl.isHidden = true
                            weakSelf?.placeholderLeaderboardImgView.isHidden = true
                            weakSelf?.collectionView.reloadData()
                            if dataArray.count == 0{
                                weakSelf?.collectionView.isHidden = true
                                weakSelf?.placeholderLeaderboardLbl.isHidden = false
                                weakSelf?.placeholderLeaderboardImgView.isHidden = false
                            }
                            else{
                                weakSelf?.collectionView.isHidden = false
                                weakSelf?.placeholderLeaderboardLbl.isHidden = true
                                weakSelf?.placeholderLeaderboardImgView.isHidden = true
                            }
                        }
                        else{
                          weakSelf?.collectionView.isHidden = true
                          weakSelf?.placeholderLeaderboardLbl.isHidden = false
                          weakSelf?.placeholderLeaderboardImgView.isHidden = false
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)

            }
        }
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return leaderboardsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width , height: 108)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeaderboardsCollectionViewCell", for: indexPath) as! LeaderboardsCollectionViewCell
        
        let details = leaderboardsArray[indexPath.row]
        cell.configData(details: details)
        cell.btnWinner.addTarget(self, action: #selector(winnerButtonPressed), for: .touchUpInside)
        cell.btnRanking.addTarget(self, action: #selector(rankingButtonPressed), for: .touchUpInside)
        cell.btnWinner.tag = indexPath.row
        cell.btnRanking.tag = indexPath.row
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let details = leaderboardsArray[indexPath.row]
        let leaderboardMemberBarVC = storyboard?.instantiateViewController(withIdentifier: "LeaderboardMembersViewController") as! LeaderboardMembersViewController
        leaderboardMemberBarVC.leaderboardID = details.leaderboardId
        leaderboardMemberBarVC.leaderboardName = details.title;
        navigationController?.pushViewController(leaderboardMemberBarVC, animated: true)
    }
   
    //MARK:- Custome Selector Methods
   
    @objc func leaderboardNotificationReceived(notification: Notification) {
        callGetLeaderboardsAPI()
    }

    //MARK:- -IBAction Methods
    @objc func winnerButtonPressed(sender: UIButton){
        
        
        let buttonRow = sender.tag
        let details = leaderboardsArray[buttonRow]
        
        let leaderboardView = LeaderboardPriceView(frame: APPDELEGATE.window!.frame)
        leaderboardView.setupDefaultData(filePath: self.leaderboardPath, leaderboardDetails: details)
        APPDELEGATE.window?.addSubview(leaderboardView)
        leaderboardView.showAnimation()
    }
    
    @objc func rankingButtonPressed(sender:UIButton){
        let buttonRow = sender.tag
        let details = leaderboardsArray[buttonRow]

        AppxorEventHandler.logAppEvent(withName: "SeeRankingsClicked", info: ["MatchID":"" , "ContestID": "", "SportsType": ""])

        let leaderboardMemberBarVC = storyboard?.instantiateViewController(withIdentifier: "LeaderboardMembersViewController") as! LeaderboardMembersViewController
        leaderboardMemberBarVC.leaderboardID = details.leaderboardId
        leaderboardMemberBarVC.leaderboardName = details.title;
        navigationController?.pushViewController(leaderboardMemberBarVC, animated: true)
    }
   
}
