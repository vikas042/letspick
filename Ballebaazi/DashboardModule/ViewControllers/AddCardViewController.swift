//
//  AddCardViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import HyperSDK
import SwiftyJSON


/*

enum CardType: String {
    case Unknown, AMEX, VISA, MASTER, MAESTRO, RUPAY, Diners, DISCOVER, JCB, Elo, Hipercard, UnionPay

    static let allCards = [AMEX, VISA, MASTER, MAESTRO, RUPAY, Diners, DISCOVER, JCB, Elo, Hipercard, UnionPay]

    var regex : String {
        switch self {
        case .AMEX:
           return "^3[47][0-9]{5,}$"
        case .VISA:
           return "^4[0-9]{6,}([0-9]{3})?$"
        case .MASTER:
           return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
            
        // NEED to crossCheck
        case .MAESTRO:
            return "^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$"
        case .RUPAY:
            return "^6[0-9]{15}$"
            
            
        case .Diners:
           return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .DISCOVER:
           return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
           return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
           return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
           return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
           return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
           return ""
        }
    }
}
 */


enum CardType: String {
    case Unknown, AMEX, VISA, MASTER, MAESTRO, RUPAY, DISCOVER

    static let allCards = [AMEX, VISA, MASTER, MAESTRO, RUPAY, DISCOVER]

    var regex : String {
        switch self {
        case .AMEX:
           return "^3[47][0-9]{5,}$"
        case .VISA:
           return "^4[0-9]{6,}([0-9]{3})?$"
        case .MASTER:
           return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .DISCOVER:
           return "^6(?:011|5[0-9]{2})[0-9]{3,}$"

            
        // NEED to crossCheck
        case .MAESTRO:
            return "^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$"
        case .RUPAY:
            return "^6[0-9]{15}$"
            
        default:
           return ""
        }
    }
}

class AddCardViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var saveCardButton: UIButton!
    @IBOutlet weak var yearTxtField: UITextField!
    @IBOutlet weak var monthTxtField: UITextField!
    @IBOutlet weak var accountNumberView: UIView!
    @IBOutlet weak var expiryView: UIView!
    @IBOutlet weak var cvvView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var accountTxtField: UITextField!
    @IBOutlet weak var cvvTxtField: UITextField!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var headerView: CustomNavigationBar!
    var orderDetails: JustPayOrderDetails?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountNumberView.layer.cornerRadius = 7
        expiryView.layer.cornerRadius = 7
        cvvView.layer.cornerRadius = 7
        nameView.layer.cornerRadius = 7
        
        accountNumberView.layer.borderWidth = 1.0
        expiryView.layer.borderWidth = 1.0
        cvvView.layer.borderWidth = 1.0
        nameView.layer.borderWidth = 1.0
        
        accountNumberView.layer.borderColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1).cgColor
        expiryView.layer.borderColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1).cgColor
        cvvView.layer.borderColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1).cgColor
        nameView.layer.borderColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1).cgColor
    }
    
    @IBAction func saveCardButtonTapped(_ sender: Any) {
        
        if saveCardButton.isSelected{
           saveCardButton.isSelected = false
        }
        else{
            saveCardButton.isSelected = true
        }
    }
    
    @IBAction func makePaymentButtonTapped(_ sender: Any) {
        let (cardType, valid) = accountTxtField.validateCreditCardFormat()
        print("cardType.rawValue", cardType.rawValue)
        if !valid{
            AppHelper.showAlertView(message: "Please enter a valid card number", isErrorMessage: true)
            return
        }
        else if monthTxtField.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter card expiry month", isErrorMessage: true)
        }
        else if monthTxtField.text?.count != 2{
            AppHelper.showAlertView(message: "Please enter valid card expiry month", isErrorMessage: true)
        }
        else if yearTxtField.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter card expiry year", isErrorMessage: true)
        }
        else if yearTxtField.text?.count != 4{
            AppHelper.showAlertView(message: "Please enter valid card expiry year", isErrorMessage: true)
        }
        else if cvvTxtField.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter CVV", isErrorMessage: true)
        }
        else if cvvTxtField.text?.count ?? 0 < 3{
            AppHelper.showAlertView(message: "Please enter valid CVV", isErrorMessage: true)
        }
        
        callSaveCardAPI(opName: kOpNameCardTxn, paymentType: "CARD", paymentMethod: cardType.rawValue, cardExpiryMonth: monthTxtField.text!, cardExpiryYear: yearTxtField.text!)
    }

        
    func callSaveCardAPI(opName: String, paymentType: String, paymentMethod: String, cardExpiryMonth: String, cardExpiryYear: String) {
        
        guard let details = orderDetails else {
            AppHelper.sharedInstance.removeSpinner()
            return
        }
        let paymentParams = AppHelper.getPaymentParams(details: details)
        
        let payload = NSMutableDictionary()
        payload["opName"] = opName
        payload["cardNumber"] = accountTxtField.text
        payload["cardExpMonth"] = cardExpiryMonth
        payload["cardExpYear"] = cardExpiryYear
        payload["nameOnCard"] = nameTxtField.text ?? ""
        payload["cardSecurityCode"] = cvvTxtField.text!
        if saveCardButton.isSelected {
            payload["saveToLocker"] = "true"
        }
        else{
            payload["saveToLocker"] = "false"
        }
        payload["isEmi"] = "false"

        if paymentType.count != 0  {
            payload["paymentMethodType"] = paymentType
        }
        
        if paymentMethod.count != 0  {
            payload["paymentMethod"] = paymentMethod
        }
        
        paymentParams["payload"] = AppHelper.dictionary(toString: payload)
        
        Hyper().start(self, data: paymentParams as! [AnyHashable : Any]) { (status, responseData, error) in
            AppHelper.sharedInstance.removeSpinner()
            if ((responseData) != nil) {
                            
                let responseJSON = JSON(responseData as AnyObject);
                print(responseJSON)

                if opName == kOpNameCardTxn{
                    let paymentStatus = responseJSON["status"].string ?? ""
                    if paymentStatus == "CHARGED" {
                        
                    }
                    else if paymentStatus == "PENDING_VBV"{
                        
                    }
                    else{
                        if let response = responseJSON["response"].dictionary {
                            let errorMessage = response["errorMessage"]?.string ?? "kErrorMsg".localized()
                            AppHelper.showAlertView(message: errorMessage, isErrorMessage: true)
                        }
                        else{
                            AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                }
            }
        }
    }
        
    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{

        if string == "" {
            return true;
        }
        
        if textField == nameTxtField {
            return true
        }
        let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))

        if accountTxtField == textField{
            if isNumber && (textField.text!.count < 19) {
                return true
            }
            return false
        }
        else if cvvTxtField == textField{
            if isNumber && (textField.text!.count < 4) {
                return true
            }
            return false
        }
        else if monthTxtField == textField{
            if isNumber && (textField.text!.count < 2) {
                return true
            }
            return false
        }
        else if yearTxtField == textField{
            if isNumber && (textField.text!.count < 4) {
                return true
            }
            return false
        }
        
        if string == " " {
            return false
        }


//        if (textField.text?.count)! < 10 {
//            return isNumber
//        }

        return isNumber
    }
}



extension UITextField{

    func validateCreditCardFormat()-> (type: CardType, valid: Bool) {
        
        let input = self.text!
        let numberOnly = input.replacingOccurrences(of: "[^0-9]", with: "")
        var type: CardType = .Unknown
        var formatted = ""
        var valid = false

        // detect card type
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: numberOnly)) {
                type = card
                break
            }
        }

        // check validity
        valid = luhnCheck(number: numberOnly)

        // format
        var formatted4 = ""
        for character in numberOnly {
            if formatted4.count == 4 {
                formatted += formatted4 + " "
                formatted4 = ""
            }
            formatted4.append(character)
        }

        formatted += formatted4 // the rest

        // return the tuple
        return (type, valid)
    }

    func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }

    func luhnCheck(number: String) -> Bool {
        var sum = 0
        let digitStrings = number.reversed().map { String($0) }

        for (index, element) in digitStrings.enumerated() {
            guard let digit = Int(element) else { return false }
            let odd = index % 2 == 1
            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }

        return sum % 10 == 0
    }
}
