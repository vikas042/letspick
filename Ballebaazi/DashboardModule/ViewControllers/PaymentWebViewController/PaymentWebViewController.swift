//
//  PaymentWebViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 05/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import WebKit

class PaymentWebViewController: UIViewController, WKNavigationDelegate {

    var htmlString = ""
    var isWebViewForPaytm = false
    var paymentAmount = ""
    var promoCode = ""
    var isPaymentCancel = false
    var selectedGameType = GameType.Cricket.rawValue

    lazy var userTeamArray = Array<UserTeamDetails>()
    lazy var categoryName = ""
    var leagueDetails: LeagueDetails?
    var matchDetails: MatchDetails?
    var ticketDetails : TicketDetails?

    @IBOutlet weak var paymentWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentWebView.backgroundColor = UIColor.white
        paymentWebView.isOpaque = false
        paymentWebView.navigationDelegate = self
        if isWebViewForPaytm {
            navigationController?.navigationBar.isHidden = false;
            AppHelper.addBackButtonOnNavigationbar(title: "  Cancel", target: self, isNeedToShowShadow: true)
        }
        else{
            navigationController?.navigationBar.isHidden = true;
            paymentWebView.loadHTMLString(htmlString, baseURL: nil)
        }
        
        AppHelper.sharedInstance.displaySpinner()
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        weak var weakSelf = self

        if let loadedUrl = navigationAction.request.url?.absoluteString{
            if loadedUrl.contains("_cpg"){
                DispatchQueue.main.async {
                    weakSelf?.showAlertViewForCancel()
                }
                decisionHandler(WKNavigationActionPolicy.cancel)
            }
            else if loadedUrl.contains("_cjlpg"){
                DispatchQueue.main.async {
                    weakSelf?.showAlertViewForCancel()
                }
                decisionHandler(WKNavigationActionPolicy.cancel)
            }
            else if loadedUrl.contains("_spg"){
                DispatchQueue.main.async {
                    weakSelf?.showAlertViewForPaymentSuccess()
                }
                decisionHandler(WKNavigationActionPolicy.allow)
            }
            else if loadedUrl.contains("_sjlpg"){
                DispatchQueue.main.async {
                    weakSelf?.showAlertViewForPaymentSuccess()
                }
                decisionHandler(WKNavigationActionPolicy.allow)
            }
            else{
                decisionHandler(WKNavigationActionPolicy.allow)
            }
        }
        else{
            decisionHandler(WKNavigationActionPolicy.allow)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        weak var weakSelf = self
        
        if let loadedUrl = webView.url?.absoluteString{
            if loadedUrl.contains("_cpg"){
                DispatchQueue.main.async {
                    weakSelf?.isPaymentCancel = true
                    weakSelf?.showAlertViewForCancel()
                }
            }
            else if loadedUrl.contains("_cjlpg"){
                DispatchQueue.main.async {
                    weakSelf?.isPaymentCancel = true
                   weakSelf?.showAlertViewForCancel()
                }
            }
            else if loadedUrl.contains("_spg"){
                DispatchQueue.main.async {
                    weakSelf?.showAlertViewForPaymentSuccess()
                }
            }
            else if loadedUrl.contains("_sjlpg"){
                DispatchQueue.main.async {
                    weakSelf?.showAlertViewForPaymentSuccess()
                }
            }
        }

        let deadlineTime = DispatchTime.now() + .seconds(2)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            AppHelper.sharedInstance.removeSpinner()
        }
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        if !isPaymentCancel {
            isPaymentCancel = true;
        }
        AppxorEventHandler.logAppEvent(withName: "PaymentFailed", info: ["Option": "PAYU"])
        AppHelper.sharedInstance.removeSpinner()
    }
    
    func showAlertViewForCancel()  {

        weak var weakSelf = self

        let alert = UIAlertController(title: kAlert, message: "PaymentCancelMsg".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
            weakSelf?.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showAlertViewForPaymentSuccess()  {
        weak var weakSelf = self
        var params = [String : Any]()
        params[amount] = Float(paymentAmount) ?? 0.0
        params[promo_code] = promoCode
        params[payment_method] = "PAYU"
        params[PLATFORM] = PLATFORM_iPHONE
        CleverTapEventDetails.sendEventToCleverTap(eventName: deposite_success, params: params)
        MixPanelEventsDetails.customDepositSuccessFull(amount: paymentAmount, paymentMethod: "PAYU")
        MixPanelEventsDetails.customDepositSuccessFullEvent(amountDeposit: paymentAmount, promoCode: promoCode, paymentMethod: "PAYU")
        AppxorEventHandler.logAppEvent(withName: "PaymentComplete", info: ["Option": "PAYU"])

        let alert = UIAlertController(title: kAlert, message: "PaymentSuccessMsg".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
            if weakSelf!.userTeamArray.count != 0 && (weakSelf!.leagueDetails != nil){
                weakSelf?.goToConfirmLeagueScreen(leagueDetails: weakSelf!.leagueDetails!, userTeamArray: weakSelf!.userTeamArray, categoryName: weakSelf!.categoryName)
            }
            else if weakSelf!.matchDetails?.isQuizMatch ?? false{
                weakSelf?.callLeagueValidationAPI(leagueDetails: weakSelf!.leagueDetails!)
            }
            else{
                weakSelf?.callUpatePersonalDetailsAPI(isNeedToreturn: true)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func callUpatePersonalDetailsAPI(isNeedToreturn: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            if isNeedToreturn{
                self.navigationController?.popToRootViewController(animated: true)
            }
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "get_profile", "screen_msg": "1" ,"user_id": UserDetails.sharedInstance.userID]
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            self.navigationController?.popToRootViewController(animated: true)
            AppHelper.sharedInstance.removeSpinner()
        }
    }

    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, categoryName: String)  {
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          let joinedLeagueConfirmVC = storyboard.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
          joinedLeagueConfirmVC?.leagueDetails = leagueDetails
          joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
          joinedLeagueConfirmVC?.matchDetails = self.matchDetails
          joinedLeagueConfirmVC?.leagueCategoryName = categoryName
          joinedLeagueConfirmVC?.ticketDetails = ticketDetails
          joinedLeagueConfirmVC?.selectedGameType = selectedGameType
          if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
              navVC.pushViewController(joinedLeagueConfirmVC!, animated: true)
          }
      }

    func callLeagueValidationAPI(leagueDetails: LeagueDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId]
        WebServiceHandler.performPOSTRequest(urlString: kQuizMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary{
                        if let details = response["league"]?.dictionary{
                            if let quizUrlString = details["quiz_url"]?.string{
                                let quizWebVC = storyboard.instantiateViewController(withIdentifier: "QuizWebViewController") as! QuizWebViewController
                                quizWebVC.matchDetails = self.matchDetails
                                quizWebVC.urlString = quizUrlString
                                quizWebVC.requestId = details["request_id"]?.string ?? ""
                                quizWebVC.leagueId = leagueDetails.leagueId
                                quizWebVC.leagueDetails = leagueDetails
                                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    navVC.pushViewController(quizWebVC, animated: true)
                                }

                            }
                        }
                    }
                }
                else if statusCode == "401"{
                    if let response = result!["response"]?.dictionary {
                        
                        let titleMessage = "Oops! Low Balance".localized()
                        let creditRequired = response["credit_required"]?.stringValue ?? "0"
                        let responseAmt = Float(creditRequired)!

                        let joiningAmount = Float(leagueDetails.joiningAmount)!
                        let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                        let message = String(format: notEnoughPoints, String(roundFigureAmt))
                        
                        let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                        alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                            
//                            let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                            addCashVC?.leagueDetails = leagueDetails
//                            addCashVC?.amount = roundFigureAmt
//                            addCashVC?.matchDetails = self.matchDetails
//                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                navVC.pushViewController(addCashVC!, animated: true)
//                            }
//                        }))
                        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
