//
//  HowToPlayerDetailsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebKit

class HowToPlayerDetailsViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    var mediaArray = Array<JSON>()
    var howToPlayDetails: HowToPlayDetails?
    var questionNumember = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupData()
    }
    
    func setupData()  {
              
        webView.backgroundColor = UIColor.clear
        webView.isOpaque = false
        collectionView.register(UINib(nibName: "YoutubeVideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "YoutubeVideoCollectionViewCell")
        
        guard let details = howToPlayDetails else {
            return;
        }
        
        mediaArray = details.mediaArray
        lblTitle.text = "\(questionNumember). \(details.title)"
        let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
        let htmlString = headerString + details.message
        
        webView.loadHTMLString(htmlString, baseURL: nil)
        webView.navigationDelegate = self
        if mediaArray.count == 0 {
            collectionViewHeightConstraint.constant = 0
            view.layoutIfNeeded()
        }
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        AppHelper.sharedInstance.removeSpinner()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        AppHelper.sharedInstance.removeSpinner()
    }

}

extension HowToPlayerDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "YoutubeVideoCollectionViewCell", for: indexPath) as! YoutubeVideoCollectionViewCell

        cell.configData(details: mediaArray[indexPath.row])
        return cell
    }
    
    

    

    
}
