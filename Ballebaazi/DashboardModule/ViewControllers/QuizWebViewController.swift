//
//  QuizWebViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 31/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import WebKit
import AVFoundation
import SwiftyJSON

class QuizWebViewController: UIViewController, WKNavigationDelegate, WKScriptMessageHandler, WKUIDelegate {

    var urlString = ""
    
    @IBOutlet weak var webViewContainer: UIView!
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    
    var requestId = ""
    var leagueId = ""
    var webView: WKWebView?
    let speechSynthesizer = AVSpeechSynthesizer()

    @IBOutlet weak var headerView: CustomNavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = matchDetails?.matchName
        guard let newStr = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }

        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        configuration.mediaTypesRequiringUserActionForPlayback = WKAudiovisualMediaTypes.all
        
        let contentController = WKUserContentController()
        contentController.add(self, name: "questionData")
        contentController.add(self, name: "gameStateData")        
        configuration.userContentController = contentController

        webView = WKWebView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: (UIScreen.main.bounds.height - 60)) , configuration: configuration)
        webView!.navigationDelegate = self
        webView!.uiDelegate = self
        self.webViewContainer.addSubview(webView!)
        webView!.load(URLRequest(url: URL(string: newStr)!))
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationFoesIntoBackground), name: NSNotification.Name(rawValue: "applicationFoesIntoBackground"), object: nil)

    }
    
    @objc func applicationFoesIntoBackground(notification: Notification) {
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.request.url?.absoluteString == "inapp://retry" {
            callLeagueValidationAPI(requestId: requestId)
        }
        else if navigationAction.request.url?.absoluteString == "inapp://playagain" {
            callLeagueValidationAPI(requestId: "")
        }
        else if navigationAction.request.url?.absoluteString == "inapp://goback" {
            webView.loadHTMLString("", baseURL: nil)
            let navArray = navigationController!.viewControllers
            var isfoundVC = false
            for selectPlayerVC in navArray {
                if selectPlayerVC is QuizPreviewViewController{
                    navigationController!.popToViewController(selectPlayerVC, animated: true)
                    isfoundVC = true;
                    break;
                }
                else if selectPlayerVC is QuizLeaguesViewController{
                    navigationController!.popToViewController(selectPlayerVC, animated: true)
                    isfoundVC = true;
                    break;
                }
            }
            
            if !isfoundVC{
                navigationController!.popToRootViewController(animated: true)
            }
        }

        decisionHandler(WKNavigationActionPolicy.allow)
    }

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print(message.name)
        switch message.name {
        case "questionData":
                
            speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)

            if var questionData = message.body as? String{
                var isHindi = false
                if questionData.contains("eng#") {
                    isHindi = false
                    questionData = questionData.replacingOccurrences(of: "eng#", with: "")
                }
                else if questionData.contains("hi#") {
                    isHindi = true
                    questionData = questionData.replacingOccurrences(of: "hi#", with: "")
                }

                let questionJSON = JSON(parseJSON: questionData)

                if let questionText = questionJSON["question_text"].string {
                    var speechString = questionText
                    if let optionsArray = questionJSON["options"].array {
                        for details in optionsArray {
                            if let value = details["value"].string{
                                speechString = speechString + "                  \n\n" + value
                            }
                        }
                    }
                    
                    if speechString.count > 0 {
                        let speechUtterance = AVSpeechUtterance(string: speechString)
                        
                        if isHindi {
                            speechUtterance.voice = AVSpeechSynthesisVoice(language: "hi")
                        }
                        else {
                            speechUtterance.voice = AVSpeechSynthesisVoice(language: "en_US")
                        }
                        speechSynthesizer.speak(speechUtterance)
                    }
                }
            }
        case "gameStateData" :
             if var gameStateData = message.body as? String{
                if gameStateData == "gameState@Searching" {
                    
                }
                else if gameStateData == "gameState@Error" {
                    
                }
                else if gameStateData == "gameState@Postmatch" {
                    
                }
            }
        default:
            print("Received invalid message: \(message.name)")
        }
    }

    
    func callLeagueValidationAPI(requestId: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
        let params = ["option": "join_league", "user_id": UserDetails.sharedInstance.userID, "match_key": matchDetails!.matchKey, "league_id": leagueId, "request_id": requestId]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kQuizMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary{
                        if let leagueDetails = response["league"]?.dictionary{
                            if requestId.count == 0 {
                                if let newRequestId = leagueDetails["request_id"]?.string {
                                    weakSelf?.requestId = newRequestId
                                }
                            }

                            if let quizUrlString = leagueDetails["quiz_url"]?.string{
                                guard let newStr = quizUrlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                                self.webView!.load(URLRequest(url: URL(string: newStr)!))
                            }
                        }
                    }
                }
                else if statusCode == "401"{
                    if let response = result!["response"]?.dictionary {
                        
                        let titleMessage = "Oops! Low Balance".localized()
                        let creditRequired = response["credit_required"]?.stringValue ?? "0"
                        let responseAmt = Float(creditRequired)!

                        let joiningAmount = Float(self.leagueDetails!.joiningAmount)!
                        let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                        let message = String(format: notEnoughPoints, String(roundFigureAmt))
                        
                        let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                        alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                            
//                            let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                            addCashVC?.leagueDetails = weakSelf?.leagueDetails
//                            addCashVC?.amount = roundFigureAmt
//                            addCashVC?.matchDetails = weakSelf!.matchDetails
//                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                navVC.pushViewController(addCashVC!, animated: true)
//                            }
//                        }))
                        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
}
