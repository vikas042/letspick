//
//  AboutUsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 23/12/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit


class AboutUsViewController: UIViewController {

    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var tblView: UITableView!
    var dataArray = [["title": "About Us", "image": "AboutUsIcon"], ["title": "Referral Policies", "image": "TermsAndConditionsIcon"], ["title": "FAQ", "image": "FaqIcon"], ["title": "Terms & Conditions", "image": "TermsAndConditionsIcon"], ["title": "Privacy Policy & Legalities", "image": "TermsAndConditionsIcon"], ["title": "Withdrawal Policies", "image": "TermsAndConditionsIcon"], ["title": "Contact Us", "image": "ContactusIcon"]]

    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "Letspick Policies".localized()
        tblView.register(UINib(nibName: "MoreTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreTableViewCell")
        
        if let rewardStatus = UserDefaults.standard.value(forKey: "rewardStatus") as? String {
            if (rewardStatus == "2") || (rewardStatus == "1") {
                let rewardDict = ["title": "How Rewards work?".localized(), "image": "AboutUsIcon"]
                dataArray.insert(rewardDict, at: 4)
            }
        }
    }
}


extension AboutUsViewController: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as? MoreTableViewCell
        
        if cell == nil {
            cell = MoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "MoreTableViewCell")
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none

        let details = dataArray[indexPath.row]
        AppHelper.showShodowOnCellsView(innerView: cell!.innerView)
        cell?.lblCoin.isHidden = true
        cell?.lblTitle.text = details["title"]?.localized()
        cell?.imgView.image = UIImage(named: details["image"]!)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let details = dataArray[indexPath.row]
        let cellTitle = details["title"]!.localized()
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)

        if cellTitle == "About Us".localized() {
            AppxorEventHandler.logAppEvent(withName: "AboutUsClicked", info: nil)
            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.AboutUs.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "How Rewards work?".localized() {
            AppxorEventHandler.logAppEvent(withName: "AboutUsClicked", info: nil)
            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.RewardProgram.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "Referral Policies".localized()  {
            AppxorEventHandler.logAppEvent(withName: "ReferralPoliciesClicked", info: nil)

            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.ReferralPolicies.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "FAQ".localized()  {
            AppxorEventHandler.logAppEvent(withName: "FAQClicked", info: nil)

            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.FAQ.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "Terms & Conditions".localized() {
            AppxorEventHandler.logAppEvent(withName: "TermsAndConditionsClicked", info: nil)

            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.termsAndConditions.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "Withdrawal Policies".localized()  {
            AppxorEventHandler.logAppEvent(withName: "WithdrawalPoliciesClicked", info: nil)

            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.WithdrawalPolicies.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "Privacy Policy & Legalities".localized()  {
            AppxorEventHandler.logAppEvent(withName: "PrivacyPolicy&LegalitiesClicked", info: nil)

            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.PrivacyPolicy.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }
        else if cellTitle == "Contact Us".localized()  {
            AppxorEventHandler.logAppEvent(withName: "ContactUsClicked", info: nil)

            let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
            privacyPolicyVC?.selectedType = SelectedWebViewType.ContactUs.rawValue
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.pushViewController(privacyPolicyVC!, animated: true)
            }
        }


    }
    
    
}
