//
//  HowToPlayViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit


class HowToPlayViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblTopTitle: UILabel!
    var dataArray = Array<HowToPlayDetails>()
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTopTitle.text = "Understand the game better. Play Better!".localized()
        headerView.headerTitle = "How To Play".localized()
        tblView.register(UINib(nibName: "HowToPlayTableViewCell", bundle: nil), forCellReuseIdentifier: "HowToPlayTableViewCell")
        callHowToPlay()
    }

    // MARK:- Tableview Delegates and Data Sources
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "HowToPlayTableViewCell") as? HowToPlayTableViewCell
        
        if cell == nil {
            cell = HowToPlayTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "HowToPlayTableViewCell")
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        AppHelper.showShodowOnCellsView(innerView: cell!.innerView)
        
        let details = dataArray[indexPath.row]
        cell!.lblTitle.text = details.title
        cell!.lblSerial.text = "\(indexPath.row + 1)."
        

        /*
        if let url = NSURL(string: UserDetails.sharedInstance.promotionImageUrl + details.icon){
            cell!.imgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    cell!.imgView.image = image
                }
                else{
                    cell!.imgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            cell!.imgView.image = UIImage(named: "Placeholder")
        }
        */
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let details = dataArray[indexPath.row]

        AppxorEventHandler.logAppEvent(withName: "HowToPlayOptionsClicked", info: ["Option": details.title])

        let howToPlayDetailsVC = storyboard?.instantiateViewController(withIdentifier: "HowToPlayerDetailsViewController") as! HowToPlayerDetailsViewController
        howToPlayDetailsVC.howToPlayDetails = details
        howToPlayDetailsVC.questionNumember = String(indexPath.row + 1)
        navigationController?.pushViewController(howToPlayDetailsVC, animated: true)
    }
    
    func callHowToPlay() {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        var selectedLanguage = "en"
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                selectedLanguage = "hi"
            }
        }
        AppHelper.sharedInstance.displaySpinner()
        let params = ["option": "how_to_play", "lang": selectedLanguage, "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kSupport, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
           AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                if let response = result!["response"]?.array{
                    weakSelf?.dataArray = HowToPlayDetails.gethowToPlayArray(dataArray: response)
                    weakSelf?.tblView.reloadData()
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg", isErrorMessage: true)
            }
        }
    }
    
}
