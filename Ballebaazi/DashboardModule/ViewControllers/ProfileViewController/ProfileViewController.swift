//
//  ProfileViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON
import MapleBacon
import CropViewController

enum SelectedProfileTab: Int {
    case profileTab = 100
    case TransactionTab = 200
}

class ProfileViewController: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate {
    
    @IBOutlet weak var scrollViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var lblWallet: UILabel!
    @IBOutlet weak var lblUsedTitle: UILabel!
    @IBOutlet weak var lblUnusedMessage: UILabel!
    @IBOutlet weak var lblWinningMessage: UILabel!
    @IBOutlet weak var lblBonusMessage: UILabel!

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblWinningsTitle: UILabel!
    @IBOutlet weak var lblBonusTitle: UILabel!
    
    @IBOutlet weak var editDetails: UIButton!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var navigationHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cashView: UIView!
    
    @IBOutlet weak var unUsedView: UIView!
    @IBOutlet weak var bonusCashView: UIView!
    
    @IBOutlet weak var winnigCashView: UIView!
    @IBOutlet weak var lblUnsedCash: UILabel!
    @IBOutlet weak var lblBonus: UILabel!
    @IBOutlet weak var lblWinnings: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    var selectedImage: UIImage?
    var isComeFromAddcash = false
        
    var dataArray = [["title": "Passes & Tickets".localized(), "image": "myTaskIcon"], ["title": "Withdrawals".localized(), "image": "WithdrawalIcon"], ["title": "Transactions".localized(), "image": "TransactionsIcon"], ["title": "Verifications".localized(), "image": "VerificationIcon"]]
    
    // MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.userImageView.image = UIImage(named: "ProfilePicPlaceholder")
        tblView.register(UINib(nibName: "MoreTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreTableViewCell")
        
        if let rewardStatus = UserDefaults.standard.value(forKey: "rewardStatus") as? String {
            if (rewardStatus == "2") || (rewardStatus == "1") {
                let rewardDict = ["title": "Rewards Coins".localized(), "image": "RewardCoinsProfile"]
                dataArray.insert(rewardDict, at: 0)
            }
        }
        
        unUsedView.layer.cornerRadius = 5.0
        unUsedView.layer.shadowColor = UIColor(red: 229.0/255, green: 240.0/255, blue: 248.0/255, alpha: 1).cgColor
        unUsedView.layer.shadowOffset = CGSize(width: 0, height: 0)
        unUsedView.layer.shadowOpacity = 2.0
        unUsedView.layer.shadowRadius = 3
        unUsedView.layer.masksToBounds = false

        winnigCashView.layer.cornerRadius = 5.0
        winnigCashView.layer.shadowColor = UIColor(red: 229.0/255, green: 240.0/255, blue: 248.0/255, alpha: 1).cgColor
        winnigCashView.layer.shadowOffset = CGSize(width: 0, height: 0)
        winnigCashView.layer.shadowOpacity = 2.0
        winnigCashView.layer.shadowRadius = 3
        winnigCashView.layer.masksToBounds = false
        
        bonusCashView.layer.cornerRadius = 5.0
        bonusCashView.layer.shadowColor = UIColor(red: 229.0/255, green: 240.0/255, blue: 248.0/255, alpha: 1).cgColor
        bonusCashView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bonusCashView.layer.shadowOpacity = 2.0
        bonusCashView.layer.shadowRadius = 3
        bonusCashView.layer.masksToBounds = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserInfoNotification(notification:)), name: NSNotification.Name(rawValue: "updateUserInfoNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userlogoutNotitification(notification:)), name: NSNotification.Name(rawValue: "userlogoutNotitification"), object: nil)

        if isComeFromAddcash {
            headerView.isHidden = false
            navigationHeightConstraint.constant = 60;
            headerView.layoutIfNeeded()
            callUpatePersonalDetailsAPI(isNeedToShowLoader: true)
        }
    }
    
    @objc func userlogoutNotitification(notification: Notification)  {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        dataArray.removeAll()
        
                
        dataArray = [["title": "Tickets".localized(), "image": "myTaskIcon"], ["title": "Withdrawals".localized(), "image": "WithdrawalIcon"], ["title": "Transactions".localized(), "image": "TransactionsIcon"], ["title": "Verifications".localized(), "image": "VerificationIcon"]]

        if let passStatus = UserDefaults.standard.value(forKey: "passStatus") as? String {
            if passStatus == "1"{
                dataArray = [["title": "Passes & Tickets".localized(), "image": "myTaskIcon"], ["title": "Withdrawals".localized(), "image": "WithdrawalIcon"], ["title": "Transactions".localized(), "image": "TransactionsIcon"], ["title": "Verifications".localized(), "image": "VerificationIcon"]]
            }
        }


        if let rewardStatus = UserDefaults.standard.value(forKey: "rewardStatus") as? String {
            if (rewardStatus == "2") || (rewardStatus == "1") {
                let rewardDict = ["title": "Rewards Coins".localized(), "image": "RewardCoinsProfile"]
                dataArray.insert(rewardDict, at: 0)
            }
        }


        lblUsedTitle.text = "Unused Cash".localized()
        lblBonusTitle.text = "Bonus Cash".localized()
        lblWinningsTitle.text = "Winnings Cash".localized()
        lblUnusedMessage.text = "Money you added".localized()
        lblWinningMessage.text = "Money you won" .localized()
        lblBonusMessage.text = "Bonus you earned".localized()
        lblWallet.text = "Wallet".localized()
        editDetails.setTitle("edit_details".localized(), for: .normal)

        self.lblUserName.text = UserDetails.sharedInstance.userName
        self.lblLocation.text = UserDetails.sharedInstance.emailAdress
        self.lblPhoneNumber.text = UserDetails.sharedInstance.phoneNumber
        configData()
    }

    // MARK:- IBAction Methods
    
    @IBAction func winningCashButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "WalletDetailsClicked", info: nil)
        let withdrawHistoryVC = storyboard?.instantiateViewController(withIdentifier: "WithDrawHistoryViewController") as? WithDrawHistoryViewController
        navigationController?.pushViewController(withdrawHistoryVC!, animated: true)

    }
    @IBAction func bonusCashButtonTapped(_ sender: Any) {
        return
        AppxorEventHandler.logAppEvent(withName: "WalletDetailsClicked", info: nil)
        let addCashVC = storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
        navigationController?.pushViewController(addCashVC!, animated: true)
    }
    
    @IBAction func editProfileButtonTapped(_ sender: Any) {
        
        AppxorEventHandler.logAppEvent(withName: "EditDetailsClicked", info: nil)
        let editVC = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController
        navigationController?.pushViewController(editVC!, animated: true)
    }
    
    @IBAction func photoButtonTapped(_ sender: Any) {
        
        weak var weakSelf = self
        let actionSheet: UIAlertController = UIAlertController(title: "Please choose an option", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheet.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            weakSelf?.cameraButtonTapped()
        }
        actionSheet.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            weakSelf?.galleryButtonTapped()
        }
        actionSheet.addAction(deleteActionButton)
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    @IBAction func viewTransactionButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "TransactionsClicked", info: ["Source": "Account"])

        let transactionsVC = storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as? TransactionsViewController
        self.navigationController?.pushViewController(transactionsVC!, animated: true)
    }
    
    @IBAction func showTicketButtonTapped(_ sender: Any) {
        
        let myTicketVC = storyboard?.instantiateViewController(withIdentifier: "MyTicketViewController") as? MyTicketViewController
        self.navigationController?.pushViewController(myTicketVC!, animated: true)
    }
    
    @IBAction func logoutButtonTapped(_ sender: Any) {
        
        let permissionPopup = PermissionView(frame: APPDELEGATE.window!.frame)
        APPDELEGATE.window?.addSubview(permissionPopup)
        permissionPopup.showAnimation()
        permissionPopup.updateMessage(title: "Logout".localized(), message: "Are your sure you want to logout?".localized())
        permissionPopup.updateButtonTitle(noButtonTitle: "No".localized(), yesButtonTitle: "Yes".localized())

        permissionPopup.noButtonTappedBlock { (status) in
            
        }
        
        permissionPopup.yesButtonTappedBlock { (status) in
            AppHelper.resetDefaults()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userlogoutNotitification"), object: nil)
            AppHelper.callLoginViewController()
        }
    }
    
    //MARK:- Image Picker Related Methods
    func galleryButtonTapped() {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func cameraButtonTapped() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        presentCropViewController()
    }
    
    func presentCropViewController() {
        
        let cropViewController = CropViewController(croppingStyle: .circular, image: selectedImage!)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        selectedImage = image
        self.userImageView.image = selectedImage
        callUpdateUserImageAPI()
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Custom Methods
    
    func configData()  {
        if let rewardStatus = UserDefaults.standard.value(forKey: "rewardStatus") as? String {
            if (rewardStatus == "2") || (rewardStatus == "1") {
                tblView.reloadData()
                scrollViewHeightConstant.constant = 790
                view.layoutIfNeeded()
            }
        }

        lblBonus.text = "pts" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.bonusCash);
        lblUnsedCash.text = "pts" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.unusedCash);
        lblWinnings.text = "pts" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.withdrawableCredits);
        lblUserName.text = UserDetails.sharedInstance.userName
    }
    
    @objc func updateUserInfoNotification(notification: Notification) {
        tblView.reloadData()
        callUpatePersonalDetailsAPI(isNeedToShowLoader: true)
    }
    
    func showProfilePicture(imageName: String?)  {
        
        if (imageName != nil){
            let userImage = UserDetails.sharedInstance.userImageUrl + imageName!
            
            if let url = NSURL(string: userImage){
                userImageView.setImage(with: url as URL, placeholder: UIImage(named: "ProfilePicPlaceholder"), progress: { received, total in
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.userImageView.image = image
                    }
                    else{
                        self?.userImageView.image = UIImage(named: "ProfilePicPlaceholder")
                    }
                })
            }
            else{
                self.userImageView.image = UIImage(named: "ProfilePicPlaceholder")
            }
        }else{
            self.userImageView.image = UIImage(named: "ProfilePicPlaceholder")
        }
    }
    
    func callUpatePersonalDetailsAPI(isNeedToShowLoader: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let params = ["option": "get_profile", "screen_msg": "1" ,"user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    if let response = result!["response"]{
                        if let active_ticket = response.dictionary!["active_tickets"]?.stringValue{
                            UserDetails.sharedInstance.active_tickets = active_ticket
                        }
                        else {
                            UserDetails.sharedInstance.active_tickets = "0"
                        }
                        
                        if let image = response.dictionary!["image"]?.string{
                            weakSelf?.showProfilePicture(imageName: image)
                        }
                        else{
                            weakSelf?.showProfilePicture(imageName: nil)
                        }
                        
                        if let dob = response.dictionary!["dob"]?.string{
                            UserDetails.sharedInstance.dob = dob
                        }
                        
                        if let usernameEditable = response.dictionary!["username_editable"]?.string{
                            if usernameEditable == "yes"{
                                UserDetails.sharedInstance.isUserNameEditable = true
                            }
                            else{
                                UserDetails.sharedInstance.isUserNameEditable = false
                            }
                        }
                        else
                        {
                            UserDetails.sharedInstance.isUserNameEditable = false
                        }
                        
                        if let name = response.dictionary!["name"]?.string{
                            UserDetails.sharedInstance.name = name
                        }
                        
                        if let username = response.dictionary!["username"]?.string{
                            UserDetails.sharedInstance.userName = username
                            weakSelf?.lblUserName.text = username
                        }

                        
                        if let city = response.dictionary!["city"]?.string{
                            UserDetails.sharedInstance.city = city
                        }
                        
                        if let state = response.dictionary!["state"]?.string{
                            UserDetails.sharedInstance.state = state
                        }
                        
                        if let gender = response.dictionary!["gender"]?.string{
                            UserDetails.sharedInstance.gender = gender
                        }
                        
                        if let email = response.dictionary!["email"]?.string{
                            UserDetails.sharedInstance.emailAdress = email;
                        }
                        
                        if let username = response.dictionary!["username"]?.string{
                            UserDetails.sharedInstance.userName = username;
                        }
                        
                        if let referralCode = response.dictionary!["referral_code"]?.string{
                            UserDetails.sharedInstance.referralCode = referralCode;
                        }
                        
                        if let phone = response.dictionary!["phone"]?.string{
                            UserDetails.sharedInstance.phoneNumber = phone;
                        }
                        
                        if let address = response.dictionary!["address"]?.string{
                            UserDetails.sharedInstance.address = address;
                        }
                        
                        DispatchQueue.main.async {
                            weakSelf?.configData()
                        }
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()

        }
    }
    
    func callUpdateUserImageAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let params = ["option": "update_image", "user_id": UserDetails.sharedInstance.userID]
        
        if selectedImage == nil{
            return;
        }
        
        let imageData = UIImageJPEGRepresentation(selectedImage!, 0.5)
        if imageData == nil{
            return;
        }
        
        WebServiceHandler.performMultipartRequest(urlString: kUserUrl, fileName: "image", params: params, imageDataArray: [imageData!], accessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let message = result!["message"]?.string
                AppHelper.showAlertView(message: message!, isErrorMessage: false)
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    @IBAction func myAccountButtonTapped(_ sender: Any) {
        let walletVC = storyboard?.instantiateViewController(withIdentifier: "WalletViewController")
        navigationController?.pushViewController(walletVC!, animated: true)
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ProfileViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as? MoreTableViewCell
        
        if cell == nil {
            cell = MoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "MoreTableViewCell")
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.lblCoin.isHidden = true
        let details = dataArray[indexPath.row]
        AppHelper.showShodowOnAccountCellsView(innerView: cell!.innerView)
        let title = details["title"] ?? ""
        if title.contains("Rewards Coins".localized()) {
            cell?.lblCoin.isHidden = false
            cell?.lblCoin.text = UserDetails.sharedInstance.currentBBcoins
        }
        cell?.lblTitle.text = title
        cell?.imgView.image = UIImage(named: details["image"]!)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let details = dataArray[indexPath.row]
        let cellTitle = details["title"]
        
        if cellTitle == "Tickets".localized() {
            let myTicketVC = storyboard?.instantiateViewController(withIdentifier: "MyTicketViewController") as! MyTicketViewController
            myTicketVC.isPassFeatureHidden = true
            navigationController?.pushViewController(myTicketVC, animated: true)
        }
        else if cellTitle == "Passes & Tickets".localized() {
            let myTicketVC = storyboard?.instantiateViewController(withIdentifier: "MyTicketViewController") as! MyTicketViewController
            navigationController?.pushViewController(myTicketVC, animated: true)
        }
        else if (cellTitle?.contains("Rewards Coins".localized()))! {
            let rewardVC = storyboard?.instantiateViewController(withIdentifier: "RewardStoreViewController") as! RewardStoreViewController
            rewardVC.isFromTabBar = false
            navigationController?.pushViewController(rewardVC, animated: true)
        }
        else if cellTitle == "Withdrawals".localized() {
            let withdrawVC = storyboard?.instantiateViewController(withIdentifier: "WithDrawHistoryViewController") as! WithDrawHistoryViewController
            navigationController?.pushViewController(withdrawVC, animated: true)
        }
        else if cellTitle == "Transactions".localized() {
            AppxorEventHandler.logAppEvent(withName: "TransactionsClicked", info: ["Source": "Account"])
            let transactionVC = storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
            navigationController?.pushViewController(transactionVC, animated: true)
        }
        else if cellTitle == "Verifications".localized() {
            AppxorEventHandler.logAppEvent(withName: "VerificationsClicked", info: nil)
            let verifyVC = storyboard?.instantiateViewController(withIdentifier: "VerifyDocumentsViewController") as! VerifyDocumentsViewController
            navigationController?.pushViewController(verifyVC, animated: true)
        }
    }
}
