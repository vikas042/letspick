//
//  PlayerStatsViewController.swift
//  Letspick
//
//  Created by Madstech on 21/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class PlayerStatsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var newIconImgView: UIImageView!
    @IBOutlet weak var seriesButton: UIButton!
    @IBOutlet weak var newsButton: UIButton!
    @IBOutlet weak var analysis: UIButton!

    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var seriesCollectionView: UICollectionView!
    @IBOutlet weak var bottomTabViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomTabView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedLeagueType = FantasyType.Classic.rawValue
    var totalPlayerInfoArray = Array<PlayerDetails>()
    var gameType = GameType.Cricket.rawValue
    var isLineupsAnnounced = false
    var playerSelectionArray = Array<PlayerMatchInfo>()
    var selectedMathDetails: MatchDetails?
    var selectedPlayerDetails: PlayerDetails?

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "PlayerStatsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayerStatsCollectionViewCell")
        seriesCollectionView.register(UINib(nibName: "PlayerStatesTableContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayerStatesTableContainerCollectionViewCell")
        seriesCollectionView.register(UINib(nibName: "PlayerAnalysisCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayerAnalysisCollectionViewCell")

        seriesButtonTapped(nil)
        headerView.headerTitle = "Player Stats".localized()
        seriesButton.setTitle("SERIES".localized(), for: .normal)
        seriesButton.setTitle("SERIES".localized(), for: .normal)
        newsButton.setTitle("NEWS".localized(), for: .normal)
        newsButton.setTitle("NEWS".localized(), for: .normal)
        analysis.setTitle("ANALYSIS".localized(), for: .normal)
        analysis.setTitle("ANALYSIS".localized(), for: .normal)
        
        if totalPlayerInfoArray.count > 0 {
            if selectedPlayerDetails == nil {
                let playerDetails = totalPlayerInfoArray[0]
                selectedPlayerDetails = playerDetails
            }
            else{
                for index in 0..<totalPlayerInfoArray.count  {
                    let details = totalPlayerInfoArray[index]
                    if details.playerKey == selectedPlayerDetails?.playerKey{
                        let indexPath = IndexPath(item: index, section: 0)
                        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                        break
                    }
                }
            }
            callGetUserPointsApi(playerDetails: selectedPlayerDetails!)
        }
    }
    
    @IBAction func seriesButtonTapped(_ sender: Any?) {
        bottomTabViewLeadingConstraint.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        newsButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        seriesButton.setTitleColor(UIColor(red: 58.0/255, green: 155.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        analysis.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        
        if sender != nil{
            let indexPath = IndexPath(row: 0, section: 0)
            seriesCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    @IBAction func analysisButtonTapped(_ sender: Any?) {
        bottomTabViewLeadingConstraint.constant = UIScreen.main.bounds.size.width/3
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }

        newsButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        seriesButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        analysis.setTitleColor(UIColor(red: 58.0/255, green: 155.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        
        if (sender != nil){
            let indexPath = IndexPath(row: 1, section: 0)
            seriesCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    @IBAction func newsButtonTapped(_ sender: Any?) {
        bottomTabViewLeadingConstraint.constant = UIScreen.main.bounds.size.width*2/3
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        newsButton.setTitleColor(UIColor(red: 58.0/255, green: 155.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        seriesButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        analysis.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        if (sender != nil){
            let indexPath = IndexPath(row: 2, section: 0)
            seriesCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    // MARK:- CollectionView Delegates and Data Source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1000{
            if selectedPlayerDetails == nil {
                return 0
            }
            return 3;
        }
        return totalPlayerInfoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1000{
            return CGSize(width: UIScreen.main.bounds.size.width, height: collectionView.frame.size.height)
        }
        return CGSize(width: 300, height: 136)
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1000 {
            if (gameType == GameType.Cricket.rawValue) && (indexPath.item == 1) {

                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerAnalysisCollectionViewCell", for: indexPath) as! PlayerAnalysisCollectionViewCell
                cell.configData(details: selectedPlayerDetails!, fantasyType: selectedLeagueType)
                
                return cell

            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerStatesTableContainerCollectionViewCell", for: indexPath) as! PlayerStatesTableContainerCollectionViewCell
                cell.configDetails(dataArray: playerSelectionArray, type: indexPath.row + 1, fantasyType: selectedLeagueType, playerName: selectedPlayerDetails?.playerName ?? "")
                return cell
            }
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerStatsCollectionViewCell", for: indexPath) as! PlayerStatsCollectionViewCell
            let playerDetails = totalPlayerInfoArray[indexPath.row]
            cell.configDetails(details: playerDetails, legueType: selectedLeagueType, gameType: gameType, selectedPlayerDetails: selectedPlayerDetails, isLineupsAnnounced: isLineupsAnnounced)
            
            return cell
        }
    }
    
    
    /*
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1000 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerStatesTableContainerCollectionViewCell", for: indexPath) as! PlayerStatesTableContainerCollectionViewCell
            cell.configDetails(dataArray: playerSelectionArray, type: indexPath.row + 1, fantasyType: selectedLeagueType, playerName: selectedPlayerDetails?.playerName ?? "")
            return cell

        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerStatsCollectionViewCell", for: indexPath) as! PlayerStatsCollectionViewCell
            let playerDetails = totalPlayerInfoArray[indexPath.row]
            cell.configDetails(details: playerDetails, legueType: selectedLeagueType, gameType: gameType, selectedPlayerDetails: selectedPlayerDetails, isLineupsAnnounced: isLineupsAnnounced)
            
            return cell
        }
    }
 */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 1000 {
            if (gameType == GameType.Cricket.rawValue) && (indexPath.item == 1) {
                return;
            }
        }
        let playerDetails = totalPlayerInfoArray[indexPath.row]
        selectedPlayerDetails = playerDetails
        callGetUserPointsApi(playerDetails: playerDetails)
    }
    
    
    // MARK:- API Related Methods
    
    func callGetUserPointsApi(playerDetails: PlayerDetails) {
        
        if selectedMathDetails == nil {
            return;
        }
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let parameters = ["option": "get_player_info","match_key": selectedMathDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID, "player_key": playerDetails.playerKey!]
        var urlString = kMatch
        
        if gameType == GameType.Cricket.rawValue {
            urlString = kMatch
        }
        else if gameType == GameType.Kabaddi.rawValue {
            urlString = kKabaddiMatchURL
        }
        else if gameType == GameType.Football.rawValue {
            urlString = kFootballMatchURL
        }
        else if gameType == GameType.Basketball.rawValue {
            urlString = kBasketballMatchURL
        }
        else if gameType == GameType.Baseball.rawValue {
            urlString = kBaseballMatchURL
        }

        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            weak var weakSelf = self
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                        if let playerInfo = response["player_info"]?.dictionary{
                            guard let weakObj = weakSelf else{
                                return;
                            }
                            
                            weakObj.selectedPlayerDetails?.classicPointsForStat = playerInfo["seasonal_classic_points"]?.stringValue ?? "0"
                            weakObj.selectedPlayerDetails?.battingPointsForStat = playerInfo["seasonal_batting_points"]?.stringValue ?? "0"
                            weakObj.selectedPlayerDetails?.bowlingPointsForStat = playerInfo["seasonal_bowling_points"]?.stringValue ?? "0"
                            weakObj.selectedPlayerDetails?.sessionalStartingPoints = playerInfo["seasonal_start_points"]?.stringValue ?? "0"
                            let formClassic = playerInfo["form_classic"]?.stringValue ?? "0"
                            weakObj.selectedPlayerDetails?.formClassic = formClassic
                            if (formClassic != "0") && (formClassic != ""){
                                let isNeedtoShowNewTagForState = UserDefaults.standard.bool(forKey: "isNeedtoShowNewTagForState")
                                if !isNeedtoShowNewTagForState{
                                    self.newIconImgView.isHidden = false
                                }

                                UserDefaults.standard.set(true, forKey: "isNeedtoShowNewTagForState")
                                UserDefaults.standard.synchronize()
                            }
                            
                            
                            weakObj.selectedPlayerDetails?.formBatting = playerInfo["form_batting"]?.stringValue ?? "0"
                            weakObj.selectedPlayerDetails?.formBowlling = playerInfo["form_bowlling"]?.stringValue ?? "0"
                            
                            if let matchInfoArray = response["match_info"]?.array{
                                weakObj.playerSelectionArray = PlayerMatchInfo.getPlayerInfoList(dataArray: matchInfoArray)
                                weakObj.seriesCollectionView.reloadData()
                                weakObj.collectionView.reloadData()
                            }
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == seriesCollectionView {
            let pageWidth = UIScreen.main.bounds.width
            let currentIndex = scrollView.contentOffset.x/pageWidth
            if currentIndex == 0{
                seriesButtonTapped(nil)
            }
            else if currentIndex == 1{
                analysisButtonTapped(nil)
            }
            else if currentIndex == 2{
                newsButtonTapped(nil)
            }
        }
    }
    
}
