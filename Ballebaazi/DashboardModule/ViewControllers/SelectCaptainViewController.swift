//
//  SelectCaptainViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/11/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit


class SelectCaptainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var teamPreviewButton: CustomBorderButton!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var saveButton: SolidButton!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: LetspickCreateTeamHeaderView!
    
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var userTeamDetails: UserTeamDetails?
    var timer: Timer?
    var captionDetails: PlayerDetails?
    var viceCaptionDetails: PlayerDetails?
    
    var tickeDtails: TicketDetails?
    
    lazy var isMatchClosingTimeRefereshing = false
    lazy var isEditPlayerTeam = false
    lazy var totalWicketKeeperArray = Array<PlayerDetails>()
    lazy var totalBatsmanArray = Array<PlayerDetails>()
    lazy var totalBowlerArray = Array<PlayerDetails>()
    lazy var totalAllRounderArray = Array<PlayerDetails>()

    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "SelectCaptainTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectCaptainTableViewCell")
        setupProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        
        if AppHelper.isApplicationRunningOnIphoneX(){
            bottomViewHeightConstraint.constant = 85;
            bottomView.layoutIfNeeded()
        }
        
        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }
    }
    
    //MARK:- -IBAction Methods
    @IBAction func saveButtonTapped(_ sender: Any) {
        
        if (captionDetails == nil) {
            AppHelper.showAlertView(message: "Please select caption.", isErrorMessage: true)
            return;
        }
        else if (viceCaptionDetails == nil) {
            AppHelper.showAlertView(message: "Please select vice-caption.", isErrorMessage: true)
            return;
        }
        
        if leagueDetails?.fantasyType == "5" {
            let selectWizardVC = storyboard?.instantiateViewController(withIdentifier: "SelectWizardViewController") as! SelectWizardViewController
            selectWizardVC.matchDetails = matchDetails
            selectWizardVC.leagueDetails = leagueDetails
            selectWizardVC.isEditPlayerTeam = isEditPlayerTeam
            selectWizardVC.userTeamDetails = userTeamDetails
            selectWizardVC.tickeDtails = tickeDtails
            navigationController?.pushViewController(selectWizardVC, animated: false)
        }
        else{
            if isEditPlayerTeam {
                callUpdateTeamAPI()
            }
            else{
                callCreateTeamAPI()
            }
        }
    }
    
    @IBAction func previewButtonTapped(_ sender: Any) {
        
        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Cricket"])

        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "TeamPreviewViewController") as! TeamPreviewViewController
        teamPreviewVC.totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        teamPreviewVC.matchDetails = matchDetails
        teamPreviewVC.isShowPlayingRole = true;

        if leagueDetails!.fantasyType == "1" {
            teamPreviewVC.selectedFantasy = FantasyType.Classic.rawValue
        }
        else if leagueDetails!.fantasyType == "2" {
            teamPreviewVC.selectedFantasy = FantasyType.Batting.rawValue
        }
        else if leagueDetails!.fantasyType == "3" {
            teamPreviewVC.selectedFantasy = FantasyType.Bowling.rawValue
        }
        else if leagueDetails!.fantasyType == "4" {
            teamPreviewVC.selectedFantasy = FantasyType.Reverse.rawValue
        }
        else if leagueDetails!.fantasyType == "5" {
            teamPreviewVC.selectedFantasy = FantasyType.Wizard.rawValue
        }

        teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
        teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
        teamPreviewVC.firstTeamkey = matchDetails!.firstTeamKey
        teamPreviewVC.secondTeamkey = matchDetails!.secondTeamKey
        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    //MARK:- Custom Methods
    func setupProperties() {
        headerView.isViewForSelectCaptain = true
        AppHelper.designBottomTabDesing(bottomView)
        
        totalWicketKeeperArray = getSelectedTypePlayerList(selectedRow: 0)
        totalBatsmanArray = getSelectedTypePlayerList(selectedRow: 1)
        totalBowlerArray = getSelectedTypePlayerList(selectedRow: 2)
        totalAllRounderArray = getSelectedTypePlayerList(selectedRow: 3)
        
        teamPreviewButton.setTitle("TeamPreview".localized(), for: .normal)
        saveButton.setTitle("Next".localized(), for: .normal)
        teamPreviewButton.setTitle("TeamPreview".localized(), for: .selected)
        saveButton.setTitle("Next".localized(), for: .selected)
        

        
        let caption = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isCaption
        }

        let viceCaption = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isViceCaption
        }
        
        if viceCaption.count > 0 {
            viceCaptionDetails = viceCaption[0]
        }
        
        if caption.count > 0 {
            captionDetails = caption[0]
        }
        headerView.setupDefaultPropertiesForCaptainVicecaptaion(fantasyType: leagueDetails?.fantasyType ?? "")

        headerView.updateCaptainAndViceCaptainName(captain: captionDetails, viceCaptain: viceCaptionDetails)

        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            
            weak var wealSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                wealSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
 
    //MARK:- Timer Handler
    @objc func updateTimerVlaue()  {
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
//                            AppHelper.showMatchCLosedAlertAndPopToRootViewController()
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            return 4
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            return 3
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            return 3
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            return 3
        }
        else if totalAllRounderArray.count > 0 && totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0{
            return 3
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            return 2
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            return 2
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            return 2
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            return 2
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            return 2
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            return 2
        }
        
        return 1

    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        
        label.text = getPlayerType(section: section)
        label.textAlignment = .center
        label.textColor = UIColor(red: 127.0/255.0, green: 132.0/255, blue: 134.0/255, alpha: 1)
        label.font = UIFont(name: "OpenSans-Semibold", size: 12)
        label.backgroundColor = UIColor(red: 235.0/255.0, green: 235.0/255, blue: 235.0/255, alpha: 1)
        return label;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
            else if section == 2 {
                return totalAllRounderArray.count
            }
            else if section == 3 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
            else if section == 2 {
                return totalAllRounderArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
            else if section == 3 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
            else if section == 2 {
                return totalBowlerArray.count
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
            else if section == 2 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBowlerArray.count
            }

        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalBatsmanArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray.count
            }
            else if section == 1 {
                return totalBowlerArray.count
            }
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalAllRounderArray.count
            }
            else if section == 1 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0{
            return totalWicketKeeperArray.count
        }
        else if totalBatsmanArray.count > 0{
            return totalBatsmanArray.count
        }
        else if totalAllRounderArray.count > 0{
            return totalAllRounderArray.count
        }
        else if totalBowlerArray.count > 0{
            return totalBowlerArray.count
        }
        

        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SelectCaptainTableViewCell") as? SelectCaptainTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "SelectCaptainTableViewCell") as? SelectCaptainTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.captainButton.tag = indexPath.row
        cell?.viceCaptainButton.tag = indexPath.row
        cell?.contentView.backgroundColor = UIColor.white
        cell?.captainButton.addTarget(self, action: #selector(self.captainButtonTapped(button:)), for: .touchUpInside)
        cell?.viceCaptainButton.addTarget(self, action: #selector(self.viceCaptainButtonTapped(button:)), for: .touchUpInside)
        cell?.captainButton.backgroundColor = UIColor.white
        cell?.viceCaptainButton.backgroundColor = UIColor.white
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        cell?.viceCaptainButton.setTitleColor(UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1), for: .normal)
        cell?.captainButton.setTitleColor(UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1), for: .normal)
        cell?.contentView.backgroundColor = UIColor.white

        if playerInfo.playerKey == captionDetails?.playerKey {
            cell?.captainButton.backgroundColor = UIColor(red: 3/255, green: 167/255, blue: 80/255, alpha: 1)
            cell?.captainButton.setTitleColor(UIColor.white, for: .normal)
            cell?.contentView.backgroundColor = UIColor(red: 192/255, green: 238/255, blue: 204/255, alpha: 1.0)
            cell?.captainButton.layer.borderColor = UIColor.clear.cgColor
        }

        if playerInfo.playerKey == viceCaptionDetails?.playerKey {
            cell?.viceCaptainButton.backgroundColor = UIColor(red: 3/255, green: 167/255, blue: 80/255, alpha: 1)
            cell?.viceCaptainButton.setTitleColor(UIColor.white, for: .normal)
            cell?.contentView.backgroundColor = UIColor(red: 192/255, green: 238/255, blue: 204/255, alpha: 1.0)
            cell?.viceCaptainButton.layer.borderColor = UIColor.clear.cgColor

        }
        
        if leagueDetails!.fantasyType == "1" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Classic.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if leagueDetails!.fantasyType == "2" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Batting.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if leagueDetails!.fantasyType == "3" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Bowling.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if leagueDetails!.fantasyType == "4" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Reverse.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if leagueDetails!.fantasyType == "5" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Wizard.rawValue, gameType: GameType.Cricket.rawValue)
        }

        return cell!
    }
    

    @objc func captainButtonTapped(button: UIButton) {
        guard let cell = button.superview?.superview as? SelectCaptainTableViewCell else {
            return // or fatalError() or whatever
        }
        
        guard let indexPath = tblView.indexPath(for: cell) else{
            return;
        }

        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        if playerInfo.playerKey == viceCaptionDetails?.playerKey{
            playerInfo.isViceCaption = false
            viceCaptionDetails = nil
//            AppHelper.showAlertView(message: "Captain and Vice-Captain can not be same.", isErrorMessage: true)
//            return;
        }
        captionDetails = playerInfo
        AppxorEventHandler.logAppEvent(withName: "SelectCaptainClicked", info: ["SportType": "Cricket", "PlayerName": playerInfo.playerName ?? "", "PlayerID": playerInfo.playerKey ?? ""])

        for details in totalBowlerArray {
            details.isCaption = false
        }
        
        for details in totalAllRounderArray {
            details.isCaption = false
        }
        
        for details in totalBatsmanArray {
            details.isCaption = false
        }
        
        for details in totalWicketKeeperArray {
            details.isCaption = false
        }
        
        captionDetails?.isCaption = true
        captionDetails?.isViceCaption = false
        headerView.updateCaptainAndViceCaptainName(captain: captionDetails, viceCaptain: viceCaptionDetails)
        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }

        tblView.reloadData()
        
    }
    
    func getPlayerType(section: Int) -> String {
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                return "Batsman".localized()
            }
            else if section == 2 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }

                return "All Rounder".localized()
            }
            else if section == 3 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }

                return "Bowler".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                return "Batsman".localized()
            }
            else if section == 2 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }

        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                return "Batsman".localized()
            }
            else if section == 2 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }

        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }

                return "All Rounder".localized()
            }
            else if section == 2 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }

                return "Bowler".localized()
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }

                return "Batsman".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                return "All Rounder".localized()
            }
            else if section == 2 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                return "Bowler".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }

                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }

                return "Batsman".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }

                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                return "All Rounder".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }

                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }

                return "Bowler".localized()
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }

                return "Batsman".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }

                return "All Rounder".localized()
            }
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }

                return "Batsman".localized()
            }
            else if section == 1 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                return "Bowler".localized()
            }
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }

                return "All Rounder".localized()
            }
            else if section == 1 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                return "Bowler".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0{
            if totalWicketKeeperArray.count > 1{
                return "Wicket Keepers".localized()
            }

            return "Wicket Keeper".localized()
        }
        else if totalBatsmanArray.count > 0{
            if totalBatsmanArray.count > 1{
                return "Batsmen".localized()
            }
            return "Batsman".localized()
        }
        else if totalAllRounderArray.count > 0{
            if totalAllRounderArray.count > 1{
                return "All Rounders".localized()
            }
            return "All Rounder".localized()
        }
        else if totalBowlerArray.count > 0{
            if totalBowlerArray.count > 1{
                return "Bowlers".localized()
            }
            return "Bowler".localized()
        }
        return ""
    }
    
    func getPlayerDetails(section: Int, index: Int) -> PlayerDetails {
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
            else if section == 2 {
                return totalAllRounderArray[index]
            }
            else if section == 3 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
            else if section == 2 {
                return totalAllRounderArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
            else if section == 3 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
            else if section == 2 {
                return totalBowlerArray[index]
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
            else if section == 2 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBowlerArray[index]
            }
            
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalBatsmanArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray[index]
            }
            else if section == 1 {
                return totalBowlerArray[index]
            }
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalAllRounderArray[index]
            }
            else if section == 1 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0{
            return totalWicketKeeperArray[index]
        }
        else if totalBatsmanArray.count > 0{
            return totalBatsmanArray[index]
        }
        else if totalAllRounderArray.count > 0{
            return totalAllRounderArray[index]
        }
        else if totalBowlerArray.count > 0{
            return totalBowlerArray[index]
        }
        return PlayerDetails()
    }
    
    @objc func viceCaptainButtonTapped(button: UIButton) {
        guard let cell = button.superview?.superview as? SelectCaptainTableViewCell else {
            return // or fatalError() or whatever
        }
        
        guard let indexPath = tblView.indexPath(for: cell) else{
            return;
        }
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)

        if playerInfo.playerKey == captionDetails?.playerKey{
            playerInfo.isCaption = false
            captionDetails = nil
//            AppHelper.showAlertView(message: "Captain and Vice-Captain can not be same.", isErrorMessage: true)
//            return;
        }
        
        viceCaptionDetails = playerInfo
        AppxorEventHandler.logAppEvent(withName: "SelectViceCaptainClicked", info: ["SportType": "Cricket", "PlayerName": playerInfo.playerName ?? "", "PlayerID": playerInfo.playerKey ?? ""])

        for details in totalBowlerArray {
            details.isViceCaption = false
        }
        
        for details in totalAllRounderArray {
            details.isViceCaption = false
        }
        
        for details in totalBatsmanArray {
            details.isViceCaption = false
        }
        
        for details in totalWicketKeeperArray {
            details.isViceCaption = false
        }
        
        viceCaptionDetails?.isViceCaption = true
        viceCaptionDetails?.isCaption = false
        headerView.updateCaptainAndViceCaptainName(captain: captionDetails, viceCaptain: viceCaptionDetails)
        
        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }

        
        tblView.reloadData()
        
    }
    
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        
        if selectedRow == 0{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
            })
        }
        
        if playersArray != nil{
            return playersArray!
        }
        
        return []
    }
    
    
    //MARK:- API Releated Methods
    
    func callCreateTeamAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var playerIDs: String?
        let captionID = captionDetails!.playerKey ?? ""
        let viceCaptionID = viceCaptionDetails!.playerKey  ?? ""
        
        for details in UserDetails.sharedInstance.selectedPlayerList {
            if playerIDs == nil{
                playerIDs = details.playerKey!
            }
            else{
                playerIDs = playerIDs! + "," + details.playerKey!
            }
        }
        
        let parameters = ["option": "save_team","captain": captionID, "vice_captain": viceCaptionID, "fantasy_type": leagueDetails!.fantasyType, "match_key": matchDetails!.matchKey, "players": playerIDs!, "user_id": UserDetails.sharedInstance.userID]

        weak var weakSelf = self;
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if weakSelf?.leagueDetails?.matchKey.count == 0{
                        AppHelper.showToast(message: "Team Created Successfully!".localized())
                        
                        let navArray = weakSelf?.navigationController?.viewControllers
                        if navArray!.count > 1 {
                            if let leagueVC = navArray![1] as? MoreLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                                }
                            }
                            else if let leagueVC = navArray![1] as? LeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    
                                    if weakSelf?.leagueDetails?.fantasyType == "1"{
                                        leagueVC.headerView.classicButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "2"{
                                        leagueVC.headerView.battingButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "3"{
                                        leagueVC.headerView.bowlingButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "4"{
                                        leagueVC.headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "5"{
                                        leagueVC.headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
                                    }
                                    
                                    weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                                }
                            }
                            else if let joinedLeagueVC = navArray![1] as? JoinedLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    
                                    if weakSelf?.leagueDetails?.fantasyType == "1"{
                                        joinedLeagueVC.headerView.classicButtonTapped(isNeedToScroll: false)
                                    }
                                    weakSelf?.navigationController?.popToViewController(joinedLeagueVC, animated: true)
                                }
                            }
                        }
                    }
                    else{
                        
                        if let response = result!["response"]{
                            let userTeam = response["user_team"]
                            if userTeam.count > 0{
                                let teamArray = UserTeamDetails.getUserTeamsArray(responseArray: [userTeam], matchDetails: weakSelf!.matchDetails!)
                                if teamArray.count > 0{
                                    weakSelf?.userTeamDetails = teamArray[0]
                                }
                            }
                        }
                        self.goToJoinLeagueConfirmation()
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    private func callUpdateTeamAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var playerIDs: String?
        let captionID = captionDetails!.playerKey
        let viceCaptionID = viceCaptionDetails!.playerKey

        for details in UserDetails.sharedInstance.selectedPlayerList {
            if playerIDs == nil{
                playerIDs = details.playerKey!
            }
            else{
                playerIDs = playerIDs! + "," + details.playerKey!
            }
        }
        
        let parameters = ["option": "update_team","captain": captionID, "vice_captain": viceCaptionID, "fantasy_type": leagueDetails!.fantasyType, "match_key": matchDetails!.matchKey, "players": playerIDs!, "user_id": UserDetails.sharedInstance.userID, "team_number": userTeamDetails!.teamNumber!]
        
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            weak var weakSelf = self;
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
//                    let message = result!["message"]?.string ?? "kErrorMsg".localized()
//                    AppHelper.showToast(message: message)
                    AppHelper.showToast(message: "Team Updated Successfully!".localized())

                    let navArray = weakSelf?.navigationController?.viewControllers
                    if navArray!.count > 1 {
                        if let leagueVC = navArray![1] as? MoreLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                            }
                        }
                        else if let leagueVC = navArray![1] as? LeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                
                                if weakSelf?.leagueDetails?.fantasyType == "1"{
                                    leagueVC.classicButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "2"{
                                    leagueVC.battingButtonTapped(isAnimation: false)
                                }else if weakSelf?.leagueDetails?.fantasyType == "3"{
                                    leagueVC.bowlingButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "4"{
                                    leagueVC.reverseButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "5"{
                                    leagueVC.wizardButtonTapped(isAnimation: false)
                                }

                                weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                            }
                        }
                        else if let joinedLeagueVC = navArray![1] as? JoinedLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                
                                if weakSelf?.leagueDetails?.fantasyType == "1"{
                                    joinedLeagueVC.classicButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "2"{
                                    joinedLeagueVC.battingButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "3"{
                                    joinedLeagueVC.bowlingButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "4"{
                                    joinedLeagueVC.reverseButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "5"{
                                    joinedLeagueVC.wizardButtonTapped(isAnimation: false)
                                }
                                weakSelf?.navigationController?.popToViewController(joinedLeagueVC, animated: true)
                            }
                        }
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    // MARK: - Navigation

    func goToJoinLeagueConfirmation() {

        let joinLegueConfiramation = storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as! JoinLeagueConfirmationViewController
        joinLegueConfiramation.matchDetails = matchDetails
        joinLegueConfiramation.ticketDetails = tickeDtails

        joinLegueConfiramation.leagueDetails = leagueDetails
        if userTeamDetails != nil {
            joinLegueConfiramation.userTeamsArray = [userTeamDetails!]
        }
        self.navigationController?.pushViewController(joinLegueConfiramation, animated: true)
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
