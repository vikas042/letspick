//
//  AddBankViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 30/09/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class AddBankViewController: UIViewController {
    
    @IBOutlet weak var txtFieldBankName: UITextField!
    @IBOutlet weak var txtFieldBranchName: UITextField!
    @IBOutlet weak var txtFieldIFSCCode: UITextField!
    @IBOutlet weak var txtFieldAccountNumber: UITextField!

    @IBOutlet weak var lblIfscCode: UILabel!
    @IBOutlet weak var lblBranchName: UILabel!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblAccountNumber: UILabel!
    
    @IBOutlet weak var containerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        containerView.layer.cornerRadius = 10.0
        containerView.layer.shadowColor = UIColor(red: 78.0/255, green: 84.0/255, blue: 90.0/255, alpha: 0.5).cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        containerView.layer.shadowOpacity = 2.35
        containerView.layer.shadowRadius = 3
        containerView.layer.masksToBounds = false
        
        lblAccountNumber.text = "Account Number".localized()
        lblIfscCode.text = "IFSC Code".localized()
        lblBankName.text = "Bank".localized()
        lblBranchName.text = "Branch".localized()
        
        txtFieldAccountNumber.placeholder = "Account Number".localized()
        txtFieldIFSCCode.placeholder = "IFSC Code".localized()
        txtFieldBankName.placeholder = "Bank".localized()
        txtFieldBranchName.placeholder = "Branch".localized()
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
    
        if txtFieldAccountNumber.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterAccountNumberMsg".localized(), isErrorMessage: true)
            return;
        }
        else if txtFieldIFSCCode.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterIFscCode".localized(), isErrorMessage: true)
            return;
        }
        else if txtFieldBankName.text?.count == 0 {
            AppHelper.showAlertView(message: "SelectBank".localized(), isErrorMessage: true)
            return;
        }
        else if txtFieldBranchName.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterBranch".localized(), isErrorMessage: true)
            return;
        }
        
        callUpdateBankDetailsAPI()
    }
    
    func callUpdateBankDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "verify_bank_request", "user_id": UserDetails.sharedInstance.userID, "bank_name": txtFieldBankName.text!, "account_number": txtFieldAccountNumber.text!, "ifsc_code": txtFieldIFSCCode.text!, "bank_branch": txtFieldBranchName.text!,]
        
        WebServiceHandler.performPOSTRequest(urlString: kVerifyPhone, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    self.navigationController?.popViewController(animated: true)
//                    AppHelper.showAlertView(message: message, isErrorMessage: false)
//                    NotificationCenter.default.post(name: Notification.Name("updateTableWithStatus"), object: SelectedDocumentType.BANK.rawValue)
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
}
