//
//  SelectWizardViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/06/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class SelectWizardViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var teamPreviewButton: CustomBorderButton!
    
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblPlayers: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var saveButton: SolidButton!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var headerView: CustomNavigationBar!
    
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var userTeamDetails: UserTeamDetails?
    var timer: Timer?
    var wizardDetails: PlayerDetails?
    
    var tickeDtails: TicketDetails?
    
    lazy var isMatchClosingTimeRefereshing = false
    lazy var isEditPlayerTeam = false
    lazy var totalWicketKeeperArray = Array<PlayerDetails>()
    lazy var totalBatsmanArray = Array<PlayerDetails>()
    lazy var totalBowlerArray = Array<PlayerDetails>()
    lazy var totalAllRounderArray = Array<PlayerDetails>()

    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "SelectWizardTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectWizardTableViewCell")
        setupProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        
        if AppHelper.isApplicationRunningOnIphoneX(){
            bottomViewHeightConstraint.constant = 85;
            bottomView.layoutIfNeeded()
        }
        
        if (wizardDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }
    }
    
    //MARK:- -IBAction Methods
    @IBAction func saveButtonTapped(_ sender: Any) {
        
        if (wizardDetails == nil) {
            AppHelper.showAlertView(message: "Please select wizard.", isErrorMessage: true)
            return;
        }
        
        if isEditPlayerTeam {
            callUpdateTeamAPI()
        }
        else{
            callCreateTeamAPI()
        }
    }
    
    @IBAction func previewButtonTapped(_ sender: Any) {
        
        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Cricket"])

        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "TeamPreviewViewController") as! TeamPreviewViewController
        teamPreviewVC.totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        teamPreviewVC.matchDetails = matchDetails
        teamPreviewVC.isShowPlayingRole = true;

        if leagueDetails!.fantasyType == "1" {
            teamPreviewVC.selectedFantasy = FantasyType.Classic.rawValue
        }
        else if leagueDetails!.fantasyType == "2" {
            teamPreviewVC.selectedFantasy = FantasyType.Batting.rawValue
        }
        else if leagueDetails!.fantasyType == "3" {
            teamPreviewVC.selectedFantasy = FantasyType.Bowling.rawValue
        }
        else if leagueDetails!.fantasyType == "4" {
            teamPreviewVC.selectedFantasy = FantasyType.Reverse.rawValue
        }
        else if leagueDetails!.fantasyType == "5" {
            teamPreviewVC.selectedFantasy = FantasyType.Wizard.rawValue
        }

        teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
        teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
        teamPreviewVC.firstTeamkey = matchDetails!.firstTeamKey
        teamPreviewVC.secondTeamkey = matchDetails!.secondTeamKey
        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    //MARK:- Custom Methods
    func setupProperties() {
        
        AppHelper.designBottomTabDesing(bottomView)
        totalWicketKeeperArray = getSelectedTypePlayerList(selectedRow: 0)
        totalBatsmanArray = getSelectedTypePlayerList(selectedRow: 1)
        totalBowlerArray = getSelectedTypePlayerList(selectedRow: 2)
        totalAllRounderArray = getSelectedTypePlayerList(selectedRow: 3)
        headerView.headerTitle = "Select Wizard".localized()
        teamPreviewButton.setTitle("TeamPreview".localized(), for: .normal)
        saveButton.setTitle("Save".localized(), for: .normal)
        teamPreviewButton.setTitle("TeamPreview".localized(), for: .selected)
        saveButton.setTitle("Save".localized(), for: .selected)
        
        lblPlayers.text = "Players".localized()
        lblPoints.text = "Points".localized()
        
        let wizardInfo = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isWizard
        }
        
        if wizardInfo.count > 0 {
            wizardDetails = wizardInfo[0]
        }
        
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            
            weak var wealSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                wealSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
 
    //MARK:- Timer Handler
    @objc func updateTimerVlaue()  {
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
        }
        else {
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
//                            AppHelper.showMatchCLosedAlertAndPopToRootViewController()
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            return 4
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            return 3
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            return 3
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            return 3
        }
        else if totalAllRounderArray.count > 0 && totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0{
            return 3
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            return 2
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            return 2
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            return 2
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            return 2
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            return 2
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            return 2
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        
        label.text = getPlayerType(section: section)
        label.textAlignment = .center
        label.textColor = UIColor(red: 127.0/255.0, green: 132.0/255, blue: 134.0/255, alpha: 1)
        label.font = UIFont(name: "OpenSans-Semibold", size: 12)
        label.backgroundColor = UIColor(red: 235.0/255.0, green: 235.0/255, blue: 235.0/255, alpha: 1)
        return label;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
            else if section == 2 {
                return totalAllRounderArray.count
            }
            else if section == 3 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
            else if section == 2 {
                return totalAllRounderArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
            else if section == 3 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
            else if section == 2 {
                return totalBowlerArray.count
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
            else if section == 2 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBowlerArray.count
            }

        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalBatsmanArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray.count
            }
            else if section == 1 {
                return totalBowlerArray.count
            }
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalAllRounderArray.count
            }
            else if section == 1 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0{
            return totalWicketKeeperArray.count
        }
        else if totalBatsmanArray.count > 0{
            return totalBatsmanArray.count
        }
        else if totalAllRounderArray.count > 0{
            return totalAllRounderArray.count
        }
        else if totalBowlerArray.count > 0{
            return totalBowlerArray.count
        }
        

        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SelectWizardTableViewCell") as? SelectWizardTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "SelectWizardTableViewCell") as? SelectWizardTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.contentView.backgroundColor = UIColor.white
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        cell?.lblCaptain.isHidden = true
        cell?.lblWizard.isHidden = true
        cell?.lblMultiplier.isHidden = true

        cell?.lblWizard.textColor = UIColor(red: 238.0/255, green: 180.0/255, blue: 65.0/255, alpha: 1)
        cell?.lblWizard.backgroundColor = UIColor(red: 35.0/255, green: 35.0/255, blue: 35.0/255, alpha: 1)
        cell?.lblWizard.layer.borderWidth = 1
        cell?.contentView.backgroundColor = UIColor.white

        if playerInfo.isCaption && playerInfo.isWizard {
            cell?.lblCaptain.textColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1)
            cell?.lblCaptain.text = "C"
            cell?.lblCaptain.backgroundColor = UIColor.white
            cell?.lblWizard.isHidden = false
            cell?.lblCaptain.isHidden = false
            cell?.lblMultiplier.isHidden = false
            cell?.lblCaptain.layer.borderWidth = 1
            cell?.lblMultiplier.text = "5x"
            cell?.contentView.backgroundColor = UIColor(red: 247.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
        }
        else if playerInfo.isViceCaption && playerInfo.isWizard {
            cell?.lblCaptain.textColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1)
            cell?.lblCaptain.text = "VC"
            cell?.lblCaptain.backgroundColor = UIColor.white
            cell?.lblCaptain.isHidden = false
            cell?.lblWizard.isHidden = false
            cell?.lblMultiplier.isHidden = false
            cell?.lblCaptain.layer.borderWidth = 1
            cell?.lblMultiplier.text = "4.5x"
            cell?.contentView.backgroundColor = UIColor(red: 247.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
        }
        else if playerInfo.isWizard{
            cell?.lblCaptain.textColor = UIColor(red: 238.0/255, green: 180.0/255, blue: 65.0/255, alpha: 1)
            cell?.lblCaptain.text = "W"
            cell?.lblCaptain.backgroundColor = UIColor(red: 35.0/255, green: 35.0/255, blue: 35.0/255, alpha: 1)
            cell?.lblWizard.isHidden = true
            cell?.lblCaptain.isHidden = false
            cell?.lblMultiplier.isHidden = false
            cell?.lblCaptain.layer.borderWidth = 0
//            cell?.lblCaptain.layer.borderWidth = 0
            cell?.lblMultiplier.text = "3x"
            cell?.contentView.backgroundColor = UIColor(red: 247.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
        }
        else if playerInfo.isCaption{
            cell?.lblCaptain.textColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1)
            cell?.lblCaptain.text = "C"
            cell?.lblCaptain.backgroundColor = UIColor.white
            cell?.lblWizard.isHidden = true
            cell?.lblCaptain.isHidden = false
            cell?.lblMultiplier.isHidden = false
            cell?.lblCaptain.layer.borderWidth = 1
            cell?.lblMultiplier.text = "2x"
        }
        else if playerInfo.isViceCaption{
            cell?.lblCaptain.textColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1)
            cell?.lblCaptain.text = "VC"
            cell?.lblCaptain.backgroundColor = UIColor.white
            cell?.lblWizard.isHidden = true
            cell?.lblCaptain.isHidden = false
            cell?.lblMultiplier.isHidden = false
            cell?.lblCaptain.layer.borderWidth = 1
            cell?.lblMultiplier.text = "1.5x"
        }
        
        if leagueDetails!.fantasyType == "1" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Classic.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if leagueDetails!.fantasyType == "2" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Batting.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if leagueDetails!.fantasyType == "3" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Bowling.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if leagueDetails!.fantasyType == "4" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Reverse.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if leagueDetails!.fantasyType == "5" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Wizard.rawValue, gameType: GameType.Cricket.rawValue)
        }
        cell?.lblWizard.layer.borderWidth = 0

        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
         wizardDetails = playerInfo
         AppxorEventHandler.logAppEvent(withName: "SelectWizardClicked", info: ["SportType": "Cricket", "PlayerName": playerInfo.playerName ?? "", "PlayerID": playerInfo.playerKey ?? ""])
         for details in totalBowlerArray {
             details.isWizard = false
         }
         
         for details in totalAllRounderArray {
             details.isWizard = false
         }
         
         for details in totalBatsmanArray {
             details.isWizard = false
         }
         
         for details in totalWicketKeeperArray {
             details.isWizard = false
         }
         
         wizardDetails?.isWizard = true

         if (wizardDetails != nil){
             saveButton.isEnabled = true
             saveButton.updateLayerProperties()
         }
         else{
             saveButton.isEnabled = false
             saveButton.updateLayerProperties()
         }

         tblView.reloadData()
    }
  
    func getPlayerType(section: Int) -> String {
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                return "Batsman".localized()
            }
            else if section == 2 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }

                return "All Rounder".localized()
            }
            else if section == 3 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }

                return "Bowler".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                return "Batsman".localized()
            }
            else if section == 2 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }

        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                return "Batsman".localized()
            }
            else if section == 2 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }

        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }

                return "All Rounder".localized()
            }
            else if section == 2 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }

                return "Bowler".localized()
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }

                return "Batsman".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                return "All Rounder".localized()
            }
            else if section == 2 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                return "Bowler".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }

                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }

                return "Batsman".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }

                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                return "All Rounder".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }

                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }

                return "Bowler".localized()
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }

                return "Batsman".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }

                return "All Rounder".localized()
            }
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }

                return "Batsman".localized()
            }
            else if section == 1 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                return "Bowler".localized()
            }
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }

                return "All Rounder".localized()
            }
            else if section == 1 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                return "Bowler".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0{
            if totalWicketKeeperArray.count > 1{
                return "Wicket Keepers".localized()
            }

            return "Wicket Keeper".localized()
        }
        else if totalBatsmanArray.count > 0{
            if totalBatsmanArray.count > 1{
                return "Batsmen".localized()
            }
            return "Batsman".localized()
        }
        else if totalAllRounderArray.count > 0{
            if totalAllRounderArray.count > 1{
                return "All Rounders".localized()
            }
            return "All Rounder".localized()
        }
        else if totalBowlerArray.count > 0{
            if totalBowlerArray.count > 1{
                return "Bowlers".localized()
            }
            return "Bowler".localized()
        }
        return ""
    }
    
    func getPlayerDetails(section: Int, index: Int) -> PlayerDetails {
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
            else if section == 2 {
                return totalAllRounderArray[index]
            }
            else if section == 3 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
            else if section == 2 {
                return totalAllRounderArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
            else if section == 3 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
            else if section == 2 {
                return totalBowlerArray[index]
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
            else if section == 2 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBowlerArray[index]
            }
            
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalBatsmanArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray[index]
            }
            else if section == 1 {
                return totalBowlerArray[index]
            }
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalAllRounderArray[index]
            }
            else if section == 1 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0{
            return totalWicketKeeperArray[index]
        }
        else if totalBatsmanArray.count > 0{
            return totalBatsmanArray[index]
        }
        else if totalAllRounderArray.count > 0{
            return totalAllRounderArray[index]
        }
        else if totalBowlerArray.count > 0{
            return totalBowlerArray[index]
        }
        return PlayerDetails()
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        
        if selectedRow == 0{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
            })
        }
        
        if playersArray != nil{
            return playersArray!
        }
        
        return []
    }
    
    
    //MARK:- API Releated Methods
    
    func callCreateTeamAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var playerIDs: String?
        
        
        let captionDetails = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isCaption
        }
        
        let viceCaptionDetails = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isViceCaption
        }

        
        let captionID = captionDetails[0].playerKey ?? ""
        let viceCaptionID = viceCaptionDetails[0].playerKey  ?? ""
        let wizardID = wizardDetails!.playerKey  ?? ""

        for details in UserDetails.sharedInstance.selectedPlayerList {
            if playerIDs == nil{
                playerIDs = details.playerKey!
            }
            else{
                playerIDs = playerIDs! + "," + details.playerKey!
            }
        }
        
        let parameters = ["option": "save_team","captain": captionID, "vice_captain": viceCaptionID, "fantasy_type": leagueDetails!.fantasyType, "match_key": matchDetails!.matchKey, "players": playerIDs!, "user_id": UserDetails.sharedInstance.userID, "wizard": wizardID]

        weak var weakSelf = self;
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if weakSelf?.leagueDetails?.matchKey.count == 0{
                        AppHelper.showToast(message: "Team Created Successfully!".localized())
                        
                        let navArray = weakSelf?.navigationController?.viewControllers
                        if navArray!.count > 1 {
                            if let leagueVC = navArray![1] as? MoreLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                                }
                            }
                            else if let leagueVC = navArray![1] as? LeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    
                                    if weakSelf?.leagueDetails?.fantasyType == "1"{
                                        leagueVC.headerView.classicButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "2"{
                                        leagueVC.headerView.battingButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "3"{
                                        leagueVC.headerView.bowlingButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "4"{
                                        leagueVC.headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "5"{
                                        leagueVC.headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
                                    }
                                    
                                    weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                                }
                            }
                            else if let joinedLeagueVC = navArray![1] as? JoinedLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    
                                    if weakSelf?.leagueDetails?.fantasyType == "1"{
                                        joinedLeagueVC.headerView.classicButtonTapped(isNeedToScroll: false)
                                    }
                                    weakSelf?.navigationController?.popToViewController(joinedLeagueVC, animated: true)
                                }
                            }
                        }
                    }
                    else{
                        
                        if let response = result!["response"]{
                            let userTeam = response["user_team"]
                            if userTeam.count > 0{
                                let teamArray = UserTeamDetails.getUserTeamsArray(responseArray: [userTeam], matchDetails: weakSelf!.matchDetails!)
                                if teamArray.count > 0{
                                    weakSelf?.userTeamDetails = teamArray[0]
                                }
                            }
                        }
                        self.goToJoinLeagueConfirmation()
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    private func callUpdateTeamAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var playerIDs: String?

        let captionDetails = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isCaption
        }
        
        let viceCaptionDetails = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isViceCaption
        }
        
        let captionID = captionDetails[0].playerKey ?? ""
        let viceCaptionID = viceCaptionDetails[0].playerKey  ?? ""
        let wizardID = wizardDetails!.playerKey  ?? ""

        for details in UserDetails.sharedInstance.selectedPlayerList {
            if playerIDs == nil{
                playerIDs = details.playerKey!
            }
            else{
                playerIDs = playerIDs! + "," + details.playerKey!
            }
        }
        
        let parameters = ["option": "update_team","captain": captionID, "vice_captain": viceCaptionID, "fantasy_type": leagueDetails!.fantasyType, "match_key": matchDetails!.matchKey, "players": playerIDs!, "user_id": UserDetails.sharedInstance.userID, "team_number": userTeamDetails!.teamNumber!, "wizard": wizardID]
        
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            weak var weakSelf = self;
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    AppHelper.showToast(message: "Team Updated Successfully!".localized())

                    let navArray = weakSelf?.navigationController?.viewControllers
                    if navArray!.count > 1 {
                        if let leagueVC = navArray![1] as? MoreLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                            }
                        }
                        else if let leagueVC = navArray![1] as? LeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                
                                if weakSelf?.leagueDetails?.fantasyType == "1"{
                                    leagueVC.classicButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "2"{
                                    leagueVC.battingButtonTapped(isAnimation: false)
                                }else if weakSelf?.leagueDetails?.fantasyType == "3"{
                                    leagueVC.bowlingButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "4"{
                                    leagueVC.reverseButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "5"{
                                    leagueVC.wizardButtonTapped(isAnimation: false)
                                }

                                weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                            }
                        }
                        else if let joinedLeagueVC = navArray![1] as? JoinedLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                
                                if weakSelf?.leagueDetails?.fantasyType == "1"{
                                    joinedLeagueVC.classicButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "2"{
                                    joinedLeagueVC.battingButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "3"{
                                    joinedLeagueVC.bowlingButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "4"{
                                    joinedLeagueVC.reverseButtonTapped(isAnimation: false)
                                }
                                else if weakSelf?.leagueDetails?.fantasyType == "5"{
                                    joinedLeagueVC.wizardButtonTapped(isAnimation: false)
                                }



                                weakSelf?.navigationController?.popToViewController(joinedLeagueVC, animated: true)
                            }
                        }
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    // MARK: - Navigation

    func goToJoinLeagueConfirmation() {

        let joinLegueConfiramation = storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as! JoinLeagueConfirmationViewController
        joinLegueConfiramation.matchDetails = matchDetails
        joinLegueConfiramation.ticketDetails = tickeDtails

        joinLegueConfiramation.leagueDetails = leagueDetails
        if userTeamDetails != nil {
            joinLegueConfiramation.userTeamsArray = [userTeamDetails!]
        }
        self.navigationController?.pushViewController(joinLegueConfiramation, animated: true)
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
