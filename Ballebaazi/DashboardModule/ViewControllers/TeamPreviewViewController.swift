//
//  TeamPreviewViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 12/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class TeamPreviewViewController: UIViewController {

    @IBOutlet weak var tourneyAnnouncementView: UIView!
    @IBOutlet weak var lblTourneyAnnounced: UILabel!
    @IBOutlet weak var pointsViewHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var wicketKeeperAspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet weak var batsmanViewCenterVertivacalConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var batsManView: UIView!
    @IBOutlet weak var keeperView: UIView!
    @IBOutlet weak var bottomSepView: UIView!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bowlerView: UIView!
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var totalPointValue: UILabel!
    @IBOutlet weak var lblTotalPointTitle: UILabel!
    @IBOutlet weak var allRounderView: UIView!

    var totalPlayerArray = Array<PlayerDetails>()
    var selectedFantasy = FantasyType.Classic.rawValue
    
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var isMatchClosed = false
    var isShowPlayingRole = false
    var firstTeamName = ""
    var secondTeamName = ""
    var firstTeamkey = ""
    var secondTeamkey = ""
    var teamNumber = ""
    var userName = ""
    var isHideEditButton = false
    var matchDetails: MatchDetails?

    @IBOutlet weak var wizardView: UIView!
    @IBOutlet weak var lblWizard: UILabel!
    @IBOutlet weak var lblBowlers: UILabel!
    @IBOutlet weak var lblAllRounders: UILabel!
    @IBOutlet weak var lblWicketKeeper: UILabel!
    @IBOutlet weak var lblBatsMan: UILabel!
    @IBOutlet weak var lblTeamNameTwo: UILabel!
    @IBOutlet weak var lblTeamNameOne: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTeamNameOne.text = firstTeamName
        lblTeamNameTwo.text = secondTeamName
        lblTourneyAnnounced.text = "ANNOUNCED".localized()
        lblWizard.text = "Wizard".localized().uppercased()
        wizardView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tourneyAnnouncementView.isHidden = true
        if isHideEditButton {
            editButton.isHidden = true
        }
        if isMatchClosed {
            lblTotalPointTitle.isHidden = false
            totalPointValue.isHidden = false
            lblTotalPointTitle.text = "Total Points".localized()
            editButton.isHidden = true
            if AppHelper.isApplicationRunningOnIphoneX() {
                bottomViewHeightConstraint.constant = 110
                batsmanViewCenterVertivacalConstraint.constant = -55
                wicketKeeperAspectRatioConstraint.constant = -100
                pointsView.layoutIfNeeded()
            }
            else{
                bottomViewHeightConstraint.constant = 70
            }
        }
        else{
            lblTotalPointTitle.text = "Credit Points".localized()
            lblTotalPointTitle.isHidden = true
            totalPointValue.isHidden = true
            bottomSepView.isHidden = true
            if AppHelper.isApplicationRunningOnIphoneX() {
                bottomViewHeightConstraint.constant = 50
                batsmanViewCenterVertivacalConstraint.constant = -55
                wicketKeeperAspectRatioConstraint.constant = -100
                pointsView.layoutIfNeeded()
            }

            view.layoutIfNeeded()
        }
        
        if selectedFantasy == FantasyType.Classic.rawValue {
            if teamNumber.count > 0{
                lblTeamName.text = "Classic Team".localized() + teamNumber
                if userName.count > 0{
                    lblUserName.text = userName
                }
                else{
                    lblUserName.text = UserDetails.sharedInstance.userName
                }
            }
            else{
                lblTeamName.text = "TeamPreview".localized()
            }
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            if teamNumber.count > 0{
                lblTeamName.text = "Batting Team".localized() + teamNumber
                if userName.count > 0{
                    lblUserName.text = userName
                }
                else{
                    lblUserName.text = UserDetails.sharedInstance.userName
                }
            }
            else{
                lblTeamName.text = "TeamPreview".localized()
            }
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            if teamNumber.count > 0{
                lblTeamName.text = "Bowling Team".localized() + teamNumber
                if userName.count > 0{
                    lblUserName.text = userName
                }
                else{
                    lblUserName.text = UserDetails.sharedInstance.userName
                }
            }
            else{
                lblTeamName.text = "TeamPreview".localized()
            }
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue {
            if teamNumber.count > 0{
                lblTeamName.text = "Reverse Team".localized() + teamNumber
                if userName.count > 0{
                    lblUserName.text = userName
                }
                else{
                    lblUserName.text = UserDetails.sharedInstance.userName
                }
            }
            else{
                lblTeamName.text = "TeamPreview".localized()
            }
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue {
            wizardView.isHidden = false
            if teamNumber.count > 0{
                lblTeamName.text = "Wizard Team".localized() + teamNumber
                if userName.count > 0{
                    lblUserName.text = userName
                }
                else{
                    lblUserName.text = UserDetails.sharedInstance.userName
                }
            }
            else{
                lblTeamName.text = "TeamPreview".localized()
            }
        }
        
        if isShowPlayingRole {
            if matchDetails?.isMatchTourney ?? false{
                showPlayersForTourney(isPlayerPlayingRole: true)
            }
            else{
                showPlayerBeforeMakingTeam(isPlayerPlayingRole: true)
            }
        }
        else{
            if matchDetails?.isMatchTourney ?? false{
                showPlayersForTourney(isPlayerPlayingRole: false)
            }
            else{
                showPlayerBeforeMakingTeam(isPlayerPlayingRole: false)
            }
        }
    }
    
    func showPlayerBeforeMakingTeam(isPlayerPlayingRole: Bool) {
        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            if isPlayerPlayingRole{
                return playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
            }
            else{
                return playerDetails.seasonalRole == PlayerType.WicketKeeper.rawValue
            }
        })
        
        let batsmanPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            if isPlayerPlayingRole{
                return playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
            }
            else{
                return playerDetails.seasonalRole == PlayerType.Batsman.rawValue
            }
        })
        
        let bowlerPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            if isPlayerPlayingRole{
                return playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
            }
            else{
                return playerDetails.seasonalRole == PlayerType.Bowler.rawValue
            }
        })
        
        let allRounderPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            if isPlayerPlayingRole{
                return playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
            }
            else{
                return playerDetails.seasonalRole == PlayerType.AllRounder.rawValue
            }
        })
        
        lblWicketKeeper.text = "WICKET KEEPER".localized()
        if wicketPlayersArray.count > 1 {
            lblWicketKeeper.text = "WICKET KEEPERS".localized()
        }
        
        lblBatsMan.text = "BATSMAN".localized()
        if batsmanPlayersArray.count > 1 {
            lblBatsMan.text = "BATSMEN".localized()
        }
        
        lblAllRounders.text = "ALL ROUNDER".localized()
        if allRounderPlayerArray.count > 1 {
            lblAllRounders.text = "ALL ROUNDERS".localized()
        }
        
        lblBowlers.text = "BOWLER".localized()
        if bowlerPlayersArray.count > 1 {
            lblBowlers.text = "BOWLERS".localized()
        }
        
        var viewWidth = UIScreen.main.bounds.width/5 - 8;

        if (batsmanPlayersArray.count >= 6) || (bowlerPlayersArray.count >= 6){
            viewWidth = UIScreen.main.bounds.width/6 - 10;
        }

        var totalPoint: Float = 0
        if wicketPlayersArray.count == 0{
            keeperView.isHidden = true
        }
        else if wicketPlayersArray.count == 1{
            let playerInfoView = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            let deatails = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            keeperView.addSubview(playerInfoView)
        }
        else if wicketPlayersArray.count == 2{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = wicketPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            keeperView.addSubview(playerInfoView1)
            keeperView.addSubview(playerInfoView2)
        }
        else if wicketPlayersArray.count == 3{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = wicketPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = wicketPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            keeperView.addSubview(playerInfoView1)
            keeperView.addSubview(playerInfoView2)
            keeperView.addSubview(playerInfoView3)
        }
        else if wicketPlayersArray.count == 4{
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = wicketPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = wicketPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = wicketPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            keeperView.addSubview(playerInfoView1)
            keeperView.addSubview(playerInfoView2)
            keeperView.addSubview(playerInfoView3)
            keeperView.addSubview(playerInfoView4)
        }
        
        if batsmanPlayersArray.count == 0{
            batsManView.isHidden = true
        }
        else if batsmanPlayersArray.count == 1{
            let playerInfoView = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView)
        }
        else if batsmanPlayersArray.count == 2{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
        }
        else if batsmanPlayersArray.count == 3{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = batsmanPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
        }
        else if batsmanPlayersArray.count == 4{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = batsmanPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = batsmanPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            
        }
        else if batsmanPlayersArray.count == 5{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView5 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = batsmanPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = batsmanPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = batsmanPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            batsManView.addSubview(playerInfoView5)
            
        }
        else if batsmanPlayersArray.count == 6{
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*3 - 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))
             
             let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - 2*viewWidth - 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))
             
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))
            
             let playerInfoView4 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))

             let playerInfoView5 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth + 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView6 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + 2*viewWidth + 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))

             let details1 = batsmanPlayersArray[0]
             totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails2 = batsmanPlayersArray[1]
             totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails3 = batsmanPlayersArray[2]
             totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails4 = batsmanPlayersArray[3]
             totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
            let deatails5 = batsmanPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails6 = batsmanPlayersArray[5]
            totalPoint = totalPoint + playerInfoView6.showPlayerInformation(details: deatails6, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            batsManView.addSubview(playerInfoView5)
            batsManView.addSubview(playerInfoView6)

        }
        
        if bowlerPlayersArray.count == 0{
            bowlerView.isHidden = true
        }
        else if bowlerPlayersArray.count == 1{
            
            let playerInfoView = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView)
        }
        else if bowlerPlayersArray.count == 2{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
        }
        else if bowlerPlayersArray.count == 3{
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = bowlerPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
        }
        else if bowlerPlayersArray.count == 4{
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = bowlerPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = bowlerPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
        }
        else if bowlerPlayersArray.count == 5{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView5 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = bowlerPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = bowlerPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = bowlerPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            bowlerView.addSubview(playerInfoView5)
        }
        else if bowlerPlayersArray.count == 6{
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*3 - 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))
             
             let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - 2*viewWidth - 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))
             
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))
            
             let playerInfoView4 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))

             let playerInfoView5 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth + 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView6 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + 2*viewWidth + 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 12))

             let details1 = bowlerPlayersArray[0]
             totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails2 = bowlerPlayersArray[1]
             totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails3 = bowlerPlayersArray[2]
             totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails4 = bowlerPlayersArray[3]
             totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
            let deatails5 = bowlerPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails6 = bowlerPlayersArray[5]
            totalPoint = totalPoint + playerInfoView6.showPlayerInformation(details: deatails6, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            bowlerView.addSubview(playerInfoView5)
            bowlerView.addSubview(playerInfoView6)
        }
        
        if allRounderPlayerArray.count == 0{
            allRounderView.isHidden = true
        }
        else if allRounderPlayerArray.count == 1{
            
            let playerInfoView = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView)
        }
        else if allRounderPlayerArray.count == 2{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let details2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            
        }
        else if allRounderPlayerArray.count == 3{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: deatails1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = allRounderPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
        }
        else if allRounderPlayerArray.count == 4{
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = allRounderPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = allRounderPlayerArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
            allRounderView.addSubview(playerInfoView4)
        }
        else if allRounderPlayerArray.count == 5{
            
            let playerInfoView1 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView5 = PlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = allRounderPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = allRounderPlayerArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = allRounderPlayerArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
            allRounderView.addSubview(playerInfoView4)
            allRounderView.addSubview(playerInfoView5)
        }
        
        totalPointValue.text = String(totalPoint)
    }

    
    func showPlayersForTourney(isPlayerPlayingRole: Bool) {
        tourneyAnnouncementView.isHidden = false
        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            if isPlayerPlayingRole{
                return playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
            }
            else{
                return playerDetails.seasonalRole == PlayerType.WicketKeeper.rawValue
            }
        })
        
        let batsmanPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            if isPlayerPlayingRole{
                return playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
            }
            else{
                return playerDetails.seasonalRole == PlayerType.Batsman.rawValue
            }
        })
        
        let bowlerPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            if isPlayerPlayingRole{
                return playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
            }
            else{
                return playerDetails.seasonalRole == PlayerType.Bowler.rawValue
            }
        })
        
        let allRounderPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            if isPlayerPlayingRole{
                return playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
            }
            else{
                return playerDetails.seasonalRole == PlayerType.AllRounder.rawValue
            }
        })
        
        lblWicketKeeper.text = "WICKET KEEPER".localized()
        if wicketPlayersArray.count > 1 {
            lblWicketKeeper.text = "WICKET KEEPERS".localized()
        }
        
        lblBatsMan.text = "BATSMAN".localized()
        if batsmanPlayersArray.count > 1 {
            lblBatsMan.text = "BATSMEN".localized()
        }
        
        lblAllRounders.text = "ALL ROUNDER".localized()
        if allRounderPlayerArray.count > 1 {
            lblAllRounders.text = "ALL ROUNDERS".localized()
        }
        
        lblBowlers.text = "BOWLER".localized()
        if bowlerPlayersArray.count > 1 {
            lblBowlers.text = "BOWLERS".localized()
        }
        
        var viewWidth = UIScreen.main.bounds.width/5 - 8;

        if (batsmanPlayersArray.count >= 6) || (bowlerPlayersArray.count >= 6){
            viewWidth = UIScreen.main.bounds.width/6 - 10;
        }

        var totalPoint: Float = 0
        if wicketPlayersArray.count == 0{
            keeperView.isHidden = true
        }
        else if wicketPlayersArray.count == 1{
            let playerInfoView = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView.playerListArray = totalPlayerArray
            playerInfoView.selectedGameType = GameType.Cricket.rawValue
            playerInfoView.matchDetails = matchDetails

            let deatails = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            keeperView.addSubview(playerInfoView)
        }
        else if wicketPlayersArray.count == 2{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            let details1 = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = wicketPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            keeperView.addSubview(playerInfoView1)
            keeperView.addSubview(playerInfoView2)
        }
        else if wicketPlayersArray.count == 3{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            let details1 = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = wicketPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = wicketPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            keeperView.addSubview(playerInfoView1)
            keeperView.addSubview(playerInfoView2)
            keeperView.addSubview(playerInfoView3)
        }
        else if wicketPlayersArray.count == 4{
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView4 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            playerInfoView4.playerListArray = totalPlayerArray
            playerInfoView4.selectedGameType = GameType.Cricket.rawValue
            playerInfoView4.matchDetails = matchDetails
            
            let details1 = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = wicketPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = wicketPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = wicketPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            keeperView.addSubview(playerInfoView1)
            keeperView.addSubview(playerInfoView2)
            keeperView.addSubview(playerInfoView3)
            keeperView.addSubview(playerInfoView4)
        }
        
        if batsmanPlayersArray.count == 0{
            batsManView.isHidden = true
        }
        else if batsmanPlayersArray.count == 1{
            let playerInfoView = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView.playerListArray = totalPlayerArray
            playerInfoView.selectedGameType = GameType.Cricket.rawValue
            playerInfoView.matchDetails = matchDetails

            
            let deatails = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView)
        }
        else if batsmanPlayersArray.count == 2{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails
            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
        }
        else if batsmanPlayersArray.count == 3{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails

            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = batsmanPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
        }
        else if batsmanPlayersArray.count == 4{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView4 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            playerInfoView4.playerListArray = totalPlayerArray
            playerInfoView4.selectedGameType = GameType.Cricket.rawValue
            playerInfoView4.matchDetails = matchDetails
            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = batsmanPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = batsmanPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            
        }
        else if batsmanPlayersArray.count == 5{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView4 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView5 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            playerInfoView4.playerListArray = totalPlayerArray
            playerInfoView4.selectedGameType = GameType.Cricket.rawValue
            playerInfoView4.matchDetails = matchDetails
            
            playerInfoView5.playerListArray = totalPlayerArray
            playerInfoView5.selectedGameType = GameType.Cricket.rawValue
            playerInfoView5.matchDetails = matchDetails

            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = batsmanPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = batsmanPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = batsmanPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            batsManView.addSubview(playerInfoView5)
            
        }
        else if batsmanPlayersArray.count == 6{
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*3 - 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))
             
             let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - 2*viewWidth - 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))
             
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))
            
             let playerInfoView4 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))

             let playerInfoView5 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth + 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView6 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + 2*viewWidth + 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))

            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            playerInfoView4.playerListArray = totalPlayerArray
            playerInfoView4.selectedGameType = GameType.Cricket.rawValue
            playerInfoView4.matchDetails = matchDetails
            
            playerInfoView5.playerListArray = totalPlayerArray
            playerInfoView5.selectedGameType = GameType.Cricket.rawValue
            playerInfoView5.matchDetails = matchDetails
            
            playerInfoView6.playerListArray = totalPlayerArray
            playerInfoView6.selectedGameType = GameType.Cricket.rawValue
            playerInfoView6.matchDetails = matchDetails

            
            
             let details1 = batsmanPlayersArray[0]
             totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails2 = batsmanPlayersArray[1]
             totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails3 = batsmanPlayersArray[2]
             totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails4 = batsmanPlayersArray[3]
             totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
            let deatails5 = batsmanPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails6 = batsmanPlayersArray[5]
            totalPoint = totalPoint + playerInfoView6.showPlayerInformation(details: deatails6, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            batsManView.addSubview(playerInfoView5)
            batsManView.addSubview(playerInfoView6)

        }
        
        if bowlerPlayersArray.count == 0{
            bowlerView.isHidden = true
        }
        else if bowlerPlayersArray.count == 1{
            
            let playerInfoView = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView.playerListArray = totalPlayerArray
            playerInfoView.selectedGameType = GameType.Cricket.rawValue
            playerInfoView.matchDetails = matchDetails

            let deatails = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView)
        }
        else if bowlerPlayersArray.count == 2{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
        }
        else if bowlerPlayersArray.count == 3{
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = bowlerPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
        }
        else if bowlerPlayersArray.count == 4{
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView4 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            playerInfoView4.playerListArray = totalPlayerArray
            playerInfoView4.selectedGameType = GameType.Cricket.rawValue
            playerInfoView4.matchDetails = matchDetails

            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = bowlerPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = bowlerPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
        }
        else if bowlerPlayersArray.count == 5{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView4 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView5 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            playerInfoView4.playerListArray = totalPlayerArray
            playerInfoView4.selectedGameType = GameType.Cricket.rawValue
            playerInfoView4.matchDetails = matchDetails
            
            playerInfoView5.playerListArray = totalPlayerArray
            playerInfoView5.selectedGameType = GameType.Cricket.rawValue
            playerInfoView5.matchDetails = matchDetails
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = bowlerPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = bowlerPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = bowlerPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            bowlerView.addSubview(playerInfoView5)
        }
        else if bowlerPlayersArray.count == 6{
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*3 - 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))
             
             let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - 2*viewWidth - 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))
             
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))
            
             let playerInfoView4 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))

             let playerInfoView5 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth + 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView6 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + 2*viewWidth + 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 8))

            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            playerInfoView4.playerListArray = totalPlayerArray
            playerInfoView4.selectedGameType = GameType.Cricket.rawValue
            playerInfoView4.matchDetails = matchDetails
            
            playerInfoView5.playerListArray = totalPlayerArray
            playerInfoView5.selectedGameType = GameType.Cricket.rawValue
            playerInfoView5.matchDetails = matchDetails
            
            playerInfoView6.playerListArray = totalPlayerArray
            playerInfoView6.selectedGameType = GameType.Cricket.rawValue
            playerInfoView6.matchDetails = matchDetails
            
             let details1 = bowlerPlayersArray[0]
             totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails2 = bowlerPlayersArray[1]
             totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails3 = bowlerPlayersArray[2]
             totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
             let deatails4 = bowlerPlayersArray[3]
             totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
             
            let deatails5 = bowlerPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails6 = bowlerPlayersArray[5]
            totalPoint = totalPoint + playerInfoView6.showPlayerInformation(details: deatails6, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            bowlerView.addSubview(playerInfoView5)
            bowlerView.addSubview(playerInfoView6)
        }
        
        if allRounderPlayerArray.count == 0{
            allRounderView.isHidden = true
        }
        else if allRounderPlayerArray.count == 1{
            
            let playerInfoView = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView.playerListArray = totalPlayerArray
            playerInfoView.selectedGameType = GameType.Cricket.rawValue
            playerInfoView.matchDetails = matchDetails

            
            let deatails = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView)
        }
        else if allRounderPlayerArray.count == 2{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails
            
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let details2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            
        }
        else if allRounderPlayerArray.count == 3{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
                        
            let deatails1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: deatails1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = allRounderPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
        }
        else if allRounderPlayerArray.count == 4{
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView4 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            playerInfoView4.playerListArray = totalPlayerArray
            playerInfoView4.selectedGameType = GameType.Cricket.rawValue
            playerInfoView4.matchDetails = matchDetails
                        
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = allRounderPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = allRounderPlayerArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
            allRounderView.addSubview(playerInfoView4)
        }
        else if allRounderPlayerArray.count == 5{
            
            let playerInfoView1 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView2 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView3 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView4 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            let playerInfoView5 = TourneyPlayerPreview(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 8))
            
            playerInfoView1.playerListArray = totalPlayerArray
            playerInfoView1.selectedGameType = GameType.Cricket.rawValue
            playerInfoView1.matchDetails = matchDetails

            playerInfoView2.playerListArray = totalPlayerArray
            playerInfoView2.selectedGameType = GameType.Cricket.rawValue
            playerInfoView2.matchDetails = matchDetails

            playerInfoView3.playerListArray = totalPlayerArray
            playerInfoView3.selectedGameType = GameType.Cricket.rawValue
            playerInfoView3.matchDetails = matchDetails
            
            playerInfoView4.playerListArray = totalPlayerArray
            playerInfoView4.selectedGameType = GameType.Cricket.rawValue
            playerInfoView4.matchDetails = matchDetails
            
            playerInfoView5.playerListArray = totalPlayerArray
            playerInfoView5.selectedGameType = GameType.Cricket.rawValue
            playerInfoView5.matchDetails = matchDetails
            
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = allRounderPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = allRounderPlayerArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = allRounderPlayerArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
            allRounderView.addSubview(playerInfoView4)
            allRounderView.addSubview(playerInfoView5)
        }
        
        totalPointValue.text = String(totalPoint)
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let teamName = firstTeamName + " vs " + secondTeamName
        
        let text = String(format: "Check out my team for %@. Create your team on Letspick. Click here https://Letspick.app.link", teamName)
        
        // set up activity view controller
        let textToShare = [text, containerView.takeScreenshot()] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}
