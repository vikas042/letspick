//
//  AddCashViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 03/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class AddCashViewController: UIViewController, PromotionsViewControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addCashButton: SolidButton!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var bottomView: UIView!
    var txtFieldAmount: UITextField?
    var txtFieldPromo: UITextField?

    lazy var selectedGameType = GameType.Cricket.rawValue
    var leagueDetails: LeagueDetails?
    var matchDetails: MatchDetails?

    lazy var userTeamArray = Array<UserTeamDetails>()
    lazy var categoryName = ""
    lazy var appliedPromocode = ""
    var ticketDetails: TicketDetails?
    lazy var isNeedToGoOnConfirmation = false
    lazy var isValidPromoCode = false
    lazy var amount = Int(UserDetails.sharedInstance.defalutAddCashAmount)
    lazy var promoCodesArray = Array<PromotionCodeDetails>()
        
    override func viewDidLoad() {
        super.viewDidLoad()
                
        addCashButton.setTitle("Add Cash".localized(), for: .normal)
        headerView.headerTitle = "Add Money".localized()
        addCashButton.setTitle("Add Money".localized(), for: .normal)
        tblView.register(UINib(nibName: "AddCashAmountTableViewCell", bundle: nil), forCellReuseIdentifier: "AddCashAmountTableViewCell")

        tblView.register(UINib(nibName: "DepositTicketTableViewCell", bundle: nil), forCellReuseIdentifier: "DepositTicketTableViewCell")
        callGetPromoCodeAPI();
        
        navigationController?.navigationBar.isHidden = true
        bottomView.isHidden = false
        AppHelper.designBottomTabDesing(bottomView)
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        
        if AppHelper.isApplicationRunningOnIphoneX() {
            bottomHeightConstraint.constant = 78.0
            view.layoutIfNeeded()
        }
    }

    @objc func promotionsButtonTapped() {
        if isValidPromoCode{
            isValidPromoCode = false
            appliedPromocode = ""
            self.tblView.reloadData()
            return;
        }
    }
    
    @objc func applyCustomPromoCode() {
        guard let txtField = txtFieldPromo else {
            return;
        }
        
        if txtField.text?.count == 0{
            AppHelper.showToast(message: "Please enter the promo code")
            return
        }
        txtField.resignFirstResponder()
        callApplyPromotionCode(promoCode: txtField.text ?? "")
    }

    
    @objc func add50ButtonTapped() {
        
        guard let txtField = txtFieldAmount else {
            return;
        }
        
        var enteredString = "0"
        isValidPromoCode = false
        if txtField.text?.count != 0{
            enteredString = txtField.text!.convertStringToInt()
        }
        let enterAmount = Int(enteredString)! + 50
        amount = enterAmount
        txtField.text = String(amount!)
        self.tblView.reloadData()
    }
    
    @objc func add100ButtonTapped() {

        guard let txtField = txtFieldAmount else {
            return;
        }
        
        var enteredString = "0"
        isValidPromoCode = false
        if txtField.text?.count != 0{
            enteredString = txtField.text!.convertStringToInt()
        }
        let enterAmount = Int(enteredString)! + 100
        amount = enterAmount
        txtField.text = String(amount!)
        self.tblView.reloadData()
    }
    
    @objc func add500ButtonTapped() {

        guard let txtField = txtFieldAmount else {
            return;
        }
        
        var enteredString = "0"
        isValidPromoCode = false
        if txtField.text?.count != 0{
            enteredString = txtField.text!.convertStringToInt()
        }
        let enterAmount = Int(enteredString)! + 500
        amount = enterAmount
        txtField.text = String(amount!)
        self.tblView.reloadData()
    }
    
    @IBAction func addCashButtonTappedButtonTapped(_ sender: Any) {
        
        guard let txtField = txtFieldAmount else {
            return;
        }

        AppxorEventHandler.logAppEvent(withName: "AddMoneyClicked", info: nil)

        if txtField.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter amount", isErrorMessage: true)
            return;
        }
        
        txtField.resignFirstResponder()
        
        let paymentOptionVC = storyboard?.instantiateViewController(withIdentifier: "PaymentOptionViewController") as? PaymentOptionViewController
        paymentOptionVC?.userTeamArray = userTeamArray
        paymentOptionVC?.categoryName = categoryName
        paymentOptionVC?.ticketDetails = ticketDetails

        paymentOptionVC?.leagueDetails = leagueDetails
        paymentOptionVC?.matchDetails = matchDetails
        paymentOptionVC?.selectedGameType = selectedGameType
        let number = txtField.text!.convertStringToInt()
        if number.count == 0 {
            AppHelper.showAlertViewWithoutMessage(titleMessage: "Please enter the valid amount", isErrorMessage: true)
            return;
        }

        paymentOptionVC?.addCashAmount = number
        if  isValidPromoCode{
            paymentOptionVC?.promoCode = appliedPromocode
        }
        navigationController?.pushViewController(paymentOptionVC!, animated: true)
    }
    
    @IBAction func myAccountButtonTapped(_ sender: Any) {
        let profileVC = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
        profileVC?.isComeFromAddcash = true
        navigationController?.pushViewController(profileVC!, animated: true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if leagueDetails == nil {
            self.navigationController?.popViewController(animated: true)
        }
        else if leagueDetails!.isPrivateLeague {
            let navArray = self.navigationController?.viewControllers
            var isfoundVC = false
            
            for selectPlayerVC in navArray! {
                if self.leagueDetails!.isPrivateLeague{
                    if selectPlayerVC is JoinedLeagueViewController{
                        self.navigationController?.popToViewController(selectPlayerVC, animated: true)
                        isfoundVC = true;
                        break;
                    }
                }
            }
            
            if !isfoundVC{
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func applyPromoCode(promoCode: String) {
//        lblPromocode.text = promoCode
//        lblPromoCodeMessage.isHidden = false
//        removePromoImg.isHidden = false
//        lblApply.isHidden = true
//        isValidPromoCode = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        weak var weakSelf = self

        if textField.tag == 100 {
            if textField.text?.count ?? 0 >= 5 {
                if string.count == 0{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                        weakSelf?.showFormatedEnterNumber()
                    }
                    return true
                }
                return false;
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                weakSelf?.showFormatedEnterNumber()
            }
        }
        
        return true
    }
    
    func showFormatedEnterNumber() {
        guard let txtField = txtFieldAmount else {
            return;
        }

        txtFieldAmount!.text = txtField.text?.convertStringToInt()
    }
    
     func callGetPromoCodeAPI() {
         
        if !AppHelper.isInterNetConnectionAvailable(){
             return;
         }
         
         AppHelper.sharedInstance.displaySpinner()

         let params = ["option": "promo_contents", "user_id": UserDetails.sharedInstance.userID]
         weak var weakSelf = self;
         WebServiceHandler.performPOSTRequest(urlString: kPmomotionUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
             AppHelper.sharedInstance.removeSpinner()
             
             if result != nil{
                 let statusCode = result!["status"]
                 if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                         if let promoArray = response["contents"]?.array{
                             weakSelf?.promoCodesArray = PromotionCodeDetails.getPromoCodesList(dataArray: promoArray)
                             weakSelf?.tblView.reloadData()
                         }
                     }
                 }
             }
             else{
                 AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
             }
         }
     }
    
    func callApplyPromotionCode(promoCode: String)  {

        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        AppxorEventHandler.logAppEvent(withName: "ApplyPromoCodeClicked", info: ["PromoType": promoCode])

        AppHelper.sharedInstance.displaySpinner()
        
        let parameters = ["option": "check", "user_id": UserDetails.sharedInstance.userID, "code": promoCode, "is_mobile": "1", "amount": (txtFieldAmount?.text ?? "0")]
        WebServiceHandler.performPOSTRequest(urlString: kPmomotionUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary {
                        let promoType = response["promotion_type"]?.stringValue
                        if promoType == "4" {
                            let tickets = response["tickets"]?.string ?? ""
                            var message = ""
                            
                            if tickets.count > 0 {
                                        
                                let ticketsIDArray = tickets.components(separatedBy: ",")
                                message = "You will receive \(ticketsIDArray.count) any match ticket on successful deposit"
                                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                                    if "hi" == lang[0]{
                                        message = "आप सफल डिपॉज़िट पर  \(ticketsIDArray.count) एनी मैच टिकट प्राप्त करेंगे"
                                    }
                                }
                            }
                            
                            let customAlertView = CustomAlertView(frame: APPDELEGATE.window!.frame)
                            customAlertView.updateMessageForApplyPromode(message: message)
                            APPDELEGATE.window!.addSubview(customAlertView)
                        }
                    }
                    
                    self.appliedPromocode = promoCode
                    self.isValidPromoCode = true
                    self.tblView.reloadData()
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension AddCashViewController: UITableViewDelegate, UITableViewDataSource{
    
    // MARK:- TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promoCodesArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 310.0
        }
        return 115.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "AddCashAmountTableViewCell") as? AddCashAmountTableViewCell
            
            if cell == nil {
                cell = AddCashAmountTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "AddCashAmountTableViewCell")
            }

            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell?.applyButton.setTitle("Apply".localized(), for: .normal)
            cell?.lblPromocode.text = "Apply Promo Code".localized()
            cell?.lblPromoCodeMessage.text = "Promo Code Applied".localized()
            cell?.lblEnterAmtTitle.text = "Enter Amount".localized()
            cell?.lblPromoCodeMessage.isHidden = true
            cell?.button125.addTarget(self, action: #selector(add50ButtonTapped), for: .touchUpInside)
            cell?.button250.addTarget(self, action: #selector(add100ButtonTapped), for: .touchUpInside)
            cell?.button500.addTarget(self, action: #selector(add500ButtonTapped), for: .touchUpInside)
            cell?.removePromoCodeButton.addTarget(self, action: #selector(promotionsButtonTapped), for: .touchUpInside)
            cell?.applyButton.addTarget(self, action: #selector(applyCustomPromoCode), for: .touchUpInside)

            txtFieldPromo = cell?.promoTxtField;
            cell?.removePromoImg.isHidden = true;
            txtFieldAmount = cell?.txtFieldAmount;
            txtFieldAmount?.text = String(amount ?? 0)
            txtFieldAmount?.delegate = self
            cell?.lblPromoCodeMessage.isHidden = true
            cell?.removePromoImg.isHidden = true
            
            if isValidPromoCode{
                cell?.lblPromoCodeMessage.isHidden = false
                cell?.removePromoImg.isHidden = false
                cell?.lblPromocode.text = appliedPromocode
            }
            
            cell?.promoTxtField.placeholder = "Apply Promo Code here".localized()

            
            return cell!

        }
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "DepositTicketTableViewCell") as? DepositTicketTableViewCell
            
            if cell == nil {
                cell = DepositTicketTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "DepositTicketTableViewCell")
            }

            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            let details = promoCodesArray[indexPath.row - 1]

            cell?.lblTitle.text = details.title
            cell?.lblPromocode.text = details.promocode
            cell?.lblMessage.text = details.subTitle
            cell?.lblExpiryDate.text = details.endDate

            cell?.lblExpireOnTitle.text = "Expires on".localized()
            cell?.lblCodeTitle.text = "Code".localized()
            cell?.joinButton.setTitle("Apply".localized(), for: .normal)
            cell?.joinButton.isHidden = false

            cell?.imgView.image = UIImage(named: "BlueTicketCardIcon")
            if appliedPromocode == details.promocode{
                cell?.imgView.image = UIImage(named: "OrangeTicketCardIcon")
                cell?.joinButton.isHidden = true
            }
            
            AppHelper.showShodowOnCellsView(innerView: cell!.innerView)
            
            return cell!
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            return
        }
        let promoDetails = promoCodesArray[indexPath.row - 1]
        AppxorEventHandler.logAppEvent(withName: "AvailablePromoClicked", info: ["PromoType": promoDetails.promocode])

        callApplyPromotionCode(promoCode: promoDetails.promocode)
    }
    
}
