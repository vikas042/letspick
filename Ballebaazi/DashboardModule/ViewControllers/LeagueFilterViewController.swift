//
//  LeagueFilterViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 21/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit


protocol LeagueFilterViewControllerDelegate {
    func applyLeagueFilter(rangArray: Array<Any>, poolArray: Array<Any>, teamsArray: Array<Any>, leaguesArray: Array<String>, totalLeagueTypeArray: Array<String>)
    func resetLeagueFilter()
}

class LeagueFilterViewController: UIViewController {

    var selectedBorderColor = UIColor(red: 34/255, green: 114/255, blue: 70/255, alpha: 1)
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerView: CustomNavigationBar!    
    @IBOutlet weak var resetFilterButton: CustomBorderButton!
    @IBOutlet weak var applyFilterButton: SolidButton!
    
    var delegate: LeagueFilterViewControllerDelegate?
    var gameType = GameType.Cricket.rawValue
    
    var entryRangeArray = [
        ["title": "Below pts 10".localized(), "isSelected": false ],
        ["title": "pts 11 - pts 49", "isSelected": false ],
        ["title": "pts 50 - pts 200", "isSelected": false ],
        ["title": "pts 201 - pts 499", "isSelected": false ],
        ["title": "pts 500 - pts 2499", "isSelected": false ],
        ["title": "2500 above".localized(), "isSelected": false ]]

    var poolRangeArray = [
        ["title": "Below pts 10".localized(), "isSelected": false ],
        ["title": "pts 11 - pts 99", "isSelected": false ],
        ["title": "pts 100 - pts 999", "isSelected": false ],
        ["title": "pts 1000 - pts 9999", "isSelected": false ],
        ["title": "pts 10,000 - pts 99,999", "isSelected": false ],
        ["title": "pts 1,00,000 above".localized(), "isSelected": false ]]

    var teamsRangArray = [
        ["title": "2", "isSelected": false ],
        ["title": "3 - 10", "isSelected": false ],
        ["title": "11 - 100", "isSelected": false ],
        ["title": "100 - 499", "isSelected": false ],
        ["title": "500 - 1000", "isSelected": false ],
        ["title": "1001 & above".localized(), "isSelected": false ]]

    var leagueTypeArray = [
        ["title": "Single Entry".localized(), "isSelected": false ],
        ["title": "Multi Entry".localized(), "isSelected": false ],
        ["title": "Single Winner".localized(), "isSelected": false ],
        ["title": "Multi Winners".localized(), "isSelected": false ],
        ["title": "Confirmed League".localized(), "isSelected": false ]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "League Filters".localized()
        collectionView.register(UINib(nibName: "LeagueFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueFilterCollectionViewCell")
        collectionView.register(UINib(nibName: "LeagueFilterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "LeagueFilterCollectionReusableView")
        
        resetFilterButton.setTitle("Reset Filter".localized(), for: .normal)
        applyFilterButton.setTitle("Apply Filter".localized(), for: .normal)
    }
    
    func showSelectedLeagueFilter(rangArray: Array<Any>, poolArray: Array<Any>, teamsArray: Array<Any>, leaguesArray: Array<String>){

        for filerDetails in rangArray{
            if let tempDetails = filerDetails as? [String: Float] {
                let minValue = tempDetails["min"]!
                let maxValue = tempDetails["max"]!
                
                if minValue >= 0 && maxValue <= 10 {
                    entryRangeArray.remove(at: 0)
                    entryRangeArray.insert(["title": "Below pts 10".localized(), "isSelected": true ], at: 0)
                }
                else if minValue >= 11 && maxValue <= 49 {
                    entryRangeArray.remove(at: 1)
                    entryRangeArray.insert(["title": "pts 11 - pts 49", "isSelected": true ], at: 1)
                }
                else if minValue >= 50 && maxValue <= 200 {
                    entryRangeArray.remove(at: 2)
                    entryRangeArray.insert(["title": "pts 50 - pts 200", "isSelected": true ], at: 2)
                }
                else if minValue >= 201 && maxValue <= 499 {
                    entryRangeArray.remove(at: 3)
                    entryRangeArray.insert(["title": "pts 201 - pts 499", "isSelected": true ], at: 3)
                }
                else if minValue >= 500 && maxValue <= 2499 {
                    entryRangeArray.remove(at: 4)
                    entryRangeArray.insert(["title": "pts 500 - pts 2499", "isSelected": true ], at: 4)
                }
                else if minValue >= 2500 {
                    entryRangeArray.remove(at: 5)
                    entryRangeArray.insert(["title": "pts 2500 above".localized(), "isSelected": true ], at: 5)
                }
            }
        }

        for filerDetails in poolArray{
            if let tempDetails = filerDetails as? [String: Float] {
                let minValue = tempDetails["min"]!
                let maxValue = tempDetails["max"]!
                
                if minValue >= 0 && maxValue <= 10 {
                    poolRangeArray.remove(at: 0)
                    poolRangeArray.insert(["title": "Below pts 10".localized(), "isSelected": true ], at: 0)
                }
                else if minValue >= 11 && maxValue <= 99 {
                    poolRangeArray.remove(at: 1)
                    poolRangeArray.insert(["title": "pts 11 - pts 99", "isSelected": true ], at: 1)
                }
                else if minValue >= 100 && maxValue <= 999 {
                    poolRangeArray.remove(at: 2)
                    poolRangeArray.insert(["title": "pts 100 - pts 999", "isSelected": true ], at: 2)
                }
                else if minValue >= 1000 && maxValue <= 9999 {
                    poolRangeArray.remove(at: 3)
                    poolRangeArray.insert(["title": "pts 1000 - pts 9999", "isSelected": true ], at: 3)
                }
                else if minValue >= 10000 && maxValue <= 99999 {
                    poolRangeArray.remove(at: 4)
                    poolRangeArray.insert(["title": "pts 10,000 - pts 99,999", "isSelected": true ], at: 4)
                }
                else if minValue >= 100000 {
                    poolRangeArray.remove(at: 5)
                    poolRangeArray.insert(["title": "pts 1,00,000 above".localized(), "isSelected": true ], at: 5)
                }
            }
        }
        
        for filerDetails in teamsArray{
            if let tempDetails = filerDetails as? [String: Float] {
                let minValue = tempDetails["min"]!
                let maxValue = tempDetails["max"]!
                
                if minValue == 2 {
                    teamsRangArray.remove(at: 0)
                    teamsRangArray.insert(["title": "2", "isSelected": true ], at: 0)
                }
                else if minValue >= 3 && maxValue <= 10 {
                    teamsRangArray.remove(at: 1)
                    teamsRangArray.insert(["title": "3 - 10", "isSelected": true ], at: 1)
                }
                else if minValue >= 11 && maxValue <= 100 {
                    teamsRangArray.remove(at: 2)
                    teamsRangArray.insert(["title": "11 - 100", "isSelected": true ], at: 2)
                }
                else if minValue >= 100 && maxValue <= 499 {
                    teamsRangArray.remove(at: 3)
                    teamsRangArray.insert(["title": "100 - 499", "isSelected": true ], at: 3)
                }
                else if minValue >= 500 && maxValue <= 1000 {
                    teamsRangArray.remove(at: 4)
                    teamsRangArray.insert(["title": "500 - 1000", "isSelected": true ], at: 4)
                }
                else if minValue >= 1000 {
                    teamsRangArray.remove(at: 5)
                    teamsRangArray.insert(["title": "1001 & above".localized(), "isSelected": true ], at: 5)
                }
            }
        }
        
        
        for filerStr in leaguesArray{
            if filerStr == "Single Entry".localized(){
                leagueTypeArray.remove(at: 0)
                leagueTypeArray.insert(["title": "Single Entry".localized(), "isSelected": true ], at: 0)
            }else if filerStr == "Multi Entry".localized(){
                leagueTypeArray.remove(at: 1)
                leagueTypeArray.insert(["title": "Multi Entry".localized(), "isSelected": true ], at: 1)
            }else if filerStr == "Single Winner".localized(){
                leagueTypeArray.remove(at: 2)
                leagueTypeArray.insert(["title": "Single Winner".localized(), "isSelected": true ], at: 2)
            }else if filerStr == "Multi Winners".localized(){
                leagueTypeArray.remove(at: 3)
                leagueTypeArray.insert(["title": "Multi Winners".localized(), "isSelected": true ], at: 3)
            }
            else if filerStr == "Confirmed League".localized(){
                leagueTypeArray.remove(at: 4)
                leagueTypeArray.insert(["title": "Confirmed League".localized(), "isSelected": true ], at: 4)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if AppHelper.isApplicationRunningOnIphoneX(){
             bottomViewHeightConstraint.constant = 65;
             bottomView.layoutIfNeeded()
         }
         else{
             view.layoutIfNeeded()
         }
        collectionView.reloadData()
    }
    
    @IBAction func resetFilterButtonTapped(_ sender: Any) {
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        delegate?.resetLeagueFilter()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applyFilterButtonTapped(_ sender: Any) {
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        var rangArray = Array<Any>()
        var poolArray = Array<Any>()
        var teamsArray = Array<Any>()
        var leaguesArray = Array<String>()

        for (index, details) in entryRangeArray.enumerated() {
            let isSelected = details["isSelected"] as! Bool
            if isSelected {
                if index == 0 {
                    let tempDict:[String: Float] = ["min": 0, "max": 10]
                    rangArray.append(tempDict)
                }
                else if index == 1 {
                    let tempDict:[String: Float] = ["min": 11, "max": 49]
                    rangArray.append(tempDict)
                }
                else if index == 2 {
                    let tempDict:[String: Float] = ["min": 50, "max": 200]
                    rangArray.append(tempDict)
                }
                else if index == 3 {
                    let tempDict:[String: Float] = ["min": 201, "max": 499]
                    rangArray.append(tempDict)
                }
                else if index == 4 {
                    let tempDict:[String: Float] = ["min": 500, "max": 2499]
                    rangArray.append(tempDict)
                }
                else if index == 5 {
                    let tempDict:[String: Float] = ["min": 2500, "max": 10000000000]
                    rangArray.append(tempDict)
                }
            }
        }
        
        for (index, details) in poolRangeArray.enumerated() {
            let isSelected = details["isSelected"] as! Bool
            if isSelected {
                if index == 0 {
                    let tempDict:[String: Float] = ["min": 0, "max": 10]
                    poolArray.append(tempDict)
                }
                else if index == 1 {
                    let tempDict:[String: Float] = ["min": 11, "max": 99]
                    poolArray.append(tempDict)
                }
                else if index == 2 {
                    let tempDict:[String: Float] = ["min": 100, "max": 999]
                    poolArray.append(tempDict)
                }
                else if index == 3 {
                    let tempDict:[String: Float] = ["min": 1000, "max": 9999]
                    poolArray.append(tempDict)
                }
                else if index == 4 {
                    let tempDict:[String: Float] = ["min": 10000, "max": 99999]
                    poolArray.append(tempDict)
                }
                else if index == 5 {
                    let tempDict:[String: Float] = ["min": 100000, "max": 10000000000]
                    poolArray.append(tempDict)
                }
            }
        }

        for (index, details) in teamsRangArray.enumerated() {
            let isSelected = details["isSelected"] as! Bool
            if isSelected {
                if index == 0 {
                    let tempDict:[String: Float] = ["min": 2, "max": 2]
                    teamsArray.append(tempDict)
                }
                else if index == 1 {
                    let tempDict:[String: Float] = ["min": 3, "max": 10]
                    teamsArray.append(tempDict)
                }
                else if index == 2 {
                    let tempDict:[String: Float] = ["min": 11, "max": 100]
                    teamsArray.append(tempDict)
                }
                else if index == 3 {
                    let tempDict:[String: Float] = ["min": 100, "max": 499]
                    teamsArray.append(tempDict)
                }
                else if index == 4 {
                    let tempDict:[String: Float] = ["min": 500, "max": 1000]
                    teamsArray.append(tempDict)
                }
                else if index == 5 {
                    let tempDict:[String: Float] = ["min": 1001, "max": 1000000]
                    teamsArray.append(tempDict)
                }
            }
        }
        
        var isMultiAndSingleBothEntyCount = 0
        var isMultiAndSingleBothWinnerCount = 0
        var templeaguesArray = Array<String>()

        for (_, details) in leagueTypeArray.enumerated() {
            let isSelected = details["isSelected"] as! Bool
                
            if isSelected {
                let title = details["title"] as! String
                if title == "Single Entry".localized() {
                    isMultiAndSingleBothEntyCount = isMultiAndSingleBothEntyCount + 1
                } else if title == "Multi Entry".localized() {
                    isMultiAndSingleBothEntyCount = isMultiAndSingleBothEntyCount + 1
                }
                else if title == "Single Winner".localized() {
                    isMultiAndSingleBothWinnerCount = isMultiAndSingleBothWinnerCount + 1
                } else if title == "Multi Winners".localized() {
                    isMultiAndSingleBothWinnerCount = isMultiAndSingleBothWinnerCount + 1
                }
                leaguesArray.append(title)
                templeaguesArray.append(title)
            }
        }
        
        if isMultiAndSingleBothEntyCount == 2 {
            templeaguesArray.removeAll { (tempStr) -> Bool in
                tempStr == "Single Entry".localized() || tempStr == "Multi Entry".localized()
            }
        }
        
        if isMultiAndSingleBothWinnerCount == 2 {
            templeaguesArray.removeAll { (tempStr) -> Bool in
                tempStr == "Single Winner".localized() || tempStr == "Multi Winners".localized()
            }
        }
        
        delegate?.applyLeagueFilter(rangArray: rangArray, poolArray: poolArray, teamsArray: teamsArray, leaguesArray: templeaguesArray, totalLeagueTypeArray: leaguesArray)
        var sportType = "Cricket"
        if gameType == GameType.Football.rawValue{
            sportType = "Football"
        }
        AppxorEventHandler.logAppEvent(withName: "ApplyFilterClicked", info: ["SportType": sportType])

        navigationController?.popViewController(animated: true)
    }
    
}

extension LeagueFilterViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return entryRangeArray.count
        }else if section == 1{
            return poolRangeArray.count
        }else if section == 2{
            return teamsRangArray.count
        }else if section == 3{
            return leagueTypeArray.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 45)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LeagueFilterCollectionReusableView", for: indexPath) as! LeagueFilterCollectionReusableView
            if indexPath.section == 0 {
                headerView.lblHeader.text = "ENTRY RANGE".localized()
            }else if indexPath.section == 1 {
                headerView.lblHeader.text = "POOL RANGE".localized()
            }else if indexPath.section == 2 {
                headerView.lblHeader.text = "NUMBER OF TEAMS".localized()
            }else if indexPath.section == 3 {
                headerView.lblHeader.text = "TYPES OF LEAGUES".localized()
            }

            return headerView

        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth: CGFloat = 100
        
        if indexPath.section == 0{
            let title = entryRangeArray[indexPath.item]["title"] as! String
            cellWidth = title.width(withConstrainedHeight: 20, font: UIFont(name: "OpenSans", size: 15)!)
        }else if indexPath.section == 1{
            let title = poolRangeArray[indexPath.item]["title"] as! String
            cellWidth = title.width(withConstrainedHeight: 20, font: UIFont(name: "OpenSans", size: 15)!)
        }else if indexPath.section == 2{
            let title = teamsRangArray[indexPath.item]["title"] as! String
            cellWidth = title.width(withConstrainedHeight: 20, font: UIFont(name: "OpenSans", size: 15)!)
        }else if indexPath.section == 3{
            let title = leagueTypeArray[indexPath.item]["title"] as! String
            cellWidth = title.width(withConstrainedHeight: 20, font: UIFont(name: "OpenSans", size: 15)!)
        }

        if cellWidth < 20 {
            cellWidth = 20
        }
        return CGSize(width: cellWidth + 40, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueFilterCollectionViewCell", for: indexPath) as! LeagueFilterCollectionViewCell
        cell.innerView.layer.cornerRadius = 7
        cell.innerView.layer.borderWidth = 1
        
        cell.innerView.layer.borderColor = UIColor.gray.cgColor
        cell.lblTitle.textColor = UIColor.gray
        cell.lblTitle.text = ""
        if indexPath.section == 0{
            let details = entryRangeArray[indexPath.item]
            let title = details["title"] as? String
            let isSelected = details["isSelected"] as! Bool
            if isSelected {
                cell.innerView.layer.borderColor = selectedBorderColor.cgColor
                cell.lblTitle.textColor = selectedBorderColor
            }
            cell.lblTitle.text = title
        }else if indexPath.section == 1{
            let details = poolRangeArray[indexPath.item]
            let title = details["title"] as? String
            let isSelected = details["isSelected"] as! Bool
            if isSelected {
                cell.innerView.layer.borderColor = selectedBorderColor.cgColor
                cell.lblTitle.textColor = selectedBorderColor
            }
            cell.lblTitle.text = title
        }else if indexPath.section == 2{
            let details = teamsRangArray[indexPath.item]
            let title = details["title"] as? String
            let isSelected = details["isSelected"] as! Bool
            if isSelected {
                cell.innerView.layer.borderColor = selectedBorderColor.cgColor
                cell.lblTitle.textColor = selectedBorderColor
            }
            cell.lblTitle.text = title
        }else if indexPath.section == 3{
            let details = leagueTypeArray[indexPath.item]
            let title = details["title"] as? String
            let isSelected = details["isSelected"] as! Bool
            if isSelected {
                cell.innerView.layer.borderColor = selectedBorderColor.cgColor
                cell.lblTitle.textColor = selectedBorderColor

            }
            cell.lblTitle.text = title
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let details = entryRangeArray[indexPath.item]
            let title = details["title"] as! String
            let isSelected = details["isSelected"] as! Bool
            entryRangeArray.remove(at: indexPath.row)

            if isSelected {
                entryRangeArray.insert(["title": title, "isSelected": false], at: indexPath.row)
            }
            else{
                entryRangeArray.insert(["title": title, "isSelected": true], at: indexPath.row)
            }
        }else if indexPath.section == 1{
            let details = poolRangeArray[indexPath.item]
             let title = details["title"] as! String
             let isSelected = details["isSelected"] as! Bool
             poolRangeArray.remove(at: indexPath.row)

             if isSelected {
                 poolRangeArray.insert(["title": title, "isSelected": false], at: indexPath.row)
             }
             else{
                 poolRangeArray.insert(["title": title, "isSelected": true], at: indexPath.row)
             }
            
        }else if indexPath.section == 2{
            let details = teamsRangArray[indexPath.item]
             let title = details["title"] as! String
             let isSelected = details["isSelected"] as! Bool
             teamsRangArray.remove(at: indexPath.row)

             if isSelected {
                 teamsRangArray.insert(["title": title, "isSelected": false], at: indexPath.row)
             }
             else{
                 teamsRangArray.insert(["title": title, "isSelected": true], at: indexPath.row)
             }
            
        }else if indexPath.section == 3{
            let details = leagueTypeArray[indexPath.item]
             let title = details["title"] as! String
             let isSelected = details["isSelected"] as! Bool
             leagueTypeArray.remove(at: indexPath.row)

             if isSelected {
                 leagueTypeArray.insert(["title": title, "isSelected": false], at: indexPath.row)
             }
             else{
                 leagueTypeArray.insert(["title": title, "isSelected": true], at: indexPath.row)
             }
        }
        
        collectionView.reloadData()
        
    }
}
