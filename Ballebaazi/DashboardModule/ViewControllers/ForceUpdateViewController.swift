//
//  ForceUpdateViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/12/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class ForceUpdateViewController: UIViewController {

    @IBOutlet weak var lblIntroducing: UILabel!
    @IBOutlet weak var lblNewChaneges: UILabel!
    @IBOutlet weak var updateButton: SolidButton!
    @IBOutlet weak var lblNewUpdate: UILabel!
    @IBOutlet weak var tblView: UITableView!
    var versionHistoryArray = Array<VersionDetails>()
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.register(UINib(nibName: "VersionDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "VersionDetailsTableViewCell")
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.estimatedRowHeight = 40

        callGetVersionHistory()
        
    }
    
    @IBAction func updateButtonTapped(_ sender: Any) {
        openAppStore()
    }
    
    func openAppStore() {
        if let url = URL(string: UserDetails.sharedInstance.updateAppUrl),
            UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:]) { (opened) in
                    if(opened){
                        print("App Store Opened")
                    }
                }
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func callGetVersionHistory() {
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }

        AppHelper.sharedInstance.displaySpinner()

        WebServiceHandler.performPOSTRequest(urlString: kSupport, andParameters: ["option": "update"], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let status = result!["status"]
                if status == "200"{
                    if let dataArray = result!["response"]?.array {
                        
                        self.versionHistoryArray =  VersionDetails.getAlliOSVersionHistory(dataArray: dataArray)
                        self.tblView.reloadData()
                    }
                }
            }
        }
    }
  
    

}



extension ForceUpdateViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return versionHistoryArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "VersionDetailsTableViewCell") as? VersionDetailsTableViewCell
        
        if cell == nil {
            cell = VersionDetailsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "VersionDetailsTableViewCell")
        }
        
        let details = versionHistoryArray[indexPath.row]
        cell!.configData(details: details)
        return cell!

    }
    

}
