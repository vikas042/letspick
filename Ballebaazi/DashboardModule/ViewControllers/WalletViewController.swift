//
//  WalletViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {
    
    @IBOutlet weak var lblUnusedTitle: UILabel!
    @IBOutlet weak var lblBounusTitle: UILabel!
    @IBOutlet weak var lblWinningTitle: UILabel!
    @IBOutlet weak var lblTakeOutWinnings: UILabel!
    @IBOutlet weak var lblBalanceAmount: UILabel!
    @IBOutlet weak var lblMyTransaction: UILabel!
    @IBOutlet weak var lblTransactionList: UILabel!
    @IBOutlet weak var lblWithDrawMoney: UILabel!
    @IBOutlet weak var addMoneyButton: UIButton!
    @IBOutlet weak var lblAvailableBalance: UILabel!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var navigationHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cashView: UIView!
    @IBOutlet weak var lblUnsedCash: UILabel!
    @IBOutlet weak var lblBonus: UILabel!
    @IBOutlet weak var lblWinnings: UILabel!
    @IBOutlet weak var userImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "Wallet".localized()
        addMoneyButton.layer.cornerRadius = 4.0
        addMoneyButton.layer.borderColor = UIColor(red: 0/255, green: 136.0/255, blue: 64.0/255, alpha: 1).cgColor
        addMoneyButton.layer.borderWidth = 1
        addMoneyButton.isHidden = true
        configData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblUnusedTitle.text = "Unused".localized()
        lblBounusTitle.text = "Bonus".localized()
        lblWinningTitle.text = "Winnings".localized()
        lblBalanceAmount.text = "Wallet Points".localized()
        lblMyTransaction.text = "My Transactions".localized()
        lblTransactionList.text = "List of all your past transactions".localized()
        lblTakeOutWinnings.text = "Take out your winnings".localized()
        lblWithDrawMoney.text = "Manage Points".localized()
        addMoneyButton.setTitle("Add_Money".localized(), for: .normal)
        configData()
    }
    
    func configData()  {
        lblBonus.text = "pts" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.bonusCash);
        lblUnsedCash.text = "pts" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.unusedCash);
        lblWinnings.text = "pts" + AppHelper.formatDecimalNumberString(digites: UserDetails.sharedInstance.withdrawableCredits);
        
        if UserDetails.sharedInstance.totalCredits.count > 0{
            lblAvailableBalance.text = "pts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: UserDetails.sharedInstance.totalCredits)
        }
        else{
            lblAvailableBalance.text = "pts 0"
        }
    }
    
    @IBAction func withdrawMoneyButtonTapped(_ sender: Any) {
        let transactionVC = storyboard?.instantiateViewController(withIdentifier: "WithDrawHistoryViewController") as! WithDrawHistoryViewController
        navigationController?.pushViewController(transactionVC, animated: true)
    }

    @IBAction func myTransactionButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "TransactionsClicked", info: ["Source": "Wallet"])

        let transactionVC = storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        navigationController?.pushViewController(transactionVC, animated: true)
    }
    
    @IBAction func addCashButtonTapped(_ sender: Any) {
        let addcashVC = storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
        navigationController?.pushViewController(addcashVC, animated: true)
    }
}

