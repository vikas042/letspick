//
//  PromotionsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/12/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

protocol PromotionsViewControllerDelegate {
    func applyPromoCode(promoCode: String)
}

class PromotionsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var announcementView: AnnouncementView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var applyButton: UIButton!
    
    @IBOutlet weak var txtFieldPromoCodeViewHeightConstraint: NSLayoutConstraint!
    
    var promoCodesArray = Array<PromotionCodeDetails>()
    var delegate: PromotionsViewControllerDelegate?
    var isFromSetting = false
    var isFromNotification = false

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var placeholderpromotionImage: UIImageView!
    @IBOutlet weak var txtFieldPromoCode: UITextField!
    @IBOutlet weak var txtFieldView: UIView!
    
    var selectedAmount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.estimatedRowHeight = 160
        lblNoDataFound.text = "NoPromoCode".localized()
        headerView.headerTitle = "Promo Code".localized()
        txtFieldPromoCode.placeholder = "Enter Promo Code".localized();
        applyButton.setTitle("Apply".localized(), for: .normal)
        tblView.register(UINib(nibName: "PromotionsTableViewCell", bundle: nil), forCellReuseIdentifier: "PromotionsTableViewCell")
        navigationController?.navigationBar.isHidden = true
        placeholderpromotionImage.isHidden = true
        if isFromNotification {
            navigationController?.navigationBar.tag = 2000
        }
        callGetPromoCodeAPI()
    }

    override func viewWillAppear(_ animated: Bool) {
        if !isFromSetting{
            txtFieldView.isHidden = false
            txtFieldPromoCodeViewHeightConstraint.constant = 70;
            self.view.layoutIfNeeded()
        }
    }
    
    func callApplyPromotionCode(promoCode: String)  {
        txtFieldPromoCode.resignFirstResponder()
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let parameters = ["option": "check", "user_id": UserDetails.sharedInstance.userID, "code": promoCode, "is_mobile": "1", "amount": String(selectedAmount)]
        WebServiceHandler.performPOSTRequest(urlString: kPmomotionUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    self.delegate?.applyPromoCode(promoCode: promoCode)
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callGetPromoCodeAPI() {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()

        let params = ["option": "promo_contents", "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self;
        WebServiceHandler.performPOSTRequest(urlString: kPmomotionUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    weakSelf?.lblNoDataFound.isHidden = true
                    weakSelf?.placeholderpromotionImage.isHidden = true
                    if let response = result!["response"]?.dictionary{
                        if let announcement = response["announcement"]?.dictionary{
                            let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                            
                            self.announcementViewHeightConstraint.constant = 50.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: details.message)
                        }
                        else{
                            self.announcementViewHeightConstraint.constant = 0.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: "")
                        }
                        
                        
                        
                        if let promoArray = response["contents"]?.array{
                            weakSelf?.promoCodesArray = PromotionCodeDetails.getPromoCodesList(dataArray: promoArray)
                            weakSelf?.tblView.reloadData()
                            if weakSelf?.promoCodesArray.count == 0{
                                weakSelf?.lblNoDataFound.isHidden = false
                                weakSelf?.placeholderpromotionImage.isHidden = false
                            }
                            else{
                                weakSelf?.lblNoDataFound.isHidden = true
                                weakSelf?.placeholderpromotionImage.isHidden = true
                            }
                        }
                    }
                }
                else{
                    weakSelf?.lblNoDataFound.isHidden = false
                    weakSelf?.placeholderpromotionImage.isHidden = false
                    weakSelf?.lblNoDataFound.text = message
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
            
        }
    }
    
    @IBAction func walletButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "WalletIconClicked", info: nil)

        let walletVC = storyboard?.instantiateViewController(withIdentifier: "WalletViewController")
        navigationController?.pushViewController(walletVC!, animated: true)
    }
    
    @IBAction func applyCustomPromoCodeButtonTapped(_ sender: Any) {
        if txtFieldPromoCode.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter the promo code.", isErrorMessage: true)
            return;
        }
        callApplyPromotionCode(promoCode: txtFieldPromoCode.text!)
    }
    
    //MARK:- TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promoCodesArray.count;
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 165.0
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "PromotionsTableViewCell") as? PromotionsTableViewCell
        
        if cell == nil {
            cell = PromotionsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PromotionsTableViewCell")
        }
        cell?.selectionStyle = .none
        let promoDetails = promoCodesArray[indexPath.row]
        if isFromSetting{
            cell?.applyButton.isHidden = true
        }
        else{
            cell?.applyButton.isHidden = false
        }
        cell?.configData(details: promoDetails)
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isFromSetting {
            let promoDetails = promoCodesArray[indexPath.row]
            callApplyPromotionCode(promoCode: promoDetails.promocode)
        }
    }

    
}
