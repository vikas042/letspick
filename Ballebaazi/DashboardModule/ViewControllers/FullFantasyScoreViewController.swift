//
//  FullFantasyScoreViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 21/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class FullFantasyScoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var headerView: CustomNavigationBar!
    
    lazy var totalWicketKeeperArray = Array<PlayerDetails>()
    lazy var totalBatsmanArray = Array<PlayerDetails>()
    lazy var totalBowlerArray = Array<PlayerDetails>()
    lazy var totalAllRounderArray = Array<PlayerDetails>()
    lazy var playerListArray = Array<PlayerDetails>()

    var fantasyType = ""
    var matchKey = ""
    var firstTeamName = ""
    var secondTeamName = ""
    var playerGender = ""
    var matchDetails: MatchDetails?

    @IBOutlet weak var teamTwoButton: UIButton!
    @IBOutlet weak var teamOneButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var tblView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "FullFantasyTableViewCell", bundle: nil), forCellReuseIdentifier: "FullFantasyTableViewCell")
        headerView.headerTitle = firstTeamName + " vs " + secondTeamName
        teamTwoButton.setTitle(secondTeamName, for: .normal)
        teamOneButton.setTitle(firstTeamName, for: .normal)

        var subTitle = ""
            
        if matchDetails?.isMatchClosed ?? false {
            if matchDetails!.matchStatus == "completed"{
                subTitle = "Completed".localized()
            }
            else if matchDetails!.matchStatus == "started"{
                subTitle = "Live".localized()
            }
            else{
                subTitle = "Leagues Closed".localized()
            }
        }
        headerView.updateHeaderTitles(title: firstTeamName + " vs " + secondTeamName, subTitle: subTitle)
        
        callGetTeamPlayersDetails()
    }
    
    @IBAction func teamTwoButtonTapped(_ sender: Any) {
        return;
        allButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        teamOneButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        teamTwoButton.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        
        
        teamTwoButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        teamOneButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        
        
        let wicketKeeperPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let batsmanPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let bowlerPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let allRounderPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)
        
        let filteredKeeperArray = wicketKeeperPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == secondTeamName
        }
        
        let filteredBatsmanArray = batsmanPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == secondTeamName
        }
        
        let filteredBowlerArray = bowlerPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == secondTeamName
        }
        
        let filteredAllRounderArray = allRounderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == secondTeamName
        }
        
        totalWicketKeeperArray = filteredKeeperArray
        totalBatsmanArray = filteredBatsmanArray
        totalBowlerArray = filteredBowlerArray
        totalAllRounderArray = filteredAllRounderArray
    }
    
    @IBAction func teamOneButtonTapped(_ sender: Any) {
        return;
        allButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        teamOneButton.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        teamTwoButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        
        
        teamOneButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        teamTwoButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        
        
        let wicketKeeperPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let batsmanPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let bowlerPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let allRounderPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)
        
        let filteredKeeperArray = wicketKeeperPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == firstTeamName
        }
        
        let filteredBatsmanArray = batsmanPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == firstTeamName
        }
        
        let filteredBowlerArray = bowlerPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == firstTeamName
        }
        
        let filteredAllRounderArray = allRounderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == firstTeamName
        }
        
        totalWicketKeeperArray = filteredKeeperArray
        totalBatsmanArray = filteredBatsmanArray
        totalBowlerArray = filteredBowlerArray
        totalAllRounderArray = filteredAllRounderArray
    }
    
    @IBAction func allTeamButtonTapped(_ sender: Any) {
        return;
        allButton.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        teamOneButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        teamTwoButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        
        
        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        teamOneButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        teamTwoButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        
        totalWicketKeeperArray = getSelectedTypePlayerList(selectedRow: 0)
        totalBatsmanArray = getSelectedTypePlayerList(selectedRow: 1)
        totalBowlerArray = getSelectedTypePlayerList(selectedRow: 2)
        totalAllRounderArray = getSelectedTypePlayerList(selectedRow: 3)
    }

    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        
        if selectedRow == 0{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
            })
        }
        
        if playersArray != nil{
            return playersArray!
        }
        
        return []
    }
    
    
    private func callGetTeamPlayersDetails()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "full_scoreboard", "match_key": matchKey, "team_number": "1", "user_id": UserDetails.sharedInstance.userID, "type": "", "fantasy_type": fantasyType]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kSocrescardUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let playerArray = result!["response"]?.array{
                        
                        weakSelf?.playerListArray = PlayerDetails.getPlayerDetailsForScoreArray(responseArray: playerArray)
                        if self.playerGender == "F" {
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: weakSelf!.playerListArray)
                        }

                        weakSelf?.totalWicketKeeperArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 0)
                        weakSelf?.totalBatsmanArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 1)
                        weakSelf?.totalBowlerArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 2)
                        weakSelf?.totalAllRounderArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 3)
                        weakSelf?.tblView.reloadData()
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            return 4
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            return 3
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            return 3
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            return 3
        }
        else if totalAllRounderArray.count > 0 && totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0{
            return 3
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            return 2
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            return 2
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            return 2
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            return 2
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            return 2
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            return 2
        }
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        label.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255, blue: 240.0/255, alpha: 1)
        label.text = getPlayerType(section: section)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.textColor = UIColor(red: 127.0/255.0, green: 132.0/255, blue: 134.0/255, alpha: 1)
        label.font = UIFont(name: "OpenSans-Semibold", size: 12)
//        label.backgroundColor = UIColor.white
        return label;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
            else if section == 2 {
                return totalAllRounderArray.count
            }
            else if section == 3 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
            else if section == 2 {
                return totalAllRounderArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
            else if section == 3 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
            else if section == 2 {
                return totalBowlerArray.count
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
            else if section == 2 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBatsmanArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            if section == 0 {
                return totalWicketKeeperArray.count
            }
            else if section == 1 {
                return totalBowlerArray.count
            }
            
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalBatsmanArray.count
            }
            else if section == 1 {
                return totalAllRounderArray.count
            }
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray.count
            }
            else if section == 1 {
                return totalBowlerArray.count
            }
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalAllRounderArray.count
            }
            else if section == 1 {
                return totalBowlerArray.count
            }
        }
        else if totalWicketKeeperArray.count > 0{
            return totalWicketKeeperArray.count
        }
        else if totalBatsmanArray.count > 0{
            return totalBatsmanArray.count
        }
        else if totalAllRounderArray.count > 0{
            return totalAllRounderArray.count
        }
        else if totalBowlerArray.count > 0{
            return totalBowlerArray.count
        }
        
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "FullFantasyTableViewCell") as? FullFantasyTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "FullFantasyTableViewCell") as? FullFantasyTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.contentView.backgroundColor = UIColor.white
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        
        if fantasyType == "1" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Classic.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if fantasyType == "2" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Batting.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if fantasyType == "3" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Bowling.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if fantasyType == "4" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Reverse.rawValue, gameType: GameType.Cricket.rawValue)
        }
        else if fantasyType == "5" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Wizard.rawValue, gameType: GameType.Cricket.rawValue)
        }

        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)

        //if (fantasyType == "1") || (fantasyType == "4") || (fantasyType == "5"){
        if (fantasyType == "1") || (fantasyType == "5"){
            let teamScoresView = ClassicTeamScoresView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
            teamScoresView.configData(details: playerInfo, matchDetails: matchDetails)
            APPDELEGATE.window!.addSubview(teamScoresView)
            teamScoresView.showAnimation()
        }
        else if fantasyType == "2"{
            let teamScoresView = BattingTeamScoreView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
            teamScoresView.configData(details: playerInfo, matchDetails: matchDetails)
            APPDELEGATE.window!.addSubview(teamScoresView)
            teamScoresView.showAnimation()
        }
        else if fantasyType == "3"{
            let teamScoresView = BowlingTeamScoreView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
            teamScoresView.configData(details: playerInfo, matchDetails: matchDetails)
            APPDELEGATE.window!.addSubview(teamScoresView)
            teamScoresView.showAnimation()
        }else if fantasyType == "4"{
            let teamScoresView = TeamScoreInstructionView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
            teamScoresView.selectedFantasyType = FantasyType.Reverse.rawValue
            teamScoresView.configData(details: playerInfo, matchDetails: matchDetails)
            APPDELEGATE.window!.addSubview(teamScoresView)
            teamScoresView.showAnimation()
        }
    }
    
    func getPlayerType(section: Int) -> String {
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                return "Batsman".localized()
            }
            else if section == 2 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }
            else if section == 3 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                
                return "Bowler".localized().localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                return "Batsman".localized()
            }
            else if section == 2 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }
            
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                return "Batsman".localized()
            }
            else if section == 2 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }
            
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }
            else if section == 2 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                
                return "Bowler".localized().localized()
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                
                return "Batsman".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                return "All Rounder".localized()
            }
            else if section == 2 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                return "Bowler".localized().localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                
                return "Batsman".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                return "All Rounder".localized()
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            if section == 0 {
                if totalWicketKeeperArray.count > 1{
                    return "Wicket Keepers".localized()
                }
                
                return "Wicket Keeper".localized()
            }
            else if section == 1 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                
                return "Bowler".localized().localized()
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                
                return "Batsman".localized()
            }
            else if section == 1 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalBatsmanArray.count > 1{
                    return "Batsmen".localized()
                }
                
                return "Batsman".localized()
            }
            else if section == 1 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                return "Bowler".localized().localized()
            }
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                if totalAllRounderArray.count > 1{
                    return "All Rounders".localized()
                }
                
                return "All Rounder".localized()
            }
            else if section == 1 {
                if totalBowlerArray.count > 1{
                    return "Bowlers".localized()
                }
                return "Bowler".localized().localized()
            }
        }
        else if totalWicketKeeperArray.count > 0{
            if totalWicketKeeperArray.count > 1{
                return "Wicket Keepers".localized()
            }
            
            return "Wicket Keeper".localized()
        }
        else if totalBatsmanArray.count > 0{
            if totalBatsmanArray.count > 1{
                return "Batsmen".localized()
            }
            return "Batsman".localized()
        }
        else if totalAllRounderArray.count > 0{
            if totalAllRounderArray.count > 1{
                return "All Rounders".localized()
            }
            return "All Rounder".localized()
        }
        else if totalBowlerArray.count > 0{
            if totalBowlerArray.count > 1{
                return "Bowlers".localized()
            }
            return "Bowler".localized().localized()
        }
        return ""
    }
    
    
    func getPlayerDetails(section: Int, index: Int) -> PlayerDetails {
        if (totalWicketKeeperArray.count > 0) && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
            else if section == 2 {
                return totalAllRounderArray[index]
            }
            else if section == 3 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
            else if section == 2 {
                return totalAllRounderArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
            else if section == 3 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
            else if section == 2 {
                return totalBowlerArray[index]
            }
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
            else if section == 2 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBatsmanArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBatsmanArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalAllRounderArray.count > 0 {
            
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0 && totalBowlerArray.count > 0 {
            if section == 0 {
                return totalWicketKeeperArray[index]
            }
            else if section == 1 {
                return totalBowlerArray[index]
            }
            
        }
        else if totalBatsmanArray.count > 0 && totalAllRounderArray.count > 0{
            if section == 0 {
                return totalBatsmanArray[index]
            }
            else if section == 1 {
                return totalAllRounderArray[index]
            }
        }
        else if totalBatsmanArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalBatsmanArray[index]
            }
            else if section == 1 {
                return totalBowlerArray[index]
            }
        }
        else if totalAllRounderArray.count > 0 && totalBowlerArray.count > 0{
            if section == 0 {
                return totalAllRounderArray[index]
            }
            else if section == 1 {
                return totalBowlerArray[index]
            }
        }
        else if totalWicketKeeperArray.count > 0{
            return totalWicketKeeperArray[index]
        }
        else if totalBatsmanArray.count > 0{
            return totalBatsmanArray[index]
        }
        else if totalAllRounderArray.count > 0{
            return totalAllRounderArray[index]
        }
        else if totalBowlerArray.count > 0{
            return totalBowlerArray[index]
        }
        return PlayerDetails()
    }
}
