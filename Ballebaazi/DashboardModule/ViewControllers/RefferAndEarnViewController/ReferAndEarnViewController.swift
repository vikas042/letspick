//
//  ReferAndEarnViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ReferAndEarnViewController: UIViewController {

    @IBOutlet weak var lblReferEarnTitle: UILabel!
    
    @IBOutlet weak var lblMessage2: UILabel!
    @IBOutlet weak var copyButton: CustomBorderButton!
    @IBOutlet weak var shareButton: SolidButton!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shareButton.isHidden = true;
        copyButton.isHidden = true;
        lblReferEarnTitle.text = "Refer & Earn".localized()
        copyButton.setTitle("Copy Link".localized(), for: .normal)
        shareButton.setTitle("Share Link".localized(), for: .normal)
        
        navigationController?.navigationBar.isHidden = true
        lblMessage.text = ""
        callGetReferAndEarnTextAPI()
    }

    
    func callGetReferAndEarnTextAPI() {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters: Parameters = ["option": "get_refer_text", "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    weakSelf?.shareButton.isHidden = false;
                    weakSelf?.copyButton.isHidden = false;

                    if let response = result!["response"]?.dictionary{
                        let referralMessage = response["text"]?.string ?? ""
                        let noteStr = response["note"]?.string ?? ""
                        weakSelf?.lblMessage2.text = referralMessage.replacingOccurrences(of: "%s", with: (UserDetails.sharedInstance.referralCode ?? ""))
                        weakSelf?.lblMessage.text = "NOTE: " + noteStr;
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    @IBAction func copyLinkButtonTapped(_ sender: Any) {
        let link = String(format: "https://Letspick.app.link/refer?refer_code=%@", UserDetails.sharedInstance.referralCode ?? "")
        UIPasteboard.general.string = link
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
    
        var userName = UserDetails.sharedInstance.name
        if userName.count == 0 {
            userName = UserDetails.sharedInstance.userName
        }
        
        let text = String(format: "%@ has sent you an invite to Letspick. Use code %@ to register and get Rs %@ as bonus.\nhttps://Letspick.app.link/refer?refer_code=%@", userName, UserDetails.sharedInstance.referralCode ?? "", UserDetails.sharedInstance.referralAmount ,UserDetails.sharedInstance.referralCode ?? "")
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
