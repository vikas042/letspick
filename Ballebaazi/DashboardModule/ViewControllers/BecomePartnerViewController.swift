//
//  BecomePartnerViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit


class BecomePartnerViewController: UIViewController {
    
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var playNowButton: SolidButton!
    @IBOutlet weak var lblHowToJoin: UILabel!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCongratsMessage: UILabel!
    @IBOutlet weak var lblCongratulations: UILabel!
    @IBOutlet weak var lblPlayAnyLeagueMessage: UILabel!
    @IBOutlet weak var lblActionMessage: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var becomePartnerView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var playNowView: UIView!
    @IBOutlet weak var playerAnyLeagueButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var lblPPTitle: UILabel!
    @IBOutlet weak var joinNowButton: UIButton!
    
    @IBOutlet weak var lblWillBackDes: UILabel!
    @IBOutlet weak var lblWillBackTitle: UILabel!
    @IBOutlet weak var shoutdownView: UIView!
    @IBOutlet weak var applicationClosedButtton: UIButton!

    var bannersArray = Array<BannerDetails>()
    var watingStatus = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell")
        playerAnyLeagueButton.isSelected = false
        lblHowToJoin.text = "How to Join".localized()
        joinNowButton.layer.cornerRadius = 5
        
        lblPlayAnyLeagueMessage.text = "Play any League".localized()
        playNowButton.setTitle("Play Now".localized(), for: .normal)
        self.joinNowButton.backgroundColor = UIColor(red: 3/255, green: 167/255, blue: 80/255, alpha: 1)
        joinNowButton.setTitle("Become a partner".localized(), for: .normal)

        lblActionMessage.text = "One More Step to go. Join a League to be eligible".localized()
        lblCongratulations.text = "Congratulations!".localized()
        lblCongratsMessage.text = "You are eligible to be a part of the partnership program".localized()
        headerView.headerTitle = "Partnership Program".localized()
        
        applicationClosedButtton.setTitle("Application Closed".localized(), for: .normal)
        lblWillBackTitle.text = "We will be Right Back!".localized()
        lblWillBackDes.text = "WillRighBackPP".localized()

        
        callGetBannerDetailsAPI()
    }
    
    func callGetBannerDetailsAPI() {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "get_banner", "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kPartnershipURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary {
                        let partnership_program_status = response["partnership_program_status"]?.stringValue
                        
                        if let bannersArray = response["banners"]?.array {
                            weakSelf?.bannersArray = BannerDetails.getPromoBannerDetails(dataArray: bannersArray)
                            weakSelf?.collectionView.reloadData()
                        }

                        
                        if let partnershipDict = response["what_is_pp"]?.dictionary {
                            let title = partnershipDict["title"]?.string ?? ""
                            weakSelf?.lblPPTitle.text = title
                            let partnershipDescription = partnershipDict["body"]?.string ?? ""
                            weakSelf?.lblMessage.showHTML(htmlString: partnershipDescription, isCenterAligment: true)
                            DispatchQueue.main.async {
                                weakSelf!.lblMessage.sizeToFit()
                                if weakSelf!.bannersArray.count == 0{
                                    weakSelf!.collectionViewHeightConstraint.constant = 0
                                    weakSelf!.scrollViewHeightConstraint.constant = weakSelf!.lblMessage.frame.height + 380
                                }
                                else{
                                    weakSelf!.scrollViewHeightConstraint.constant = weakSelf!.lblMessage.frame.height + 500
                                }
                                weakSelf!.containerView.layoutIfNeeded()
                            }
                        }
                        

                        if let leaguesDict = response["leagues"]?.dictionary {
                            if let isWating = leaguesDict["is_waiting"]?.boolValue {
                                self.watingStatus = isWating
                                if isWating {
                                    
                                    UserDefaults.standard.set(true, forKey: "pp_isWatingForApproval")
                                    UserDefaults.standard.synchronize()
                                    
                                    self.joinNowButton.setTitle("Waiting for approval".localized(), for: .normal)
                                    self.joinNowButton.backgroundColor = UIColor(red: 3/255, green: 167/255, blue: 80/255, alpha: 1)

                                }
                            }
                            if let isPlayedLeague = leaguesDict["no_leagues"]?.boolValue {
                                if isPlayedLeague {
                                    weakSelf?.playNowView.isHidden = false
                                    weakSelf?.becomePartnerView.isHidden = true
                                }
                                else{
                                    weakSelf?.playNowView.isHidden = true
                                    weakSelf?.becomePartnerView.isHidden = false
                                }
                            }
                        }
                        
                        var isWaiting = false
                        if let leaguesDict = response["leagues"]?.dictionary {
                            isWaiting = leaguesDict["is_waiting"]?.boolValue ?? false
                        }

                        weakSelf?.shoutdownView.isHidden = true
                        if ((partnership_program_status == "0") && !isWaiting) && !((UserDetails.sharedInstance.userAffilate == "1") || (UserDetails.sharedInstance.userAffilate == "2")) {
                            weakSelf?.shoutdownView.isHidden = false
                        }
                    }
                    weakSelf?.containerView.isHidden = false
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callBecomePartnerAPI() {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "become_partner", "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kPartnershipURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    self.watingStatus = true
                    UserDefaults.standard.set(true, forKey: "pp_isWatingForApproval")
                    UserDefaults.standard.synchronize()
                    self.joinNowButton.setTitle("Waiting for approval".localized(), for: .normal)
                    self.joinNowButton.backgroundColor = UIColor(red: 3/255, green: 167/255, blue: 80/255, alpha: 1)
                }
                else if (statusCode == "400") && (message == "Already a partner"){
                    let partnershipProgramVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "PartnershipProgramViewController") as? PartnershipProgramViewController
                    if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                        navigationVC.pushViewController(partnershipProgramVC!, animated: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    @IBAction func playNowButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "PlayNowClicked", info: nil)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playNowFromPartnership"), object:  nil)
        navigationController?.popViewController(animated: true);
    }
    
   
    @IBAction func joinNowButtonTapped(_ sender: Any) {
        if watingStatus {
            return
        }
        AppxorEventHandler.logAppEvent(withName: "JoinNowClicked", info: nil)
        callBecomePartnerAPI()
    }

}

extension BecomePartnerViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 20, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
        let details = bannersArray[indexPath.row]
        cell.configBannerData(details: details)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            let details = bannersArray[indexPath.item]
            
            if details.redirectType == "2" {
                let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
                navVC.pushViewController(leagueVC, animated: true)
            }
            else if details.redirectType == "3" {
                
                if details.websiteUrl.count == 0{
                    return;
                }
                let webviewVC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webviewVC.urlString = details.websiteUrl
                navVC.pushViewController(webviewVC, animated: true)
            }
            else if details.redirectType == "4" {
                let playerView = YoutubeVideoPlayerView(frame: APPDELEGATE.window!.frame)
                playerView.videoID = details.videoUrl
                playerView.playView()
                APPDELEGATE.window!.addSubview(playerView)
            }
            else if details.redirectType == "6" {
                let promoCodeVC = storyboard.instantiateViewController(withIdentifier: "PromotionsViewController") as! PromotionsViewController
                promoCodeVC.isFromSetting = true
                navVC.pushViewController(promoCodeVC, animated: true)
            }
            else if details.redirectType == "7" {
                let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
                navVC.pushViewController(addCashVC, animated: true)
            }
            else if details.redirectType == "8" {
                
                let howToPlayVC = storyboard.instantiateViewController(withIdentifier: "HowToPlayViewController") as! HowToPlayViewController
                navVC.pushViewController(howToPlayVC, animated: true)
            }
        }
    }
    
}
