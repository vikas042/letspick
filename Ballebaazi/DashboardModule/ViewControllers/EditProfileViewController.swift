
//
//  EditProfileViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit


class EditProfileViewController: UIViewController {

    @IBOutlet weak var updateProfile: SolidButton!
    
    @IBOutlet weak var lblDateOfBirth: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmailID: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblPinCode: UILabel!
    @IBOutlet weak var lblReferalCode: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var txtFieldDob: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldUserName: UITextField!
    @IBOutlet weak var txtFieldReferralCode: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var txtFieldAddress: UITextField!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var txtFieldPinCode: UITextField!
    @IBOutlet weak var fieldsContainerView: UIView!

    let datePicker: UIDatePicker = UIDatePicker()
    var selectedDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        maleButton.isSelected = true
        txtFieldDob.inputView = datePicker
                
        let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.datePickerMode = .date
//        datePicker.locale = Locale(identifier: "en_US")
        datePicker.maximumDate = date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        txtFieldReferralCode.isUserInteractionEnabled = false
        navigationController?.navigationBar.isHidden = true
//        AppHelper.designBottomTabDesing(bottomView)
        updateUserDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        lblName.text = "name".localized()
        lblUserName.text = "Username".localized()
        lblEmailID.text = "email_id".localized()
        lblPhoneNumber.text = "Mobile_number".localized()
        lblGender.text = "gender".localized()
        lblDateOfBirth.text = "dob".localized()
        lblReferalCode.text = "referralCode".localized()
        updateProfile.setTitle("update_Profile".localized(), for: .normal)
        maleButton.setTitle("Male".localized(), for: .normal)
        maleButton.setTitle("Male".localized(), for: .selected)
        femaleButton.setTitle("Female".localized(), for: .normal)
        femaleButton.setTitle("Female".localized(), for: .selected)
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
//        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
//            let locatStr = lang[0]
//            if locatStr == "hi"{
//                dateFormatter.locale = Locale(identifier: locatStr)
//            }
//            else{
//                dateFormatter.locale = Locale(identifier: "en_US")
//            }
//        }
        dateFormatter.locale = Locale(identifier: "en_US")

        selectedDate = dateFormatter.string(from: sender.date)
        txtFieldDob.text = AppHelper.getRequiredFormattedDate(dateString: selectedDate)
    }

    func updateUserDetails()  {
        
        if UserDetails.sharedInstance.panVerified{
            txtFieldName.isUserInteractionEnabled = false
            txtFieldDob.isUserInteractionEnabled = false
        }

        txtFieldName.text = UserDetails.sharedInstance.name
        selectedDate = UserDetails.sharedInstance.dob 
        txtFieldDob.text = AppHelper.getRequiredFormattedDate(dateString: selectedDate)
        if txtFieldDob.text?.count == 0 {
            txtFieldDob.isUserInteractionEnabled = true
        }
        
        if UserDetails.sharedInstance.phoneNumber.count == 0{
            txtFieldPhoneNumber.isUserInteractionEnabled = true
        }
        else{
            txtFieldPhoneNumber.isUserInteractionEnabled = false
        }
        
        if UserDetails.sharedInstance.name.count == 0{
            txtFieldName.isUserInteractionEnabled = true
        }
        else{
            txtFieldName.isUserInteractionEnabled = false
        }
        
        if UserDetails.sharedInstance.emailAdress.count > 0{
            if UserDetails.sharedInstance.emailVerified{
                txtFieldEmail.isUserInteractionEnabled = false
            }
            txtFieldEmail.text = UserDetails.sharedInstance.emailAdress
        }
        
        if UserDetails.sharedInstance.userName.count > 0{
            txtFieldUserName.text = UserDetails.sharedInstance.userName
            if UserDetails.sharedInstance.isUserNameEditable{
                txtFieldUserName.isUserInteractionEnabled = true
            }
            else{
                txtFieldUserName.isUserInteractionEnabled = false
            }
        }
        
        if UserDetails.sharedInstance.gender == "F"{
            maleButton.isSelected = false
            femaleButton.isSelected = true
        }
        
        txtFieldReferralCode.text = UserDetails.sharedInstance.referralCode
        txtFieldPhoneNumber.text = UserDetails.sharedInstance.phoneNumber
        txtFieldAddress.text = UserDetails.sharedInstance.address
        txtFieldCity.text = UserDetails.sharedInstance.city
        txtFieldPinCode.text = UserDetails.sharedInstance.zipcode
    }
    
    @IBAction func maleButtonTapped(_ sender: Any) {
        maleButton.isSelected = true
        femaleButton.isSelected = false
    }
    
    
    @IBAction func femaleButtonTapped(_ sender: Any) {
        maleButton.isSelected = false
        femaleButton.isSelected = true
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if txtFieldName.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterNameMsg".localized(), isErrorMessage: true)
            return;
        }
        else if txtFieldDob.text?.count == 0 {
            AppHelper.showAlertView(message: "SelectDOB".localized(), isErrorMessage: true)
            return;
        }
        else if txtFieldEmail.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterEmailMsg".localized(), isErrorMessage: true)
            return;
        }
        else if AppHelper.isNotValidEmail(email: txtFieldEmail.text!){
            AppHelper.showAlertView(message: "EnterValidEmailMsg".localized(), isErrorMessage: true)
            return
        }
        else if txtFieldUserName.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterUserNameMsg".localized(), isErrorMessage: true)
            return;
        }
        else if txtFieldPhoneNumber.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterPhoneMsg".localized(), isErrorMessage: true)
            return;
        }

        AppxorEventHandler.logAppEvent(withName: "UpdateProfileClicked", info: nil)

        callUpdateUserDetailsAPI()
    }

    
    func callUpdateUserDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        var gender = "M"
        
        if maleButton.isSelected{
            gender = "M"
        }
        else{
            gender = "F"
        }
        
        let params = ["option": "edit_profile", "user_id": UserDetails.sharedInstance.userID, "name": txtFieldName.text!, "email": txtFieldEmail.text!, "dob": selectedDate, "gender": gender, "phone": txtFieldPhoneNumber.text!, "username": (txtFieldUserName.text ?? "")]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""

                if statusCode == "200"{
                    if let response = result!["response"]{
                        
                        if let dob = response.dictionary!["dob"]?.string{
                            UserDetails.sharedInstance.dob = dob
                        }
                        
                        if let name = response.dictionary!["name"]?.string{
                            UserDetails.sharedInstance.name = name
                        }
                        
                        if let city = response.dictionary!["city"]?.string{
                            UserDetails.sharedInstance.city = city
                        }
                        
                        if let usernameEditable = response.dictionary!["username_editable"]?.string{
                            if usernameEditable == "yes"{
                                UserDetails.sharedInstance.isUserNameEditable = true
                                weakSelf!.txtFieldUserName.isUserInteractionEnabled = true
                            }
                            else{
                                UserDetails.sharedInstance.isUserNameEditable = false
                                weakSelf!.txtFieldUserName.isUserInteractionEnabled = false
                            }
                        }
                        else
                        {
                            UserDetails.sharedInstance.isUserNameEditable = false
                            weakSelf!.txtFieldUserName.isUserInteractionEnabled = false
                        }
                        
                        if let state = response.dictionary!["state"]?.string{
                            UserDetails.sharedInstance.state = state
                        }
                        
                        if let gender = response.dictionary!["gender"]?.string{
                            UserDetails.sharedInstance.gender = gender
                        }
                        
                        if let email = response.dictionary!["email"]?.string{
                            UserDetails.sharedInstance.emailAdress = email;
                        }
                        
                        if let username = response.dictionary!["username"]?.string{
                            
                            UserDetails.sharedInstance.userName = username;
                        }
                        
                        if let referralCode = response.dictionary!["referral_code"]?.string{
                            UserDetails.sharedInstance.referralCode = referralCode;
                        }
                        
                        if let phone = response.dictionary!["phone"]?.string{
                            UserDetails.sharedInstance.phoneNumber = phone;
                        }
                        
                        if let address = response.dictionary!["address"]?.string{
                            UserDetails.sharedInstance.address = address;
                        }

                        let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
                            weakSelf!.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        MixPanelEventsDetails.setPeopleProperties(isFromEdit: false)
       
                        var profileData = [name: UserDetails.sharedInstance.name, identity: UserDetails.sharedInstance.userID, email_address: UserDetails.sharedInstance.emailAdress, device: PLATFORM_iPHONE, user_name: UserDetails.sharedInstance.userName, "Pan Status": UserDetails.sharedInstance.pan_verified, "Bank Status": UserDetails.sharedInstance.BankStatus, "Gender": UserDetails.sharedInstance.gender, custome_name: UserDetails.sharedInstance.name] as [String: Any]
                        
                        let dateFormatter: DateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        dateFormatter.locale = Locale(identifier: "en_US")

                        if let date = dateFormatter.date(from: UserDetails.sharedInstance.dob){
                            profileData[dob] = date
                        }
                        CleverTapEventDetails.updateProfile(updateProfile: profileData)

                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
