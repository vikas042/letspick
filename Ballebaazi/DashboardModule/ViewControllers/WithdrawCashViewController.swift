//
//  WithdrawCashViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 19/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit


var kSelectedColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)

protocol WithdrawCashViewControllerDeleagte {
    func refreshList()
}

class WithdrawCashViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var selectedIndex = 1000
    var showAddBankCell = false
    var isOneOptionAvailableForWithdraw = false
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    
    var amountTxtField: UITextField?

    var delegate:WithdrawCashViewControllerDeleagte?
    var withdrawOptionArray = Array<WithdrawOptionDetails>()
    var selectecWithdrawlDetails: WithdrawOptionDetails?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        amountTxtField?.delegate = self
        tblView.register(UINib(nibName: "AddBankTableViewCell", bundle: nil), forCellReuseIdentifier: "AddBankTableViewCell")
        tblView.register(UINib(nibName: "AddPaytmTableViewCell", bundle: nil), forCellReuseIdentifier: "AddPaytmTableViewCell")
        tblView.register(UINib(nibName: "WithdrawTableViewCell", bundle: nil), forCellReuseIdentifier: "WithdrawTableViewCell")
        tblView.register(UINib(nibName: "AddBankDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "AddBankDetailsTableViewCell")
        tblView.register(UINib(nibName: "AvailableWithdrawTableViewCell", bundle: nil), forCellReuseIdentifier: "AvailableWithdrawTableViewCell")
        
        headerView.headerTitle = "Withdraw".localized()
        
        NotificationCenter.default.addObserver(self, selector: #selector(bankDetailsApprovedNotification(notification:)), name: Notification.Name("bankDetailsApprovedNotification"), object: nil)
        showPanNotVerifiedPopup()
        
        if withdrawOptionArray.count == 1 {
            selectecWithdrawlDetails = withdrawOptionArray[0]
            if selectecWithdrawlDetails?.withdrawType == WithdrawOptionType.Bank.rawValue {
                selectedIndex = 0
            }
            else if selectecWithdrawlDetails?.withdrawType == WithdrawOptionType.Paytm.rawValue {
                selectedIndex = 1
            }

        }
        for details in withdrawOptionArray {
            if details.bankDetails != nil{
                isOneOptionAvailableForWithdraw = true
                break
            }
            else if details.is_paytm_linked == "1"{
                isOneOptionAvailableForWithdraw = true
                break
            }
        }
    }
    
    func showPanNotVerifiedPopup() {
                
        if !UserDetails.sharedInstance.panVerified || !UserDetails.sharedInstance.emailVerified {
            let overlayMessage = PanNotVerifiedOverLayview(frame: APPDELEGATE.window!.frame)
            APPDELEGATE.window?.addSubview(overlayMessage)
            overlayMessage.showAnimation()
        }
    }
    
    @objc func bankDetailsApprovedNotification(notification: Notification) {
        
        for details in self.withdrawOptionArray {
            if details.withdrawType == WithdrawOptionType.Bank.rawValue {
                if notification.object is BankDetails {
                    details.bankDetails = notification.object as? BankDetails
                    showAddBankCell = false
                    self.isOneOptionAvailableForWithdraw = true
                    tblView.reloadData()
                }
                break;
            }
        }

        if withdrawOptionArray.count > 0 {
            let details = withdrawOptionArray[0]
            if notification.object is BankDetails {
                details.bankDetails = notification.object as? BankDetails
                showAddBankCell = false
                tblView.reloadData()
            }
        }
    }
    
    func showShodowOnView(innerView: UIView)  {
        
        innerView.layer.cornerRadius = 13.0
        innerView.layer.shadowColor = UIColor(red: 78.0/255, green: 84.0/255, blue: 90.0/255, alpha: 0.5).cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        innerView.layer.shadowOpacity = 0.35
        innerView.layer.shadowRadius = 3
        innerView.layer.masksToBounds = false
    }
        
    @objc func withdrawalPoliciesButtonTapped() {
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
        privacyPolicyVC?.selectedType = SelectedWebViewType.WithdrawalPolicies.rawValue
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.pushViewController(privacyPolicyVC!, animated: true)
        }
    }

    @objc func withdrawButtonTapped() {

        if amountTxtField?.text?.count == 0 {
            AppHelper.showAlertViewWithoutMessage(titleMessage: "Please enter amount".localized(), isErrorMessage: true)
            return;
        }
        
        let number = amountTxtField?.text!.convertStringToInt()
        if number?.count == 0 {
            AppHelper.showAlertViewWithoutMessage(titleMessage: "Please enter the valid amount".localized(), isErrorMessage: true)
            return;
        }

        guard let details = selectecWithdrawlDetails  else {
            AppHelper.showAlertViewWithoutMessage(titleMessage: "Please select withdraw option".localized(), isErrorMessage: true)
            return
        }


        let withdrawableMinCredits = Float(details.min_amount) ?? 0
        let withdrawableMaxCredits = Float(details.max_amount) ?? 0

        if Int(number!) ?? 0 >  Int(withdrawableMaxCredits){
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    AppHelper.showAlertViewWithoutMessage(titleMessage: "विद्ड्रॉअल की राशि pts " + details.max_amount + "के बराबर या कम होनी चाहिए", isErrorMessage: true)
                    return;
                }
            }
            AppHelper.showAlertViewWithoutMessage(titleMessage: "Withdraw amount must be equal or less than pts " + details.max_amount, isErrorMessage: true)
            return;
        }
        else if Int(number!) ?? 0 <  Int(withdrawableMinCredits){
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    AppHelper.showAlertViewWithoutMessage(titleMessage: "विद्ड्रॉअल की राशि pts " + details.min_amount + "के बराबर या अधिक होनी चाहिए", isErrorMessage: true)
                    return;
                }
            }
            AppHelper.showAlertViewWithoutMessage(titleMessage: "Withdraw amount must be equal or greater than pts " + details.min_amount, isErrorMessage: true)
            return;
        }

        AppxorEventHandler.logAppEvent(withName: "WithdrawMoneyClicked", info: nil)
        callWithdrawCashAPI(amount: number!)
    }
    
    func callWithdrawCashAPI(amount: String)  {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        amountTxtField?.resignFirstResponder()
        
        guard let details = selectecWithdrawlDetails  else {
            AppHelper.showAlertViewWithoutMessage(titleMessage: "Please select withdraw option".localized(), isErrorMessage: true)
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
   
        let params = ["option": "withdraw_cash_v1", "user_id": UserDetails.sharedInstance.userID, "amount": amount, "type": "transfer", "enable_tds": "1", "gateway_type": details.instrument_type]
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    let customAlertView = CustomAlertView(frame: APPDELEGATE.window!.frame)
                    customAlertView.updateMessage(title: "rquest_submitted".localized(), message: "withdrawal_processed".localized())
                    APPDELEGATE.window!.addSubview(customAlertView)
                    customAlertView.closePopupViewBlock(complationBlock: { (status) in
                        self.navigationController?.popViewController(animated: true)
                        self.delegate?.refreshList()
                    })
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    
    func callLinkPayTMWalletAPI()  {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }

        let params = ["option": "paytm_activate", "user_id": UserDetails.sharedInstance.userID]
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary {
                        self.isOneOptionAvailableForWithdraw = true
                        let paytmLinked = response["is_paytm_linked"]?.string ?? ""
                        for details in self.withdrawOptionArray {
                            if details.withdrawType == WithdrawOptionType.Paytm.rawValue {
                                details.is_paytm_linked = paytmLinked
                                break;
                            }
                        }

                        self.tblView.reloadData()
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        weak var weakSelf = self
        
        if textField.text?.count ?? 0 >= 7 {
            
            if string.count == 0{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                    weakSelf?.showFormatedEnterNumber()
                }
                return true
            }
            return false;
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            weakSelf?.showFormatedEnterNumber()
        }
        
        return true
    }
    
    func showFormatedEnterNumber() {
        amountTxtField?.text = amountTxtField?.text?.convertStringToInt()
    }
    
    
    //MARK:- TableView Data Source and Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if withdrawOptionArray.count == 0 {
            return 1
        }
        if isOneOptionAvailableForWithdraw {
            if withdrawOptionArray.count == 3 {
                return withdrawOptionArray.count + 1
            }
            return withdrawOptionArray.count + 2
        }
        return withdrawOptionArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (withdrawOptionArray.count == 0) || !isOneOptionAvailableForWithdraw{
            if indexPath.row == 0 {
                return 100.0
            }
        }

        if indexPath.row == 0 {
            return 260.0
        }
        else if indexPath.row == withdrawOptionArray.count + 1 {
            return 100.0
        }
        else{
            let details = withdrawOptionArray[indexPath.row - 1]
            if details.withdrawType == WithdrawOptionType.Bank.rawValue {
                if showAddBankCell {
                    return 400.0
                }
            }
            else{
                if details.withdrawType == WithdrawOptionType.Paytm.rawValue {
                    return 110.0
                }
            }
            return 100.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            return getAvailableCell(indexPath: indexPath)
        }
        else if indexPath.row == withdrawOptionArray.count + 1 {
            if !UserDetails.sharedInstance.panVerified || !UserDetails.sharedInstance.emailVerified {
                showPanNotVerifiedPopup()
                return UITableViewCell();
            }

            return getWithdrawButtonCell(indexPath: indexPath)
        }
        else{
            if withdrawOptionArray.count == 3 {
                if indexPath.row == withdrawOptionArray.count {
                    if !UserDetails.sharedInstance.panVerified || !UserDetails.sharedInstance.emailVerified {
                        showPanNotVerifiedPopup()
                        return UITableViewCell();
                    }

                    return getWithdrawButtonCell(indexPath: indexPath)
                }
                else{
                    let details = withdrawOptionArray[indexPath.row - 1]
                    if details.withdrawType == WithdrawOptionType.Bank.rawValue {
                        if showAddBankCell {
                            return getAddBankDetailsInputFormCell(indexPath: indexPath)
                        }
                        else{
                            return getShowBankDetailsCell(indexPath: indexPath, details: details)
                        }
                    }
                    else{
                        if details.withdrawType == WithdrawOptionType.Paytm.rawValue {
                            return getAddpaymentCell(indexPath: indexPath, details: details)
                        }
                    }

                }
            }
            else{
                let details = withdrawOptionArray[indexPath.row - 1]
                if details.withdrawType == WithdrawOptionType.Bank.rawValue {
                    if showAddBankCell {
                        return getAddBankDetailsInputFormCell(indexPath: indexPath)
                    }
                    else{
                        return getShowBankDetailsCell(indexPath: indexPath, details: details)
                    }
                }
                else{
                    if details.withdrawType == WithdrawOptionType.Paytm.rawValue {
                        return getAddpaymentCell(indexPath: indexPath, details: details)
                    }
                }
            }
            
        }

        return UITableViewCell();
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.row == 0) || (indexPath.row == withdrawOptionArray.count + 1) {
            return;
        }
        
        if !UserDetails.sharedInstance.panVerified || !UserDetails.sharedInstance.emailVerified {
            showPanNotVerifiedPopup()
            return;
        }
        
        showAddBankCell = false
        
        let details = withdrawOptionArray[indexPath.row - 1]
        if details.withdrawType == WithdrawOptionType.Bank.rawValue {
            if details.bankDetails != nil {
                selectecWithdrawlDetails = details
                selectedIndex = 0
            }
            else{
                selectecWithdrawlDetails = nil
                showAddBankCell = true
            }
        }
        else{
            if details.withdrawType == WithdrawOptionType.Paytm.rawValue {
                if details.is_paytm_linked == "1" {
                    selectedIndex = 1
                    selectecWithdrawlDetails = details
                }
                else{
                    
                    
                    let permissionPopup = PermissionView(frame: APPDELEGATE.window!.frame)
                    APPDELEGATE.window?.addSubview(permissionPopup)
                    permissionPopup.showAnimation()
                    permissionPopup.updateMessage(title: "Link PayTM".localized(), message: "LinkPaytmConfirmation".localized())
                    permissionPopup.updateButtonTitle(noButtonTitle: "Cancel".localized(), yesButtonTitle: "Confirm".localized())

                    permissionPopup.noButtonTappedBlock { (status) in
                        
                    }
                    
                    permissionPopup.yesButtonTappedBlock { (status) in
                        self.selectecWithdrawlDetails = nil
                        self.callLinkPayTMWalletAPI()
                    }
                }
            }
        }
        
        tblView.reloadData()
    }
    
    // MARK:- Get Cells
    func getAvailableCell(indexPath: IndexPath) -> AvailableWithdrawTableViewCell {
        var cell = tblView.dequeueReusableCell(withIdentifier: "AvailableWithdrawTableViewCell") as? AvailableWithdrawTableViewCell
        
        if cell == nil {
            cell = AvailableWithdrawTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "AvailableWithdrawTableViewCell")
        }
        cell!.selectionStyle = .none
        amountTxtField = cell!.amountTxtField
        cell!.lblAvailableAmt.text = "Available Points".localized()
        cell!.lblEnterWithdrawAmt.text = "Enter Withdraw Amount".localized()
        cell!.lblAvailableBalance.text = "pts" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: UserDetails.sharedInstance.withdrawableCredits);
        showShodowOnView(innerView: cell!.containerView)
        cell!.containerView.isHidden = false

        if (withdrawOptionArray.count == 0) || !isOneOptionAvailableForWithdraw{
            cell!.containerView.isHidden = true
        }
        return cell!
    }
    
    func getWithdrawButtonCell(indexPath: IndexPath) -> WithdrawTableViewCell {
        var cell = tblView.dequeueReusableCell(withIdentifier: "WithdrawTableViewCell") as? WithdrawTableViewCell
        
        if cell == nil {
            cell = WithdrawTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "WithdrawTableViewCell")
        }
        
        cell?.selectionStyle = .none
        
        cell!.withdrawPolicy.setTitle("Withdrawal Policies".localized(), for: .normal)
        cell!.withdrawButton.setTitle("Withdraw".localized(), for: .normal)
        cell!.withdrawButton.addTarget(self, action: #selector(withdrawButtonTapped), for: .touchUpInside)
        cell!.withdrawPolicy.addTarget(self, action: #selector(withdrawalPoliciesButtonTapped), for: .touchUpInside)

        return cell!;
    }

    
    func getAddpaymentCell(indexPath: IndexPath, details: WithdrawOptionDetails) -> AddPaytmTableViewCell {
        
        var cell = tblView.dequeueReusableCell(withIdentifier: "AddPaytmTableViewCell") as? AddPaytmTableViewCell
        
        if cell == nil {
            cell = AddPaytmTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "AddPaytmTableViewCell")
        }
        
        cell!.innerView.layer.borderColor = kSelectedColor.cgColor
        cell?.selectionStyle = .none
        AppHelper.showShodowOnCellsView(innerView: cell!.innerView)

        if details.is_paytm_linked == "1" {
            cell!.addPaytmView.isHidden = true
            cell!.paytmDetailsView.isHidden = false
            cell!.lblPaytm.text = "Paytm Wallet".localized()
            cell!.lblPaytmNumber.text = UserDetails.sharedInstance.phoneNumber
            cell!.lblMinmumLimit.text = "Minimum withdrawal amount".localized() + details.min_amount
            cell!.checkboxImgView.isHidden = false
            cell!.lblMinmumLimit.textColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 132.0/255, alpha: 1)

            if selectedIndex == 1 {
                cell!.innerView.layer.borderWidth = 1.0
                cell!.lblPaytm.textColor = kSelectedColor
                cell!.lblPaytmNumber.textColor = kSelectedColor
//                cell!.lblMinmumLimit.textColor = kSelectedColor
                cell!.imgView?.image = UIImage(named: "PaytmWalletSelected")
            }
            else{
                cell!.innerView.layer.borderWidth = 0
                cell!.lblPaytm.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
                cell!.lblPaytmNumber.textColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 132.0/255, alpha: 1)
//                cell!.lblMinmumLimit.textColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 132.0/255, alpha: 1)
                cell!.imgView?.image = UIImage(named: "PaytmWallet")
                cell!.checkboxImgView.isHidden = true
            }
        }
        else{
            cell!.paytmButton.setTitle("Link to PayTm wallet".localized(), for: .normal)
            cell!.addPaytmView.isHidden = false
            cell!.paytmDetailsView.isHidden = true
            cell!.innerView.layer.borderWidth = 0
            cell!.lblPaytm.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
            cell!.lblPaytmNumber.textColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 132.0/255, alpha: 1)
            cell!.lblMinmumLimit.textColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 132.0/255, alpha: 1)
            cell!.imgView?.image = UIImage(named: "PaytmWallet")
            cell!.checkboxImgView.isHidden = true
        }

        return cell!;
    }
    
    func getAddBankDetailsInputFormCell(indexPath: IndexPath) -> AddBankDetailsTableViewCell {
        var cell = tblView.dequeueReusableCell(withIdentifier: "AddBankDetailsTableViewCell") as? AddBankDetailsTableViewCell
        
        if cell == nil {
            cell = AddBankDetailsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "AddBankDetailsTableViewCell")
        }
        cell!.selectionStyle = .none
        AppHelper.showShodowOnCellsView(innerView: cell!.containerView)
        cell!.configData()
        return cell!
    }
    
    
    func getShowBankDetailsCell(indexPath: IndexPath, details: WithdrawOptionDetails) -> AddBankTableViewCell {
        
        var cell = tblView.dequeueReusableCell(withIdentifier: "AddBankTableViewCell") as? AddBankTableViewCell
        
        if cell == nil {
            cell = AddBankTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "AddBankTableViewCell")
        }
        
        cell?.selectionStyle = .none
        AppHelper.showShodowOnCellsView(innerView: cell!.innerView)
        cell!.innerView.layer.borderColor = kSelectedColor.cgColor
        if details.bankDetails != nil {
            cell!.addBankView.isHidden = true
            cell!.bankDetailsView.isHidden = false
            cell!.lblMinimumLimit.textColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 132.0/255, alpha: 1)

            if selectedIndex == 0 {
                cell!.innerView.layer.borderWidth = 1.0
                cell!.lblBankName.textColor = kSelectedColor
                cell!.lblAccountNumber.textColor = kSelectedColor
                cell!.imgView?.image = UIImage(named: "BankAddedImageSelected")
                cell!.checkboxImgView.isHidden = false
            }
            else{
                cell!.innerView.layer.borderWidth = 0
                cell!.lblBankName.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
                cell!.lblAccountNumber.textColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 132.0/255, alpha: 1)
                cell!.imgView?.image = UIImage(named: "BankAddedImage")
                cell!.checkboxImgView.isHidden = true
            }
            cell!.lblBankName.text = details.bankDetails!.bankName
            cell!.lblAccountNumber.text = details.bankDetails!.accountNumber
            
            let accountNumber = details.bankDetails!.accountNumber
            if accountNumber.count > 4 {
                let substr = accountNumber.suffix(4)
                cell!.lblAccountNumber.text = "**** **** **** " + substr
            }
            else{
                cell!.lblAccountNumber.text = "**** **** **** " + accountNumber
            }

            cell!.lblMinimumLimit.text = "Minimum withdrawal amount".localized() + details.min_amount
        }
        else{
            cell!.addBankView.isHidden = false
            cell!.bankDetailsView.isHidden = true
            cell!.innerView.layer.borderWidth = 0.0
            cell!.checkboxImgView.isHidden = true
            cell!.lblBankName.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
            cell!.lblAccountNumber.textColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 132.0/255, alpha: 1)
            cell!.lblMinimumLimit.textColor = UIColor(red: 127.0/255, green: 130.0/255, blue: 132.0/255, alpha: 1)
            cell!.imgView?.image = UIImage(named: "BankAddedImage")

        }

        return cell!;
    }
}


extension String
{
    func convertStringToInt() -> String {
        
        let numberFormat = NumberFormatter()
        numberFormat.locale = Locale(identifier: "en")
        
        if let getOP = numberFormat.number(from: self) {
            if let number = numberFormat.string(from: getOP) {
                return number
            }
        }
        return "";
    }
}
