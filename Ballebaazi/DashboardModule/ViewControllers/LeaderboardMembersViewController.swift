
import UIKit
import SwiftyJSON

class LeaderboardMembersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var headerView: LetspickFantasyWithoutWalletHeaderView!
    @IBOutlet weak var placeholderImgView: UIImageView!
    @IBOutlet weak var placeholderLbl: UILabel!
    @IBOutlet weak var lblSelfNoDataFound: UILabel!
    @IBOutlet weak var lblSelfRank: UILabel!
    @IBOutlet weak var lblSelfImage: UIImageView!
    @IBOutlet weak var lblSelfName: UILabel!
    @IBOutlet weak var lblSelfPoint: UILabel!
    @IBOutlet weak var lblSelfpoints: UILabel!
    @IBOutlet weak var selfInfoView: UIView!
    @IBOutlet weak var tblView: UITableView!
   
    lazy var classicPageNumber = 1
    lazy var battingPageNumber = 1
    lazy var bowlingPageNumber = 1
    lazy var limit = 50
    lazy var leaderboardsArray = Array<LeaderboardsUsers>()
    lazy var classicArray = Array<JSON>()
    lazy var battingArray = Array<JSON>()
    lazy var bowlingArray = Array<JSON>()
    lazy var classicSelfArray = Array<JSON>()
    lazy var battingSelfArray = Array<JSON>()
    lazy var bowlingSelfArray = Array<JSON>()
    
    lazy var classicSelfDict = [String: JSON]()
    lazy var battingSelfDict = [String: JSON]()
    lazy var bowlingSelDict = [String: JSON]()
    
    var leaderboardsDetails: LeaderboardsUsers?
    lazy var leaderboardID = ""
    lazy var leaderboardName = ""
    lazy var isNeedToLoadMoreForClassic = false
    lazy var isNeedToLoadMoreForBatting = false
    lazy var isNeedToLoadMoreForBowling = false
    var selectedLeagueType = FantasyType.Classic.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSelfpoints.text = "Points".localized();
        weak var weakSelf = self
        headerView.clasicButtonTappedBlock { (status) in
            weakSelf?.classicButtonTapped(status)
        }
        
        headerView.battingButtonTappedBlock { (status) in
            weakSelf?.battingButtonTapped(status)
        }
        
        headerView.bowlingButtonTappedBlock { (status) in
            weakSelf?.bowlingButtonTapped(status)
        }
        headerView.updateMatchName(details: nil)
        headerView.backButtonTitle = leaderboardName;
        navigationController?.navigationBar.isHidden = true
        tblView.register(UINib(nibName: "LeaderboardMembersTableViewCell", bundle: nil), forCellReuseIdentifier: "LeaderboardMembersTableViewCell")
        tblView.register(UINib(nibName: "LeaderboardMemberBarTableViewCell", bundle: nil), forCellReuseIdentifier: "LeaderboardMemberBarTableViewCell")
        tblView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        callGetLeaderboardUsersAPI(isNeedToShowLoader: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        AppHelper.addBackButtonWithSubTitleOnNavigationbar(title: "  Leaderboards", target: self, isNeedToShowShadow: true, subtitle: leaderboardName)
    }
    //MARK:- -IBAction Methods
    
    @IBAction func classicButtonTapped(_ isAnimation: Bool) {
        
        self.leaderboardsArray = LeaderboardsUsers.getAllLeaderboardsUsers(dataArray: self.classicArray)

        if(classicSelfDict.count == 0 && classicArray.count == 0){
            lblSelfpoints.isHidden = true
            lblSelfPoint.isHidden = true
            lblSelfRank.isHidden = true
            lblSelfName.isHidden = true
            lblSelfImage.isHidden = true
            lblSelfNoDataFound.isHidden = true
            selfInfoView.isHidden = true
            self.tblView.isHidden = true
            self.placeholderImgView.isHidden = false
            self.placeholderLbl.isHidden = false
        }
        else{
            selfInfoView.isHidden = false
            self.tblView.isHidden = false
            self.placeholderImgView.isHidden = true
            self.placeholderLbl.isHidden = true
        }
        
       if(classicSelfDict.count == 0){
            lblSelfpoints.isHidden = true
            lblSelfPoint.isHidden = true
            lblSelfRank.isHidden = true
            lblSelfName.isHidden = true
            lblSelfImage.isHidden = true
            lblSelfNoDataFound.isHidden = false
            lblSelfNoDataFound.text = "Join Cash League & Get Prizes in Leaderboard"
        }
        else{
            self.lblSelfImage.isHidden = true
        weak var weakSelf = self

            if let imageURL = self.classicSelfDict["image"]?.string{
                let userImage = UserDetails.sharedInstance.userImageUrl + imageURL
                DispatchQueue.main.async {
                    weakSelf?.lblSelfImage.isHidden = false
                }
                if let url = NSURL(string: userImage){
                    weakSelf?.lblSelfImage.setImage(with: url as URL, placeholder: UIImage(named: "PlaceholderRankTableImage"), progress: { received, total in
                        // Report progress
                    }, completion: { [weak self] image in
                        if (image != nil){
                            self?.lblSelfImage.image = image
                        }
                        else{
                            self?.lblSelfImage.image = UIImage(named: "PlaceholderRankTableImage")
                        }
                    })
                }
                else{
                    self.lblSelfImage.image = UIImage(named: "PlaceholderRankTableImage")
                }
            }
        
            lblSelfPoint.isHidden = false
            lblSelfpoints.isHidden = false
            lblSelfRank.isHidden = false
            lblSelfName.isHidden = false
            lblSelfNoDataFound.isHidden = true
            lblSelfName.text = classicSelfDict["username"]?.string
            lblSelfRank.text = classicSelfDict["rank"]?.string
            lblSelfPoint.text = classicSelfDict["all_points"]?.string
        }
        
        tblView.reloadData()
    }
    
    @IBAction func battingButtonTapped(_ isAnimation: Bool) {
        
        self.leaderboardsArray = LeaderboardsUsers.getAllLeaderboardsUsers(dataArray: self.battingArray)

        if(battingSelfDict.count == 0 && battingArray.count == 0){
            
            lblSelfpoints.isHidden = true
            lblSelfPoint.isHidden = true
            lblSelfRank.isHidden = true
            lblSelfName.isHidden = true
            lblSelfImage.isHidden = true
            lblSelfNoDataFound.isHidden = true
            selfInfoView.isHidden = true
            self.tblView.isHidden = true
            self.placeholderImgView.isHidden = false
            self.placeholderLbl.isHidden = false
        }
        else{
            selfInfoView.isHidden = false
            self.tblView.isHidden = false
            self.placeholderImgView.isHidden = true
            self.placeholderLbl.isHidden = true
        }
        
        if(battingSelfDict.count == 0){
            lblSelfpoints.isHidden = true
            lblSelfPoint.isHidden = true
            lblSelfRank.isHidden = true
            lblSelfName.isHidden = true
            lblSelfImage.isHidden = true
            lblSelfNoDataFound.isHidden = false
            lblSelfNoDataFound.text = "Join Cash League & Get Prizes in Leaderboard"
        }
        else{
            
            if let imageURL = self.battingSelfDict["image"]?.string{
                let userImage = UserDetails.sharedInstance.userImageUrl + imageURL
                self.lblSelfImage.isHidden = false
                if let url = NSURL(string: userImage){
                    self.lblSelfImage.setImage(with: url as URL, placeholder: UIImage(named: "PlaceholderRankTableImage"), progress: { received, total in
                        // Report progress
                    }, completion: { [weak self] image in
                        if (image != nil){
                            self?.lblSelfImage.image = image
                        }
                        else{
                            self?.lblSelfImage.image = UIImage(named: "PlaceholderRankTableImage")
                        }
                    })
                }
                else{
                    self.lblSelfImage.image = UIImage(named: "PlaceholderRankTableImage")
                }
            }
            
            lblSelfPoint.isHidden = false
            lblSelfpoints.isHidden = false
            lblSelfRank.isHidden = false
            lblSelfName.isHidden = false
            lblSelfImage.isHidden = false
            lblSelfNoDataFound.isHidden = true
            lblSelfName.text = battingSelfDict["username"]?.string
            lblSelfRank.text = battingSelfDict["rank"]?.string
            lblSelfPoint.text = battingSelfDict["all_points"]?.string
        }
        
        tblView.reloadData()
    }
    
    @IBAction func bowlingButtonTapped(_ isAnimation: Bool) {
      
        self.leaderboardsArray = LeaderboardsUsers.getAllLeaderboardsUsers(dataArray: self.bowlingArray)

        if(bowlingSelDict.count == 0 && bowlingArray.count == 0){
            
            lblSelfpoints.isHidden = true
            lblSelfPoint.isHidden = true
            lblSelfRank.isHidden = true
            lblSelfName.isHidden = true
            lblSelfImage.isHidden = true
            lblSelfNoDataFound.isHidden = true
            selfInfoView.isHidden = true
            self.tblView.isHidden = true
            self.placeholderImgView.isHidden = false
            self.placeholderLbl.isHidden = false
            
        }
        else{
            selfInfoView.isHidden = false
            self.tblView.isHidden = false
            self.placeholderImgView.isHidden = true
            self.placeholderLbl.isHidden = true
        }
     
        if(bowlingSelDict.count == 0){
            
            lblSelfPoint.isHidden = true
            lblSelfpoints.isHidden = true
            lblSelfRank.isHidden = true
            lblSelfName.isHidden = true
            lblSelfImage.isHidden = true
            lblSelfNoDataFound.isHidden = false
            lblSelfNoDataFound.text = "Join Cash League & Get Prizes in Leaderboard"
        }
        else{
            
            if let imageURL = self.bowlingSelDict["image"]?.string{
                let userImage = UserDetails.sharedInstance.userImageUrl + imageURL
                self.lblSelfImage.isHidden = false
                if let url = NSURL(string: userImage){
                    self.lblSelfImage.setImage(with: url as URL, placeholder: UIImage(named: "PlaceholderRankTableImage"), progress: { received, total in
                        // Report progress
                    }, completion: { [weak self] image in
                        if (image != nil){
                            self?.lblSelfImage.image = image
                        }
                        else{
                            self?.lblSelfImage.image = UIImage(named: "PlaceholderRankTableImage")
                        }
                    })
                }
                else{
                    self.lblSelfImage.image = UIImage(named: "PlaceholderRankTableImage")
                }
            }
            
            lblSelfpoints.isHidden = false
            lblSelfPoint.isHidden = false
            lblSelfRank.isHidden = false
            lblSelfName.isHidden = false
            lblSelfImage.isHidden = false
            lblSelfNoDataFound.isHidden = true
            lblSelfName.text = bowlingSelDict["username"]?.string
            lblSelfRank.text = bowlingSelDict["rank"]?.string
            lblSelfPoint.text = bowlingSelDict["all_points"]?.string
        }
        
        tblView.reloadData()
    }
    
    func callGetLeaderboardUsersAPI(isNeedToShowLoader: Bool) {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        let params = ["option": "get_leaders", "leaderboard_id": leaderboardID, "page": "1"]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kLeaderboardURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                        if let leaderDict = response["leaders"]?.dictionary{
                            
                            let bowlingDict = leaderDict["bowling"]?.dictionary;
                            let battingDict = leaderDict["batting"]?.dictionary;
                            let classicDict = leaderDict["classic"]?.dictionary;
//                            var isNeedToIncreasePageNumber = false
                            
                            if let classicLeaderboards  = classicDict!["others"]?.array{
                                weakSelf?.classicArray = classicLeaderboards
                                if classicLeaderboards.count >= 50{
                                    weakSelf?.classicPageNumber = 2
                                    weakSelf?.isNeedToLoadMoreForClassic = true
                                }
                            }
                            
                            if let battingLeaderboards  = battingDict!["others"]?.array{
                                weakSelf!.battingArray = weakSelf!.battingArray + battingLeaderboards
                                if battingLeaderboards.count >= 50{
                                    weakSelf?.battingPageNumber = 2
                                    weakSelf?.isNeedToLoadMoreForBatting = true
                                }
                            }
                            
                            if let bowlingLeaderboards  = bowlingDict!["others"]?.array{
                                weakSelf!.bowlingArray = weakSelf!.bowlingArray + bowlingLeaderboards
                                if bowlingLeaderboards.count >= 50{
                                    weakSelf!.bowlingPageNumber = 2
                                    weakSelf!.isNeedToLoadMoreForBowling = true
                                }
                            }
                            
                            if(classicDict!["self"]?.dictionary != nil)
                            {
                                weakSelf!.classicSelfDict = ((classicDict!["self"]?.dictionary)!)
                            }
                            if(bowlingDict!["self"]?.dictionary != nil)
                            {
                                weakSelf!.bowlingSelDict = ((bowlingDict!["self"]?.dictionary)!)
                            }
                            if(battingDict!["self"]?.dictionary != nil)
                            {
                                weakSelf!.battingSelfDict = ((battingDict!["self"]?.dictionary)!)
                            }
                            
                            if(weakSelf!.classicSelfDict.count == 0 && weakSelf!.classicArray.count == 0){
                                weakSelf!.lblSelfpoints.isHidden = true
                                weakSelf!.lblSelfPoint.isHidden = true
                                weakSelf!.lblSelfRank.isHidden = true
                                weakSelf!.lblSelfName.isHidden = true
                                weakSelf!.lblSelfImage.isHidden = true
                                weakSelf!.lblSelfNoDataFound.isHidden = true
                                weakSelf!.selfInfoView.isHidden = true
                                weakSelf!.tblView.isHidden = true
                                weakSelf!.placeholderImgView.isHidden = false
                                weakSelf!.placeholderLbl.isHidden = false
                            }
                            else if(weakSelf!.classicSelfDict.count == 0){
                                
                                weakSelf!.lblSelfPoint.isHidden = true
                                weakSelf!.lblSelfpoints.isHidden = true
                                weakSelf!.lblSelfRank.isHidden = true
                                weakSelf!.lblSelfName.isHidden = true
                                weakSelf!.lblSelfImage.isHidden = true
                                weakSelf!.lblSelfNoDataFound.isHidden = false
                                weakSelf!.selfInfoView.isHidden = false
                                weakSelf!.lblSelfNoDataFound.text = "Join Cash League & Get Prizes in Leaderboard"
                            }
                            else{
                                
                                weakSelf!.lblSelfPoint.isHidden = false
                                weakSelf!.lblSelfpoints.isHidden = false
                                weakSelf!.lblSelfRank.isHidden = false
                                weakSelf!.lblSelfName.isHidden = false
                                weakSelf!.lblSelfImage.isHidden = true
                                weakSelf!.lblSelfNoDataFound.isHidden = false
                                weakSelf!.selfInfoView.isHidden = false
                                
                                weakSelf!.lblSelfName.text = weakSelf!.classicSelfDict["username"]?.string
                                weakSelf!.lblSelfRank.text = weakSelf!.classicSelfDict["rank"]?.string
                                weakSelf!.lblSelfPoint.text = weakSelf!.classicSelfDict["all_points"]?.string
                                
                                if let imageURL =
                                    weakSelf!.classicSelfDict["image"]?.string{
                                    weakSelf!.lblSelfImage.isHidden = false
                                    let userImage = UserDetails.sharedInstance.userImageUrl + imageURL
                                    if let url = NSURL(string: userImage){
                                        weakSelf!.lblSelfImage.setImage(with: url as URL, placeholder: UIImage(named: "PlaceholderRankTableImage"), progress: { received, total in
                                            // Report progress
                                        }, completion: { [weak self] image in
                                            if (image != nil){
                                                self?.lblSelfImage.image = image
                                            }
                                            else{
                                                self?.lblSelfImage.image = UIImage(named: "PlaceholderRankTableImage")
                                            }
                                        })
                                    }
                                    else{
                                        weakSelf!.lblSelfImage.image = UIImage(named: "PlaceholderRankTableImage")
                                    }
                                }
                            }
                            
                            weakSelf!.leaderboardsArray = LeaderboardsUsers.getAllLeaderboardsUsers(dataArray: weakSelf!.classicArray)
                            weakSelf!.tblView.reloadData()
                        }
                        
                        if (response["self"]?.dictionary) != nil{
                            
                            weakSelf!.leaderboardsDetails = LeaderboardsUsers.parseDetails(details: response["self"]!)
                            weakSelf!.tblView.reloadData()
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                
            }
        }
    }
    
        
    func callGetLeaderboardUsersPagingAPI(fantacyType: String) {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        var pageNumber = 0

        if fantacyType == "1" {
            pageNumber = classicPageNumber
        }
        else if fantacyType == "2" {
            pageNumber = battingPageNumber
        }
        else if fantacyType == "3" {
            pageNumber = bowlingPageNumber
        }
        
        let params = ["option": "get_leaders", "leaderboard_id": leaderboardID, "page": String(pageNumber), "fantasy_type": fantacyType]
        weak var weakSelf = self;
        WebServiceHandler.performPOSTRequest(urlString: kLeaderboardURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""

                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                        if let leaderDict = response["leaders"]?.dictionary{
                            
                            if let leaderboardsData  = leaderDict["others"]?.array{
                                if fantacyType == "1" {
                                    weakSelf!.classicArray = weakSelf!.classicArray + leaderboardsData
                                    weakSelf!.leaderboardsArray = LeaderboardsUsers.getAllLeaderboardsUsers(dataArray: weakSelf!.classicArray)
                                    if leaderboardsData.count != 0{
                                        weakSelf!.isNeedToLoadMoreForClassic = true
                                        weakSelf!.classicPageNumber = weakSelf!.classicPageNumber + 1
                                    }
                                    else{
                                        weakSelf!.isNeedToLoadMoreForClassic = false
                                    }
                                }
                                else if fantacyType == "2"{
                                    weakSelf!.battingArray = weakSelf!.battingArray + leaderboardsData
                                    weakSelf!.leaderboardsArray = LeaderboardsUsers.getAllLeaderboardsUsers(dataArray: weakSelf!.battingArray)
                                    if leaderboardsData.count != 0{
                                        weakSelf!.isNeedToLoadMoreForBatting = true
                                        weakSelf!.battingPageNumber = weakSelf!.battingPageNumber + 1
                                    }
                                    else{
                                        weakSelf!.isNeedToLoadMoreForBatting = false
                                    }
                                }
                                else if fantacyType == "3" {
                                    weakSelf!.bowlingArray = weakSelf!.bowlingArray + leaderboardsData
                                    weakSelf!.leaderboardsArray = LeaderboardsUsers.getAllLeaderboardsUsers(dataArray: weakSelf!.bowlingArray)
                                    if leaderboardsData.count != 0{
                                        weakSelf!.isNeedToLoadMoreForBowling = true
                                        weakSelf!.bowlingPageNumber = weakSelf!.bowlingPageNumber + 1
                                    }
                                    else{
                                        weakSelf!.isNeedToLoadMoreForBowling = false
                                    }
                                }
                            }
                            weakSelf!.tblView.reloadData()
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    //MARK:- TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if leaderboardsArray.count > 3 {
            var isNeedToLoadMore = false
            
            if selectedLeagueType == FantasyType.Batting.rawValue {
                isNeedToLoadMore = isNeedToLoadMoreForClassic
            }
            else if selectedLeagueType == FantasyType.Batting.rawValue {
                isNeedToLoadMore = isNeedToLoadMoreForBatting
            }
            else if selectedLeagueType == FantasyType.Bowling.rawValue {
                isNeedToLoadMore = isNeedToLoadMoreForBowling
            }
            
            if isNeedToLoadMore{
                return leaderboardsArray.count - 1
            }
            return leaderboardsArray.count - 2
        }
        else if leaderboardsArray.count == 0{
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            if leaderboardsArray.count > 0{
                return 195
            }
            return 160
        }
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "LeaderboardMemberBarTableViewCell") as? LeaderboardMemberBarTableViewCell
            
            if cell == nil {
                cell = LeaderboardMemberBarTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LeaderboardMemberBarTableViewCell")
            }
            cell?.selectionStyle = .none
            cell?.configData(rankOneDetailsArray: leaderboardsArray)
            if leaderboardsArray.count <= 0{
                cell?.rankTitleView.isHidden = true
            }
            else{
                cell?.rankTitleView.isHidden = false
            }
            return cell!
        }
        else if indexPath.row == leaderboardsArray.count - 2 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            
            if cell == nil {
                cell = LoadMoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LoadMoreTableViewCell")
            }
            cell?.selectionStyle = .none
            var fantacyType = ""
            var isNeedToShowLoader = false
            
            if selectedLeagueType == FantasyType.Classic.rawValue {
                isNeedToShowLoader = isNeedToLoadMoreForClassic
                isNeedToLoadMoreForClassic = false
                fantacyType = "1"
            }
            else if selectedLeagueType == FantasyType.Batting.rawValue {
                isNeedToShowLoader = isNeedToLoadMoreForBatting
                isNeedToLoadMoreForBatting = false
                fantacyType = "2"
            }
            else if selectedLeagueType == FantasyType.Bowling.rawValue {
                isNeedToShowLoader = isNeedToLoadMoreForBowling
                isNeedToLoadMoreForBowling = false
                fantacyType = "3"
            }
            if isNeedToShowLoader{
                callGetLeaderboardUsersPagingAPI(fantacyType: fantacyType)
            }
            
            return cell!
        }
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "LeaderboardMembersTableViewCell") as? LeaderboardMembersTableViewCell
            cell?.selectionStyle = .none

            if cell == nil {
                cell = LeaderboardMembersTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LeaderboardMembersTableViewCell")
            }
            
            let count = indexPath.row + 2
            let details = leaderboardsArray[count]
            cell?.configData(details: details)
            return cell!
        }
    }
    

}
