//
//  JustPayPaymentOptionViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 06/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import HyperSDK
import SwiftyJSON

let JusPayClientID = "ballebaazi_ios"
let kGETPAYMENTMETHODS = "getPaymentMethods"
let kOPNameTxn = "nbTxn"
let kOpNameWalletTxn = "walletTxn"
let kOpNameCardTxn = "cardTxn"
let kOpNameUPITxn = "upiTxn"
let kOpNameCardInfo = "cardInfo"
let kOpNameCardList = "cardList"



//let JusPayClientID = "ballebaazi_android"

enum SelectedPaymentType: Int {
    case Wallet = 100
    case Cards = 101
    case NetBanking = 102
    case UPI = 103
}

class JustPayPaymentOptionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var lblTotalAmountTitle: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var tblView: UITableView!

    var orderDetails: JustPayOrderDetails?
    var paymentOptionsArray = Array<PaymentMethods>()
    var selectedPaymentType = SelectedPaymentType.Cards.rawValue
    
    var dataArray = [
        ["title": "Debit/Credit Cards", "image": ""],
        ["title": "Wallets", "image": ""],
        ["title": "Net Banking", "image": ""],
        ["title": "UPI", "image": ""]]
    
    var banksArray = [PaymentMethods]()
    var walletsArray = [PaymentMethods]()
    var cardsArray = [PaymentMethods]()
    var savecardsDetailsArray = [SaveCardsDetails]()

//    var dataArray = [
//        ["title": "Wallets", "image": ""],
//        ["title": "Paytm Wallet", "image": ""],
//        ["title": "UPI", "image": ""],
//        ["title": "Paytm UPI", "image": ""],
//        ["title": "Debit/Credit Cards", "image": ""],
//        ["title": "Net Banking", "image": ""]]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.register(UINib(nibName: "PaymentOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentOptionTableViewCell")
        tblView.register(UINib(nibName: "JusPayBanksTableViewCell", bundle: nil), forCellReuseIdentifier: "JusPayBanksTableViewCell")
        tblView.register(UINib(nibName: "SavedCardsTableViewCell", bundle: nil), forCellReuseIdentifier: "SavedCardsTableViewCell")
        
        tblView.tableFooterView = UIView()
        orderDetails =  JustPayOrderDetails()
        containerView.layer.cornerRadius = 10.0
        containerView.layer.masksToBounds = true

        callGetJustPayTokens()
    }
    
    
    func callGetJustPayTokens() {
        
       if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()

        let params = ["option": "add_cash_juspay", "amount": "1", "url": "home", "promo": "", "user_id": UserDetails.sharedInstance.userID, "is_mobile": "1"]

        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary {
                        self.orderDetails?.orderID = response["orderId"]?.string ?? ""
                        self.orderDetails?.amount = response["amount"]?.string ?? ""
                        self.orderDetails?.customerEmail = response["customerEmail"]?.string ?? "abc@example.com"
                        self.orderDetails?.currency = response["currency"]?.string ?? ""
                        self.orderDetails?.merchantId = response["merchantId"]?.string ?? ""
                        self.orderDetails?.customerId = response["customerId"]?.string ?? ""
                        self.orderDetails?.productId = response["productId"]?.string ?? ""
                        self.orderDetails?.customerPhone = response["customerPhone"]?.string ?? ""
                        if let jusPayDict = response["juspay"]?.dictionary {
                            self.orderDetails?.authToken = jusPayDict["client_auth_token"]?.string ?? ""
                        }
                        self.showPamentOption(opName: kOpNameCardList, paymentType: "", paymentMethod: "", upiAddress: "", cardNumber: "", cardExpiryMonth: "", cardExpiryYear: "", nameOnCard: "", cvvNumber: "")

//                        self.showPamentOption(opName: kGETPAYMENTMETHODS, paymentType: "", paymentMethod: "", upiAddress: "", cardNumber: "");
                    }
                    else{
                        AppHelper.sharedInstance.removeSpinner()
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                    AppHelper.sharedInstance.removeSpinner()
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
            
    func showPamentOption(opName: String, paymentType: String, paymentMethod: String, upiAddress: String, cardNumber: String, cardExpiryMonth: String, cardExpiryYear: String, nameOnCard: String, cvvNumber: String) {
        
        guard let details = orderDetails else {
            AppHelper.sharedInstance.removeSpinner()
            return
        }
        let paymentParams = AppHelper.getPaymentParams(details: details)

        let payload = NSMutableDictionary()
        payload["opName"] = opName
                
        payload["cardNumber"] = cardNumber
        payload["cardExpMonth"] = cardExpiryMonth
        payload["cardExpYear"] = cardExpiryYear
        payload["nameOnCard"] = nameOnCard
        payload["cardSecurityCode"] = cvvNumber
        
        payload["saveToLocker"] = "true"
        payload["isEmi"] = "false"
        
        if paymentType.count != 0  {
            payload["paymentMethodType"] = paymentType
        }
        
        if paymentMethod.count != 0  {
            payload["paymentMethod"] = paymentMethod
        }
        if upiAddress.count != 0 {
            payload["custVpa"] = upiAddress
        }
        paymentParams["payload"] = AppHelper.dictionary(toString: payload)
        
        
        
        
        Hyper().start(self, data: paymentParams as! [AnyHashable : Any]) { (status, responseData, error) in
            AppHelper.sharedInstance.removeSpinner()
            if ((responseData) != nil) {
                            
                let responseJSON = JSON(responseData as AnyObject);
                print(responseJSON)

                if opName == kGETPAYMENTMETHODS {
                    if let responseArray = responseJSON["paymentMethods"].array {
                        self.paymentOptionsArray = PaymentMethods.getAllPaymentOption(dataArray: responseArray)
                        
                        self.banksArray = self.paymentOptionsArray.filter { (details) -> Bool in
                            details.paymentMethodType == "NB"
                        }
                        
                        self.walletsArray = self.paymentOptionsArray.filter { (details) -> Bool in
                            details.paymentMethodType == "WALLET"
                        }

                        self.cardsArray = self.paymentOptionsArray.filter { (details) -> Bool in
                            details.paymentMethodType == "CARD"
                        }
                    }
                    DispatchQueue.main.async {
//                        self.showPamentOption(opName: kOpNameCardList, paymentType: "", paymentMethod: "", upiAddress: "")
                    }
                }
                else if opName == kOpNameCardList{
                    if let cardsArray = responseJSON["cards"].array {
                        self.savecardsDetailsArray = SaveCardsDetails.getAllSavedCards(dataArray: cardsArray)
                    }
                    self.tblView.reloadData()
                    DispatchQueue.main.async {
                        self.showPamentOption(opName: kGETPAYMENTMETHODS, paymentType: "", paymentMethod: "", upiAddress: "", cardNumber: "", cardExpiryMonth: "", cardExpiryYear: "", nameOnCard: "", cvvNumber: "")
                    }
                }
                else if opName == kOPNameTxn{
                    let paymentStatus = responseJSON["status"].string ?? ""
                    if (paymentStatus == "CHARGED") || (paymentStatus == "SUCCESS") {
                        AppHelper.showAlertView(message: "PaymentSuccessMsg".localized(), isErrorMessage: false)
                    }
                    else if paymentStatus == "PENDING_VBV"{
                        
                    }
                    else{
                        AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                    }
                }
                else if opName == kOpNameCardTxn{
                    let paymentStatus = responseJSON["status"].string ?? ""
                    if (paymentStatus == "CHARGED") || (paymentStatus == "SUCCESS") {
                        AppHelper.showAlertView(message: "PaymentSuccessMsg".localized(), isErrorMessage: false)
                    }
                    else if paymentStatus == "PENDING_VBV"{
                        
                    }
                    else{
                        AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                    }
                }
                else if opName == kOpNameWalletTxn{
                    let paymentStatus = responseJSON["status"].string ?? ""
                    if (paymentStatus == "CHARGED") || (paymentStatus == "SUCCESS") {
                        AppHelper.showAlertView(message: "PaymentSuccessMsg".localized(), isErrorMessage: false)

//                        let alert = UIAlertController(title: kAlert, message: "PaymentSuccessMsg".localized(), preferredStyle: UIAlertControllerStyle.alert)
////                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
////                            if weakSelf!.userTeamArray.count != 0 && (weakSelf!.leagueDetails != nil){
////                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: weakSelf!.leagueDetails!, userTeamArray: weakSelf!.userTeamArray, categoryName: weakSelf!.categoryName)
////                            }
////                            else{
////                                weakSelf?.navigationController?.popToRootViewController(animated: true)
////                            }
////                        }))
////                        self.present(alert, animated: true, completion: nil)

                    }
                    else if paymentStatus == "PENDING_VBV"{
                        
                    }
                    else{
                        AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                    }
                }
            }
        }

    }

    
    
}

extension JustPayPaymentOptionViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 1) && (indexPath.section == 0){
        }
        else if (indexPath.row == 1) && (indexPath.section == 1){
            
        }
        else if (indexPath.row == 1) && (indexPath.section == 2){
            return 120.0
        }
        else if (indexPath.row == 1) && (indexPath.section == 3){
            
        }
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (selectedPaymentType == SelectedPaymentType.Cards.rawValue) && (section == 0){
            return savecardsDetailsArray.count + 1
        }
        else if (selectedPaymentType == SelectedPaymentType.Wallet.rawValue) && (section == 1){
            return walletsArray.count + 1
        }
        else if (selectedPaymentType == SelectedPaymentType.NetBanking.rawValue) && (section == 2){
            return 2
        }
        else if (selectedPaymentType == SelectedPaymentType.UPI.rawValue) && (section == 3){
            
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row < savecardsDetailsArray.count) && (indexPath.section == 0){
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "SavedCardsTableViewCell") as? SavedCardsTableViewCell
            if cell == nil {
                cell = SavedCardsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "SavedCardsTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            let details = savecardsDetailsArray[indexPath.row]
            cell?.lblCardNumber.text = details.cardNumber
            return cell!
        }
        else if (indexPath.row == savecardsDetailsArray.count) && (indexPath.section == 0){
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "PaymentOptionTableViewCell") as? PaymentOptionTableViewCell
            if cell == nil {
                cell = PaymentOptionTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PaymentOptionTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.lblPaymentType.text = "Debit/Credit Cards".localized()
            return cell!
        }
        else if (indexPath.row >= 1) && (indexPath.section == 1){
            var cell = tableView.dequeueReusableCell(withIdentifier: "PaymentOptionTableViewCell") as? PaymentOptionTableViewCell
            if cell == nil {
                cell = PaymentOptionTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PaymentOptionTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            let details = walletsArray[indexPath.row - 1]
            cell?.lblPaymentType.text = details.paymentMethodDescription
            return cell!
        }
        else if (indexPath.row >= 1) && (indexPath.section == 1){
            var cell = tableView.dequeueReusableCell(withIdentifier: "PaymentOptionTableViewCell") as? PaymentOptionTableViewCell
            if cell == nil {
                cell = PaymentOptionTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PaymentOptionTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            let details = walletsArray[indexPath.row - 1]
            cell?.lblPaymentType.text = details.paymentMethodDescription
            return cell!
        }
        else if (indexPath.row >= 1) && (indexPath.section == 2){
            var cell = tableView.dequeueReusableCell(withIdentifier: "JusPayBanksTableViewCell") as? JusPayBanksTableViewCell
            
            if cell == nil {
                cell = JusPayBanksTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "JusPayBanksTableViewCell")
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.collectionView.register(UINib(nibName: "JusPayAvilableBanksCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JusPayAvilableBanksCollectionViewCell")
            cell?.collectionView.delegate = self
            cell?.collectionView.dataSource = self
            cell?.collectionView.reloadData()
            return cell!
        }
        else if (indexPath.row >= 1) && (indexPath.section == 3){
            
        }
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "PaymentOptionTableViewCell") as? PaymentOptionTableViewCell
            
            if cell == nil {
                cell = PaymentOptionTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PaymentOptionTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.lblPaymentType.text = dataArray[indexPath.section]["title"]
            return cell!
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellTitle = dataArray[indexPath.section]["title"]
        if cellTitle == "Wallets" {
            if indexPath.row == 0 {
                if selectedPaymentType == SelectedPaymentType.Wallet.rawValue {
                    selectedPaymentType = 1000
                }
                else{
                    selectedPaymentType = SelectedPaymentType.Wallet.rawValue
                }
            }
            else{
                let details = walletsArray[indexPath.row - 1]
                showPamentOption(opName: kOpNameWalletTxn, paymentType: details.paymentMethodType, paymentMethod: details.paymentMethod, upiAddress: "", cardNumber: "", cardExpiryMonth: "", cardExpiryYear: "", nameOnCard: "", cvvNumber: "")
            }
        }
        else if cellTitle == "Debit/Credit Cards"  {
            if (indexPath.row < savecardsDetailsArray.count) && (indexPath.section == 0){
                
            }
            else{
                let addCardVC = storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
                addCardVC.orderDetails = orderDetails
                navigationController?.pushViewController(addCardVC, animated: true)

            }
            
//            if indexPath.row == 0 {
//                if selectedPaymentType == SelectedPaymentType.Cards.rawValue {
//                    selectedPaymentType = 1000
//                }
//                else{
//                    selectedPaymentType = SelectedPaymentType.Cards.rawValue
//                }
//            }
//            else{
//                if indexPath.row == 0{
//                    return
//                }
//                let addCardVC = storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
//                addCardVC.orderDetails = orderDetails
//                navigationController?.pushViewController(addCardVC, animated: true)
//            }
            
            
        }
        else if cellTitle == "Net Banking"  {
            if selectedPaymentType == SelectedPaymentType.NetBanking.rawValue {
                selectedPaymentType = 1000
            }
            else{
                selectedPaymentType = SelectedPaymentType.NetBanking.rawValue
            }
        }
        else if cellTitle == "UPI"  {
//            showPamentOption(opName: kOpNameUPITxn, paymentType: "UPI", paymentMethod: "UPI", upiAddress: "rjyotiverma527@okicici")

//            if selectedPaymentType == SelectedPaymentType.UPI.rawValue {
//                selectedPaymentType = 1000
//            }
//            else{
//                selectedPaymentType = SelectedPaymentType.UPI.rawValue
//            }
        }
        
        tblView.reloadData()
    }
    
    
    //MARK:- Collection View Data Source and Delegate

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return banksArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JusPayAvilableBanksCollectionViewCell", for: indexPath) as? JusPayAvilableBanksCollectionViewCell
        
        let details = banksArray[indexPath.row]
        cell?.lblBankName.text = details.paymentMethodDescription
        let bankImage = UIImage(named: details.paymentMethod)
        if (bankImage != nil) {
            cell?.imgView.image = bankImage
        }
        else{
            cell?.imgView.image = bankImage
        }
        return cell!;
     }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let details = banksArray[indexPath.row]
        showPamentOption(opName: kOPNameTxn, paymentType: details.paymentMethodType, paymentMethod: details.paymentMethod, upiAddress: "", cardNumber: "", cardExpiryMonth: "", cardExpiryYear: "", nameOnCard: "", cvvNumber: "")
    }

}
