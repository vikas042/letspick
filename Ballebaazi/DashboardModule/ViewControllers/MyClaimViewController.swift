//
//  MyClaimViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 26/02/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class MyClaimViewController: UIViewController {

    @IBOutlet weak var lblNoDataFound: UILabel!
    var myClaimArray = Array<ClaimDetails>()
    
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoDataFound.text = "no_Claim_history".localized()
        lblNoDataFound.isHidden = true
        headerView.headerTitle = "My Claims".localized()
        tblView.register(UINib(nibName: "MyClaimTableViewCell", bundle: nil), forCellReuseIdentifier: "MyClaimTableViewCell")
        callGetRewardStoreAPI(isNeedToShowLoader: true)
    }
    
    @IBAction func transactionButtonTapped(_ sender: Any) {
        let transactionVC = storyboard?.instantiateViewController(withIdentifier: "RewardTransactionViewController") as! RewardTransactionViewController
        navigationController?.pushViewController(transactionVC, animated: true)
    }
    
    func callGetRewardStoreAPI(isNeedToShowLoader: Bool)  {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }

        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        weak var weakSelf = self

        let params = ["option": "claim_transaction", "user_id": UserDetails.sharedInstance.userID ] as [String : Any]
        
        WebServiceHandler.performPOSTRequest(urlString: kRewardProgram, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        if let history = response["history"]?.array {
                            weakSelf?.myClaimArray = ClaimDetails.getAllClaimList(dataArray: history)
                            if weakSelf?.myClaimArray.count == 0 {
                                weakSelf?.lblNoDataFound.isHidden = false
                            }
                            weakSelf?.tblView.reloadData()
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }

        }
    }
}

extension MyClaimViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myClaimArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "MyClaimTableViewCell") as? MyClaimTableViewCell
        
        if cell == nil {
            cell = MyClaimTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "MyClaimTableViewCell")
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none

        let details = myClaimArray[indexPath.row]
        cell?.configData(details: details)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
