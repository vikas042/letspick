//
//  MoreLeagueViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 28/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit


class MoreLeagueViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var lblCatMessage: UILabel!
    @IBOutlet weak var catImgView: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerView: LetspickFantasyHeaderView!
    
    var leagueArray = Array<LeagueDetails>()
    var categoryID = ""
    var fantasyType = ""
    
    var categoryName = ""
    var categoryMessage = ""
    var categoryImg = ""
    lazy var userTicketsArray = Array<TicketDetails>()

    var isMatchClosingTimeRefereshing = false
    var isUserValidatingToJoinLeague = false
    
    var matchDetails: MatchDetails?
    var selectedFantasy = 0
    var isPullToRefresh = false
    var refreshControl = UIRefreshControl()
    var timer: Timer?

    @IBOutlet weak var lblMyTeam: UILabel!
    @IBOutlet weak var lblJoinLeagues: UILabel!
    @IBOutlet weak var lblJoinedLeagueTitle: UILabel!
    @IBOutlet weak var lblMyTeamTitle: UILabel!

    var totalTeams = 0
    var totalJoinedLeague = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationComesInForground), name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)
        collectionView.register(UINib(nibName: "LeagueCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueCollectionViewCell")
        collectionView.register(UINib(nibName: "SingleLeagueCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SingleLeagueCollectionViewCell")
        collectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionReusableView")

        collectionView.addSubview(refreshControl)
        refreshControl.tintColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshLeagueData(_:)), for: .valueChanged)
        headerView.isFantasyModeEnable = false
        headerView.updateData(details: matchDetails)

        fantasyType = "1"
        lblCategoryName.text = categoryName.uppercased()
        lblCatMessage.text = categoryMessage
        
        if categoryImg.count != 0{
            if let url = NSURL(string: UserDetails.sharedInstance.teamImageUrl + categoryImg){
                catImgView.setImage(with: url as URL, placeholder: UIImage(named: "LeagueDefaultIcon"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.catImgView.image = image
                    }
                    else{
                        self?.catImgView.image = UIImage(named: "LeagueDefaultIcon")
                    }
                })
            }
        }
        else{
            catImgView.image = UIImage(named: "LeagueDefaultIcon")
        }

        
        if selectedFantasy == FantasyType.Classic.rawValue {
            fantasyType = "1"
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            fantasyType = "2"
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            fantasyType = "3"
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue {
            fantasyType = "4"
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue {
            fantasyType = "5"
        }

        let urlString = kMatch + "?option=get_match_v1_category&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey + "&fantasy_type=" + fantasyType + "&category=" + categoryID + "&screen_msg=1"
        callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: true)
        updateTimerValue()

        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerValue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerValue), userInfo: nil, repeats: true)
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblMyTeam.text = String(totalTeams)
        lblJoinLeagues.text = String(totalJoinedLeague)
        
        if totalTeams > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalJoinedLeague) > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
    }
    
    @objc func applicationComesInForground(notification: Notification)  {
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            if ((navVC.visibleViewController as? MoreLeagueViewController) != nil) {
                let urlString = kMatch + "?option=get_match_v1_category&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey + "&fantasy_type=" + fantasyType + "&category=" + categoryID + "&screen_msg=1"
                callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: false)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "applicationComesInForground"), object: nil)
    }

    
    @objc func refreshLeagueData(_ sender: Any?) {
        self.refreshControl.endRefreshing()

        let urlString = kMatch + "?option=get_match_v1_category&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey + "&fantasy_type=" + fantasyType + "&category=" + categoryID + "&screen_msg=1"
        if sender != nil {
            callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: true)
        }
        else{
            callGetLeagueAPI(urlString: urlString, isNeedToShowLoader: false)
        }
    }
    
    // MARK:- Collection View Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return leagueArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let reminder = leagueArray.count % 2
        if (reminder == 1) && (indexPath.row == leagueArray.count-1) {
            return CGSize(width: collectionView.frame.width , height: 137)
        }
        else{
            return CGSize(width: collectionView.frame.width/2 , height: 166)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionElementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
            footerView.lblViewMore.isHidden = true
            footerView.viewMoreButton.isHidden = true
            return footerView
            
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let reminder = leagueArray.count % 2
        if (reminder == 1) && (indexPath.row == leagueArray.count-1) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleLeagueCollectionViewCell", for: indexPath) as! SingleLeagueCollectionViewCell
            cell.joinButton.tag = indexPath.row
            cell.joinButton.addTarget(self, action: #selector(self.joinLeagueButtonTapped(button:)), for: .touchUpInside)
            cell.configData(details: leagueArray[indexPath.row], matchDetails: matchDetails!, gameType: GameType.Cricket.rawValue)
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueCollectionViewCell", for: indexPath) as! LeagueCollectionViewCell
            cell.joinButton.tag = indexPath.row
            cell.joinButton.addTarget(self, action: #selector(self.joinLeagueButtonTapped(button:)), for: .touchUpInside)
            cell.configData(details: leagueArray[indexPath.row], matchDetails: matchDetails!, gameType: GameType.Cricket.rawValue)
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let leagueDetails = leagueArray[indexPath.row]
        if leagueDetails.leagueType == "2" {
            return
        }
        
        let leaguePreview = storyboard?.instantiateViewController(withIdentifier: "LeaguePreviewViewController") as! LeaguePreviewViewController
        leaguePreview.leagueDetails = leagueDetails
        leaguePreview.matchDetails = matchDetails
        leaguePreview.userTicketsArray = userTicketsArray
        navigationController?.pushViewController(leaguePreview, animated: true)
    }
    
    @objc func joinLeagueButtonTapped(button: SolidButton)  {
        
        let leagueDetails = leagueArray[button.tag]
        
        if (leagueDetails.teamType == "1") {
            var maxTeamAllow = 0;
            
            if leagueDetails.fantasyType == "1"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForClassic;
            }
            else if leagueDetails.fantasyType == "2"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBatting;
            }else if leagueDetails.fantasyType == "3"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBowling;
            }
            else if leagueDetails.fantasyType == "4"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForReverse;
            }else if leagueDetails.fantasyType == "5"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForWizard;
            }

            if leagueDetails.joinedLeagueCount == maxTeamAllow{
                return;
            }
        }
        else if (leagueDetails.teamType == "1") {
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.confirmedLeague == "2"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.teamType == "2"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.teamType == "3"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        
        if isUserValidatingToJoinLeague {
            return;
        }
        
        var fantasyType = ""
        if leagueDetails.fantasyType == "1"{
            fantasyType = "Classic";
        }
        else if leagueDetails.fantasyType == "2"{
            fantasyType = "Batting";
        }else if leagueDetails.fantasyType == "3"{
            fantasyType = "Bowling";
        }
        else if leagueDetails.fantasyType == "4"{
            fantasyType = "Reverse";
        }else if leagueDetails.fantasyType == "5"{
            fantasyType = "Wizard";
        }

        AppxorEventHandler.logAppEvent(withName: "JoinNowButtonClicked", info: ["ContestType": categoryName, "ContestID": leagueDetails.leagueId, "Type": fantasyType, "SportType": "Cricket"])
        
        callLeagueValidationAPI(leagueDetails: leagueDetails, categoryName: categoryName)
    }
    
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails, categoryName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        isUserValidatingToJoinLeague = true
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "league_prev_data_v2","check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            weakSelf?.isUserValidatingToJoinLeague = false
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                                        
//                                        let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = weakSelf!.matchDetails
//                                        addCashVC?.userTeamArray = teamArray
//                                        addCashVC?.categoryName = categoryName
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                    playerVC.matchDetails = weakSelf?.matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails, userTeamArray: teamArray, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                playerVC.matchDetails = weakSelf?.matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                
                            }
                        }
                        else{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, ticketDetails: TicketDetails?)  {
        
        let joinedLeagueConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
        joinedLeagueConfirmVC?.leagueDetails = leagueDetails
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails

        joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
        joinedLeagueConfirmVC?.matchDetails = self.matchDetails
        joinedLeagueConfirmVC?.leagueCategoryName = categoryName
        navigationController?.pushViewController(joinedLeagueConfirmVC!, animated: true)
    }
    
    
    func goToSelectPlayerScreen(leagueDetails: LeagueDetails)  {
        
        let playerVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectPlayerViewController") as? SelectPlayerViewController
        playerVC!.matchDetails = matchDetails
        playerVC!.leagueDetails = leagueDetails
        navigationController?.pushViewController(playerVC!, animated: true)
    }
    
    func updateLeagueJoinedStatus(joinedLeague: LeagueDetails) {
        
        for leagueDetails in leagueArray {
            if leagueDetails.leagueId == joinedLeague.leagueId{
                leagueDetails.joinedLeagueCount += 1
                totalJoinedLeague += 1
                break;
            }
        }
        updateTeamCount()
        refreshLeagueData(nil)
    }
    
    func updateTeamCount() {
        lblMyTeam.text = String(totalTeams)
        lblJoinLeagues.text = String(totalJoinedLeague)
        
        if totalTeams > 1{
            lblMyTeamTitle.text = "Teams".localized()
        }
        else{
            lblMyTeamTitle.text = "Team".localized()
        }
        
        if Int(totalJoinedLeague) > 1{
            lblJoinedLeagueTitle.text = "Leagues".localized()
        }
        else{
            lblJoinedLeagueTitle.text = "League".localized()
        }
        collectionView.reloadData()

    }

    func callGetLeagueAPI(urlString: String, isNeedToShowLoader: Bool)  {
        
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                isPullToRefresh = false
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        weak var weakSelf = self

        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            self.isPullToRefresh = false
            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
//                    DispatchQueue.global().async {
//                        self.callMyTicketsAPI()
//                    }

                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        self.leagueArray = LeagueDetails.getCategoryLeagueDetails(responseResult: result!)
                        var totalClassicJoinedLeague = "0"
                        var totalBattingJoinedLeague = "0"
                        var totalBowlingJoinedLeague = "0"
                        var totalReverseJoinedLeague = "0"
                        var totalWizardJoinedLeague = "0"

                        (totalClassicJoinedLeague, totalBattingJoinedLeague, totalBowlingJoinedLeague, totalReverseJoinedLeague, totalWizardJoinedLeague) = LeagueDetails.getJoinedLeagueCounts(responseResult: result!)

                        if let response = result!["response"].dictionary{
                            if let userTeamsArray = response["user_teams"]?.array{
                                UserDetails.sharedInstance.userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: userTeamsArray, matchDetails: self.matchDetails!)
                                self.totalTeams = userTeamsArray.count
                            }
                            
                            if let tempArray = response["active_tickets"]?.array {
                                (weakSelf!.userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                                
                                for ticketDetails in weakSelf!.userTicketsArray{
                                    for details in weakSelf!.leagueArray {
                                        if ticketDetails.ticketType != "2"{
                                            if (details.categoryId == ticketDetails.leagueCategory) && (ticketDetails.matchKey == weakSelf!.matchDetails!.matchKey) && (ticketDetails.joiningAmount == details.joiningAmount){
                                                details.isTicketAvailable = true
                                                if ticketDetails.ticketType == "3" {
                                                    details.isPassAvailable = true
                                                }
                                            }
                                        }
                                        else{
                                            if (details.categoryId == ticketDetails.leagueCategory) && (ticketDetails.joiningAmount == details.joiningAmount){
                                                details.isTicketAvailable = true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if self.selectedFantasy == FantasyType.Classic.rawValue {
                            self.totalJoinedLeague = Int(totalClassicJoinedLeague) ?? 0
                        }
                        else if self.selectedFantasy == FantasyType.Batting.rawValue {
                            self.totalJoinedLeague = Int(totalBattingJoinedLeague) ?? 0
                            
                        }
                        else if self.selectedFantasy == FantasyType.Bowling.rawValue {
                            self.totalJoinedLeague = Int(totalBowlingJoinedLeague) ?? 0
                        }
                        else if self.selectedFantasy == FantasyType.Reverse.rawValue {
                            self.totalJoinedLeague = Int(totalReverseJoinedLeague) ?? 0
                        }
                        else if self.selectedFantasy == FantasyType.Wizard.rawValue {
                            self.totalJoinedLeague = Int(totalWizardJoinedLeague) ?? 0
                        }

                        self.updateTeamCount()
                        if self.leagueArray.count == 0{
                            self.collectionView.isHidden = true
                        }
                        else{
                            self.collectionView.isHidden = false
                        }
                        
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                        
                        AppHelper.sharedInstance.removeSpinner()
                    }
                }
                else{
                    AppHelper.sharedInstance.removeSpinner()
                }
            }
            else{
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
    
    func callMyTicketsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        let params = ["option": "user_tickets", "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200"{
                    
                    if let response = result!["response"]?.dictionary{
                        if let tempArray = response["active_tickets"]?.array {
                            (weakSelf!.userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                            
                            for ticketDetails in weakSelf!.userTicketsArray{
                                for details in self.leagueArray {
                                    if (details.templateID == ticketDetails.templateID) && (ticketDetails.matchKey == weakSelf!.matchDetails!.matchKey){
                                        details.isTicketAvailable = true
                                    }
                                }
                            }
                            self.collectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    // MARK:- Timer Handlers
    @objc func updateTimerValue()  {
        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
        collectionView.reloadData()
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func myLeagueButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "TeamTabClicked", info: ["SportType": "Cricket"])

        let myTeamVC = storyboard?.instantiateViewController(withIdentifier: "MyTeamsViewController") as! MyTeamsViewController
        myTeamVC.matchDetails = matchDetails
        myTeamVC.userTeamsArray = UserDetails.sharedInstance.userTeamsArray
        
        if selectedFantasy == FantasyType.Classic.rawValue {
            myTeamVC.selectedFantasyType = FantasyType.Classic.rawValue
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            myTeamVC.selectedFantasyType = FantasyType.Batting.rawValue
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            myTeamVC.selectedFantasyType = FantasyType.Bowling.rawValue
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue {
            myTeamVC.selectedFantasyType = FantasyType.Reverse.rawValue
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue {
            myTeamVC.selectedFantasyType = FantasyType.Wizard.rawValue
        }

        navigationController?.pushViewController(myTeamVC, animated: true)
    }
    
    @IBAction func joinedLeagueButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "LeagueTabClicked", info: ["SportType": "Cricket"])

        let joinedWinnerRankVC = storyboard?.instantiateViewController(withIdentifier: "JoinedLeagueViewController") as! JoinedLeagueViewController
        joinedWinnerRankVC.matchDetails = matchDetails
        if selectedFantasy == FantasyType.Classic.rawValue {
            joinedWinnerRankVC.selectedFantasyType = FantasyType.Classic.rawValue
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            joinedWinnerRankVC.selectedFantasyType = FantasyType.Batting.rawValue
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            joinedWinnerRankVC.selectedFantasyType = FantasyType.Bowling.rawValue
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue {
            joinedWinnerRankVC.selectedFantasyType = FantasyType.Reverse.rawValue
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue {
            joinedWinnerRankVC.selectedFantasyType = FantasyType.Wizard.rawValue
        }

        navigationController?.pushViewController(joinedWinnerRankVC, animated: true)
    }



}
