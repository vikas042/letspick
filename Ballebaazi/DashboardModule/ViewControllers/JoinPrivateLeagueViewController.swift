//
//  JoinPrivateLeagueViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 21/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire


class JoinPrivateLeagueViewController: UIViewController {

    @IBOutlet weak var proceedButton: SolidButton!
    @IBOutlet weak var lblInviteCode: UILabel!
    @IBOutlet weak var lblEnterInviteCode: UILabel!
    @IBOutlet weak var headerView: CustomNavigationBar!
    
    var leagueCode: String?
    
    @IBOutlet weak var textFieldInviteCode: UITextField!
    
    var matchDetails: MatchDetails?
    
    var leagueDetails: LeagueDetails?
    var userTeamsArray = Array<UserTeamDetails>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "Join League".localized()
        UserDetails.sharedInstance.userLoggedIn = true;
        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
        UserDefaults.standard.set(false, forKey: "isUserOnUserName")
        textFieldInviteCode.placeholder = "Invite Code".localized()
        proceedButton.setTitle("Proceed".localized(), for: .normal)
        proceedButton.backgroundColor = UIColor(red: 0/255, green: 136.0/255, blue: 64.0/255, alpha: 1)
        lblInviteCode.text = "Enter Invite Code".localized()
        lblEnterInviteCode.text = "Enter Invite Code to join league".localized()
        
        textFieldInviteCode.text = leagueCode
        navigationController?.navigationBar.isHidden = false

        if navigationController?.viewControllers.count == 1 {
            navigationController?.navigationBar.tag = 2000;
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    func updateLeagueCode() {
        textFieldInviteCode.text = leagueCode
    }
    
    func showShowOnCreateLeagueButton() {

    }
    
    @IBAction func joinNowButtonTapped(_ sender: Any) {
        
        if textFieldInviteCode.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter invitation code".localized(), isErrorMessage: true)
            return
        }
        callVerifyInvitationCodeAPI()
    }
    
    @IBAction func createLeagueButtonTapped(_ sender: Any) {

        let createPrivateLeague = storyboard?.instantiateViewController(withIdentifier: "CreatePrivateLeagueViewController") as! CreatePrivateLeagueViewController
        navigationController?.pushViewController(createPrivateLeague, animated: true)
    }
    
    func callVerifyInvitationCodeAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        var gameType = GameType.Cricket.rawValue
        var urlString = kMatch
        
        if (textFieldInviteCode.text!.hasPrefix("CT")) {
            gameType = GameType.Cricket.rawValue
            urlString = kMatch;
        } else if (textFieldInviteCode.text!.hasPrefix("KI")) {
            gameType = GameType.Kabaddi.rawValue
            urlString = kKabaddiMatchURL;
        } else if (textFieldInviteCode.text!.hasPrefix("FT")) {
            gameType = GameType.Football.rawValue
            urlString = kFootballMatchURL;
        } else if (textFieldInviteCode.text!.hasPrefix("BT")) {
            gameType = GameType.Basketball.rawValue
            urlString = kBasketballMatchURL;
        } else if (textFieldInviteCode.text!.hasPrefix("BE")) {
            gameType = GameType.Baseball.rawValue
            urlString = kBaseballMatchURL;
        }

        
        let parameters: Parameters = ["option": "check_league_code","league_code": textFieldInviteCode.text!, "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "401"{
                    
                    let alert = UIAlertController(title: "Oops! Not Enough Cash", message: nil, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
                        let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
                        addCashVC?.leagueDetails = weakSelf?.leagueDetails
                        weakSelf?.navigationController?.pushViewController(addCashVC!, animated: true)
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                    weakSelf?.present(alert, animated: true, completion: nil)
                }
                else{
                    if let response = result!["response"]?.dictionary{
                        
                        if let league = response["league"]?.dictionary{
                            weakSelf?.leagueDetails = LeagueDetails.getLeagueDetails(details: league)
                        }
                        
                        if let match = response["match"]?.dictionary{
                            weakSelf?.matchDetails = MatchDetails.parseMatchDetails(details: match)
                        }
                        
                        if let tempArray = response["teams_obj"]?.array{
                            weakSelf?.userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: tempArray, matchDetails: weakSelf!.matchDetails!)
                        }

                       
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            if gameType == GameType.Cricket.rawValue{
                                let leaguePreview = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "LeaguePreviewViewController") as! LeaguePreviewViewController
                                leaguePreview.leagueDetails = weakSelf?.leagueDetails
                                leaguePreview.matchDetails = weakSelf?.matchDetails
                                leaguePreview.userTeamsArray = weakSelf!.userTeamsArray
                                leaguePreview.isViewForPrivateLeague = true
                                leaguePreview.categoryName = kPrivateLeague
                                weakSelf?.navigationController?.pushViewController(leaguePreview, animated: true)

                            }
                            else if gameType == GameType.Kabaddi.rawValue{
                                let leaguePreview = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "KabaddiLeaguePreviewViewController") as! KabaddiLeaguePreviewViewController
                                leaguePreview.leagueDetails = weakSelf?.leagueDetails
                                leaguePreview.matchDetails = weakSelf?.matchDetails
                                leaguePreview.userTeamsArray = weakSelf!.userTeamsArray
                                leaguePreview.isViewForPrivateLeague = true
                                leaguePreview.categoryName = kPrivateLeague
                                weakSelf?.navigationController?.pushViewController(leaguePreview, animated: true)

                            }
                            else if gameType == GameType.Football.rawValue{
                                let leaguePreview = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "FootballLeaguePreviewViewController") as! FootballLeaguePreviewViewController
                                leaguePreview.leagueDetails = weakSelf?.leagueDetails
                                leaguePreview.matchDetails = weakSelf?.matchDetails
                                leaguePreview.userTeamsArray = weakSelf!.userTeamsArray
                                leaguePreview.isViewForPrivateLeague = true
                                leaguePreview.categoryName = kPrivateLeague
                                weakSelf?.navigationController?.pushViewController(leaguePreview, animated: true)

                            }
                            else if gameType == GameType.Basketball.rawValue{
                                let leaguePreview = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "BasketballLeaguePreviewViewController") as! BasketballLeaguePreviewViewController
                                leaguePreview.leagueDetails = weakSelf?.leagueDetails
                                leaguePreview.matchDetails = weakSelf?.matchDetails
                                leaguePreview.userTeamsArray = weakSelf!.userTeamsArray
                                leaguePreview.isViewForPrivateLeague = true
                                leaguePreview.categoryName = kPrivateLeague
                                weakSelf?.navigationController?.pushViewController(leaguePreview, animated: true)

                            }
                            else if gameType == GameType.Baseball.rawValue{
                                let leaguePreview = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "BaseballLeaguePreviewPlayersViewController") as! BaseballLeaguePreviewPlayersViewController
                                leaguePreview.leagueDetails = weakSelf?.leagueDetails
                                leaguePreview.matchDetails = weakSelf?.matchDetails
                                leaguePreview.userTeamsArray = weakSelf!.userTeamsArray
                                leaguePreview.isViewForPrivateLeague = true
                                leaguePreview.categoryName = kPrivateLeague
                                weakSelf?.navigationController?.pushViewController(leaguePreview, animated: true)
                            }
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
//        
//    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails?, userTeamArray: Array<UserTeamDetails>)  {
//
//        let joinedLeagueConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
//        joinedLeagueConfirmVC?.leagueDetails = leagueDetails
//        joinedLeagueConfirmVC?.ticketDetails = ticketDetails
//        joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
//        joinedLeagueConfirmVC?.leagueCategoryName = kPrivateLeague
//        joinedLeagueConfirmVC?.isNeedToShowMatchClosePopup = true
//        joinedLeagueConfirmVC?.matchDetails = matchDetails
//        navigationController?.pushViewController(joinedLeagueConfirmVC!, animated: true)
//    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
