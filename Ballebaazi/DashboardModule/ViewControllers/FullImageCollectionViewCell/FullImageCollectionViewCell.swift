//
//  FullImageCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/03/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class FullImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
