//
//  NotificationsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 12/09/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var headerView: CustomNavigationBar!
    
//    var notificationDetailsArray = Array<[String: Any]>()
    var isNeedToShowLoadMore = false
    var notificationsArray = Array<NotificationsDetails>()
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoRecordFound: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "Notifications".localized()
        tblView.register(UINib(nibName: "NotificationsTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationsTableViewCell")
        tblView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")

        UserDetails.sharedInstance.notificationCount = "";
        tblView.estimatedRowHeight = 50
        tblView.rowHeight = UITableViewAutomaticDimension
        lblNoRecordFound.isHidden = true
        lblNoRecordFound.text = "Notifications are not available."
        callNotificationAPI(isNeedToShowLoader: true)
    }

    
    //MARK:- API Related Methods
    
    func callNotificationAPI(isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        var notificationID = "0"
        
        if let details = notificationsArray.last{
            notificationID = details.notificationId
        }
        
        let params = ["option": "notifications", "user_id": UserDetails.sharedInstance.userID, "last_id": notificationID]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                if let response = result!["response"]?.dictionary{
                    if let responseArray = response["notifications"]?.array{
                        
                        self.notificationsArray.append(contentsOf: NotificationsDetails.getAllNotificationList(responseArray: responseArray))
                                                
                        if weakSelf?.notificationsArray.count != 0{
                            weakSelf?.lblNoRecordFound.isHidden = true
                        }
                        else{
                            weakSelf?.lblNoRecordFound.isHidden = false
                        }
                        
                        if responseArray.count == 0{
                            weakSelf?.isNeedToShowLoadMore = false
                        }
                        else{
                            weakSelf?.isNeedToShowLoadMore = true
                        }
                        weakSelf?.tblView.reloadData()
                    }
                }
            }
            
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    /*
    func callNotificationAPI(isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        var notificationID = "0"
        
        if let details = notificationDetailsArray.last{
            let detailsArray = details["details_list"] as! Array<NotificationsDetails>
            if let details = detailsArray.last{
                notificationID = details.notificationId
            }
        }
        
        let params = ["option": "notifications", "user_id": UserDetails.sharedInstance.userID, "last_id": notificationID]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                if let response = result!["response"]?.dictionary{
                    if let responseArray = response["notifications"]?.array{
                        
                        let detailsTempArray = NotificationsDetails.getAllNotificationList(responseArray: responseArray)
                        
                        let tempArray = detailsTempArray.enumerated().map { (index,element) in
                            element.notificationDate
                        }

                        let tempSetArray = Array(Set(tempArray))
                        let setArray = AppHelper.getTheSortedArray(dataArray: tempSetArray)
                        
                        let lastObj = weakSelf?.notificationDetailsArray.last
                        
                        for dateStr in setArray{
                            let transactionArray = detailsTempArray.filter({ (details) -> Bool in
                                details.notificationDate == dateStr
                            })
                            if lastObj != nil{
                                let dateTitle = lastObj!["date"] as! String
                                
                                if dateTitle == dateStr{
                                    let detailsArray = lastObj!["details_list"] as! Array<NotificationsDetails>
                                    let newTransactionArray = detailsArray + transactionArray
                                    weakSelf?.notificationDetailsArray.removeLast()
                                    let tempDetails = ["date": dateStr, "details_list": newTransactionArray] as [String : Any]
                                    weakSelf?.notificationDetailsArray.append(tempDetails)
                                }
                                else{
                                    let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                                    weakSelf?.notificationDetailsArray.append(tempDetails)
                                }
                            }
                            else{
                                let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                                weakSelf?.notificationDetailsArray.append(tempDetails)
                            }
                        }
                        
                        
                        if weakSelf?.notificationDetailsArray.count != 0{
                            weakSelf?.lblNoRecordFound.isHidden = true
                        }
                        else{
                            weakSelf?.lblNoRecordFound.isHidden = false
                        }
                        
                        if responseArray.count == 0{
                            weakSelf?.isNeedToShowLoadMore = false
                        }
                        else{
                            weakSelf?.isNeedToShowLoadMore = true
                        }
                        weakSelf?.tblView.reloadData()
                    }
                }
            }
            
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    */
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 38
//    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 38))
//        let titleLabel = UILabel(frame: CGRect(x: 0, y: 4, width: tableView.bounds.size.width, height: 34))
//        titleLabel.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
//        titleLabel.font = UIFont(name: "OpenSans-Semibold", size: 14)
//        headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
//        titleLabel.clipsToBounds = true;
//        headerView.addSubview(titleLabel)
//        headerView.clipsToBounds = true;
//        let transAction = notificationDetailsArray[section]
//        titleLabel.text = transAction["date"] as? String
//        titleLabel.textAlignment = .center
//        return headerView
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let transAction = notificationsArray[section]
//        let detailsArray = transAction["details_list"] as! Array<NotificationsDetails>
        
        if isNeedToShowLoadMore && (section == notificationsArray.count - 1) {
            return notificationsArray.count + 1;
        }

        return notificationsArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let transAction = notificationDetailsArray[indexPath.section]
//        let detailsArray = transAction["details_list"] as! Array<NotificationsDetails>

        if (indexPath.row == notificationsArray.count){
            var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            if isNeedToShowLoadMore{
                isNeedToShowLoadMore = false
                callNotificationAPI(isNeedToShowLoader: false)
            }
            
            return cell!

        }else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell") as? NotificationsTableViewCell
            if cell == nil {
                cell = NotificationsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "NotificationsTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            AppHelper.showShodowOnCellsView(innerView: cell!.innerView)
            let notificationDetails = notificationsArray[indexPath.row]
            cell?.lblTitle.text = notificationDetails.notificationTitle
            cell?.lblNotificationMessage.text = notificationDetails.notificationMessage.htmlToString
            cell?.lblDate.text = AppHelper.getNotificationFormattedTime(dateString: notificationDetails.notificationDateAndTime)
            if notificationDetails.notificationImage.count > 0 {
                cell?.imgViewHeightConstraint.constant = 140.0
                
                if let url = NSURL(string: UserDetails.sharedInstance.promotionImageUrl + notificationDetails.notificationImage){
                    cell?.imgView.setImage(with: url as URL, placeholder: UIImage(named: "ImagePlaceholder"), progress: { received, total in
                        // Report progress
                    }, completion: { [weak self] image in
                        if (image != nil){
                            cell?.imgView.image = image
                        }
                        else{
                            cell?.imgView.image = UIImage(named: "ImagePlaceholder")
                        }
                    })
                }
                else{
                    cell?.imgView.image = UIImage(named: "ImagePlaceholder")
                }
            }
            else{
                cell?.imgViewHeightConstraint.constant = 0.0
            }
            cell?.layoutIfNeeded()
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension UILabel{
    
}



