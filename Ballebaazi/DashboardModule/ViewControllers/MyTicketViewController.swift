//
//  MyTicketViewController.swift
//  Letspick
//
//  Created by MADSTECH on 16/02/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

enum TicketTabs: Int {
    case MyPasses = 100
    case MyTickets
}



class MyTicketViewController: UIViewController, UIScrollViewDelegate {
  
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var announcementView: AnnouncementView!
    @IBOutlet weak var ticketHeaderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ticketHeaderView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomSepratorLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var myPassesButton: UIButton!
    @IBOutlet weak var myTicketButton: UIButton!
    @IBOutlet weak var headerView: CustomNavigationBar!

    var userTicketsArray = Array<[String: AnyObject]>()
    var passesArray = Array<PassesDetails>()
    var isPassNotAvaileble = false
    var isPassFeatureHidden = false

    var selectedTab = TicketTabs.MyPasses.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "Passes & Tickets".localized()
        
        myPassesButton.setTitle("My Passes".localized(), for: .normal)
        myTicketButton.setTitle("My Tickets".localized(), for: .normal)
        
        collectionView.register(UINib(nibName: "MyTicketsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MyTicketsCollectionViewCell")
        collectionView.register(UINib(nibName: "MyPassesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MyPassesCollectionViewCell")
//        if isPassFeatureHidden {
//            headerView.headerTitle = "Tickets".localized()
//            collectionView.isScrollEnabled = false
//            myTicketsButtonTapped(nil)
//            ticketHeaderView.isHidden = true
//        }
//        else{
//            myPassesButtonTapped(nil)
//        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if isPassFeatureHidden {
            ticketHeaderViewHeightConstraint.constant = 0
            ticketHeaderView.layoutIfNeeded()
        }
        
        if isPassFeatureHidden {
            headerView.headerTitle = "Tickets".localized()
            collectionView.isScrollEnabled = false
            myTicketsButtonTapped(nil)
            ticketHeaderView.isHidden = true
        }
        else{
            if selectedTab == TicketTabs.MyTickets.rawValue{
                userTicketsArray.removeAll()
                myTicketsButtonTapped(nil)
            }
            else{
                myPassesButtonTapped(nil)
            }
        }
        
    }

    @IBAction func myTicketsButtonTapped(_ sender: Any?) {
        selectedTab = TicketTabs.MyTickets.rawValue
        if userTicketsArray.count == 0 {
            callMyTicketsAPI()
        }
        myTicketButton.alpha = 1.0
        myPassesButton.alpha = 0.6
        collectionView.reloadData()
        
        bottomSepratorLeadingConstraint.constant = UIScreen.main.bounds.width/2
        view.layoutIfNeeded()
        DispatchQueue.main.async {
            if sender == nil{
                self.collectionView.scrollToItem(at: IndexPath(row: 1, section: 0), at: .centeredHorizontally, animated: false)
            }
            else{
                self.collectionView.scrollToItem(at: IndexPath(row: 1, section: 0), at: .centeredHorizontally, animated: true)
            }
            self.collectionView.reloadData()
        }
    }
    
    @IBAction func myPassesButtonTapped(_ sender: Any?) {
        selectedTab = TicketTabs.MyPasses.rawValue
        if passesArray.count == 0 {
            callGetPassesStoreAPI(isNeedToShowLoader: true);
        }
        myTicketButton.alpha = 0.6
        myPassesButton.alpha = 1.0
        bottomSepratorLeadingConstraint.constant = 0
        view.layoutIfNeeded()
        
        DispatchQueue.main.async {
            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
            self.collectionView.reloadData()
        }

    }

    // MARK:- API Related Methods
    func callMyTicketsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "user_tickets", "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let message = result!["message"]?.string
                let statusCode = result!["status"]?.string
                if statusCode == "200"{
                
                    if let response = result!["response"]?.dictionary{
                        
                        if let announcement = response["announcement"]?.dictionary{
                            let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                            
                            self.announcementViewHeightConstraint.constant = 50.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: details.message)
                        }
                        else{
                            self.announcementViewHeightConstraint.constant = 0.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: "")
                        }
                        
                        
                        if let tempArray = response["active_tickets"]?.array {
                            var tempTicketsArray = Array<TicketDetails>()
                            (tempTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                            let filteredMatchSpecificRecords = tempTicketsArray.filter({ (details) -> Bool in
                                return (details.matchKey.count > 0) && (details.ticketType != "3")
                            })
                            
                            let filteredAnyMatchRecords = tempTicketsArray.filter({ (details) -> Bool in
                                return (details.matchKey.count == 0) && (details.ticketType != "3")
                            })
                            
                            if filteredMatchSpecificRecords.count > 0{
                                let matchSpecificDict = ["title": "Match Specific Tickets".localized(), "list": filteredMatchSpecificRecords] as [String : AnyObject]
                                weakSelf?.userTicketsArray.append(matchSpecificDict);
                            }
                            
                            if filteredAnyMatchRecords.count > 0{
                                let matchSpecificDict = ["title": "Any Match Tickets".localized(), "list": filteredAnyMatchRecords] as [String : AnyObject]
                                weakSelf?.userTicketsArray.append(matchSpecificDict);
                            }

                            weakSelf!.collectionView.reloadData()
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message!, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callGetPassesStoreAPI(isNeedToShowLoader: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }

        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }

        let params = ["option":"user_purchase_pass", "user_id": UserDetails.sharedInstance.userID]
        
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kPassesURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary{
                        
                        if let announcement = response["announcement"]?.dictionary{
                            let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                            
                            self.announcementViewHeightConstraint.constant = 50.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: details.message)
                        }
                        else{
                            self.announcementViewHeightConstraint.constant = 0.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: "")
                        }
                        
                        if let dataArray = response["pass_list"]?.array {
                            weakSelf?.passesArray = PassesDetails.getAllPasesDetails(dataArray: dataArray)
                            if dataArray.count == 0 {
                                weakSelf?.isPassNotAvaileble = true
                            }
                            
                            weakSelf?.collectionView.reloadData()
                        }
                        else{
                            weakSelf?.passesArray.removeAll()
                            weakSelf?.collectionView.reloadData()
                            if weakSelf?.passesArray.count == 0 {
                                weakSelf?.isPassNotAvaileble = true
                            }
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
        if Int(currentPage) == 0{
            if selectedTab != TicketTabs.MyPasses.rawValue {
                myPassesButtonTapped(nil)
            }
        }
        else if Int(currentPage) == 1{
            
            if selectedTab != TicketTabs.MyTickets.rawValue {
                myTicketsButtonTapped(nil)
            }

        }
    }
}

extension MyTicketViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyPassesCollectionViewCell", for: indexPath) as! MyPassesCollectionViewCell
            cell.purchasebutton.layer.cornerRadius = 0
            cell.configData(dataArray: passesArray, isPurchased: true)

            if isPassNotAvaileble {
                cell.purchaseMiddleButton.isHidden = false
                cell.purchasebutton.isHidden = true
                cell.lblNoRecordFound.isHidden = false
                cell.placeholderImgView.isHidden = false
            }
            else {
                cell.purchaseMiddleButton.isHidden = true
                cell.purchasebutton.isHidden = false
                cell.lblNoRecordFound.isHidden = true
                cell.placeholderImgView.isHidden = true
            }

            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyTicketsCollectionViewCell", for: indexPath) as! MyTicketsCollectionViewCell
            cell.configData(ticketsArray: userTicketsArray)
            return cell
        }

    }
}
