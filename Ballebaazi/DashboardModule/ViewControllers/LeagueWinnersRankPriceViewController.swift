//
//  LeagueWinnersRankPriceViewController.swift
//  Letspick
//
//  Created by Mads Technologies on 29/06/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LeagueWinnersRankPriceViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    

    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var bannerArray = Array<String>()

    var matchDetails: MatchDetails?
    var leagueID: String?
    lazy var leagueWinningAmount = ""
    lazy var winPerUser = ""
    lazy var leagueWinnerType = ""

    lazy var totalWinner = "0"

    lazy var winnersArray = Array<LeagueWinnersRank>()
    lazy var isInfinityLeague = false
    lazy var totalWinnersPercent = ""
    var bannerImageStr = ""
    
    var selectedGameType = GameType.Cricket.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.headerTitle = "Winners".localized();
        
        collectionView.register(UINib(nibName: "LeagueWinneraListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueWinneraListCollectionViewCell")
        collectionView.register(UINib(nibName: "LeaguePreviewDetailsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeaguePreviewDetailsCollectionViewCell")
        collectionView.register(UINib(nibName: "JackpotBannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JackpotBannerCollectionViewCell")

        callTotalWinnersRankAPI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func crossBackButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func callTotalWinnersRankAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var userString = kMatch;
        if selectedGameType == GameType.Kabaddi.rawValue {
            userString = kKabaddiMatchURL
        }
        else if selectedGameType == GameType.Football.rawValue {
            userString = kFootballMatchURL
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            userString = kBasketballMatchURL
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            userString = kBaseballMatchURL
        }

        let parameters: Parameters = ["option": "league_winners_v1","league_id": leagueID!]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: userString, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
   
                    weakSelf?.winnersArray = LeagueWinnersRank.getAllRankDetails(response: result!)
                    
                    if weakSelf!.isInfinityLeague{
                        if weakSelf!.leagueWinnerType == "dynamic_winner" {
                            let leagueRank = LeagueWinnersRank()
                            leagueRank.rankCount = "Top " + weakSelf!.totalWinnersPercent + "%"
                            leagueRank.winAmount = weakSelf!.winPerUser
                            weakSelf?.winnersArray.append(leagueRank)
                        }
                    }
                    else{
                        if weakSelf?.winnersArray.count == 0{
                            let winnerRank = LeagueWinnersRank()
                            winnerRank.winAmount = weakSelf?.leagueWinningAmount
                            winnerRank.rankCount = "Rank".localized() + " : 1"
                            weakSelf?.winnersArray.append(winnerRank)
                        }
                    }
    
                    weakSelf?.collectionView.reloadData()
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    //MARK:- CollectionView Data Source and Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if bannerArray.count > 0 {
            return 2
        }
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        if (indexPath.row == 0) && (bannerArray.count != 0){
            let size = CGSize(width: UIScreen.main.bounds.width, height: 90)
            return size
        }
        else{
            var height = 145.0;
            height = height + Double(winnersArray.count*50)
            let size = CGSize(width: UIScreen.main.bounds.width, height: CGFloat(height))
            return size
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (indexPath.row == 0) && (bannerArray.count != 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JackpotBannerCollectionViewCell", for: indexPath) as? JackpotBannerCollectionViewCell
            cell?.configData(dataArray: bannerArray)
            return cell!;
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueWinneraListCollectionViewCell", for: indexPath) as? LeagueWinneraListCollectionViewCell
            let leagueDetails = LeagueDetails()
            leagueDetails.winAmount = leagueWinningAmount
            leagueDetails.totalWinners = totalWinner
            if isInfinityLeague {
                leagueDetails.isInfinity = "1"
                leagueDetails.totalWinnersPercent = totalWinnersPercent
            }
            cell?.configData(details: leagueDetails, dataArray: winnersArray, isPreview: true, isQuiz: false)
            return cell!;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
