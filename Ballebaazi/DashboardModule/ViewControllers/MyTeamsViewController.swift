
//
//  MyTeamsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/11/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire

class MyTeamsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var creatTeamBottomButton: SolidButton!
    @IBOutlet weak var scoreViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scoreView: TeamScoreView!
    @IBOutlet var headerView: LetspickFantasyWithoutWalletHeaderView!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!

    lazy var userTeamsArray = Array<UserTeamDetails>()
    var timer: Timer?
    var matchDetails: MatchDetails?

    lazy var classicTeam = Array<UserTeamDetails>()
    lazy var battingTeam = Array<UserTeamDetails>()
    lazy var bowlingTeam = Array<UserTeamDetails>()
    lazy var reverseTeam = Array<UserTeamDetails>()
    lazy var wizardTeam = Array<UserTeamDetails>()

    var isFromJoinedLeague = false
    
    lazy var firstTabFantasyType = FantasyType.Classic.rawValue;
    lazy var secondTabFantasyType = FantasyType.Batting.rawValue;
    lazy var thirdTabFantasyType = FantasyType.Bowling.rawValue;
    lazy var fourthTabFantasyType = FantasyType.Reverse.rawValue;
    lazy var fifthTabFantasyType = FantasyType.Wizard.rawValue;
    lazy var tabsArray = Array<Int>()
    var selectedFantasyType = FantasyType.Classic.rawValue

    
    // MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: "CricketMyTeamCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CricketMyTeamCollectionViewCell")
        
        creatTeamBottomButton.setTitle("Create Team".localized(), for: .normal)
        if isFromJoinedLeague{
            creatTeamBottomButton.setTitle("FullFantasyScore".localized(), for: .normal)
        }
        
        classicTeam = userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "1"
        }
        
        battingTeam = userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "2"
        }
        
        bowlingTeam = userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "3"
        }
        
        reverseTeam = userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "4"
        }

        wizardTeam = userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "5"
        }

        weak var weakSelf = self

        headerView.clasicButtonTappedBlock { (status) in
            weakSelf?.classicButtonTapped(isAnimate: status)
        }
        
        headerView.battingButtonTappedBlock { (status) in
            weakSelf?.battingButtonTapped(isAnimate: status)
        }
        
        headerView.bowlingButtonTappedBlock { (status) in
            weakSelf?.bowlingButtonTapped(isAnimate: status)
        }
        
        headerView.reverseFantasyButtonTapped { (status) in
            weakSelf?.reverseButtonTapped(isAnimate: status)
        }

        headerView.wizardFantasyButtonTapped { (status) in
            weakSelf?.wizardButtonTapped(isAnimate: status)
        }

        headerView.updateMatchName(details: matchDetails)
        
        if userTeamsArray.count == 0 {
            creatTeamBottomButton.isHidden = true;
        }
        
        if matchDetails!.isMatchClosed{
            scoreView.showTeamScore(details: matchDetails!)
        }

        showScoreView()
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }


    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        DispatchQueue.main.async {
            if self.selectedFantasyType == FantasyType.Classic.rawValue {
                self.headerView.classicButtonTapped(isNeedToScroll: false)
            }
            else if self.selectedFantasyType == FantasyType.Batting.rawValue {
                self.headerView.battingButtonTapped(isNeedToScroll: false)
            }
            else if self.selectedFantasyType == FantasyType.Bowling.rawValue {
                self.headerView.bowlingButtonTapped(isNeedToScroll: false)
            }
            else if self.selectedFantasyType == FantasyType.Reverse.rawValue {
                self.headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
            }
            else if self.selectedFantasyType == FantasyType.Wizard.rawValue {
                self.headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
            }
        }

        updateTabsData()
    }
 
    
    // MARK:- IBAction Methods
    @IBAction func createNewTeamButtonTapped(_ sender: Any) {
        if isFromJoinedLeague {
            let fullFantasyScoreboard = storyboard?.instantiateViewController(withIdentifier: "FullFantasyScoreViewController") as? FullFantasyScoreViewController
            fullFantasyScoreboard?.matchKey = matchDetails!.matchKey
            fullFantasyScoreboard?.playerGender = matchDetails!.playersGender
            let details = LeagueDetails()
            if selectedFantasyType == FantasyType.Classic.rawValue {
                details.fantasyType = "1"
            }
            else if selectedFantasyType == FantasyType.Batting.rawValue {
                details.fantasyType = "2"
            }
            else if selectedFantasyType == FantasyType.Bowling.rawValue {
                details.fantasyType = "3"
            }
            else if selectedFantasyType == FantasyType.Reverse.rawValue {
                details.fantasyType = "4"
            }
            else if selectedFantasyType == FantasyType.Wizard.rawValue {
                details.fantasyType = "5"
            }

            UserDetails.sharedInstance.selectedPlayerList.removeAll()
            fullFantasyScoreboard?.fantasyType = details.fantasyType
            fullFantasyScoreboard?.matchDetails = matchDetails
            fullFantasyScoreboard?.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            fullFantasyScoreboard?.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            navigationController?.pushViewController(fullFantasyScoreboard!, animated: true)
        }
        else{
            UserDetails.sharedInstance.selectedPlayerList.removeAll()
            self.performSegue(withIdentifier: "SelectPlayerViewController", sender: nil)
        }
    }

    func classicButtonTapped(isAnimate: Bool) {

        var maxTeamAllow = 0;
        if self.selectedFantasyType == FantasyType.Classic.rawValue {
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForClassic;
        }
        else if self.selectedFantasyType == FantasyType.Batting.rawValue {
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBatting;
        }
        else if self.selectedFantasyType == FantasyType.Bowling.rawValue {
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBowling;
        }
        else if self.selectedFantasyType == FantasyType.Reverse.rawValue {
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForReverse;
        }
        else if self.selectedFantasyType == FantasyType.Wizard.rawValue {
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForWizard;
        }
        
        creatTeamBottomButton.isHidden = false
        selectedFantasyType = FantasyType.Classic.rawValue
        if classicTeam.count == 0{
            creatTeamBottomButton.isHidden = true
            if matchDetails!.isMatchClosed  {
                creatTeamBottomButton.isHidden = true
            }
        }
        else{
            if matchDetails!.isMatchClosed  {
                if isFromJoinedLeague{
                    creatTeamBottomButton.setTitle("FullFantasyScore".localized(), for: .normal)
                    creatTeamBottomButton.isHidden = false
                }
                else{
                    creatTeamBottomButton.isHidden = true
                }
            }
            else{
                if classicTeam.count >= maxTeamAllow{
                    creatTeamBottomButton.isHidden = true
                }
            }
            view.layoutIfNeeded()
        }
       
        var indexPathCount = 0

        if selectedFantasyType == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
            indexPathCount = 4
        }

        let indexPath = IndexPath(item: indexPathCount, section: 0)

        if isAnimate {
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }else{
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }
        
        if classicTeam.count > 1{
            headerView.backButtonTitle = "My Teams".localized()
        }
        else{
            headerView.backButtonTitle = "My Team".localized()
        }
    }
    
    func battingButtonTapped(isAnimate: Bool) {
        selectedFantasyType = FantasyType.Batting.rawValue
        creatTeamBottomButton.isHidden = false
        if battingTeam.count == 0{
            creatTeamBottomButton.isHidden = true
            if matchDetails!.isMatchClosed  {
                creatTeamBottomButton.isHidden = true
            }
        }
        else{
            if matchDetails!.isMatchClosed  {
                if isFromJoinedLeague{
                    creatTeamBottomButton.setTitle("FullFantasyScore".localized(), for: .normal)
                    creatTeamBottomButton.isHidden = false
                }
                else{
                    creatTeamBottomButton.isHidden = true
                }
            }
            else{
                creatTeamBottomButton.isHidden = false
                if battingTeam.count >= UserDetails.sharedInstance.maxTeamAllowedForBatting{
                    creatTeamBottomButton.isHidden = true
                }
            }
        }
        
        var indexPathCount = 0

        if selectedFantasyType == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
            indexPathCount = 4
        }
        let indexPath = IndexPath(item: indexPathCount, section: 0)

        if isAnimate {
            DispatchQueue.main.async {
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
        }
        else{
            DispatchQueue.main.async {
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
            }
        }
        
        if battingTeam.count > 1{
            headerView.backButtonTitle = "My Teams".localized()
        }
        else{
            headerView.backButtonTitle = "My Team".localized()
        }

    }
    
    func bowlingButtonTapped(isAnimate: Bool) {
        selectedFantasyType = FantasyType.Bowling.rawValue
        creatTeamBottomButton.isHidden = false
        if bowlingTeam.count == 0{
            creatTeamBottomButton.isHidden = true
            if matchDetails!.isMatchClosed  {
                creatTeamBottomButton.isHidden = true
            }
        }
        else{
            if matchDetails!.isMatchClosed  {
                if isFromJoinedLeague{
                    creatTeamBottomButton.setTitle("FullFantasyScore".localized(), for: .normal)
                    creatTeamBottomButton.isHidden = false
                }
                else{
                    creatTeamBottomButton.isHidden = true
                }
            }
            else{
                creatTeamBottomButton.isHidden = false
                if bowlingTeam.count >= UserDetails.sharedInstance.maxTeamAllowedForBowling{
                    creatTeamBottomButton.isHidden = true
                }
            }
            view.layoutIfNeeded()
        }
        
        var indexPathCount = 0

        if selectedFantasyType == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
            indexPathCount = 4
        }

        if isAnimate {
            DispatchQueue.main.async {
                let indexPath = IndexPath(item: indexPathCount, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
        }
        else{
            DispatchQueue.main.async {
                let indexPath = IndexPath(item: indexPathCount, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
            }
        }
        
        if bowlingTeam.count > 1{
            headerView.backButtonTitle = "My Teams".localized()
        }
        else{
            headerView.backButtonTitle = "My Team".localized()
        }
    }
    
    
    func reverseButtonTapped(isAnimate: Bool) {
        selectedFantasyType = FantasyType.Reverse.rawValue
        creatTeamBottomButton.isHidden = false
        if reverseTeam.count == 0{
            creatTeamBottomButton.isHidden = true
            if matchDetails!.isMatchClosed  {
                creatTeamBottomButton.isHidden = true
            }
        }
        else{
            if matchDetails!.isMatchClosed  {
                if isFromJoinedLeague{
                    creatTeamBottomButton.setTitle("FullFantasyScore".localized(), for: .normal)
                    creatTeamBottomButton.isHidden = false
                }
                else{
                    creatTeamBottomButton.isHidden = true
                }
            }
            else{
                creatTeamBottomButton.isHidden = false
                if reverseTeam.count >= UserDetails.sharedInstance.maxTeamAllowedForReverse{
                    creatTeamBottomButton.isHidden = true
                }
            }
            view.layoutIfNeeded()
        }
        
        var indexPathCount = 0

        if selectedFantasyType == firstTabFantasyType{
            indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
            indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
            indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
            indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
            indexPathCount = 4
        }

        if isAnimate {
            DispatchQueue.main.async {
                let indexPath = IndexPath(item: indexPathCount, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
        }
        else{
            DispatchQueue.main.async {
                let indexPath = IndexPath(item: indexPathCount, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
            }
        }
        
        if reverseTeam.count > 1{
            headerView.backButtonTitle = "My Teams".localized()
        }
        else{
            headerView.backButtonTitle = "My Team".localized()
        }
    }
    
    func wizardButtonTapped(isAnimate: Bool) {
        selectedFantasyType = FantasyType.Wizard.rawValue
        creatTeamBottomButton.isHidden = false
        if wizardTeam.count == 0{
            creatTeamBottomButton.isHidden = true
            if matchDetails!.isMatchClosed  {
                creatTeamBottomButton.isHidden = true
            }
        }
        else{
            if matchDetails!.isMatchClosed  {
                if isFromJoinedLeague{
                    creatTeamBottomButton.setTitle("FullFantasyScore".localized(), for: .normal)
                    creatTeamBottomButton.isHidden = false
                }
                else{
                    creatTeamBottomButton.isHidden = true
                }
            }
            else{
                creatTeamBottomButton.isHidden = false
                if wizardTeam.count >= UserDetails.sharedInstance.maxTeamAllowedForWizard{
                    creatTeamBottomButton.isHidden = true
                }
            }
            view.layoutIfNeeded()
        }
        
         
        var indexPathCount = 0

        if selectedFantasyType == firstTabFantasyType{
         indexPathCount = 0
        }
        else if selectedFantasyType == secondTabFantasyType{
         indexPathCount = 1
        }
        else if selectedFantasyType == thirdTabFantasyType{
         indexPathCount = 2
        }
        else if selectedFantasyType == fourthTabFantasyType{
         indexPathCount = 3
        }
        else if selectedFantasyType == fifthTabFantasyType{
         indexPathCount = 4
        }
        
        if isAnimate {
            DispatchQueue.main.async {
                let indexPath = IndexPath(item: indexPathCount, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
        }
        else{
            DispatchQueue.main.async {
                let indexPath = IndexPath(item: indexPathCount, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
            }
        }
        
        if wizardTeam.count > 1{
            headerView.backButtonTitle = "My Teams".localized()
        }
        else{
            headerView.backButtonTitle = "My Team".localized()
        }
    }

    //MARK:- CollectionView Delegates and Data Source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CricketMyTeamCollectionViewCell", for: indexPath) as? CricketMyTeamCollectionViewCell
 
        let leagueObj = LeagueDetails()

        let fantasyType = tabsArray[indexPath.row]

        if fantasyType == FantasyType.Classic.rawValue {
            leagueObj.fantasyType = "1"

            cell!.configData(userTeamsArray: classicTeam, fantasyType: FantasyType.Classic.rawValue, selectedMatchDetails: matchDetails, leagueInfo: leagueObj, teamAllowed: UserDetails.sharedInstance.maxTeamAllowedForClassic)
        }
        else if fantasyType == FantasyType.Batting.rawValue {
            leagueObj.fantasyType = "2"

            cell!.configData(userTeamsArray: battingTeam, fantasyType: FantasyType.Batting.rawValue, selectedMatchDetails: matchDetails, leagueInfo: leagueObj, teamAllowed: UserDetails.sharedInstance.maxTeamAllowedForBatting)
        }
        else if fantasyType == FantasyType.Bowling.rawValue {
            leagueObj.fantasyType = "3"

            cell!.configData(userTeamsArray: bowlingTeam, fantasyType: FantasyType.Bowling.rawValue, selectedMatchDetails: matchDetails, leagueInfo: leagueObj, teamAllowed: UserDetails.sharedInstance.maxTeamAllowedForBowling)
        }
        else if fantasyType == FantasyType.Reverse.rawValue {
            leagueObj.fantasyType = "4"

            cell!.configData(userTeamsArray: reverseTeam, fantasyType: FantasyType.Reverse.rawValue, selectedMatchDetails: matchDetails, leagueInfo: leagueObj, teamAllowed: UserDetails.sharedInstance.maxTeamAllowedForReverse)
        }
        else if fantasyType == FantasyType.Wizard.rawValue {
            leagueObj.fantasyType = "5"

            cell!.configData(userTeamsArray: wizardTeam, fantasyType: FantasyType.Wizard.rawValue, selectedMatchDetails: matchDetails, leagueInfo: leagueObj, teamAllowed: UserDetails.sharedInstance.maxTeamAllowedForWizard)
        }
        
        return cell!;
    }
    
    @objc func updateTimerVlaue()  {
        
        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(details: matchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
        }
    }

    //MARK:- Scroll View Delegates
    
     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
                 
         if collectionView == scrollView{
             let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
             if Int(currentPage) == 0{
                 let fansatyType = tabsArray[0]
                 if selectedFantasyType == fansatyType{
                     return;
                 }
                 selectedFantasyType = fansatyType
             }
             else if Int(currentPage) == 1{
                 let fansatyType = tabsArray[1]
                 if selectedFantasyType == fansatyType{
                     return;
                 }
                 selectedFantasyType = fansatyType
             }
             else if Int(currentPage) == 2{
                 let fansatyType = tabsArray[2]
                 if selectedFantasyType == fansatyType{
                     return;
                 }
                 selectedFantasyType = fansatyType
             }
             else if Int(currentPage) == 3{
                 let fansatyType = tabsArray[3]
                 if selectedFantasyType == fansatyType{
                     return;
                 }
                 selectedFantasyType = fansatyType
             }
             else if Int(currentPage) == 4{
                 let fansatyType = tabsArray[4]
                 if selectedFantasyType == fansatyType{
                     return;
                 }
                 selectedFantasyType = fansatyType
             }
             else if Int(currentPage) == 5{
                 let fansatyType = tabsArray[5]
                 if selectedFantasyType == fansatyType{
                     return;
                 }
                 selectedFantasyType = fansatyType
             }

             updateHeaderOnscroll(gameType: selectedFantasyType)
         }
     }
     
     func updateHeaderOnscroll(gameType: Int) {
         if gameType == FantasyType.Classic.rawValue{
             headerView.classicButtonTapped(isNeedToScroll: false)
         }
         else if gameType == FantasyType.Batting.rawValue{
             headerView.battingButtonTapped(isNeedToScroll: false)
         }
         else if gameType == FantasyType.Bowling.rawValue{
             headerView.bowlingButtonTapped(isNeedToScroll: false)
         }
         else if gameType == FantasyType.Reverse.rawValue{
             headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
         }
         else if gameType == FantasyType.Wizard.rawValue{
             headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
         }
     }
    
    func updateTabsData() {
        headerView.matchFantasyType = matchDetails?.matchFantasyType ?? ""
        headerView.isFantasyModeEnable = false
        headerView.updateTabsData()
        (firstTabFantasyType, secondTabFantasyType, thirdTabFantasyType, fourthTabFantasyType, fifthTabFantasyType) = AppHelper.getFantasyLandingOrder(fantasyType: matchDetails?.matchFantasyType ?? "")
        
        tabsArray.removeAll()
         if firstTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(firstTabFantasyType)
         }
         
         if secondTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(secondTabFantasyType)
         }

         if thirdTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(thirdTabFantasyType)
         }
         
         if fourthTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(fourthTabFantasyType)
         }
         
         if fifthTabFantasyType != FantasyType.None.rawValue {
             tabsArray.append(fifthTabFantasyType)
         }
        
        if tabsArray.count > 1 {
            headerViewHeightConstraint.constant = 90
            self.view.layoutIfNeeded()
            headerView.isFantasyModeEnable = true
        }
        collectionView.reloadData()
        headerView.updateMatchName(details: matchDetails!)
    }
    

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "SelectPlayerViewController" {
            let playerVC = segue.destination as! SelectPlayerViewController
            playerVC.matchDetails = matchDetails
            let leagueDetails = LeagueDetails()
            
            if selectedFantasyType == FantasyType.Classic.rawValue {
                leagueDetails.fantasyType = "1"
            }
            else if selectedFantasyType == FantasyType.Batting.rawValue {
                leagueDetails.fantasyType = "2"
            }
            else if selectedFantasyType == FantasyType.Bowling.rawValue {
                leagueDetails.fantasyType = "3"
            }
            else if selectedFantasyType == FantasyType.Reverse.rawValue {
                leagueDetails.fantasyType = "4"
            }
            else if selectedFantasyType == FantasyType.Wizard.rawValue {
                leagueDetails.fantasyType = "5"
            }

            playerVC.leagueDetails = leagueDetails
        }
    }
    
    func showScoreView() {
        if (matchDetails?.firstTeamLiveScore.count != 0) && (matchDetails?.secondTeamLiveScore.count != 0) && (matchDetails?.thirdTeamLiveScore.count != 0){
            self.scoreViewHeightConstraint.constant = 110
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        else  if (matchDetails?.firstTeamLiveScore.count != 0) && (matchDetails?.secondTeamLiveScore.count != 0) && (matchDetails?.fourthTeamLiveScore.count != 0){
            self.scoreViewHeightConstraint.constant = 110
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        else if (matchDetails?.firstTeamLiveScoreShortName.count == 0) && (matchDetails?.secondTeamLiveScoreShortName.count == 0){
            self.scoreViewHeightConstraint.constant = 0
            self.scoreView.isHidden = true
            self.scoreView.layoutIfNeeded()
        }
        else{
            self.scoreViewHeightConstraint.constant = 50.0
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
