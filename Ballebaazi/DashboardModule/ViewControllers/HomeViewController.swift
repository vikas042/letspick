//
//  HomeViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/6/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire


enum SelectedTab: Int {
    
    case HomeTab = 100
    case LeagueTab = 200
    case Leaderboard = 300
    case MyAccount = 400
    case MoreTab = 500
    case RewardTab = 600
}


class HomeViewController: UIViewController {

    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var annuncementView: AnnouncementView!
    @IBOutlet weak var collectionView: UICollectionView!
    private var indexOfCellBeforeDragging = 0

    lazy var cricketMatchsArray = Array<MatchDetails>()
    lazy var kabaddiMatchsArray = Array<MatchDetails>()
    lazy var footballMatchsArray = Array<MatchDetails>()
    lazy var quizMatchsArray = Array<MatchDetails>()
    lazy var baseballMatchsArray = Array<MatchDetails>()
    lazy var basketballMatchsArray = Array<MatchDetails>()

    lazy var cricketBannerArray = Array<BannerDetails>()
    lazy var kabaddiBannerArray = Array<BannerDetails>()
    lazy var footballBannerArray = Array<BannerDetails>()
    lazy var quizBannerArray = Array<BannerDetails>()
    lazy var baseballBannerArray = Array<BannerDetails>()
    lazy var basketballBannerArray = Array<BannerDetails>()

    var tabsArray = Array<Int>()
    var bannerHeight: CGFloat = 90
    var isCleverTapDataUpdated = false

    var cricketUrlString = kMatch + "?option=get_home_new_v1&screen_msg=1&season_key=&only_upcomings=1&user_id="
    var kabaddiUrlString = kKabaddiMatchURL + "?option=get_home_v1&screen_msg=1&season_key=&only_upcomings=1"
    var footballUrlString = kFootballMatchURL + "?option=get_home_v1&screen_msg=1&season_key=&only_upcomings=1"
    var quizUrlString = kQuizMatchURL +  "?option=get_match&screen_msg=1&season_key=&only_upcomings=1"
    
    var basketballUrlString = kBasketballMatchURL + "?option=get_home_v1&screen_msg=1&season_key=&only_upcomings=1&user_id="
    var baseballUrlString = kBaseballMatchURL + "?option=get_home_v1&screen_msg=1&season_key=&only_upcomings=1&user_id="
    
    lazy var selectedGameType = GameType.Cricket.rawValue
    var firstTabGameType = GameType.Cricket.rawValue;
    var secondTabGameType = GameType.Kabaddi.rawValue;
    var thirdTabGameType = GameType.Football.rawValue;
    var fourthTabGameType = GameType.Quiz.rawValue;
    var fifthTabGameType = GameType.Basketball.rawValue;
    var sixthTabGameType = GameType.Baseball.rawValue;

    
    //MARK:- View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewProperties()
        (firstTabGameType, secondTabGameType, thirdTabGameType, fourthTabGameType, fifthTabGameType, sixthTabGameType) = AppHelper.getLandingOrder()
        quizUrlString = quizUrlString + "&user_id=" + UserDetails.sharedInstance.userID
        AppxorEventHandler.logAppEvent(withName: "HomeScreenLaunch", info: nil)

        if firstTabGameType != GameType.None.rawValue {
            tabsArray.append(firstTabGameType)
        }
        
        if secondTabGameType != GameType.None.rawValue {
            tabsArray.append(secondTabGameType)
        }
        

        if thirdTabGameType != GameType.None.rawValue {
            tabsArray.append(thirdTabGameType)
        }
        
        if fourthTabGameType != GameType.None.rawValue {
            tabsArray.append(fourthTabGameType)
        }
        
        if fifthTabGameType != GameType.None.rawValue {
            tabsArray.append(fifthTabGameType)
        }
        
        if sixthTabGameType != GameType.None.rawValue {
            tabsArray.append(sixthTabGameType)
        }
    }

    func setupViewProperties() {
        AppHelper.getUserDetails()
        cricketUrlString = cricketUrlString + UserDetails.sharedInstance.userID
        basketballUrlString = basketballUrlString + UserDetails.sharedInstance.userID
        baseballUrlString = baseballUrlString + UserDetails.sharedInstance.userID

        NotificationCenter.default.addObserver(self, selector: #selector(self.homeButtonTapped), name: NSNotification.Name(rawValue: "homeButtonTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.homeGameTypeChangeNotification(notification:)), name: NSNotification.Name(rawValue: "homeGameTypeChangeNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateListOnPullToRefresh(notification:)), name: NSNotification.Name(rawValue: "updateListOnPullToRefresh"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userlogoutNotitification(notification:)), name: NSNotification.Name(rawValue: "userlogoutNotitification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeUpcomingMatchFromLocal(notification:)), name: NSNotification.Name(rawValue: "removeUpcomingMatchFromLocal"), object: nil)

        collectionView.register(UINib(nibName: "HomeMatchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeMatchCollectionViewCell")
        
        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
        UserDefaults.standard.set(false, forKey: "isUserOnUserName")
        UserDefaults.standard.synchronize()
        MixPanelEventsDetails.intialiseMixpanel()
        MixPanelEventsDetails.updateUsernamePeopleProperties()
    }
    
    @objc func removeUpcomingMatchFromLocal(notification: Notification) {

        guard let matchDetails = notification.object as? MatchDetails else {
            return
        }

        if selectedGameType == GameType.Cricket.rawValue {
            cricketMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
        }
        else if selectedGameType == GameType.Kabaddi.rawValue {
            kabaddiMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }

        }
        else if selectedGameType == GameType.Football.rawValue {
            footballMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
        }
        else if selectedGameType == GameType.Quiz.rawValue {
            quizMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            basketballMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            baseballMatchsArray.removeAll { (details) -> Bool in
                details.matchKey == matchDetails.matchKey
            }
        }
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

    func updateUserdetailsToCleverTap() {
                
        var profileData = [name: UserDetails.sharedInstance.name, identity: UserDetails.sharedInstance.userID, email_address: UserDetails.sharedInstance.emailAdress, device: PLATFORM_iPHONE, user_name: UserDetails.sharedInstance.userName, mobile_number: UserDetails.sharedInstance.phoneNumber,"Pan Status": UserDetails.sharedInstance.pan_verified, "Bank Status": UserDetails.sharedInstance.BankStatus, custome_name: UserDetails.sharedInstance.name, "Gender": UserDetails.sharedInstance.gender, "MSG-push": true] as [String: Any]
                           
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US")
       
        var gender = "Male"
        if UserDetails.sharedInstance.gender == "F"{
            gender = "Female"
        }

        let userInfo = ["Username": UserDetails.sharedInstance.userName, "Name": UserDetails.sharedInstance.name, "EmailID": UserDetails.sharedInstance.emailAdress, "Phone Number": UserDetails.sharedInstance.phoneNumber, "Gender": gender, "DateOfBirth": "", "ReferralCode": UserDetails.sharedInstance.referralCode ?? "", "UserId": UserDetails.sharedInstance.userID] as [String : AnyObject]
        profileData[dob] = UserDetails.sharedInstance.dob
        if UserDetails.sharedInstance.emailVerified {
            profileData["Email"] = UserDetails.sharedInstance.emailAdress
        }

        CleverTapEventDetails.updateProfile(updateProfile: profileData)
        AppxorEventHandler.setUserCustomInfo(userInfo: userInfo)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let dashboardVC = navigationController?.viewControllers.first as? DashboardViewController{
            if dashboardVC.preselectedTab == 100{
                if isCleverTapDataUpdated{
//                    showTableDataData(isTabClicked: false)
                }
            }
        }
    }

    @objc func userlogoutNotitification(notification: Notification)  {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK:- Custom Methods
    func showTableDataData(isTabClicked: Bool) {
        
        weak var weakSelf = self
        if selectedGameType == GameType.Cricket.rawValue {
            
            let savedResponse = AppHelper.getValueFromCoreData(urlString: cricketUrlString)
            if (savedResponse != nil) {
                
                if let response = savedResponse?.dictionary!["response"]{
                    
                    let tempArray = MatchDetails.getAllMatchDetails(responseResult: savedResponse!)
                    var quizArray = Array<MatchDetails>()
                    cricketMatchsArray.removeAll()

                    if let dataArray = response["quizes"].array{
                        quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                     }

                    if tempArray.count == 0 {
                        weakSelf?.cricketMatchsArray = quizArray
                    }
                    else{
                        var matchIndexCount = 0
                        
                        for index in 0 ..< (tempArray.count + quizArray.count){
                            var filteredArray = quizArray.filter { (details) -> Bool in
                                details.matchIndex == index
                            }
                            filteredArray.reverse()

                            if filteredArray.count > 0 {
                                weakSelf?.cricketMatchsArray.insert(contentsOf: filteredArray, at: index)
                                quizArray.removeAll { (details) -> Bool in
                                    details.matchIndex == index
                                }
                            }
                            else{
                                if matchIndexCount < tempArray.count {
                                    let matchDetails = tempArray[matchIndexCount]
                                    weakSelf?.cricketMatchsArray.append(matchDetails)
                                    matchIndexCount = matchIndexCount + 1
                                }
                            }
                        }
                        
                        weakSelf?.cricketMatchsArray.append(contentsOf: quizArray)
                    }

                    if let tempArray = response["user_tickets"].array {
                        let (userTicketsArray,_) = TicketDetails.getTicketDetails(dataArray: tempArray)
                        for matchDetails in cricketMatchsArray{
                            for ticketDetails in userTicketsArray{
                                if matchDetails.matchKey == ticketDetails.matchKey{
                                    matchDetails.isTicketAvailable = true
                                    if ticketDetails.ticketType == "3" {
                                        matchDetails.isPassAvailable = true
                                    }
                                    matchDetails.templateID = ticketDetails.templateID
                                }
                            }
                        }
                    }
                    
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                    
                                        
                     weakSelf?.cricketBannerArray.removeAll()
                     if let dataArray = response["first_time_banner"].array{
                         weakSelf?.cricketBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                     }
                     
                     if let dataArray = response["banners"].array{
                         weakSelf?.cricketBannerArray.append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                     }
                }

                DispatchQueue.global().async {
                    weakSelf?.callGetMatchesAPI(urlString: weakSelf?.cricketUrlString ?? "", isNeedToShowLoader: false)
                }

                DispatchQueue.main.async {
                    weakSelf?.collectionView.reloadData()
                }
            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")

                callGetMatchesAPI(urlString: weakSelf?.cricketUrlString ?? "", isNeedToShowLoader: true)
            }
        }
        else if selectedGameType == GameType.Kabaddi.rawValue{
            
            let savedResponse = AppHelper.getValueFromCoreData(urlString: kabaddiUrlString)
            if (savedResponse != nil) {
                if let response = savedResponse?.dictionary!["response"]{
                    kabaddiMatchsArray.removeAll()
                    let tempArray = MatchDetails.getAllMatchDetails(responseResult: savedResponse!)
                    var quizArray = Array<MatchDetails>()
                    
                    if let dataArray = response["quizes"].array{
                         quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                     }

                    var matchIndexCount = 0
                    if tempArray.count == 0 {
                        weakSelf?.kabaddiMatchsArray = quizArray
                    }
                    else{
                        for index in 0 ..< (tempArray.count + quizArray.count){
                            var filteredArray = quizArray.filter { (details) -> Bool in
                                details.matchIndex == index
                            }
                            filteredArray.reverse()

                            if filteredArray.count > 0 {
                                weakSelf?.kabaddiMatchsArray.insert(contentsOf: filteredArray, at: index)
                                quizArray.removeAll { (details) -> Bool in
                                    details.matchIndex == index
                                }
                            }
                            else{
                                if matchIndexCount < tempArray.count {
                                    let matchDetails = tempArray[matchIndexCount]
                                    weakSelf?.kabaddiMatchsArray.append(matchDetails)
                                    matchIndexCount = matchIndexCount + 1
                                }
                            }
                        }
                        weakSelf?.kabaddiMatchsArray.append(contentsOf: quizArray)
                    }

                    if let tempArray = response["user_tickets"].array {
                        let (userTicketsArray,_) = TicketDetails.getTicketDetails(dataArray: tempArray)
                        for matchDetails in kabaddiMatchsArray{
                            for ticketDetails in userTicketsArray{
                                if matchDetails.matchKey == ticketDetails.matchKey{
                                    matchDetails.isTicketAvailable = true
                                    if ticketDetails.ticketType == "3" {
                                        matchDetails.isPassAvailable = true
                                    }

                                    matchDetails.templateID = ticketDetails.templateID
                                }
                            }
                        }
                    }

                    weakSelf?.kabaddiBannerArray.removeAll()
                    if let dataArray = response["first_time_banner"].array{
                        weakSelf?.kabaddiBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                    }
                    
                    if let dataArray = response["banners"].array{
                        weakSelf?.kabaddiBannerArray.append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                    }

                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }

                DispatchQueue.global().async {
                    weakSelf?.callGetMatchesAPI(urlString: weakSelf?.kabaddiUrlString ?? "", isNeedToShowLoader: false)
                }
                
                DispatchQueue.main.async {
                    weakSelf?.collectionView.reloadData()
                }
            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                callGetMatchesAPI(urlString: weakSelf?.kabaddiUrlString ?? "", isNeedToShowLoader: true)
            }
        }
        else if selectedGameType == GameType.Football.rawValue{
            
            let savedResponse = AppHelper.getValueFromCoreData(urlString: footballUrlString)
            if (savedResponse != nil) {
                if let response = savedResponse?.dictionary!["response"]{
                    footballMatchsArray.removeAll()
                    let tempArray = MatchDetails.getAllMatchDetails(responseResult: savedResponse!)
                    var quizArray = Array<MatchDetails>()
                    
                    if let dataArray = response["quizes"].array{
                         quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                     }

                    if tempArray.count == 0 {
                        weakSelf?.footballMatchsArray = quizArray
                    }
                    else{
                        var matchIndexCount = 0
                        for index in 0 ..< (tempArray.count + quizArray.count){
                            
                        var filteredArray = quizArray.filter { (details) -> Bool in
                             details.matchIndex == index
                         }
                            
                        filteredArray.reverse()


                         if filteredArray.count > 0 {
                             weakSelf?.footballMatchsArray.insert(contentsOf: filteredArray, at: index)
                             quizArray.removeAll { (details) -> Bool in
                                 details.matchIndex == index
                             }
                         }
                         else{
                             if matchIndexCount < tempArray.count {
                                 let matchDetails = tempArray[matchIndexCount]
                                 weakSelf?.footballMatchsArray.append(matchDetails)
                                 matchIndexCount = matchIndexCount + 1
                             }
                         }
                        }
                        weakSelf?.footballMatchsArray.append(contentsOf: quizArray)
                    }

                    if let tempArray = response["user_tickets"].array {
                        let (userTicketsArray,_) = TicketDetails.getTicketDetails(dataArray: tempArray)
                        for matchDetails in footballMatchsArray{
                            for ticketDetails in userTicketsArray{
                                if matchDetails.matchKey == ticketDetails.matchKey{
                                    matchDetails.isTicketAvailable = true
                                    if ticketDetails.ticketType == "3" {
                                        matchDetails.isPassAvailable = true
                                    }
                                    matchDetails.templateID = ticketDetails.templateID
                                }
                            }
                        }
                    }
                    
                    weakSelf?.footballBannerArray.removeAll()
                    if let dataArray = response["first_time_banner"].array{
                        weakSelf?.footballBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                    }
                    
                    if let dataArray = response["banners"].array{
                        weakSelf?.footballBannerArray
                            .append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                    }
                    
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }
                
                DispatchQueue.global().async {
                    weakSelf?.callGetMatchesAPI(urlString: weakSelf?.footballUrlString ?? "", isNeedToShowLoader: false)
                }
                
                DispatchQueue.main.async {
                    weakSelf?.collectionView.reloadData()
                }
            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                callGetMatchesAPI(urlString: weakSelf?.footballUrlString ?? "", isNeedToShowLoader: true)
            }
        }
        else if selectedGameType == GameType.Quiz.rawValue{
            let savedResponse = AppHelper.getValueFromCoreData(urlString: quizUrlString)
            if (savedResponse != nil) {
                if let response = savedResponse?.dictionary!["response"]{
                    quizMatchsArray.removeAll()
                    weakSelf?.quizBannerArray.removeAll()
                    if let dataArray = response["first_time_banner"].array{
                        weakSelf?.quizBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                    }
                    
                    var quizArray = Array<MatchDetails>()
                    if let dataArray = response["active_match"].array{
                        quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: false)
                    }
                    quizMatchsArray = quizArray
                    
                    if let dataArray = response["banners"].array{
                        weakSelf?.quizBannerArray
                            .append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                    }
                    
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }

                DispatchQueue.global().async {
                    weakSelf?.callGetMatchesAPI(urlString: weakSelf?.quizUrlString ?? "", isNeedToShowLoader: false)
                }
                
                DispatchQueue.main.async {
                    weakSelf?.collectionView.reloadData()
                }
            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                callGetMatchesAPI(urlString: weakSelf?.quizUrlString ?? "", isNeedToShowLoader: true)
            }
        }
        else if selectedGameType == GameType.Basketball.rawValue{
            
            let savedResponse = AppHelper.getValueFromCoreData(urlString: basketballUrlString)
            if (savedResponse != nil) {
                if let response = savedResponse?.dictionary!["response"]{
                    basketballMatchsArray.removeAll()
                    let tempArray = MatchDetails.getAllMatchDetails(responseResult: savedResponse!)
                    var quizArray = Array<MatchDetails>()
                    
                    if let dataArray = response["quizes"].array{
                         quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                     }

                    if tempArray.count == 0 {
                        weakSelf?.basketballMatchsArray = quizArray
                    }
                    else{
                        var matchIndexCount = 0
                        for index in 0 ..< (tempArray.count + quizArray.count){
                            
                        var filteredArray = quizArray.filter { (details) -> Bool in
                             details.matchIndex == index
                         }
                            
                        filteredArray.reverse()


                         if filteredArray.count > 0 {
                             weakSelf?.basketballMatchsArray.insert(contentsOf: filteredArray, at: index)
                             quizArray.removeAll { (details) -> Bool in
                                 details.matchIndex == index
                             }
                         }
                         else{
                             if matchIndexCount < tempArray.count {
                                 let matchDetails = tempArray[matchIndexCount]
                                 weakSelf?.basketballMatchsArray.append(matchDetails)
                                 matchIndexCount = matchIndexCount + 1
                             }
                         }
                        }
                        weakSelf?.basketballMatchsArray.append(contentsOf: quizArray)
                    }

                    if let tempArray = response["user_tickets"].array {
                        let (userTicketsArray,_) = TicketDetails.getTicketDetails(dataArray: tempArray)
                        for matchDetails in basketballMatchsArray{
                            for ticketDetails in userTicketsArray{
                                if matchDetails.matchKey == ticketDetails.matchKey{
                                    matchDetails.isTicketAvailable = true
                                    if ticketDetails.ticketType == "3" {
                                        matchDetails.isPassAvailable = true
                                    }
                                    matchDetails.templateID = ticketDetails.templateID
                                }
                            }
                        }
                    }
                    
                    weakSelf?.basketballBannerArray.removeAll()
                    if let dataArray = response["first_time_banner"].array{
                        weakSelf?.basketballBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                    }
                    
                    if let dataArray = response["banners"].array{
                        weakSelf?.basketballBannerArray
                            .append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                    }
                    
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }
                
                DispatchQueue.global().async {
                    weakSelf?.callGetMatchesAPI(urlString: weakSelf?.basketballUrlString ?? "", isNeedToShowLoader: false)
                }
                
                DispatchQueue.main.async {
                    weakSelf?.collectionView.reloadData()
                }
            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                callGetMatchesAPI(urlString: weakSelf?.basketballUrlString ?? "", isNeedToShowLoader: true)
            }
        }
        else if selectedGameType == GameType.Baseball.rawValue{
            
            let savedResponse = AppHelper.getValueFromCoreData(urlString: baseballUrlString)
            if (savedResponse != nil) {
                if let response = savedResponse?.dictionary!["response"]{
                    baseballMatchsArray.removeAll()
                    let tempArray = MatchDetails.getAllMatchDetails(responseResult: savedResponse!)
                    var quizArray = Array<MatchDetails>()
                    
                    if let dataArray = response["quizes"].array{
                         quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                     }

                    if tempArray.count == 0 {
                        weakSelf?.baseballMatchsArray = quizArray
                    }
                    else{
                        var matchIndexCount = 0
                        for index in 0 ..< (tempArray.count + quizArray.count){
                            
                        var filteredArray = quizArray.filter { (details) -> Bool in
                             details.matchIndex == index
                         }
                            
                        filteredArray.reverse()
                         if filteredArray.count > 0 {
                             weakSelf?.baseballMatchsArray.insert(contentsOf: filteredArray, at: index)
                             quizArray.removeAll { (details) -> Bool in
                                 details.matchIndex == index
                             }
                         }
                         else{
                             if matchIndexCount < tempArray.count {
                                 let matchDetails = tempArray[matchIndexCount]
                                 weakSelf?.baseballMatchsArray.append(matchDetails)
                                 matchIndexCount = matchIndexCount + 1
                             }
                         }
                        }
                        weakSelf?.baseballMatchsArray.append(contentsOf: quizArray)
                    }

                    if let tempArray = response["user_tickets"].array {
                        let (userTicketsArray,_) = TicketDetails.getTicketDetails(dataArray: tempArray)
                        for matchDetails in baseballMatchsArray{
                            for ticketDetails in userTicketsArray{
                                if matchDetails.matchKey == ticketDetails.matchKey{
                                    matchDetails.isTicketAvailable = true
                                    if ticketDetails.ticketType == "3" {
                                        matchDetails.isPassAvailable = true
                                    }
                                    matchDetails.templateID = ticketDetails.templateID
                                }
                            }
                        }
                    }
                    
                    weakSelf?.baseballBannerArray.removeAll()
                    if let dataArray = response["first_time_banner"].array{
                        weakSelf?.baseballBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                    }
                    
                    if let dataArray = response["banners"].array{
                        weakSelf?.baseballBannerArray
                            .append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                    }
                    
                    if let announcement = response["announcement"].dictionary{
                        weakSelf?.announcementViewHeightConstraint.constant = 50.0
                        weakSelf?.view.layoutIfNeeded()
                        let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                        weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                    }
                    else{
                        weakSelf?.announcementViewHeightConstraint.constant = 0.0
                        weakSelf?.view.layoutIfNeeded()
                        weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                    }
                }
                
                DispatchQueue.global().async {
                    weakSelf?.callGetMatchesAPI(urlString: weakSelf?.baseballUrlString ?? "", isNeedToShowLoader: false)
                }
                DispatchQueue.main.async {
                    weakSelf?.collectionView.reloadData()
                }
            }
            else{
                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                weakSelf?.view.layoutIfNeeded()
                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                callGetMatchesAPI(urlString: weakSelf?.baseballUrlString ?? "", isNeedToShowLoader: true)
            }
        }
    }

    @objc func homeButtonTapped(notification: Notification)  {
        selectedGameType =  notification.object as! Int
        showTableDataData(isTabClicked: true)
        if selectedGameType == firstTabGameType{
            let indexpath = IndexPath(item: 0, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == secondTabGameType{
            let indexpath = IndexPath(item: 1, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == thirdTabGameType{
            let indexpath = IndexPath(item: 2, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == fourthTabGameType{
            let indexpath = IndexPath(item: 3, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == fifthTabGameType{
            let indexpath = IndexPath(item: 4, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == sixthTabGameType{
            let indexpath = IndexPath(item: 5, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }

        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    @objc func updateListOnPullToRefresh(notification: Notification)  {
        if selectedGameType == GameType.Cricket.rawValue {
            callGetMatchesAPI(urlString: cricketUrlString, isNeedToShowLoader: true)
        }
        else if selectedGameType == GameType.Kabaddi.rawValue {
            callGetMatchesAPI(urlString: kabaddiUrlString, isNeedToShowLoader: true)
        }
        else if selectedGameType == GameType.Football.rawValue {
            callGetMatchesAPI(urlString: footballUrlString, isNeedToShowLoader: true)
        }
        else if selectedGameType == GameType.Quiz.rawValue {
            callGetMatchesAPI(urlString: quizUrlString, isNeedToShowLoader: true)
        }
        else if selectedGameType == GameType.Basketball.rawValue {
            callGetMatchesAPI(urlString: basketballUrlString, isNeedToShowLoader: true)
        }
        else if selectedGameType == GameType.Baseball.rawValue {
            callGetMatchesAPI(urlString: baseballUrlString, isNeedToShowLoader: true)
        }

    }
    
    @objc func homeGameTypeChangeNotification(notification: Notification)  {
        selectedGameType =  notification.object as! Int
        showTableDataData(isTabClicked: false)
        if selectedGameType == firstTabGameType{
            let indexpath = IndexPath(item: 0, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == secondTabGameType{
            let indexpath = IndexPath(item: 1, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == thirdTabGameType{
            let indexpath = IndexPath(item: 2, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == fourthTabGameType{
            let indexpath = IndexPath(item: 3, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == fifthTabGameType{
            let indexpath = IndexPath(item: 4, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }
        else if selectedGameType == sixthTabGameType{
            let indexpath = IndexPath(item: 5, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
        }

        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }


    // MARK:- API Related Method
    func callGetMatchesAPI(urlString: String, isNeedToShowLoader: Bool)  {
        
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        
        weak var weakSelf = self

        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
                    
                    if !weakSelf!.isCleverTapDataUpdated{
                        weakSelf!.isCleverTapDataUpdated = true
                        weakSelf?.updateUserdetailsToCleverTap()
                    }
                    
                    DispatchQueue.main.async {
                        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
                        let tempArray = MatchDetails.getAllMatchDetails(responseResult: savedResponse!)
                        
                        if let response = savedResponse?["response"].dictionary{
                            
                            let cricketLineups = response["cc_lineups"]?.stringValue
                            let kabaddiLineups = response["kb_lineups"]?.stringValue
                            let footballLineups = response["fb_lineups"]?.stringValue
                            let basketballLineups = response["bk_lineups"]?.stringValue
                            let baseballLineups = response["bs_lineups"]?.stringValue
                            
                            UserDetails.sharedInstance.isCricketLineupsOut = false
                            UserDetails.sharedInstance.isKabaddiLineupsOut = false
                            UserDetails.sharedInstance.isFootballLineupsOut = false
                            UserDetails.sharedInstance.isBasketballLineupsOut = false
                            UserDetails.sharedInstance.isBaseballLineupsOut = false

                            if cricketLineups == "1" {
                                UserDetails.sharedInstance.isCricketLineupsOut = true
                            }
                            
                            if kabaddiLineups == "1" {
                                UserDetails.sharedInstance.isKabaddiLineupsOut = true
                            }

                            if footballLineups == "1" {
                                UserDetails.sharedInstance.isFootballLineupsOut = true
                            }
                            
                            if basketballLineups == "1" {
                                UserDetails.sharedInstance.isBasketballLineupsOut = true
                            }

                            if baseballLineups == "1" {
                                UserDetails.sharedInstance.isBaseballLineupsOut = true
                            }

                            if let announcement = response["announcement"]?.dictionary{
                                let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                                weakSelf?.announcementViewHeightConstraint.constant = 50.0
                                weakSelf?.view.layoutIfNeeded()
                                weakSelf?.annuncementView.showAnnouncementMessage(message: details.message)
                            }
                            else{
                                weakSelf?.announcementViewHeightConstraint.constant = 0.0
                                weakSelf?.view.layoutIfNeeded()
                                weakSelf?.annuncementView.showAnnouncementMessage(message: "")
                            }
                        }
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateLineupsNotification"), object: nil)
                        if weakSelf?.cricketUrlString == urlString{
                            if let response = savedResponse?["response"].dictionary{
                                weakSelf?.cricketMatchsArray.removeAll()
                                weakSelf?.cricketBannerArray.removeAll()
                                if let dataArray = response["first_time_banner"]?.array{
                                    weakSelf?.cricketBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                                }

                                var quizArray = Array<MatchDetails>()
                                if let dataArray = response["quizes"]?.array{
                                    quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                                }
                                
                                if tempArray.count == 0 {
                                    weakSelf?.cricketMatchsArray = quizArray
                                }
                                else{
                                    
                                    var matchIndexCount = 0
                                     for index in 0 ..< (tempArray.count + quizArray.count){
                                        
                                        var filteredArray = quizArray.filter { (details) -> Bool in
                                            details.matchIndex == index
                                        }

                                        filteredArray.reverse()
                                         if filteredArray.count > 0 {
                                             weakSelf?.cricketMatchsArray.insert(contentsOf: filteredArray, at: index)
                                             quizArray.removeAll { (details) -> Bool in
                                                 details.matchIndex == index
                                             }
                                         }
                                         else{
                                             if matchIndexCount < tempArray.count {
                                                 let matchDetails = tempArray[matchIndexCount]
                                                 weakSelf?.cricketMatchsArray.append(matchDetails)
                                                 matchIndexCount = matchIndexCount + 1
                                             }
                                         }
                                     }

                                     weakSelf?.cricketMatchsArray.append(contentsOf: quizArray)
                                }
                                
                                if let dataArray = response["banners"]?.array{
                                    weakSelf?.cricketBannerArray.append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                                }
                                
                                if let tempArray = response["user_tickets"]?.array {
                                    let (userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                                    for matchDetails in weakSelf!.cricketMatchsArray{
                                        for ticketDetails in userTicketsArray{
                                            if matchDetails.matchKey == ticketDetails.matchKey{
                                                matchDetails.isTicketAvailable = true
                                                if ticketDetails.ticketType == "3" {
                                                    matchDetails.isPassAvailable = true
                                                }
                                                matchDetails.templateID = ticketDetails.templateID
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if weakSelf?.kabaddiUrlString == urlString{
                            if let response = savedResponse?["response"].dictionary{
                                weakSelf?.kabaddiMatchsArray.removeAll()
                                weakSelf?.kabaddiBannerArray.removeAll()
                                if let dataArray = response["first_time_banner"]?.array{
                                    weakSelf?.kabaddiBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                                }
                                
                                var quizArray = Array<MatchDetails>()
                                if let dataArray = response["quizes"]?.array{
                                    quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                                }
                                
                                                                
                                if tempArray.count == 0 {
                                    weakSelf?.kabaddiMatchsArray = quizArray
                                }
                                else{
                                    var matchIndexCount = 0
                                    
                                    for index in 0 ..< (tempArray.count + quizArray.count){
                                        var filteredArray = quizArray.filter { (details) -> Bool in
                                            details.matchIndex == index
                                        }
                                        filteredArray.reverse()

                                        if filteredArray.count > 0 {
                                            weakSelf?.kabaddiMatchsArray.insert(contentsOf: filteredArray, at: index)
                                            quizArray.removeAll { (details) -> Bool in
                                                details.matchIndex == index
                                            }
                                        }
                                        else{
                                            if matchIndexCount < tempArray.count {
                                                let matchDetails = tempArray[matchIndexCount]
                                                weakSelf?.kabaddiMatchsArray.append(matchDetails)
                                                matchIndexCount = matchIndexCount + 1
                                            }
                                        }
                                    }

                                    weakSelf?.kabaddiMatchsArray.append(contentsOf: quizArray)
                                }
                                
                                if let dataArray = response["banners"]?.array{
                                    weakSelf?.kabaddiBannerArray.append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                                }
                                
                                if let tempArray = response["user_tickets"]?.array {
                                    let (userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                                    for matchDetails in weakSelf!.kabaddiMatchsArray{
                                        for ticketDetails in userTicketsArray{
                                            if matchDetails.matchKey == ticketDetails.matchKey{
                                                matchDetails.isTicketAvailable = true
                                                if ticketDetails.ticketType == "3" {
                                                    matchDetails.isPassAvailable = true
                                                }
                                                matchDetails.templateID = ticketDetails.templateID
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if weakSelf?.footballUrlString == urlString{
                            if let response = savedResponse?["response"].dictionary{
                                weakSelf?.footballMatchsArray.removeAll()
                                weakSelf?.footballBannerArray.removeAll()
                                if let dataArray = response["first_time_banner"]?.array{
                                    weakSelf?.footballBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                                }
                                
                                var quizArray = Array<MatchDetails>()
                                if let dataArray = response["quizes"]?.array{
                                    quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                                }
                                
                                if tempArray.count == 0 {
                                    weakSelf?.footballMatchsArray = quizArray
                                }
                                else{
                                    var matchIndexCount = 0
                                    
                                    for index in 0 ..< (tempArray.count + quizArray.count){
                                        var filteredArray = quizArray.filter { (details) -> Bool in
                                            details.matchIndex == index
                                        }
                                        filteredArray.reverse()

                                        if filteredArray.count > 0 {
                                            weakSelf?.footballMatchsArray.insert(contentsOf: filteredArray, at: index)
                                            quizArray.removeAll { (details) -> Bool in
                                                details.matchIndex == index
                                            }
                                        }
                                        else{
                                            if matchIndexCount < tempArray.count {
                                                let matchDetails = tempArray[matchIndexCount]
                                                weakSelf?.footballMatchsArray.append(matchDetails)
                                                matchIndexCount = matchIndexCount + 1
                                            }
                                        }
                                    }

                                    weakSelf?.footballMatchsArray.append(contentsOf: quizArray)
                                }
                                

                                if let dataArray = response["banners"]?.array{
                                    weakSelf?.footballBannerArray.append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                                }
                                
                                if let tempArray = response["user_tickets"]?.array {
                                    let (userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                                    for matchDetails in weakSelf!.footballMatchsArray{
                                        for ticketDetails in userTicketsArray{
                                            if matchDetails.matchKey == ticketDetails.matchKey{
                                                matchDetails.isTicketAvailable = true
                                                if ticketDetails.ticketType == "3" {
                                                    matchDetails.isPassAvailable = true
                                                }
                                                matchDetails.templateID = ticketDetails.templateID
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if weakSelf?.basketballUrlString == urlString{
                            if let response = savedResponse?["response"].dictionary{
                                weakSelf?.basketballMatchsArray.removeAll()
                                weakSelf?.basketballBannerArray.removeAll()
                                if let dataArray = response["first_time_banner"]?.array{
                                    weakSelf?.basketballBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                                }
                                
                                var quizArray = Array<MatchDetails>()
                                if let dataArray = response["quizes"]?.array{
                                    quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                                }
                                
                                if tempArray.count == 0 {
                                    weakSelf?.basketballMatchsArray = quizArray
                                }
                                else{
                                    var matchIndexCount = 0
                                    
                                    for index in 0 ..< (tempArray.count + quizArray.count){
                                        var filteredArray = quizArray.filter { (details) -> Bool in
                                            details.matchIndex == index
                                        }
                                        filteredArray.reverse()

                                        if filteredArray.count > 0 {
                                            weakSelf?.basketballMatchsArray.insert(contentsOf: filteredArray, at: index)
                                            quizArray.removeAll { (details) -> Bool in
                                                details.matchIndex == index
                                            }
                                        }
                                        else{
                                            if matchIndexCount < tempArray.count {
                                                let matchDetails = tempArray[matchIndexCount]
                                                weakSelf?.basketballMatchsArray.append(matchDetails)
                                                matchIndexCount = matchIndexCount + 1
                                            }
                                        }
                                    }

                                    weakSelf?.basketballMatchsArray.append(contentsOf: quizArray)
                                }
                                

                                if let dataArray = response["banners"]?.array{
                                    weakSelf?.basketballBannerArray.append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                                }
                                
                                if let tempArray = response["user_tickets"]?.array {
                                    let (userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                                    for matchDetails in weakSelf!.basketballMatchsArray{
                                        for ticketDetails in userTicketsArray{
                                            if matchDetails.matchKey == ticketDetails.matchKey{
                                                matchDetails.isTicketAvailable = true
                                                if ticketDetails.ticketType == "3" {
                                                    matchDetails.isPassAvailable = true
                                                }
                                                matchDetails.templateID = ticketDetails.templateID
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if weakSelf?.baseballUrlString == urlString{
                            if let response = savedResponse?["response"].dictionary{
                                weakSelf?.baseballMatchsArray.removeAll()
                                weakSelf?.baseballBannerArray.removeAll()
                                if let dataArray = response["first_time_banner"]?.array{
                                    weakSelf?.baseballBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                                }
                                
                                var quizArray = Array<MatchDetails>()
                                if let dataArray = response["quizes"]?.array{
                                    quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: true)
                                }
                                
                                if tempArray.count == 0 {
                                    weakSelf?.baseballMatchsArray = quizArray
                                }
                                else{
                                    var matchIndexCount = 0
                                    
                                    for index in 0 ..< (tempArray.count + quizArray.count){
                                        var filteredArray = quizArray.filter { (details) -> Bool in
                                            details.matchIndex == index
                                        }
                                        filteredArray.reverse()

                                        if filteredArray.count > 0 {
                                            weakSelf?.baseballMatchsArray.insert(contentsOf: filteredArray, at: index)
                                            quizArray.removeAll { (details) -> Bool in
                                                details.matchIndex == index
                                            }
                                        }
                                        else{
                                            if matchIndexCount < tempArray.count {
                                                let matchDetails = tempArray[matchIndexCount]
                                                weakSelf?.baseballMatchsArray.append(matchDetails)
                                                matchIndexCount = matchIndexCount + 1
                                            }
                                        }
                                    }

                                    weakSelf?.baseballMatchsArray.append(contentsOf: quizArray)
                                }
                                

                                if let dataArray = response["banners"]?.array{
                                    weakSelf?.baseballBannerArray.append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                                }
                                
                                if let tempArray = response["user_tickets"]?.array {
                                    let (userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                                    for matchDetails in weakSelf!.baseballMatchsArray{
                                        for ticketDetails in userTicketsArray{
                                            if matchDetails.matchKey == ticketDetails.matchKey{
                                                matchDetails.isTicketAvailable = true
                                                if ticketDetails.ticketType == "3" {
                                                    matchDetails.isPassAvailable = true
                                                }
                                                matchDetails.templateID = ticketDetails.templateID
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if weakSelf?.quizUrlString == urlString{
                            if let response = savedResponse?["response"].dictionary{
                                weakSelf?.quizMatchsArray.removeAll()
                                weakSelf?.quizBannerArray.removeAll()
                                if let dataArray = response["first_time_banner"]?.array{
                                    weakSelf?.quizBannerArray = BannerDetails.getPromoBannerDetails(dataArray: dataArray)
                                }
                                
                                var quizArray = Array<MatchDetails>()
                                if let dataArray = response["active_match"]?.array{
                                    quizArray = MatchDetails.getAllQuizArray(dataArray: dataArray, isNeedToSort: false)
                                }
                                
                                weakSelf?.quizMatchsArray = quizArray
                                
                                if let dataArray = response["banners"]?.array{
                                    weakSelf?.quizBannerArray.append(contentsOf: BannerDetails.getPromoBannerDetails(dataArray: dataArray))
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            weakSelf?.collectionView.reloadData()
                        }
                    }
                    
                    if let maintenance = result!["under_maintenance"].string{
                    
                        if maintenance != "yes"{
                            var bannerVisibleDuration: Double = 0
                            if let duration = result!["popup_banner_duration"].string{
                                bannerVisibleDuration = Double(duration) ?? 0
                                if bannerVisibleDuration != 0{
                                    bannerVisibleDuration = bannerVisibleDuration/1000
                                }
                            }

                            if let bannerTimestamp = UserDefaults.standard.value(forKey: "bannerTimestamp") as? Double{
                                let timestamp = NSDate().timeIntervalSince1970
                                if (timestamp - bannerTimestamp) > bannerVisibleDuration {
                                    DispatchQueue.global().async {
                                        weakSelf?.callGetBannerDetailsAPI()
                                    }
                                }
                            }
                            else{
                                DispatchQueue.global().async {
                                    weakSelf?.callGetBannerDetailsAPI()
                                }
                            }
                        }
                    }
                }
            }
        }        
    }
    
    
    func callGetBannerDetailsAPI() {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        var isNewSignup = "0"
        if APPDELEGATE.isNewUserRegister{
            isNewSignup = "1"
        }
        
        let params = ["option": "promo_banners","signup": isNewSignup]
        WebServiceHandler.performPOSTRequest(urlString: kPmomotionUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                        if let dataArray = response["banners"]?.array{
                            if dataArray.count > 0{
                                let bannerDetails = BannerDetails.parseBannerDetails(details: dataArray[0])
                                DispatchQueue.main.async {
//                                    if !APPDELEGATE.isNewUserRegister{
                                        let timestamp = NSDate().timeIntervalSince1970
                                        APPDELEGATE.isNewUserRegister = false
                                        UserDefaults.standard.set(timestamp, forKey: "bannerTimestamp")
                                        UserDefaults.standard.synchronize()
//                                    }
                                    
                                    if UserDetails.sharedInstance.userLoggedIn{
                                        if !UserDetails.sharedInstance.isForceUpdateViewOpen{
                                            return
                                        }
                                    }
                                    
                                    let bannerView = BannerView(frame: APPDELEGATE.window!.frame)
                                    bannerView.bannerDetails = bannerDetails
                                    if let filePathDict = result!["file_path"]?.dictionary{
                                        bannerView.filePath = filePathDict["promotion_images"]?.string ?? ""
                                    }
                                    bannerView.updateData()
                                    APPDELEGATE.window?.addSubview(bannerView)
                                }
                            }
                        }
                    }
                    
                }
            }
        }
    }
    
    func callMyTicketsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        let params = ["option": "user_tickets", "user_id": UserDetails.sharedInstance.userID]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200"{
                    
                    if let response = result!["response"]?.dictionary{
                        if let tempArray = response["user_tickets"]?.array {
                            let (userTicketsArray, _) = TicketDetails.getTicketDetails(dataArray: tempArray)
                            if weakSelf?.selectedGameType == GameType.Cricket.rawValue{
                                for matchDetails in weakSelf!.cricketMatchsArray{
                                    for ticketDetails in userTicketsArray{
                                        if matchDetails.matchKey == ticketDetails.matchKey{
                                            matchDetails.isTicketAvailable = true
                                            if ticketDetails.ticketType == "3" {
                                                matchDetails.isPassAvailable = true
                                            }
                                            matchDetails.templateID = ticketDetails.templateID
                                        }
                                    }
                                }
                            }
                            else if weakSelf?.selectedGameType == GameType.Kabaddi.rawValue{
                                
                            }
                            else if weakSelf?.selectedGameType == GameType.Football.rawValue{
                                
                            }
                        }
                    }
                }
            }
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    // MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LeagueViewController" {
            let leagueVC = segue.destination as! LeagueViewController
            leagueVC.matchDetails = (sender as! MatchDetails)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        if let dashboardVC = self.navigationController?.viewControllers.first as? DashboardViewController{
            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
            if Int(currentPage) == 0{
                let gameType = tabsArray[0]
                
                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 1{
                let gameType = tabsArray[1]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 2{
                let gameType = tabsArray[2]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 3{
                let gameType = tabsArray[3]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 4{
                let gameType = tabsArray[4]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }
            else if Int(currentPage) == 5{
                let gameType = tabsArray[5]

                if selectedGameType == gameType{
                    return;
                }
                selectedGameType = gameType
            }

            dashboardVC.updateHeaderOnscroll(gameType: selectedGameType)
        }
    }
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeMatchCollectionViewCell", for: indexPath) as! HomeMatchCollectionViewCell
        let gameType = tabsArray[indexPath.row]
        
        if gameType == GameType.Cricket.rawValue {
            cell.configData(matchArray: cricketMatchsArray, bannersList: cricketBannerArray, gameType: GameType.Cricket.rawValue)
        }
        else if gameType == GameType.Kabaddi.rawValue {
            cell.configData(matchArray: kabaddiMatchsArray, bannersList: kabaddiBannerArray, gameType: GameType.Kabaddi.rawValue)
        }
        else if gameType == GameType.Football.rawValue {
            cell.configData(matchArray: footballMatchsArray, bannersList: footballBannerArray, gameType: GameType.Football.rawValue)
        }
        else if gameType == GameType.Quiz.rawValue {
            cell.configData(matchArray: quizMatchsArray, bannersList: quizBannerArray, gameType: GameType.Quiz.rawValue)
        }
        else if gameType == GameType.Basketball.rawValue {
            cell.configData(matchArray: basketballMatchsArray, bannersList: basketballBannerArray, gameType: GameType.Basketball.rawValue)
        }
        else if gameType == GameType.Baseball.rawValue {
            cell.configData(matchArray: baseballMatchsArray, bannersList: baseballBannerArray, gameType: GameType.Baseball.rawValue)
        }
        else if gameType == GameType.None.rawValue {
            cell.configData(matchArray: nil, bannersList: nil, gameType: selectedGameType)
        }
        else{
            cell.configData(matchArray: cricketMatchsArray, bannersList: cricketBannerArray, gameType: GameType.Cricket.rawValue)
        }
        
        return cell
    }
    
}



