//
//  VerifyDocumentsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import MessageUI

enum SelectedDocumentType: Int {
    case Email = 50
    case Phone = 100
    case PAN = 200
    case BANK = 300
    case ADHAR = 400
}

class VerifyDocumentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {

    var selectedType = 1000
    
    @IBOutlet weak var announcementView: AnnouncementView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var announcementViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblViewBottonConstraint: NSLayoutConstraint!
    @IBOutlet weak var supportMailView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var aadhaarDetails: AadhaarDetails?
    var bankDetails: BankDetails?
    var panDetails: PanDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.headerTitle = "Verifications".localized();
        navigationController?.navigationBar.isHidden = true        
        UserDetails.sharedInstance.getAllState()
        
        tblView.register(UINib(nibName: "EmailVerificationTableViewCell", bundle: nil), forCellReuseIdentifier: "EmailVerificationTableViewCell")
        tblView.register(UINib(nibName: "PhoneVerificationTableViewCell", bundle: nil), forCellReuseIdentifier: "PhoneVerificationTableViewCell")

        tblView.register(UINib(nibName: "BankVerificationTableViewCell", bundle: nil), forCellReuseIdentifier: "BankVerificationTableViewCell")

        tblView.register(UINib(nibName: "PANVerificationTableViewCell", bundle: nil), forCellReuseIdentifier: "PANVerificationTableViewCell")

        tblView.register(UINib(nibName: "AdharVerificationTableViewCell", bundle: nil), forCellReuseIdentifier: "AdharVerificationTableViewCell")
        NotificationCenter.default.addObserver(self, selector: #selector(updateTableWithStatus(notification:)), name: Notification.Name("updateTableWithStatus"), object: nil)
        callUpatePersonalDetailsAPI(isNeedToShowLoader: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDetails.sharedInstance.emailVerified && UserDetails.sharedInstance.phoneVerified && UserDetails.sharedInstance.panVerified && UserDetails.sharedInstance.bankVerified && UserDetails.sharedInstance.aadhaarVerified {
            supportMailView.isHidden = true
            tblViewBottonConstraint.constant = 0;
            tblView.layoutIfNeeded()
            view.layoutIfNeeded()
        }
    }
    
    func callUpatePersonalDetailsAPI(isNeedToShowLoader: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let params = ["option": "get_profile", "screen_msg": "1" ,"user_id": UserDetails.sharedInstance.userID]
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    if let response = result!["response"]{
                        if let announcementDict = response["announcement"].dictionary{
                            if let announcement = announcementDict["1"]?.dictionary{
                                let details = AnnouncementDetails.parseAnnoumentDetails(details: announcement)
                                self.announcementViewHeightConstraint.constant = 50.0
                                self.view.layoutIfNeeded()
                                self.announcementView.showAnnouncementMessage(message: details.message)
                            }
                        }
                        else{
                            self.announcementViewHeightConstraint.constant = 0.0
                            self.view.layoutIfNeeded()
                            self.announcementView.showAnnouncementMessage(message: "")
                        }

                        if let active_ticket = response.dictionary!["active_tickets"]?.stringValue{
                            UserDetails.sharedInstance.active_tickets = active_ticket
                        }
                        else {
                            UserDetails.sharedInstance.active_tickets = "0"
                        }                        
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
            
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    @objc func updateTableWithStatus(notification: Notification)  {
        
        selectedType = 1000
        tblView.reloadData()
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedType == SelectedDocumentType.Email.rawValue {
            if indexPath.row == 1{
                if UserDetails.sharedInstance.emailVerified{
                    return 55.0
                }
                return 160.0
            }
        }
        else if selectedType == SelectedDocumentType.PAN.rawValue {
            if indexPath.row == 2{
                if panDetails != nil{
                    return 285
                }
                return 340
            }
        }
        else if selectedType == SelectedDocumentType.BANK.rawValue {
            if indexPath.row == 4{
                if bankDetails != nil{
                    return 375.0
                }
                return 425.0
            }
        }
        else if selectedType == SelectedDocumentType.ADHAR.rawValue {
            if indexPath.row == 3{
                if aadhaarDetails != nil{
                    return 595.0
                }
                return 790.0
            }
        }
        
        if indexPath.row == 1{
            return 70.0
        }
        
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 1 {
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "EmailVerificationTableViewCell") as? EmailVerificationTableViewCell
            
            if cell == nil {
                cell = EmailVerificationTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "EmailVerificationTableViewCell")
            }
            
            cell?.dropDownButton.tag = indexPath.row
            cell?.dropDownButton.addTarget(self, action: #selector(self.dropDownButtonTapped(button:)), for: .touchUpInside)
            
            if selectedType == SelectedDocumentType.Email.rawValue{
                if !UserDetails.sharedInstance.emailVerified{
                    cell?.fieldsContainerView.isHidden = false
                }
            }
            else{
                cell?.fieldsContainerView.isHidden = true
            }
            cell?.txtFieldEmail.text = UserDetails.sharedInstance.emailAdress

            if UserDetails.sharedInstance.emailVerified{
                cell?.submitButton.isHidden = true
                cell?.txtFieldEmail.isUserInteractionEnabled = false
                cell?.lblEmail.isHidden = false
            }
            else{
                cell?.submitButton.isHidden = false
                cell?.txtFieldEmail.isUserInteractionEnabled = true
                cell?.lblEmail.isHidden = true
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.tag = indexPath.row
            cell?.configData()
            return cell!
        }
        else if indexPath.row == 0{
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "PhoneVerificationTableViewCell") as? PhoneVerificationTableViewCell
            
            if cell == nil {
                cell = PhoneVerificationTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PhoneVerificationTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
//            cell?.dropDownButton.tag = indexPath.row
//            cell?.dropDownButton.addTarget(self, action: #selector(self.dropDownButtonTapped(button:)), for: .touchUpInside)

            cell?.tag = indexPath.row
            cell?.configData()
            return cell!
        }
        else if indexPath.row == 2{
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "PANVerificationTableViewCell") as? PANVerificationTableViewCell
            
            if cell == nil {
                cell = PANVerificationTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PANVerificationTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.tag = indexPath.row
            cell?.dropDownButton.tag = indexPath.row
            cell?.dropDownButton.addTarget(self, action: #selector(self.dropDownButtonTapped(button:)), for: .touchUpInside)
            
            if selectedType == SelectedDocumentType.PAN.rawValue{
                 cell?.fieldsContainerView.isHidden = false
            }
            else{
                 cell?.fieldsContainerView.isHidden = true
            }
            cell?.configData(panDetails)
            return cell!
        }
        else if indexPath.row == 4{
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "BankVerificationTableViewCell") as? BankVerificationTableViewCell
            
            if cell == nil {
                cell = BankVerificationTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "BankVerificationTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.tag = indexPath.row
            
            if selectedType == SelectedDocumentType.BANK.rawValue{
                cell?.fieldsContainerView.isHidden = false
            }
            else{
                cell?.fieldsContainerView.isHidden = true
            }
            cell?.dropdownButton.tag = indexPath.row
            cell?.dropdownButton.addTarget(self, action: #selector(self.dropDownButtonTapped(button:)), for: .touchUpInside)
            
            cell?.configData(bankDetails)
            return cell!
        }
        else if indexPath.row == 3{
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "AdharVerificationTableViewCell") as? AdharVerificationTableViewCell
            
            if cell == nil {
                cell = AdharVerificationTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "AdharVerificationTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.tag = indexPath.row
            
            cell?.dropDownButton.tag = indexPath.row
            cell?.dropDownButton.addTarget(self, action: #selector(self.dropDownButtonTapped(button:)), for: .touchUpInside)
            
            if selectedType == SelectedDocumentType.ADHAR.rawValue{
                cell?.fieldsContainerView.isHidden = false
            }
            else{
                cell?.fieldsContainerView.isHidden = true
            }
            cell?.configData(aadhaarDetails)
            return cell!
        }
        
        return UITableViewCell()
    }
    
    @objc func dropDownButtonTapped(button: UIButton)  {
        weak var weakSelf = self

        if button.tag == 1 {
            if UserDetails.sharedInstance.emailVerified{
                return;
            }

            if (selectedType == SelectedDocumentType.Email.rawValue) {
                let currentIndexPath = IndexPath(row: 0, section: 0)
                selectedType = 1000
                tblView.beginUpdates()
                tblView.reloadRows(at: [currentIndexPath], with: .none)
                tblView.endUpdates()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    weakSelf?.tblView.reloadData()
                }
                
                return
            }
            
            selectedType = SelectedDocumentType.Email.rawValue
            let currentIndexPath = IndexPath(row: 0, section: 0)
            
            tblView.beginUpdates()
            tblView.reloadRows(at: [currentIndexPath], with: .none)
            tblView.endUpdates()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                weakSelf?.tblView.reloadData()
            }

        }
        else if button.tag == 2 {
            if (selectedType == SelectedDocumentType.PAN.rawValue) || UserDetails.sharedInstance.panVerified || UserDetails.sharedInstance.panVerifictionSubmitted{
                if selectedType == SelectedDocumentType.PAN.rawValue{
                    selectedType = 10000
                }
                else{
                    guard let details = panDetails else {
                        callGetPanDetailsAPI()
                        return;
                    }

                    selectedType = SelectedDocumentType.PAN.rawValue
                }
                
                let currentIndexPath = IndexPath(row: 2, section: 0)
                tblView.beginUpdates()
                tblView.reloadRows(at: [currentIndexPath], with: .none)
                tblView.endUpdates()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    weakSelf?.tblView.reloadData()
                }

                return
            }

            selectedType = SelectedDocumentType.PAN.rawValue
            
            let currentIndexPath = IndexPath(row: 2, section: 0)
            
            tblView.beginUpdates()
            tblView.reloadRows(at: [currentIndexPath], with: .none)
            tblView.endUpdates()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                weakSelf?.tblView.reloadData()
            }

        }
        else if button.tag == 4 {
            
            if !UserDetails.sharedInstance.panVerified{
                return;
            }
            
            if (selectedType == SelectedDocumentType.BANK.rawValue) || UserDetails.sharedInstance.bankVerified || UserDetails.sharedInstance.bankVerifictionSubmitted{
                let currentIndexPath = IndexPath(row: 4, section: 0)
//                selectedType = 1000
                tblView.beginUpdates()
                tblView.reloadRows(at: [currentIndexPath], with: .none)
                tblView.endUpdates()
                
                
                if selectedType == SelectedDocumentType.BANK.rawValue{
                    selectedType = 10000
                }
                else{
                    guard let details = bankDetails else {
                        callBankDetailsAPI()
                        return
                    }

                    selectedType = SelectedDocumentType.BANK.rawValue
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    weakSelf?.tblView.reloadData()
                }

                return
            }
            
            selectedType = SelectedDocumentType.BANK.rawValue
            
            let currentIndexPath = IndexPath(row: 4, section: 0)
            tblView.beginUpdates()
            tblView.reloadRows(at: [currentIndexPath], with: .none)
            tblView.endUpdates()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                weakSelf?.tblView.reloadData()
            }

        }
        else if button.tag == 3 {
            if !UserDetails.sharedInstance.panVerified{
                return;
            }

            if (selectedType == SelectedDocumentType.ADHAR.rawValue) || UserDetails.sharedInstance.aadhaarVerified || UserDetails.sharedInstance.aadhaarVerifictionSubmitted{
                
                if selectedType == SelectedDocumentType.ADHAR.rawValue{
                    selectedType = 10000
                }
                else{
                    guard let details = aadhaarDetails else {
                        callAadhaarDetailsAPI()
                        return
                    }

                    selectedType = SelectedDocumentType.ADHAR.rawValue
                }

                
                let currentIndexPath = IndexPath(row: 3, section: 0)
                
//                selectedType = 1000
                tblView.beginUpdates()
                tblView.reloadRows(at: [currentIndexPath], with: .none)
                tblView.endUpdates()
//
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    weakSelf?.tblView.reloadData()
                }
                return
            }

            selectedType = SelectedDocumentType.ADHAR.rawValue
            let currentIndexPath = IndexPath(row: 3, section: 0)
            
            tblView.beginUpdates()
            tblView.reloadRows(at: [currentIndexPath], with: .none)
            tblView.endUpdates()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                weakSelf?.tblView.reloadData()
            }
        }
        
        tblView.reloadData()
    }
    
    
    @IBAction func supportButtonTapped(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@letspick.io"])
            present(mail, animated: true)
        } else {
            // show failure alert
        }

    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

    
    func callGetPanDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "get_pan_details", "user_id": UserDetails.sharedInstance.userID]
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                    
                        if let details = response["pan_details"]?.dictionary{
                            self.panDetails = PanDetails.getPanDetails(details: details)
                            self.selectedType = SelectedDocumentType.PAN.rawValue
                            self.tblView.reloadData();
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message, isErrorMessage: true)
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    func callBankDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "get_bank_details", "user_id": UserDetails.sharedInstance.userID]
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                        
                        if let details = response["bank_details"]?.dictionary{
                            self.bankDetails = BankDetails.getBankDetails(details: details)
                            self.selectedType = SelectedDocumentType.BANK.rawValue
                            self.tblView.reloadData();
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    func callAadhaarDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "get_aadhaar_details", "user_id": UserDetails.sharedInstance.userID]
        
        WebServiceHandler.performPOSTRequest(urlString: kUserUrl, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                        if let details = response["aadhaar_details"]?.dictionary{
                            self.aadhaarDetails = AadhaarDetails.getAadharDetails(details: details)
                            self.selectedType = SelectedDocumentType.ADHAR.rawValue
                            self.tblView.reloadData();
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
