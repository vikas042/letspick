//
//  AddClaimViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 28/02/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class AddClaimViewController: UIViewController {

    @IBOutlet weak var lblProductInfoTitle: UILabel!
    @IBOutlet weak var useAnotherAddress: UIButton!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var lblProductDescriptionTitle: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var headerView: CustomNavigationBar!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var confirmButton: SolidButton!
    @IBOutlet weak var txtFiledCity: UITextField!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var txtFieldMobileNumber: UITextField!
    @IBOutlet weak var txtFieldName: UITextField!
    
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var txtFieldAddress1: UITextField!

    @IBOutlet weak var lblAddress2: UILabel!
    @IBOutlet weak var txtFieldAddress2: UITextField!

    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var txtFieldState: UITextField!

    @IBOutlet weak var lblPinCode: UILabel!
    @IBOutlet weak var txtFieldPinCode: UITextField!

    @IBOutlet weak var addressLine2View: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var pincodeView: UIView!

    @IBOutlet weak var scrollViewContainerHeightconstraint: NSLayoutConstraint!
    
    var productArray = Array<RewardProductDetails>()
    var addressDetails: RewardAddressDetails?

    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "RewardProductTableViewCell", bundle: nil), forCellReuseIdentifier: "RewardProductTableViewCell")
        
        lblProductInfoTitle.text = "Product Information".localized()
        lblName.text = "Delivery Information".localized()
        lblProductDescriptionTitle.text = "Product Description".localized()

        lblMobileNumber.text = "Mobile_number".localized()
        lblAddress1.text = "Address Line1".localized()
        lblAddress2.text = "Address Line2".localized()
        lblCity.text = "City".localized()
        lblState.text = "State".localized()
        lblPinCode.text = "Zip Code".localized()
        useAnotherAddress.setTitle("Use Another Address".localized(), for: .normal)
        confirmButton.setTitle("Confirm".localized(), for: .normal)
        headerView.headerTitle = "Confirm Claim".localized()
        txtFieldName.isUserInteractionEnabled = false
        txtFieldMobileNumber.isUserInteractionEnabled = false
        
        txtFiledCity.text = addressDetails?.city
        txtFieldName.text = UserDetails.sharedInstance.name
        if UserDetails.sharedInstance.name == "" {
            txtFieldName.text = UserDetails.sharedInstance.userName
        }
        
        txtFieldAddress1.text = addressDetails?.address
        txtFieldAddress2.text = addressDetails?.addLine2
        txtFieldState.text = addressDetails?.state

        txtFieldPinCode.text = addressDetails?.pincode
        txtFieldMobileNumber.text = UserDetails.sharedInstance.phoneNumber
        
        if productArray.count > 0 {
            let details = productArray[0]
            lblProductDescription.text = details.descriptionEnglish.htmlToString
            if details.rewardCategoryId == "2"{
                addressLine2View.isHidden = true
                cityView.isHidden = true
                stateView.isHidden = true
                pincodeView.isHidden = true
                lblAddress1.text = "Email Address".localized()
                txtFieldAddress1.text = UserDetails.sharedInstance.emailAdress
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if productArray.count > 0 {
            let details = productArray[0]
            if details.rewardCategoryId == "2"{
                scrollViewContainerHeightconstraint.constant = 350
                view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func confirmButtonTapped(_ sender: Any) {
        if productArray.count > 0 {
            let details = productArray[0]
            if details.rewardCategoryId == "2"{
                callClaimProductAPI(details: details)
            }
            else{
                if txtFieldAddress1.text?.count == 0 {
                    AppHelper.showAlertViewWithoutMessage(titleMessage: "Please enter your address", isErrorMessage: true)
                    return;
                }
                else if txtFiledCity.text?.count == 0 {
                    AppHelper.showAlertViewWithoutMessage(titleMessage: "Please enter your city", isErrorMessage: true)
                    return;
                }
                else if txtFieldState.text?.count == 0 {
                    AppHelper.showAlertViewWithoutMessage(titleMessage: "Please enter your state", isErrorMessage: true)
                    return;
                }
                else if txtFieldPinCode.text?.count == 0 {
                    AppHelper.showAlertViewWithoutMessage(titleMessage: "Please enter your zipcode", isErrorMessage: true)
                    return;
                }

                callAddClaimAPI()
            }
        }
    }
    
    @IBAction func userAnotherAddressButtonTapped(_ sender: Any) {
        txtFiledCity.text = ""
        txtFieldAddress1.text = ""
        txtFieldAddress2.text = ""
        txtFieldPinCode.text = ""
        txtFieldState.text = ""
    }
    
    func callAddClaimAPI() {
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        var prodcutID = ""
        
        if productArray.count > 0 {
            let details = productArray[0]
            prodcutID = details.rewardProdId
        }
        else{
            AppHelper.showAlertView(message: "Prodcut info is missing", isErrorMessage: true)
            return
        }
        AppHelper.sharedInstance.displaySpinner()


        let params = ["add_line2": txtFieldAddress2.text ?? "", "address": txtFieldAddress1.text ?? "", "city": txtFiledCity.text ?? "", "option": "claim_confirm", "pincode": txtFieldPinCode.text ?? "", "product": prodcutID, "state": txtFieldState.text ?? "", "user_id": UserDetails.sharedInstance.userID ]
        
        WebServiceHandler.performPOSTRequest(urlString: kRewardProgram, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200" {
                    var message = ""
                    let details = self.productArray[0]
                    
                    if details.rewardCategoryId == "1" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be sent to your address within 7-10 business days. Please check your email"
                    }
                    else if details.rewardCategoryId == "2" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be sent to your email address. Please check your email"
                    }
                    else if details.rewardCategoryId == "3" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be credited in your account in some time. Keep playing on Letspick"
                    }

                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            if details.rewardCategoryId == "1" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपको 7 - 10 कार्य दिवस में आपके पते पर भेज दिया जायेगा। कृपया अपना ईमेल अकाउंट चेक करें।"

                            }
                            else if details.rewardCategoryId == "2" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपको आपके ईमेल पर भेज दिया जायेगा । कृपया अपना ईमेल अकाउंट चेक करें।"
                            }
                            else if details.rewardCategoryId == "3" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपके अकाउंट में कुछ देर बाद क्रेडिट कर दिया जायेगा । Letspick पर खेलते रहें।"
                            }
                        }
                    }
                    
                    let confratsView = CongratsRewarsClaimPopup(frame: APPDELEGATE.window!.frame)
                    confratsView.isViewForClaim = true
                    confratsView.configData(mesage: message)
                    APPDELEGATE.window!.addSubview(confratsView)
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }

            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callClaimProductAPI(details: RewardProductDetails)  {
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "claim_confirm", "user_id": UserDetails.sharedInstance.userID, "product": details.rewardProdId ] as [String : Any]
        
        WebServiceHandler.performPOSTRequest(urlString: kRewardProgram, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()

                if statusCode == "200" {
                    var message = ""
                    if details.rewardCategoryId == "1" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be sent to your address within 7-10 business days. Please check your email"
                    }
                    else if details.rewardCategoryId == "2" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be sent to your email address. Please check your email"
                    }
                    else if details.rewardCategoryId == "3" {
                        message = "Congratulations! You have successfully claimed your \(details.rewardNameEnglish.htmlToString) and the same will be credited in your account in some time. Keep playing on Letspick"
                    }
                    
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            if details.rewardCategoryId == "1" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपको 7 - 10 कार्य दिवस में आपके पते पर भेज दिया जायेगा। कृपया अपना ईमेल अकाउंट चेक करें।"

                            }
                            else if details.rewardCategoryId == "2" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपको आपके ईमेल पर भेज दिया जायेगा । कृपया अपना ईमेल अकाउंट चेक करें।"
                            }
                            else if details.rewardCategoryId == "3" {
                                message = "बधाई हो। आपने \(details.rewardNameEnglish.htmlToString) सफलतापूर्वक क्लेम कर लिया है और आपके अकाउंट में कुछ देर बाद क्रेडिट कर दिया जायेगा । Letspick पर खेलते रहें।"
                            }
                        }
                    }

                    let confratsView = CongratsRewarsClaimPopup(frame: APPDELEGATE.window!.frame)
                    confratsView.isViewForClaim = true

                    confratsView.configData(mesage: message)
                    APPDELEGATE.window!.addSubview(confratsView)
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized() , isErrorMessage: true)
            }
        }
    }
}

extension AddClaimViewController: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "RewardProductTableViewCell") as? RewardProductTableViewCell
        
        if cell == nil {
            cell = RewardProductTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "RewardProductTableViewCell")
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none

        let details = productArray[indexPath.row]
        cell?.configData(details: details)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
