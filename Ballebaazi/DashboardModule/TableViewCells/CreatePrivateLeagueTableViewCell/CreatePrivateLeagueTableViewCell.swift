//
//  CreatePrivateLeagueTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 24/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class CreatePrivateLeagueTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var innerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configCell() {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblMessage.attributedText = "CREATE YOUR OWN LEAGUE".getUnderLineAttributedText()
    }
}
