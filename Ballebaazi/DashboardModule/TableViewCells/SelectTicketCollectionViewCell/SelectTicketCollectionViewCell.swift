//
//  SelectTicketCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 04/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class SelectTicketCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var checkBoxImg: UIImageView!
    @IBOutlet weak var lblTicketTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
