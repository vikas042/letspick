//
//  JoinedLeagueTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 03/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire


class JoinedLeagueTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblRankingTitle: UILabel!
    @IBOutlet weak var lblEntry: UILabel!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var inviteFriendButton: UIButton!
    @IBOutlet weak var joinLeagueButton: UIButton!
    @IBOutlet weak var inviteButton: UIButton!
    @IBOutlet weak var lblPricePoolTitle: UILabel!
    @IBOutlet weak var lblTopRankTitlew: UILabel!
    @IBOutlet weak var lblEntryFee: UILabel!
    @IBOutlet weak var rankButtonView: UIView!
    @IBOutlet weak var myTeamButtonView: UIView!
    @IBOutlet weak var joinPrivateLeagueView: UIView!
    @IBOutlet weak var privateLeagueView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblLeagueName: UILabel!
    @IBOutlet weak var lblTeamCount: UILabel!
    @IBOutlet weak var lblPoolPrize: UILabel!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var viewRankingButton: UIButton!
    @IBOutlet weak var lblTeamTitle: UILabel!
    @IBOutlet weak var lblMyTeamCountTitle: UILabel!
    @IBOutlet weak var lblMyTeamCount: UILabel!
    @IBOutlet weak var inviteFriendsView: UIView!
    @IBOutlet weak var prizePoolButton: UIButton!
    
    var joinedLeagueDetails: JoinedLeagueDetails?
    var selectedMatchDetails: MatchDetails?
    var isUserValidatingToJoinLeague = false

    var userTeamsArray = Array<UserTeamDetails>()
    var selectedGameType = GameType.Cricket.rawValue
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configData(details: JoinedLeagueDetails, matchDetails: MatchDetails, teamsArray: Array<UserTeamDetails>, gameType: Int)  {
        selectedGameType = gameType;
        prizePoolButton.isHidden = false
        if details.leagueType == "2" {
            prizePoolButton.isHidden = true
        }
        
        lblRankingTitle.text = "Ranking".localized()
        lblEntry.text = "Entry".localized()
        joinButton.setTitle("Join".localized(), for: .normal)
        inviteFriendButton.setTitle("Invite".localized(), for: .normal)
        
        joinButton.setTitle("Join".localized(), for: .selected)
        
        inviteFriendButton.setTitle("Invite".localized(), for: .selected)
        joinLeagueButton.setTitle("Join".localized(), for: .selected)
        inviteButton.setTitle("Invite".localized(), for: .selected)
        inviteButton.setTitle("Invite".localized(), for: .normal)

        lblPoolPrize.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.winAmount ?? "0"))

        if matchDetails.isMatchClosed {
            lblPricePoolTitle.text = "Winners".localized()
            lblPoolPrize.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.totalWinners))
        }
        else{
            lblPricePoolTitle.text = "Prize pool".localized()
            lblPoolPrize.text =  "" + AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.winAmount ?? "0"))
        }
        lblTeamTitle.text = "Teams".localized()
        lblTopRankTitlew.text = "TOP RANK:".localized()
        selectedMatchDetails = matchDetails
        userTeamsArray = teamsArray
        showShadowOnActionView()
        joinedLeagueDetails = details
        inviteFriendsView.isHidden = true
        privateLeagueView.isHidden = true
        rankButtonView.isHidden = true
        myTeamButtonView.isHidden = true
        joinPrivateLeagueView.isHidden = true
        lblEntryFee.text = "pts" + details.joiningAmount
        if let myTeamArray = details.myTeamPlayers{
            if myTeamArray.count > 1{
                lblMyTeamCountTitle.text = "Teams".localized()
            }
            else{
                lblMyTeamCountTitle.text = "Team".localized()
            }
            lblMyTeamCount.text = String(myTeamArray.count);
        }
        else{
            lblMyTeamCountTitle.text = "Team".localized()
            lblMyTeamCount.text = "0";
        }

        if matchDetails.isMatchClosed{
            rankButtonView.isHidden = false
        }
        else if details.isPrivateLeague && !details.isPrivateLeagueJoined{
            joinPrivateLeagueView.isHidden = false
        }
        else{
            myTeamButtonView.isHidden = false
            if details.teamType != "1" {
                inviteFriendsView.isHidden = false
                myTeamButtonView.isHidden = true
            }

            if (Int(details.totalJoined ?? "0") ?? 0) >= (Int(details.maxPlayers ?? "0") ?? 0){
                inviteFriendsView.isHidden = true
                myTeamButtonView.isHidden = true
            }
        }
        
        joinLeagueButton.setTitle("Rejoin".localized(), for: .normal)
        
        if details.totalJoinedSelf >= UserDetails.sharedInstance.maxTeamAllowedForClassic {
            inviteFriendsView.isHidden = false
            myTeamButtonView.isHidden = true
        }

        layoutIfNeeded()        
        lblLeagueName.text = details.leagueName
        if details.teamRank == "0" {
            lblRank.text = "0"
        }
        else{
            lblRank.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details.teamRank ?? "0"))
        }
        
        if matchDetails.active == "4" {
            inviteFriendsView.isHidden = true
            privateLeagueView.isHidden = true
            rankButtonView.isHidden = true
            myTeamButtonView.isHidden = true
            joinPrivateLeagueView.isHidden = true
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func pricePoolButtonTapped(_ sender: Any) {
        if joinedLeagueDetails?.leagueType == "2" {
            return;
        }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let leagueWinnerRankVC = storyboard.instantiateViewController(withIdentifier: "LeagueWinnersRankPriceViewController") as! LeagueWinnersRankPriceViewController
        leagueWinnerRankVC.leagueID = joinedLeagueDetails?.leagueId
        leagueWinnerRankVC.leagueWinningAmount = joinedLeagueDetails!.winAmount!
        leagueWinnerRankVC.leagueWinnerType = joinedLeagueDetails!.leagueWinnerType
        leagueWinnerRankVC.bannerArray = joinedLeagueDetails!.bannerImages
        leagueWinnerRankVC.totalWinner = joinedLeagueDetails!.totalWinners
        leagueWinnerRankVC.selectedGameType = selectedGameType
        if joinedLeagueDetails?.isInfinity == "1"{
            leagueWinnerRankVC.winPerUser = joinedLeagueDetails!.winPerUser
            leagueWinnerRankVC.isInfinityLeague = true
            leagueWinnerRankVC.totalWinnersPercent = joinedLeagueDetails!.totalWinnersPercent
        }

        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.pushViewController(leagueWinnerRankVC, animated: true)
        }


    }
        
    @IBAction func inviteFrieldsButtonTapped(_ sender: Any) {
    
        var userName = UserDetails.sharedInstance.name
        if userName.count == 0 {
            userName = UserDetails.sharedInstance.userName
        }
                
        let text = String(format: "Letspick Ke Liye Taiyyar?\n%@ has challenged you on Letspick! Make your team now and join the %@ League to win pts%@.\nGet into the game Aur Jeet Ka Karo Aim\nhttps://Letspick.app.link?league_code=%@", userName, (joinedLeagueDetails!.leagueName ?? ""),(joinedLeagueDetails!.winAmount ?? "0"), joinedLeagueDetails!.leagueCode, joinedLeagueDetails!.leagueCode)

        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func joinLeagueButtonTapped(_ sender: Any) {
        
        let leagueDetails = LeagueDetails()
        leagueDetails.leagueId = joinedLeagueDetails!.leagueId!
        leagueDetails.matchKey = selectedMatchDetails!.matchKey
        leagueDetails.joiningAmount = joinedLeagueDetails!.joiningAmount
        leagueDetails.fantasyType = joinedLeagueDetails!.fantasyType!
        if isUserValidatingToJoinLeague {
            return;
        }
        
        callLeagueValidationAPI(details: leagueDetails, categoryName: kPrivateLeague, isPrivate: joinedLeagueDetails!.fantasyType! )
    }
    
    func callLeagueValidationAPI(details: LeagueDetails, categoryName: String, isPrivate: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        var optionValue = ""
        
        isUserValidatingToJoinLeague = true
        weak var weakSelf = self

        var urlString = kMatch
        
        if selectedGameType == GameType.Cricket.rawValue{
            urlString = kMatch
            optionValue = "league_prev_data_v2"
        }
        else if selectedGameType == GameType.Kabaddi.rawValue{
            urlString = kKabaddiMatchURL
            optionValue = "join_league_preview_v1"
        }
        else if selectedGameType == GameType.Football.rawValue{
            urlString = kFootballMatchURL
            optionValue = "join_league_preview_v1"
        }
        else if selectedGameType == GameType.Basketball.rawValue{
            urlString = kBasketballMatchURL
            optionValue = "join_league_preview_v1"
        }
        else if selectedGameType == GameType.Baseball.rawValue{
            urlString = kBaseballMatchURL
            optionValue = "join_league_preview_v1"
        }
        
        let params = ["option": optionValue,"check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": details.matchKey, "league_id": details.leagueId, "fantasy_type": details.fantasyType, "is_private": isPrivate]

        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            weakSelf?.isUserValidatingToJoinLeague = false
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string
                if let response = result!["response"]?.dictionary {
                    if let tempArray = response["user_teams"]?.array{
                        UserDetails.sharedInstance.userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: tempArray, matchDetails: weakSelf!.selectedMatchDetails!)
                    }

                }

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == details.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(details.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: "Not enough points to join this league", preferredStyle: UIAlertControllerStyle.alert)
                                    /*
                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
                                        
                                        let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
                                        addCashVC?.leagueDetails = details
                                        addCashVC?.amount = roundFigureAmt
                                        addCashVC?.matchDetails = weakSelf!.selectedMatchDetails
                                        addCashVC?.userTeamArray = teamArray
                                        addCashVC?.categoryName = categoryName
                                        addCashVC?.selectedGameType = weakSelf!.selectedGameType
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                            navVC.pushViewController(addCashVC!, animated: true)
                                        }
                                    }))
                                    */
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    
                                    
                                    if weakSelf!.selectedGameType == GameType.Cricket.rawValue{
                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                        playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                    else if weakSelf!.selectedGameType == GameType.Kabaddi.rawValue{

                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                        playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                    else if weakSelf!.selectedGameType == GameType.Football.rawValue{
                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                        playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                    else if weakSelf!.selectedGameType == GameType.Basketball.rawValue{
                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                        playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                    else if weakSelf!.selectedGameType == GameType.Baseball.rawValue{
                                        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                        playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                        playerVC.leagueDetails = details
                                        playerVC.ticketDetails = ticketDetais
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                            navVC.pushViewController(playerVC, animated: true)
                                        }
                                    }
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{

                            if weakSelf!.selectedGameType == GameType.Cricket.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.selectedGameType == GameType.Kabaddi.rawValue{

                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.selectedGameType == GameType.Football.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.selectedGameType == GameType.Basketball.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.selectedGameType == GameType.Baseball.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                            
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == details.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: details, userTeamArray: teamArray, categoryName: categoryName, ticketDetails: ticketDetais, gameType: self.selectedGameType)
                            }
                            else{
                                if weakSelf!.selectedGameType == GameType.Cricket.rawValue{
                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                     playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }
                                 else if weakSelf!.selectedGameType == GameType.Kabaddi.rawValue{

                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                     playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }
                                 else if weakSelf!.selectedGameType == GameType.Football.rawValue{
                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                     playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }
                                 else if weakSelf!.selectedGameType == GameType.Basketball.rawValue{
                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                     playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }
                                 else if weakSelf!.selectedGameType == GameType.Baseball.rawValue{
                                     let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                     playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                     playerVC.leagueDetails = details
                                     playerVC.ticketDetails = ticketDetais
                                     if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                     }
                                 }                            }
                        }
                        else{
                                    
                            if weakSelf!.selectedGameType == GameType.Cricket.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.selectedGameType == GameType.Kabaddi.rawValue{

                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.selectedGameType == GameType.Football.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.selectedGameType == GameType.Basketball.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                             else if weakSelf!.selectedGameType == GameType.Baseball.rawValue{
                                 let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                 playerVC.matchDetails = weakSelf?.selectedMatchDetails
                                 playerVC.leagueDetails = details
                                 playerVC.ticketDetails = ticketDetais
                                 if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                 }
                             }
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, categoryName: String, ticketDetails: TicketDetails?, gameType: Int)  {

        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let joinedLeagueConfirmVC = storyboard.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
            joinedLeagueConfirmVC?.isNeedToShowMatchClosePopup = true
            joinedLeagueConfirmVC?.leagueDetails = leagueDetails
            joinedLeagueConfirmVC?.ticketDetails = ticketDetails
            joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
            joinedLeagueConfirmVC?.matchDetails = self.selectedMatchDetails
            joinedLeagueConfirmVC?.leagueCategoryName = categoryName
            joinedLeagueConfirmVC?.selectedGameType = gameType
            navigationVC.pushViewController(joinedLeagueConfirmVC!, animated: true)
        }
    }
    
    func showShadowOnActionView() {
        AppHelper.showShodowOnCellsView(innerView: innerView)

        showShadowOnButtonsView(view: inviteFriendsView)
        showShadowOnButtonsView(view: myTeamButtonView)
        showShadowOnButtonsView(view: joinPrivateLeagueView)
        showShadowOnButtonsView(view: rankButtonView)
    }
    
    func showShadowOnButtonsView(view: UIView) {
        view.layer.cornerRadius = 20.0
        view.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.35
        view.layer.shadowRadius = 6.5
        view.layer.masksToBounds = false
    }

    
    @IBAction func rankButtonTapped(_ sender: Any) {
   
    }
    
}
