//
//  LeaderboardMembersTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/01/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LeaderboardMembersTableViewCell: UITableViewCell {

    @IBOutlet weak var lblUserNameNotImage: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPointsCount: UILabel!
    @IBOutlet weak var lblRankNumber: UILabel!
    
    func configData(details: LeaderboardsUsers) {
//        ProfilePicPlaceholder
        lblUserNameNotImage.text = ""
        lblUserName.text = details.username
        lblUserNameNotImage.text = ""
        if Float(details.allPoints) ?? 0 > 1 {
            lblPointsCount.text = details.allPoints
        }
        else{
            lblPointsCount.text = details.allPoints
        }
        lblRankNumber.text = details.rank
        
        if (details.image != ""){
            
            let userImage = UserDetails.sharedInstance.userImageUrl + details.image
        
        if let url = NSURL(string: userImage){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: "ProfilePicPlaceholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    
//                    let strName = details.username.uppercased()
//                    let strFirstLetterName = strName as NSString
//                    if strFirstLetterName.length >= 2
//                    {
//                        self!.lblUserNameNotImage.text = strFirstLetterName.substring(with: NSRange(location: 0, length: strFirstLetterName.length > 1 ? 1 : strFirstLetterName.length))
//                    }
                    
                    self?.imgView.image = UIImage(named: "ProfilePicPlaceholder")
                }
            })
        }
        }
        else{
            
//
//            let strName = details.username.uppercased()
//            let strFirstLetterName = strName as NSString
//            if strFirstLetterName.length >= 2
//            {
//                lblUserNameNotImage.text = strFirstLetterName.substring(with: NSRange(location: 0, length: strFirstLetterName.length > 1 ? 1 : strFirstLetterName.length))
//            }
            
            self.imgView.image = UIImage(named: "ProfilePicPlaceholder")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
