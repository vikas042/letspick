//
//  VersionDetailsTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/12/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class VersionDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configData(details: VersionDetails) {
        lblTitle.text = "v_" + details.versionNumber
        lblMessage.showHTML(htmlString: details.versionChanges, isCenterAligment: false)            
    }
}
