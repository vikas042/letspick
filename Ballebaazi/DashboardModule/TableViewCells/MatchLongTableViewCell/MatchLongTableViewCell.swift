//
//  MatchLongTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 22/06/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class MatchLongTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleValue: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
