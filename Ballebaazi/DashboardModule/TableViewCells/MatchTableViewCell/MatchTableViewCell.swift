//
//  MatchTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/7/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import MapleBacon


class MatchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var ticketImgView: UIImageView!
    @IBOutlet weak var lblTicketAvilableMessage: UILabel!
    @IBOutlet weak var lblLineupsOut: UILabel!
    @IBOutlet weak var lblFreeTicketAvailable: UILabel!
    @IBOutlet weak var freeTicketView: UIView!
    @IBOutlet weak var linesUpView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblSecondTeamName: UILabel!
    @IBOutlet weak var lblFirstTeamName: UILabel!
    @IBOutlet weak var lblMatchType: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var firstTeamImageView: UIImageView!
    @IBOutlet weak var secondTeamImageView: UIImageView!
    
    var selectedMatchDetails: MatchDetails?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configData(matchDetails: MatchDetails)  {
        selectedMatchDetails = matchDetails

        overlayView.isHidden = true
        if matchDetails.active == "2" {
            overlayView.isHidden = false
        }
        overlayView.backgroundColor = UIColor(red: 215.0/255, green: 215.0/255, blue: 215.0/255, alpha: 0.4)
        overlayView.layer.cornerRadius = 7.0
        lblLineupsOut.text = "LINEUPS OUT".localized()
        freeTicketView.roundViewCorners(corners: [.bottomLeft,.bottomRight], radius: 7)
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblMatchType.text = matchDetails.seasonShortName.uppercased()
        lblTimer.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: matchDetails.startDateTimestemp)
        lblFirstTeamName.text = matchDetails.firstTeamShortName
        lblSecondTeamName.text = matchDetails.secondTeamShortName
        linesUpView.isHidden = true
        if matchDetails.show_playing22 == "1"{
            linesUpView.isHidden = false
        }

        if matchDetails.firstTeamImageUrl.count < 5 {
            self.firstTeamImageView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: matchDetails.firstTeamImageUrl){
            firstTeamImageView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                 self?.firstTeamImageView.image = image
                }
                else{
                    self?.firstTeamImageView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.firstTeamImageView.image = UIImage(named: "Placeholder")
        }
        
        if matchDetails.secondTeamImageUrl.count < 5 {
            self.secondTeamImageView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: matchDetails.secondTeamImageUrl){
            secondTeamImageView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.secondTeamImageView.image = image
                }
                else{
                    self?.secondTeamImageView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.secondTeamImageView.image = UIImage(named: "Placeholder")
        }
        
        if (matchDetails.active == "4") && !matchDetails.isMatchClosed {
            lblTimer.text = "00:00"
        }
    }
    
}
