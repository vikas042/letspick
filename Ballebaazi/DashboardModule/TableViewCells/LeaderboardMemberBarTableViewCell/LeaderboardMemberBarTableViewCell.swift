//
//  LeaderboardMemberBarTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/01/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LeaderboardMemberBarTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblRank: UILabel!
    
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    
    
    @IBOutlet weak var rankTitleView: UIView!
    @IBOutlet weak var lblRank1Name: UILabel!
    @IBOutlet weak var lblRank2Name: UILabel!
    @IBOutlet weak var lblRank3Name: UILabel!
    
    @IBOutlet weak var rank1UserImgView: UIImageView!
    @IBOutlet weak var rank2UserImgView: UIImageView!
    @IBOutlet weak var rank3UserImgView: UIImageView!
    
    
    @IBOutlet weak var lblRank1UserName: UILabel!
    @IBOutlet weak var lblRank1Point: UILabel!
    
    @IBOutlet weak var lblRank2UserName: UILabel!
    @IBOutlet weak var lblRank2Point: UILabel!
    
    @IBOutlet weak var lblRank3UserName: UILabel!
    @IBOutlet weak var lblRank3Point: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configData(rankOneDetailsArray: [LeaderboardsUsers]) {

        lblRank.text = "Rank".localized()
        lblPoints.text = "Points".localized()
        lblPlayerName.text = "Player Name".localized()
        
        if rankOneDetailsArray.count == 1 {
            
            lblRank1Name.text = ""
            lblRank2Name.text = ""
            lblRank3Name.text = ""

            
            lblRank1Point.isHidden = false
            lblRank2Point.isHidden = true
            lblRank3Point.isHidden = true
            
            lblRank1UserName.isHidden = false
            lblRank2UserName.isHidden = true
            lblRank3UserName.isHidden = true
            
            rank1UserImgView.isHidden = false
            rank2UserImgView.isHidden = true
            rank3UserImgView.isHidden = true
            
            
            let details = rankOneDetailsArray[0]
            
//            let strName = details.username.uppercased()
//            let strFirstLetterName = strName as NSString
//            if strFirstLetterName.length >= 2
//            {
//                if (details.image == ""){
//                    lblRank1Name.text = strFirstLetterName.substring(with: NSRange(location: 0, length: strFirstLetterName.length > 1 ? 1 : strFirstLetterName.length))
//                }
//            }
//
            showDataOnCell(details: details, userImgView: rank1UserImgView, pointLabel: lblRank1Point, userNameLabel: lblRank1UserName)

        }
        else if rankOneDetailsArray.count == 2 {
            
            lblRank1Name.text = ""
            lblRank2Name.text = ""
            lblRank3Name.text = ""
            
            lblRank1Point.isHidden = false
            lblRank2Point.isHidden = false
            lblRank3Point.isHidden = true
            
            lblRank1UserName.isHidden = false
            lblRank2UserName.isHidden = false
            lblRank3UserName.isHidden = true
           
            rank1UserImgView.isHidden = false
            rank2UserImgView.isHidden = false
            rank3UserImgView.isHidden = true

            let details = rankOneDetailsArray[0]
            let secondRankDetails = rankOneDetailsArray[1]
            
//            let strName = details.username.uppercased()
//            let strFirstLetterName = strName as NSString
//            if strFirstLetterName.length >= 2
//            {
//                if (details.image == ""){
//                    lblRank1Name.text = strFirstLetterName.substring(with: NSRange(location: 0, length: strFirstLetterName.length > 1 ? 1 : strFirstLetterName.length))
//                }
//            }
            
//            let strName1 = secondRankDetails.username.uppercased()
//            let strFirstLetterName1 = strName1 as NSString
//            if strFirstLetterName1.length >= 2
//            {
//                if (secondRankDetails.image == ""){
//                    lblRank2Name.text = strFirstLetterName1.substring(with: NSRange(location: 0, length: strFirstLetterName1.length > 1 ? 1 : strFirstLetterName1.length))
//                }
//            }
            
            showDataOnCell(details: details, userImgView: rank1UserImgView, pointLabel: lblRank1Point, userNameLabel: lblRank1UserName)
            showDataOnCell(details: secondRankDetails, userImgView: rank2UserImgView, pointLabel: lblRank2Point, userNameLabel: lblRank2UserName)
        }
        else if rankOneDetailsArray.count >= 3 {
            
            lblRank1Name.text = ""
            lblRank2Name.text = ""
            lblRank3Name.text = ""
                        
            lblRank1UserName.isHidden = false
            lblRank2UserName.isHidden = false
            lblRank3UserName.isHidden = false
            
            lblRank1Point.isHidden = false
            lblRank2Point.isHidden = false
            lblRank3Point.isHidden = false

            rank1UserImgView.isHidden = false
            rank2UserImgView.isHidden = false
            rank3UserImgView.isHidden = false
            
            
            let details = rankOneDetailsArray[0]
            let secondRankDetails = rankOneDetailsArray[1]
            let thiredRankDetails = rankOneDetailsArray[2]
            
//            let strName = details.username.uppercased()
//            let strFirstLetterName = strName as NSString
//            if strFirstLetterName.length >= 2
//            {
//                if (details.image == ""){
//                lblRank1Name.text = strFirstLetterName.substring(with: NSRange(location: 0, length: strFirstLetterName.length > 1 ? 1 : strFirstLetterName.length))
//                }
//            }
            
//            let strName1 = secondRankDetails.username.uppercased()
//            let strFirstLetterName1 = strName1 as NSString
//            if strFirstLetterName1.length >= 2
//            {
//                if (secondRankDetails.image == ""){
//                    lblRank2Name.text = strFirstLetterName1.substring(with: NSRange(location: 0, length: strFirstLetterName1.length > 1 ? 1 : strFirstLetterName1.length))
//                }
//            }
            
//            let strName2 = thiredRankDetails.username.uppercased()
//            let strFirstLetterName2 = strName2 as NSString
//            if strFirstLetterName2.length >= 2
//            {
//                if (thiredRankDetails.image == ""){
//                    lblRank3Name.text = strFirstLetterName2.substring(with: NSRange(location: 0, length: strFirstLetterName2.length > 1 ? 1 : strFirstLetterName2.length))
//                }
//            }
//            
            showDataOnCell(details: details, userImgView: rank1UserImgView, pointLabel: lblRank1Point, userNameLabel: lblRank1UserName)
            showDataOnCell(details: secondRankDetails, userImgView: rank2UserImgView, pointLabel: lblRank2Point, userNameLabel: lblRank2UserName)
            showDataOnCell(details: thiredRankDetails, userImgView: rank3UserImgView, pointLabel: lblRank3Point, userNameLabel: lblRank3UserName)

        }
    }
    
    
    func showDataOnCell(details: LeaderboardsUsers, userImgView: UIImageView, pointLabel: UILabel, userNameLabel: UILabel) {
        
        userNameLabel.text = details.username
        if Float(details.allPoints) ?? 0 > 1 {
            pointLabel.text = details.allPoints
        }
        else{
            pointLabel.text = details.allPoints
        }
        
        if (details.image != ""){
            
            let userImage = UserDetails.sharedInstance.userImageUrl + details.image
            
        if let url = NSURL(string: userImage){
            userImgView.setImage(with: url as URL, placeholder: UIImage(named: "ProfilePicPlaceholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    userImgView.image = image
                }
                else{
                    userImgView.image = UIImage(named: "ProfilePicPlaceholder")
                }
            })
        }
        }
        else{
            userImgView.image = UIImage(named: "ProfilePicPlaceholder")
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
