//
//  InputFiledTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 21/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class InputFiledTableViewCell: UITableViewCell {

    @IBOutlet weak var txtFieldRank: UITextField!
    @IBOutlet weak var cupImgView: UIImageView!
    
    @IBOutlet weak var lblRowNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
