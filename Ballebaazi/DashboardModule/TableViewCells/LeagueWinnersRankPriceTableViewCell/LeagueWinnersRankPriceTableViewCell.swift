//
//  LeagueWinnersRankPriceTableViewCell.swift
//  Letspick
//
//  Created by Mads Technologies on 29/06/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class LeagueWinnersRankPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var coinImgView: UIImageView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblRank: UILabel!

    @IBOutlet weak var lblGadget: UILabel!
    @IBOutlet weak var rankLabelWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configDetails(details: LeagueWinnersRank, leagueAmount: String)  {
                
        coinImgView.isHidden = true
        lblGadget.text = ""
        lblAmount.text = ""
        lblRank.text = details.rankCount
        
        if details.ticketName.count > 0{
            lblAmount.text = details.ticketName
        }
        else if (details.winPercent.count > 0) && (details.winProduct.count > 0){
            lblGadget.text = "+ " + details.winProduct
            let winPercent = (Float(details.winPercent) ?? 0.0) * (Float(leagueAmount) ?? 0.0)
            lblAmount.text = "" + String(winPercent/100) + ("(\(details.winPercent)% of Prizepool)")
        }
        else if (details.winPercent.count > 0) && (details.winPercent != "0"){
            let winPercent = (Float(details.winPercent) ?? 0.0) * (Float(leagueAmount) ?? 0.0)
            lblAmount.text = "" + String(winPercent/100) + ("(\(details.winPercent)% of Prizepool)")
        }
        else if (details.winProduct.count > 0) && (details.winAmount?.count ?? 0 > 0) && (details.winAmount != "0"){
            lblGadget.text = "+ " + details.winProduct
            lblAmount.text = "" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: (details.winAmount ?? "0" ))
        }
        else if details.winProduct.count > 0{
            lblAmount.text = details.winProduct
        }
        else if (details.bbcoins.count > 0) && (details.bbcoins != "0"){
            coinImgView.isHidden = false
            lblAmount.text = details.bbcoins + " " +  "LP Coins".localized()
        }
        else{
            lblAmount.text = "" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: (details.winAmount ?? "0" ))
        }
    }
    
    
    /*
     
     func configDetails(details: LeagueWinnersRank)  {
         lblGadget.text = ""
         lblAmount.text = ""
         lblRank.text = details.rankCount
         
         if (details.winProduct.count > 0) && (details.winAmount?.count ?? 0 > 0) && (details.winAmount != "0"){
             lblGadget.text = "+ " + details.winProduct
             lblAmount.text = "" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: (details.winAmount ?? "0" ))
         }
         else if (details.ticketName.count > 0) && (details.winAmount?.count ?? 0 > 0) && (details.winAmount != "0"){
                 lblGadget.text = "+ " + details.ticketName
                 lblAmount.text = "" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: (details.winAmount ?? "0" ))

         }
         else if (details.ticketName.count > 0) && (details.winProduct.count > 0){
             lblAmount.text = "+ " + details.winProduct
             lblGadget.text = "+ " + details.ticketName
         }
         else if details.winProduct.count > 0{
             lblAmount.text = details.winProduct
         }
         else if details.ticketName.count > 0{
             lblAmount.text = details.ticketName
         }
         else{
             lblAmount.text = "" + AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: (details.winAmount ?? "0" ))
         }
     }
     */
}
