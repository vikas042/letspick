//
//  AddPaytmTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class AddPaytmTableViewCell: UITableViewCell {
    @IBOutlet weak var addPaytmView: UIView!
    
    @IBOutlet weak var checkboxImgView: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var paytmDetailsView: UIView!
    
    @IBOutlet weak var lblPaytm: UILabel!
    
    @IBOutlet weak var lblPaytmNumber: UILabel!
    @IBOutlet weak var lblMinmumLimit: UILabel!
    
    @IBOutlet weak var paytmButton: SolidButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
