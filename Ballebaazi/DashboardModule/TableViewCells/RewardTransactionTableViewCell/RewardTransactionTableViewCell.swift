//
//  RewardTransactionTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 04/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class RewardTransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configRewardData(details: TransactionsDetails) {
                    
        lblDate.text = details.transactionTime
    
        if details.isNegativeTranaction {
            lblAmount.text = "-" + details.bbCoins
            lblAmount.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        else{
            lblAmount.text = "+" + details.bbCoins
            lblAmount.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
        }
        
        if details.transactionType == "1" {
            lblDescription.text = "LP Coins Earned".localized()
        }
        else if details.transactionType == "2" {
            lblDescription.text = "LP Coins Redeemed".localized()
        }
        else{
            lblDescription.text = details.transactionMessage
        }
    }
}
