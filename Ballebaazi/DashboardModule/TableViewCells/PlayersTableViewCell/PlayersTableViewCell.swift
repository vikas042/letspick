//
//  PlayersTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/11/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class PlayersTableViewCell: UITableViewCell {

    @IBOutlet weak var lblChoosenBy: UILabel!
    @IBOutlet weak var lblSelectionPercentage: UILabel!
    @IBOutlet weak var lblLastPlayed: UILabel!
    @IBOutlet weak var lblPredicted: UILabel!
    @IBOutlet weak var playerOverlayView: UIView!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var playerPointInfoButton: UIButton!
    @IBOutlet weak var addPlayerButton: UIButton!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var playerTypeImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configDetails(details: PlayerDetails, legueType: Int, gameType: Int, isLineupsAnnounced: Bool)  {
        
        lblPlayerName.text = details.playerShortName
        lblCredits.text = details.credits
        
        if details.isSelected {
            addPlayerButton.setImage(UIImage(named: "RemovePlayerIcon"), for: .normal)
        }
        else{
            addPlayerButton.setImage(UIImage(named: "AddPlayerIcon"), for: .normal)
        }
        
        lblTeam.text = details.teamShortName
        lblChoosenBy.text = "Chosen by".localized()
        if (legueType == FantasyType.Classic.rawValue) {
            lblPoints.text = details.classicPoints
            lblSelectionPercentage.text = details.classicSelected + "%"
        }
        else if legueType == FantasyType.Batting.rawValue{
            lblPoints.text = details.battingPoints
            lblSelectionPercentage.text = details.battingSelected + "%"
        }
        else if legueType == FantasyType.Bowling.rawValue{
            lblPoints.text = details.bowlingPoints
            lblSelectionPercentage.text = details.bowlingSelected + "%"
        }
        else if legueType == FantasyType.Reverse.rawValue{
            lblPoints.text = details.reversePoints
            lblSelectionPercentage.text = details.reverseSelected + "%"
        }
        else if legueType == FantasyType.Wizard.rawValue{
            lblPoints.text = details.wizardPoints
            lblSelectionPercentage.text = details.wizardSelected + "%"
        }
                
        showPlayerTypeImg(details: details, gameType: gameType)
        playerTypeImageView.image = UIImage(named: details.playerPlaceholder)

        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerTypeImageView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerTypeImageView.image = image
                    }
                    else{
                        self?.playerTypeImageView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }

        lblLastPlayed.isHidden = true
        lblPredicted.isHidden = true

        if isLineupsAnnounced {
            if details.isPlayerPlaying{
                lblLastPlayed.isHidden = false
                lblLastPlayed.text = "ANNOUNCED".localized()
                lblLastPlayed.backgroundColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
            }
        }
        else{
            lblLastPlayed.isHidden = true
            lblLastPlayed.text = ""
            
            if details.isPlaying11Last == "1"{
                lblLastPlayed.isHidden = false
                
                lblLastPlayed.text = "LAST".localized()
                lblLastPlayed.backgroundColor = UIColor(red: 255.0/255, green: 184.0/255, blue: 64.0/255, alpha: 1)
            }
            
            if details.isPlaying11Prob == "1"{
                lblPredicted.isHidden = false
                lblPredicted.text = "PREDICTED".localized()
                lblPredicted.backgroundColor = UIColor(red: 83.0/255, green: 173.0/255, blue: 255.0/255, alpha: 1)
            }
            else{
                lblPredicted.isHidden = true
                lblPredicted.text = ""
            }
        }
    }
    
    
    func showPlayerTypeImg(details: PlayerDetails, gameType: Int){
                
        if details.playerPlayingRole == PlayerType.Batsman.rawValue {
            playerTypeImageView.image = UIImage(named: "BatIcon")
        }
        else if details.playerPlayingRole == PlayerType.Bowler.rawValue {
            playerTypeImageView.image = UIImage(named: "BowlIcon")
        }
        else if details.playerPlayingRole == PlayerType.AllRounder.rawValue {
            if gameType == GameType.Kabaddi.rawValue{
                playerTypeImageView.image = UIImage(named: "AllrounderKabaddiPlaceholder")
            }
            else{
                playerTypeImageView.image = UIImage(named: "AllRounderIcon")
            }
        }
        else if details.playerPlayingRole == PlayerType.WicketKeeper.rawValue {
            playerTypeImageView.image = UIImage(named: "WicketIcon")
        }
        else if details.playerPlayingRole == PlayerType.Defender.rawValue {
            if gameType == GameType.Football.rawValue{
                playerTypeImageView.image = UIImage(named: "FootballDefenderIcon")
            }
            else{
                playerTypeImageView.image = UIImage(named: "DefenderIconPlaceholder")
            }
        }
        else if details.playerPlayingRole == PlayerType.Raider.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
        }
        else if details.playerPlayingRole == PlayerType.GoalKeeper.rawValue {
            playerTypeImageView.image = UIImage(named: "FootballGoalKeeperIcon")
        }
        else if details.playerPlayingRole == PlayerType.MidFielder.rawValue {
            playerTypeImageView.image = UIImage(named: "FootballMidFielderIcon")
        }
        else if details.playerPlayingRole == PlayerType.Sticker.rawValue {
            playerTypeImageView.image = UIImage(named: "FootballForwardIcon")
        }
    }
    
    
}
