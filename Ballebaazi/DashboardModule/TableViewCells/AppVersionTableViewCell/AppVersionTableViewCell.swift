//
//  AppVersionTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 24/02/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class AppVersionTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
