//
//  AddBankTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class AddBankTableViewCell: UITableViewCell {
    
    @IBOutlet weak var checkboxImgView: UIImageView!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var lblMinimumLimit: UILabel!

    @IBOutlet weak var addBankView: UIView!
    @IBOutlet weak var bankDetailsView: UIView!
    @IBOutlet weak var innerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
