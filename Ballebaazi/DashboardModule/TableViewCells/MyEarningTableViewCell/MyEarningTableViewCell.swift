//
//  MyEarningTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 19/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class MyEarningTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLastPlayed: UILabel!
    @IBOutlet weak var lblAddedDate: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configData(details: ReferalDetails) {
        lblAmount.text = details.totalEarnings
        lblAddedDate.text = "Transaction Date".localized() + ": " + details.dateAdded
        lblAmount.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
        lblAmount.text = "+" + details.totalEarnings

        if details.lastPlayed.count > 0 {
            lblLastPlayed.text = "Last Played".localized() + ": " + details.lastPlayed
        }
        else{
            lblLastPlayed.text = ""
        }
        
        
        lblTitle.text = details.username
    }
    
}
