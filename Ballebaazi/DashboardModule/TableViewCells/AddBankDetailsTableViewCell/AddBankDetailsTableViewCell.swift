//
//  AddBankDetailsTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 11/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class AddBankDetailsTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var sumbitButton: SolidButton!
    @IBOutlet weak var acountTxtFieldContainerView: UIView!
    @IBOutlet weak var ifscTxtFieldContainerView: UIView!
    @IBOutlet weak var bankNameTxtFieldContainerView: UIView!
    @IBOutlet weak var branchNameTxtFieldContainerView: UIView!

    
    @IBOutlet weak var lblBankNameTitle: UILabel!
    @IBOutlet weak var lblBranchName: UILabel!
    
    @IBOutlet weak var txtFieldBankName: UITextField!
    @IBOutlet weak var txtFieldIfscCode: UITextField!
    @IBOutlet weak var txtFiledBranchName: UITextField!
    @IBOutlet weak var txtFieldAccountNumber: UITextField!

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblIfscCodeTitle: UILabel!
    @IBOutlet weak var lblAccountNumberTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configData() {

        sumbitButton.setTitle("Submit".localized(), for: .normal)
        acountTxtFieldContainerView.layer.cornerRadius = 5
        ifscTxtFieldContainerView.layer.cornerRadius = 5
        bankNameTxtFieldContainerView.layer.cornerRadius = 5
        branchNameTxtFieldContainerView.layer.cornerRadius = 5
        
        acountTxtFieldContainerView.layer.borderWidth = 1
        ifscTxtFieldContainerView.layer.borderWidth = 1
        bankNameTxtFieldContainerView.layer.borderWidth = 1
        branchNameTxtFieldContainerView.layer.borderWidth = 1

        acountTxtFieldContainerView.layer.borderColor = UIColor(red: 219.0/255, green: 225.0/255, blue: 229.0/255, alpha: 1).cgColor
        ifscTxtFieldContainerView.layer.borderColor = UIColor(red: 219.0/255, green: 225.0/255, blue: 229.0/255, alpha: 1).cgColor
        bankNameTxtFieldContainerView.layer.borderColor = UIColor(red: 219.0/255, green: 225.0/255, blue: 229.0/255, alpha: 1).cgColor
        branchNameTxtFieldContainerView.layer.borderColor = UIColor(red: 219.0/255, green: 225.0/255, blue: 229.0/255, alpha: 1).cgColor

//        containerView.layer.cornerRadius = 10.0
//        containerView.layer.shadowColor = UIColor(red: 78.0/255, green: 84.0/255, blue: 90.0/255, alpha: 0.5).cgColor
//        containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
//        containerView.layer.shadowOpacity = 2.35
//        containerView.layer.shadowRadius = 3
//        containerView.layer.masksToBounds = false

        lblAccountNumberTitle.text = "Account Number".localized()
        lblIfscCodeTitle.text = "IFSC Code".localized()
        lblBankNameTitle.text = "Bank".localized()
        lblBranchName.text = "Branch".localized()
    }
    
        
    func getIFSCDetails(ifscString: String) {
        let params = ["option": "verify_ifsc_code", "ifsc_code": ifscString]

        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(urlString: kVerifyPhone, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                let status = result!["status"]?.stringValue

                if status == "200"{
                    if let response = result!["response"]?.dictionary{
                        if let branch = response["BRANCH"]?.string{
                            self.txtFiledBranchName.text = branch
                        }
                        
                        if let bank = response["BANK"]?.string{
                            self.txtFieldBankName.text = bank
                        }
                    }
                    else{
                        AppHelper.showToast(message: message)
                    }
                }
                else{
                    AppHelper.showToast(message: message)
                }
                
            }
            else{
                self.txtFieldBankName.text = ""
                self.txtFiledBranchName.text = ""
                AppHelper.showAlertView(message: "Please enter valid IFSC code.", isErrorMessage: true)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if txtFieldIfscCode == textField {
            if textField.text!.count == 10{
                if string.count != 0{
                    getIFSCDetails(ifscString: txtFieldIfscCode.text! + string)
                }
                else{
                    self.txtFieldBankName.text = ""
                    self.txtFiledBranchName.text = ""
                }
            }
            if textField.text!.count > 10{
                if string.count != 0{
                    return false
                }
            }
        }
        
        return true
    }
    
    func callUpdateBankDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "verify_bank_request", "user_id": UserDetails.sharedInstance.userID, "bank_name": txtFieldBankName.text!, "account_number": txtFieldAccountNumber.text!, "ifsc_code": txtFieldIfscCode.text!, "bank_branch": txtFiledBranchName.text!,]
        
        WebServiceHandler.performPOSTRequest(urlString: kVerifyPhone, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    AppHelper.showAlertView(message: message, isErrorMessage: false)
                    
                    if let response = result!["response"]?.dictionary {
                        if let bankList = response["bankList"]?.array {
                            if bankList.count > 0 {
                                let bankDetails = bankList[0]
                                let bankDetailsModel = BankDetails()
                                bankDetailsModel.bankName = bankDetails["bank_name"].string ?? ""
                                bankDetailsModel.bankBranch = bankDetails["bank_branch"].string ?? ""
                                bankDetailsModel.accountNumber = bankDetails["account_number"].string ?? ""
                                bankDetailsModel.ifscCode = bankDetails["ifsc_code"].string ?? ""
                                NotificationCenter.default.post(name: Notification.Name("bankDetailsApprovedNotification"), object: bankDetailsModel)

                            }
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if txtFieldAccountNumber.text?.count == 0 {
               
            AppHelper.showAlertView(message: "EnterAccountNumberMsg".localized(), isErrorMessage: true)
            return;
           }
           else if txtFieldIfscCode.text?.count == 0 {
               AppHelper.showAlertView(message: "EnterIFscCode".localized(), isErrorMessage: true)
               return;
           }
           else if txtFieldBankName.text?.count == 0 {
               AppHelper.showAlertView(message: "SelectBank".localized(), isErrorMessage: true)
               return;
           }
           else if txtFiledBranchName.text?.count == 0 {
               AppHelper.showAlertView(message: "EnterBranch".localized(), isErrorMessage: true)
               return;
           }
        callUpdateBankDetailsAPI()
        
    }
    
}
