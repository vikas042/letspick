//
//  BowlingPlayerScoreTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class BowlingPlayerScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var lblWicketCount: UILabel!
    
    @IBOutlet weak var lblMaddenCOunt: UILabel!
    
    @IBOutlet weak var lblWEconomyRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
