//
//  JoinedLeagueUserTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 03/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class JoinedLeagueUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgCoin: UIImageView!
    @IBOutlet weak var lblCreditWonLeadingconstraint: NSLayoutConstraint!
    @IBOutlet weak var swapButton: UIButton!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblCreditWon: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblTeamTopConstainnt: NSLayoutConstraint!
    @IBOutlet weak var rightArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configData(details: JoinedLeagueUserList, isPointsDistributed: Bool, fantasyType: String) {
        
        swapButton.setTitle("Swap".localized(), for: .normal)
        imgCoin.isHidden = true
        if details.rank == "0"{
            lblRank.text = "_ _"
        }
        else{
            lblRank.text = details.rank
        }
        if isPointsDistributed{
            lblCreditWon.isHidden = false;
            let creditWon = Double(details.creditWon) ?? 0.0
            let coinWon = Double(details.coinWon) ?? 0.0
            lblCreditWonLeadingconstraint.constant = 3.0

            if creditWon > 0.0{
                lblCreditWon.text = "(\("Won".localized())" + AppHelper.makeCommaSeparatedDigitsWithString(digites: details.creditWon) + ")"
            }
            else if coinWon > 0.0{
                imgCoin.isHidden = false
                lblCreditWon.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: details.coinWon) + " " + "LP Coins".localized()
                lblCreditWonLeadingconstraint.constant = 13.0
            }

            else{
                lblTeamTopConstainnt.constant = 16.0
                lblCreditWon.isHidden = true;
            }
            layoutIfNeeded()

        }
        else{
            lblTeamTopConstainnt.constant = 16.0
            lblCreditWon.isHidden = true;
            layoutIfNeeded()
        }
        
        lblUserName.text = details.userName
        lblPoints.text = details.totalPoints ?? "0.00" + "Pts."

//        if fantasyType == "4"{
//            lblPoints.text = details.totalPoints ?? "0.00" + "Pts."
//        }
//        else if fantasyType == "5"{
//            lblPoints.text = details.totalPoints ?? "0.00" + "Pts."
//        }
//        else{
//            lblPoints.text = details.totalPoints ?? "0.00" + "Pts."
//        }
        lblTeamName.text = "Team".localized() + " " + details.teamNumber!
    }
}
