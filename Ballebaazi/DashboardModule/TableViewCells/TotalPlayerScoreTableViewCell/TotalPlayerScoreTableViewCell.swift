//
//  TotalPlayerScoreTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class TotalPlayerScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTotal: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
