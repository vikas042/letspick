//
//  AffiliateTransactionsTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 15/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class AffiliateTransactionsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configPartnershipData(details: TransactionsDetails) {
        lblAmount.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
        lblAmount.text = "+" + details.affiliateAmount

        lblTitle.text = "Added to Affiliate Wallet".localized()
//        lblTitle.text = "Withdraw to BB Wallet".localized()
        lblDescription.text = details.receiverUsername + " " + details.matchName
    }

    
}
