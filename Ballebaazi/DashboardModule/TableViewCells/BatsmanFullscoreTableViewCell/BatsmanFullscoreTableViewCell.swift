//
//  BatsmanFullscoreTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class BatsmanFullscoreTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblBowlCount: UILabel!
    @IBOutlet weak var lblBatsmanName: UILabel!
    
    @IBOutlet weak var lblRunsCount: UILabel!
    
    @IBOutlet weak var lblSixesCount: UILabel!
    
    @IBOutlet weak var lblFoursCount: UILabel!
    
    @IBOutlet weak var lblPlayerDescription: UILabel!
    
    @IBOutlet weak var lblStrickRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
