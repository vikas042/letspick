//
//  MatchLeagueTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 04/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
//import Gifu

enum LeagueContestType: Int {
    case LiveContest = 100
    case UpcomingContest = 200
    case CompletedContest = 300
}

class MatchLeagueTableViewCell: UITableViewCell {
    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var liveView: UIView!
    @IBOutlet weak var liveGifImage: UIImageView!
    @IBOutlet weak var lblMatchResult: UILabel!
    @IBOutlet weak var lblSessionShortName: UILabel!
    @IBOutlet weak var lblFirstTeamNameTeam: UILabel!
    @IBOutlet weak var lblsecondTeamNameTeam: UILabel!
    @IBOutlet weak var firstTeamImageView: UIImageView!
    @IBOutlet weak var secondTeamImageView: UIImageView!
    
    var timer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func configData(matchDetails: MatchDetails, contestType: Int)  {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblMatchResult.isHidden = false
        liveView.layer.cornerRadius = 8.0
        liveView.layer.borderWidth = 1.0
        liveView.layer.borderColor = liveGifImage.backgroundColor!.cgColor
        liveView.backgroundColor = UIColor.white
        liveGifImage.layer.cornerRadius = 3.0

        liveView.clipsToBounds = true
        liveView.isHidden = true

        lblSessionShortName.text = matchDetails.seasonShortName.uppercased()
        lblFirstTeamNameTeam.text = matchDetails.firstTeamShortName
        lblsecondTeamNameTeam.text = matchDetails.secondTeamShortName

        if contestType == LeagueContestType.LiveContest.rawValue {
            lblMatchResult.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: matchDetails.startDateTimestemp) + " (Ago)"
            lblMatchResult.textColor = liveGifImage.backgroundColor
            liveView.isHidden = false
            lblMatchResult.text = "Live".localized()
            if timer == nil {
                weak var weakSelf = self
                if #available(iOS 10.0, *) {
                    timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true){_ in
                        weakSelf?.showLiveAnimation()
                    }
                }
                else {
                    timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.showLiveAnimation), userInfo: nil, repeats: true)
                }
            }
        }
        else if contestType == LeagueContestType.UpcomingContest.rawValue {
            lblMatchResult.text = AppHelper.getTimeDifferenceBetweenTwoDatesInReverserOrder(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: matchDetails.startDateTimestemp)
            lblMatchResult.textColor = UIColor(red: 255.0/255, green: 14.0/255 , blue: 0.0/255, alpha: 1)
            liveView.isHidden = true
            if matchDetails.isMatchClosed{
                lblMatchResult.text = "Leagues Closed".localized()
            }
        }
        else if contestType == LeagueContestType.CompletedContest.rawValue {
            lblMatchResult.text = matchDetails.matchResult
            liveView.isHidden = true
            if (matchDetails.adminStatus == "1"){
                lblMatchResult.text = "Points in Review".localized()
                lblMatchResult.textColor = UIColor(red: 255.0/255, green: 184.0/255 , blue: 64.0/255, alpha: 1)
            }
            else{
                lblMatchResult.textColor = UIColor(red: 249.0/255, green: 46.0/255 , blue: 34.0/255, alpha: 1)
            }
        }

        if let url = NSURL(string: matchDetails.firstTeamImageUrl){
            firstTeamImageView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.firstTeamImageView.image = image
                }
                else{
                    self?.firstTeamImageView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.firstTeamImageView.image = UIImage(named: "Placeholder")
        }
        
        if let url = NSURL(string: matchDetails.secondTeamImageUrl){
            secondTeamImageView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.secondTeamImageView.image = image
                }
                else{
                    self?.secondTeamImageView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.secondTeamImageView.image = UIImage(named: "Placeholder")
        }
        
        if (matchDetails.active == "4") && !matchDetails.isMatchClosed {
            lblMatchResult.text = "00:00"
        }

    }
    
    
    
    @objc func showLiveAnimation()  {
        weak var weakSelf = self

        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            weakSelf?.liveGifImage.alpha = 0.0
        }, completion: {
            (finished: Bool) -> Void in
            
            // Fade in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                weakSelf?.liveGifImage.alpha = 1.0
            }, completion: nil)
        })
    }
}
