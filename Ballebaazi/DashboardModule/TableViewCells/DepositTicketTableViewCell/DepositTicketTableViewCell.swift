//
//  DepositTicketTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 06/12/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class DepositTicketTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblCodeTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var lblPromocode: UILabel!
    @IBOutlet weak var lblExpireOnTitle: UILabel!
    @IBOutlet weak var lblGameType: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
