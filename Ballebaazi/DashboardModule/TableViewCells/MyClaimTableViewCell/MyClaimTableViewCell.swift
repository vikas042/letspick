//
//  MyClaimTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 26/02/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class MyClaimTableViewCell: UITableViewCell {

    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPriceTitle: UILabel!
    @IBOutlet weak var lblCoin: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var innerView: UIView!
    
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(details: ClaimDetails) {
        
        DispatchQueue.main.async {
            self.imageContainerView.roundViewCorners(corners: [.topLeft, .bottomLeft], radius: 7)
        }
        AppHelper.showShodowOnAccountCellsView(innerView: innerView)
        lblPriceTitle.text = "Price".localized() + ":"
        lblCoin.text = details.claimBBcoins
        lblTitle.text = details.rewardNameEnglish
        lblDate.text = "Date".localized() + ": " + AppHelper.getTransactionFormattedDate(dateString: details.date)
        lblStatus.text = ""
        
        if details.status == "4" {
            lblStatus.text = "Claimed".localized()
        }
        
        if details.image.count < 10 {
            self.imgView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: details.image){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.imgView.image = UIImage(named: "Placeholder")
        }
    }
    
}
