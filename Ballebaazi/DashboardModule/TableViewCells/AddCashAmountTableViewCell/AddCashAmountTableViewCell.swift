//
//  AddCashAmountTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 12/12/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class AddCashAmountTableViewCell: UITableViewCell {

    @IBOutlet weak var promoTxtField: UITextField!
    @IBOutlet weak var removePromoCodeButton: UIButton!
    @IBOutlet weak var button125: UIButton!
    @IBOutlet weak var button250: UIButton!
    @IBOutlet weak var button500: UIButton!
    @IBOutlet weak var lblEnterAmtTitle: UILabel!
    @IBOutlet weak var lblPromocode: UILabel!
    @IBOutlet weak var removePromoImg: UIImageView!
    @IBOutlet weak var txtFieldAmount: UITextField!
    @IBOutlet weak var lblPromoCodeMessage: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
