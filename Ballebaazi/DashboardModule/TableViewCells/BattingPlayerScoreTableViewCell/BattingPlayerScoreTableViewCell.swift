//
//  BattingPlayerScoreTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class BattingPlayerScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCenturyCount: UILabel!
    
    @IBOutlet weak var lblStrickRate: UILabel!
    
    @IBOutlet weak var lblSixesCount: UILabel!
    
    @IBOutlet weak var lblFoursCount: UILabel!
    
    @IBOutlet weak var lblRuns: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
