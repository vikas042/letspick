//
//  SelectWizardTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/06/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class SelectWizardTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMultiplier: UILabel!
    @IBOutlet weak var lblWizard: UILabel!
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var playerTypeImageView: UIImageView!
    @IBOutlet weak var playerNameWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblWizardSelection: UILabel!

    func configData(details: PlayerDetails, legueType: Int, gameType: Int){
        
        lblCaptain.layer.borderColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1).cgColor
        lblWizardSelection.text = details.wizardRoleSelected + "%"

        lblTeam.text = details.teamShortName
        lblPlayerName.text = details.playerShortName
        
        if (legueType == FantasyType.Classic.rawValue) {
            lblPoints.text = details.classicPoints
        }
        else if legueType == FantasyType.Batting.rawValue{
            lblPoints.text = details.battingPoints
        }
        else if legueType == FantasyType.Bowling.rawValue{
            lblPoints.text = details.bowlingPoints
        }
        else if legueType == FantasyType.Reverse.rawValue{
            lblPoints.text = details.reversePoints
        }
        else if legueType == FantasyType.Wizard.rawValue{
            lblPoints.text = details.wizardPoints
        }

        let constraintValue = details.playerShortName.width(withConstrainedHeight: lblPlayerName.frame.height, font: lblPlayerName.font) + 2.0
        if constraintValue >= (UIScreen.main.bounds.width - 235.0){
            playerNameWidthConstraint.constant = UIScreen.main.bounds.width - 235.0
        }
        else{
            playerNameWidthConstraint.constant = constraintValue
        }
        showPlayerTypeImg(details: details, gameType: gameType)
        playerTypeImageView.image = UIImage(named: details.playerPlaceholder)

        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerTypeImageView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerTypeImageView.image = image
                    }
                    else{
                        self?.playerTypeImageView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }
        else{
            self.playerTypeImageView.image = UIImage(named: details.playerPlaceholder)
        }
    }
    
    func showPlayerTypeImg(details: PlayerDetails, gameType: Int) {
        
        if details.playerPlayingRole == PlayerType.Batsman.rawValue {
            playerTypeImageView.image = UIImage(named: "BatIcon")
        }
        else if details.playerPlayingRole == PlayerType.Bowler.rawValue {
            playerTypeImageView.image = UIImage(named: "BowlIcon")
        }
        else if details.playerPlayingRole == PlayerType.AllRounder.rawValue {
            if gameType == GameType.Kabaddi.rawValue{
                playerTypeImageView.image = UIImage(named: "AllrounderKabaddiPlaceholder")
            }
            else{
                playerTypeImageView.image = UIImage(named: "AllRounderIcon")
            }
        }
        else if details.playerPlayingRole == PlayerType.WicketKeeper.rawValue {
            playerTypeImageView.image = UIImage(named: "WicketIcon")
        }
        else if details.playerPlayingRole == PlayerType.Defender.rawValue {
            if gameType == GameType.Football.rawValue{
                playerTypeImageView.image = UIImage(named: "FootballDefenderIcon")
            }
            else{
                playerTypeImageView.image = UIImage(named: "DefenderIconPlaceholder")
            }
        }
        else if details.playerPlayingRole == PlayerType.Raider.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
        }
        else if details.playerPlayingRole == PlayerType.GoalKeeper.rawValue {
            playerTypeImageView.image = UIImage(named: "FootballGoalKeeperIcon")
        }
        else if details.playerPlayingRole == PlayerType.MidFielder.rawValue {
            playerTypeImageView.image = UIImage(named: "FootballMidFielderIcon")
        }
        else if details.playerPlayingRole == PlayerType.Sticker.rawValue {
            playerTypeImageView.image = UIImage(named: "FootballForwardIcon")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
