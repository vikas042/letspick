//
//  PhoneVerificationTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class PhoneVerificationTableViewCell: UITableViewCell {

    @IBOutlet weak var innerView: UIView!

    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var verifiedImageView: UIImageView!
    @IBOutlet weak var lblVerificationStatus: UILabel!
    @IBOutlet weak var lblDocumentNumber: UILabel!
//    @IBOutlet weak var dropDownButton: UIButton!

    @IBOutlet weak var lblMobileTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configData()  {
        showShadow()
        lblMobileTitle.text = "Mobile_number".localized()
        lblPhone.text = UserDetails.sharedInstance.phoneNumber
        if UserDetails.sharedInstance.phoneVerified {
            verifiedImageView.image = #imageLiteral(resourceName: "VerifiedDocumentIcon")
            lblVerificationStatus.text = "Verified".localized()
            lblVerificationStatus.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)

        }
        else{
            verifiedImageView.image = #imageLiteral(resourceName: "NotVerifyDocumentIcon")
            lblVerificationStatus.text = "Not Verified".localized()
            lblVerificationStatus.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
    }
    

    
    func showShadow()  {
        
//        innerView.layer.borderWidth = 0.5
//        innerView.layer.borderColor = UIColor(red: 213.0/255, green: 213.0/255, blue: 213.0/255, alpha: 1.0).cgColor
//        innerView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
//        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
//        innerView.layer.shadowOpacity = 0.25
//        innerView.layer.shadowRadius = 1.0
//        innerView.layer.masksToBounds = false
    }

}
