//
//  AdharVerificationTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class AdharVerificationTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var lblOptionTitle: UILabel!
    
    @IBOutlet weak var lblAadhaarTitle: UILabel!
    @IBOutlet weak var lblNameHint: UILabel!
    @IBOutlet weak var aadharImagesView: UIView!
    @IBOutlet weak var submitButton: SolidButton!
    @IBOutlet weak var dobDropDownArrow: UIImageView!
    @IBOutlet weak var stateDropDownArrow: UIImageView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblDocumentNumber: UILabel!

    
    @IBOutlet weak var secondImgButton: UIButton!
    @IBOutlet weak var firstImgButton: UIButton!
    @IBOutlet weak var verifiedImageView: UIImageView!
    
    @IBOutlet weak var dropDownButton: UIButton!
    
    @IBOutlet weak var firstAddressTxtField: UITextField!
    
    @IBOutlet weak var secondsAddressTxtField: UITextField!
    
    @IBOutlet weak var cityAddressTxtField: UITextField!
    
    @IBOutlet weak var zipCodeAddressTxtField: UITextField!
    
    
    @IBOutlet weak var lblVerificationStatus: UILabel!

    @IBOutlet weak var fieldsContainerView: UIView!
    @IBOutlet weak var secondImageView: UIImageView!
    
    @IBOutlet weak var firstImageView: UIImageView!
    
    @IBOutlet weak var textFieldName: UITextField!
    
    @IBOutlet weak var textFieldAdharNumber: UITextField!
    @IBOutlet weak var textFielddob: UITextField!
    @IBOutlet weak var textFieldState: UITextField!
    @IBOutlet weak var firstImageContainerView: UIView!
    @IBOutlet weak var secoundImageContainerView: UIView!

    var statePicker = UIPickerView()
    var selectedDate = ""
    let datePicker: UIDatePicker = UIDatePicker()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(_ details: AadhaarDetails?)  {
//        datePicker.locale = Locale(identifier: "en_US")

        showShadow()
        
        if UserDetails.sharedInstance.aadhaarVerified {
            verifiedImageView.image = #imageLiteral(resourceName: "VerifiedDocumentIcon")
            lblVerificationStatus.text = "Verified".localized()
            lblVerificationStatus.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
        }
        else if UserDetails.sharedInstance.aadhaarVerifictionSubmitted {
            verifiedImageView.image = #imageLiteral(resourceName: "SubittedForVerificationDocumentIcon")
            lblVerificationStatus.text = "Submitted".localized()
            lblVerificationStatus.textColor = UIColor(red: 254.0/255, green: 168.0/255, blue: 15.0/255, alpha: 1)
        }
        else{
            verifiedImageView.image = #imageLiteral(resourceName: "NotVerifyDocumentIcon")
            lblVerificationStatus.text = "Not Verified".localized()
            lblVerificationStatus.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        
        statePicker.delegate = self;
        textFieldState.inputView = statePicker
        let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        textFielddob.inputView = datePicker
        datePicker.datePickerMode = .date
        datePicker.maximumDate = date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        
        textFielddob.placeholder = "DOB".localized()
        textFieldState.placeholder = "State".localized()
        lblAadhaarTitle.text = "Aadhaar Card".localized()
        lblOptionTitle.text = "(Optional)".localized()
        textFieldName.placeholder = "Full Name".localized()
        textFieldAdharNumber.placeholder = "Adhar Card Number".localized()
        firstAddressTxtField.placeholder = "Address".localized()
        secondsAddressTxtField.placeholder = "Address(Optional)".localized()
        cityAddressTxtField.placeholder = "City".localized()
        zipCodeAddressTxtField.placeholder = "Zip Code".localized()
        lblNameHint.text = "pan_card_msg".localized()
        
        guard let aadharDetails = details else { return }
        
        textFieldName.isUserInteractionEnabled = false
        textFieldAdharNumber.isUserInteractionEnabled = false
        textFielddob.isUserInteractionEnabled = false
        firstAddressTxtField.isUserInteractionEnabled = false
        cityAddressTxtField.isUserInteractionEnabled = false
        zipCodeAddressTxtField.isUserInteractionEnabled = false
        textFieldState.isUserInteractionEnabled = false
        secondsAddressTxtField.isUserInteractionEnabled = false
        aadharImagesView.isHidden = true;
        submitButton.isHidden = true;
        dobDropDownArrow.isHidden = true;
        stateDropDownArrow.isHidden = true;
        firstImgButton.isHidden = true;
        secondImgButton.isHidden = true;
        lblNameHint.isHidden = true;
        
        textFieldName.text = aadharDetails.aadhaarName
        textFieldAdharNumber.text = aadharDetails.aadhaarNumber
        textFielddob.text = AppHelper.getRequiredFormattedDate(dateString: aadharDetails.dob)
        textFieldState.text = aadharDetails.state
        firstAddressTxtField.text = aadharDetails.address1
        secondsAddressTxtField.text = aadharDetails.address1
        cityAddressTxtField.text = aadharDetails.city
        zipCodeAddressTxtField.text = aadharDetails.zipcode
        
        if let url = NSURL(string: aadharDetails.image){
            firstImageView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.firstImageView.image = image
                }
                else{
                    self?.firstImageView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.firstImageView.image = UIImage(named: "Placeholder")
        }
        
        if let url = NSURL(string: aadharDetails.image1){
            secondImageView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.secondImageView.image = image
                }
                else{
                    self?.secondImageView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.secondImageView.image = UIImage(named: "Placeholder")
        }

    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
//        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
//            let locatStr = lang[0]
//            if locatStr == "hi"{
//                dateFormatter.locale = Locale(identifier: locatStr)
//            }
//            else{
//                dateFormatter.locale = Locale(identifier: "en_US")
//            }
//        }
        dateFormatter.locale = Locale(identifier: "en_US")

        selectedDate = dateFormatter.string(from: sender.date)
        textFielddob.text = AppHelper.getRequiredFormattedDate(dateString: selectedDate)
    }

    func showShadow()  {
        
        firstImageContainerView.layer.borderWidth = 1.0
        firstImageContainerView.layer.borderColor = UIColor(red: 112.0/255, green: 112.0/255, blue: 112.0/255, alpha: 1).cgColor
        secoundImageContainerView.layer.borderWidth = 1.0
        secoundImageContainerView.layer.borderColor = UIColor(red: 112.0/255, green: 112.0/255, blue: 112.0/255, alpha: 1).cgColor
        
//        innerView.layer.borderWidth = 0.5
//        innerView.layer.borderColor = UIColor(red: 213.0/255, green: 213.0/255, blue: 213.0/255, alpha: 1.0).cgColor
//        innerView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
//        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
//        innerView.layer.shadowOpacity = 0.25
//        innerView.layer.shadowRadius = 1.0
//        innerView.layer.masksToBounds = false
    }
    
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if textFieldName.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterNameMsg".localized(), isErrorMessage: true)
            return;
        }
        else if textFieldAdharNumber.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterAadharNumber".localized(), isErrorMessage: true)
            return;
        }
        else if textFielddob.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterIFscCode".localized(), isErrorMessage: true)
            return;
        }
        else if textFielddob.text?.count == 0 {
            AppHelper.showAlertView(message: "SelectDOB".localized(), isErrorMessage: true)
            return;
        }
        else if firstAddressTxtField.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter the address.", isErrorMessage: true)
            return;
        }
        else if cityAddressTxtField.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter your city.", isErrorMessage: true)
            return;
        }
        else if zipCodeAddressTxtField.text?.count == 0 {
            AppHelper.showAlertView(message: "Please enter your zip code.", isErrorMessage: true)
            return;
        }
        else if textFieldState.text?.count == 0 {
            AppHelper.showAlertView(message: "SelectState".localized(), isErrorMessage: true)
            return;
        }
        else if firstImageView?.image == nil {
            AppHelper.showAlertView(message: "SelectAadharImage".localized(), isErrorMessage: true)
            return;
        }
        else if secondImageView?.image == nil {
            AppHelper.showAlertView(message: "SelectAadharImage".localized(), isErrorMessage: true)
            return;
        }
        
        callUpdateAadharDetailsAPI()
    }
    
    @IBAction func photoButtonTapped(_ sender: Any) {
        
        let actionSheet: UIAlertController = UIAlertController(title: "Please choose an option", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheet.addAction(cancelActionButton)
        weak var weakSelf = self

        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            weakSelf?.cameraButtonTapped()
        }
        actionSheet.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            weakSelf?.galleryButtonTapped()
        }
        actionSheet.addAction(deleteActionButton)
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    func galleryButtonTapped() {
       
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func cameraButtonTapped() {
       
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if (firstImageView.image != nil) {
            secondImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        }
        else{
            firstImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        }
    }
    
    func callUpdateAadharDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        AppxorEventHandler.logAppEvent(withName: "AadharCardSubmitClicked", info: nil)

        var secondsAddress = ""
        
        if secondsAddressTxtField.text?.count != 0 {
            secondsAddress = secondsAddressTxtField.text!
        }
        
        let params = ["option": "verify_aadhaar_request", "user_id": UserDetails.sharedInstance.userID, "aadhaar_name": textFieldName.text!, "aadhaar_number": textFieldAdharNumber.text!, "dob": selectedDate, "state": textFieldState.text!, "address1": firstAddressTxtField.text!, "address2": secondsAddress, "city": cityAddressTxtField.text!, "zipcode": zipCodeAddressTxtField.text!]
        
        let firstImageData = UIImageJPEGRepresentation(firstImageView.image!, 0.5)
        let secImageData = UIImageJPEGRepresentation(firstImageView.image!, 0.5)

        if firstImageData == nil{
            return;
        }
        
        if secImageData == nil{
            return;
        }

        
        WebServiceHandler.performMultipartRequest(urlString: kVerifyPhone, fileName: "image", params: params, imageDataArray: [firstImageData!, secImageData!], accessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    AppHelper.showAlertView(message: message, isErrorMessage: false)
                    NotificationCenter.default.post(name: Notification.Name("updateTableWithStatus"), object: SelectedDocumentType.ADHAR.rawValue)
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    //MARK:- Picker View Data Source and Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return UserDetails.sharedInstance.statsArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let details = UserDetails.sharedInstance.statsArray[row]
        return details["stateName"].string
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let details = UserDetails.sharedInstance.statsArray[row]
        textFieldState.text = details["stateName"].string
    }

}
