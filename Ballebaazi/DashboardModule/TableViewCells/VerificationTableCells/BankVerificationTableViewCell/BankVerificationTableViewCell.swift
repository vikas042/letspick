//
//  BankVerificationTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class BankVerificationTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var lblBankTitle: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var submitButton: SolidButton!
    @IBOutlet weak var fieldsContainerView: UIView!
    @IBOutlet weak var bankImageView: UIImageView!
    @IBOutlet weak var verifiedImageView: UIImageView!
    @IBOutlet weak var lblVerificationStatus: UILabel!
    @IBOutlet weak var dropdownButton: UIButton!
    @IBOutlet weak var lblDocumentNumber: UILabel!
    @IBOutlet weak var textFieldAccount: UITextField!
    @IBOutlet weak var textFieldConfirmAccount: UITextField!
    @IBOutlet weak var textFieldIfscCodeunt: UITextField!
    @IBOutlet weak var textFieldBank: UITextField!
    @IBOutlet weak var textFieldBranch: UITextField!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configData(_ details: BankDetails?)  {
        textFieldBank.isUserInteractionEnabled = false
        textFieldBranch.isUserInteractionEnabled = false
        lblBankTitle.text = "Bank Account".localized();
        
        if UserDetails.sharedInstance.bankVerified {
            verifiedImageView.image = #imageLiteral(resourceName: "VerifiedDocumentIcon")
            lblVerificationStatus.text = "Accepted".localized()
            lblVerificationStatus.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
        }
        else if UserDetails.sharedInstance.bankVerifictionSubmitted {
            verifiedImageView.image = #imageLiteral(resourceName: "SubittedForVerificationDocumentIcon")
            lblVerificationStatus.text = "Submitted".localized()
            lblVerificationStatus.textColor = UIColor(red: 254.0/255, green: 168.0/255, blue: 15.0/255, alpha: 1)
        }
        else{
            verifiedImageView.image = #imageLiteral(resourceName: "NotVerifyDocumentIcon")
            lblVerificationStatus.text = "Not Verified".localized()
            lblVerificationStatus.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        
        showShadow()
        
        guard let bankDetails = details else { return }
        
        textFieldAccount.placeholder = "Account Number".localized()
        textFieldConfirmAccount.placeholder = "Confirm Account Number".localized()
        textFieldIfscCodeunt.placeholder = "IFSC Code".localized()
        textFieldBank.placeholder = "Bank".localized()
        textFieldBranch.placeholder = "Branch".localized()

        textFieldAccount.isUserInteractionEnabled = false
        textFieldConfirmAccount.isUserInteractionEnabled = false
        textFieldIfscCodeunt.isUserInteractionEnabled = false
        textFieldBank.isUserInteractionEnabled = false
        textFieldBranch.isUserInteractionEnabled = false

        textFieldAccount.text = bankDetails.accountNumber
        textFieldConfirmAccount.text = bankDetails.accountNumber
        textFieldIfscCodeunt.text = bankDetails.ifscCode
        textFieldBank.text = bankDetails.bankName
        textFieldBranch.text = bankDetails.bankBranch
        submitButton.isHidden = true;
    }
    

    func showShadow()  {
        
//        imageContainerView.layer.borderWidth = 1.0
//        imageContainerView.layer.borderColor = UIColor(red: 112.0/255, green: 112.0/255, blue: 112.0/255, alpha: 1).cgColor
//        innerView.layer.borderWidth = 0.5
//         innerView.layer.borderColor = UIColor(red: 213.0/255, green: 213.0/255, blue: 213.0/255, alpha: 1.0).cgColor
//         innerView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
//         innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
//         innerView.layer.shadowOpacity = 0.25
//         innerView.layer.shadowRadius = 1.0
//         innerView.layer.masksToBounds = false
    }
    
    

    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if textFieldAccount.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterAccountNumberMsg".localized(), isErrorMessage: true)
            return;
        }
        else if textFieldConfirmAccount.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterConfirmAccountNumberMsg".localized(), isErrorMessage: true)
            return;
        }
        else if textFieldAccount.text != textFieldConfirmAccount.text {
            AppHelper.showAlertView(message: "EnterConfirmAccountNumberNotMatchedMsg".localized(), isErrorMessage: true)
            return;
        }
        else if textFieldIfscCodeunt.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterIFscCode".localized(), isErrorMessage: true)
            return;
        }
        else if textFieldBank.text?.count == 0 {
            AppHelper.showAlertView(message: "SelectBank".localized(), isErrorMessage: true)
            return;
        }
        else if textFieldBranch.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterBranch".localized(), isErrorMessage: true)
            return;
        }
//        else if bankImageView?.image == nil {
//            AppHelper.showAlertView(message: "SelectBank".localized()Image)
//            return;
//        }
        
        callUpdatePanDetailsAPI()
    }
    
    @IBAction func photoButtonTapped(_ sender: Any) {
      
        let actionSheet: UIAlertController = UIAlertController(title: "Please choose an option", message: nil, preferredStyle: .actionSheet)
        
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }

        actionSheet.addAction(cancelActionButton)
        weak var weakSelf = self

        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            weakSelf?.cameraButtonTapped()
        }
        actionSheet.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            weakSelf?.galleryButtonTapped()
        }
        actionSheet.addAction(deleteActionButton)
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    
    func galleryButtonTapped() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func cameraButtonTapped() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        bankImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
    }
    
    func callUpdatePanDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        AppxorEventHandler.logAppEvent(withName: "BankDetailsSubmitClicked", info: nil)

        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "verify_bank_request", "user_id": UserDetails.sharedInstance.userID, "bank_name": textFieldBank.text!, "account_number": textFieldAccount.text!, "ifsc_code": textFieldIfscCodeunt.text!, "bank_branch": textFieldBranch.text!,]
        
        WebServiceHandler.performPOSTRequest(urlString: kVerifyPhone, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    AppHelper.showAlertView(message: message, isErrorMessage: false)
                    NotificationCenter.default.post(name: Notification.Name("updateTableWithStatus"), object: SelectedDocumentType.BANK.rawValue)
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    //MARK:- Picker View Data Source and Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return UserDetails.sharedInstance.statsArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let details = UserDetails.sharedInstance.statsArray[row]
        return details["stateName"].string
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let details = UserDetails.sharedInstance.statsArray[row]
        textFieldBank.text = details["stateName"].string
    }
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//        if textField == textFieldIfscCodeunt {
//            textFieldBank.text = ""
//            textFieldBranch.text = ""
//        }
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == textFieldIfscCodeunt {
//            if textField.text!.count > 0{
//                getIFSCDetails()
//            }
//        }
//    }
    
    @IBAction func verifyIfscCodeButtonTapped(_ sender: Any) {
        if textFieldIfscCodeunt.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterIFscCode".localized(), isErrorMessage: true)
            return;
        }
//        getIFSCDetails()
    }
    
    func getIFSCDetails(ifscString: String) {
        let params = ["option": "verify_ifsc_code", "ifsc_code": ifscString]

        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(urlString: kVerifyPhone, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                let status = result!["status"]?.stringValue

                if status == "200"{
                    if let response = result!["response"]?.dictionary{
                        if let branch = response["BRANCH"]?.string{
                            self.textFieldBranch.text = branch
                        }
                        
                        if let bank = response["BANK"]?.string{
                            self.textFieldBank.text = bank
                        }
                    }
                    else{
                        AppHelper.showToast(message: message)
                    }
                }
                else{
                    AppHelper.showToast(message: message)
                }
                
            }
            else{
                self.textFieldBranch.text = ""
                self.textFieldBank.text = ""
                AppHelper.showAlertView(message: "Please enter valid IFSC code.", isErrorMessage: true)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textFieldIfscCodeunt == textField {
            if textField.text!.count == 10{
                if string.count != 0{
                    getIFSCDetails(ifscString: textFieldIfscCodeunt.text! + string)
                }
                else{
                    self.textFieldBranch.text = ""
                    self.textFieldBank.text = ""
                }
            }
            if textField.text!.count > 10{
                if string.count != 0{
                    return false
                }
            }
        }
        
        return true
    }
}
