//
//  PANVerificationTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class PANVerificationTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var lblPanCardTitle: UILabel!
    @IBOutlet weak var lblPanCardHint: UILabel!
    @IBOutlet weak var lblDobHint: UILabel!
    @IBOutlet weak var innerView: UIView!

    @IBOutlet weak var dobDropdownArrow: UIImageView!
    @IBOutlet weak var stateDropdownArrow: UIImageView!
    @IBOutlet weak var submitButton: SolidButton!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var lblDocumentNumber: UILabel!
    @IBOutlet weak var verifiedImageView: UIImageView!
    
    @IBOutlet weak var lblVerificationStatus: UILabel!

    @IBOutlet weak var fieldsContainerView: UIView!
    @IBOutlet weak var textFieldPanNumber: UITextField!
    @IBOutlet weak var textFielDob: UITextField!
    @IBOutlet weak var textFieldState: UITextField!
    
    @IBOutlet weak var imageContainerView: UIView!
    
    let datePicker: UIDatePicker = UIDatePicker()
    var statePicker = UIPickerView()

    var selectedDate = ""

    @IBOutlet weak var panImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configData(_ details: PanDetails?)  {
        showShadow()
        
//        datePicker.locale = Locale(identifier: "en_US")

        submitButton.setTitle("Submit for verification".localized(), for: .normal)
        
        if UserDetails.sharedInstance.panVerified {
            verifiedImageView.image = #imageLiteral(resourceName: "VerifiedDocumentIcon")
            lblVerificationStatus.text = "Verified".localized()
            lblVerificationStatus.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
        }
        else if UserDetails.sharedInstance.panVerifictionSubmitted {
            verifiedImageView.image = #imageLiteral(resourceName: "SubittedForVerificationDocumentIcon")
            lblVerificationStatus.text = "Submitted".localized()
            lblVerificationStatus.textColor = UIColor(red: 254.0/255, green: 168.0/255, blue: 15.0/255, alpha: 1)
        }
        else{
            verifiedImageView.image = #imageLiteral(resourceName: "NotVerifyDocumentIcon")
            lblVerificationStatus.text = "Not Verified".localized()
            lblVerificationStatus.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        
        statePicker.delegate = self;
        textFieldState.inputView = statePicker
        let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        textFielDob.inputView = datePicker
        datePicker.datePickerMode = .date
        datePicker.maximumDate = date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        lblPanCardTitle.text = "PAN Card".localized()
        textFieldPanNumber.placeholder = "PAN Card Number".localized()
        textFielDob.placeholder = "DOB".localized()
        textFieldState.placeholder = "State".localized()
        textFieldPanNumber.placeholder = "PAN Card".localized()
        lblDobHint.text = "DOB_hind".localized();
        lblPanCardHint.text = "PanCardNameHint".localized();
        guard let panDetails = details else { return }
        textFieldPanNumber.isUserInteractionEnabled = false
        textFielDob.isUserInteractionEnabled = false
        textFieldState.isUserInteractionEnabled = false
        stateDropdownArrow.isHidden = true
        dobDropdownArrow.isHidden = true
        lblDobHint.isHidden = true
        lblPanCardHint.isHidden = true
        textFieldPanNumber.text = panDetails.panNumber
        textFielDob.text = AppHelper.getRequiredFormattedDate(dateString: panDetails.dob)
        textFieldState.text = panDetails.state
        submitButton.isHidden = true;

    }
    

    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US")
        selectedDate = dateFormatter.string(from: sender.date)
        textFielDob.text = AppHelper.getRequiredFormattedDate(dateString: selectedDate)
    }
    
    func showShadow()  {
        
//        imageContainerView.layer.borderWidth = 1.0
//        imageContainerView.layer.borderColor = UIColor(red: 112.0/255, green: 112.0/255, blue: 112.0/255, alpha: 1).cgColor
        
//        innerView.layer.borderWidth = 0.5
//        innerView.layer.borderColor = UIColor(red: 213.0/255, green: 213.0/255, blue: 213.0/255, alpha: 1.0).cgColor
//        innerView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
//        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
//        innerView.layer.shadowOpacity = 0.25
//        innerView.layer.shadowRadius = 1.0
//        innerView.layer.masksToBounds = false
    }
    
    
    @IBAction func submitButtonTapped(_ sender: Any) {

        
        if textFieldPanNumber.text?.count == 0 {
            AppHelper.showAlertView(message: "EnterPANNumberMsg".localized(), isErrorMessage: true)
            return;
        }
        else if textFielDob.text?.count == 0 {
            AppHelper.showAlertView(message: "SelectDOB".localized(), isErrorMessage: true)
            return;
        }
        else if textFieldState.text?.count == 0 {
            AppHelper.showAlertView(message: "SelectState".localized(), isErrorMessage: true)
            return;
        }
//        else if panImageView?.image == nil {
//            AppHelper.showAlertView(message: SelectPanImage)
//            return;
//        }
        
        callUpdatePanDetailsAPI()
    }
    
    @IBAction func photoButtonTapped(_ sender: Any) {
        let actionSheet: UIAlertController = UIAlertController(title: "Please choose an option", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheet.addAction(cancelActionButton)
        weak var weakSelf = self

        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            weakSelf?.cameraButtonTapped()
        }
        actionSheet.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            weakSelf?.galleryButtonTapped()
        }
        actionSheet.addAction(deleteActionButton)
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    
    func galleryButtonTapped() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }

    }
    
    func cameraButtonTapped() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
         panImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage

    }
    
    func callUpdatePanDetailsAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        AppxorEventHandler.logAppEvent(withName: "PANCardSubmitClicked", info: nil)

        let params = ["option": "verify_pan_request", "user_id": UserDetails.sharedInstance.userID, "pan_number": textFieldPanNumber.text!, "dob": selectedDate, "state": textFieldState.text!,]

        WebServiceHandler.performPOSTRequest(urlString: kVerifyPhone, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                    NotificationCenter.default.post(name: Notification.Name("updateTableWithStatus"), object: SelectedDocumentType.PAN.rawValue)                    
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }

    
    //MARK:- Picker View Data Source and Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return UserDetails.sharedInstance.statsArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let details = UserDetails.sharedInstance.statsArray[row]
        return details["stateName"].string
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let details = UserDetails.sharedInstance.statsArray[row]
        textFieldState.text = details["stateName"].string
    }

}
