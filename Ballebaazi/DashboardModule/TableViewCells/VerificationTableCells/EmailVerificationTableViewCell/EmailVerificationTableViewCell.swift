//
//  EmailVerificationTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class EmailVerificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var verifiedImageView: UIImageView!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var lblVerificationStatus: UILabel!
    @IBOutlet weak var lblDocumentNumber: UILabel!
    @IBOutlet weak var submitButton: SolidButton!
    @IBOutlet weak var fieldsContainerView: UIView!
    @IBOutlet weak var lblEmailAddressTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configData()  {
        txtFieldEmail.placeholder = "email_id".localized()
        lblEmailAddressTitle.text = "Email Address".localized()
        
        lblEmail.text = UserDetails.sharedInstance.emailAdress;
        if UserDetails.sharedInstance.emailVerified {
            verifiedImageView.image = #imageLiteral(resourceName: "VerifiedDocumentIcon")
            lblVerificationStatus.text = "Verified".localized()
            lblVerificationStatus.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
        }
        else{
            lblEmail.text = ""
            verifiedImageView.image = #imageLiteral(resourceName: "NotVerifyDocumentIcon")
            lblVerificationStatus.text = "Not Verified".localized()
            lblVerificationStatus.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        
        showShadow()
    }
    
    func showShadow()  {
        
//        innerView.layer.borderWidth = 0.5
//        innerView.layer.borderColor = UIColor(red: 213.0/255, green: 213.0/255, blue: 213.0/255, alpha: 1.0).cgColor
//        innerView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
//        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
//        innerView.layer.shadowOpacity = 0.25
//        innerView.layer.shadowRadius = 1.0
//        innerView.layer.masksToBounds = false
    }

    
    @IBAction func resendButtonTapped(_ sender: Any) {
        
        if txtFieldEmail.text?.count == 0{
            AppHelper.showAlertView(message: "EnterEmailMsg".localized(), isErrorMessage: true)
            return
        }
        else if AppHelper.isNotValidEmail(email: txtFieldEmail.text!){
            AppHelper.showAlertView(message: "EnterValidEmailMsg".localized(), isErrorMessage: true)
            return
        }
        
        callResendEmailAPI()
    }
    
    func callResendEmailAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        AppxorEventHandler.logAppEvent(withName: "EmailIDSubmitClicked", info: nil)

        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "verify_email_request", "user_id": UserDetails.sharedInstance.userID, "email": txtFieldEmail.text!]
        
        WebServiceHandler.performPOSTRequest(urlString: kVerifyPhone, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                let message = result!["message"]?.string ?? ""
                
                if statusCode == "200"{
                    AppHelper.showAlertView(message: message, isErrorMessage: false)
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
}
