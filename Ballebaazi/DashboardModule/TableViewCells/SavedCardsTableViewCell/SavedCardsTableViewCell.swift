//
//  SavedCardsTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 24/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class SavedCardsTableViewCell: UITableViewCell {

    @IBOutlet weak var cardTypeImgView: UIImageView!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var txtField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
