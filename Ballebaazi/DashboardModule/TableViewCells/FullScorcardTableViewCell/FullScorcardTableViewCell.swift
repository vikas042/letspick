//
//  FullScorcardTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

enum VisiableTeam: Int {
    case FirstTeam = 1000
    case SecondTeam = 2000
    case ThirdTeam = 3000
    case FourthTeam = 4000
}


class FullScorcardTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var dropDownButton: UIButton!    
    @IBOutlet weak var fallOfWicketView: UIView!
    @IBOutlet weak var lblFallOfWicketsDescription: UILabel!    
    @IBOutlet weak var batsmanTableContainerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var fallOfWicketsContainerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var batsmanView: UIView!
    
    @IBOutlet weak var bowlerView: UIView!
   
    @IBOutlet weak var batsmanTblView: UITableView!
    
    @IBOutlet weak var bowlerTblView: UITableView!
    
    @IBOutlet weak var dropdownArrow: UIImageView!
    
    @IBOutlet weak var innerView: UIView!
    
    @IBOutlet weak var lblTeamName: UILabel!
    
    @IBOutlet weak var lblScoreTitle: UILabel!
    
    var firstTeamBatsmanArray = Array<LiveScoreMathchPlayerDetails>()
    var secodsTeamBatsmanArray = Array<LiveScoreMathchPlayerDetails>()
    var thirdTeamBatsmanArray = Array<LiveScoreMathchPlayerDetails>()
    var fourthTeamBatsmanArray = Array<LiveScoreMathchPlayerDetails>()
    var firstTeamBowlerArray = Array<LiveScoreMathchPlayerDetails>()
    var secondTeamBowlerArray = Array<LiveScoreMathchPlayerDetails>()
    var thirdTeamBowlerArray = Array<LiveScoreMathchPlayerDetails>()
    var fourthTeamBowlerArray = Array<LiveScoreMathchPlayerDetails>()
    
    var selectedTeamType = VisiableTeam.FirstTeam.rawValue;
    var scoreDetails: LiveScoreDetails?
    
    
    func firstTeamData(liveScoreDetails: LiveScoreDetails)  {
        showShadow()
       
        scoreDetails = liveScoreDetails
        selectedTeamType = VisiableTeam.FirstTeam.rawValue;
        firstTeamBatsmanArray = liveScoreDetails.firstTeamBatsmanArray
        firstTeamBowlerArray = liveScoreDetails.firstTeamBowlerArray
        secodsTeamBatsmanArray = liveScoreDetails.secodsTeamBatsmanArray
        secondTeamBowlerArray = liveScoreDetails.secondTeamBowlerArray
        
        batsmanTblView.register(UINib(nibName: "BatsmanFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BatsmanFullscoreTableViewCell")
        batsmanTblView.register(UINib(nibName: "TotalFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "TotalFullscoreTableViewCell")

        bowlerTblView.register(UINib(nibName: "BowlerFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BowlerFullscoreTableViewCell")
        lblFallOfWicketsDescription.text = liveScoreDetails.firstTeamFallOfWickets
        let deviceWidth = self.frame.size.width - 93
        
        let textHeight = scoreDetails?.firstTeamDoNotBats.height(withConstrainedWidth: deviceWidth, font: UIFont(name: "OpenSans-Semibold", size: 15)!)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let fallofWicketTextHeight = scoreDetails?.firstTeamFallOfWickets.height(withConstrainedWidth: screenWidth - 45, font: UIFont(name: "OpenSans-Semibold", size: 15)!)

        fallOfWicketsContainerViewHeightConstraint.constant = fallofWicketTextHeight! + 50 + 20
        batsmanTableContainerViewHeightConstraint.constant = CGFloat(firstTeamBatsmanArray.count * 50) + 30.0 + 100 + textHeight! + 30
        batsmanView.layoutIfNeeded()
        fallOfWicketView.layoutIfNeeded()
        batsmanTblView.reloadData()
        bowlerTblView.reloadData()
        lblTeamName.text = liveScoreDetails.firstTeamShortName
        lblScoreTitle.text = liveScoreDetails.firstTeamTotalTitleScore
    }
    
    func secondTeamData(liveScoreDetails: LiveScoreDetails)  {
        showShadow()
        
        scoreDetails = liveScoreDetails
        selectedTeamType = VisiableTeam.SecondTeam.rawValue;
        secodsTeamBatsmanArray = liveScoreDetails.secodsTeamBatsmanArray
        secondTeamBowlerArray = liveScoreDetails.secondTeamBowlerArray
        
        batsmanTblView.register(UINib(nibName: "BatsmanFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BatsmanFullscoreTableViewCell")
        batsmanTblView.register(UINib(nibName: "TotalFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "TotalFullscoreTableViewCell")

        bowlerTblView.register(UINib(nibName: "BowlerFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BowlerFullscoreTableViewCell")
        
        lblFallOfWicketsDescription.text = liveScoreDetails.secondTeamFallOfWickets

        let deviceWidth = self.frame.size.width - 93
        
        let textHeight = scoreDetails?.secondTeamDoNotBats.height(withConstrainedWidth: deviceWidth, font: UIFont(name: "OpenSans", size: 15)!)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let fallofWicketTextHeight = scoreDetails?.secondTeamFallOfWickets.height(withConstrainedWidth: screenWidth - 45, font: UIFont(name: "OpenSans", size: 15)!)
        
        fallOfWicketsContainerViewHeightConstraint.constant = fallofWicketTextHeight! + 50 + 20
        batsmanTableContainerViewHeightConstraint.constant = CGFloat(secodsTeamBatsmanArray.count * 50) + 30.0 + 100 + textHeight! + 30
        
        batsmanView.layoutIfNeeded()
        fallOfWicketView.layoutIfNeeded()

        batsmanTblView.reloadData()
        bowlerTblView.reloadData()
        lblTeamName.text = liveScoreDetails.secondTeamFullName
        lblScoreTitle.text = liveScoreDetails.secondTeamTotalTitleScore
    }
    
    func thirdTeamData(liveScoreDetails: LiveScoreDetails)  {
        showShadow()
        
        scoreDetails = liveScoreDetails
        selectedTeamType = VisiableTeam.ThirdTeam.rawValue;
        thirdTeamBatsmanArray = liveScoreDetails.thirdTeamBatsmanArray
        thirdTeamBowlerArray = liveScoreDetails.thirdTeamBowlerArray
        
        batsmanTblView.register(UINib(nibName: "BatsmanFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BatsmanFullscoreTableViewCell")
        batsmanTblView.register(UINib(nibName: "TotalFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "TotalFullscoreTableViewCell")
        
        bowlerTblView.register(UINib(nibName: "BowlerFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BowlerFullscoreTableViewCell")
        
        lblFallOfWicketsDescription.text = liveScoreDetails.thirdTeamFallOfWickets
        
        let deviceWidth = self.frame.size.width - 93
        
        let textHeight = scoreDetails?.thirdTeamDoNotBats.height(withConstrainedWidth: deviceWidth, font: UIFont(name: "OpenSans", size: 15)!)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let fallofWicketTextHeight = scoreDetails?.thirdTeamFallOfWickets.height(withConstrainedWidth: screenWidth - 45, font: UIFont(name: "OpenSans", size: 15)!)
        
        fallOfWicketsContainerViewHeightConstraint.constant = fallofWicketTextHeight! + 50 + 20
        batsmanTableContainerViewHeightConstraint.constant = CGFloat(thirdTeamBatsmanArray.count * 50) + 30.0 + 100 + textHeight! + 30
        
        batsmanView.layoutIfNeeded()
        fallOfWicketView.layoutIfNeeded()
        
        batsmanTblView.reloadData()
        bowlerTblView.reloadData()
        lblTeamName.text = liveScoreDetails.thirdTeamFullName
        lblScoreTitle.text = liveScoreDetails.thirdTeamTotalTitleScore
        
    }
    
    func fourthTeamData(liveScoreDetails: LiveScoreDetails)  {
        showShadow()
        
        scoreDetails = liveScoreDetails
        selectedTeamType = VisiableTeam.FourthTeam.rawValue;
        fourthTeamBatsmanArray = liveScoreDetails.fourthTeamBatsmanArray
        fourthTeamBowlerArray = liveScoreDetails.fourthTeamBowlerArray
        
        batsmanTblView.register(UINib(nibName: "BatsmanFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BatsmanFullscoreTableViewCell")
        batsmanTblView.register(UINib(nibName: "TotalFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "TotalFullscoreTableViewCell")
        
        bowlerTblView.register(UINib(nibName: "BowlerFullscoreTableViewCell", bundle: nil), forCellReuseIdentifier: "BowlerFullscoreTableViewCell")
        
        lblFallOfWicketsDescription.text = liveScoreDetails.fourthTeamFallOfWickets
        
        let deviceWidth = self.frame.size.width - 93
        
        let textHeight = scoreDetails?.fourthTeamDoNotBats.height(withConstrainedWidth: deviceWidth, font: UIFont(name: "OpenSans", size: 15)!)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let fallofWicketTextHeight = scoreDetails?.fourthTeamFallOfWickets.height(withConstrainedWidth: screenWidth - 45, font: UIFont(name: "OpenSans", size: 15)!)
        
        fallOfWicketsContainerViewHeightConstraint.constant = fallofWicketTextHeight! + 50 + 20
        batsmanTableContainerViewHeightConstraint.constant = CGFloat(fourthTeamBatsmanArray.count * 50) + 30.0 + 100 + textHeight! + 30
        
        batsmanView.layoutIfNeeded()
        fallOfWicketView.layoutIfNeeded()
        
        batsmanTblView.reloadData()
        bowlerTblView.reloadData()
        lblTeamName.text = liveScoreDetails.fourthTeamFullName
        lblScoreTitle.text = liveScoreDetails.fourthTeamTotalTitleScore
    }
    
    func showShadow()  {
        
        innerView.layer.borderWidth = 0.5
        innerView.layer.borderColor = UIColor(red: 213.0/255, green: 213.0/255, blue: 213.0/255, alpha: 1.0).cgColor
        innerView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        innerView.layer.shadowOpacity = 0.25
        innerView.layer.shadowRadius = 1.0
        innerView.layer.masksToBounds = false
    }

    
    //MARK:- Table view Data and Delegats
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var arrayCount = 0
        
        if selectedTeamType == VisiableTeam.FirstTeam.rawValue {
            arrayCount = firstTeamBatsmanArray.count
        }
        else if selectedTeamType == VisiableTeam.SecondTeam.rawValue{
           arrayCount = secodsTeamBatsmanArray.count
        }
        else if selectedTeamType == VisiableTeam.ThirdTeam.rawValue{
            arrayCount = thirdTeamBatsmanArray.count
        }
        else if selectedTeamType == VisiableTeam.FourthTeam.rawValue{
            arrayCount = fourthTeamBatsmanArray.count
        }
        
        
        if tableView.tag == 100{
            if indexPath.row == arrayCount + 2{
                let deviceWidth = self.frame.size.width - 93
                if selectedTeamType == VisiableTeam.FirstTeam.rawValue{
                    let textHeight = scoreDetails?.firstTeamDoNotBats.height(withConstrainedWidth: deviceWidth, font: UIFont(name: "OpenSans", size: 15)!)
                    return textHeight! + 20
                }
                else if selectedTeamType == VisiableTeam.SecondTeam.rawValue{
                    let textHeight = scoreDetails?.secondTeamDoNotBats.height(withConstrainedWidth: deviceWidth, font: UIFont(name: "OpenSans", size: 15)!)
                    return textHeight! + 20
                }
                else if selectedTeamType == VisiableTeam.ThirdTeam.rawValue{
                    let textHeight = scoreDetails?.thirdTeamDoNotBats.height(withConstrainedWidth: deviceWidth, font: UIFont(name: "OpenSans", size: 15)!)
                    return textHeight! + 20
                }
                else if selectedTeamType == VisiableTeam.FourthTeam.rawValue{
                    let textHeight = scoreDetails?.fourthTeamDoNotBats.height(withConstrainedWidth: deviceWidth, font: UIFont(name: "OpenSans", size: 15)!)
                    return textHeight! + 20
                }
                return 0
            }
            else if indexPath.row >= arrayCount{
                return 40
            }
            
            
            return 50
        }
        else{
            return 34
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedTeamType == VisiableTeam.FirstTeam.rawValue {
            if tableView.tag == 100 {
                if firstTeamBatsmanArray.count == 0{
                    return 0
                }
                return firstTeamBatsmanArray.count + 3
            }
            else{
                return firstTeamBowlerArray.count
            }
        }
        else if selectedTeamType == VisiableTeam.SecondTeam.rawValue {
            if tableView.tag == 100 {

                if secodsTeamBatsmanArray.count == 0{
                    return 0
                }
                return secodsTeamBatsmanArray .count + 3
            }
            else{
                return secondTeamBowlerArray.count
            }
        }
        else if selectedTeamType == VisiableTeam.ThirdTeam.rawValue {
            if tableView.tag == 100 {
                
                if thirdTeamBatsmanArray.count == 0{
                    return 0
                }
                return thirdTeamBatsmanArray .count + 3
            }
            else{
                return thirdTeamBowlerArray.count
            }
        }
        else if selectedTeamType == VisiableTeam.FourthTeam.rawValue {
            if tableView.tag == 100 {
                
                if fourthTeamBatsmanArray.count == 0{
                    return 0
                }
                return fourthTeamBatsmanArray .count + 3
            }
            else{
                return fourthTeamBowlerArray.count
            }
        }

        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100 {
            
            var arrayCount = 0
            
            if selectedTeamType == VisiableTeam.FirstTeam.rawValue{
                arrayCount = firstTeamBatsmanArray.count
            }
            else if selectedTeamType == VisiableTeam.SecondTeam.rawValue{
                arrayCount = secodsTeamBatsmanArray.count
            }
            else if selectedTeamType == VisiableTeam.ThirdTeam.rawValue{
                arrayCount = thirdTeamBatsmanArray.count
            }
            else if selectedTeamType == VisiableTeam.FourthTeam.rawValue{
                arrayCount = fourthTeamBatsmanArray.count
            }
            
            if indexPath.row == arrayCount{
                var cell = tableView.dequeueReusableCell(withIdentifier: "TotalFullscoreTableViewCell") as? TotalFullscoreTableViewCell
                
                if cell == nil {
                    cell = TotalFullscoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TotalFullscoreTableViewCell")
                }
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.lblTitle.text = "Extra"

                if selectedTeamType == VisiableTeam.FirstTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.firstTeamExtraScore
                }
                else if selectedTeamType == VisiableTeam.SecondTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.secondTeamExtraScore
                }
                else if selectedTeamType == VisiableTeam.ThirdTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.thirdTeamExtraScore
                }
                else if selectedTeamType == VisiableTeam.FourthTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.fourthTeamExtraScore
                }

                return cell!
            }
            else if indexPath.row == arrayCount + 1{
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "TotalFullscoreTableViewCell") as? TotalFullscoreTableViewCell
                
                if cell == nil {
                    cell = TotalFullscoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TotalFullscoreTableViewCell")
                }
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.lblTitle.text = "Total"
                if selectedTeamType == VisiableTeam.FirstTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.firstTeamTotalScore
                }
                else if selectedTeamType == VisiableTeam.SecondTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.secondTeamTotalScore
                }
                else if selectedTeamType == VisiableTeam.ThirdTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.thirdTeamTotalScore
                }
                else if selectedTeamType == VisiableTeam.FourthTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.fourthTeamTotalScore
                }
                
                return cell!
            }
            else if indexPath.row == arrayCount + 2{
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "TotalFullscoreTableViewCell") as? TotalFullscoreTableViewCell
                
                if cell == nil {
                    cell = TotalFullscoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TotalFullscoreTableViewCell")
                }
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.lblTitle.text = "Did not bat"
                if selectedTeamType == VisiableTeam.FirstTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.firstTeamDoNotBats
                }
                else if selectedTeamType == VisiableTeam.SecondTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.secondTeamDoNotBats
                }
                else if selectedTeamType == VisiableTeam.ThirdTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.thirdTeamDoNotBats
                }
                else if selectedTeamType == VisiableTeam.FourthTeam.rawValue{
                    cell?.lblDescription.text = scoreDetails?.fourthTeamDoNotBats
                }
                return cell!
            }
            else{
                var cell = tableView.dequeueReusableCell(withIdentifier: "BatsmanFullscoreTableViewCell") as? BatsmanFullscoreTableViewCell
                
                if cell == nil {
                    cell = BatsmanFullscoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "BatsmanFullscoreTableViewCell")
                }
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                if selectedTeamType == VisiableTeam.FirstTeam.rawValue{
                    let firstTeamBatsmanDetails = firstTeamBatsmanArray[indexPath.row]
                    cell?.lblPlayerDescription.text = firstTeamBatsmanDetails.batsmanStatusMessage
                    cell?.lblBatsmanName.text = firstTeamBatsmanDetails.playerName
                    cell?.lblRunsCount.text = firstTeamBatsmanDetails.battingRun
                    cell?.lblBowlCount.text = firstTeamBatsmanDetails.battingBowl
                    cell?.lblFoursCount.text = firstTeamBatsmanDetails.battingFours
                    cell?.lblSixesCount.text = firstTeamBatsmanDetails.battingSixes
                    cell?.lblStrickRate.text = firstTeamBatsmanDetails.batsmanStrikeRate
                }
                else if selectedTeamType == VisiableTeam.SecondTeam.rawValue{
                    
                    let secondTeamBatsmanDetails = secodsTeamBatsmanArray[indexPath.row]
                    cell?.lblPlayerDescription.text = secondTeamBatsmanDetails.batsmanStatusMessage
                    cell?.lblBatsmanName.text = secondTeamBatsmanDetails.playerName
                    cell?.lblRunsCount.text = secondTeamBatsmanDetails.battingRun
                    cell?.lblBowlCount.text = secondTeamBatsmanDetails.battingBowl
                    cell?.lblFoursCount.text = secondTeamBatsmanDetails.battingFours
                    cell?.lblSixesCount.text = secondTeamBatsmanDetails.battingSixes
                    cell?.lblStrickRate.text = secondTeamBatsmanDetails.batsmanStrikeRate
                }
                else if selectedTeamType == VisiableTeam.ThirdTeam.rawValue{
                    
                    let thirdTeamBatsmanDetails = thirdTeamBatsmanArray[indexPath.row]
                    cell?.lblPlayerDescription.text = thirdTeamBatsmanDetails.batsmanStatusMessage
                    cell?.lblBatsmanName.text = thirdTeamBatsmanDetails.playerName
                    cell?.lblRunsCount.text = thirdTeamBatsmanDetails.battingRun
                    cell?.lblBowlCount.text = thirdTeamBatsmanDetails.battingBowl
                    cell?.lblFoursCount.text = thirdTeamBatsmanDetails.battingFours
                    cell?.lblSixesCount.text = thirdTeamBatsmanDetails.battingSixes
                    cell?.lblStrickRate.text = thirdTeamBatsmanDetails.batsmanStrikeRate
                }
                else if selectedTeamType == VisiableTeam.FourthTeam.rawValue{
                    
                    let fourthTeamBatsmanDetails = fourthTeamBatsmanArray[indexPath.row]
                    cell?.lblPlayerDescription.text = fourthTeamBatsmanDetails.batsmanStatusMessage
                    cell?.lblBatsmanName.text = fourthTeamBatsmanDetails.playerName
                    cell?.lblRunsCount.text = fourthTeamBatsmanDetails.battingRun
                    cell?.lblBowlCount.text = fourthTeamBatsmanDetails.battingBowl
                    cell?.lblFoursCount.text = fourthTeamBatsmanDetails.battingFours
                    cell?.lblSixesCount.text = fourthTeamBatsmanDetails.battingSixes
                    cell?.lblStrickRate.text = fourthTeamBatsmanDetails.batsmanStrikeRate
                }
                return cell!
            }
        }
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "BowlerFullscoreTableViewCell") as? BowlerFullscoreTableViewCell
            
            if cell == nil {
                cell = BowlerFullscoreTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "BowlerFullscoreTableViewCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            if selectedTeamType == VisiableTeam.FirstTeam.rawValue{
                let firstTeamBowlerDetails = firstTeamBowlerArray[indexPath.row]

                cell?.lblBowlerName.text = firstTeamBowlerDetails.playerName
                cell?.lblOverCount.text = firstTeamBowlerDetails.bowlerOverBowled
                cell?.lblMeddenCount.text = firstTeamBowlerDetails.bowlerMaindenOvers
                cell?.lblRunCount.text = firstTeamBowlerDetails.bowlerRunConceded
                cell?.lblWickets.text = firstTeamBowlerDetails.bowlerWicketTaken
                cell?.lblNoBowlesCount.text = firstTeamBowlerDetails.bowlerNoBowl
                cell?.lblWideBowl.text = firstTeamBowlerDetails.bowlerWideBalls
                cell?.lblEconomyRate.text = firstTeamBowlerDetails.bowlerEcoRate
            }
            else if selectedTeamType == VisiableTeam.SecondTeam.rawValue{
                
                let secondTeamBowlerDetails = secondTeamBowlerArray[indexPath.row]
                
                cell?.lblBowlerName.text = secondTeamBowlerDetails.playerName
                cell?.lblOverCount.text = secondTeamBowlerDetails.bowlerOverBowled
                cell?.lblMeddenCount.text = secondTeamBowlerDetails.bowlerMaindenOvers
                cell?.lblRunCount.text = secondTeamBowlerDetails.bowlerRunConceded
                cell?.lblWickets.text = secondTeamBowlerDetails.bowlerWicketTaken
                cell?.lblNoBowlesCount.text = secondTeamBowlerDetails.bowlerNoBowl
                cell?.lblWideBowl.text = secondTeamBowlerDetails.bowlerWideBalls
                cell?.lblEconomyRate.text = secondTeamBowlerDetails.bowlerEcoRate
                
            }
            else if selectedTeamType == VisiableTeam.ThirdTeam.rawValue{
                
                let thirdTeamBowlerDetails = thirdTeamBowlerArray[indexPath.row]
                
                cell?.lblBowlerName.text = thirdTeamBowlerDetails.playerName
                cell?.lblOverCount.text = thirdTeamBowlerDetails.bowlerOverBowled
                cell?.lblMeddenCount.text = thirdTeamBowlerDetails.bowlerMaindenOvers
                cell?.lblRunCount.text = thirdTeamBowlerDetails.bowlerRunConceded
                cell?.lblWickets.text = thirdTeamBowlerDetails.bowlerWicketTaken
                cell?.lblNoBowlesCount.text = thirdTeamBowlerDetails.bowlerNoBowl
                cell?.lblWideBowl.text = thirdTeamBowlerDetails.bowlerWideBalls
                cell?.lblEconomyRate.text = thirdTeamBowlerDetails.bowlerEcoRate
                
            }
            else if selectedTeamType == VisiableTeam.FourthTeam.rawValue{
                
                let fourthTeamBowlerDetails = fourthTeamBowlerArray[indexPath.row]
                
                cell?.lblBowlerName.text = fourthTeamBowlerDetails.playerName
                cell?.lblOverCount.text = fourthTeamBowlerDetails.bowlerOverBowled
                cell?.lblMeddenCount.text = fourthTeamBowlerDetails.bowlerMaindenOvers
                cell?.lblRunCount.text = fourthTeamBowlerDetails.bowlerRunConceded
                cell?.lblWickets.text = fourthTeamBowlerDetails.bowlerWicketTaken
                cell?.lblNoBowlesCount.text = fourthTeamBowlerDetails.bowlerNoBowl
                cell?.lblWideBowl.text = fourthTeamBowlerDetails.bowlerWideBalls
                cell?.lblEconomyRate.text = fourthTeamBowlerDetails.bowlerEcoRate
                
            }
            return cell!
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}



