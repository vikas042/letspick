//
//  AvailableWithdrawTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class AvailableWithdrawTableViewCell: UITableViewCell {

    @IBOutlet weak var lblEnterWithdrawAmt: UILabel!
    @IBOutlet weak var lblAvailableAmt: UILabel!
    @IBOutlet weak var lblAvailableBalance: UILabel!
    @IBOutlet weak var amountTxtField: UITextField!

    @IBOutlet weak var containerView: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
