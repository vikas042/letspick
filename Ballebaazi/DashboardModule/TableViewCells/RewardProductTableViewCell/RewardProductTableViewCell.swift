//
//  RewardProductTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class RewardProductTableViewCell: UITableViewCell {

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCoin: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imageContainerView: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(details: RewardProductDetails) {
        lblName.text = details.rewardNameEnglish
        lblCoin.text = details.coins
        
        DispatchQueue.main.async {
            self.imageContainerView.roundViewCorners(corners: [.topLeft, .bottomLeft], radius: 7)
        }
        
        AppHelper.showShodowOnAccountCellsView(innerView: innerView)
        
        if details.image.count < 10 {
            self.imgView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: details.image){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.imgView.image = UIImage(named: "Placeholder")
        }
    }
    
}
