//
//  PaymentOptionTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class PaymentOptionTableViewCell: UITableViewCell {
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var imgView: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
