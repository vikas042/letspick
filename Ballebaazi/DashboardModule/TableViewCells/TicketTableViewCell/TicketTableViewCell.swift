//
//  TicketTableViewCell.swift
//  Letspick
//
//  Created by MADSTECH on 17/02/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class TicketTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblGameType: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblMatchType: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblExiperyDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        }
}
