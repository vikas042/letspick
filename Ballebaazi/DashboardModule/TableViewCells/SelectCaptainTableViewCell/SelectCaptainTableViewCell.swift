//
//  SelectCaptainTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 05/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import MapleBacon

class SelectCaptainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var captainButton: UIButton!
    @IBOutlet weak var viceCaptainButton: UIButton!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var playerTypeImageView: UIImageView!
    
    @IBOutlet weak var lblViceCaptainSelection: UILabel!
    @IBOutlet weak var lblCaptainSelection: UILabel!

    func configData(details: PlayerDetails, legueType: Int, gameType: Int){
        
        captainButton.layer.borderWidth = 1
        viceCaptainButton.layer.borderWidth = 1
        captainButton.layer.borderColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1).cgColor
        viceCaptainButton.layer.borderColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1).cgColor
                
        lblTeam.text = details.teamShortName
        lblPlayerName.text = details.playerShortName
        lblCaptainSelection.text = details.captainRoleSelected + "%"
        lblViceCaptainSelection.text = details.viceCaptainRoleSelected + "%"

        if (legueType == FantasyType.Classic.rawValue) {
            lblPoints.text = details.classicPoints
        }
        else if legueType == FantasyType.Batting.rawValue{
            lblPoints.text = details.battingPoints
        }
        else if legueType == FantasyType.Bowling.rawValue{
            lblPoints.text = details.bowlingPoints
        }
        else if legueType == FantasyType.Reverse.rawValue{
            lblPoints.text = details.reversePoints
        }
        else if legueType == FantasyType.Wizard.rawValue{
            lblPoints.text = details.wizardPoints
        }

        showPlayerTypeImg(details: details, gameType: gameType)
        playerTypeImageView.image = UIImage(named: details.playerPlaceholder)

        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerTypeImageView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerTypeImageView.image = image
                    }
                    else{
                        self?.playerTypeImageView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }
        else{
            self.playerTypeImageView.image = UIImage(named: details.playerPlaceholder)
        }
    }
    
    func showPlayerTypeImg(details: PlayerDetails, gameType: Int) {
        
        if details.playerPlayingRole == PlayerType.Batsman.rawValue {
            playerTypeImageView.image = UIImage(named: "BatIcon")
//            return "BatIcon"
        }
        else if details.playerPlayingRole == PlayerType.Bowler.rawValue {
            playerTypeImageView.image = UIImage(named: "BowlIcon")
//            return "BowlIcon"
        }
        else if details.playerPlayingRole == PlayerType.AllRounder.rawValue {
            if gameType == GameType.Kabaddi.rawValue{
                playerTypeImageView.image = UIImage(named: "AllrounderKabaddiPlaceholder")
            }
            else{
                playerTypeImageView.image = UIImage(named: "AllRounderIcon")
            }
        }
        else if details.playerPlayingRole == PlayerType.WicketKeeper.rawValue {
            playerTypeImageView.image = UIImage(named: "WicketIcon")
        }
        else if details.playerPlayingRole == PlayerType.Defender.rawValue {
            if gameType == GameType.Football.rawValue{
                playerTypeImageView.image = UIImage(named: "FootballDefenderIcon")
            }
            else{
                playerTypeImageView.image = UIImage(named: "DefenderIconPlaceholder")
//                return "DefenderIconPlaceholder"
            }
        }
        else if details.playerPlayingRole == PlayerType.Raider.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
//            return "DefenderIconPlaceholder"
        }
        else if details.playerPlayingRole == PlayerType.GoalKeeper.rawValue {
            playerTypeImageView.image = UIImage(named: "FootballGoalKeeperIcon")
//            return "FootballGoalKeeperIcon"
        }
        else if details.playerPlayingRole == PlayerType.MidFielder.rawValue {
            playerTypeImageView.image = UIImage(named: "FootballMidFielderIcon")
//            return "FootballMidFielderIcon"
        }
        else if details.playerPlayingRole == PlayerType.Sticker.rawValue {
            playerTypeImageView.image = UIImage(named: "FootballForwardIcon")
//            return "FootballForwardIcon"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
