//
//  BottomMessageTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 01/03/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class BottomMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
