//
//  HowToPlayTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class HowToPlayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblSerial: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
}
