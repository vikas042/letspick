//
//  WithdrawTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 11/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class WithdrawTableViewCell: UITableViewCell {

    @IBOutlet weak var withdrawPolicy: UIButton!
    @IBOutlet weak var withdrawButton: SolidButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
