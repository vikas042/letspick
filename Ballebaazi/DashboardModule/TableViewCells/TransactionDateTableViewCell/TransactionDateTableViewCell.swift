//
//  TransactionDateTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class TransactionDateTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDescription: UILabel!    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configData(details: TransactionsDetails) {
                    
        lblDate.text = details.transactionTime
        if (details.transactionType == "2") || (details.transactionType == "15"){
            lblAmount.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
            lblAmount.text = "" + AppHelper.makeCommaSeparatedDigitsWithString(digites: details.transactionAmount)
        }
        else if (details.transactionType == "4"){
            lblAmount.textColor = UIColor(red: 6.0/255, green: 106.0/255, blue: 196.0/255, alpha: 1)
            lblAmount.text = "" + AppHelper.makeCommaSeparatedDigitsWithString(digites: details.transactionAmount)
        }
        else{
            lblAmount.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
            lblAmount.text = "" + AppHelper.makeCommaSeparatedDigitsWithString(digites: details.transactionAmount)
        }
        
        if details.isNegativeTranaction {
            lblAmount.text = "-" + (lblAmount.text ?? "")
            lblAmount.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        else{
            if details.transactionType == "4"{
                lblAmount.textColor = UIColor(red: 6.0/255, green: 106.0/255, blue: 196.0/255, alpha: 1)
                lblAmount.text = "-" + (lblAmount.text ?? "")
            }
            else{
                lblAmount.text = "+" + (lblAmount.text ?? "")
            }
        }
        
        if details.transactionType == "1" {
            lblDescription.text = "Contest Won".localized()
        }
        else if details.transactionType == "2" {
            if details.byAdmin == "1"{
                lblDescription.text = "Deducted by admin".localized()
            }
            else{
                lblDescription.text = "League Joined".localized()
            }
        }
        else if details.transactionType == "3" {
            lblDescription.text = "Refund Received".localized()
        }
        else if details.transactionType == "4" {
            lblDescription.text = "Withdrawal Requested".localized()
        }
        else if details.transactionType == "5" {
            lblDescription.text = "Promotion recieved".localized()
        }
        else if details.transactionType == "6" {
            lblDescription.text = "Cash Deposited".localized()
        }
        else if details.transactionType == "7" {
            lblDescription.text = "Withdrawal Reversed".localized()
        }
        else if details.transactionType == "8" {
            lblDescription.text = "You have been referred".localized()
        }
        else if details.transactionType == "9" {
            lblDescription.text = "Referral bonus recived".localized()
        }
        else if details.transactionType == "10" {
            lblDescription.text = "Signup Bonus".localized()
        }
        else if details.transactionType == "11" {
            lblDescription.text = "Earned for referring".localized()
        }
        else if details.transactionType == "12" {
            lblDescription.text = "Cash Redeemed Successfully".localized()
        }else if details.transactionType == "13" {
            lblDescription.text = "Bonus Redeemed Successfully".localized()
        }
        else if details.transactionType == "14" {
            lblDescription.text = "Congrats! Winning Received".localized()
        }
        else if details.transactionType == "15" {
            lblDescription.text = "Joined with ticket".localized()
        }
        else if details.transactionType == "16" {
            lblDescription.text = "Withdrawal Cancelled".localized()
        }
        else if details.transactionType == "17" {
            lblDescription.text = "Unused Cash Credited Successfully".localized()
        }
        else if details.transactionType == "24" {
            lblDescription.text = "Received a season pass".localized()
        }
        else if details.transactionType == "25" {
            lblDescription.text = "Joined with pass".localized()
        }
        else if details.transactionType == "26" {
            lblDescription.text = "Joined a quiz".localized()
        }
        else if details.transactionType == "27" {
            lblDescription.text = "Quiz Refund Received".localized()
        }
        else if details.transactionType == "28" {
            lblDescription.text = "Won Quiz".localized()
        }
        else{
            lblDescription.text = details.transactionMessage
        }
    }
    
    func configRewardData(details: TransactionsDetails) {
                    
        lblDate.text = details.transactionTime
    
        if details.isNegativeTranaction {
            lblAmount.text = "-" + details.bbCoins
            lblAmount.textColor = UIColor(red: 255.0/255, green: 71.0/255, blue: 49.0/255, alpha: 1)
        }
        else{
            lblAmount.text = "+" + details.bbCoins
            lblAmount.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
        }
        
        if details.transactionType == "1" {
            lblDescription.text = "LP Coins Earned".localized()
        }
        else if details.transactionType == "2" {
            lblDescription.text = "LP Coins Redeemed".localized()
        }
        else{
            lblDescription.text = details.transactionMessage
        }
    }
}
