//
//  QuizMatchTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 02/04/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class QuizMatchTableViewCell: UITableViewCell {

    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var lblQuizTimer: UILabel!
    @IBOutlet weak var getInfoButton: UIButton!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var playNowButton: CustomBorderButton!
    @IBOutlet weak var lblQuizName: UILabel!
    @IBOutlet weak var lblActivePlayers: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(details: MatchDetails) {
        getInfoButton.setTitle("Get Info".localized(), for: .normal)
        playNowButton.setTitle("Play Now".localized(), for: .normal)

        lblQuizName.text = details.matchName.uppercased()
        let activePlayers = Int(details.activePlayers) ?? 0
        
        overlayView.isHidden = true
        overlayView.backgroundColor = UIColor(red: 215.0/255, green: 215.0/255, blue: 215.0/255, alpha: 0.4)
        overlayView.layer.cornerRadius = 7.0
        
        let startDateTimestemp = Double(details.startDateTimestemp ?? "0") ?? 0

        let startDateIndia = Double(details.startDateIndia) ?? 0
        let quizCloseInerval = Double(details.quizCloseInerval) ?? 0
        let serverTimeStemp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
        lblActivePlayers.isHidden = false
        lblActivePlayers.backgroundColor = UIColor(red: 206.0/255, green: 255.0/255, blue: 228.0/255, alpha: 0.85)
        lblQuizTimer.text = ""
        
        if activePlayers > 1 {
            lblActivePlayers.text = "  \("Active Players".localized()) \(details.activePlayers)  "
        }
        else{
            lblActivePlayers.text = "  \("Active Player".localized()) \(details.activePlayers)  "
        }
        lblActivePlayers.textColor = UIColor(red: 0.0/255, green: 160.0/255, blue: 70.0/255, alpha: 0.85)
        lblActivePlayers.layer.cornerRadius = 0
     
        if startDateIndia > 0 {
            
            if startDateTimestemp >= serverTimeStemp {
                lblActivePlayers.text = "  \(details.matchViewName)  "
                lblActivePlayers.isHidden = false
                lblQuizTimer.isHidden = true
                overlayView.isHidden = false
                lblActivePlayers.backgroundColor = UIColor(red: 245.0/255, green: 212.0/255, blue: 182.0/255, alpha: 1)
                lblActivePlayers.textColor = UIColor(red: 206.0/255, green: 115.0/255, blue: 47.0/255, alpha: 1)
                lblActivePlayers.layer.cornerRadius = 3
            }
            else{
                
                lblQuizTimer.text  = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: String(startDateTimestemp + details.closingTs * 60.0))
                lblQuizTimer.isHidden = false
                lblActivePlayers.isHidden = false
                overlayView.isHidden = true
            }
        }

        AppHelper.showShodowOnCellsView(innerView: innerView)
        if details.imageName.count < 5 {
            self.imgView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: details.imageName){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                 
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.imgView.image = UIImage(named: "Placeholder")
        }
    }
    
    @IBAction func getInfoButtonTapped(_ sender: Any) {
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let privacyPolicyVC = loginStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyWebViewController") as? PrivacyPolicyWebViewController
        privacyPolicyVC?.selectedType = SelectedWebViewType.Quiz.rawValue
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.pushViewController(privacyPolicyVC!, animated: true)
        }
    }
    
}
