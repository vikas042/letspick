//
//  CricketBannerTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class CricketBannerTableViewCell: UITableViewCell  {
    lazy var bannerArray = Array<BannerDetails>()

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    func configData(bannerList: Array<BannerDetails>) {
        bannerArray = bannerList
        collectionView.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell")
        collectionView.reloadData()
    }
    
}


extension CricketBannerTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
        
        let details = bannerArray[indexPath.item]
        cell.configBannerData(details: details)

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if bannerArray.count == 1 {
            return CGSize(width: UIScreen.main.bounds.width - 15, height: 90)
        }
        return CGSize(width: 290, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        

        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            let details = bannerArray[indexPath.item]
            
            var sportType = ""

            if details.gameType == "1" {
                sportType = "Cricket"
            }
            else if details.gameType == "2" {
                sportType = "Kabaddi"
            }
            else if details.gameType == "3" {
                sportType = "Football"
            }
            else if details.gameType == "4" {
                sportType = "Quiz"
            }
            else if details.gameType == "5" {
                sportType = "Basketball"
            }
            else if details.gameType == "6" {
                sportType = "Baseball"
            }

            AppxorEventHandler.logAppEvent(withName: "BannerClicked", info: ["BannerRedirectionType": details.redirectType, "SportType": sportType, "BannerName": details.title])
        
            if details.redirectType == "1" {
                if details.gameType == "1" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "2" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "3" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "4" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "QuizLeaguesViewController") as! QuizLeaguesViewController
                    let matchDetails = MatchDetails()
                    matchDetails.matchKey = details.matchKey
                    matchDetails.isQuizMatch = true
                    leagueVC.matchDetails = matchDetails
                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                        navVC.pushViewController(leagueVC, animated: true)
                    }
                }
                else if details.gameType == "5" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as! BasketballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "6" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BaseballLeagueViewController") as! BaseballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else if details.redirectType == "2" {
                let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
                navVC.pushViewController(leagueVC, animated: true)
            }
            else if details.redirectType == "3" {
                
                if details.websiteUrl.count == 0{
                    return;
                }
                let webviewVC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webviewVC.urlString = details.websiteUrl
                navVC.pushViewController(webviewVC, animated: true)
            }
            else if details.redirectType == "4" {
                let playerView = YoutubeVideoPlayerView(frame: APPDELEGATE.window!.frame)
                playerView.videoID = details.videoUrl
                playerView.playView()
                APPDELEGATE.window!.addSubview(playerView)
            }
            else if details.redirectType == "5" {
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                    if let dashboardVC = navVC.viewControllers.first as? DashboardViewController{
                        if details.redirectSportType == "1"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Cricket.rawValue)
                        }
                        else if details.redirectSportType == "2"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Kabaddi.rawValue)
                        }
                        else if details.redirectSportType == "3"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Football.rawValue)
                        }
                        else if details.redirectSportType == "4"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Quiz.rawValue)
                        }
                        else if details.redirectSportType == "5"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Basketball.rawValue)
                        }
                        else if details.redirectSportType == "6"{
                            dashboardVC.updateHeaderOnscroll(gameType: GameType.Baseball.rawValue)
                        }
                    }
                }
            }
            else if details.redirectType == "6" {
                let promoCodeVC = storyboard.instantiateViewController(withIdentifier: "PromotionsViewController") as! PromotionsViewController
                promoCodeVC.isFromSetting = true
                navVC.pushViewController(promoCodeVC, animated: true)
            }
            else if details.redirectType == "7" {
                let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
                navVC.pushViewController(addCashVC, animated: true)
            }
            else if details.redirectType == "8" {
                
                let howToPlayVC = storyboard.instantiateViewController(withIdentifier: "HowToPlayViewController") as! HowToPlayViewController
                navVC.pushViewController(howToPlayVC, animated: true)
            }
            else if details.redirectType == "9" {

                let becomePartnerVC = storyboard.instantiateViewController(withIdentifier: "BecomePartnerViewController") as! BecomePartnerViewController
                navVC.pushViewController(becomePartnerVC, animated: true)
            }
            else if details.redirectType == "10" {
                // Batting fantasy
                if details.gameType == "1" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.matchKey = details.matchKey
                    leagueVC.selectedFantasy = FantasyType.Batting.rawValue
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "2" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "3" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "5" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as! BasketballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "6" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BaseballLeagueViewController") as! BaseballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else if details.redirectType == "11" {
                // Bowling fantasy
                if details.gameType == "1" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                    leagueVC.matchKey = details.matchKey
                    leagueVC.selectedFantasy = FantasyType.Bowling.rawValue
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "2" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "3" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "5" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as! BasketballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
                else if details.gameType == "6" {
                    let leagueVC = storyboard.instantiateViewController(withIdentifier: "BaseballLeagueViewController") as! BaseballLeagueViewController
                    leagueVC.matchKey = details.matchKey
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else if details.redirectType == "12" {
                let rewardVC = storyboard.instantiateViewController(withIdentifier: "RewardStoreViewController") as! RewardStoreViewController
                rewardVC.isFromTabBar = false
                navVC.pushViewController(rewardVC, animated: true)
            }
            else if details.redirectType == "13" {
                let sessionVC = storyboard.instantiateViewController(withIdentifier: "SessionPassesViewController")
                navVC.pushViewController(sessionVC, animated: true)
            }

        }
    }
}
