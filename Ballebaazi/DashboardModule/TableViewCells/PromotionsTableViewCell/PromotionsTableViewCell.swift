//
//  PromotionsTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/12/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class PromotionsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblCodeTitle: UILabel!
    
    @IBOutlet weak var applyButton: CustomBorderButton!
    @IBOutlet weak var subTitleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var innerView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
   
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var lblExpiryDate: UILabel!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblPromotionCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(details: PromotionCodeDetails) {
        lblCodeTitle.text = "Code".localized()
//        topView.roundViewCorners(corners: .topRight, radius: 7.0)
//        topView.roundViewCorners(corners: .topRight, radius: 7.0)
        applyButton.setTitle("Apply".localized(), for: .normal)
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblTitle.text = details.title
        lblSubTitle.text = details.subTitle
        lblExpiryDate.text = "Expiry Date: ".localized() + details.endDate
        lblPromotionCode.text = details.promocode        
    }
}
