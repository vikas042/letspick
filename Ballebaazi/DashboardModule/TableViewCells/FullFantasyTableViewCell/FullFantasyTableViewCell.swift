//
//  FullFantasyTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 27/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class FullFantasyTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var playerTypeImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configData(details: PlayerDetails, legueType: Int, gameType: Int){
        lblTeam.text = details.teamShortName
        lblPlayerName.text = details.playerName
        
        if details.playerPlayingRole == PlayerType.Batsman.rawValue {
            lblTeam.text = "BAT".localized()
        }
        else if details.playerPlayingRole == PlayerType.Bowler.rawValue {
            lblTeam.text = "BOW".localized()
        }
        else if details.playerPlayingRole == PlayerType.AllRounder.rawValue {
            lblTeam.text = "ALR".localized()
        }
        else if details.playerPlayingRole == PlayerType.WicketKeeper.rawValue {
            lblTeam.text = "WK".localized()
        }
        else if details.playerPlayingRole == PlayerType.Defender.rawValue {
            lblTeam.text = "DEF".localized()
        }
        else if details.playerPlayingRole == PlayerType.Raider.rawValue {
            lblTeam.text = "RAI".localized()
        }
        else if details.playerPlayingRole == PlayerType.GoalKeeper.rawValue {
            lblTeam.text = "GK".localized()
        }
        else if details.playerPlayingRole == PlayerType.MidFielder.rawValue {
            lblTeam.text = "MID".localized()
        }
        else if details.playerPlayingRole == PlayerType.Sticker.rawValue {
            lblTeam.text = "ST".localized()
        }
        else if details.playerPlayingRole == PlayerType.ShootingGuard.rawValue {
            lblTeam.text = "SG".localized()
        }
        else if details.playerPlayingRole == PlayerType.PointGuard.rawValue {
            lblTeam.text = "PG".localized()
        }
        else if details.playerPlayingRole == PlayerType.SmallForward.rawValue {
            lblTeam.text = "SF".localized()
        }
        else if details.playerPlayingRole == PlayerType.Center.rawValue {
            lblTeam.text = "Cen".localized()
        }
        else if details.playerPlayingRole == PlayerType.PowerForward.rawValue {
            lblTeam.text = "PF".localized()
        }
        else if details.playerPlayingRole == PlayerType.Outfielders.rawValue {
            lblTeam.text = "OF".localized()
        }
        else if details.playerPlayingRole == PlayerType.Catcher.rawValue {
            lblTeam.text = "CAT".localized()
        }
        else if details.playerPlayingRole == PlayerType.Infielder.rawValue {
            lblTeam.text = "IF".localized()
        }
        else if details.playerPlayingRole == PlayerType.Pitcher.rawValue {
            lblTeam.text = "PIT".localized()
        }
        
        if details.imgURL.count > 0 {
          if let url = URL(string: details.imgURL){
              playerTypeImageView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                  // Report progress
              }, completion: { [weak self] image in
                  if (image != nil){
                      self?.playerTypeImageView.image = image
                  }
                  else{
                      self?.playerTypeImageView.image = UIImage(named: details.playerPlaceholder)
                  }
              })
          }
      }
      else{
          self.playerTypeImageView.image = UIImage(named: details.playerPlaceholder)
      }

        
        if legueType == FantasyType.Classic.rawValue{
            if details.isCaption{
                lblPoints.text = details.totalClassicCaptainScore
            }
            else if details.isViceCaption{
                lblPoints.text = details.totalClassicViceCaptainScore
            }
            else {
                lblPoints.text = details.totalClasscPlayerScore
            }
        }
        else if legueType == FantasyType.Batting.rawValue{
            if details.isCaption{
                lblPoints.text = details.totalBattingCaptainScore
            }
            else if details.isViceCaption{
                lblPoints.text = details.totalBattingViceCaptainScore
            }
            else {
                lblPoints.text = details.totalBattingPlayerScore
            }
        }
        else if legueType == FantasyType.Bowling.rawValue{
            
            if details.isCaption{
                lblPoints.text = details.totalBowlingCaptainScore
            }
            else if details.isViceCaption{
                lblPoints.text = details.totalBowlingViceCaptainScore
            }
            else {
                lblPoints.text = details.totalBowlingPlayerScore
            }
        }
        else if legueType == FantasyType.Reverse.rawValue{
            if details.isCaption{
                lblPoints.text = details.captainScoreReverse
            }
            else if details.isViceCaption{
                lblPoints.text = details.viceCaptainScoreReverse
            }
            else {
                lblPoints.text = details.totalClasscPlayerScore
            }
        }
        else if legueType == FantasyType.Wizard.rawValue{
            if details.isCaption{
                lblPoints.text = details.captainScoreWizard
            }
            else if details.isViceCaption{
                lblPoints.text = details.viceCaptainScoreWizard
            }
            else {
                lblPoints.text = details.totalClasscPlayerScore
            }
        }
    }
}
