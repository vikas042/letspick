//
//  TeamTableViewCell.swift
//  Letspick
//
//  Created by Mads Technologies on 28/06/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire

class TeamTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPreView: UILabel!
    @IBOutlet weak var lblCaptainTitle: UILabel!
    @IBOutlet weak var lblViceCaptainTitle: UILabel!
    @IBOutlet weak var lblWK: UILabel!
    @IBOutlet weak var lblBAT: UILabel!
    @IBOutlet weak var lblALR: UILabel!
    @IBOutlet weak var lblBOW: UILabel!
    @IBOutlet weak var lblEdit: UILabel!
    @IBOutlet weak var lblPreview: UILabel!
    @IBOutlet weak var lblClone: UILabel!
    @IBOutlet weak var lblTotalScoreTitle: UILabel!
    @IBOutlet weak var actionButtonView: UIView!
    @IBOutlet weak var teamView: UIView!
    @IBOutlet weak var scoreButtonView: UIView!
    @IBOutlet var lblTeamName: UILabel!
    @IBOutlet var lblCaptionName: UILabel!
    @IBOutlet var lblViceCaptionName: UILabel!
    @IBOutlet var innerView: UIView!
    @IBOutlet var lblWickerKeeperCount: UILabel!
    @IBOutlet var lblBatsmanCount: UILabel!
    @IBOutlet var lblBowlerCount: UILabel!
    @IBOutlet var lblAllRounderCount: UILabel!
    @IBOutlet weak var lblTotalScore: UILabel!
    var selectedFantasy = FantasyType.Classic.rawValue

    var selectedUserTeam: UserTeamDetails?
    var matchDetails: MatchDetails?
    var totalTeamCount = 0
    
    
    @IBOutlet weak var wizardViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblWizardPlayerName: UILabel!
    @IBOutlet weak var lblWizardIcon: UILabel!
    @IBOutlet weak var wizardView: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configDetails(details: UserTeamDetails, selectedMatchDetails: MatchDetails?, selectedType: Int, teamCount: Int) {
        
        totalTeamCount = teamCount;
        lblALR.text = "ALR".localized()
        lblBOW.text = "BOW".localized()
        lblBAT.text = "BAT".localized()
        lblWK.text = "WK".localized()

        lblEdit.text = "EDIT".localized()
        lblPreview.text = "PREVIEW".localized()
        lblPreView.text = "PREVIEW".localized()

        lblClone.text = "CLONE".localized()
        lblTotalScoreTitle.text = "Total Score:".localized()

        lblCaptainTitle.text = "Captain".localized()
        lblViceCaptainTitle.text = "subCaptain".localized()
        
        if selectedMatchDetails!.isMatchClosed{
            actionButtonView.isHidden = true
            scoreButtonView.isHidden = false
            lblTotalScore.text = AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: details.totalTeamScore!)
        }
        else{
            scoreButtonView.isHidden = true
            actionButtonView.isHidden = false
        }
        
        showShadowOnActionView()
        matchDetails = selectedMatchDetails
        selectedUserTeam = details
        
        lblTeamName.text = "Team".localized().uppercased() + " " + details.teamNumber!
        lblCaptionName.text = details.captionName
        lblViceCaptionName.text = details.viceCaptionName
        lblWickerKeeperCount.text = details.totalKeeperCount
        lblBowlerCount.text = details.totalBowlerCount
        lblAllRounderCount.text = details.totalAllrounderCount
        lblBatsmanCount.text = details.totalBatsmanCount
        selectedFantasy = selectedType
        wizardView.isHidden = true
        lblWizardIcon.isHidden = true
        lblWizardPlayerName.isHidden = true
        wizardViewHeightConstraint.constant = 0
        if selectedType == FantasyType.Wizard.rawValue {
            wizardView.isHidden = false
            lblWizardIcon.isHidden = false
            lblWizardPlayerName.isHidden = false

            wizardViewHeightConstraint.constant = 34
            lblWizardIcon.layer.cornerRadius = 11.0
            lblWizardIcon.layer.borderWidth = 0
//            lblWizardIcon.layer.borderColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1).cgColor

            if (details.wizardType == "1"){
                lblWizardPlayerName.text = "\(details.wizardPlayerName.uppercased())  (3x)"
            }
            else if (details.wizardType == "2"){
                lblWizardPlayerName.text = "\(details.wizardPlayerName.uppercased())  (5x)".uppercased()
            }
            else if (details.wizardType == "3"){
                lblWizardPlayerName.text = "\(details.wizardPlayerName.uppercased())  (4.5x)"
            }
        }
    }
    
    @IBAction func editButtonTapped(_ sender: Any) {
       
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let selectPlayerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
            selectPlayerVC.matchDetails = matchDetails
            selectPlayerVC.userTeamDetails = selectedUserTeam
            
            let leagueDetails = LeagueDetails()
            
            if selectedFantasy == FantasyType.Classic.rawValue{
                leagueDetails.fantasyType = "1"
             }
             else if selectedFantasy == FantasyType.Batting.rawValue{
                 leagueDetails.fantasyType = "2"
             }
             else if selectedFantasy == FantasyType.Bowling.rawValue{
                leagueDetails.fantasyType = "3"
             }
            else if selectedFantasy == FantasyType.Reverse.rawValue{
                leagueDetails.fantasyType = "4"
            }
            else if selectedFantasy == FantasyType.Wizard.rawValue{
               leagueDetails.fantasyType = "5"
            }

            selectPlayerVC.leagueDetails = leagueDetails
            selectPlayerVC.isEditPlayerTeam = true
            selectPlayerVC.isComeFromEditPlayerTeamScreen = true

            for details in selectedUserTeam!.playersArray!{
                details.isSelected = true
            }
            UserDetails.sharedInstance.selectedPlayerList = selectedUserTeam!.playersArray!
            navVC.pushViewController(selectPlayerVC, animated: true)
        }
    }
    
    @IBAction func previewButtonTapped(_ sender: Any?) {
        
        if matchDetails!.isMatchClosed {
            callGetFullScoreTeamPlayersDetails(userID: UserDetails.sharedInstance.userID, teamNumber: selectedUserTeam!.teamNumber ?? "", userName: UserDetails.sharedInstance.userName)
        }
        else{
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let teamPlayerList = storyboard.instantiateViewController(withIdentifier: "TeamPreviewViewController") as! TeamPreviewViewController
                teamPlayerList.totalPlayerArray = selectedUserTeam!.playersArray!
                if matchDetails!.isMatchClosed {
                    teamPlayerList.isMatchClosed = true
                }
                teamPlayerList.matchDetails = matchDetails
                AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Cricket"])

                teamPlayerList.selectedFantasy = selectedFantasy
                teamPlayerList.teamNumber = selectedUserTeam!.teamNumber ?? ""
                teamPlayerList.firstTeamName = matchDetails!.firstTeamShortName ?? ""
                teamPlayerList.secondTeamName = matchDetails!.secondTeamShortName ?? ""
                teamPlayerList.firstTeamkey = matchDetails!.firstTeamKey
                teamPlayerList.secondTeamkey = matchDetails!.secondTeamKey

                teamPlayerList.isHideEditButton = true
                navVC.pushViewController(teamPlayerList, animated: true)
            }
        }
    }
    
    
    func callGetFullScoreTeamPlayersDetails(userID: String, teamNumber: String, userName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let type = "user_team"
        
        var fantasyType = "1"
        
        if selectedFantasy == FantasyType.Classic.rawValue{
            fantasyType = "1"
        }
        else if selectedFantasy == FantasyType.Batting.rawValue{
            fantasyType = "2"
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue{
            fantasyType = "3"
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue{
            fantasyType = "4"
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue{
            fantasyType = "5"
        }


        let parameters: Parameters = ["option": "full_scoreboard", "match_key": matchDetails!.matchKey, "team_number": teamNumber,"user_id": userID, "type": type, "fantasy_type": fantasyType]
        
        WebServiceHandler.performPOSTRequest(urlString: kSocrescardUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let playerArray = result!["response"]?.array{
                        let playerListArray = PlayerDetails.getPlayerDetailsForScoreArray(responseArray: playerArray)
                        if self.matchDetails!.playersGender == "F" {
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: playerListArray)
                        }

                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Cricket"])
                        let teamPlayerList = storyboard.instantiateViewController(withIdentifier: "TeamPreviewViewController") as! TeamPreviewViewController
                        teamPlayerList.matchDetails = self.matchDetails
                        teamPlayerList.isMatchClosed = true
                        teamPlayerList.totalPlayerArray = playerListArray
                        teamPlayerList.firstTeamName = self.matchDetails!.firstTeamShortName ?? ""
                        teamPlayerList.teamNumber = teamNumber
                        teamPlayerList.userName = userName
                        teamPlayerList.secondTeamName = self.matchDetails!.secondTeamShortName ?? ""
                        teamPlayerList.selectedFantasy = self.selectedFantasy
                        teamPlayerList.firstTeamkey = self.matchDetails!.firstTeamKey 
                        teamPlayerList.secondTeamkey = self.matchDetails!.secondTeamKey 

                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.pushViewController(teamPlayerList, animated: true)

                        }
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    @IBAction func cloneButtonTapped(_ sender: Any) {
        if matchDetails!.isMatchClosed{
            previewButtonTapped(nil)
            return;
        }
        else if selectedFantasy == FantasyType.Classic.rawValue{
            if totalTeamCount >= UserDetails.sharedInstance.maxTeamAllowedForClassic{
                AppHelper.showAlertView(message: "You have already created maximum number of teams.", isErrorMessage: true)
                return;
            }
        }
        else if selectedFantasy == FantasyType.Batting.rawValue{
            
            if totalTeamCount >= UserDetails.sharedInstance.maxTeamAllowedForBatting{
                AppHelper.showAlertView(message: "You have already created maximum number of teams.", isErrorMessage: true)
                return;
            }
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue{
            
            if totalTeamCount >= UserDetails.sharedInstance.maxTeamAllowedForBowling{
                AppHelper.showAlertView(message: "You have already created maximum number of teams.", isErrorMessage: true)
                return;
            }
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue{
            
            if totalTeamCount >= UserDetails.sharedInstance.maxTeamAllowedForReverse{
                AppHelper.showAlertView(message: "You have already created maximum number of teams.", isErrorMessage: true)
                return;
            }
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue{
            
            if totalTeamCount >= UserDetails.sharedInstance.maxTeamAllowedForWizard{
                AppHelper.showAlertView(message: "You have already created maximum number of teams.", isErrorMessage: true)
                return;
            }
        }

        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let selectPlayerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
            selectPlayerVC.matchDetails = matchDetails
            
            let leagueDetails = LeagueDetails()
            
            if selectedFantasy == FantasyType.Classic.rawValue{
                leagueDetails.fantasyType = "1"
            }
            else if selectedFantasy == FantasyType.Batting.rawValue{
                leagueDetails.fantasyType = "2"
            }
            else if selectedFantasy == FantasyType.Bowling.rawValue{
                leagueDetails.fantasyType = "3"
            }
            else if selectedFantasy == FantasyType.Reverse.rawValue{
                leagueDetails.fantasyType = "4"
            }
            else if selectedFantasy == FantasyType.Wizard.rawValue{
                leagueDetails.fantasyType = "5"
            }

            selectPlayerVC.leagueDetails = leagueDetails
            selectPlayerVC.userTeamDetails = selectedUserTeam
            selectPlayerVC.isComeFromEditPlayerTeamScreen = true

            for details in selectedUserTeam!.playersArray!{
                details.isSelected = true
            }
            UserDetails.sharedInstance.selectedPlayerList = selectedUserTeam!.playersArray!
            navVC.pushViewController(selectPlayerVC, animated: true)
        }
    }
    
    
    @IBAction func ScoreButtonTapped(_ sender: Any) {
        return;
//        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            
//            let teamPlayerList = storyboard.instantiateViewController(withIdentifier: "TeamPlayersListViewController") as! TeamPlayersListViewController
//            teamPlayerList.matchDetails = matchDetails
//            teamPlayerList.leagueDetails = leagueDetails
//            teamPlayerList.isViewForScore = true
//            teamPlayerList.teamNumber = selectedUserTeam?.teamNumber ?? ""
//            for details in selectedUserTeam!.playersArray!{
//                details.isSelected = true
//                if details.isCaption{
//                    teamPlayerList.captionID = details.playerKey!
//                }
//                
//                if details.isViceCaption{
//                    teamPlayerList.viceCaptionID = details.playerKey!
//                }
//            }
//            UserDetails.sharedInstance.selectedPlayerList = selectedUserTeam!.playersArray!
//            navVC.pushViewController(teamPlayerList, animated: true)
//        }
    }
    
    func showShadowOnActionView() {
        AppHelper.showShodowOnCellsView(innerView: innerView)

        // Edit/Privew/Clone View Layout
        actionButtonView.layer.cornerRadius = 20.0
        actionButtonView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        actionButtonView.layer.shadowOffset = CGSize(width: 0, height: 0)
        actionButtonView.layer.shadowOpacity = 0.35
        actionButtonView.layer.shadowRadius = 6.5
        actionButtonView.layer.masksToBounds = false
        
        // Score View Layout
        scoreButtonView.layer.cornerRadius = 20.0
        scoreButtonView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        scoreButtonView.layer.shadowOffset = CGSize(width: 0, height: 0)
        scoreButtonView.layer.shadowOpacity = 0.35
        scoreButtonView.layer.shadowRadius = 6.5
        scoreButtonView.layer.masksToBounds = false
    }
}
