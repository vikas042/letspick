//
//  SettingsTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 13/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblReferAndEarnTitle: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var rightArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
