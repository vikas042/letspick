//
//  MoreTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 23/12/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {
    @IBOutlet weak var innerView: UIView!
    
    @IBOutlet weak var lblCoin: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
