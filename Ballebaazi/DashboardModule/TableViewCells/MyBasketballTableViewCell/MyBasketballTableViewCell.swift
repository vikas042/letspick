//
//  MyFootballTableViewCell.swift
//  Letspick
//
//  Created by Madstech on 29/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire


class MyBasketballTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPreView: UILabel!
    @IBOutlet weak var lblCaptainTitle: UILabel!
    @IBOutlet weak var lblViceCaptainTitle: UILabel!
    
    @IBOutlet weak var lblPFTitle: UILabel!
    @IBOutlet weak var lblSGTitle: UILabel!
    @IBOutlet weak var lblSFTitle: UILabel!
    @IBOutlet weak var lblPGTitle: UILabel!
    @IBOutlet weak var lblCENTitle: UILabel!

    @IBOutlet weak var lblEdit: UILabel!
    @IBOutlet weak var lblPreview: UILabel!
    @IBOutlet weak var lblClone: UILabel!
    @IBOutlet weak var lblTotalScoreTitle: UILabel!    
    @IBOutlet weak var actionButtonView: UIView!
    @IBOutlet weak var teamView: UIView!
    @IBOutlet weak var scoreButtonView: UIView!
    @IBOutlet var lblTeamName: UILabel!
    @IBOutlet var lblCaptionName: UILabel!
    @IBOutlet var lblViceCaptionName: UILabel!
    @IBOutlet var innerView: UIView!
    
    @IBOutlet var lblPGCount: UILabel!
    @IBOutlet var lblSGCount: UILabel!
    @IBOutlet var lblSFCount: UILabel!
    @IBOutlet var lblPFCount: UILabel!
    @IBOutlet var lblCENCount: UILabel!

    @IBOutlet weak var lblTotalScore: UILabel!
    
    var selectedUserTeam: UserTeamDetails?
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    
    var totalTeamCount = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configDetails(details: UserTeamDetails, selectedMatchDetails: MatchDetails?, selectedLeagueDetails: LeagueDetails?, teamCount: Int) {
        totalTeamCount = teamCount;

        lblPFTitle.text = "PF".localized()
        lblSGTitle.text = "SG".localized()
        lblSFTitle.text = "SF".localized()
        lblPGTitle.text = "PG".localized()
        lblCENTitle.text = "Cen".localized()

        lblEdit.text = "EDIT".localized()
        lblPreview.text = "PREVIEW".localized()
        lblClone.text = "CLONE".localized()
        lblTotalScoreTitle.text = "Total Score:".localized()
        lblPreView.text = "PREVIEW".localized()

        lblCaptainTitle.text = "Captain".localized()
        lblViceCaptainTitle.text = "subCaptain".localized()
        
        
        if selectedMatchDetails!.isMatchClosed{
            actionButtonView.isHidden = true
            scoreButtonView.isHidden = false
            lblTotalScore.text = AppHelper.makeCommaSeparatedDigitsForDecimalWithString(digites: details.totalTeamScore!)
        }
        else{
            scoreButtonView.isHidden = true
            actionButtonView.isHidden = false
        }
        
        showShadowOnActionView()
        matchDetails = selectedMatchDetails
        leagueDetails = selectedLeagueDetails
        selectedUserTeam = details
        
        lblTeamName.text = "Team".localized().uppercased() + " " + details.teamNumber!
        lblCaptionName.text = details.captionName
        lblViceCaptionName.text = details.viceCaptionName

        lblPGCount.text = details.totalPointGuardCount
        lblSGCount.text = details.totalShootingGuardCount
        lblSFCount.text = details.totalSmallForwardCount
        lblPFCount.text = details.totalPowerForwardCount
        lblCENCount.text = details.totalCenterCount

    }
    
    @IBAction func editButtonTapped(_ sender: Any) {
        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let selectPlayerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
            selectPlayerVC.matchDetails = matchDetails
            selectPlayerVC.userTeamDetails = selectedUserTeam
            selectPlayerVC.leagueDetails = leagueDetails
            if leagueDetails == nil{
                let leagueDetails = LeagueDetails()
                leagueDetails.fantasyType = "1"
                selectPlayerVC.leagueDetails = leagueDetails

            }

            selectPlayerVC.isEditPlayerTeam = true
            selectPlayerVC.isComeFromEditPlayerTeamScreen = true
            
            for details in selectedUserTeam!.playersArray!{
                details.isSelected = true
            }
            UserDetails.sharedInstance.selectedPlayerList = selectedUserTeam!.playersArray!
            navVC.pushViewController(selectPlayerVC, animated: true)
        }
    }
    
    @IBAction func previewButtonTapped(_ sender: Any?) {
        
        if matchDetails!.isMatchClosed {
            callGetFullScoreTeamPlayersDetails(userID: UserDetails.sharedInstance.userID, teamNumber: selectedUserTeam!.teamNumber ?? "", userName: UserDetails.sharedInstance.userName)
        }
        else{
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Football"])

                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let teamPlayerList = storyboard.instantiateViewController(withIdentifier: "BasketTeamPreviewViewController") as! BasketTeamPreviewViewController
                teamPlayerList.totalPlayerArray = selectedUserTeam!.playersArray!
                if matchDetails!.isMatchClosed {
                    teamPlayerList.isMatchClosed = true
                }
                
                if leagueDetails!.fantasyType == "1"{
                    teamPlayerList.selectedFantasy = FantasyType.Classic.rawValue
                }
                else if leagueDetails!.fantasyType == "2"{
                    teamPlayerList.selectedFantasy = FantasyType.Batting.rawValue
                }
                else if leagueDetails!.fantasyType == "3"{
                    teamPlayerList.selectedFantasy = FantasyType.Bowling.rawValue
                }
                teamPlayerList.teamNumber = selectedUserTeam!.teamNumber ?? ""
                teamPlayerList.firstTeamkey = matchDetails!.firstTeamKey
                teamPlayerList.secondTeamkey = matchDetails!.secondTeamKey

                teamPlayerList.firstTeamName = matchDetails!.firstTeamShortName ?? ""
                teamPlayerList.secondTeamName = matchDetails!.secondTeamShortName ?? ""
                teamPlayerList.isHideEditButton = true
                navVC.pushViewController(teamPlayerList, animated: true)
            }
        }
    }
    
    
    func callGetFullScoreTeamPlayersDetails(userID: String, teamNumber: String, userName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let type = "user_team"
        
        let parameters: Parameters = ["option": "full_scoreboard", "match_key": matchDetails!.matchKey, "team_number": teamNumber,"user_id": userID, "type": type, "fantasy_type": leagueDetails!.fantasyType]
        
        WebServiceHandler.performPOSTRequest(urlString: kFootballSocrescardUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let playerArray = result!["response"]?.array{
                        let playerListArray = PlayerDetails.getPlayerDetailsForScoreArray(responseArray: playerArray)
                        if self.matchDetails!.playersGender == "F" {
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: playerListArray)
                        }

                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Football"])

                        let teamPlayerList = storyboard.instantiateViewController(withIdentifier: "BasketTeamPreviewViewController") as! BasketTeamPreviewViewController
                        teamPlayerList.isMatchClosed = true
                        teamPlayerList.totalPlayerArray = playerListArray
                        teamPlayerList.firstTeamName = self.matchDetails!.firstTeamShortName ?? ""
                        teamPlayerList.teamNumber = teamNumber
                        teamPlayerList.userName = userName
                        teamPlayerList.firstTeamkey = self.matchDetails!.firstTeamKey
                        teamPlayerList.secondTeamkey = self.matchDetails!.secondTeamKey

                        teamPlayerList.secondTeamName = self.matchDetails!.secondTeamShortName ?? ""
                        if self.leagueDetails!.fantasyType == "1"{
                            teamPlayerList.selectedFantasy = FantasyType.Classic.rawValue
                        }
                        else if self.leagueDetails!.fantasyType == "2"{
                            teamPlayerList.selectedFantasy = FantasyType.Batting.rawValue
                        }
                        else if self.leagueDetails!.fantasyType == "3"{
                            teamPlayerList.selectedFantasy = FantasyType.Bowling.rawValue
                        }
                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.pushViewController(teamPlayerList, animated: true)
                            
                        }
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    @IBAction func cloneButtonTapped(_ sender: Any) {
        if matchDetails!.isMatchClosed{
            previewButtonTapped(nil)
            return;
        }
                    
        if totalTeamCount >= UserDetails.sharedInstance.maxTeamAllowedForBasketball{
            AppHelper.showAlertView(message: "You have already created maximum number of teams.", isErrorMessage: true)
            return;
        }

        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let selectPlayerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
            selectPlayerVC.matchDetails = matchDetails
            selectPlayerVC.leagueDetails = leagueDetails
            selectPlayerVC.userTeamDetails = selectedUserTeam
            selectPlayerVC.isComeFromEditPlayerTeamScreen = true
            if leagueDetails == nil{
                let leagueDetails = LeagueDetails()
                leagueDetails.fantasyType = "1"
                selectPlayerVC.leagueDetails = leagueDetails
            }
            for details in selectedUserTeam!.playersArray!{
                details.isSelected = true
            }
            UserDetails.sharedInstance.selectedPlayerList = selectedUserTeam!.playersArray!
            navVC.pushViewController(selectPlayerVC, animated: true)
        }
    }
    
    
    @IBAction func ScoreButtonTapped(_ sender: Any) {
        return;
    }
    
    func showShadowOnActionView() {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        
        // Edit/Privew/Clone View Layout
        actionButtonView.layer.cornerRadius = 20.0
        actionButtonView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        actionButtonView.layer.shadowOffset = CGSize(width: 0, height: 0)
        actionButtonView.layer.shadowOpacity = 0.35
        actionButtonView.layer.shadowRadius = 6.5
        actionButtonView.layer.masksToBounds = false
        
        // Score View Layout
        scoreButtonView.layer.cornerRadius = 20.0
        scoreButtonView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        scoreButtonView.layer.shadowOffset = CGSize(width: 0, height: 0)
        scoreButtonView.layer.shadowOpacity = 0.35
        scoreButtonView.layer.shadowRadius = 6.5
        scoreButtonView.layer.masksToBounds = false
    }
}
