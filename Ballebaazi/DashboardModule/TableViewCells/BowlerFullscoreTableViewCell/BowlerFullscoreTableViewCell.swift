//
//  BowlerFullscoreTableViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class BowlerFullscoreTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBowlerName: UILabel!
    
    @IBOutlet weak var lblMeddenCount: UILabel!
    
    @IBOutlet weak var lblRunCount: UILabel!
    
    @IBOutlet weak var lblWickets: UILabel!
    
    @IBOutlet weak var lblNoBowlesCount: UILabel!
    
    @IBOutlet weak var lblOverCount: UILabel!
    
    
    @IBOutlet weak var lblWideBowl: UILabel!
    
    @IBOutlet weak var lblEconomyRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
