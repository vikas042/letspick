//
//  TourneyNewTableViewCell.swift
//  Letspick
//
//  Created by anurag singh on 20/07/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class TourneyNewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblBBMatch: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(matchDetails: MatchDetails)  {
        /*
         overlayView.isHidden = true
         if matchDetails.active == "2" {
             overlayView.isHidden = false
         }
         overlayView.backgroundColor = UIColor(red: 215.0/255, green: 215.0/255, blue: 215.0/255, alpha: 0.4)
         AppHelper.showShodowOnCellsView(innerView: innerView)
        */
         lblBBMatch.text = matchDetails.seasonShortName.uppercased()
         lblBBMatch.layer.cornerRadius = 4.0
         lblBBMatch.clipsToBounds = true
       //  overlayView.layer.cornerRadius = 7.0
       //  freeTicketView.roundViewCorners(corners: [.bottomLeft,.bottomRight], radius: 7)
         lblTimer.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: matchDetails.startDateTimestemp)
       //  freeTicketView.isHidden = true
         if matchDetails.isPassAvailable{
            /*
             timerImgviewBottomConstraint.constant = 18
             freeTicketView.isHidden = false
             ticketImgView.image = UIImage(named: "PassIcon")
             lblTicketAvilableMessage.text = "PASS AVAILABLE".localized()
             lblTicketAvilableMessage.textColor = UIColor(red: 66.0/255, green: 117.0/255, blue: 211.0/255, alpha: 1)
             freeTicketView.backgroundColor = UIColor(red: 240.0/255, green: 240.0/255, blue: 240.0/255, alpha: 1)
             */
           //  layoutIfNeeded()
         }
         else if matchDetails.isTicketAvailable{
            /*
             timerImgviewBottomConstraint.constant = 18
             freeTicketView.isHidden = false
             ticketImgView.image = UIImage(named: "TicketHomeIcon")
             lblTicketAvilableMessage.text = "FreeTicketAvailable".localized()
             lblTicketAvilableMessage.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
             freeTicketView.backgroundColor = UIColor(red: 235.0/255, green: 251.0/255, blue: 235.0/255, alpha: 1)
             */
           //  layoutIfNeeded()
         }
         else{
            // timerImgviewBottomConstraint.constant = 8
            // layoutIfNeeded()
         }
         
         if matchDetails.imageName.count < 5 {
             self.imgView.image = UIImage(named: "Placeholder")
         }
         else if let url = NSURL(string: matchDetails.imageName){
             imgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
                 // Report progress
             }, completion: { [weak self] image in
                 if (image != nil){
                     self?.imgView.image = image
                 }
                 else{
                     self?.imgView.image = UIImage(named: "Placeholder")
                 }
             })
         }
         else{
             self.imgView.image = UIImage(named: "Placeholder")
         }
         
         if (matchDetails.active == "4") && !matchDetails.isMatchClosed {
             lblTimer.text = "00:00"
         }
        // lblTimer.textColor = UIColor.white

         if matchDetails.isMatchClosed{
             lblTimer.text = "Leagues Closed".localized()
             lblTimer.textColor = UIColor(red: 255.0/255, green: 14.0/255 , blue: 0.0/255, alpha: 1)

         }

     }
    
}
