//
//  BasketballFullFantasyViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class BasketballFullFantasyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var headerView: CustomNavigationBar!
    
    lazy var totalPointGuardArray = Array<PlayerDetails>()
    lazy var totalShootingGuardArray = Array<PlayerDetails>()
    lazy var totalSmallForwardArray = Array<PlayerDetails>()
    lazy var totalPowerForwardArray = Array<PlayerDetails>()
    lazy var totalCenterArray = Array<PlayerDetails>()

    lazy var playerListArray = Array<PlayerDetails>()
    
    var fantasyType = ""
    var matchKey = ""
    var firstTeamName = ""
    var secondTeamName = ""
    var playersGender = ""
    var matchDetails: MatchDetails?

    @IBOutlet weak var teamTwoButton: UIButton!
    @IBOutlet weak var teamOneButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "FullFantasyTableViewCell", bundle: nil), forCellReuseIdentifier: "FullFantasyTableViewCell")
        headerView.headerTitle = firstTeamName + " vs " + secondTeamName
        teamTwoButton.setTitle(secondTeamName, for: .normal)
        teamOneButton.setTitle(firstTeamName, for: .normal)
        var subTitle = ""
            
        if matchDetails?.isMatchClosed ?? false {
            if matchDetails!.matchStatus == "completed"{
                subTitle = "Completed".localized()
            }
            else if matchDetails!.matchStatus == "started"{
                subTitle = "Live".localized()
            }
            else{
                subTitle = "Leagues Closed".localized()
            }
        }
        headerView.updateHeaderTitles(title: firstTeamName + " vs " + secondTeamName, subTitle: subTitle)

        callGetTeamPlayersDetails()
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        
        if selectedRow == 0{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.PointGuard.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.ShootingGuard.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.SmallForward.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.PowerForward.rawValue
            })
        }
        else if selectedRow == 4{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Center.rawValue
            })
        }

        if playersArray != nil{
            return playersArray!
        }
        
        return []
    }
    
    
    private func callGetTeamPlayersDetails()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "full_scoreboard", "match_key": matchKey, "team_number": "1", "user_id": UserDetails.sharedInstance.userID, "type": "", "fantasy_type": fantasyType]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kBasketballSocrescardUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let playerArray = result!["response"]?.array{
                        
                        weakSelf?.playerListArray = PlayerDetails.getPlayerDetailsForScoreArray(responseArray: playerArray)
                        if self.playersGender == "F" {
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: weakSelf!.playerListArray)
                        }
                        
                        weakSelf?.totalPointGuardArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 0)
                        weakSelf?.totalShootingGuardArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 1)
                        weakSelf?.totalSmallForwardArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 2)
                        weakSelf?.totalPowerForwardArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 3)
                        weakSelf?.totalCenterArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 4)

                        weakSelf?.tblView.reloadData()
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 5
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 4
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            return 4
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 4
        }
        else if (totalPointGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 4
        }
        else if (totalShootingGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 4
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            return 3
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 3
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            return 3
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 3
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            return 3
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 3
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 {
            return 2
        }
        else if totalPointGuardArray.count > 0 && totalSmallForwardArray.count > 0 {
            return 2
        }
        else if totalPointGuardArray.count > 0 && totalPowerForwardArray.count > 0 {
            return 2
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 2
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            return 2
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 2
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 2
        }
        else if totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            return 2
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 2
        }
        else if totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            return 2
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 2
        }
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        label.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255, blue: 240.0/255, alpha: 1)
        label.text = getPlayerType(section: section)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.textColor = UIColor(red: 127.0/255.0, green: 132.0/255, blue: 134.0/255, alpha: 1)
        label.font = UIFont(name: "OpenSans-Semibold", size: 12)
//        label.backgroundColor = UIColor.white
        return label;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalSmallForwardArray.count
            }
            else if section == 3 {
                return totalPowerForwardArray.count
            }
            else if section == 4 {
                return totalCenterArray.count
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
                        
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalSmallForwardArray.count
            }
            else if section == 3 {
                return totalPowerForwardArray.count
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalSmallForwardArray.count
            }
            else if section == 3 {
                return totalCenterArray.count
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
            else if section == 3 {
                return totalCenterArray.count
            }
        }
        else if (totalPointGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
            else if section == 3 {
                return totalCenterArray.count
            }
        }
        else if (totalShootingGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
            else if section == 3 {
                return totalCenterArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalSmallForwardArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalCenterArray.count
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
            else if section == 2 {
                return totalCenterArray.count
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray.count
            }
            else if section == 1 {
                return totalPowerForwardArray.count
            }
            else if section == 2 {
                return totalCenterArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 {
            
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalSmallForwardArray.count > 0 {
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalPowerForwardArray.count > 0 {
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalPowerForwardArray.count
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPowerForwardArray.count
            }
            else if section == 1 {
                return totalCenterArray.count
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalPowerForwardArray.count

            }
        }
        else if totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalCenterArray.count
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray.count
            }
            else if section == 1 {
                return totalPowerForwardArray.count
            }
        }
        else if totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray.count
            }
            else if section == 1 {
                return totalCenterArray.count
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalCenterArray.count
            }
        }
        else if totalPointGuardArray.count > 0{
            return totalPointGuardArray.count
        }
        else if totalShootingGuardArray.count > 0{
            return totalShootingGuardArray.count
        }
        else if totalSmallForwardArray.count > 0{
            return totalSmallForwardArray.count
        }
        else if totalPowerForwardArray.count > 0{
            return totalPowerForwardArray.count
        }
        else if totalCenterArray.count > 0{
            return totalCenterArray.count
        }
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "FullFantasyTableViewCell") as? FullFantasyTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "FullFantasyTableViewCell") as? FullFantasyTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.contentView.backgroundColor = UIColor.white
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        cell?.configData(details: playerInfo, legueType: FantasyType.Classic.rawValue, gameType: GameType.Football.rawValue)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        let teamScoresView = BasketballPlayerPointsView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
        teamScoresView.configData(details: playerInfo)
        APPDELEGATE.window!.addSubview(teamScoresView)
        teamScoresView.showAnimation()
    }
    
    func getPlayerType(section: Int) -> String {
        
        if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 3 {

                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
                
            }
            else if section == 4 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
                        
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 3 {

                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 3 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 3 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if (totalPointGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {

                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 3 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if (totalShootingGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 3 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 2 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 2 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 {
            
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalSmallForwardArray.count > 0 {
            
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalPowerForwardArray.count > 0 {
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 1 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 1 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalPointGuardArray.count > 0{
            if totalPointGuardArray.count > 1 {
                return "Point-Guard".localized()
            }
            return "Point-Guard".localized()
        }
        else if totalShootingGuardArray.count > 0{
            if totalShootingGuardArray.count > 1 {
                return "Shooting-Guard".localized()
            }
            return "Shooting-Guard".localized()
        }
        else if totalSmallForwardArray.count > 0{
            if totalSmallForwardArray.count > 1 {
                return "Small-Forward".localized()
            }
            return "Small-Forward".localized()
        }
        else if totalPowerForwardArray.count > 0{
            if totalPowerForwardArray.count > 1 {
                return "Power-Forward".localized()
            }
            return "Power-Forward".localized()
        }
        else if totalCenterArray.count > 0{
            if totalCenterArray.count > 1 {
                return "Center".localized()
            }
            return "Center".localized()
        }
        return ""
    }
    
    
    func getPlayerDetails(section: Int, index: Int) -> PlayerDetails {
        
        if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalSmallForwardArray[index]
            }
            else if section == 3 {
                return totalPowerForwardArray[index]
            }
            else if section == 4 {
                return totalCenterArray[index]
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalSmallForwardArray[index]
            }
            else if section == 3 {
                return totalPowerForwardArray[index]
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalSmallForwardArray[index]
            }
            else if section == 3 {
                return totalCenterArray[index]
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
            else if section == 3 {
                return totalCenterArray[index]
            }
        }
        else if (totalPointGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
            else if section == 3 {
                return totalCenterArray[index]
            }
        }
        else if (totalShootingGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
            else if section == 3 {
                return totalCenterArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalSmallForwardArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalCenterArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
            else if section == 2 {
                return totalCenterArray[index]
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
            else if section == 2 {
                return totalCenterArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 {
            
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalSmallForwardArray.count > 0 {
            
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalPowerForwardArray.count > 0 {
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPowerForwardArray[index]
            }
            else if section == 1 {
                return totalCenterArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalCenterArray[index]
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray[index]
            }
            else if section == 1 {
                return totalCenterArray[index]
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPowerForwardArray[index]
            }
            else if section == 1 {
                return totalCenterArray[index]
            }
        }
        else if totalPointGuardArray.count > 0{
            return totalPointGuardArray[index]
        }
        else if totalShootingGuardArray.count > 0{
            return totalShootingGuardArray[index]
        }
        else if totalSmallForwardArray.count > 0{
            return totalSmallForwardArray[index]
        }
        else if totalPowerForwardArray.count > 0{
            return totalPowerForwardArray[index]
        }
        else if totalCenterArray.count > 0{
            return totalCenterArray[index]
        }
        return PlayerDetails()
    }
}
