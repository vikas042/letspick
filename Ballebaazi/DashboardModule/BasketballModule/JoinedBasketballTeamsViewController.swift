//
//  JoinedBasketballTeamsViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class JoinedBasketballTeamsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SwipeTeamViewDelegate {
    
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblTeams: UILabel!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var teamButton: UIButton!
    @IBOutlet weak var tblView: UITableView!
    lazy var nextPageCount = "0"
    lazy var isNeedToShowLoadMore = false
    lazy var contestUserArray = Array<JoinedLeagueUserList>()
    lazy var myTeamsArray = Array<JoinedLeagueUserList>()
    var joinedLeagueDetails: JoinedLeagueDetails?
    lazy var refreshControl = UIRefreshControl()
    lazy var isPullTORefresh = false
    @IBOutlet weak var tblViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: LetspickFantasyWithoutWalletHeaderView!
    var timer: Timer?
    var selectedMatchDetails: MatchDetails?
    var userTeamsArray:Array<UserTeamDetails>?
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.isFantasyModeEnable = false
        headerView.backButtonTitle = "My Teams".localized()
        lblUserName.text = "Username".localized()
        lblTeams.text = "Team".localized()
        lblPoints.text = "Points".localized()
        lblRank.text = "Rank".localized()
        
        headerView.updateMatchName(details: selectedMatchDetails)
        navigationController?.navigationBar.isHidden = true
        contestUserArray = joinedLeagueDetails!.contestPlayers!
        myTeamsArray = joinedLeagueDetails!.myTeamPlayers!
        tblView.register(UINib(nibName: "JoinedLeagueUserTableViewCell", bundle: nil), forCellReuseIdentifier: "JoinedLeagueUserTableViewCell")
        tblView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        
        nextPageCount = joinedLeagueDetails!.nextPageCount ?? "0"
        if nextPageCount != "0"{
            isNeedToShowLoadMore = true
        }
        
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing...")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        tblView.addSubview(refreshControl)
        
        updateTimerVlaue()
        
        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
        
        if(joinedLeagueDetails?.userTeamsPdf == ""){
            teamButton.isHidden = true
            tblView.layoutIfNeeded()
        }
        else{
            teamButton.isHidden = false
            //            tblViewBottomConstraint.constant = 56
            tblView.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        if contestUserArray.count > 0 {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        let titleLabel = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.bounds.size.width - 15, height: 30))
        titleLabel.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont(name: "OpenSans-Semibold", size: 12)
        headerView.backgroundColor = UIColor(red: 247.0/255, green: 247.0/255, blue: 247.0/255, alpha: 1)
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        
        if section == 0 {
            if myTeamsArray.count > 1{
                titleLabel.text = "My Teams".localized()
            }
            titleLabel.text = "My Team".localized()
        }
        else{
            
            if contestUserArray.count > 1{
                titleLabel.text = "Opponent Team".localized()
            }
            titleLabel.text = "Opponent Teams".localized()
        }

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            if isNeedToShowLoadMore{
                return contestUserArray.count + 1
            }
            return contestUserArray.count
        }
        
        return myTeamsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if (indexPath.row == contestUserArray.count) && (indexPath.section == 1){
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            }
            
            if isNeedToShowLoadMore{
                isNeedToShowLoadMore = false
                callGetNextPageJoinedLeagueAPI()
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            return cell!
        }
        else{
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "JoinedLeagueUserTableViewCell") as? JoinedLeagueUserTableViewCell
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "JoinedLeagueUserTableViewCell") as? JoinedLeagueUserTableViewCell
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            var details: JoinedLeagueUserList!
            if indexPath.section == 0 {
                details = myTeamsArray[indexPath.row]
            }
            else{
                details = contestUserArray[indexPath.row]
            }
            
            cell?.lblPoints.isHidden = false
            cell?.swapButton.isHidden = true
            cell?.lblRank.isHidden = false
            cell?.swapButton.addTarget(self, action: #selector(swapTeamButtonTapped(button:)), for: .touchUpInside)
            
            if selectedMatchDetails!.isMatchClosed{
                cell?.swapButton.isHidden = true
                cell?.lblRank.isHidden = false
            }
            else{
                if details.userID == UserDetails.sharedInstance.userID{
                    cell?.swapButton.isHidden = false
                    cell?.lblRank.isHidden = true
                }
            }
            
            if (selectedMatchDetails?.matchStatus == "completed") &&  (selectedMatchDetails?.adminStatus == "2"){
                cell?.configData(details: details, isPointsDistributed: true, fantasyType: joinedLeagueDetails?.fantasyType ?? "")
            }
            else{
                cell?.configData(details: details, isPointsDistributed: false, fantasyType: joinedLeagueDetails?.fantasyType ?? "")
            }
            
            if selectedMatchDetails!.matchStatus == "notstarted"{
                cell?.lblRank.text = "Not Yet".localized()
            }
            
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedMatchDetails!.isMatchClosed{
            var details: JoinedLeagueUserList!
            if indexPath.section == 0{
                details = myTeamsArray[indexPath.row]
            }
            else{
                details = contestUserArray[indexPath.row]
            }
            callGetFullScoreTeamPlayersDetails(userID: details.userID!, teamNumber: details.teamNumber!, userName: details.userName!, type: "user_team")
        }
        else{
            if indexPath.section == 0{
                let details = myTeamsArray[indexPath.row]
                callGetFullScoreTeamPlayersDetails(userID: details.userID!, teamNumber: details.teamNumber!, userName: details.userName!, type: "user_team")
            }
            
        }
    }
    
    func callGetFullScoreTeamPlayersDetails(userID: String, teamNumber: String, userName: String, type: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        //        let type = "user_team"
        
        let parameters = ["option": "full_scoreboard", "match_key": selectedMatchDetails!.matchKey, "team_number": teamNumber,"user_id": userID, "type": type, "fantasy_type": joinedLeagueDetails!.fantasyType!]
        
        WebServiceHandler.performPOSTRequest(urlString: kBasketballSocrescardUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let playerArray = result!["response"]?.array{
                        let playerListArray = PlayerDetails.getPlayerDetailsForScoreArray(responseArray: playerArray)
                        if self.selectedMatchDetails!.playersGender == "F" {
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: playerListArray)
                        }

                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Basketball"])

                        let teamPreviewVC = storyboard.instantiateViewController(withIdentifier: "BasketTeamPreviewViewController") as! BasketTeamPreviewViewController
                        teamPreviewVC.isMatchClosed = true
                        teamPreviewVC.totalPlayerArray = playerListArray
                        teamPreviewVC.firstTeamName = self.selectedMatchDetails!.firstTeamShortName ?? ""
                        teamPreviewVC.firstTeamkey = self.selectedMatchDetails!.firstTeamKey
                        teamPreviewVC.secondTeamkey = self.selectedMatchDetails!.secondTeamKey
                        teamPreviewVC.teamNumber = teamNumber
                        teamPreviewVC.userName = userName
                        teamPreviewVC.secondTeamName = self.selectedMatchDetails!.secondTeamShortName ?? ""
                        self.navigationController?.pushViewController(teamPreviewVC, animated: true)
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    //MARK:- API Related Method
    @objc func swapTeamButtonTapped(button: UIButton) {
        
        if userTeamsArray != nil{
            guard let cell = button.superview?.superview as? JoinedLeagueUserTableViewCell else {
                return // or fatalError() or whatever
            }
            
            guard let indexPath = tblView.indexPath(for: cell) else{
                return;
            }
            if indexPath.section != 0{
                return;
            }
            let details = myTeamsArray[indexPath.row]
            if details.userID == UserDetails.sharedInstance.userID{
                if (userTeamsArray?.count)! <= 1{
                    AppHelper.showAlertView(message: "You have only single team. Please create another team.", isErrorMessage: true)
                    return
                }
                
                let swipeTeamView = SwipeTeamView(frame: (APPDELEGATE.window?.frame)!)
                var tempArray = Array<UserTeamDetails>()
                
                for tempDetails in userTeamsArray!{
                    if details.teamNumber != tempDetails.teamNumber{
                        tempArray.append(tempDetails)
                    }
                }
                swipeTeamView.userTeamsArray = tempArray
                swipeTeamView.leagueUserDetails = details;
                swipeTeamView.matchDetails = selectedMatchDetails
                swipeTeamView.leagueDetails = joinedLeagueDetails
                swipeTeamView.selectedGameType = GameType.Basketball.rawValue
                swipeTeamView.deleagte = self;
                swipeTeamView.updateData(gameType: GameType.Basketball.rawValue)
                
                APPDELEGATE.window?.addSubview(swipeTeamView)
                swipeTeamView.showAnimation()
            }
        }
    }
    
    func callGetNextPageJoinedLeagueAPI(){
        
        if !AppHelper.isInterNetConnectionAvailable(){
            self.refreshControl.endRefreshing()
            return;
        }
        
        let parameters = ["option": "get_contest_data", "match_key": selectedMatchDetails!.matchKey,"league_id": joinedLeagueDetails!.leagueId!, "user_id": UserDetails.sharedInstance.userID, "page": nextPageCount]
        weak var weakSelf = self;
        WebServiceHandler.performPOSTRequest(urlString: kBasketballSocresUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            self.isNeedToShowLoadMore = false
            self.refreshControl.endRefreshing()
            if result != nil{
                
                let statusCode = result!["status"]?.string
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                        let nextPage = response["next_page"]?.string ?? "0"
                        if nextPage != "0"{
                            weakSelf?.nextPageCount = nextPage
                            weakSelf?.isNeedToShowLoadMore = true
                        }
                        weakSelf?.joinedLeagueDetails?.nextPageCount = nextPage
                        var isNeedToAddCurrentUser = true
                        
                        if weakSelf!.isPullTORefresh{
                            isNeedToAddCurrentUser = true
                        }
                        else if weakSelf?.contestUserArray.count != 0{
                            isNeedToAddCurrentUser = false;
                        }
                        
                        let (tempArray, myTeams) = JoinedLeagueUserList.getNextPageParticipatedUsers(contestUsers: response, isNeedToAddCurrentUser: isNeedToAddCurrentUser)
                        
                        if weakSelf!.isPullTORefresh{
                            weakSelf?.contestUserArray = tempArray
                            weakSelf?.myTeamsArray = myTeams
                            weakSelf?.isPullTORefresh = false
                        }
                        else{
                            weakSelf?.contestUserArray.append(contentsOf: tempArray)
                        }
                        weakSelf?.tblView.reloadData()
                    }
                    
                    weakSelf?.tblView.reloadData()
                }
                else{
                    let message = result!["message"]?.string ?? "kErrorMsg".localized()
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    //MARK:- Timer Handler
    @objc func updateTimerVlaue(){
        
        if (selectedMatchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if selectedMatchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(details: selectedMatchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: selectedMatchDetails!)
        }
    }
    
    func refereshteamData(teamNumebr: String) {

        tblView.reloadData()
    }
    
    @objc func refresh(sender:AnyObject) {
        nextPageCount = "0"
        isPullTORefresh = true
        callGetNextPageJoinedLeagueAPI()
    }
    
    //MARK:- -IBAction Method
    @IBAction func pdfDownloadButtonTapped(_ sender: Any) {
        
        if UserDetails.sharedInstance.teams_pdf.count == 0 {
            return;
        }
        else if joinedLeagueDetails == nil {
            return;
        }
        
        let pdfString = UserDetails.sharedInstance.teams_pdf + joinedLeagueDetails!.userTeamsPdf
        
        if let url = URL(string: pdfString),
            UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
