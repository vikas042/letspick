//
//  SelectCaptainBasketballViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 06/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class SelectCaptainBasketballViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var saveButton: SolidButton!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: LetspickCreateTeamHeaderView!
    @IBOutlet weak var teamPreview: CustomBorderButton!

    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var userTeamDetails: UserTeamDetails?
    var timer: Timer?
    var captionDetails: PlayerDetails?
    var viceCaptionDetails: PlayerDetails?
    var ticketDetails: TicketDetails?
    
    lazy var isMatchClosingTimeRefereshing = false

    lazy var isEditPlayerTeam = false
    
    lazy var totalPointGuardArray = Array<PlayerDetails>()
    lazy var totalShootingGuardArray = Array<PlayerDetails>()
    lazy var totalSmallForwardArray = Array<PlayerDetails>()
    lazy var totalPowerForwardArray = Array<PlayerDetails>()
    lazy var totalCenterArray = Array<PlayerDetails>()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "SelectCaptainTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectCaptainTableViewCell")
        setupProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        
        if AppHelper.isApplicationRunningOnIphoneX(){
            bottomViewHeightConstraint.constant = 85;
            bottomView.layoutIfNeeded()
        }
        
        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }
    }
    
    //MARK:- -IBAction Methods
    @IBAction func saveButtonTapped(_ sender: Any) {
        
        if (captionDetails == nil) {
            AppHelper.showAlertView(message: "Please select caption.", isErrorMessage: true)
            return;
        }
        else if (viceCaptionDetails == nil) {
            AppHelper.showAlertView(message: "Please select vice-caption.", isErrorMessage: true)
            return;
        }
        
        if isEditPlayerTeam {
            callUpdateTeamAPI()
        }
        else{
            callCreateTeamAPI()
        }
    }
    
    @IBAction func previewButtonTapped(_ sender: Any) {
        
        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "BasketTeamPreviewViewController") as! BasketTeamPreviewViewController
        teamPreviewVC.totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        teamPreviewVC.isShowPlayingRole = true;
        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Basketball"])
        teamPreviewVC.selectedFantasy = FantasyType.Classic.rawValue
        teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
        teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
        teamPreviewVC.firstTeamkey = self.matchDetails!.firstTeamKey
        teamPreviewVC.secondTeamkey = self.matchDetails!.secondTeamKey

        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    //MARK:- Custom Methods
    func setupProperties() {
        headerView.isViewForSelectCaptain = true
        AppHelper.designBottomTabDesing(bottomView)
        
        totalPointGuardArray = getSelectedTypePlayerList(selectedRow: 0)
        totalPowerForwardArray = getSelectedTypePlayerList(selectedRow: 1)
        totalSmallForwardArray = getSelectedTypePlayerList(selectedRow: 2)
        totalShootingGuardArray = getSelectedTypePlayerList(selectedRow: 3)
        totalCenterArray = getSelectedTypePlayerList(selectedRow: 4)
        teamPreview.setTitle("TeamPreview".localized(), for: .normal)
        saveButton.setTitle("Next".localized(), for: .normal)
        teamPreview.setTitle("TeamPreview".localized(), for: .selected)
        saveButton.setTitle("Next".localized(), for: .selected)

        let caption = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isCaption
        }
        
        let viceCaption = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isViceCaption
        }
        
        if viceCaption.count > 0 {
            viceCaptionDetails = viceCaption[0]
        }
        
        if caption.count > 0 {
            captionDetails = caption[0]
        }
        
        headerView.setupDefaultPropertiesForCaptainVicecaptaion(fantasyType: leagueDetails?.fantasyType ?? "")
        headerView.updateCaptainAndViceCaptainName(captain: captionDetails, viceCaptain: viceCaptionDetails)
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            
            weak var wealSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                wealSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    
    //MARK:- Timer Handler
    @objc func updateTimerVlaue()  {
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kBasketballMatchURL, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 5
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 4
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            return 4
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 4
        }
        else if (totalPointGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 4
        }
        else if (totalShootingGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 4
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            return 3
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 3
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            return 3
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 3
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            return 3
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 3
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 {
            return 2
        }
        else if totalPointGuardArray.count > 0 && totalSmallForwardArray.count > 0 {
            return 2
        }
        else if totalPointGuardArray.count > 0 && totalPowerForwardArray.count > 0 {
            return 2
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 2
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            return 2
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 2
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 2
        }
        else if totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            return 2
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            return 2
        }
        else if totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            return 2
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            return 2
        }
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        
        label.text = getPlayerType(section: section)
        label.textAlignment = .center
        label.textColor = UIColor(red: 127.0/255.0, green: 132.0/255, blue: 134.0/255, alpha: 1)
        label.font = UIFont(name: "OpenSans-Semibold", size: 12)
        label.backgroundColor = UIColor(red: 235.0/255.0, green: 235.0/255, blue: 235.0/255, alpha: 1)
        return label;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalSmallForwardArray.count
            }
            else if section == 3 {
                return totalPowerForwardArray.count
            }
            else if section == 4 {
                return totalCenterArray.count
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
                        
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalSmallForwardArray.count
            }
            else if section == 3 {
                return totalPowerForwardArray.count
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalSmallForwardArray.count
            }
            else if section == 3 {
                return totalCenterArray.count
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
            else if section == 3 {
                return totalCenterArray.count
            }
        }
        else if (totalPointGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
            else if section == 3 {
                return totalCenterArray.count
            }
        }
        else if (totalShootingGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
            else if section == 3 {
                return totalCenterArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalSmallForwardArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
            else if section == 2 {
                return totalCenterArray.count
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
            else if section == 2 {
                return totalPowerForwardArray.count
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
            else if section == 2 {
                return totalCenterArray.count
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray.count
            }
            else if section == 1 {
                return totalPowerForwardArray.count
            }
            else if section == 2 {
                return totalCenterArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 {
            
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalShootingGuardArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalSmallForwardArray.count > 0 {
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
        }
        else if totalPointGuardArray.count > 0 && totalPowerForwardArray.count > 0 {
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalPowerForwardArray.count
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPowerForwardArray.count
            }
            else if section == 1 {
                return totalCenterArray.count
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalSmallForwardArray.count
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalPowerForwardArray.count

            }
        }
        else if totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray.count
            }
            else if section == 1 {
                return totalCenterArray.count
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray.count
            }
            else if section == 1 {
                return totalPowerForwardArray.count
            }
        }
        else if totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray.count
            }
            else if section == 1 {
                return totalCenterArray.count
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray.count
            }
            else if section == 1 {
                return totalCenterArray.count
            }
        }
        else if totalPointGuardArray.count > 0{
            return totalPointGuardArray.count
        }
        else if totalShootingGuardArray.count > 0{
            return totalShootingGuardArray.count
        }
        else if totalSmallForwardArray.count > 0{
            return totalSmallForwardArray.count
        }
        else if totalPowerForwardArray.count > 0{
            return totalPowerForwardArray.count
        }
        else if totalCenterArray.count > 0{
            return totalCenterArray.count
        }
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SelectCaptainTableViewCell") as? SelectCaptainTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "SelectCaptainTableViewCell") as? SelectCaptainTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.captainButton.tag = indexPath.row
        cell?.viceCaptainButton.tag = indexPath.row
        cell?.contentView.backgroundColor = UIColor.white
        cell?.captainButton.addTarget(self, action: #selector(self.captainButtonTapped(button:)), for: .touchUpInside)
        cell?.viceCaptainButton.addTarget(self, action: #selector(self.viceCaptainButtonTapped(button:)), for: .touchUpInside)
        cell?.captainButton.backgroundColor = UIColor.white
        cell?.viceCaptainButton.backgroundColor = UIColor.white
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        cell?.viceCaptainButton.setTitleColor(UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1), for: .normal)
        cell?.captainButton.setTitleColor(UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1), for: .normal)
        cell?.contentView.backgroundColor = UIColor.white
        
        if playerInfo.playerKey == captionDetails?.playerKey {
            cell?.captainButton.backgroundColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1)
            cell?.captainButton.setTitleColor(UIColor.white, for: .normal)
            cell?.contentView.backgroundColor = UIColor(red: 247.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
        }
        
        if playerInfo.playerKey == viceCaptionDetails?.playerKey {
            cell?.viceCaptainButton.backgroundColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1)
            cell?.viceCaptainButton.setTitleColor(UIColor.white, for: .normal)
            cell?.contentView.backgroundColor = UIColor(red: 247.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
        }
        
        cell?.configData(details: playerInfo, legueType: FantasyType.Classic.rawValue, gameType: GameType.Baseball.rawValue)

        return cell!
    }
    
    
    @objc func captainButtonTapped(button: UIButton) {
        guard let cell = button.superview?.superview as? SelectCaptainTableViewCell else {
            return // or fatalError() or whatever
        }
        
        guard let indexPath = tblView.indexPath(for: cell) else{
            return;
        }
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        if playerInfo.playerKey == viceCaptionDetails?.playerKey{
            playerInfo.isViceCaption = false
            viceCaptionDetails = nil
        }
        captionDetails = playerInfo
        AppxorEventHandler.logAppEvent(withName: "SelectCaptainClicked", info: ["SportType": "Basketball", "PlayerName": playerInfo.playerName ?? "", "PlayerID": playerInfo.playerKey ?? ""])

        for details in totalSmallForwardArray {
            details.isCaption = false
        }
        
        for details in totalShootingGuardArray {
            details.isCaption = false
        }
        
        for details in totalPowerForwardArray {
            details.isCaption = false
        }
        
        for details in totalPointGuardArray {
            details.isCaption = false
        }
        
        captionDetails?.isCaption = true
        captionDetails?.isViceCaption = false
        headerView.updateCaptainAndViceCaptainName(captain: captionDetails, viceCaptain: viceCaptionDetails)
        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }
        
        tblView.reloadData()
        
    }
    
    func getPlayerType(section: Int) -> String {
        
        if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 3 {

                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
                
            }
            else if section == 4 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
                        
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 3 {

                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 3 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 3 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if (totalPointGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {

                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 3 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if (totalShootingGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 3 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 2 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 2 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 2 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 2 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 {
            
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalSmallForwardArray.count > 0 {
            
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
        }
        else if totalPointGuardArray.count > 0 && totalPowerForwardArray.count > 0 {
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
            else if section == 1 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalShootingGuardArray.count > 1 {
                    return "Shooting-Guard".localized()
                }
                return "Shooting-Guard".localized()
            }
            else if section == 1 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 1 {
                if totalPowerForwardArray.count > 1 {
                    return "Power-Forward".localized()
                }
                return "Power-Forward".localized()
            }
        }
        else if totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalSmallForwardArray.count > 1 {
                    return "Small-Forward".localized()
                }
                return "Small-Forward".localized()
            }
            else if section == 1 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                if totalPointGuardArray.count > 1 {
                    return "Point-Guard".localized()
                }
                return "Point-Guard".localized()
            }
            else if section == 1 {
                if totalCenterArray.count > 1 {
                    return "Center".localized()
                }
                return "Center".localized()
            }
        }
        else if totalPointGuardArray.count > 0{
            if totalPointGuardArray.count > 1 {
                return "Point-Guard".localized()
            }
            return "Point-Guard".localized()
        }
        else if totalShootingGuardArray.count > 0{
            if totalShootingGuardArray.count > 1 {
                return "Shooting-Guard".localized()
            }
            return "Shooting-Guard".localized()
        }
        else if totalSmallForwardArray.count > 0{
            if totalSmallForwardArray.count > 1 {
                return "Small-Forward".localized()
            }
            return "Small-Forward".localized()
        }
        else if totalPowerForwardArray.count > 0{
            if totalPowerForwardArray.count > 1 {
                return "Power-Forward".localized()
            }
            return "Power-Forward".localized()
        }
        else if totalCenterArray.count > 0{
            if totalCenterArray.count > 1 {
                return "Center".localized()
            }
            return "Center".localized()
        }
        return ""
    }
    
    func getPlayerDetails(section: Int, index: Int) -> PlayerDetails {
        
        if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalSmallForwardArray[index]
            }
            else if section == 3 {
                return totalPowerForwardArray[index]
            }
            else if section == 4 {
                return totalCenterArray[index]
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalSmallForwardArray[index]
            }
            else if section == 3 {
                return totalPowerForwardArray[index]
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalSmallForwardArray[index]
            }
            else if section == 3 {
                return totalCenterArray[index]
            }
        }
        else if (totalPointGuardArray.count > 0) && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
            else if section == 3 {
                return totalCenterArray[index]
            }
        }
        else if (totalPointGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
            else if section == 3 {
                return totalCenterArray[index]
            }
        }
        else if (totalShootingGuardArray.count > 0) && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
            else if section == 3 {
                return totalCenterArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalSmallForwardArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
            else if section == 2 {
                return totalCenterArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
            else if section == 2 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
            else if section == 2 {
                return totalCenterArray[index]
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
            else if section == 2 {
                return totalCenterArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalShootingGuardArray.count > 0 {
            
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalShootingGuardArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalSmallForwardArray.count > 0 {
            
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
        }
        else if totalPointGuardArray.count > 0 && totalPowerForwardArray.count > 0 {
            if section == 0 {
                return totalPointGuardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPowerForwardArray[index]
            }
            else if section == 1 {
                return totalCenterArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalSmallForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalSmallForwardArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalShootingGuardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalShootingGuardArray[index]
            }
            else if section == 1 {
                return totalCenterArray[index]
            }
        }
        else if totalSmallForwardArray.count > 0 && totalPowerForwardArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray[index]
            }
            else if section == 1 {
                return totalPowerForwardArray[index]
            }
        }
        else if totalSmallForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalSmallForwardArray[index]
            }
            else if section == 1 {
                return totalCenterArray[index]
            }
        }
        else if totalPowerForwardArray.count > 0 && totalCenterArray.count > 0{
            if section == 0 {
                return totalPowerForwardArray[index]
            }
            else if section == 1 {
                return totalCenterArray[index]
            }
        }
        else if totalPointGuardArray.count > 0{
            return totalPointGuardArray[index]
        }
        else if totalShootingGuardArray.count > 0{
            return totalShootingGuardArray[index]
        }
        else if totalSmallForwardArray.count > 0{
            return totalSmallForwardArray[index]
        }
        else if totalPowerForwardArray.count > 0{
            return totalPowerForwardArray[index]
        }
        else if totalCenterArray.count > 0{
            return totalCenterArray[index]
        }
        return PlayerDetails()
    }
    
    @objc func viceCaptainButtonTapped(button: UIButton) {
        guard let cell = button.superview?.superview as? SelectCaptainTableViewCell else {
            return // or fatalError() or whatever
        }
        
        guard let indexPath = tblView.indexPath(for: cell) else{
            return;
        }
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        
        if playerInfo.playerKey == captionDetails?.playerKey{
            playerInfo.isCaption = false
            captionDetails = nil
        }
        
        viceCaptionDetails = playerInfo
        AppxorEventHandler.logAppEvent(withName: "SelectViceCaptainClicked", info: ["SportType": "Basketball", "PlayerName": playerInfo.playerName ?? "", "PlayerID": playerInfo.playerKey ?? ""])

        for details in totalSmallForwardArray {
            details.isViceCaption = false
        }
        
        for details in totalShootingGuardArray {
            details.isViceCaption = false
        }
        
        for details in totalPowerForwardArray {
            details.isViceCaption = false
        }
        
        for details in totalPointGuardArray {
            details.isViceCaption = false
        }
        
        viceCaptionDetails?.isViceCaption = true
        viceCaptionDetails?.isCaption = false
        headerView.updateCaptainAndViceCaptainName(captain: captionDetails, viceCaptain: viceCaptionDetails)
        
        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }
        
        tblView.reloadData()
    }
    
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        
        if selectedRow == 0{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.PointGuard.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.ShootingGuard.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.SmallForward.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.PowerForward.rawValue
            })
        }
        else if selectedRow == 4{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Center.rawValue
            })
        }
        
        if playersArray != nil{
            return playersArray!
        }
        
        return []
    }
    
    
    //MARK:- API Releated Methods
    
    func callCreateTeamAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var playerIDs: String?
        let captionID = captionDetails!.playerKey ?? ""
        let viceCaptionID = viceCaptionDetails!.playerKey ?? ""
        
        for details in UserDetails.sharedInstance.selectedPlayerList {
            if playerIDs == nil{
                playerIDs = details.playerKey!
            }
            else{
                playerIDs = playerIDs! + "," + details.playerKey!
            }
        }
        
        let parameters = ["option": "create_team","captain": captionID, "vice_captain": viceCaptionID, "fantasy_type": leagueDetails!.fantasyType, "match_key": matchDetails!.matchKey, "players": playerIDs!, "user_id": UserDetails.sharedInstance.userID]
        
        weak var weakSelf = self;
        WebServiceHandler.performPOSTRequest(urlString: kBasketballMatchURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if weakSelf?.leagueDetails?.matchKey.count == 0{
                        AppHelper.showToast(message: "Team Created Successfully!".localized())
                        
                        let navArray = weakSelf?.navigationController?.viewControllers
                        if navArray!.count > 1 {
                            if let leagueVC = navArray![1] as? BasketballMoreLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                                }
                            }
                            else if let leagueVC = navArray![1] as? BasketballLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    leagueVC.headerView.classicButtonTapped(isNeedToScroll: false)
                                    weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                                }
                            }
                            else if let joinedLeagueVC = navArray![1] as? FootballJoinedLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    joinedLeagueVC.headerView.classicButtonTapped(isNeedToScroll: false)
                                    weakSelf?.navigationController?.popToViewController(joinedLeagueVC, animated: true)
                                }
                            }
                        }
                    }
                    else{
                        
                        if let response = result!["response"]{
                            let userTeam = response["user_team"]
                            if userTeam.count > 0{
                                let teamArray = UserTeamDetails.getUserTeamsArray(responseArray: [userTeam], matchDetails: weakSelf!.matchDetails!)
                                if teamArray.count > 0{
                                    weakSelf?.userTeamDetails = teamArray[0]
                                }
                            }
                        }
                        self.goToJoinLeagueConfirmation()
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    private func callUpdateTeamAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var playerIDs: String?
        let captionID = captionDetails!.playerKey ?? ""
        let viceCaptionID = viceCaptionDetails!.playerKey ?? ""
        
        for details in UserDetails.sharedInstance.selectedPlayerList {
            if playerIDs == nil{
                playerIDs = details.playerKey!
            }
            else{
                playerIDs = playerIDs! + "," + details.playerKey!
            }
        }
        
        let parameters = ["option": "update_team","captain": captionID, "vice_captain": viceCaptionID, "fantasy_type": leagueDetails!.fantasyType, "match_key": matchDetails!.matchKey, "players": playerIDs!, "user_id": UserDetails.sharedInstance.userID, "team_number": userTeamDetails!.teamNumber!]
        
        WebServiceHandler.performPOSTRequest(urlString: kBasketballMatchURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            weak var weakSelf = self;
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    AppHelper.showToast(message: "Team Updated Successfully!".localized())
                    
                    let navArray = weakSelf?.navigationController?.viewControllers
                    if navArray!.count > 1 {
                        if let leagueVC = navArray![1] as? BasketballMoreLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                            }
                        }
                        else if let leagueVC = navArray![1] as? BasketballLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                leagueVC.classicButtonTapped(isAnimation: false)
                                weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                            }
                        }
                        else if let joinedLeagueVC = navArray![1] as? FootballJoinedLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                
                                if weakSelf?.leagueDetails?.fantasyType == "1"{
                                    joinedLeagueVC.classicButtonTapped(isNeedToAnimate: false)
                                }
                                weakSelf?.navigationController?.popToViewController(joinedLeagueVC, animated: true)
                            }
                        }
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    // MARK: - Navigation
    
    func goToJoinLeagueConfirmation() {
        
        let joinLegueConfiramation = storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as! JoinLeagueConfirmationViewController
        joinLegueConfiramation.matchDetails = matchDetails
        joinLegueConfiramation.ticketDetails = ticketDetails
        joinLegueConfiramation.leagueDetails = leagueDetails
        if userTeamDetails != nil {
            joinLegueConfiramation.userTeamsArray = [userTeamDetails!]
        }
        joinLegueConfiramation.selectedGameType = GameType.Basketball.rawValue
        self.navigationController?.pushViewController(joinLegueConfiramation, animated: true)
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
