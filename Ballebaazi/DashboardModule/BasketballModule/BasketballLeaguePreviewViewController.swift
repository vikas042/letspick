//
//  BasketballLeaguePreviewViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 06/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class BasketballLeaguePreviewViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var joinButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblEntry: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var ticketIcon: UIImageView!
    @IBOutlet weak var joinedViewWidthConstaint: NSLayoutConstraint!
    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var headerView: LetspickFantasyHeaderView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var joinButtonBackgroundView: UIView!
    @IBOutlet weak var lblJoinButtonTitle: UILabel!
    @IBOutlet weak var lblEntryFee: UILabel!
    //@IBOutlet var lblJoinedText: UILabel!
    @IBOutlet weak var lblTicketAvailable: UILabel!
    var isCoinsAvailable = false

    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    lazy var winnersArray = Array<LeagueWinnersRank>()
    lazy var bannerArray = Array<String>()

    var timer: Timer?
    lazy var isUserValidatingToJoinLeague = false
    var categoryName = ""
    lazy var isMatchClosingTimeRefereshing = false
    var userTicketsArray = Array<TicketDetails>()

    lazy var isViewForPrivateLeague = false
    var userTeamsArray = Array<UserTeamDetails>()
    var isHindiMode = false
    
    //MARK: View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppxorEventHandler.logAppEvent(withName: "ContestClicked", info: nil)
        setupDefaultProperties()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        if AppHelper.isApplicationRunningOnIphoneX() {
            bottomViewHeightConstraint.constant = 85.0
        }
    }
    
    // MARK:- Custom Methods
    private func setupDefaultProperties() {
        navigationController?.navigationBar.isHidden = true
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                isHindiMode = true
            }
        }
        lblEntry.text = "Entry".localized()
        headerView.isFantasyModeEnable = false
        AppHelper.designBottomTabDesing(bottomView)
        headerView.updateData(details: matchDetails)
        lblEntryFee.text = "pts" + AppHelper.makeCommaSeparatedDigitsWithString(digites: leagueDetails!.joiningAmount)
        headerView.updateMatchName(matchDetails: matchDetails)
        
        collectionView.register(UINib(nibName: "LeagueWinneraListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueWinneraListCollectionViewCell")
        collectionView.register(UINib(nibName: "LeaguePreviewDetailsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeaguePreviewDetailsCollectionViewCell")
        collectionView.register(UINib(nibName: "JackpotBannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JackpotBannerCollectionViewCell")
        
        callTotalWinnersRankAPI()

        if !isViewForPrivateLeague{
            userTeamsArray = UserDetails.sharedInstance.userTeamsArray
        }

        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            weak var weakSelf = self;
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }

        adjustButtonTitle()

        if leagueDetails != nil {
            bannerArray = leagueDetails!.bannerImages
        }
    }
    
    func showLeagueType() -> Int {
        
        var visibleLableCount = 0
        
        if (leagueDetails!.bonusApplicable == "2") && (leagueDetails!.teamType == "1") && (leagueDetails!.confirmedLeague == "2"){
            visibleLableCount = 3
        }
        else if (leagueDetails!.bonusApplicable == "2") && (leagueDetails!.teamType == "1"){
           visibleLableCount = 2
        }
        else if (leagueDetails!.teamType == "1") && (leagueDetails!.confirmedLeague == "2"){
            visibleLableCount = 2
        }
        else if (leagueDetails!.bonusApplicable == "2") && (leagueDetails!.confirmedLeague == "2"){
            visibleLableCount = 2
        }
        else if (leagueDetails!.bonusApplicable == "2"){
            visibleLableCount = 1
        }
        else if (leagueDetails!.teamType == "1"){
            visibleLableCount = 1
        }
        else if (leagueDetails!.confirmedLeague == "2"){
            visibleLableCount = 1
        }
        
        if (leagueDetails!.leagueType == "2"){
            if (leagueDetails!.confirmedLeague == "2"){
                visibleLableCount = 1
            }
            else{
                visibleLableCount = 0
            }
        }
        
        if (leagueDetails!.leagueType == "3"){
            visibleLableCount = 0
        }
        
        if visibleLableCount == 0{
            var cellHeight = 120
            
            if leagueDetails!.isJackpot == "1"{
                cellHeight =  135
            }
            else{
                cellHeight = 120
            }
            if isCoinsAvailable {
                cellHeight = cellHeight + 38
            }
            return cellHeight
        }
        else if visibleLableCount == 1{
            var cellHeight = 157

            if leagueDetails!.isJackpot == "1"{
                cellHeight = 172;
            }
            else{
                cellHeight = 157
            }
            
            if isCoinsAvailable {
                cellHeight = cellHeight + 38
            }
            return cellHeight
        }
        else if visibleLableCount == 2{
            var cellHeight = 190

            if leagueDetails!.isJackpot == "1"{
                cellHeight =  205;
            }
            else{
                cellHeight =  190
            }
            
            if isCoinsAvailable {
                cellHeight = cellHeight + 38
            }
            return cellHeight
        }
        else if visibleLableCount == 3{
            var cellHeight = 210

            if leagueDetails!.isJackpot == "1"{
                cellHeight =  225;
            }
            else{
                cellHeight =  210
            }
            
            if isCoinsAvailable {
                cellHeight = cellHeight + 38
            }
            return cellHeight
        }
        
        var cellHeight = 227

        if leagueDetails!.isJackpot == "1"{
            cellHeight = 215;
        }
        else{
            cellHeight = 200;
        }
        
        if isCoinsAvailable {
            cellHeight = cellHeight + 38
        }
        return cellHeight
    }
    
    func changeShadowAndBackgroundButtonView(color: UIColor)  {
        joinButtonBackgroundView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        joinButtonBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 3)
        joinButtonBackgroundView.layer.shadowOpacity = 1.0
        joinButtonBackgroundView.layer.shadowRadius = 10.0
        joinButtonBackgroundView.layer.masksToBounds = false
        joinButtonBackgroundView.layer.cornerRadius = 5.0;
        joinButtonBackgroundView.backgroundColor = color
        view.layoutIfNeeded()
    }
    
    func adjustButtonTitle() {
        var maxTeamAllow = 0;
        
        if leagueDetails!.fantasyType == "1"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBasketball;
        }
        else if leagueDetails!.fantasyType == "2"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBasketball;
        }else if leagueDetails!.fantasyType == "3"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBasketball;
        }
        
        lblTicketAvailable.isHidden = true;
        ticketIcon.isHidden = true;
//        lblJoinedText.isHidden = true
        rightArrow.isHidden = false
        lblJoinButtonTitle.isHidden = false
        if (leagueDetails!.teamType == "1") {
            if leagueDetails!.joinedLeagueCount == maxTeamAllow{
                lblJoinButtonTitle.text = "Joined".localized()
                lblJoinButtonTitle.text = "Joined".localized()
//                lblJoinedText.text = "Joined".localized()
//                lblJoinedText.isHidden = false
                if isHindiMode {
                    joinedViewWidthConstaint.constant = 150
                }
                rightArrow.isHidden = true
                lblJoinButtonTitle.isHidden = false
                changeShadowAndBackgroundButtonView(color: UIColor(red: 255.0/255, green: 97.0/255, blue: 106.0/255, alpha: 1));
            }
            else if leagueDetails!.joinedLeagueCount > 0{
                lblJoinButtonTitle.text = "Rejoin".localized()
                changeShadowAndBackgroundButtonView(color: UIColor(red: 255.0/255, green: 164.0/255, blue: 100.0/255, alpha: 1));
            }
            else{
                lblJoinButtonTitle.text = "Join".localized()
                changeShadowAndBackgroundButtonView(color: UIColor(red: 56.0/255, green: 154.0/255, blue: 254.0/255, alpha: 1));
            }
        }
        else if (leagueDetails!.teamType == "2") {
            if leagueDetails!.joinedLeagueCount > 0{
                lblJoinButtonTitle.text = "Joined".localized()
                lblJoinButtonTitle.text = "Joined".localized()
//                lblJoinedText.text = "Joined".localized()
//                lblJoinedText.isHidden = false
                rightArrow.isHidden = true
                lblJoinButtonTitle.isHidden = false
                if isHindiMode {
                    joinedViewWidthConstaint.constant = 150
                }

                changeShadowAndBackgroundButtonView(color: UIColor(red: 255.0/255, green: 97.0/255, blue: 106.0/255, alpha: 1));
                
            }
            else{
                lblJoinButtonTitle.text = "Join".localized()
                changeShadowAndBackgroundButtonView(color: UIColor(red: 56.0/255, green: 154.0/255, blue: 254.0/255, alpha: 1));
            }
        }
        else if (leagueDetails!.confirmedLeague == "2"){
            if leagueDetails!.joinedLeagueCount > 0{
                lblJoinButtonTitle.text = "Joined".localized()
                lblJoinButtonTitle.text = "Joined".localized()
//                lblJoinedText.text = "Joined".localized()
//                lblJoinedText.isHidden = false
                rightArrow.isHidden = true
                lblJoinButtonTitle.isHidden = false
                if isHindiMode {
                    joinedViewWidthConstaint.constant = 150
                }

                changeShadowAndBackgroundButtonView(color: UIColor(red: 255.0/255, green: 97.0/255, blue: 106.0/255, alpha: 1));
            }
            else{
                lblJoinButtonTitle.text = "Join".localized()
                changeShadowAndBackgroundButtonView(color: UIColor(red: 56.0/255, green: 154.0/255, blue: 254.0/255, alpha: 1));
            }
        }
        else{
            lblJoinButtonTitle.text = "Join".localized()
            changeShadowAndBackgroundButtonView(color: UIColor(red: 56.0/255, green: 154.0/255, blue: 254.0/255, alpha: 1));
        }
           
        for ticketDetails in userTicketsArray{
            if ticketDetails.ticketType != "2"{
                if (leagueDetails?.categoryId == ticketDetails.leagueCategory) && (ticketDetails.matchKey == leagueDetails?.matchKey) && (ticketDetails.joiningAmount == leagueDetails?.joiningAmount){
                    if ticketDetails.ticketType == "3" {
                        ticketIcon.image = UIImage(named: "PassIcon")
                    }
                    lblTicketAvailable.isHidden = false;
                    ticketIcon.isHidden = false;
                }
            }
            else{
                if (leagueDetails?.categoryId == ticketDetails.leagueCategory) && (ticketDetails.joiningAmount == leagueDetails?.joiningAmount){
                    lblTicketAvailable.isHidden = false;
                    ticketIcon.isHidden = false;
                }
            }
        }
        
        joinButtonBackgroundView.layoutIfNeeded()
        
    }

    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, ticketDetails:TicketDetails?)  {

        let joinedLeagueConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
        joinedLeagueConfirmVC?.leagueDetails = leagueDetails
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails
        joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
        joinedLeagueConfirmVC?.matchDetails = self.matchDetails
        joinedLeagueConfirmVC?.leagueCategoryName = categoryName
        joinedLeagueConfirmVC?.selectedGameType = GameType.Basketball.rawValue
        navigationController?.pushViewController(joinedLeagueConfirmVC!, animated: true)
    }
    
    //MARK:- -IBAction Methods
    @IBAction func joinButtonTapped(_ sender: Any) {
        
        var maxTeamAllow = 0;
        
        if leagueDetails!.fantasyType == "1"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBasketball;
        }
        else if leagueDetails!.fantasyType == "2"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBasketball;
        }else if leagueDetails!.fantasyType == "3"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBasketball;
        }

        if (leagueDetails?.teamType == "1") {
            if leagueDetails?.joinedLeagueCount == maxTeamAllow{
                return;
            }
        }
        else if (leagueDetails?.teamType == "1") {
            if leagueDetails!.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails?.confirmedLeague == "2"){
            if leagueDetails!.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails?.teamType == "2"){
            if leagueDetails!.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails?.teamType == "3"){
            if leagueDetails!.joinedLeagueCount > 0{
                return
            }
        }
        
        if isUserValidatingToJoinLeague {
            return;
        }
        callLeagueValidationAPI(leagueDetails: leagueDetails!)
    }
    
    @objc func updateTimerVlaue()  {
        
        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        if leagueDetails != nil {
            collectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
        }

        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
        }
        
    }
   
    // MARK:- API Related Method
    
    func callTotalWinnersRankAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "league_winners_v1","league_id": leagueDetails!.leagueId]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kBasketballMatchURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            if result != nil{
                let statusCode = result!["status"]
                
                if statusCode == "200"{

                    weakSelf?.winnersArray = LeagueWinnersRank.getAllRankDetails(response: result!)
                    
                    if weakSelf!.leagueDetails?.isInfinity == "1"{
                        if weakSelf!.leagueDetails?.leagueWinnerType == "dynamic_winner" {
                            let leagueRank = LeagueWinnersRank()
                            leagueRank.rankCount = "Top " + weakSelf!.leagueDetails!.totalWinnersPercent + "%"
                            leagueRank.winAmount = weakSelf!.leagueDetails!.winPerUser
                            weakSelf!.winnersArray.append(leagueRank)
                        }
                    }
                    else{
                        if weakSelf?.winnersArray.count == 0{
                            let winnerRank = LeagueWinnersRank()
                            winnerRank.winAmount = weakSelf?.leagueDetails?.winAmount
                            winnerRank.rankCount = "Rank".localized() + " : 1"
                            weakSelf?.winnersArray.append(winnerRank)
                        }
                    }
                    let filerArray = weakSelf!.winnersArray.filter { (rankDetails) -> Bool in
                        return Int(rankDetails.bbcoins) ?? 0 > 0
                    }
                    
                    if filerArray.count > 0 {
                        weakSelf?.isCoinsAvailable = true
                    }

                    weakSelf?.collectionView.reloadData()
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league_preview_v1", "check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType]
        isUserValidatingToJoinLeague = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kBasketballMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            weakSelf?.isUserValidatingToJoinLeague = false
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                                        
//                                        let addCashVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = weakSelf!.matchDetails
//                                        addCashVC?.userTeamArray = teamArray
//                                        addCashVC?.categoryName = weakSelf!.categoryName
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                    playerVC.matchDetails = weakSelf?.matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails, userTeamArray: teamArray, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                playerVC.matchDetails = weakSelf?.matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                
                            }
                        }
                        else{
                            let playerVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    // MARK:- Timer Handlers
    @objc func updateTimerValue()  {
        
        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
        }
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }

    //MARK:- CollectionView Data Source and Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if leagueDetails == nil{
            return 0
        }
        
        if bannerArray.count > 0 {
            return 3
        }
        
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        if indexPath.row == 0 {
            let height = showLeagueType();
            let size = CGSize(width: UIScreen.main.bounds.width, height: CGFloat(height))
            return size
        }
        else if (indexPath.row == 1) && (bannerArray.count != 0){
            let size = CGSize(width: UIScreen.main.bounds.width, height: 90)
            return size
        }
        else{
            var height = 260.0;
            if UIScreen.main.bounds.width <= 375 {
                height = 280.0;
            }

            if leagueDetails != nil {
            if (leagueDetails!.leagueMsg.count > 0){
                    height = height + 50.0;
                }
            }
            height = height + Double(winnersArray.count*50)

            let size = CGSize(width: UIScreen.main.bounds.width, height: CGFloat(height))
            return size
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeaguePreviewDetailsCollectionViewCell", for: indexPath) as? LeaguePreviewDetailsCollectionViewCell

            cell?.configData(leagueDetails: leagueDetails!, gameType: GameType.Basketball.rawValue, matchDetails: matchDetails!, isCoinApplicable: isCoinsAvailable)
            return cell!;
        }
        else if (indexPath.row == 1) && (bannerArray.count != 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JackpotBannerCollectionViewCell", for: indexPath) as? JackpotBannerCollectionViewCell
            cell?.configData(dataArray: bannerArray)
            return cell!;
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueWinneraListCollectionViewCell", for: indexPath) as? LeagueWinneraListCollectionViewCell
            cell?.configData(details: leagueDetails!, dataArray: winnersArray, isPreview: false, isQuiz: false)
            return cell!;
        }
    }
    
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}
