//
//  BasketTeamPreviewViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class BasketTeamPreviewViewController: UIViewController {
    
    @IBOutlet weak var pointGuardAspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet weak var shootingGuardViewCenterVertivacalConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var bottomSepView: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblPointGuard: UILabel!
    @IBOutlet weak var lblShootingGuard: UILabel!
    @IBOutlet weak var lblSmallForward: UILabel!
    @IBOutlet weak var lblPowerForward: UILabel!
    @IBOutlet weak var lblCenter: UILabel!

    @IBOutlet weak var pointGuardView: UIView!
    @IBOutlet weak var shootingGuardView: UIView!
    @IBOutlet weak var smallForwardView: UIView!
    @IBOutlet weak var powerForwardView: UIView!
    @IBOutlet weak var centerView: UIView!

    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var totalPointValue: UILabel!
    @IBOutlet weak var lblTotalPointTitle: UILabel!
    @IBOutlet weak var lblAnnounced: UILabel!

    lazy var totalPlayerArray = Array<PlayerDetails>()
    var selectedFantasy = FantasyType.Classic.rawValue
    
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var isMatchClosed = false
    var isShowPlayingRole = false
    var firstTeamName = ""
    var secondTeamName = ""
    var firstTeamkey = ""
    var secondTeamkey = ""
    var teamNumber = ""
    var userName = ""
    var isHideEditButton = false
    
    @IBOutlet weak var lblTeamNameTwo: UILabel!
    @IBOutlet weak var lblTeamNameOne: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTeamNameOne.text = firstTeamName
        lblTeamNameTwo.text = secondTeamName
        lblAnnounced.text = "ANNOUNCED".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isHideEditButton {
            editButton.isHidden = true
        }
        if isMatchClosed {
            lblTotalPointTitle.text = "Total Points".localized()
            editButton.isHidden = true
            lblTotalPointTitle.isHidden = false
            totalPointValue.isHidden = false

            if AppHelper.isApplicationRunningOnIphoneX() {
                bottomViewHeightConstraint.constant = 110
                shootingGuardViewCenterVertivacalConstraint.constant = -130
                pointGuardAspectRatioConstraint.constant = -98
                pointsView.layoutIfNeeded()
            }
            else{
                bottomViewHeightConstraint.constant = 70
            }
        }
        else{
            lblTotalPointTitle.text = "Credit Points".localized()
            if AppHelper.isApplicationRunningOnIphoneX() {
                bottomViewHeightConstraint.constant = 50
                shootingGuardViewCenterVertivacalConstraint.constant = -112
                pointGuardAspectRatioConstraint.constant = -98
                pointsView.layoutIfNeeded()
            }
            lblTotalPointTitle.isHidden = true
            totalPointValue.isHidden = true
            bottomSepView.isHidden = true
            view.layoutIfNeeded()
        }
        
        if selectedFantasy == FantasyType.Classic.rawValue {
            if teamNumber.count > 0{
                lblTeamName.text = "Team".localized() + " " + teamNumber
                if userName.count > 0{
                    lblUserName.text = userName
                }
                else{
                    lblUserName.text = UserDetails.sharedInstance.userName
                }
            }
            else{
                lblTeamName.text = "TeamPreview".localized()
            }
        }
                
        showPlayerBeforeMakingTeam()
    }
    
    func showPlayerBeforeMakingTeam() {

        let pointGuardArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.PointGuard.rawValue
        })
        
        let shootingGuardArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.ShootingGuard.rawValue
        })
        
        let smallForwardPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.SmallForward.rawValue
        })
        
        let powerForwardPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.PowerForward.rawValue
        })
        
        let centerPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.Center.rawValue
        })
        
        powerForwardView.isHidden = true
        shootingGuardView.isHidden = true
        smallForwardView.isHidden = true
        powerForwardView.isHidden = true
        centerView.isHidden = true
        
        if powerForwardPlayerArray.count != 0 {
            powerForwardView.isHidden = false
        }
        
        if shootingGuardArray.count != 0 {
            shootingGuardView.isHidden = false
        }
        
        if smallForwardPlayersArray.count != 0 {
            smallForwardView.isHidden = false
        }
        
        if powerForwardPlayerArray.count != 0 {
            powerForwardView.isHidden = false
        }
        
        if centerPlayerArray.count != 0 {
            centerView.isHidden = false
        }
        
        lblPointGuard.text = "Point-Guard".localized()
        lblShootingGuard.text = "Shooting-Guard".localized()
        lblSmallForward.text = "Small-Forward".localized()
        lblPowerForward.text = "Power-Forward".localized()
        lblCenter.text = "Center".localized()

        var verticalSpecting: CGFloat = 0
        if AppHelper.isApplicationRunningOnIphoneX() {
            verticalSpecting = 10
        }
        
        let viewWidth = UIScreen.main.bounds.width/5 - 8;
        var totalPoint: Float = 0
        
        if pointGuardArray.count == 0{
            pointGuardView.isHidden = true
        }
        else if pointGuardArray.count == 1{
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            let details = pointGuardArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: details, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            playerInfoView.selectedGameType = GameType.Basketball.rawValue
            pointGuardView.addSubview(playerInfoView)
        }
        else if pointGuardArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/1.0) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))

            let details1 = pointGuardArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = pointGuardArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)

            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue

            pointGuardView.addSubview(playerInfoView1)
            pointGuardView.addSubview(playerInfoView2)
        }
        else if pointGuardArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = pointGuardArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = pointGuardArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = pointGuardArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue

            pointGuardView.addSubview(playerInfoView1)
            pointGuardView.addSubview(playerInfoView2)
            pointGuardView.addSubview(playerInfoView3)
        }
        else if pointGuardArray.count == 4{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = pointGuardArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = pointGuardArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = pointGuardArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = pointGuardArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue
            playerInfoView4.selectedGameType = GameType.Basketball.rawValue

            pointGuardView.addSubview(playerInfoView1)
            pointGuardView.addSubview(playerInfoView2)
            pointGuardView.addSubview(playerInfoView3)
            pointGuardView.addSubview(playerInfoView4)
        }
        else if pointGuardArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + verticalSpecting) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = pointGuardArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = pointGuardArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = pointGuardArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = shootingGuardArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = pointGuardArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue
            playerInfoView4.selectedGameType = GameType.Basketball.rawValue
            playerInfoView5.selectedGameType = GameType.Basketball.rawValue

            pointGuardView.addSubview(playerInfoView1)
            pointGuardView.addSubview(playerInfoView2)
            pointGuardView.addSubview(playerInfoView3)
            pointGuardView.addSubview(playerInfoView4)
            pointGuardView.addSubview(playerInfoView5)
        }
        
        
        
        
        if shootingGuardArray.count == 0{
            shootingGuardView.isHidden = true
        }
        else if shootingGuardArray.count == 1{
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let deatails = shootingGuardArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView.selectedGameType = GameType.Basketball.rawValue

            shootingGuardView.addSubview(playerInfoView)
        }
        else if shootingGuardArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/1.0) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))

            let details1 = shootingGuardArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = shootingGuardArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue

            shootingGuardView.addSubview(playerInfoView1)
            shootingGuardView.addSubview(playerInfoView2)
        }
        else if shootingGuardArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = shootingGuardArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = shootingGuardArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = shootingGuardArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue

            shootingGuardView.addSubview(playerInfoView1)
            shootingGuardView.addSubview(playerInfoView2)
            shootingGuardView.addSubview(playerInfoView3)
        }
        else if shootingGuardArray.count == 4{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = shootingGuardArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = shootingGuardArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = shootingGuardArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = shootingGuardArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue
            playerInfoView4.selectedGameType = GameType.Basketball.rawValue

            shootingGuardView.addSubview(playerInfoView1)
            shootingGuardView.addSubview(playerInfoView2)
            shootingGuardView.addSubview(playerInfoView3)
            shootingGuardView.addSubview(playerInfoView4)
        }
        else if shootingGuardArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + verticalSpecting) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = shootingGuardArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = shootingGuardArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = shootingGuardArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = shootingGuardArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = shootingGuardArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue
            playerInfoView4.selectedGameType = GameType.Basketball.rawValue
            playerInfoView5.selectedGameType = GameType.Basketball.rawValue

            shootingGuardView.addSubview(playerInfoView1)
            shootingGuardView.addSubview(playerInfoView2)
            shootingGuardView.addSubview(playerInfoView3)
            shootingGuardView.addSubview(playerInfoView4)
            shootingGuardView.addSubview(playerInfoView5)
        }
        
        if smallForwardPlayersArray.count == 0{
            smallForwardView.isHidden = true
        }
        else if smallForwardPlayersArray.count == 1{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let deatails = smallForwardPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView.selectedGameType = GameType.Basketball.rawValue

            smallForwardView.addSubview(playerInfoView)
        }
        else if smallForwardPlayersArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/1.0) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = smallForwardPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = smallForwardPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            
            smallForwardView.addSubview(playerInfoView1)
            smallForwardView.addSubview(playerInfoView2)
        }
        else if smallForwardPlayersArray.count == 3{
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = smallForwardPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = smallForwardPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = smallForwardPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue

            smallForwardView.addSubview(playerInfoView1)
            smallForwardView.addSubview(playerInfoView2)
            smallForwardView.addSubview(playerInfoView3)
        }
        else if smallForwardPlayersArray.count == 4{
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = smallForwardPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = smallForwardPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = smallForwardPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = smallForwardPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue
            playerInfoView4.selectedGameType = GameType.Basketball.rawValue

            smallForwardView.addSubview(playerInfoView1)
            smallForwardView.addSubview(playerInfoView2)
            smallForwardView.addSubview(playerInfoView3)
            smallForwardView.addSubview(playerInfoView4)
        }
        else if smallForwardPlayersArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + verticalSpecting) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = smallForwardPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = smallForwardPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = smallForwardPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = smallForwardPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = smallForwardPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue
            playerInfoView4.selectedGameType = GameType.Basketball.rawValue
            playerInfoView5.selectedGameType = GameType.Basketball.rawValue

            smallForwardView.addSubview(playerInfoView1)
            smallForwardView.addSubview(playerInfoView2)
            smallForwardView.addSubview(playerInfoView3)
            smallForwardView.addSubview(playerInfoView4)
            smallForwardView.addSubview(playerInfoView5)
        }
        
        if powerForwardPlayerArray.count == 0{
            powerForwardView.isHidden = true
        }
        else if powerForwardPlayerArray.count == 1{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let deatails = powerForwardPlayerArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView.selectedGameType = GameType.Basketball.rawValue

            powerForwardView.addSubview(playerInfoView)
        }
        else if powerForwardPlayerArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/1.0) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))

            let details1 = powerForwardPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let details2 = powerForwardPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue

            powerForwardView.addSubview(playerInfoView1)
            powerForwardView.addSubview(playerInfoView2)
        }
        else if powerForwardPlayerArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let deatails1 = powerForwardPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: deatails1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = powerForwardPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = powerForwardPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue

            powerForwardView.addSubview(playerInfoView1)
            powerForwardView.addSubview(playerInfoView2)
            powerForwardView.addSubview(playerInfoView3)
        }
        else if powerForwardPlayerArray.count == 4{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = powerForwardPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = powerForwardPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = powerForwardPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = powerForwardPlayerArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue
            playerInfoView4.selectedGameType = GameType.Basketball.rawValue

            powerForwardView.addSubview(playerInfoView1)
            powerForwardView.addSubview(playerInfoView2)
            powerForwardView.addSubview(playerInfoView3)
            powerForwardView.addSubview(playerInfoView4)
        }
        
        if centerPlayerArray.count == 0{
            centerView.isHidden = true
        }
        else if centerPlayerArray.count == 1{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let deatails = centerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            playerInfoView.selectedGameType = GameType.Basketball.rawValue
            
            centerView.addSubview(playerInfoView)
        }
        else if centerPlayerArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/1.0) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))

            let details1 = centerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let details2 = centerPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue

            centerView.addSubview(playerInfoView1)
            centerView.addSubview(playerInfoView2)
        }
        else if centerPlayerArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let deatails1 = centerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: deatails1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = centerPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = centerPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue

            centerView.addSubview(playerInfoView1)
            centerView.addSubview(playerInfoView2)
            centerView.addSubview(playerInfoView3)
        }
        else if centerPlayerArray.count == 4{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 18, width: viewWidth, height: viewWidth + verticalSpecting))
            
            let details1 = centerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = centerPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = centerPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = centerPlayerArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            playerInfoView1.selectedGameType = GameType.Basketball.rawValue
            playerInfoView2.selectedGameType = GameType.Basketball.rawValue
            playerInfoView3.selectedGameType = GameType.Basketball.rawValue
            playerInfoView4.selectedGameType = GameType.Basketball.rawValue

            centerView.addSubview(playerInfoView1)
            centerView.addSubview(playerInfoView2)
            centerView.addSubview(playerInfoView3)
            centerView.addSubview(playerInfoView4)
        }
        totalPointValue.text = String(totalPoint)
    }

    
    @IBAction func closeButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let teamName = firstTeamName + " vs " + secondTeamName
        
        let text = String(format: "Check out my team for %@. Create your team on Letspick. Click here https://Letspick.app.link", teamName)
        
        // set up activity view controller
        let textToShare = [text, containerView.takeScreenshot()] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
}
