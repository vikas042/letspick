//
//  SelectBasketballPlayersViewController.swift
//  Letspick
//
//  Created by Vikash Rajput on 06/05/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class SelectBasketballPlayersViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var pointGuardIcon: UIImageView!
    @IBOutlet weak var shootingGuardIcon: UIImageView!
    @IBOutlet weak var smallForwardIcon: UIImageView!
    @IBOutlet weak var powerForwardIcon: UIImageView!
    @IBOutlet weak var centerIcon: UIImageView!

    @IBOutlet weak var lblPointGuardTitle: UILabel!
    @IBOutlet weak var lblPointGuardCount: UILabel!
    
    @IBOutlet weak var lblShootingGuardTitle: UILabel!
    @IBOutlet weak var lblShootingGuardCount: UILabel!
    
    @IBOutlet weak var lblSmallForwardTitle: UILabel!
    @IBOutlet weak var lblSmallForwardCount: UILabel!
    
    @IBOutlet weak var lblPowerForwardTitle: UILabel!
    @IBOutlet weak var lblPowerForwardCount: UILabel!

    @IBOutlet weak var lblCenterTitle: UILabel!
    @IBOutlet weak var lblCenterCount: UILabel!
    
    @IBOutlet weak var headerView: LetspickCreateTeamHeaderViewNew!
    @IBOutlet weak var sliderLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var pointsButton: UIButton!
    @IBOutlet weak var creditButton: UIButton!
    @IBOutlet weak var secondTeamButton: UIButton!
    @IBOutlet weak var firstTeamButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var previewButton: CustomBorderButton!
    @IBOutlet weak var nextButton: SolidButton!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottonView: UIView!
    @IBOutlet weak var lblPickMessage: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var inOutButton: UIButton!

    lazy var selectedPlayerType = PlayerType.PointGuard.rawValue

    lazy var totalPointGuardArray = Array<PlayerDetails>()
    lazy var totalShootingGuardArray = Array<PlayerDetails>()
    lazy var totalSmallForwardArray = Array<PlayerDetails>()
    lazy var totalPowerForwardArray = Array<PlayerDetails>()
    lazy var totalCenterArray = Array<PlayerDetails>()

    lazy var isEditPlayerTeam = false
    lazy var isPlaying22 = ""
    lazy var isMatchClosingTimeRefereshing = false
    lazy var playerSortedType = false
    lazy var teamSortedType = false
    lazy var pointsSortedType = true
    lazy var creditsSortedType = true
    lazy var isComeFromEditPlayerTeamScreen = false
    lazy var isPlayerPlaying = false
    
    var ticketDetails: TicketDetails?
    var totalPlayerArray: Array<PlayerDetails>?
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var userTeamDetails: UserTeamDetails?
    var timer: Timer?
    
    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.isViewForSelectCaptain = false
        setupDefaultProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails!.fantasyType, gameType: GameType.Basketball.rawValue)
        inOutButton.isHidden = true

        if self.selectedPlayerType == PlayerType.PointGuard.rawValue{
            pointGuardButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.ShootingGuard.rawValue{
            shootingGuardButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.SmallForward.rawValue{
            smallFordrdButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.PowerForward.rawValue{
            powerForwardButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.Center.rawValue{
            centerButtonTapped(nil)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerListContainerCollectionViewCell", for: indexPath) as? PlayerListContainerCollectionViewCell
        var playerList: Array<PlayerDetails>?
        
        if indexPath.row == 0 {
            playerList = totalPointGuardArray
        }
        else if indexPath.row == 1 {
            playerList = totalShootingGuardArray
        }
        else if indexPath.row == 2 {
            playerList = totalSmallForwardArray
        }
        else if indexPath.row == 3 {
            playerList = totalPowerForwardArray
        }
        else if indexPath.row == 4 {
            playerList = totalCenterArray
        }
        
        cell?.configData(playerList: playerList!, leagueType: FantasyType.Classic.rawValue, gameType: GameType.Basketball.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        
        return cell!;
    }
    
    // MARK:- API Related Method
    
    func callGetPlayerListAPI(urlString: String, isNeedToShowLoader: Bool)  {
        
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        
        weak var waekSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
                    DispatchQueue.main.async {
                        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
                        let response = savedResponse?.dictionary!["response"]
                        if let flagDict = response?.dictionary!["team_flags"]{
                            UserDetails.sharedInstance.teamFlag = flagDict;
                        }
                        
                        if let selectedmatch = response!.dictionary!["selected_match"]?.dictionary{
                            waekSelf?.isPlaying22 = selectedmatch["show_playing22"]?.string ?? "0"
                            if waekSelf?.isPlaying22 != "1"{
                                waekSelf?.inOutButton.isHidden = true
                            }
                            else{
                                waekSelf?.inOutButton.isHidden = false
                            }
                            
                            if waekSelf?.matchDetails?.firstTeamShortName?.count == 0{
                                let teamFirstName = selectedmatch["team_a_short_name"]?.string ?? ""

                                if let startTime = selectedmatch["start_date_unix"]?.string{
                                    var closingTime = selectedmatch["closing_ts"]?.intValue ?? 0
                                    if closingTime == 0{
                                        closingTime = UserDetails.sharedInstance.closingTimeForMatch
                                    }
                                    let calcultedTime = Int(startTime)! - closingTime
                                    waekSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                                }
                                let teamSecondShortName = selectedmatch["team_b_short_name"]?.string ?? ""
                                
                                if let firstTeamImageName = selectedmatch["team_a_flag"]?.string{
                                    waekSelf?.matchDetails?.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                                }
                                   
                                if let secondTeamImageName = selectedmatch["team_b_flag"]?.string{
                                    waekSelf?.matchDetails?.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                                }
                                
                                waekSelf?.matchDetails?.firstTeamShortName = teamFirstName
                                waekSelf?.matchDetails?.secondTeamShortName = teamSecondShortName
                                

                                waekSelf?.firstTeamButton.setTitle(teamFirstName, for: .normal)
                                waekSelf?.secondTeamButton.setTitle(teamSecondShortName, for: .normal)
                            }
                        }
                        
                        let playerList = response?.dictionary!["match_players"]?.array
                        waekSelf?.totalPlayerArray = PlayerDetails.getPlayerDetailsArray(responseArray: playerList!, matchDetails: self.matchDetails)
                        if waekSelf?.matchDetails?.playersGender == "F"{
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: waekSelf!.totalPlayerArray!)
                        }
                        if waekSelf?.matchDetails?.isMatchTourney ?? false{
                            waekSelf?.allButton.isHidden = true
                            waekSelf?.firstTeamButton.isHidden = true
                            waekSelf?.secondTeamButton.isHidden = true
                        }
                        waekSelf?.modifySelectedPlayerDetails()
                        waekSelf?.updateTimerVlaue()
                        waekSelf?.totalPointGuardArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 0)
                        waekSelf?.totalShootingGuardArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 1)
                        waekSelf?.totalSmallForwardArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 2)
                        waekSelf?.totalPowerForwardArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 3)
                        waekSelf?.totalCenterArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 4)

                        waekSelf?.collectionView.reloadData()
                        waekSelf?.upperView.isHidden = false
                        waekSelf?.bottonView.isHidden = false
                        waekSelf?.collectionView.isHidden = false
                        waekSelf?.inOutButtonTapped(nil)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    //MARK:- Custom Methods
    
    func setupDefaultProperties() {
        
        lblPointGuardTitle.text = "PG".localized()
        lblPowerForwardTitle.text = "PF".localized()
        lblSmallForwardTitle.text = "SF".localized()
        lblShootingGuardTitle.text = "SG".localized()
        lblCenterTitle.text = "Cen".localized()

        previewButton.setTitle("TeamPreview".localized(), for: .normal)
        nextButton.setTitle("Next".localized(), for: .normal)
        previewButton.setTitle("TeamPreview".localized(), for: .selected)
        nextButton.setTitle("Next".localized(), for: .selected)
        pointsButton.setTitle("Points".localized(), for: .normal)
        pointsButton.setTitle("Points".localized(), for: .selected)
        creditButton.setTitle("Credits".localized(), for: .normal)
        creditButton.setTitle("Credits".localized(), for: .selected)
        
        lblPointGuardCount.text = "0"
        lblShootingGuardCount.text = "0"
        lblSmallForwardCount.text = "0"
        lblPowerForwardCount.text = "0"
        lblCenterCount.text = "0"

        upperView.isHidden = true
        bottonView.isHidden = true
        collectionView.isHidden = true
        
        if matchDetails?.isMatchTourney ?? false{
            allButton.isHidden = true
            firstTeamButton.isHidden = true
            secondTeamButton.isHidden = true
        }

        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        
        firstTeamButton.setTitle(matchDetails!.firstTeamShortName, for: .normal)
        secondTeamButton.setTitle(matchDetails!.secondTeamShortName, for: .normal)
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails!.fantasyType, gameType: GameType.Basketball.rawValue)
        selectedPlayerType = PlayerType.PointGuard.rawValue
        AppHelper.designBottomTabDesing(bottonView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerSelectionUpdateNotification), name: NSNotification.Name(rawValue: "PlayerSelectionUpdateNotification"), object: nil)
        
        collectionView.register(UINib(nibName: "PlayerListContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayerListContainerCollectionViewCell")
        
        let urlString = kBasketballMatchURL + "?option=match_players&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.callGetPlayerListAPI(urlString: urlString, isNeedToShowLoader: true)
        }
        
        playerSelectionUpdateNotification()
        
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        for selectedDetails in UserDetails.sharedInstance.selectedPlayerList{
            for details in totalPlayerArray!{
                
                if details.playerKey == selectedDetails.playerKey{
                    details.isSelected = selectedDetails.isSelected
                    selectedDetails.playerPlayingRole = details.playerPlayingRole
                    selectedDetails.bowlingPoints = details.bowlingPoints
                    selectedDetails.isPlayerPlaying = details.isPlayerPlaying
                    selectedDetails.battingPoints = details.battingPoints
                    selectedDetails.classicPoints = details.classicPoints
                    selectedDetails.teamShortName = details.teamShortName
                    break;
                }
            }
        }
        
        if selectedRow == 0{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.PointGuard.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.ShootingGuard.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.SmallForward.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.PowerForward.rawValue
            })
        }
        else if selectedRow == 4{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                print(playerDetails.seasonalRole)
                print(playerDetails.seasonalRole == PlayerType.Center.rawValue)

                return playerDetails.seasonalRole == PlayerType.Center.rawValue
            })
        }

        if playersArray != nil{
            return playersArray!
        }
        return []
    }
    
    @objc func playerSelectionUpdateNotification()  {
        
        headerView.updatePlayerSelectionData(matchDetails: matchDetails!, fantasyType: leagueDetails!.fantasyType)
        if leagueDetails?.fantasyType == "1" {
            if UserDetails.sharedInstance.selectedPlayerList.count == 8 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }

        getSelectedPlayerCounts()
    }

    
    func getSelectedPlayerCounts(){
        
        let totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        if totalPlayerArray.count == 1 {
            AppxorEventHandler.logAppEvent(withName: "AddPlayer", info: ["SportType": "Basketball"])
        }

        let pointGuardPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.PointGuard.rawValue
        })
        
        let shootingGuardPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.ShootingGuard.rawValue
        })
        
        let smallForwardPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.SmallForward.rawValue
        })
        
        let powerForwardPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.PowerForward.rawValue
        })
        
        let centerPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Center.rawValue
        })
        
        lblPointGuardCount.text = "(" + String(pointGuardPlayersArray.count) + ")"
        lblShootingGuardCount.text = "(" + String(shootingGuardPlayersArray.count) + ")"
        lblSmallForwardCount.text = "(" + String(smallForwardPlayersArray.count) + ")"
        lblPowerForwardCount.text = "(" + String(powerForwardPlayerArray.count) + ")"
        lblCenterCount.text = "(" + String(centerPlayerArray.count) + ")"
    }
    
    func modifySelectedPlayerDetails()  {
        
        if isEditPlayerTeam{
            for selectedPlayermDetails in UserDetails.sharedInstance.selectedPlayerList{
                for playermDetails in totalPlayerArray!{
                    if selectedPlayermDetails.playerKey == playermDetails.playerKey{
                        
                        selectedPlayermDetails.bowlingPoints = playermDetails.bowlingPoints
                        selectedPlayermDetails.battingPoints = playermDetails.battingPoints
                        selectedPlayermDetails.classicPoints = playermDetails.classicPoints
                        selectedPlayermDetails.teamShortName = playermDetails.teamShortName
                        break
                    }
                }
            }
        }
        playerSelectionUpdateNotification()
    }
    
    //MARK:- -IBAction Methods
    @IBAction func pointGuardButtonTapped(_ sender: Any?) {
        pointGuardSelected()
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func shootingGuardButtonTapped(_ sender: Any?) {
        shootingGuardSelected()
        let indexPath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func smallFordrdButtonTapped(_ sender: Any?) {
        smallForwardSelected()
        let indexPath = IndexPath(item: 2, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func powerForwardButtonTapped(_ sender: Any?) {
        powerForwardSelected()
        let indexPath = IndexPath(item: 3, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func centerButtonTapped(_ sender: Any?) {
        centerSelected()
        let indexPath = IndexPath(item: 4, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }

    
    func pointGuardSelected() {

      lblPickMessage.text = "Pick 1-4 Point Guard".localized()
      selectedPlayerType = PlayerType.PointGuard.rawValue
      AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Basketball", "Type": "PG"])
        
      lblPointGuardTitle.textColor = kPlayerTypeSelectedColor
      lblSmallForwardTitle.textColor = kPlayerTypeUnselectedColor
      lblPowerForwardTitle.textColor = kPlayerTypeUnselectedColor
      lblShootingGuardTitle.textColor = kPlayerTypeUnselectedColor
      lblCenterTitle.textColor = kPlayerTypeUnselectedColor

      lblPointGuardCount.textColor = kPlayerTypeSelectedColor
      lblShootingGuardCount.textColor = kPlayerTypeUnselectedColor
      lblSmallForwardCount.textColor = kPlayerTypeUnselectedColor
      lblPowerForwardCount.textColor = kPlayerTypeUnselectedColor
      lblCenterCount.textColor = kPlayerTypeUnselectedColor

      pointGuardIcon.image = UIImage(named: "PointGuradSelectedIcon")
      shootingGuardIcon.image = UIImage(named: "ShootingGuradIcon")
      smallForwardIcon.image = UIImage(named: "SmallForwardIcon")
      powerForwardIcon.image = UIImage(named: "PowerForwardIcon")
      centerIcon.image = UIImage(named: "CenterIcon")

        sliderLeadingConstraint.constant = lblPointGuardTitle.frame.origin.x - 26
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    func shootingGuardSelected()  {

        lblPickMessage.text = "Pick 1-4 Shooting-Guard".localized()
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Basketball", "Type": "SG"])

        selectedPlayerType = PlayerType.ShootingGuard.rawValue
        lblPointGuardTitle.textColor = kPlayerTypeUnselectedColor
        lblShootingGuardTitle.textColor = kPlayerTypeSelectedColor
        lblSmallForwardTitle.textColor = kPlayerTypeUnselectedColor
        lblPowerForwardTitle.textColor = kPlayerTypeUnselectedColor
        lblCenterTitle.textColor = kPlayerTypeUnselectedColor

        lblPointGuardCount.textColor = kPlayerTypeUnselectedColor
        lblShootingGuardCount.textColor = kPlayerTypeSelectedColor
        lblSmallForwardCount.textColor = kPlayerTypeUnselectedColor
        lblPowerForwardCount.textColor = kPlayerTypeUnselectedColor
        lblCenterCount.textColor = kPlayerTypeUnselectedColor

        pointGuardIcon.image = UIImage(named: "PointGuradIcon")
        shootingGuardIcon.image = UIImage(named: "ShootingGuradSelectedIcon")
        smallForwardIcon.image = UIImage(named: "SmallForwardIcon")
        powerForwardIcon.image = UIImage(named: "PowerForwardIcon")
        centerIcon.image = UIImage(named: "CenterIcon")
        sliderLeadingConstraint.constant = lblShootingGuardTitle.frame.origin.x - 26
        UIView.animate(withDuration: 0.2) {
          self.upperView.layoutIfNeeded()
        }
    }

    func smallForwardSelected()  {

        lblPickMessage.text = "Pick 1-4 Small-Forward".localized()
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Basketball", "Type": "SF"])

        selectedPlayerType = PlayerType.SmallForward.rawValue
        lblPointGuardTitle.textColor = kPlayerTypeUnselectedColor
        lblShootingGuardTitle.textColor = kPlayerTypeUnselectedColor
        lblSmallForwardTitle.textColor = kPlayerTypeSelectedColor
        lblPowerForwardTitle.textColor = kPlayerTypeUnselectedColor
        lblCenterTitle.textColor = kPlayerTypeUnselectedColor

        lblPointGuardCount.textColor = kPlayerTypeUnselectedColor
        lblShootingGuardCount.textColor = kPlayerTypeUnselectedColor
        lblSmallForwardCount.textColor = kPlayerTypeSelectedColor
        lblPowerForwardCount.textColor = kPlayerTypeUnselectedColor
        lblCenterCount.textColor = kPlayerTypeUnselectedColor

        pointGuardIcon.image = UIImage(named: "PointGuradIcon")
        shootingGuardIcon.image = UIImage(named: "ShootingGuradIcon")
        smallForwardIcon.image = UIImage(named: "SmallForwardSelectedSelected")
        powerForwardIcon.image = UIImage(named: "PowerForwardIcon")
        centerIcon.image = UIImage(named: "CenterIcon")
        
        
        sliderLeadingConstraint.constant = lblSmallForwardTitle.frame.origin.x - 26
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
  
    func powerForwardSelected() {
                
        lblPickMessage.text = "Pick 1-4 Power-Forward".localized()
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Basketball", "Type": "PF"])

        selectedPlayerType = PlayerType.PowerForward.rawValue

        lblPointGuardTitle.textColor = kPlayerTypeUnselectedColor
        lblShootingGuardTitle.textColor = kPlayerTypeUnselectedColor
        lblSmallForwardTitle.textColor = kPlayerTypeUnselectedColor
        lblPowerForwardTitle.textColor = kPlayerTypeSelectedColor
        lblCenterTitle.textColor = kPlayerTypeUnselectedColor

        lblPointGuardCount.textColor = kPlayerTypeUnselectedColor
        lblShootingGuardCount.textColor = kPlayerTypeUnselectedColor
        lblSmallForwardCount.textColor = kPlayerTypeUnselectedColor
        lblPowerForwardCount.textColor = kPlayerTypeSelectedColor
        lblCenterCount.textColor = kPlayerTypeUnselectedColor

        pointGuardIcon.image = UIImage(named: "PointGuradIcon")
        shootingGuardIcon.image = UIImage(named: "ShootingGuradIcon")
        smallForwardIcon.image = UIImage(named: "SmallForwardIcon")
        powerForwardIcon.image = UIImage(named: "PowerForwardSelectedIcon")
        centerIcon.image = UIImage(named: "CenterIcon")
        
        sliderLeadingConstraint.constant = lblPowerForwardTitle.frame.origin.x - 26
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    func centerSelected() {
               
       lblPickMessage.text = "Pick 1-4 Center".localized()
       AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Basketball", "Type": "Cen"])

       selectedPlayerType = PlayerType.Center.rawValue

       lblPointGuardTitle.textColor = kPlayerTypeUnselectedColor
       lblSmallForwardTitle.textColor = kPlayerTypeUnselectedColor
       lblPowerForwardTitle.textColor = kPlayerTypeUnselectedColor
       lblShootingGuardTitle.textColor = kPlayerTypeUnselectedColor
       lblCenterTitle.textColor = kPlayerTypeSelectedColor

       lblPointGuardCount.textColor = kPlayerTypeUnselectedColor
       lblShootingGuardCount.textColor = kPlayerTypeUnselectedColor
       lblSmallForwardCount.textColor = kPlayerTypeUnselectedColor
       lblPowerForwardCount.textColor = kPlayerTypeUnselectedColor
       lblCenterCount.textColor = kPlayerTypeSelectedColor

       pointGuardIcon.image = UIImage(named: "PointGuradIcon")
       shootingGuardIcon.image = UIImage(named: "ShootingGuradIcon")
       smallForwardIcon.image = UIImage(named: "SmallForwardIcon")
       powerForwardIcon.image = UIImage(named: "PowerForwardIcon")
       centerIcon.image = UIImage(named: "CenterSelectedIcon")
       
       sliderLeadingConstraint.constant = lblCenterTitle.frame.origin.x - 26
       UIView.animate(withDuration: 0.2) {
           self.upperView.layoutIfNeeded()
       }
    }
    
    @IBAction func teamPreviewButtonTapped(_ sender: Any) {
        if UserDetails.sharedInstance.selectedPlayerList.count == 0 {
            AppHelper.showAlertView(message: "Please select atleast one player", isErrorMessage: true)
            return
        }
        
        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Basketball"])
        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "BasketTeamPreviewViewController") as! BasketTeamPreviewViewController
        teamPreviewVC.totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        teamPreviewVC.isShowPlayingRole = true;
        teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
        teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
        teamPreviewVC.firstTeamkey = self.matchDetails!.firstTeamKey
        teamPreviewVC.secondTeamkey = self.matchDetails!.secondTeamKey

        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    @IBAction func creditButtonTapped(_ sender: Any){
        
        let pointGuardPlayerListArray = totalPointGuardArray
        let shootingGuardPlayerListArray = totalShootingGuardArray
        let smallForwardPlayerListArray = totalSmallForwardArray
        let powerForwardPlayerListArray = totalPowerForwardArray
        let centerPlayerListArray = totalCenterArray

        
        let sortedKeeperArray =  pointGuardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
                
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedShootingGuardArray =  shootingGuardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedSmallForwardArray =  smallForwardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedPowerForwardArray =  powerForwardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedCenterArray = centerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        if creditsSortedType {
            creditButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }
        else{
            creditButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }
        
        creditsSortedType = !creditsSortedType
        totalPointGuardArray = sortedKeeperArray
        totalShootingGuardArray = sortedShootingGuardArray
        totalSmallForwardArray = sortedSmallForwardArray
        totalPowerForwardArray = sortedPowerForwardArray
        totalCenterArray = sortedCenterArray

        collectionView.reloadData()
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

    }
    
    @IBAction func pointsButtonTapped(_ sender: Any){
        
        let pointGuardPlayerListArray = totalPointGuardArray
        let smallForwardPlayerListArray = totalSmallForwardArray
        let shootingGuardPlayerListArray = totalShootingGuardArray
        let powerForwardPlayerListArray = totalPowerForwardArray
        let centerPlayerListArray = totalCenterArray

        let sortedKeeperArray =  pointGuardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        let sortedShootingGuardArray =  shootingGuardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!

            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        let sortedSmallForwardArray =  smallForwardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        let sortedPowerForwardArray =  powerForwardPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        let sortedCenterArray = centerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
            }
            else{
                return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
            }
        })
        
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        if pointsSortedType {
            pointsButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }
        else{
            pointsButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }
        
        pointsSortedType = !pointsSortedType
        totalPointGuardArray = sortedKeeperArray
        totalShootingGuardArray = sortedShootingGuardArray
        totalSmallForwardArray = sortedSmallForwardArray
        totalPowerForwardArray = sortedPowerForwardArray
        totalCenterArray = sortedCenterArray

        collectionView.reloadData()
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

    }
    
    @IBAction func allTeamButtonTapped(_ sender: Any?) {
        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        
//        totalPointGuardArray = getSelectedTypePlayerList(selectedRow: 0)
//        totalShootingGuardArray = getSelectedTypePlayerList(selectedRow: 1)
//        totalSmallForwardArray = getSelectedTypePlayerList(selectedRow: 2)
//        totalPowerForwardArray = getSelectedTypePlayerList(selectedRow: 3)
//        totalCenterArray = getSelectedTypePlayerList(selectedRow: 4)
//

        sortPlayerBasedOnLineupsOut(isFromAll: true)
    }
    
    @IBAction func firstTeamButtonTapped(_ sender: Any) {
        firstTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        let pointGuardPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let shootingGuardPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let smallForwardPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let powerForwardPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)
        let centerPlayerListArray = getSelectedTypePlayerList(selectedRow: 4)

        
        let filteredKeeperArray = pointGuardPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredMidFielderArray = smallForwardPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredDefenderArray = powerForwardPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredForwardArray = shootingGuardPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
       
        let sortedCenterArray = centerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return playerDetails.isPlayerPlaying != isPlayerPlaying
        })

        totalPointGuardArray = filteredKeeperArray
        totalSmallForwardArray = filteredMidFielderArray
        totalPowerForwardArray = filteredDefenderArray
        totalShootingGuardArray = filteredForwardArray
        totalCenterArray = sortedCenterArray

        collectionView.reloadData()
    }
    
    @IBAction func secondTeamButtonTapped(_ sender: Any) {
        
        secondTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        let pointGuardPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let shootingGuardPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let smallForwardPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let powerForwardPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)
        let centerPlayerListArray = getSelectedTypePlayerList(selectedRow: 4)


        let filteredKeeperArray = pointGuardPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredMidFielderArray = smallForwardPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredDefenderArray = powerForwardPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredForwardArray = shootingGuardPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let sortedCenterArray = centerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return playerDetails.isPlayerPlaying != isPlayerPlaying
        })

        totalPointGuardArray = filteredKeeperArray
        totalPowerForwardArray = filteredDefenderArray
        totalSmallForwardArray = filteredMidFielderArray
        totalShootingGuardArray = filteredForwardArray
        totalCenterArray = sortedCenterArray

        collectionView.reloadData()
    }

    
    @IBAction func nextButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "NextButtonClicked", info: ["SportType": "Basketball"])
        if UserDetails.sharedInstance.selectedPlayerList.count != 8{
            AppHelper.showAlertView(message: "Please select 8 players", isErrorMessage: true)
            return;
        }
        
        let selectCaptionVC = storyboard?.instantiateViewController(withIdentifier: "SelectCaptainBasketballViewController") as! SelectCaptainBasketballViewController
        selectCaptionVC.matchDetails = matchDetails
        selectCaptionVC.leagueDetails = leagueDetails
        selectCaptionVC.isEditPlayerTeam = isEditPlayerTeam
        selectCaptionVC.userTeamDetails = userTeamDetails
        selectCaptionVC.ticketDetails = ticketDetails

        navigationController?.pushViewController(selectCaptionVC, animated: true)
    }
    
    @IBAction func inOutButtonTapped(_ sender: Any?) {
//        isPlayerPlaying = !isPlayerPlaying
        sortPlayerBasedOnLineupsOut(isFromAll: false)
    }
    
    func sortPlayerBasedOnLineupsOut(isFromAll: Bool) {
        

        if isFromAll {
            allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
            firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        }
        else{
            inOutButton.setTitleColor(UIColor(red: 60.0/255, green: 196.0/255, blue: 266.0/255, alpha: 1), for: .normal)
            secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        }
        
//        let pointGuardPlayerListArray = totalPointGuardArray
//        let powerForwardPlayerListArray = totalPowerForwardArray
//        let shootingGuardPlayerListArray = totalShootingGuardArray
//        let smallForwardPlayerListArray = totalSmallForwardArray
//        let centerPlayerListArray = totalCenterArray
//
        let pointGuardPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let shootingGuardPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let smallForwardPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let powerForwardPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)
        let centerPlayerListArray = getSelectedTypePlayerList(selectedRow: 4)
        
        
        
        let sortedpointGuardArray = pointGuardPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }
        
        let sortedPowerForwardArray = powerForwardPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }

        let sortedShootingGuardArray = shootingGuardPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }

        let sortedSmallForwardArray = smallForwardPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }

        let sortedCenterArray = centerPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }


//
//        let sortedpointGuardArray = pointGuardPlayerListArray.filter { (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedPowerForwardArray =  powerForwardPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedShootingGuardArray =  shootingGuardPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedSmallForwardArray =  smallForwardPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
//
//        let sortedCenterArray = centerPlayerListArray.filter{ (playerDetails) -> Bool in
//            return playerDetails.isPlayerPlaying == true
//        }
        
        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        totalPointGuardArray = sortedpointGuardArray
        totalShootingGuardArray = sortedShootingGuardArray
        totalSmallForwardArray = sortedSmallForwardArray
        totalPowerForwardArray = sortedPowerForwardArray
        totalCenterArray = sortedCenterArray

        collectionView.reloadData()
    }
    
    // MARK: - Timer Handler
    @objc func updateTimerVlaue()  {
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kBasketballMatchURL, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    
    //MARK:- Scroll View Delegates
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if collectionView == scrollView{
            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
            if Int(currentPage) == 0{
                pointGuardSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 1{
                shootingGuardSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 2{
                smallForwardSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 3{
                powerForwardSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 4{
                centerSelected()
                collectionView.reloadData()
            }
        }
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
