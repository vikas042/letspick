//
//  FootballFullFantasyViewController.swift
//  Letspick
//
//  Created by Mads Technologies on 19/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class FootballFullFantasyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var headerView: CustomNavigationBar!
    lazy var totalGoalKeeperArray = Array<PlayerDetails>()
    lazy var totalDefenderArray = Array<PlayerDetails>()
    lazy var totalForwardsArray = Array<PlayerDetails>()
    lazy var totalMidFielderArray = Array<PlayerDetails>()
    lazy var playerListArray = Array<PlayerDetails>()
    
    var fantasyType = ""
    var matchKey = ""
    var firstTeamName = ""
    var secondTeamName = ""
    var playersGender = ""
    var matchDetails: MatchDetails?

    @IBOutlet weak var teamTwoButton: UIButton!
    @IBOutlet weak var teamOneButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "FullFantasyTableViewCell", bundle: nil), forCellReuseIdentifier: "FullFantasyTableViewCell")
        headerView.headerTitle = firstTeamName + " vs " + secondTeamName
        teamTwoButton.setTitle(secondTeamName, for: .normal)
        teamOneButton.setTitle(firstTeamName, for: .normal)
        
        var subTitle = ""
            
        if matchDetails?.isMatchClosed ?? false {
            if matchDetails!.matchStatus == "completed"{
                subTitle = "Completed".localized()
            }
            else if matchDetails!.matchStatus == "started"{
                subTitle = "Live".localized()
            }
            else{
                subTitle = "Leagues Closed".localized()
            }
        }
        headerView.updateHeaderTitles(title: firstTeamName + " vs " + secondTeamName, subTitle: subTitle)

        callGetTeamPlayersDetails()
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        
        if selectedRow == 0{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.GoalKeeper.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.MidFielder.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = playerListArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Sticker.rawValue
            })
        }
        
        if playersArray != nil{
            return playersArray!
        }
        
        return []
    }
    
    
    private func callGetTeamPlayersDetails()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let parameters = ["option": "full_scoreboard", "match_key": matchKey, "team_number": "1", "user_id": UserDetails.sharedInstance.userID, "type": "", "fantasy_type": fantasyType]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kFootballSocrescardUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let playerArray = result!["response"]?.array{
                        
                        weakSelf?.playerListArray = PlayerDetails.getPlayerDetailsForScoreArray(responseArray: playerArray)
                        if self.playersGender == "F" {
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: weakSelf!.playerListArray)
                        }

                        weakSelf?.totalGoalKeeperArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 0)
                        weakSelf?.totalDefenderArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 1)
                        weakSelf?.totalMidFielderArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 2)
                        weakSelf?.totalForwardsArray = weakSelf!.getSelectedTypePlayerList(selectedRow: 3)
                        weakSelf?.tblView.reloadData()
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (totalGoalKeeperArray.count > 0) && totalDefenderArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            return 4
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 && totalMidFielderArray.count > 0{
            return 3
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 && totalForwardsArray.count > 0{
            return 3
        }
        else if totalDefenderArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            return 3
        }
        else if totalMidFielderArray.count > 0 && totalGoalKeeperArray.count > 0 && totalForwardsArray.count > 0{
            return 3
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 {
            return 2
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 {
            return 2
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardsArray.count > 0 {
            return 2
        }
        else if totalDefenderArray.count > 0 && totalMidFielderArray.count > 0{
            return 2
        }
        else if totalDefenderArray.count > 0 && totalForwardsArray.count > 0{
            return 2
        }
        else if totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            return 2
        }
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        label.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255, blue: 240.0/255, alpha: 1)
        label.text = getPlayerType(section: section)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.textColor = UIColor(red: 127.0/255.0, green: 132.0/255, blue: 134.0/255, alpha: 1)
        label.font = UIFont(name: "OpenSans-Semibold", size: 12)
        return label;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (totalGoalKeeperArray.count > 0) && totalDefenderArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalDefenderArray.count
            }
            else if section == 2 {
                return totalMidFielderArray.count
            }
            else if section == 3 {
                return totalForwardsArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalDefenderArray.count
            }
            else if section == 2 {
                return totalMidFielderArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalDefenderArray.count
            }
            else if section == 3 {
                return totalForwardsArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalMidFielderArray.count
            }
            else if section == 2 {
                return totalForwardsArray.count
            }
        }
        else if totalDefenderArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalDefenderArray.count
            }
            else if section == 1 {
                return totalMidFielderArray.count
            }
            else if section == 2 {
                return totalForwardsArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 {
            
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalDefenderArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 {
            
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalMidFielderArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardsArray.count > 0 {
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalForwardsArray.count
            }
            
        }
        else if totalDefenderArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalDefenderArray.count
            }
            else if section == 1 {
                return totalMidFielderArray.count
            }
        }
        else if totalDefenderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalDefenderArray.count
            }
            else if section == 1 {
                return totalForwardsArray.count
            }
        }
        else if totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalMidFielderArray.count
            }
            else if section == 1 {
                return totalForwardsArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0{
            return totalGoalKeeperArray.count
        }
        else if totalDefenderArray.count > 0{
            return totalDefenderArray.count
        }
        else if totalMidFielderArray.count > 0{
            return totalMidFielderArray.count
        }
        else if totalForwardsArray.count > 0{
            return totalForwardsArray.count
        }
        
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "FullFantasyTableViewCell") as? FullFantasyTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "FullFantasyTableViewCell") as? FullFantasyTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.contentView.backgroundColor = UIColor.white
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        cell?.configData(details: playerInfo, legueType: FantasyType.Classic.rawValue, gameType: GameType.Football.rawValue)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        let teamScoresView = FootballPointsScoreView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
        teamScoresView.configData(details: playerInfo)
        APPDELEGATE.window!.addSubview(teamScoresView)
        teamScoresView.showAnimation()
    }
    
    func getPlayerType(section: Int) -> String {
        if (totalGoalKeeperArray.count > 0) && totalDefenderArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized().localized()
                }
                return "Goal Keeper".localized().localized()
            }
            else if section == 1 {
                if totalDefenderArray.count > 1{
                    return "Defenders".localized()
                }
                return "Defender".localized()
            }
            else if section == 2 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                
                return "MidFielder".localized().localized()
            }
            else if section == 3 {
                if totalForwardsArray.count > 1{
                    return "Strikers".localized()
                }
                
                return "Striker".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized().localized()
                }
                return "Goal Keeper".localized().localized()
            }
            else if section == 1 {
                if totalDefenderArray.count > 1{
                    return "Defenders".localized()
                }
                return "Defender".localized()
            }
            else if section == 2 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                
                return "MidFielder".localized().localized()
            }
            
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized().localized()
                }
                return "Goal Keeper".localized().localized()
            }
            else if section == 1 {
                if totalDefenderArray.count > 1{
                    return "Defenders".localized()
                }
                return "Defender".localized()
            }
            else if section == 2 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                
                return "MidFielder".localized().localized()
            }
            
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized().localized()
                }
                return "Goal Keeper".localized().localized()
            }
            else if section == 1 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                
                return "MidFielder".localized().localized()
            }
            else if section == 2 {
                if totalForwardsArray.count > 1{
                    return "Strikers".localized()
                }
                
                return "Striker".localized()
            }
        }
        else if totalDefenderArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                if totalDefenderArray.count > 1{
                    return "Defenders".localized()
                }
                
                return "Defender".localized()
            }
            else if section == 1 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                return "MidFielder".localized().localized()
            }
            else if section == 2 {
                if totalForwardsArray.count > 1{
                    return "Strikers".localized()
                }
                return "Striker".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 {
            
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized().localized()
                }
                
                return "Goal Keeper".localized().localized()
            }
            else if section == 1 {
                if totalDefenderArray.count > 1{
                    return "Defenders".localized()
                }
                
                return "Defender".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 {
            
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized().localized()
                }
                
                return "Goal Keeper".localized().localized()
            }
            else if section == 1 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                return "MidFielder".localized().localized()
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardsArray.count > 0 {
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized().localized()
                }
                
                return "Goal Keeper".localized().localized()
            }
            else if section == 1 {
                if totalForwardsArray.count > 1{
                    return "Strikers".localized()
                }
                
                return "Striker".localized()
            }
        }
        else if totalDefenderArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                if totalDefenderArray.count > 1{
                    return "Defenders".localized()
                }
                
                return "Defender".localized()
            }
            else if section == 1 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                
                return "MidFielder".localized().localized()
            }
        }
        else if totalDefenderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                if totalDefenderArray.count > 1{
                    return "Defenders".localized()
                }
                
                return "Defender".localized()
            }
            else if section == 1 {
                if totalForwardsArray.count > 1{
                    return "Strikers".localized()
                }
                return "Striker".localized()
            }
        }
        else if totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                
                return "MidFielder".localized().localized()
            }
            else if section == 1 {
                if totalForwardsArray.count > 1{
                    return "Strikers".localized()
                }
                return "Striker".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0{
            if totalGoalKeeperArray.count > 1{
                return "Goal Keeper".localized().localized()
            }
            
            return "Goal Keeper".localized().localized()
        }
        else if totalDefenderArray.count > 0{
            if totalDefenderArray.count > 1{
                return "Defenders".localized()
            }
            return "Defender".localized()
        }
        else if totalMidFielderArray.count > 0{
            if totalMidFielderArray.count > 1{
                return "MidFielders".localized()
            }
            return "MidFielder".localized().localized()
        }
        else if totalForwardsArray.count > 0{
            if totalForwardsArray.count > 1{
                return "Strikers".localized()
            }
            return "Striker".localized()
        }
        return ""
    }
    
    
    func getPlayerDetails(section: Int, index: Int) -> PlayerDetails {
        if (totalGoalKeeperArray.count > 0) && totalDefenderArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalDefenderArray[index]
            }
            else if section == 2 {
                return totalMidFielderArray[index]
            }
            else if section == 3 {
                return totalForwardsArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalDefenderArray[index]
            }
            else if section == 2 {
                return totalMidFielderArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalDefenderArray[index]
            }
            else if section == 3 {
                return totalForwardsArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalMidFielderArray[index]
            }
            else if section == 2 {
                return totalForwardsArray[index]
            }
        }
        else if totalDefenderArray.count > 0 && totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalDefenderArray[index]
            }
            else if section == 1 {
                return totalMidFielderArray[index]
            }
            else if section == 2 {
                return totalForwardsArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefenderArray.count > 0 {
            
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalDefenderArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 {
            
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalMidFielderArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardsArray.count > 0 {
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalForwardsArray[index]
            }
            
        }
        else if totalDefenderArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalDefenderArray[index]
            }
            else if section == 1 {
                return totalMidFielderArray[index]
            }
        }
        else if totalDefenderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalDefenderArray[index]
            }
            else if section == 1 {
                return totalForwardsArray[index]
            }
        }
        else if totalMidFielderArray.count > 0 && totalForwardsArray.count > 0{
            if section == 0 {
                return totalMidFielderArray[index]
            }
            else if section == 1 {
                return totalForwardsArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0{
            return totalGoalKeeperArray[index]
        }
        else if totalDefenderArray.count > 0{
            return totalDefenderArray[index]
        }
        else if totalMidFielderArray.count > 0{
            return totalMidFielderArray[index]
        }
        else if totalForwardsArray.count > 0{
            return totalForwardsArray[index]
        }
        return PlayerDetails()
    }
}
