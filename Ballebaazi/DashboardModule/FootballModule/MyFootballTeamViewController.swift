//
//  MyFootballTeamViewController.swift
//  Letspick
//
//  Created by Mads Technologies on 19/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class MyFootballTeamViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var creatTeamBottomButton: SolidButton!
    @IBOutlet weak var scoreViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scoreView: TeamScoreView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var headerView: LetspickFantasyWithoutWalletHeaderView!
    @IBOutlet var noTeamView: UIView!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblDoNotHaveTeam: UILabel!
    @IBOutlet weak var createFirstTeamButton: SolidButton!
    @IBOutlet weak var lblCreateTeamMessage: UILabel!
    
    lazy var userTeamsArray = Array<UserTeamDetails>()
    var timer: Timer?
    var matchDetails: MatchDetails?
    var leagueDetails:LeagueDetails?
    lazy var selectedLeagueType = 0
    
    lazy var classicTeam = Array<UserTeamDetails>()
    var isFromJoinedLeague = false
    
    // MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        createFirstTeamButton.setTitle("Create First Team".localized(), for: .normal)
        creatTeamBottomButton.setTitle("Create Team".localized(), for: .normal)

        lblCreateTeamMessage.text = ""
        lblDoNotHaveTeam.text = "You don't have any team as of now.\nCreate your team to participte and earn."
        tblView.register(UINib(nibName: "MyFootballTableViewCell", bundle: nil), forCellReuseIdentifier: "MyFootballTableViewCell")
        headerView.isFantasyModeEnable = false
        
        if isFromJoinedLeague{
            creatTeamBottomButton.setTitle("FullFantasyScore".localized(), for: .normal)
        }
        
        classicTeam = userTeamsArray.filter { (teamDetails) -> Bool in
            teamDetails.fantasyType == "1"
        }
        
        headerView.updateMatchName(details: matchDetails)
        
        if userTeamsArray.count == 0 {
            tblView.isHidden = true
            creatTeamBottomButton.isHidden = true;
        }
        
        if matchDetails!.isMatchClosed{
            scoreView.showTeamScore(details: matchDetails!)
        }
        showScoreView()
        updateTimerVlaue()
        classicButtonTapped();
        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK:- IBAction Methods
    @IBAction func createNewTeamButtonTapped(_ sender: Any) {
        if isFromJoinedLeague {
            let fullFantasyScoreboard = storyboard?.instantiateViewController(withIdentifier: "FootballFullFantasyViewController") as? FootballFullFantasyViewController
            fullFantasyScoreboard?.matchKey = matchDetails!.matchKey
            fullFantasyScoreboard?.matchDetails = matchDetails
            fullFantasyScoreboard?.playersGender = matchDetails!.playersGender
            fullFantasyScoreboard?.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            fullFantasyScoreboard?.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            navigationController?.pushViewController(fullFantasyScoreboard!, animated: true)
        }
        else{
            UserDetails.sharedInstance.selectedPlayerList.removeAll()
            let playerVC = storyboard?.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
            UserDetails.sharedInstance.selectedPlayerList.removeAll()
            playerVC.matchDetails = matchDetails
            if leagueDetails == nil{
                let leagueInfo = LeagueDetails()
                leagueInfo.fantasyType = "1"
                playerVC.leagueDetails = leagueInfo
            }
            else{
                playerVC.leagueDetails = leagueDetails
            }
            navigationController?.pushViewController(playerVC, animated: true);
        }
    }
    
    
    func classicButtonTapped() {
        let maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForFootball;
        lblCreateTeamMessage.text = "Create your team of 11 players. Make upto " + String(maxTeamAllow) + " teams and win more."

        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                lblCreateTeamMessage.text = "11 खिलाड़ियों की अपनी टीम बनाएं।  " + String(maxTeamAllow) + " टीमें बनाएं और अधिक जीतें"
            }
            else{
                lblCreateTeamMessage.text = "Create your team of 11 players. Make upto " + String(maxTeamAllow) + " teams and win more."
            }
        }
        
        
        if classicTeam.count >= maxTeamAllow{
            creatTeamBottomButton.isHidden = true
        }
        if classicTeam.count == 0{
            tblView.isHidden = true
            noTeamView.isHidden = false
            creatTeamBottomButton.isHidden = true
            
            if matchDetails!.isMatchClosed  {
                lblDoNotHaveTeam.text = "You don't have any team as of now."
                lblCreateTeamMessage.isHidden = true
                creatTeamBottomButton.isHidden = true
            }
        }
        else{
            
            tblView.isHidden = false
            noTeamView.isHidden = true
            
            if matchDetails!.isMatchClosed  {
                if isFromJoinedLeague{
                    creatTeamBottomButton.setTitle("FullFantasyScore".localized(), for: .normal)
                    creatTeamBottomButton.isHidden = false
                }
                else{
                    creatTeamBottomButton.isHidden = true
                }
            }
            else{
                creatTeamBottomButton.isHidden = false
                if classicTeam.count >= maxTeamAllow{
                    creatTeamBottomButton.isHidden = true
                }
            }
            tblView.layoutIfNeeded()
            view.layoutIfNeeded()
        }
        
        if classicTeam.count > 1{
            headerView.backButtonTitle = "My Teams".localized()
        }
        else{
            headerView.backButtonTitle = "My Team".localized()
        }

        tblView.reloadData()
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let blankView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        
        blankView.backgroundColor = UIColor.clear
        return blankView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classicTeam.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "MyFootballTableViewCell") as? MyFootballTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "MyFootballTableViewCell") as? MyFootballTableViewCell
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        let details = classicTeam[indexPath.row]
        let teamCount = classicTeam.count;
        
        if leagueDetails == nil{
            leagueDetails = LeagueDetails()
            leagueDetails?.fantasyType = "1"
        }
        
        cell?.configDetails(details: details, selectedMatchDetails: matchDetails, selectedLeagueDetails: leagueDetails, teamCount: teamCount)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if matchDetails!.isMatchClosed {
            let details = classicTeam[indexPath.row]
            let teamNumber = details.teamNumber ?? ""
            callGetFullScoreTeamPlayersDetails(userID: UserDetails.sharedInstance.userID, teamNumber: teamNumber, userName: UserDetails.sharedInstance.userName)
        }
        else{
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Football"])
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let teamPlayerList = storyboard.instantiateViewController(withIdentifier: "FootballTeamPreviewViewController") as! FootballTeamPreviewViewController
            let details = classicTeam[indexPath.row]
            teamPlayerList.totalPlayerArray = details.playersArray!
            teamPlayerList.teamNumber = details.teamNumber ?? ""
            
            teamPlayerList.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPlayerList.secondTeamName = matchDetails!.secondTeamShortName ?? ""
           
            teamPlayerList.firstTeamkey = self.matchDetails!.firstTeamKey
            teamPlayerList.secondTeamkey = self.matchDetails!.secondTeamKey

            navigationController?.pushViewController(teamPlayerList, animated: true)
        }
    }
    
    func callGetFullScoreTeamPlayersDetails(userID: String, teamNumber: String, userName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let type = "user_team"
        
        let parameters = ["option": "full_scoreboard", "match_key": matchDetails!.matchKey, "team_number": teamNumber,"user_id": userID, "type": type, "fantasy_type": "1"]
        
        WebServiceHandler.performPOSTRequest(urlString: kFootballSocrescardUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let playerArray = result!["response"]?.array{
                        let playerListArray = PlayerDetails.getPlayerDetailsForScoreArray(responseArray: playerArray)
                        if self.matchDetails!.playersGender == "F" {
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: playerListArray)
                        }

                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Football"])
                        let teamPlayerList = storyboard.instantiateViewController(withIdentifier: "FootballTeamPreviewViewController") as! FootballTeamPreviewViewController
                        teamPlayerList.isMatchClosed = true
                        teamPlayerList.totalPlayerArray = playerListArray
                        teamPlayerList.firstTeamName = self.matchDetails!.firstTeamShortName ?? ""
                        teamPlayerList.teamNumber = teamNumber
                        teamPlayerList.userName = userName
                        
                        teamPlayerList.firstTeamkey = self.matchDetails!.firstTeamKey
                        teamPlayerList.secondTeamkey = self.matchDetails!.secondTeamKey

                        
                        teamPlayerList.secondTeamName = self.matchDetails!.secondTeamShortName ?? ""
                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.pushViewController(teamPlayerList, animated: true)
                            
                        }
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    @objc func updateTimerVlaue()  {
        
        if (matchDetails == nil) || (UserDetails.sharedInstance.serverTimeStemp == "0") || (UserDetails.sharedInstance.serverTimeStemp.count == 0) {
            return;
        }
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(details: matchDetails!)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
        }
    }
    
    func showScoreView() {
        if (matchDetails?.firstTeamLiveScore.count != 0) && (matchDetails?.secondTeamLiveScore.count != 0) && (matchDetails?.thirdTeamLiveScore.count != 0){
            self.scoreViewHeightConstraint.constant = 110
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        else  if (matchDetails?.firstTeamLiveScore.count != 0) && (matchDetails?.secondTeamLiveScore.count != 0) && (matchDetails?.fourthTeamLiveScore.count != 0){
            self.scoreViewHeightConstraint.constant = 110
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        else if (matchDetails?.firstTeamLiveScoreShortName.count == 0) && (matchDetails?.secondTeamLiveScoreShortName.count == 0){
            self.scoreViewHeightConstraint.constant = 0
            self.scoreView.isHidden = true
            self.scoreView.layoutIfNeeded()
        }
        else{
            self.scoreViewHeightConstraint.constant = 50.0
            self.scoreView.isHidden = false
            self.scoreView.layoutIfNeeded()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
