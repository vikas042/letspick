//
//  SelectFootballPlayersViewController.swift
//  Letspick
//
//  Created by Mads Technologies on 19/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class SelectFootballPlayersViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var goalKeeperIcon: UIImageView!
    @IBOutlet weak var midFielderIcon: UIImageView!
    @IBOutlet weak var defenderIcon: UIImageView!
    @IBOutlet weak var forwardIcon: UIImageView!
    
    @IBOutlet weak var lblGoalKeerTitle: UILabel!
    @IBOutlet weak var lblGoalKeerCount: UILabel!

    @IBOutlet weak var lblDefenderTitle: UILabel!
    @IBOutlet weak var lblDefenderCount: UILabel!
    @IBOutlet weak var lblMidFielderTitle: UILabel!
    @IBOutlet weak var lblMidFielderCount: UILabel!
    
    @IBOutlet weak var lblForwardTitle: UILabel!
    @IBOutlet weak var lblForwardCount: UILabel!
    
    @IBOutlet weak var headerView: LetspickCreateTeamHeaderViewNew!
    @IBOutlet weak var sliderLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var pointsButton: UIButton!
    @IBOutlet weak var creditButton: UIButton!
    @IBOutlet weak var secondTeamButton: UIButton!
    @IBOutlet weak var firstTeamButton: UIButton!
    @IBOutlet weak var allButton: UIButton!

    @IBOutlet weak var previewButton: CustomBorderButton!
    @IBOutlet weak var nextButton: SolidButton!

    @IBOutlet weak var upperView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottonView: UIView!
    @IBOutlet weak var lblPickMessage: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var inOutButton: UIButton!

    
    lazy var selectedPlayerType = PlayerType.GoalKeeper.rawValue
    var totalPlayerArray: Array<PlayerDetails>?
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var userTeamDetails: UserTeamDetails?
    var timer: Timer?
    lazy var isEditPlayerTeam = false
    lazy var isPlaying22 = ""
    lazy var totalGoalKeeperArray = Array<PlayerDetails>()
    lazy var totalMidFielderArray = Array<PlayerDetails>()
    lazy var totalDefenderArray = Array<PlayerDetails>()
    lazy var totalStrickerArray = Array<PlayerDetails>()
    var isMatchClosingTimeRefereshing = false

    lazy var playerSortedType = false
    lazy var teamSortedType = false
    lazy var pointsSortedType = true
    lazy var creditsSortedType = true
    lazy var selectedLeagueType = 0
    lazy var isComeFromEditPlayerTeamScreen = false
    lazy var isPlayerPlaying = false
    var ticketDetails: TicketDetails?
    
    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.isViewForSelectCaptain = false
        setupDefaultProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails!.fantasyType, gameType: GameType.Football.rawValue)
        inOutButton.isHidden = true

        
        if self.selectedPlayerType == PlayerType.MidFielder.rawValue{
            self.midFielderButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.Defender.rawValue{
            self.defenderButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.Sticker.rawValue{
            self.forwardButtonTapped(nil)
        }
        else if self.selectedPlayerType == PlayerType.GoalKeeper.rawValue{
            self.goalKeeperButtonTapped(nil)
            
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Collection View Data Source and Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerListContainerCollectionViewCell", for: indexPath) as? PlayerListContainerCollectionViewCell
        var playerList: Array<PlayerDetails>?
        
        if indexPath.row == 0 {
            playerList = totalGoalKeeperArray
        }
        else if indexPath.row == 1 {
            playerList = totalStrickerArray
        }
        else if indexPath.row == 2 {
            playerList = totalMidFielderArray
        }
        else if indexPath.row == 3 {
            playerList = totalDefenderArray
        }
        
        if leagueDetails?.fantasyType == "1" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Classic.rawValue, gameType: GameType.Football.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        else if leagueDetails?.fantasyType == "2" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Batting.rawValue, gameType: GameType.Football.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        else if leagueDetails?.fantasyType == "3" {
            cell?.configData(playerList: playerList!, leagueType: FantasyType.Bowling.rawValue, gameType: GameType.Football.rawValue, mathDetails: matchDetails!, isPlaying22: isPlaying22)
        }
        
        return cell!;
    }
    
    // MARK:- API Related Method
    
    func callGetPlayerListAPI(urlString: String, isNeedToShowLoader: Bool)  {
        
        if isNeedToShowLoader {
            if !AppHelper.isInterNetConnectionAvailable(){
                return;
            }
            AppHelper.sharedInstance.displaySpinner()
        }
        
        weak var waekSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result?.dictionary!["status"]?.string
                if statusCode == "200" {
                    DispatchQueue.main.async {
                        let savedResponse = AppHelper.getValueFromCoreData(urlString: urlString)
                        let response = savedResponse?.dictionary!["response"]
                        if let flagDict = response?.dictionary!["team_flags"]{
                            UserDetails.sharedInstance.teamFlag = flagDict;
                        }
                        
                        if let selectedmatch = response!.dictionary!["selected_match"]?.dictionary{
                            waekSelf?.isPlaying22 = selectedmatch["show_playing22"]?.string ?? "0"
                            if waekSelf?.isPlaying22 != "1"{
                                waekSelf?.inOutButton.isHidden = true
                            }
                            else{
                                waekSelf?.inOutButton.isHidden = false
                            }
                            
                            if waekSelf?.matchDetails?.firstTeamShortName?.count == 0{
                                let teamFirstName = selectedmatch["team_a_short_name"]?.string ?? ""
                                let teamSecondShortName = selectedmatch["team_b_short_name"]?.string ?? ""
                                waekSelf?.matchDetails?.firstTeamShortName = teamFirstName
                                waekSelf?.matchDetails?.secondTeamShortName = teamSecondShortName
                                if let firstTeamImageName = selectedmatch["team_a_flag"]?.string{
                                    waekSelf?.matchDetails?.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                                }
                                   
                                if let secondTeamImageName = selectedmatch["team_b_flag"]?.string{
                                    waekSelf?.matchDetails?.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                                }

                                waekSelf?.firstTeamButton.setTitle(teamFirstName, for: .normal)
                                waekSelf?.secondTeamButton.setTitle(teamSecondShortName, for: .normal)
                                if let startTime = selectedmatch["start_date_unix"]?.string{
                                    var closingTime = selectedmatch["closing_ts"]?.intValue ?? 0
                                    if closingTime == 0{
                                        closingTime = UserDetails.sharedInstance.closingTimeForMatch
                                    }
                                    let calcultedTime = Int(startTime)! - closingTime
                                    waekSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                                }
                            }
                        }
                        
                        let playerList = response?.dictionary!["match_players"]?.array
                        waekSelf?.totalPlayerArray = PlayerDetails.getPlayerDetailsArray(responseArray: playerList!, matchDetails: self.matchDetails)
                        if waekSelf?.matchDetails?.playersGender == "F"{
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: waekSelf!.totalPlayerArray!)
                        }
                        if waekSelf?.matchDetails?.isMatchTourney ?? false{
                            waekSelf?.allButton.isHidden = true
                            waekSelf?.firstTeamButton.isHidden = true
                            waekSelf?.secondTeamButton.isHidden = true
                        }
                        waekSelf?.modifySelectedPlayerDetails()
                        waekSelf?.updateTimerVlaue()
                        
                        waekSelf?.totalGoalKeeperArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 0)
                        waekSelf?.totalStrickerArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 1)
                        waekSelf?.totalMidFielderArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 2)
                        waekSelf?.totalDefenderArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 3)

                        waekSelf?.collectionView.reloadData()
                        waekSelf?.upperView.isHidden = false
                        waekSelf?.bottonView.isHidden = false
                        waekSelf?.collectionView.isHidden = false
                        waekSelf?.inOutButtonTapped(nil)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    //MARK:- Custom Methods
    
    func setupDefaultProperties() {
        
        lblGoalKeerTitle.text = "GK".localized()
        lblDefenderTitle.text = "DEF".localized()
        lblMidFielderTitle.text = "MID".localized()
        lblForwardTitle.text = "ST".localized()
        previewButton.setTitle("TeamPreview".localized(), for: .normal)
        nextButton.setTitle("Next".localized(), for: .normal)
        previewButton.setTitle("TeamPreview".localized(), for: .selected)
        nextButton.setTitle("Next".localized(), for: .selected)
        pointsButton.setTitle("Points".localized(), for: .normal)
        pointsButton.setTitle("Points".localized(), for: .selected)
        
        creditButton.setTitle("Credits".localized(), for: .normal)
        creditButton.setTitle("Credits".localized(), for: .selected)
        

        lblGoalKeerCount.text = "0"
        lblForwardCount.text = "0"
        lblMidFielderCount.text = "0"
        lblDefenderCount.text = "0"
        upperView.isHidden = true
        bottonView.isHidden = true
        collectionView.isHidden = true
        
        if matchDetails?.isMatchTourney ?? false{
            allButton.isHidden = true
            firstTeamButton.isHidden = true
            secondTeamButton.isHidden = true
        }

        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        
        firstTeamButton.setTitle(matchDetails!.firstTeamShortName, for: .normal)
        secondTeamButton.setTitle(matchDetails!.secondTeamShortName, for: .normal)
        headerView.setupDefaultProperties(matchDetails: matchDetails, fantasyType: leagueDetails!.fantasyType, gameType: GameType.Football.rawValue)
        
        if leagueDetails?.fantasyType == "1" {
            selectedLeagueType = FantasyType.Classic.rawValue;
            selectedPlayerType = PlayerType.GoalKeeper.rawValue
        }
        else if leagueDetails?.fantasyType == "2" {
            selectedLeagueType = FantasyType.Batting.rawValue;
            selectedPlayerType = PlayerType.MidFielder.rawValue
        }
        else if leagueDetails?.fantasyType == "3" {
            selectedLeagueType = FantasyType.Bowling.rawValue;
            selectedPlayerType = PlayerType.Defender.rawValue
        }
        
        AppHelper.designBottomTabDesing(bottonView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerSelectionUpdateNotification), name: NSNotification.Name(rawValue: "PlayerSelectionUpdateNotification"), object: nil)
        
        collectionView.register(UINib(nibName: "PlayerListContainerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayerListContainerCollectionViewCell")
        
        let urlString = kFootballMatchURL + "?option=match_players&season_key=" + matchDetails!.seasonKey! + "&match_key=" + matchDetails!.matchKey
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.callGetPlayerListAPI(urlString: urlString, isNeedToShowLoader: true)
        }
        
        playerSelectionUpdateNotification()
        
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        for selectedDetails in UserDetails.sharedInstance.selectedPlayerList{
            for details in totalPlayerArray!{
                
                if details.playerKey == selectedDetails.playerKey{
                    details.isSelected = selectedDetails.isSelected
                    selectedDetails.playerPlayingRole = details.playerPlayingRole
                    selectedDetails.bowlingPoints = details.bowlingPoints
                    selectedDetails.isPlayerPlaying = details.isPlayerPlaying
                    selectedDetails.battingPoints = details.battingPoints
                    selectedDetails.classicPoints = details.classicPoints
                    selectedDetails.teamShortName = details.teamShortName
                    break;
                }
            }
        }
        
        if selectedRow == 0{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.GoalKeeper.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Sticker.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.MidFielder.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = totalPlayerArray?.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
            })
        }
        if playersArray != nil{
            return playersArray!
        }
        return []
    }
    
    @objc func playerSelectionUpdateNotification()  {
        
        headerView.updatePlayerSelectionData(matchDetails: matchDetails!, fantasyType: leagueDetails!.fantasyType)
        if leagueDetails?.fantasyType == "1" {
            if UserDetails.sharedInstance.selectedPlayerList.count == 11 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }
        else if leagueDetails?.fantasyType == "2" {
            if UserDetails.sharedInstance.selectedPlayerList.count == 5 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }
        else{
            if UserDetails.sharedInstance.selectedPlayerList.count == 5 {
                nextButton.isEnabled = true
                nextButton.updateLayerProperties()
            }
            else{
                nextButton.isEnabled = false
                nextButton.updateLayerProperties()
            }
        }
        
        if leagueDetails?.fantasyType == "1" {
            getSelectedPlayerCounts()
        }
        else{
            if isComeFromEditPlayerTeamScreen{
                getSelectedPlayerCountsForMidFielderAndDefenderFantasy()
            }
            else{
                getSelectedPlayerCounts()
            }
        }
    }
    
    func getSelectedPlayerCountsForMidFielderAndDefenderFantasy(){
        
        let totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        if totalPlayerArray.count == 1 {
            AppxorEventHandler.logAppEvent(withName: "AddPlayer", info: ["SportType": "Football"])
        }

        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.GoalKeeper.rawValue
        })
        
        let MidFielderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.MidFielder.rawValue
        })
        
        let DefenderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.Defender.rawValue
        })
        
        let ForwardPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.Sticker.rawValue
        })
        
        lblGoalKeerCount.text = "(" + String(wicketPlayersArray.count) + ")"
        lblForwardCount.text = "(" + String(ForwardPlayerArray.count) + ")"
        lblMidFielderCount.text = "(" + String(MidFielderPlayersArray.count) + ")"
        lblDefenderCount.text = "(" + String(DefenderPlayersArray.count) + ")"
    }
    
    func getSelectedPlayerCounts(){
        
        let totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        if totalPlayerArray.count == 1 {
            AppxorEventHandler.logAppEvent(withName: "AddPlayer", info: ["SportType": "Football"])
        }

        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.GoalKeeper.rawValue
        })
        
        let MidFielderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.MidFielder.rawValue
        })
        
        let DefenderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
        })
        
        let ForwardPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Sticker.rawValue
        })
        
        
        lblGoalKeerCount.text = "(" + String(wicketPlayersArray.count) + ")"
        lblForwardCount.text = "(" + String(ForwardPlayerArray.count) + ")"
        lblMidFielderCount.text = "(" + String(MidFielderPlayersArray.count) + ")"
        lblDefenderCount.text = "(" + String(DefenderPlayersArray.count) + ")"
    }
    
    func modifySelectedPlayerDetails()  {
        
        if isEditPlayerTeam{
            for selectedPlayermDetails in UserDetails.sharedInstance.selectedPlayerList{
                for playermDetails in totalPlayerArray!{
                    if selectedPlayermDetails.playerKey == playermDetails.playerKey{
                        
                        selectedPlayermDetails.bowlingPoints = playermDetails.bowlingPoints
                        selectedPlayermDetails.battingPoints = playermDetails.battingPoints
                        selectedPlayermDetails.classicPoints = playermDetails.classicPoints
                        selectedPlayermDetails.teamShortName = playermDetails.teamShortName
                        break
                    }
                }
            }
        }
        playerSelectionUpdateNotification()
    }
    
    //MARK:- -IBAction Methods
    @IBAction func goalKeeperButtonTapped(_ sender: Any?) {
        
        goalKeeperSelected()
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func defenderButtonTapped(_ sender: Any?) {
        
        defenderSelected()
        let indexPath = IndexPath(item: 3, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    @IBAction func midFielderButtonTapped(_ sender: Any?) {
        
        midFielderSelected()
        let indexPath = IndexPath(item: 2, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    
    @IBAction func forwardButtonTapped(_ sender: Any?) {
        
        forwardSelected()
        
        let indexPath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
    func goalKeeperSelected()  {
        if selectedLeagueType == FantasyType.Classic.rawValue{
            lblPickMessage.text = "Pick 1 Goal-Keeper".localized()
        }
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Football", "Type": "GK"])

        selectedPlayerType = PlayerType.GoalKeeper.rawValue
        goalKeeperIcon.image = UIImage(named: "GoalkeeperSelected")
        midFielderIcon.image = UIImage(named: "MidfielderUnselected")
        forwardIcon.image = UIImage(named: "ForwardUnselected")
        defenderIcon.image = UIImage(named: "FootDefenderUnselected")
        
        lblGoalKeerTitle.textColor = kPlayerTypeSelectedColor
        lblMidFielderTitle.textColor = kPlayerTypeUnselectedColor
        lblDefenderTitle.textColor = kPlayerTypeUnselectedColor
        lblForwardTitle.textColor = kPlayerTypeUnselectedColor
        
        lblGoalKeerCount.textColor = kPlayerTypeSelectedColor
        lblMidFielderCount.textColor = kPlayerTypeUnselectedColor
        lblDefenderCount.textColor = kPlayerTypeUnselectedColor
        lblForwardCount.textColor = kPlayerTypeUnselectedColor
        
        sliderLeadingConstraint.constant = 0.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    func defenderSelected() {
        if selectedLeagueType == FantasyType.Classic.rawValue{
            lblPickMessage.text = "Pick 3-5 Defenders".localized()
        }
        
        selectedPlayerType = PlayerType.Defender.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Football", "Type": "DEF"])

        lblGoalKeerTitle.textColor = kPlayerTypeUnselectedColor
        lblMidFielderTitle.textColor = kPlayerTypeUnselectedColor
        lblDefenderTitle.textColor = kPlayerTypeSelectedColor
        lblForwardTitle.textColor = kPlayerTypeUnselectedColor
        
        lblGoalKeerCount.textColor = kPlayerTypeUnselectedColor
        lblMidFielderCount.textColor = kPlayerTypeUnselectedColor
        lblForwardCount.textColor = kPlayerTypeUnselectedColor
        lblDefenderCount.textColor = kPlayerTypeSelectedColor
        
        goalKeeperIcon.image = UIImage(named: "GoalkeeperUnselected")
        midFielderIcon.image = UIImage(named: "MidfielderUnselected")
        forwardIcon.image = UIImage(named: "ForwardUnselected")
        defenderIcon.image = UIImage(named: "FootDefenderSelected")
        
        sliderLeadingConstraint.constant = defenderIcon.frame.origin.x - 9.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    func midFielderSelected()  {
        if selectedLeagueType == FantasyType.Classic.rawValue{
            lblPickMessage.text = "Pick 3-5 MidFielders".localized()
        }
        
        selectedPlayerType = PlayerType.MidFielder.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Football", "Type": "MID"])

        lblGoalKeerTitle.textColor = kPlayerTypeUnselectedColor
        lblMidFielderTitle.textColor = kPlayerTypeSelectedColor
        lblDefenderTitle.textColor = kPlayerTypeUnselectedColor
        lblForwardTitle.textColor = kPlayerTypeUnselectedColor
        
        lblGoalKeerCount.textColor = kPlayerTypeUnselectedColor
        lblMidFielderCount.textColor = kPlayerTypeSelectedColor
        lblForwardCount.textColor = kPlayerTypeUnselectedColor
        lblDefenderCount.textColor = kPlayerTypeUnselectedColor
        
        goalKeeperIcon.image = UIImage(named: "GoalkeeperUnselected")
        midFielderIcon.image = UIImage(named: "MidfielderSelected")
        forwardIcon.image = UIImage(named: "ForwardUnselected")
        defenderIcon.image = UIImage(named: "FootDefenderUnselected")
        
        sliderLeadingConstraint.constant = midFielderIcon.frame.origin.x - 9.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    func forwardSelected() {
        if selectedLeagueType == FantasyType.Classic.rawValue{
            lblPickMessage.text = "Pick 1-3 Forwards".localized()
        }
        
        selectedPlayerType = PlayerType.Sticker.rawValue
        AppxorEventHandler.logAppEvent(withName: "PlayerTypeClicked", info: ["SportType": "Football", "Type": "ST"])

        forwardIcon.layer.borderColor = kPlayerSelectionMaxLimitColor.cgColor
        lblGoalKeerTitle.textColor = kPlayerTypeUnselectedColor
        lblMidFielderTitle.textColor = kPlayerTypeUnselectedColor
        lblDefenderTitle.textColor = kPlayerTypeUnselectedColor
        lblForwardTitle.textColor = kPlayerTypeSelectedColor
        
        lblGoalKeerCount.textColor = kPlayerTypeUnselectedColor
        lblMidFielderCount.textColor = kPlayerTypeUnselectedColor
        lblDefenderCount.textColor = kPlayerTypeUnselectedColor
        lblForwardCount.textColor = kPlayerTypeSelectedColor
        
        goalKeeperIcon.image = UIImage(named: "GoalkeeperUnselected")
        midFielderIcon.image = UIImage(named: "MidfielderUnselected")
        forwardIcon.image = UIImage(named: "ForwardSelected")
        defenderIcon.image = UIImage(named: "FootDefenderUnselected")
        
        sliderLeadingConstraint.constant = forwardIcon.frame.origin.x - 9.0
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    @IBAction func teamPreviewButtonTapped(_ sender: Any) {
        
        if UserDetails.sharedInstance.selectedPlayerList.count == 0 {
            AppHelper.showAlertView(message: "Please select atleast one player", isErrorMessage: true)
            return
        }
        
        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Football"])
        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "FootballTeamPreviewViewController") as! FootballTeamPreviewViewController
        teamPreviewVC.totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        teamPreviewVC.selectedFantasy = selectedLeagueType
        teamPreviewVC.isShowPlayingRole = true;
        
        if leagueDetails!.fantasyType == "1"{
            teamPreviewVC.selectedFantasy = FantasyType.Classic.rawValue
        }
        else if leagueDetails!.fantasyType == "2"{
            teamPreviewVC.selectedFantasy = FantasyType.Batting.rawValue
        }
        else if leagueDetails!.fantasyType == "3"{
            teamPreviewVC.selectedFantasy = FantasyType.Bowling.rawValue
        }
        
        teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
        teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
        teamPreviewVC.firstTeamkey = self.matchDetails!.firstTeamKey
        teamPreviewVC.secondTeamkey = self.matchDetails!.secondTeamKey

        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    @IBAction func creditButtonTapped(_ sender: Any){
        
        let GoalKeeperPlayerListArray = totalGoalKeeperArray
        let MidFielderPlayerListArray = totalMidFielderArray
        let strickerPlayerListArray = totalStrickerArray
        let DefenderPlayerListArray = totalDefenderArray
        
        let sortedKeeperArray =  GoalKeeperPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
                
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedMidFielderArray =  MidFielderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedStrickerArray =  strickerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        
        let sortedDefenderArray =  DefenderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if creditsSortedType{
                return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
            }
            else{
                return Float(nextPlayerDetails.credits)! > Float(playerDetails.credits)!
            }
        })
        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        if creditsSortedType {
            creditButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }
        else{
            creditButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }
        
        creditsSortedType = !creditsSortedType
        totalGoalKeeperArray = sortedKeeperArray
        totalMidFielderArray = sortedMidFielderArray
        totalDefenderArray = sortedDefenderArray
        totalStrickerArray = sortedStrickerArray
        
        collectionView.reloadData()
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

    }
    
    @IBAction func pointsButtonTapped(_ sender: Any){
        
        let GoalKeeperPlayerListArray = totalGoalKeeperArray
        let MidFielderPlayerListArray = totalMidFielderArray
        let strickerPlayerListArray = totalStrickerArray
        let DefenderPlayerListArray = totalDefenderArray
        
        let sortedKeeperArray =  GoalKeeperPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! > Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.bowlingPoints)!
                }
            }
            else{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! < Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! < Float(playerDetails.bowlingPoints)!
                }
            }
            
            return false
        })
        
        let sortedMidFielderArray =  MidFielderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! > Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.bowlingPoints)!
                }
            }
            else{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! < Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! < Float(playerDetails.bowlingPoints)!
                }
            }
            
            return false
        })
        
        
        let sortedStrickerArray =  strickerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! > Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.bowlingPoints)!
                }
            }
            else{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! < Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! < Float(playerDetails.bowlingPoints)!
                }
            }
            
            return false
        })
        
        let sortedDefenderArray =  DefenderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! > Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! > Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! > Float(playerDetails.bowlingPoints)!
                }
            }
            else{
                if selectedLeagueType == FantasyType.Classic.rawValue{
                    return Float(nextPlayerDetails.classicPoints)! < Float(playerDetails.classicPoints)!
                }
                else if selectedLeagueType == FantasyType.Batting.rawValue{
                    return Float(nextPlayerDetails.battingPoints)! < Float(playerDetails.battingPoints)!
                }
                else if selectedLeagueType == FantasyType.Bowling.rawValue{
                    return Float(nextPlayerDetails.bowlingPoints)! < Float(playerDetails.bowlingPoints)!
                }
            }
            
            return false
        })
        
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        if pointsSortedType {
            pointsButton.setImage(UIImage(named: "SortIcon"), for: .normal)
        }
        else{
            pointsButton.setImage(UIImage(named: "SortUpIcon"), for: .normal)
        }
        
        pointsSortedType = !pointsSortedType
        totalGoalKeeperArray = sortedKeeperArray
        totalMidFielderArray = sortedMidFielderArray
        totalDefenderArray = sortedDefenderArray
        totalStrickerArray = sortedStrickerArray
        
        collectionView.reloadData()
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

    }
    
    @IBAction func allTeamButtonTapped(_ sender: Any?) {
        allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        
//        totalGoalKeeperArray = getSelectedTypePlayerList(selectedRow: 0)
//        totalStrickerArray = getSelectedTypePlayerList(selectedRow: 1)
//        totalMidFielderArray = getSelectedTypePlayerList(selectedRow: 2)
//        totalDefenderArray = getSelectedTypePlayerList(selectedRow: 3)
        sortPlayerBasedOnLineupsOut(isFromAll: true)
        collectionView.reloadData()
    }
    
    @IBAction func firstTeamButtonTapped(_ sender: Any) {
        
        //            allButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        //            firstTeamButton.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        //            secondTeamButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        //
        
        firstTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        
        let GoalKeeperPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let strickerPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let MidFielderPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let DefenderPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)

        
        let filteredKeeperArray = GoalKeeperPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredMidFielderArray = MidFielderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredDefenderArray = DefenderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        let filteredForwardArray = strickerPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.firstTeamShortName
        }
        
        totalGoalKeeperArray = filteredKeeperArray
        totalMidFielderArray = filteredMidFielderArray
        totalDefenderArray = filteredDefenderArray
        totalStrickerArray = filteredForwardArray
        
        collectionView.reloadData()
    }
    
    @IBAction func secondTeamButtonTapped(_ sender: Any) {
        
        //            allButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        //            firstTeamButton.backgroundColor = UIColor(red: 238.0/255, green: 242.0/255, blue: 245.0/255, alpha: 1)
        //            secondTeamButton.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        //
        
        secondTeamButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
        firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        
        let GoalKeeperPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let strickerPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let MidFielderPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let DefenderPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)
        
        let filteredKeeperArray = GoalKeeperPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredMidFielderArray = MidFielderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredDefenderArray = DefenderPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        let filteredForwardArray = strickerPlayerListArray.filter { (playerDetails) -> Bool in
            return playerDetails.teamShortName == matchDetails!.secondTeamShortName
        }
        
        totalGoalKeeperArray = filteredKeeperArray
        totalDefenderArray = filteredDefenderArray
        totalMidFielderArray = filteredMidFielderArray
        totalStrickerArray = filteredForwardArray
        
        collectionView.reloadData()
    }

    
    @IBAction func nextButtonTapped(_ sender: Any) {
        AppxorEventHandler.logAppEvent(withName: "NextButtonClicked", info: ["SportType": "Football"])

        if leagueDetails?.fantasyType == "1" {
            if UserDetails.sharedInstance.selectedPlayerList.count < 11{
                AppHelper.showAlertView(message: "Please select 11 players", isErrorMessage: true)
                return;
            }
        }
        else if leagueDetails?.fantasyType == "2"{
            if UserDetails.sharedInstance.selectedPlayerList.count < 5{
                AppHelper.showAlertView(message: "Please select 5 players", isErrorMessage: true)
                return;
            }
        }
        else if leagueDetails?.fantasyType == "3"{
            if UserDetails.sharedInstance.selectedPlayerList.count < 5{
                AppHelper.showAlertView(message: "Please select 5 players", isErrorMessage: true)
                return;
            }
        }
        
        self.performSegue(withIdentifier: "SelectFootballCaptainsViewController", sender: nil)
    }
    
    @IBAction func inOutButtonTapped(_ sender: Any?) {
        sortPlayerBasedOnLineupsOut(isFromAll: false)
    }
    
    func sortPlayerBasedOnLineupsOut(isFromAll: Bool) {
        
//        if isPlaying22 != "1"{
//            allTeamButtonTapped(nil)
//            return
//        }

        if isFromAll {
            allButton.setTitleColor(UIColor(red: 56.0/255, green: 154.0/255, blue: 243.0/255, alpha: 1), for: .normal)
            firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            inOutButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        }
        else{
            inOutButton.setTitleColor(UIColor(red: 60.0/255, green: 196.0/255, blue: 266.0/255, alpha: 1), for: .normal)
            secondTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            firstTeamButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
            allButton.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        }


        let goalKeeperPlayerListArray = getSelectedTypePlayerList(selectedRow: 0)
        let strickerPlayerListArray = getSelectedTypePlayerList(selectedRow: 1)
        let midfielderPlayerListArray = getSelectedTypePlayerList(selectedRow: 2)
        let defenderPlayerListArray = getSelectedTypePlayerList(selectedRow: 3)

        let sortedKeeperArray = goalKeeperPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }
        
        let sortedDefenderArray = defenderPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }

        let sortedMidFielderArray = midfielderPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }

        let sortedStrickerArray = strickerPlayerListArray.sorted { (playerDetails, nextPlayerDetails) -> Bool in
            return playerDetails.isPlayerPlaying == true
        }

        pointsButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        creditButton.setImage(UIImage(named: "SortDefaultIcon"), for: .normal)
        
        totalGoalKeeperArray = sortedKeeperArray
        totalDefenderArray = sortedDefenderArray
        totalStrickerArray = sortedStrickerArray
        totalMidFielderArray = sortedMidFielderArray
        
        collectionView.reloadData()
    }
    
    // MARK: - Timer Handler
    @objc func updateTimerVlaue()  {
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kFootballMatchURL, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectFootballCaptainsViewController" {
            let selectCaptionVC = segue.destination as! SelectFootballCaptainsViewController
            selectCaptionVC.matchDetails = matchDetails
            selectCaptionVC.leagueDetails = leagueDetails
            selectCaptionVC.isEditPlayerTeam = isEditPlayerTeam
            selectCaptionVC.userTeamDetails = userTeamDetails
            selectCaptionVC.ticketDetails = ticketDetails

        }
    }
    
    //MARK:- Scroll View Delegates
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if collectionView == scrollView{
            let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
            if Int(currentPage) == 0{
                goalKeeperSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 1{
                forwardSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 2{
                midFielderSelected()
                collectionView.reloadData()
            }
            else if Int(currentPage) == 3{
                defenderSelected()
                collectionView.reloadData()
            }
        }
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
