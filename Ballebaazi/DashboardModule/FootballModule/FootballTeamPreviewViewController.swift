//
//  FootballTeamPreviewViewController.swift
//  Letspick
//
//  Created by Mads Technologies on 19/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class FootballTeamPreviewViewController: UIViewController {
    
    @IBOutlet weak var goalKeeperAspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet weak var batsmanViewCenterVertivacalConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var bottomSepView: UIView!

    @IBOutlet weak var lblDefender: UILabel!
    @IBOutlet weak var lblStricker: UILabel!
    @IBOutlet weak var lblMidFielder: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var batsManView: UIView!
    @IBOutlet weak var keeperView: UIView!
    @IBOutlet weak var bowlerView: UIView!
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var totalPointValue: UILabel!
    @IBOutlet weak var lblTotalPointTitle: UILabel!
    @IBOutlet weak var allRounderView: UIView!
    var totalPlayerArray = Array<PlayerDetails>()
    var selectedFantasy = FantasyType.Classic.rawValue
    
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblAnnounced: UILabel!
    
    var isMatchClosed = false
    var isShowPlayingRole = false
    var firstTeamName = ""
    var secondTeamName = ""
    
    var firstTeamkey = ""
    var secondTeamkey = ""

    var teamNumber = ""
    var userName = ""
    var isHideEditButton = false
    
    @IBOutlet weak var lblTeamNameTwo: UILabel!
    @IBOutlet weak var lblTeamNameOne: UILabel!

    @IBOutlet weak var lblGoalKeeper: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTeamNameOne.text = firstTeamName
        lblTeamNameTwo.text = secondTeamName
        lblAnnounced.text = "ANNOUNCED".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isHideEditButton {
            editButton.isHidden = true
        }
        if isMatchClosed {
            lblTotalPointTitle.text = "Total Points".localized()
            editButton.isHidden = true
            lblTotalPointTitle.isHidden = false
            totalPointValue.isHidden = false

            if AppHelper.isApplicationRunningOnIphoneX() {
                bottomViewHeightConstraint.constant = 110
                batsmanViewCenterVertivacalConstraint.constant = -40
                goalKeeperAspectRatioConstraint.constant = -98
                pointsView.layoutIfNeeded()
            }
            else{
                bottomViewHeightConstraint.constant = 70
            }
        }
        else{
            lblTotalPointTitle.text = "Credit Points".localized()
//            bottomViewConstraint.constant = 5
            if AppHelper.isApplicationRunningOnIphoneX() {
                bottomViewHeightConstraint.constant = 50
                batsmanViewCenterVertivacalConstraint.constant = -40
                goalKeeperAspectRatioConstraint.constant = -98
                pointsView.layoutIfNeeded()
            }
            lblTotalPointTitle.isHidden = true
            totalPointValue.isHidden = true
            bottomSepView.isHidden = true
            view.layoutIfNeeded()
        }
        
        if selectedFantasy == FantasyType.Classic.rawValue {
            if teamNumber.count > 0{
                lblTeamName.text = "Team".localized() + " " + teamNumber
                if userName.count > 0{
                    lblUserName.text = userName
                }
                else{
                    lblUserName.text = UserDetails.sharedInstance.userName
                }
            }
            else{
                lblTeamName.text = "TeamPreview".localized()
            }
        }
                
        if isShowPlayingRole {
            showPlayerBeforeMakingTeam()
        }
        else{
            showPlayerList()
        }
        
    }
    
    func showPlayerBeforeMakingTeam() {
        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.GoalKeeper.rawValue
        })
        
        let defenderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
        })
        
        let midFielderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.MidFielder.rawValue
        })
        
        let strikerPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Sticker.rawValue
        })

        lblDefender.text = "DEFENDER".localized()
        if defenderPlayersArray.count > 1 {
            lblDefender.text = "DEFENDERS".localized()
        }
        
        lblMidFielder.text = "MID FIELDER".localized()
        if midFielderPlayersArray.count > 1 {
            lblMidFielder.text = "MID FIELDERS".localized()
        }
        
        lblStricker.text = "STRIKER".localized()
        if strikerPlayerArray.count > 1 {
            lblStricker.text = "STRIKERS".localized()
        }
        
        lblGoalKeeper.text = "GOAL KEEPER".localized()
        if strikerPlayerArray.count > 1 {
            lblGoalKeeper.text = "GOAL KEEPERS".localized()
        }

        
        let viewWidth = UIScreen.main.bounds.width/5 - 8;
        var totalPoint: Float = 0
        
        if wicketPlayersArray.count == 0{
            keeperView.isHidden = true
        }
        else{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            let details = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: details, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            keeperView.addSubview(playerInfoView)
        }
        
        if defenderPlayersArray.count == 0{
            batsManView.isHidden = true
        }
        else if defenderPlayersArray.count == 1{
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView)
        }
        else if defenderPlayersArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
        }
        else if defenderPlayersArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = defenderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
        }
        else if defenderPlayersArray.count == 4{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = defenderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = defenderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            
        }
        else if defenderPlayersArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = defenderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = defenderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = defenderPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            batsManView.addSubview(playerInfoView5)
            
        }
        
        if midFielderPlayersArray.count == 0{
            bowlerView.isHidden = true
        }
        else if midFielderPlayersArray.count == 1{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView)
        }
        else if midFielderPlayersArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = midFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
        }
        else if midFielderPlayersArray.count == 3{
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = midFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = midFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
        }
        else if midFielderPlayersArray.count == 4{
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = midFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = midFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = midFielderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            
        }
        else if midFielderPlayersArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = midFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = midFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = midFielderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = midFielderPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            bowlerView.addSubview(playerInfoView5)
        }
        
        if strikerPlayerArray.count == 0{
            allRounderView.isHidden = true
        }
        else if strikerPlayerArray.count == 1{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = strikerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView)
        }
        else if strikerPlayerArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = strikerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let details2 = strikerPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            
        }
        else if strikerPlayerArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails1 = strikerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: deatails1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = strikerPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = strikerPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
        }
        
        totalPointValue.text = String(totalPoint)
        
    }
    
    func showPlayerList() {
        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.GoalKeeper.rawValue
        })
        
        let defenderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.Defender.rawValue
        })
        
        let midFielderPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.MidFielder.rawValue
        })
        
        let strikerPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            playerDetails.seasonalRole == PlayerType.Sticker.rawValue
        })
        
        lblDefender.text = "DEFENDER".localized()
        if defenderPlayersArray.count > 1 {
            lblDefender.text = "DEFENDERS".localized()
        }
        
        lblMidFielder.text = "MID FIELDER".localized()
        if midFielderPlayersArray.count > 1 {
            lblMidFielder.text = "MID FIELDERS".localized()
        }
        
        lblStricker.text = "STRIKER".localized()
        if strikerPlayerArray.count > 1 {
            lblStricker.text = "STRIKERS".localized()
        }
        
        lblGoalKeeper.text = "GOAL KEEPER".localized()
        if strikerPlayerArray.count > 1 {
            lblGoalKeeper.text = "GOAL KEEPERS".localized()
        }
        
        let viewWidth = UIScreen.main.bounds.width/5 - 8;
        var totalPoint: Float = 0
        
        if wicketPlayersArray.count == 0{
            keeperView.isHidden = true
        }
        else{
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            let deatails = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            keeperView.addSubview(playerInfoView)
        }
        
        if defenderPlayersArray.count == 0{
            batsManView.isHidden = true
        }
        else if defenderPlayersArray.count == 1{
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView)
        }
        else if defenderPlayersArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
        }
        else if defenderPlayersArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = defenderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
        }
        else if defenderPlayersArray.count == 4{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = defenderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = defenderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            
        }
        else if defenderPlayersArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = defenderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = defenderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = defenderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = defenderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = defenderPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            
            batsManView.addSubview(playerInfoView1)
            batsManView.addSubview(playerInfoView2)
            batsManView.addSubview(playerInfoView3)
            batsManView.addSubview(playerInfoView4)
            batsManView.addSubview(playerInfoView5)
            
        }
        
        
        if midFielderPlayersArray.count == 0{
            bowlerView.isHidden = true
        }
        else if midFielderPlayersArray.count == 1{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView)
        }
        else if midFielderPlayersArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = midFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
        }
        else if midFielderPlayersArray.count == 3{
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = midFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = midFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
        }
        else if midFielderPlayersArray.count == 4{
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = midFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = midFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = midFielderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            
        }
        else if midFielderPlayersArray.count == 5{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView4 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView5 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = midFielderPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = midFielderPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = midFielderPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails4 = midFielderPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: deatails4, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails5 = midFielderPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: deatails5, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            bowlerView.addSubview(playerInfoView5)
        }
        
        if strikerPlayerArray.count == 0{
            allRounderView.isHidden = true
        }
        else if strikerPlayerArray.count == 1{
            
            let playerInfoView = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails = strikerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView)
        }
        else if strikerPlayerArray.count == 2{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let details1 = strikerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let details2 = strikerPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            
        }
        else if strikerPlayerArray.count == 3{
            
            let playerInfoView1 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView2 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let playerInfoView3 = FootballPlayerPreviewView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 12))
            
            let deatails1 = strikerPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: deatails1, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails2 = strikerPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: deatails2, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            let deatails3 = strikerPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: deatails3, legueType: selectedFantasy, isMatchClosed: isMatchClosed, firstTeamkey: firstTeamkey)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
        }
        
        totalPointValue.text = String(totalPoint)
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let teamName = firstTeamName + " vs " + secondTeamName
        
        let text = String(format: "Check out my team for %@. Create your team on Letspick. Click here https://Letspick.app.link", teamName)
        
        // set up activity view controller
        let textToShare = [text, containerView.takeScreenshot()] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
}
