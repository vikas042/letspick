//
//  SelectFootballCaptainsViewController.swift
//  Letspick
//
//  Created by Mads Technologies on 19/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class SelectFootballCaptainsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var saveButton: SolidButton!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: LetspickCreateTeamHeaderView!
    
    @IBOutlet weak var teamPreview: CustomBorderButton!
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var userTeamDetails: UserTeamDetails?
    var timer: Timer?
    var captionDetails: PlayerDetails?
    var viceCaptionDetails: PlayerDetails?
    var ticketDetails: TicketDetails?
    
    lazy var isMatchClosingTimeRefereshing = false

    lazy var isEditPlayerTeam = false
    lazy var totalGoalKeeperArray = Array<PlayerDetails>()
    lazy var totalDefendersArray = Array<PlayerDetails>()
    lazy var totalMidFielderArray = Array<PlayerDetails>()
    lazy var totalForwardArray = Array<PlayerDetails>()
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "SelectCaptainTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectCaptainTableViewCell")
        setupProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        
        if AppHelper.isApplicationRunningOnIphoneX(){
            bottomViewHeightConstraint.constant = 85;
            bottomView.layoutIfNeeded()
        }
        
        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }
    }
    
    //MARK:- -IBAction Methods
    @IBAction func saveButtonTapped(_ sender: Any) {
        
        if (captionDetails == nil) {
            AppHelper.showAlertView(message: "Please select caption.", isErrorMessage: true)
            return;
        }
        else if (viceCaptionDetails == nil) {
            AppHelper.showAlertView(message: "Please select vice-caption.", isErrorMessage: true)
            return;
        }
        
        if isEditPlayerTeam {
            callUpdateTeamAPI()
        }
        else{
            callCreateTeamAPI()
        }
    }
    
    @IBAction func previewButtonTapped(_ sender: Any) {
        
        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "FootballTeamPreviewViewController") as! FootballTeamPreviewViewController
        teamPreviewVC.totalPlayerArray = UserDetails.sharedInstance.selectedPlayerList
        teamPreviewVC.isShowPlayingRole = true;
        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Football"])

        if leagueDetails!.fantasyType == "1" {
            teamPreviewVC.selectedFantasy = FantasyType.Classic.rawValue
        }
        else if leagueDetails!.fantasyType == "2" {
            teamPreviewVC.selectedFantasy = FantasyType.Batting.rawValue
        }
        else if leagueDetails!.fantasyType == "3" {
            teamPreviewVC.selectedFantasy = FantasyType.Bowling.rawValue
        }
        
        teamPreviewVC.firstTeamName = matchDetails!.firstTeamShortName ?? ""
        teamPreviewVC.secondTeamName = matchDetails!.secondTeamShortName ?? ""
        
        teamPreviewVC.firstTeamkey = self.matchDetails!.firstTeamKey
        teamPreviewVC.secondTeamkey = self.matchDetails!.secondTeamKey

        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    //MARK:- Custom Methods
    func setupProperties() {
        headerView.isViewForSelectCaptain = true
        AppHelper.designBottomTabDesing(bottomView)
        
        totalGoalKeeperArray = getSelectedTypePlayerList(selectedRow: 0)
        totalDefendersArray = getSelectedTypePlayerList(selectedRow: 1)
        totalMidFielderArray = getSelectedTypePlayerList(selectedRow: 2)
        totalForwardArray = getSelectedTypePlayerList(selectedRow: 3)
        teamPreview.setTitle("TeamPreview".localized(), for: .normal)
        saveButton.setTitle("Next".localized(), for: .normal)
        teamPreview.setTitle("TeamPreview".localized(), for: .selected)
        saveButton.setTitle("Next".localized(), for: .selected)
        

        let caption = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isCaption
        }
        
        let viceCaption = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.isViceCaption
        }
        
        if viceCaption.count > 0 {
            viceCaptionDetails = viceCaption[0]
        }
        
        if caption.count > 0 {
            captionDetails = caption[0]
        }
        headerView.setupDefaultPropertiesForCaptainVicecaptaion(fantasyType: leagueDetails?.fantasyType ?? "")

        headerView.updateCaptainAndViceCaptainName(captain: captionDetails, viceCaptain: viceCaptionDetails)
        
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            
            weak var wealSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                wealSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    
    //MARK:- Timer Handler
    @objc func updateTimerVlaue()  {
        
        if matchDetails!.isMatchClosed  {
            timer?.invalidate()
            timer = nil
            headerView.updateMatchName(matchDetails: matchDetails)
        }
        else {
            headerView.updateTimerValue(matchDetails: matchDetails!)
            let remainingTime = AppHelper.getMatchRemaingTime(startDateTimeStemp: matchDetails!.startDateTimestemp!)
            weak var weakSelf = self
            if remainingTime <= 5 {
                DispatchQueue.main.async {
                    weakSelf?.callMatchClosingStatus(isNeedToShowLoader: false)
                }
            }
        }
    }
    
    func callMatchClosingStatus(isNeedToShowLoader: Bool) {
        if isMatchClosingTimeRefereshing {
            return
        }
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        isMatchClosingTimeRefereshing = true
        weak var weakSelf = self

        WebServiceHandler.performPOSTRequest(urlString: kFootballMatchURL, andParameters: ["option": "match_close_info", "match_key": matchDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if isNeedToShowLoader{
                AppHelper.sharedInstance.removeSpinner()
            }
            weakSelf?.isMatchClosingTimeRefereshing = false
            if result != nil{
                let statusCode = result!["status"]?.string
                if statusCode == "200" {
                    if let response = result!["response"]?.dictionary {
                        let closeStatus = response["closed"]?.string ?? ""
                        
                        if let startTime = response["start_date_unix"]?.string{
                            var closingTime = response["closing_ts"]?.intValue ?? 0
                            if closingTime == 0{
                                closingTime = UserDetails.sharedInstance.closingTimeForMatch
                            }
                            let calcultedTime = Int(startTime)! - closingTime
                            weakSelf?.matchDetails?.startDateTimestemp = String(calcultedTime)
                        }
                        
                        if closeStatus == "1" {
                            weakSelf?.matchDetails?.isMatchClosed = true
                        }
                        else{
                            weakSelf?.matchDetails?.isMatchClosed = false
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (totalGoalKeeperArray.count > 0) && totalDefendersArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            return 4
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 && totalForwardArray.count > 0{
            return 3
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 && totalMidFielderArray.count > 0{
            return 3
        }
        else if totalDefendersArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            return 3
        }
        else if totalForwardArray.count > 0 && totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0{
            return 3
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 {
            return 2
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardArray.count > 0 {
            return 2
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 {
            return 2
        }
        else if totalDefendersArray.count > 0 && totalForwardArray.count > 0{
            return 2
        }
        else if totalDefendersArray.count > 0 && totalMidFielderArray.count > 0{
            return 2
        }
        else if totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            return 2
        }
        
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        
        label.text = getPlayerType(section: section)
        label.textAlignment = .center
        label.textColor = UIColor(red: 127.0/255.0, green: 132.0/255, blue: 134.0/255, alpha: 1)
        label.font = UIFont(name: "OpenSans-Semibold", size: 12)
        label.backgroundColor = UIColor(red: 235.0/255.0, green: 235.0/255, blue: 235.0/255, alpha: 1)
        return label;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (totalGoalKeeperArray.count > 0) && totalDefendersArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalForwardArray.count
            }
            else if section == 2 {
                return totalMidFielderArray.count
            }
            else if section == 3 {
                return totalDefendersArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 && totalForwardArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalForwardArray.count
            }
            else if section == 2 {
                return totalDefendersArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalMidFielderArray.count
            }
            else if section == 3 {
                return totalDefendersArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalForwardArray.count
            }
            else if section == 2 {
                return totalMidFielderArray.count
            }
        }
        else if totalDefendersArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalForwardArray.count
            }
            else if section == 1 {
                return totalMidFielderArray.count
            }
            else if section == 2 {
                return totalDefendersArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 {
            
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalDefendersArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardArray.count > 0 {
            
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalForwardArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 {
            if section == 0 {
                return totalGoalKeeperArray.count
            }
            else if section == 1 {
                return totalMidFielderArray.count
            }
            
        }
        else if totalDefendersArray.count > 0 && totalForwardArray.count > 0{
            if section == 0 {
                return totalForwardArray.count
            }
            else if section == 1 {
                return totalDefendersArray.count
            }
        }
        else if totalDefendersArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalMidFielderArray.count
            }
            else if section == 1 {
                return totalDefendersArray.count

            }
        }
        else if totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalForwardArray.count
            }
            else if section == 1 {
                return totalMidFielderArray.count
            }
        }
        else if totalGoalKeeperArray.count > 0{
            return totalGoalKeeperArray.count
        }
        else if totalDefendersArray.count > 0{
            return totalDefendersArray.count
        }
        else if totalForwardArray.count > 0{
            return totalForwardArray.count
        }
        else if totalMidFielderArray.count > 0{
            return totalMidFielderArray.count
        }
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SelectCaptainTableViewCell") as? SelectCaptainTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "SelectCaptainTableViewCell") as? SelectCaptainTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.captainButton.tag = indexPath.row
        cell?.viceCaptainButton.tag = indexPath.row
        cell?.contentView.backgroundColor = UIColor.white
        cell?.captainButton.addTarget(self, action: #selector(self.captainButtonTapped(button:)), for: .touchUpInside)
        cell?.viceCaptainButton.addTarget(self, action: #selector(self.viceCaptainButtonTapped(button:)), for: .touchUpInside)
        cell?.captainButton.backgroundColor = UIColor.white
        cell?.viceCaptainButton.backgroundColor = UIColor.white
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        cell?.viceCaptainButton.setTitleColor(UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1), for: .normal)
        cell?.captainButton.setTitleColor(UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1), for: .normal)
        cell?.contentView.backgroundColor = UIColor.white
        
        if playerInfo.playerKey == captionDetails?.playerKey {
            cell?.captainButton.backgroundColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1)
            cell?.captainButton.setTitleColor(UIColor.white, for: .normal)
            cell?.contentView.backgroundColor = UIColor(red: 247.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
            
        }
        
        if playerInfo.playerKey == viceCaptionDetails?.playerKey {
            cell?.viceCaptainButton.backgroundColor = UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1)
            cell?.viceCaptainButton.setTitleColor(UIColor.white, for: .normal)
            cell?.contentView.backgroundColor = UIColor(red: 247.0/255, green: 235.0/255, blue: 235.0/255, alpha: 1.0)
            
        }
        
        if leagueDetails!.fantasyType == "1" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Classic.rawValue, gameType: GameType.Football.rawValue)
        }
        else if leagueDetails!.fantasyType == "2" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Batting.rawValue, gameType: GameType.Football.rawValue)
        }
        else if leagueDetails!.fantasyType == "3" {
            cell?.configData(details: playerInfo, legueType: FantasyType.Bowling.rawValue, gameType: GameType.Football.rawValue)
        }
        
        return cell!
    }
    
    
    @objc func captainButtonTapped(button: UIButton) {
        guard let cell = button.superview?.superview as? SelectCaptainTableViewCell else {
            return // or fatalError() or whatever
        }
        
        guard let indexPath = tblView.indexPath(for: cell) else{
            return;
        }
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        if playerInfo.playerKey == viceCaptionDetails?.playerKey{
            playerInfo.isViceCaption = false
            viceCaptionDetails = nil
            //            AppHelper.showAlertView(message: "Captain and Vice-Captain can not be same.", isErrorMessage: true)
            //            return;
        }
        captionDetails = playerInfo
        AppxorEventHandler.logAppEvent(withName: "SelectCaptainClicked", info: ["SportType": "Football", "PlayerName": playerInfo.playerName ?? "", "PlayerID": playerInfo.playerKey ?? ""])

        for details in totalMidFielderArray {
            details.isCaption = false
        }
        
        for details in totalForwardArray {
            details.isCaption = false
        }
        
        for details in totalDefendersArray {
            details.isCaption = false
        }
        
        for details in totalGoalKeeperArray {
            details.isCaption = false
        }
        
        captionDetails?.isCaption = true
        captionDetails?.isViceCaption = false
        headerView.updateCaptainAndViceCaptainName(captain: captionDetails, viceCaptain: viceCaptionDetails)
        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }
        
        tblView.reloadData()
        
    }
    
    func getPlayerType(section: Int) -> String {
        if (totalGoalKeeperArray.count > 0) && totalDefendersArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized()
                }
                return "Goal Keeper".localized()
            }
            else if section == 1 {
                if totalForwardArray.count > 1{
                    return "Strikers".localized()
                }
                
                return "Striker".localized()
            }
            else if section == 2 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                
                return "MidFielder".localized()
            }
            else if section == 3 {
                if totalDefendersArray.count > 1{
                    return "Defenders".localized()
                }
                return "Defender".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 && totalForwardArray.count > 0{
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized()
                }
                return "Goal Keeper".localized()
            }
            else if section == 1 {
                if totalForwardArray.count > 1{
                    return "Strikers".localized()
                }
                
                return "Striker".localized()
            }
            else if section == 2 {
                if totalDefendersArray.count > 1{
                    return "Defenders".localized()
                }
                return "Defender".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized()
                }
                return "Goal Keeper".localized()
            }
            else if section == 1 {
                if totalForwardArray.count > 1{
                    return "Strikers".localized()
                }
                
                return "Striker".localized()
            }
            else if section == 2 {
                if totalDefendersArray.count > 1{
                    return "Defenders".localized()
                }
                return "Defender".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized()
                }
                return "Goal Keeper".localized()
            }
            else if section == 1 {
                if totalForwardArray.count > 1{
                    return "Strikers".localized()
                }
                
                return "Striker".localized()
            }
            else if section == 2 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                
                return "MidFielder".localized()
            }
        }
        else if totalDefendersArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                if totalForwardArray.count > 1{
                    return "Strikers".localized()
                }
                return "Striker".localized()
            }
            else if section == 1 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                return "MidFielder".localized()
            }
            else if section == 2 {
                if totalDefendersArray.count > 1{
                    return "Defenders".localized()
                }
                
                return "Defender".localized()
            }

        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 {
            
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized()
                }
                
                return "Goal Keeper".localized()
            }
            else if section == 1 {
                if totalDefendersArray.count > 1{
                    return "Defenders".localized()
                }
                
                return "Defender".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardArray.count > 0 {
            
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized()
                }
                
                return "Goal Keeper".localized()
            }
            else if section == 1 {
                if totalForwardArray.count > 1{
                    return "Strikers".localized()
                }
                return "Striker".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 {
            if section == 0 {
                if totalGoalKeeperArray.count > 1{
                    return "Goal Keeper".localized()
                }
                
                return "Goal Keeper".localized()
            }
            else if section == 1 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                
                return "MidFielder".localized()
            }
        }
        else if totalDefendersArray.count > 0 && totalForwardArray.count > 0{
            if section == 0 {
                if totalForwardArray.count > 1{
                    return "Strikers".localized()
                }
                
                return "Striker".localized()

            }
            else if section == 1 {
                if totalDefendersArray.count > 1{
                    return "Defenders".localized()
                }
                
                return "Defender".localized()
            }
        }
        else if totalDefendersArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                return "MidFielder".localized()
            }
            else if section == 1 {
                if totalDefendersArray.count > 1{
                    return "Defenders".localized()
                }
                
                return "Defender".localized()
            }

        }
        else if totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                if totalForwardArray.count > 1{
                    return "Strikers".localized()
                }
                
                return "Striker".localized()
            }
            else if section == 1 {
                if totalMidFielderArray.count > 1{
                    return "MidFielders".localized()
                }
                return "MidFielder".localized()
            }
        }
        else if totalGoalKeeperArray.count > 0{
            if totalGoalKeeperArray.count > 1{
                return "Goal Keeper".localized()
            }
            
            return "Goal Keeper".localized()
        }
        else if totalDefendersArray.count > 0{
            if totalDefendersArray.count > 1{
                return "Defenders".localized()
            }
            return "Defender".localized()
        }
        else if totalForwardArray.count > 0{
            if totalForwardArray.count > 1{
                return "Strikers".localized()
            }
            return "Striker".localized()
        }
        else if totalMidFielderArray.count > 0{
            if totalMidFielderArray.count > 1{
                return "MidFielders".localized()
            }
            return "MidFielder".localized()
        }
        return ""
    }
    
    func getPlayerDetails(section: Int, index: Int) -> PlayerDetails {
        if (totalGoalKeeperArray.count > 0) && totalDefendersArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalForwardArray[index]
            }
            else if section == 2 {
                return totalMidFielderArray[index]
            }
            else if section == 3 {
                return totalDefendersArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 && totalForwardArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalForwardArray[index]
            }
            else if section == 2 {
                return totalDefendersArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalMidFielderArray[index]
            }
            else if section == 2 {
                return totalDefendersArray[index]
            }

        }
        else if totalGoalKeeperArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalForwardArray[index]
            }
            else if section == 2 {
                return totalMidFielderArray[index]
            }
        }
        else if totalDefendersArray.count > 0 && totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalForwardArray[index]
            }
            else if section == 1 {
                return totalMidFielderArray[index]
            }
            else if section == 2 {
                return totalDefendersArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalDefendersArray.count > 0 {
            
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalDefendersArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalForwardArray.count > 0 {
            
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalForwardArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0 && totalMidFielderArray.count > 0 {
            if section == 0 {
                return totalGoalKeeperArray[index]
            }
            else if section == 1 {
                return totalMidFielderArray[index]
            }
            
        }
        else if totalDefendersArray.count > 0 && totalForwardArray.count > 0{
            if section == 0 {
                return totalForwardArray[index]
            }
            else if section == 1 {
                return totalDefendersArray[index]
            }
        }
        else if totalDefendersArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalMidFielderArray[index]
            }
            else if section == 1 {
                return totalDefendersArray[index]
            }
        }
        else if totalForwardArray.count > 0 && totalMidFielderArray.count > 0{
            if section == 0 {
                return totalForwardArray[index]
            }
            else if section == 1 {
                return totalMidFielderArray[index]
            }
        }
        else if totalGoalKeeperArray.count > 0{
            return totalGoalKeeperArray[index]
        }
        else if totalDefendersArray.count > 0{
            return totalDefendersArray[index]
        }
        else if totalForwardArray.count > 0{
            return totalForwardArray[index]
        }
        else if totalMidFielderArray.count > 0{
            return totalMidFielderArray[index]
        }
        return PlayerDetails()
    }
    
    @objc func viceCaptainButtonTapped(button: UIButton) {
        guard let cell = button.superview?.superview as? SelectCaptainTableViewCell else {
            return // or fatalError() or whatever
        }
        
        guard let indexPath = tblView.indexPath(for: cell) else{
            return;
        }
        
        let playerInfo = getPlayerDetails(section: indexPath.section, index: indexPath.row)
        
        if playerInfo.playerKey == captionDetails?.playerKey{
            playerInfo.isCaption = false
            captionDetails = nil
            //            AppHelper.showAlertView(message: "Captain and Vice-Captain can not be same.", isErrorMessage: true)
            //            return;
        }
        
        viceCaptionDetails = playerInfo
        AppxorEventHandler.logAppEvent(withName: "SelectViceCaptainClicked", info: ["SportType": "Football", "PlayerName": playerInfo.playerName ?? "", "PlayerID": playerInfo.playerKey ?? ""])

        for details in totalMidFielderArray {
            details.isViceCaption = false
        }
        
        for details in totalForwardArray {
            details.isViceCaption = false
        }
        
        for details in totalDefendersArray {
            details.isViceCaption = false
        }
        
        for details in totalGoalKeeperArray {
            details.isViceCaption = false
        }
        
        viceCaptionDetails?.isViceCaption = true
        viceCaptionDetails?.isCaption = false
        headerView.updateCaptainAndViceCaptainName(captain: captionDetails, viceCaptain: viceCaptionDetails)
        
        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveButton.isEnabled = true
            saveButton.updateLayerProperties()
        }
        else{
            saveButton.isEnabled = false
            saveButton.updateLayerProperties()
        }
        
        
        tblView.reloadData()
        
    }
    
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        
        if selectedRow == 0{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.GoalKeeper.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.MidFielder.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = UserDetails.sharedInstance.selectedPlayerList.filter({ (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Sticker.rawValue
            })
        }
        
        if playersArray != nil{
            return playersArray!
        }
        
        return []
    }
    
    
    //MARK:- API Releated Methods
    
    func callCreateTeamAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var playerIDs: String?
        let captionID = captionDetails!.playerKey ?? ""
        let viceCaptionID = viceCaptionDetails!.playerKey ?? ""
        
        for details in UserDetails.sharedInstance.selectedPlayerList {
            if playerIDs == nil{
                playerIDs = details.playerKey!
            }
            else{
                playerIDs = playerIDs! + "," + details.playerKey!
            }
        }
        
        let parameters = ["option": "create_team","captain": captionID, "vice_captain": viceCaptionID, "fantasy_type": leagueDetails!.fantasyType, "match_key": matchDetails!.matchKey, "players": playerIDs!, "user_id": UserDetails.sharedInstance.userID]
        
        weak var weakSelf = self;
        WebServiceHandler.performPOSTRequest(urlString: kFootballMatchURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if weakSelf?.leagueDetails?.matchKey.count == 0{
                        AppHelper.showToast(message: "Team Created Successfully!".localized())
                        
                        let navArray = weakSelf?.navigationController?.viewControllers
                        if navArray!.count > 1 {
                            if let leagueVC = navArray![1] as? FootballMoreLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    
                                    weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                                }
                            }
                            else if let leagueVC = navArray![1] as? FootballLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    
                                    if weakSelf?.leagueDetails?.fantasyType == "1"{
                                        leagueVC.headerView.classicButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "2"{
                                        leagueVC.headerView.battingButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "3"{
                                        leagueVC.headerView.bowlingButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "4"{
                                        leagueVC.headerView.reverseFantasyButtonTapped(isNeedToScroll: false)
                                    }
                                    else if weakSelf?.leagueDetails?.fantasyType == "5"{
                                        leagueVC.headerView.wizardFantasyButtonTapped(isNeedToScroll: false)
                                    }

                                    weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                                }
                            }
                            else if let joinedLeagueVC = navArray![1] as? FootballJoinedLeagueViewController{
                                DispatchQueue.main.async {
                                    AppHelper.sharedInstance.removeSpinner()
                                    
                                    if weakSelf?.leagueDetails?.fantasyType == "1"{
                                        joinedLeagueVC.headerView.classicButtonTapped(isNeedToScroll: false)
                                    }
                                    weakSelf?.navigationController?.popToViewController(joinedLeagueVC, animated: true)
                                }
                            }
                        }
                    }
                    else{
                        
                        if let response = result!["response"]{
                            let userTeam = response["user_team"]
                            if userTeam.count > 0{
                                let teamArray = UserTeamDetails.getUserTeamsArray(responseArray: [userTeam], matchDetails: weakSelf!.matchDetails!)
                                if teamArray.count > 0{
                                    weakSelf?.userTeamDetails = teamArray[0]
                                }
                            }
                        }
                        self.goToJoinLeagueConfirmation()
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    private func callUpdateTeamAPI()  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        var playerIDs: String?
        let captionID = captionDetails!.playerKey ?? ""
        let viceCaptionID = viceCaptionDetails!.playerKey ?? ""
        
        for details in UserDetails.sharedInstance.selectedPlayerList {
            if playerIDs == nil{
                playerIDs = details.playerKey!
            }
            else{
                playerIDs = playerIDs! + "," + details.playerKey!
            }
        }
        
        let parameters = ["option": "update_team","captain": captionID, "vice_captain": viceCaptionID, "fantasy_type": leagueDetails!.fantasyType, "match_key": matchDetails!.matchKey, "players": playerIDs!, "user_id": UserDetails.sharedInstance.userID, "team_number": userTeamDetails!.teamNumber!]
        
        WebServiceHandler.performPOSTRequest(urlString: kFootballMatchURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            weak var weakSelf = self;
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    AppHelper.showToast(message: "Team Updated Successfully!".localized())
                    
                    let navArray = weakSelf?.navigationController?.viewControllers
                    if navArray!.count > 1 {
                        if let leagueVC = navArray![1] as? FootballMoreLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                            }
                        }
                        else if let leagueVC = navArray![1] as? FootballLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                
                                if weakSelf?.leagueDetails?.fantasyType == "1"{
                                    leagueVC.classicButtonTapped(isAnimation: false)
                                }
                                weakSelf?.navigationController?.popToViewController(leagueVC, animated: true)
                            }
                        }
                        else if let joinedLeagueVC = navArray![1] as? FootballJoinedLeagueViewController{
                            DispatchQueue.main.async {
                                AppHelper.sharedInstance.removeSpinner()
                                
                                if weakSelf?.leagueDetails?.fantasyType == "1"{
                                    joinedLeagueVC.classicButtonTapped(isNeedToAnimate: false)
                                }
                                weakSelf?.navigationController?.popToViewController(joinedLeagueVC, animated: true)
                            }
                        }
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    // MARK: - Navigation
    
    func goToJoinLeagueConfirmation() {
        
        let joinLegueConfiramation = storyboard?.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as! JoinLeagueConfirmationViewController
        joinLegueConfiramation.matchDetails = matchDetails
        joinLegueConfiramation.ticketDetails = ticketDetails

        joinLegueConfiramation.leagueDetails = leagueDetails
        if userTeamDetails != nil {
            joinLegueConfiramation.userTeamsArray = [userTeamDetails!]
        }
        joinLegueConfiramation.selectedGameType = GameType.Football.rawValue
        self.navigationController?.pushViewController(joinLegueConfiramation, animated: true)
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
