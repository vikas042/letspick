//
//  LeagueContainerCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 19/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

protocol LeagueContainerCollectionViewCellDelegates {
    func pullToRefereshLeagues()
}

class LeagueContainerCollectionViewCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var delegate:LeagueContainerCollectionViewCellDelegates?
    
    var matchDetails: MatchDetails?
    lazy var isUserValidatingToJoinLeague = false
    var userTicketsArray = Array<TicketDetails>()

    lazy var categoryArray = Array<LeagueCategoryDetails>()
    @IBOutlet weak var lblStayTuned: UILabel!
    @IBOutlet weak var imgPlaceholder: UIImageView!
    @IBOutlet weak var leagueCollectionView: UICollectionView!
    
    var refreshControl: UIRefreshControl?
    
    var totalJoinedLeague = 0;
    var totalJoinedTeam = 0;
    var selectedFantasy = FantasyType.Classic.rawValue
    var selectedGameType = GameType.Cricket.rawValue
    
    @objc func refreshData(sender:AnyObject) {
        delegate?.pullToRefereshLeagues();
        refreshControl?.endRefreshing()
    }
    
    func configData(catArray: Array<LeagueCategoryDetails>, selectedMatchDetails: MatchDetails?, fantasyType: Int, ticketsArray: Array<TicketDetails>, isFilterApplied: Bool, gameType: Int) {
        selectedGameType = gameType
        selectedFantasy = fantasyType;
        userTicketsArray = ticketsArray
        if refreshControl == nil {
            refreshControl = UIRefreshControl()
            refreshControl?.tintColor = .clear
            refreshControl?.addTarget(self, action: #selector(refreshData(sender:)), for: UIControl.Event.valueChanged)
            leagueCollectionView.addSubview(refreshControl!)
        }
        
        categoryArray = catArray
        matchDetails = selectedMatchDetails
        leagueCollectionView.register(UINib(nibName: "LeagueCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueCollectionViewCell")
        leagueCollectionView.register(UINib(nibName: "SingleLeagueCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SingleLeagueCollectionViewCell")
        leagueCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        leagueCollectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionReusableView")
        leagueCollectionView.reloadData()
        leagueCollectionView.isHidden = false;
        lblStayTuned.text = "Stay_Tuned_Message".localized()
        
        if isFilterApplied{
            lblStayTuned.text = "We did not find any league available as per your selection. Try using some other filter".localized()
        }
        
        lblStayTuned.isHidden = true
        imgPlaceholder.isHidden = true
        if categoryArray.count == 0{
            let ismatchClosed = matchDetails?.isMatchClosed ?? false
            
            if !ismatchClosed {
                leagueCollectionView.isHidden = true;
                lblStayTuned.isHidden = false
                imgPlaceholder.isHidden = false
            }
        }
    }
    
    //MARK:- Collection View Data Source and Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return categoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let details = categoryArray[section]
        if details.leaguesArray.count < details.visiableLeagueCount{
            return details.leaguesArray.count
        }
        return details.visiableLeagueCount
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 48)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let details = categoryArray[section]
        
        if categoryArray.count == section+1{
            if details.leaguesArray.count > details.visiableLeagueCount{
                return CGSize(width: collectionView.frame.size.width, height: 80)
            }
            else{
                return CGSize(width: collectionView.frame.size.width, height: 60)
            }
        }
        else if details.leaguesArray.count > details.visiableLeagueCount{
            return CGSize(width: collectionView.frame.size.width, height: 20)
        }
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let details = categoryArray[indexPath.section]
        let reminder = details.visiableLeagueCount % 2
        if (reminder == 1) && (indexPath.row == details.visiableLeagueCount - 1){
            return CGSize(width: collectionView.frame.width , height: 142)
        }
        else{
            return CGSize(width: collectionView.frame.width/2 , height: 172)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            let details = categoryArray[indexPath.section]
            headerView.lblCategoryName.text = details.categoryName.uppercased()
            headerView.lblMessage.text = details.categoryMessage

            if details.categoryImg.count != 0{
                if let url = NSURL(string: UserDetails.sharedInstance.teamImageUrl + details.categoryImg){
                    headerView.imgView.setImage(with: url as URL, placeholder: UIImage(named: "LeagueDefaultIcon"), progress: { received, total in
                        // Report progress
                    }, completion: { [weak self] image in
                        if (image != nil){
                            headerView.imgView.image = image
                        }
                        else{
                            headerView.imgView.image = UIImage(named: "LeagueDefaultIcon")
                        }
                    })
                }
                else{
                    headerView.imgView.image = UIImage(named: "LeagueDefaultIcon")
                }
            }
            else{
                headerView.imgView.image = UIImage(named: "LeagueDefaultIcon")
            }

            headerView.layoutIfNeeded()
            return headerView
            
        case UICollectionElementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
            let details = categoryArray[indexPath.section]
            footerView.lblViewMore.text = "VIEW MORE(".localized() + String(details.leaguesArray.count - details.visiableLeagueCount) + ")"
            footerView.viewMoreButton.isHidden = false
            footerView.lblViewMore.isHidden = false
            if categoryArray.count == indexPath.section+1{
                if details.leaguesArray.count > details.visiableLeagueCount{
                    footerView.viewMoreButton.isHidden = false
                    footerView.lblViewMore.isHidden = false
                }
                else{
                    footerView.viewMoreButton.isHidden = true
                    footerView.lblViewMore.isHidden = true
                }
            }
            footerView.viewMoreButton.tag = indexPath.section;
            footerView.viewMoreButton.addTarget(self, action: #selector(viewMoreButtonTapped(button:)), for: .touchUpInside)
            return footerView
            
        default:
            return UICollectionReusableView()
        }
    }
    
    @objc func viewMoreButtonTapped(button: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let moreLeagueVC = storyboard.instantiateViewController(withIdentifier: "MoreLeagueViewController") as! MoreLeagueViewController
        AppxorEventHandler.logAppEvent(withName: "ViewMoreClicked", info: ["SportType": "Cricket"])

        let details = categoryArray[button.tag]
        moreLeagueVC.selectedFantasy = selectedFantasy
        moreLeagueVC.categoryName = details.categoryName
        moreLeagueVC.categoryImg = details.categoryImg
        moreLeagueVC.categoryMessage = details.categoryMessage
        moreLeagueVC.categoryID = details.categoryID
        moreLeagueVC.totalJoinedLeague = totalJoinedLeague
        moreLeagueVC.totalTeams = totalJoinedTeam
        moreLeagueVC.matchDetails = matchDetails
        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(moreLeagueVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let details = categoryArray[indexPath.section]
        let reminder = details.visiableLeagueCount % 2

        if (reminder == 1) && (indexPath.row == details.visiableLeagueCount - 1){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleLeagueCollectionViewCell", for: indexPath) as! SingleLeagueCollectionViewCell
            cell.joinButton.tag = indexPath.row
            cell.joinButton.addTarget(self, action: #selector(self.joinLeagueButtonTapped(button:)), for: .touchUpInside)
            cell.configData(details: details.leaguesArray[indexPath.row], matchDetails: matchDetails!, gameType: selectedGameType)
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueCollectionViewCell", for: indexPath) as! LeagueCollectionViewCell
            cell.joinButton.tag = indexPath.row
            cell.joinButton.addTarget(self, action: #selector(self.joinLeagueButtonTapped(button:)), for: .touchUpInside)
            cell.configData(details: details.leaguesArray[indexPath.row], matchDetails: matchDetails!, gameType: selectedGameType)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if categoryArray.count <= indexPath.section{
            return
        }
        let categoryDetails = categoryArray[indexPath.section]
        let leagueDetails = categoryDetails.leaguesArray[indexPath.row]
        if leagueDetails.leagueType == "2" {
            return
        }
        
        let leaguePreview = storyboard.instantiateViewController(withIdentifier: "LeaguePreviewViewController") as! LeaguePreviewViewController
        leaguePreview.leagueDetails = leagueDetails
        leaguePreview.userTicketsArray = userTicketsArray
        leaguePreview.matchDetails = matchDetails
        leaguePreview.categoryName = categoryDetails.categoryName
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(leaguePreview, animated: true)
        }
    }
    
    @objc func joinLeagueButtonTapped(button: SolidButton)  {
        
        var leagueCell: LeagueCollectionViewCell?
        var singleCell: SingleLeagueCollectionViewCell?
        
        if let cell = button.superview?.superview?.superview?.superview?.superview as? LeagueCollectionViewCell {
            leagueCell = cell
        }
        
        if let cell = button.superview?.superview?.superview?.superview?.superview as? SingleLeagueCollectionViewCell {
            singleCell = cell
        }
        
        var cellIndexPath: IndexPath!
        
        if leagueCell != nil{
            guard let indexPath = leagueCollectionView.indexPath(for: leagueCell!) else{
                return;
            }
            cellIndexPath = indexPath
        }
        
        if singleCell != nil{
            guard let indexPath = leagueCollectionView.indexPath(for: singleCell!) else{
                return;
            }
            cellIndexPath = indexPath
        }
        
        let categoryDetails = categoryArray[cellIndexPath.section]
        let leagueDetails = categoryDetails.leaguesArray[cellIndexPath.row]
        
        if (leagueDetails.teamType == "1") {
            var maxTeamAllow = 0;
            
            if leagueDetails.fantasyType == "1"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForClassic;
                
                if selectedGameType == GameType.Cricket.rawValue {
                    maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForClassic;
                }
                else if selectedGameType == GameType.Kabaddi.rawValue {
                    maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForKabaddi;
                }
                else if selectedGameType == GameType.Football.rawValue {
                    maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForFootball;
                }
                else if selectedGameType == GameType.Basketball.rawValue {
                    maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBasketball;
                }
                else if selectedGameType == GameType.Baseball.rawValue {
                    maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBaseball;
                }

            }
            else if leagueDetails.fantasyType == "2"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBatting;
            }else if leagueDetails.fantasyType == "3"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBowling;
            }
            else if leagueDetails.fantasyType == "4"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForReverse;
            }
            else if leagueDetails.fantasyType == "5"{
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForWizard;
            }

            if leagueDetails.joinedLeagueCount == maxTeamAllow{
                return;
            }
        }
        else if (leagueDetails.teamType == "1") {
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.confirmedLeague == "2"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.teamType == "2"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        else if (leagueDetails.teamType == "3"){
            if leagueDetails.joinedLeagueCount > 0{
                return
            }
        }
        
        if isUserValidatingToJoinLeague {
            return;
        }
        
        var fantasyType = ""
        if leagueDetails.fantasyType == "1"{
            fantasyType = "Classic";
        }
        else if leagueDetails.fantasyType == "2"{
            fantasyType = "Batting";
        }
        else if leagueDetails.fantasyType == "3"{
            fantasyType = "Bowling";
        }
        else if leagueDetails.fantasyType == "4"{
            fantasyType = "Reverse";
        }
        else if leagueDetails.fantasyType == "5"{
            fantasyType = "Wizard";
        }
        
        AppxorEventHandler.logAppEvent(withName: "JoinNowButtonClicked", info: ["ContestType": categoryDetails.categoryName, "ContestID": leagueDetails.leagueId, "Type": fantasyType, "SportType": "Cricket"])
        
        callLeagueValidationAPI(leagueDetails: leagueDetails, categoryName: categoryDetails.categoryName)
    }
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails, categoryName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        isUserValidatingToJoinLeague = true
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "league_prev_data_v2","check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            weakSelf?.isUserValidatingToJoinLeague = false
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: "Not enough points to join this league", preferredStyle: UIAlertControllerStyle.alert)
                                    /*
                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
                                        
                                        let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
                                        addCashVC?.leagueDetails = leagueDetails
                                        addCashVC?.amount = roundFigureAmt
                                        addCashVC?.matchDetails = weakSelf!.matchDetails
                                        addCashVC?.userTeamArray = teamArray
                                        addCashVC?.categoryName = categoryName
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                            navVC.pushViewController(addCashVC!, animated: true)
                                        }
                                    }))
                                     */
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                    playerVC.matchDetails = weakSelf?.matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                        navVC.pushViewController(playerVC, animated: true)
                                    }
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                navVC.pushViewController(playerVC, animated: true)
                            }
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails, userTeamArray: teamArray, categoryName: categoryName, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                playerVC.matchDetails = weakSelf?.matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                    navVC.pushViewController(playerVC, animated: true)
                                }

                            }
                        }
                        else{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                navVC.pushViewController(playerVC, animated: true)
                            }
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, categoryName: String, ticketDetails: TicketDetails?)  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let joinedLeagueConfirmVC = storyboard.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
        joinedLeagueConfirmVC?.leagueDetails = leagueDetails
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails
        joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
        joinedLeagueConfirmVC?.matchDetails = self.matchDetails
        joinedLeagueConfirmVC?.leagueCategoryName = categoryName
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.pushViewController(joinedLeagueConfirmVC!, animated: true)
        }
    }
    
    func gotoSelectPlayerVC(leagueDetails: LeagueDetails)  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
        playerVC.matchDetails = matchDetails
        playerVC.leagueDetails = leagueDetails
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.pushViewController(playerVC, animated: true)
        }
    }
    
//    //MARK:- Scroll View Delegates
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView.contentOffset.y < -120{
//            delegate?.pullToRefereshLeagues();
//        }
//    }
}
