//
//  LeagueWinneraListCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LeagueWinneraListCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblTotalWinners: UILabel!
    @IBOutlet weak var tblView: UITableView!
   
    @IBOutlet weak var lblRankTitle: UILabel!
    @IBOutlet weak var lblPriceBreakup: UILabel!
    @IBOutlet weak var lblPriceTitle: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var totalWinerContainerView: UIView!
    var isJoinedMatchWinnerPreview = false
    var isViewForQuiz = false
    
    var leagueDetails: LeagueDetails?
    lazy var winnersArray = Array<LeagueWinnersRank>()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(details: LeagueDetails, dataArray: Array<LeagueWinnersRank>, isPreview: Bool, isQuiz: Bool) {
        winnersArray = dataArray
        leagueDetails = details
        isViewForQuiz = isQuiz
        isJoinedMatchWinnerPreview = isPreview
        if details.isInfinity == "1" {
            if details.leagueWinnerType == "dynamic_winner"{
                lblTotalWinners.text = leagueDetails!.totalWinnersPercent + "%" + " Winners"
            }
            else{
                if Int(details.totalWinners) ?? 0 > 1{
                    lblTotalWinners.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: details.totalWinners) + " " + "Winners".localized()
                }
                else{
                    lblTotalWinners.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: details.totalWinners) + " " + "Winner".localized()
                }
            }
        }
        else{
            if Int(details.totalWinners) ?? 0 > 1{
                lblTotalWinners.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: details.totalWinners) + " " + "Winners".localized()
            }
            else{
                lblTotalWinners.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: details.totalWinners) + " " + "Winner".localized()
            }
        }
        
        totalWinerContainerView.clipsToBounds = true
        totalWinerContainerView.roundViewCorners(corners: .topLeft, radius: 10)
        totalWinerContainerView.roundViewCorners(corners: .topRight, radius: 10)

        innerView.layer.cornerRadius = 10.0
        innerView.layer.shadowColor = UIColor(red: 78.0/255, green: 84.0/255, blue: 90.0/255, alpha: 0.5).cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        innerView.layer.shadowOpacity = 0.35
        innerView.layer.shadowRadius = 3
        innerView.layer.masksToBounds = false
        
        lblRankTitle.text = "Rank".localized()
        lblPriceBreakup.text = "Prize Breakup".localized()
        lblPriceTitle.text = "Prize".localized()
        
        tblView.register(UINib(nibName: "BottomMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "BottomMessageTableViewCell")
        tblView.register(UINib(nibName: "LeagueWinnersRankPriceTableViewCell", bundle: nil), forCellReuseIdentifier: "LeagueWinnersRankPriceTableViewCell")
        tblView.tableFooterView = UIView()
        DispatchQueue.main.async {   
            self.tblView.reloadData()
        }
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        if indexPath.row == winnersArray.count{
            if leagueDetails != nil {
                if (leagueDetails!.leagueMsg.count > 0) && (winnersArray.count != 0){
                    return 50.0
                }
                if isViewForQuiz {
                    return 50.0
                }
                if UIScreen.main.bounds.width <= 375 {
                    return 135.0
                }
                return 120.0
            }
        }
        else if indexPath.row == winnersArray.count + 1{
            if isViewForQuiz {
                return 50.0
            }
            if UIScreen.main.bounds.width <= 375 {
                return 135.0
            }
            return 120.0
        }

        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isJoinedMatchWinnerPreview {
            return winnersArray.count
        }
        else{
            if isViewForQuiz && (leagueDetails!.dynamicLeague != "2"){
                if leagueDetails != nil {
                    if (leagueDetails!.leagueMsg.count > 0) && (winnersArray.count != 0){
                        return winnersArray.count + 1
                    }
                }
                return winnersArray.count
            }
            else{
                if leagueDetails != nil {
                    if (leagueDetails!.leagueMsg.count > 0) && (winnersArray.count != 0){
                        return winnersArray.count + 2
                    }
                }
                return winnersArray.count + 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == winnersArray.count {
            var cell = tableView.dequeueReusableCell(withIdentifier: "BottomMessageTableViewCell") as? BottomMessageTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "BottomMessageTableViewCell") as? BottomMessageTableViewCell
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell?.lblMessage.text = "TDSWIningMessage".localized()
            cell?.lblMessage.textColor = lblPriceBreakup.textColor
            
            if (leagueDetails!.dynamicLeague == "2") && isViewForQuiz{
                cell?.lblMessage.text = "Ranking is subject to change if the number of players is less than \(leagueDetails!.maxPlayers)"
                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{
                        cell?.lblMessage.text = "यदि खिलाड़ियों की संख्या \(leagueDetails!.maxPlayers) से कम है तो रैंकिंग परिवर्तन के अधीन है|"
                    }
                }
            }
            

            if leagueDetails != nil {
                if (leagueDetails!.leagueMsg.count > 0) && (winnersArray.count != 0){
                    cell?.lblMessage.text = leagueDetails?.leagueMsg
                    cell?.lblMessage.textColor = UIColor(red: 45.0/255, green: 46.0/255.0, blue: 48.0/255, alpha: 1);
                }
            }
            
            return cell!
        }
        else if indexPath.row == winnersArray.count + 1{
            var cell = tableView.dequeueReusableCell(withIdentifier: "BottomMessageTableViewCell") as? BottomMessageTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "BottomMessageTableViewCell") as? BottomMessageTableViewCell
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.lblMessage.text = "TDSWIningMessage".localized()
            cell?.lblMessage.textColor = lblPriceBreakup.textColor
            
            if (leagueDetails!.dynamicLeague == "2") && isViewForQuiz{
                cell?.lblMessage.text = "Ranking is subject to change if the number of players is less than \(leagueDetails!.maxPlayers)"
                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{
                        cell?.lblMessage.text = "यदि खिलाड़ियों की संख्या \(leagueDetails!.maxPlayers) से कम है तो रैंकिंग परिवर्तन के अधीन है|"
                    }
                }
            }

            return cell!
        }
        else
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "LeagueWinnersRankPriceTableViewCell") as? LeagueWinnersRankPriceTableViewCell
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LeagueWinnersRankPriceTableViewCell") as? LeagueWinnersRankPriceTableViewCell
            }
            
            let winAmount = leagueDetails?.winAmount ?? "0"
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            let rankDetails = winnersArray[indexPath.row]
            cell?.configDetails(details: rankDetails, leagueAmount: winAmount)
            return cell!
        }
    }


}
