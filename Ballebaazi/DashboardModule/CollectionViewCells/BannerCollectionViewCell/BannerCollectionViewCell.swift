//
//  BannerCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 15/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgViewClock: UIImageView!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    var timer: Timer?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func configBannerDataForBonanaza(details: BannerDetails) {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        if let url = URL(string: UserDetails.sharedInstance.bannersFilePath + details.image){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: "BannrPlaceholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: "BannrPlaceholder")
                }
            })
        }
        else{
            self.imgView.image = UIImage(named: "BannrPlaceholder")
        }
        
        lblTimer.text = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: details.endDateTimestamp)

        weak var weakSelf = self;
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.updateTimerVlaue(details: details)
        }

    }

    @objc func updateTimerVlaue(details: BannerDetails){
        
        let remainigTime = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: details.endDateTimestamp)
        lblTimer.text = remainigTime
        
        if remainigTime.count == 0{
            NotificationCenter.default.post(name: NSNotification.Name("removeBonanazeBanner"), object: details.promotionID)
        }
    }

    func configBannerData(details: BannerDetails) {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        if let url = URL(string: UserDetails.sharedInstance.bannersFilePath + details.image){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: "BannrPlaceholder"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: "BannrPlaceholder")
                }
            })
        }
        else{
            self.imgView.image = UIImage(named: "BannrPlaceholder")
        }
    }
    
    func configJackpotData(imageURL: String) {
        let bannerImageURL = imageURL.trimmingCharacters(in: .whitespaces)
        AppHelper.showShodowOnCellsView(innerView: innerView)
        if let url = URL(string: UserDetails.sharedInstance.promotionImageUrl + bannerImageURL){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: "BannrPlaceholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: "BannrPlaceholder")
                }
            })
        }
        else{
            self.imgView.image = UIImage(named: "BannrPlaceholder")
        }
    }    
    
}
