//
//  QuizPreviewDetailsCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 02/04/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class QuizPreviewDetailsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dynamicTagVerticalConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var dynamicLeagueBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet var lblFirstMatchTypeMessage: UILabel!
    @IBOutlet var lblSecondMatchTypeMessage: UILabel!
    @IBOutlet var lblThirdMatchTypeMessage: UILabel!
    @IBOutlet var lblFirstMatchType: UILabel!
    @IBOutlet var lblSecondMatchType: UILabel!
    @IBOutlet var lblFourthMatchTypeMessage: UILabel!
    @IBOutlet var imgViewCoinApplicable: UIImageView!
    @IBOutlet weak var lblTotalWinningsTitle: UILabel!
    @IBOutlet weak var lblTotalWinning: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    var selectedLeague: LeagueDetails!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configData(leagueDetails: LeagueDetails) {
       
        selectedLeague = leagueDetails
        lblSecondMatchType.textColor = bonusQuizLeageTextColor
        lblSecondMatchType.backgroundColor = bonusQuizLeageBgColor
        
        lblFirstMatchType.textColor = dynamicLeageTextColor
        lblFirstMatchType.backgroundColor = dynamicLeageBgColor
        
        lblFirstMatchType.text = "D"
        lblSecondMatchType.text = "B"

        if (leagueDetails.dynamicLeague == "2") && (leagueDetails.bonusApplicable == "2"){
        }
        else if (leagueDetails.dynamicLeague == "2"){
            lblSecondMatchType.isHidden = true
            lblSecondMatchTypeMessage.isHidden = true
            dynamicLeagueBottomConstraint.constant = -25.0
            self.layoutIfNeeded()
        }
        else if (leagueDetails.bonusApplicable == "2"){

            lblFirstMatchType.isHidden = true
            lblFirstMatchTypeMessage.isHidden = true
        }
        else{
            lblFirstMatchType.isHidden = true
            lblFirstMatchTypeMessage.isHidden = true

            lblSecondMatchType.isHidden = true
            lblSecondMatchTypeMessage.isHidden = true
        }

        AppHelper.showShodowOnCellsView(innerView: innerView)
        let bonusMessage = leagueDetails.bounsPercentage + "bonusMessage".localized()
        lblSecondMatchTypeMessage.text = bonusMessage
        lblFirstMatchTypeMessage.text = "DynamicLeagueMessage".localized()

        if (leagueDetails.dynamicLeague == "2"){
            lblThirdMatchTypeMessage.text = "Limit of max \(leagueDetails.maxPlayers) players"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    dynamicTagVerticalConstraint.constant = 0
                    self.layoutIfNeeded()
                    lblThirdMatchTypeMessage.text = "\(leagueDetails.maxPlayers) खिलाड़ियों तक सीमित।"
                }
            }
        }
        else{
            lblThirdMatchTypeMessage.text = "Play with \(leagueDetails.maxPlayers) players"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    lblThirdMatchTypeMessage.text = "\(leagueDetails.maxPlayers) खिलाड़ियों के साथ खेलें"
                }
            }
        }
        
        lblFourthMatchTypeMessage.text = leagueDetails.totalQuestions + "Questions to answer".localized()

        lblTotalWinningsTitle.text = "Total Winnings".localized()
        lblTotalWinning.text = "pts" +   AppHelper.makeCommaSeparatedDigitsWithString(digites: leagueDetails.winAmount ?? "")
    }

    @IBAction func infoButtonTapped(_ sender: Any) {
        let quizInfoView = QuizInfoView(frame: APPDELEGATE.window!.frame)
        quizInfoView.updateInfo(leagueDetails: selectedLeague)
        APPDELEGATE.window?.addSubview(quizInfoView)
        
    }
    
    
}
