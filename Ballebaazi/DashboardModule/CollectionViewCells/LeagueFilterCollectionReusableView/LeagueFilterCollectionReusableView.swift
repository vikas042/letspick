//
//  LeagueFilterCollectionReusableView.swift
//  Letspick
//
//  Created by Vikash Rajput on 21/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class LeagueFilterCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var lblHeader: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
