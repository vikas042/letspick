//
//  PlayerStatesTableContainerCollectionViewCell.swift
//  Letspick
//
//  Created by Madstech on 22/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class PlayerStatesTableContainerCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var lblPlayed: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblPlayerStats: UILabel!
    @IBOutlet weak var lblSelection: UILabel!
    
    @IBOutlet weak var placeholderImgView: UIImageView!
    @IBOutlet weak var comingSoonView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewContainer: UIView!
    
    var playerInfoArray = Array<PlayerMatchInfo>()
    var selectedFantasyType = "1"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
        
    func configDetails(dataArray: Array<PlayerMatchInfo>, type: Int, fantasyType: Int, playerName: String) {
        
        lblPlayed.text = "Played".localized()
        lblPoints.text = "Points".localized()
        lblPlayerStats.text = "Series Fantasy Stats".localized() + playerName
        lblSelection.text = "Selected_Percentage".localized()
        lblNoData.text = ""
        if fantasyType == FantasyType.Classic.rawValue{
            selectedFantasyType = "1"
        }
        else if fantasyType == FantasyType.Batting.rawValue{
            selectedFantasyType = "2"
        }
        else if fantasyType == FantasyType.Bowling.rawValue{
            selectedFantasyType = "3"
        }
        
        playerInfoArray = dataArray
        tblView.register(UINib(nibName: "PlayerPointInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerPointInfoTableViewCell")
        tblView.tableFooterView = UIView()
        tblView.reloadData();
        placeholderImgView.image = nil
        
        if type == 1 {
            placeholderImgView.image = UIImage(named: "PlayerStatsSeriesPlaceholder")
            lblNoData.text = "payer_detail_not_found".localized()
        }
        else if type == 2 {
            placeholderImgView.image = UIImage(named: "PlayerStatsAnalysisPlaceholder")
            lblNoData.text = "no_analysis_msg".localized()
        }
        else if type == 3 {
            placeholderImgView.image = UIImage(named: "PlayerStatsNewsPlaceholder")
            lblNoData.text = "the_latest_updates_are_rolling_stay_tuned".localized()
        }
        
        if type == 1{
            if dataArray.count != 0{
                comingSoonView.isHidden = true
                tblViewContainer.isHidden = false
            }
            else{
                comingSoonView.isHidden = false
                tblViewContainer.isHidden = true
            }
        }
        else{
            comingSoonView.isHidden = false
            tblViewContainer.isHidden = true
        }
        
        tblView.reloadData()
    }
    
    
    // MARK:- TableView Delegates and Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerInfoArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "PlayerPointInfoTableViewCell") as? PlayerPointInfoTableViewCell
        
        if cell == nil {
            cell = PlayerPointInfoTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PlayerPointInfoTableViewCell")
        }
        
        let details = playerInfoArray[indexPath.row];
        cell?.configData(details: details, fantacyType: selectedFantasyType)
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }


}
