//
//  RewardStoreCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 25/02/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class RewardStoreCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bottomContentView: UIView!
    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCoin: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var claimButton: ClaimButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configData(details: RewardProductDetails) {
        lblName.text = details.rewardNameEnglish
        lblCoin.text = details.coins
        DispatchQueue.main.async {
            self.bottomContentView.roundViewCorners(corners: [.bottomLeft, .bottomRight], radius: 7)
        }
        if details.isProductClaimed {
            claimButton.isSelected = true
        }
        else{
            claimButton.isSelected = false
        }

        
        
        claimButton.setTitle("Claim".localized(), for: .normal)
        AppHelper.showShodowOnCellsView(innerView: innerView)
        if details.image.count < 10 {
            self.imgView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: details.image){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: "Placeholder")
                }
            })
        }
        else{
            self.imgView.image = UIImage(named: "Placeholder")
        }

    }
}
