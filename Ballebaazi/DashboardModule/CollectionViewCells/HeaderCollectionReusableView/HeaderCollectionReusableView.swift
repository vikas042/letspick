//
//  HeaderCollectionReusableView.swift
//  Letspick
//
//  Created by Vikash Rajput on 27/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var categoryNameLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
