//
//  FootballSwapCollectionViewCell.swift
//  Letspick
//
//  Created by Madstech on 29/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class FootballSwapCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var captainSepImgView1: UIImageView!
    @IBOutlet weak var captainSepImgView2: UIImageView!
    @IBOutlet weak var lblForwardTitle: UILabel!
    @IBOutlet weak var lblMidFielderTitle: UILabel!
    @IBOutlet weak var lblDefenderTitle: UILabel!
    @IBOutlet weak var lblGkTitle: UILabel!
    @IBOutlet weak var lblForwardCount: UILabel!
    @IBOutlet weak var lblMidFielderCount: UILabel!
    @IBOutlet weak var lblDefenderCount: UILabel!
    @IBOutlet weak var lblGkCount: UILabel!
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var lblViceCaptain: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var teamPreviewButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var lblVicecaptainTitle: UILabel!
    @IBOutlet weak var lblCaptainTitle: UILabel!
    @IBOutlet weak var lblTeamNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(_ details: UserTeamDetails, selectedTeam: UserTeamDetails?) {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblTeamNumber.text = "Team".localized().uppercased() + " " + details.teamNumber!
        innerView.clipsToBounds = true
        lblCaptain.text = details.captionName
        lblViceCaptain.text = details.viceCaptionName
        lblMidFielderCount.text = details.totalMidFielderCount
        lblForwardCount.text = details.totalForwardCount
        lblGkCount.text = details.totalGoalKeeperCount
        lblDefenderCount.text = details.totalDefenderCount
        lblCaptainTitle.text = "Captain".localized()
        lblVicecaptainTitle.text = "subCaptain".localized()

        lblGkTitle.text = "GK".localized()
        lblMidFielderTitle.text = "MID".localized()
        lblDefenderTitle.text = "DEF".localized()
        lblForwardTitle.text = "ST".localized()

        
        if details.teamNumber == selectedTeam?.teamNumber {
            showViewForSelected()
        }
        else{
            showViewForUnselected()
        }
        
    }
    
    func showViewForSelected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgBlueIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorBlueIcon")

       // headerContentView.backgroundColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)
       // innerView.backgroundColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1)
        lblForwardTitle.textColor = UIColor.white
        lblMidFielderTitle.textColor = UIColor.white
        lblDefenderTitle.textColor = UIColor.white
        lblGkTitle.textColor = UIColor.white
        lblForwardTitle.textColor = UIColor.white
        lblTeamNumber.textColor = UIColor.white
        lblCaptain.textColor = UIColor.white
        lblViceCaptain.textColor = UIColor.white
        
        lblCaptainTitle.textColor = UIColor.white
        lblVicecaptainTitle.textColor = UIColor.white
        
        lblMidFielderCount.textColor = UIColor.white
        lblDefenderCount.textColor = UIColor.white
        lblGkCount.textColor = UIColor.white
        lblForwardCount.textColor = UIColor.white
        
        lblMidFielderCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblDefenderCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblGkCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblForwardCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        
        selectButton.isSelected = true;
    }
    
    func showViewForUnselected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorIcon")

        lblTeamNumber.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        headerContentView.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        innerView.backgroundColor = UIColor.white
        lblForwardTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblMidFielderTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblDefenderTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblGkTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblForwardTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        lblViceCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        

        lblCaptainTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 135.0/255, alpha: 1)
        lblVicecaptainTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 135.0/255, alpha: 1)

        lblMidFielderCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblDefenderCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblGkCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblForwardCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        
        lblMidFielderCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblDefenderCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblGkCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblForwardCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        
        selectButton.isSelected = false;
    }
    
}
