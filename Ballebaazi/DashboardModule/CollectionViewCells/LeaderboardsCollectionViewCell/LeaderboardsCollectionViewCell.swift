//
//  LeaderboardsCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/01/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LeaderboardsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblWinningsTitle: UILabel!
    
    @IBOutlet weak var rankingView: UIView!
    @IBOutlet weak var btnRanking: UIButton!
    @IBOutlet weak var btnWinner: UIButton!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblWinnerCount: UILabel!
    @IBOutlet weak var lblLeaderbaordName: UILabel!
    @IBOutlet weak var lblWinnerAmt: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    func configData(details: Leaderboards) {
        lblWinningsTitle.text = "Winnings".localized()
        btnRanking.setTitle("See Rankings".localized(), for: .normal)
        rankingView.roundViewCorners(corners: .bottomLeft, radius: 7)
        rankingView.roundViewCorners(corners: .bottomLeft, radius: 7)
        AppHelper.showShodowOnCellsView(innerView:innerView)
        if Int(details.totalWinners) ?? 0 > 1{
            lblWinnerCount.text = details.totalWinners + " " + "Winners".localized()
        }
        else{
            lblWinnerCount.text = details.totalWinners + " " + "Winner".localized()
        }
        
        lblWinnerAmt.text = "" + AppHelper.makeCommaSeparatedDigitsWithString(digites: details.winAmount)
        lblLeaderbaordName.text = details.title
        
        if details.startDate.count != 0{
            lblDate.text = getDateAndMonthByDate(dateString: details.startDate) + " - " +  getDateAndMonthByDate(dateString: details.endDate)
        }
        else{
            lblDate.text = "N/A"
        }
    }
    
    
    
    func getDayByDate(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "dd"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }
            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr
            
        }
        return ""
    }

    
    
    func getDateAndMonthByDate(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "dd MMM"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }
            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr
            
        }
        return ""
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
