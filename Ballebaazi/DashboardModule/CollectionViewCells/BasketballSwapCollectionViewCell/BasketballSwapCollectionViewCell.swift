//
//  FootballSwapCollectionViewCell.swift
//  Letspick
//
//  Created by Madstech on 29/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class BasketballSwapCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var captainSepImgView1: UIImageView!
    @IBOutlet weak var captainSepImgView2: UIImageView!
    
    @IBOutlet weak var lblSGTitle: UILabel!
    @IBOutlet weak var lblSFTitle: UILabel!
    @IBOutlet weak var lblPFTitle: UILabel!
    @IBOutlet weak var lblPGTitle: UILabel!
    @IBOutlet weak var lblCENTitle: UILabel!

    @IBOutlet weak var lblSGCount: UILabel!
    @IBOutlet weak var lblSFCount: UILabel!
    @IBOutlet weak var lblPFCount: UILabel!
    @IBOutlet weak var lblPGCount: UILabel!
    @IBOutlet weak var lblCENCount: UILabel!

    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var lblViceCaptain: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var teamPreviewButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var lblVicecaptainTitle: UILabel!
    @IBOutlet weak var lblCaptainTitle: UILabel!
    @IBOutlet weak var lblTeamNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(_ details: UserTeamDetails, selectedTeam: UserTeamDetails?) {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblTeamNumber.text = "Team".localized().uppercased() + " " + details.teamNumber!
        innerView.clipsToBounds = true
        lblCaptain.text = details.captionName
        lblViceCaptain.text = details.viceCaptionName
        
        lblSFCount.text = details.totalSmallForwardCount
        lblSGCount.text = details.totalShootingGuardCount
        lblPGCount.text = details.totalPointGuardCount
        lblPFCount.text = details.totalPowerForwardCount
        lblCENCount.text = details.totalCenterCount

        lblPGTitle.text = "PG".localized()
        lblSFTitle.text = "SF".localized()
        lblPFTitle.text = "PF".localized()
        lblSGTitle.text = "SG".localized()
        lblCENTitle.text = "Cen".localized()

        
        if details.teamNumber == selectedTeam?.teamNumber {
            showViewForSelected()
        }
        else{
            showViewForUnselected()
        }
        
    }
    
    func showViewForSelected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgBlueIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorBlueIcon")

        lblCaptainTitle.text = "Captain".localized()
        lblVicecaptainTitle.text = "subCaptain".localized()

        headerContentView.backgroundColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)
        innerView.backgroundColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1)
        lblSGTitle.textColor = UIColor.white
        lblSFTitle.textColor = UIColor.white
        lblPFTitle.textColor = UIColor.white
        lblPGTitle.textColor = UIColor.white
        lblSGTitle.textColor = UIColor.white
        lblCENTitle.textColor = UIColor.white

        lblTeamNumber.textColor = UIColor.white
        lblCaptain.textColor = UIColor.white
        lblViceCaptain.textColor = UIColor.white
        
        lblCaptainTitle.textColor = UIColor.white
        lblVicecaptainTitle.textColor = UIColor.white
        
        lblSFCount.textColor = UIColor.white
        lblPFCount.textColor = UIColor.white
        lblPGCount.textColor = UIColor.white
        lblSGCount.textColor = UIColor.white
        lblCENCount.textColor = UIColor.white

        
        lblSFCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblPFCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblPGCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblSGCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblCENCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)

        selectButton.isSelected = true;
    }
    
    func showViewForUnselected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorIcon")

        lblTeamNumber.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        headerContentView.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        innerView.backgroundColor = UIColor.white
        lblSGTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblSFTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblPFTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblPGTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblSGTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblCENTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        lblViceCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        

        lblCaptainTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 135.0/255, alpha: 1)
        lblVicecaptainTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 135.0/255, alpha: 1)

        lblSFCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblPFCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblPGCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblSGCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblCENCount.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)

        lblSFCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblPFCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblPGCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblSGCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblCENCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)

        selectButton.isSelected = false;
    }
    
}
