//
//  MyTicketsCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class MyTicketsCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var ImageNoRecord: UIImageView!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var tblView: UITableView!
    var userTicketsArray = Array<[String: AnyObject]>()
    var navigationController: UINavigationController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(ticketsArray: Array<[String: AnyObject]>) {
                
        userTicketsArray = ticketsArray
        navigationController = APPDELEGATE.window?.rootViewController as? UINavigationController
        lblNote.text = "Win_handsomely".localized()
        lblNoRecordFound.text = "NoTicket".localized()
        tblView.register(UINib(nibName: "TicketTableViewCell", bundle: nil), forCellReuseIdentifier: "TicketTableViewCell")

        if ticketsArray.count == 0 {
            self.lblNoRecordFound.isHidden = false
            self.ImageNoRecord.isHidden = false
            lblNote.isHidden = true
        }
        else{
            self.lblNoRecordFound.isHidden = true
            self.ImageNoRecord.isHidden = true
            lblNote.isHidden = false
        }
        tblView.reloadData()
    }
    
    // MARK:- TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return userTicketsArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let ticketDict = userTicketsArray[section]
        let ticketsArray = ticketDict["list"] as! Array<TicketDetails>
        return ticketsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38.0;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116.0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 38))
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 4, width: tableView.bounds.size.width, height: 34))
        titleLabel.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        titleLabel.font = UIFont(name: "OpenSans-Semibold", size: 14)
        headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        let ticketDict = userTicketsArray[section]
        titleLabel.text = ticketDict["title"] as? String
        titleLabel.textAlignment = .center
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "TicketTableViewCell") as? TicketTableViewCell
        
        if cell == nil {
            cell = TicketTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TicketTableViewCell")
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        let ticketDict = userTicketsArray[indexPath.section]
        let ticketsArray = ticketDict["list"] as! Array<TicketDetails>
        let ticketDetails = ticketsArray[indexPath.row]
        cell?.joinButton.addTarget(self, action: #selector(joinButtonTapped(button:)), for: .touchUpInside)
        cell?.lblExiperyDate?.text = AppHelper.getTicketFormattedDate(dateString: ticketDetails.ticketExpiry)
        cell?.lblTime?.text = AppHelper.getTicketFormattedTime(dateString: ticketDetails.ticketExpiry)
        cell?.joinButton.isHidden = true
        cell?.joinButton.setTitle("Join".localized(), for: .normal)
        
        if ticketDetails.gameType == "1"{
            cell?.lblGameType.text = "Cricket"
            cell?.imgView.image = UIImage(named: "OrangeTicketCardIcon")
        }
        else if ticketDetails.gameType == "2"{
            cell?.lblGameType.text = "Kabaddi"
            cell?.imgView.image = UIImage(named: "GreenTicketCardIcon")
        }
        else if ticketDetails.gameType == "3"{
            cell?.lblGameType.text = "Football"
            cell?.imgView.image = UIImage(named: "BlueTicketCardIcon")
        }
        else if ticketDetails.gameType == "5"{
            cell?.lblGameType.text = "Basketball"
            cell?.imgView.image = UIImage(named: "BaseballTicketCardIcon")
        }
        else if ticketDetails.gameType == "6"{
            cell?.lblGameType.text = "Baseball"
            cell?.imgView.image = UIImage(named: "BasketballTicketCardIcon")
        }

        if ticketDetails.matchKey.count > 0{
            cell?.joinButton.isHidden = false
        }
        
        if ticketDetails.matchName.count > 0{
            cell?.lblMatchType?.text = ticketDetails.matchName
        }
        else {
            cell?.lblMatchType?.text = "Any Match Tickets".localized()
            cell?.imgView.image = UIImage(named: "PurpleTicketCardIcon")
        }
        
        cell?.lblTitle?.text = ticketDetails.ticketTitle
        AppHelper.showShodowOnCellsView(innerView: cell!.innerView)
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let ticketDict = userTicketsArray[indexPath.section]
        let ticketsArray = ticketDict["list"] as! Array<TicketDetails>
        let ticketDetails = ticketsArray[indexPath.row]
        if ticketDetails.matchKey.count > 0{
            joinTicket(indexPath: indexPath)
        }
        else{
            var message = "To use this ticket go to any match and join the league \(ticketDetails.leagueName) with joining amount pts \(ticketDetails.joiningAmount)";
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    message = "टिकट यूज करने के लिए किसी भी मैच पर जाएं और \(ticketDetails.leagueName) लीग \(ticketDetails.joiningAmount) रुपए के जोइनिंग अमाउंट का भुगतान करके ज्वाइन कर लें।"
                }
            }
            
            let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
            }))
            APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func joinButtonTapped(button: UIButton) {
        guard let cell = button.superview?.superview?.superview?.superview as? TicketTableViewCell else {
            return // or fatalError() or whatever
        }
        
        guard let indexPath = tblView.indexPath(for: cell) else{
            return
        }
        joinTicket(indexPath: indexPath)
    }
    
    func joinTicket(indexPath: IndexPath) {
        
        let ticketDict = userTicketsArray[indexPath.section]
        let ticketsArray = ticketDict["list"] as! Array<TicketDetails>
        let ticketDetails = ticketsArray[indexPath.row]

        let leagueDetails = LeagueDetails()
        leagueDetails.fantasyType = "1"
        leagueDetails.leagueId = ticketDetails.leagueId
        leagueDetails.matchKey = ticketDetails.matchKey
        leagueDetails.joiningAmount = ticketDetails.joiningAmount
        
        let matchDetails = MatchDetails()
        matchDetails.matchKey = ticketDetails.matchKey
        matchDetails.matchShortName = ticketDetails.matchShortName
        matchDetails.firstTeamShortName = ""
        matchDetails.secondTeamShortName = ""
        matchDetails.startDateTimestemp = ticketDetails.startDateUnix
        matchDetails.seasonKey = ""
        
        if ticketDetails.gameType == "1" {
            callLeagueValidationAPI(details: leagueDetails, matchDetails: matchDetails)
        }
        else if ticketDetails.gameType == "2"{
            callLeagueValidationAPIForKabaddi(details: leagueDetails, matchDetails: matchDetails)
        }
        else if ticketDetails.gameType == "3"{
            callLeagueValidationAPIForFootball(details: leagueDetails, matchDetails: matchDetails)
        }
        else if ticketDetails.gameType == "5"{
            callLeagueValidationAPIForBasketball(details: leagueDetails, matchDetails: matchDetails)
        }
        else if ticketDetails.gameType == "6"{
            callLeagueValidationAPIForBaseball(details: leagueDetails, matchDetails: matchDetails)
        }
    }
    
    //MARK:- API Related Methods
    func callLeagueValidationAPI(details: LeagueDetails, matchDetails: MatchDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "league_prev_data_v1","check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": details.matchKey, "template_id": details.leagueId, "fantasy_type": details.fantasyType]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                                ticketDetais?.isFromTicketScreen = true
                            }
                        }
                    }
                    
                    var userTeamsArray = Array<UserTeamDetails>()
                    if let teamsArray = response?["user_teams"]?.array{
                        userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: teamsArray, matchDetails: matchDetails)
                    }
                    
                    var leagueDetails:LeagueDetails?
                    
                    if let leagueObj = response?["league"]?.dictionary{
                        leagueDetails = LeagueDetails.getLeagueDetails(details: leagueObj)
                    }


                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails?.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails!.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//
//                                        let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.selectedGameType = GameType.Cricket.rawValue
//                                        addCashVC?.matchDetails = matchDetails
//                                        addCashVC?.userTeamArray = teamArray
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                    playerVC.matchDetails = matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails!.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails!, userTeamArray: teamArray, matchDetails: matchDetails, gameType: GameType.Cricket.rawValue, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                playerVC.matchDetails = matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            }
                        }
                        else{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callLeagueValidationAPIForKabaddi(details: LeagueDetails, matchDetails: MatchDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league_preview_v1", "check_ticket": "1", "match_key": details.matchKey, "fantasy_type": details.fantasyType, "template_id": details.leagueId]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kKabaddiMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                                ticketDetais?.isFromTicketScreen = true
                            }
                        }
                    }
                    var leagueDetails:LeagueDetails?
                    
                    if let leagueObj = response?["league"]?.dictionary{
                        leagueDetails = LeagueDetails.getLeagueDetails(details: leagueObj)
                    }

                    var userTeamsArray = Array<UserTeamDetails>()
                    if let teamsArray = response?["user_teams"]?.array{
                        userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: teamsArray, matchDetails: matchDetails)
                    }

                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails!.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails!.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//
//                                        let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = matchDetails
//                                        addCashVC?.userTeamArray = teamArray
//                                        addCashVC?.selectedGameType = GameType.Kabaddi.rawValue
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    
                                    let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                    playerVC.matchDetails = matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails!.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails!, userTeamArray: teamArray, matchDetails: matchDetails, gameType: GameType.Kabaddi.rawValue, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                                playerVC.matchDetails = matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                
                            }
                        }
                        else{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectKadaddiPlayersViewController") as! SelectKadaddiPlayersViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    
    func callLeagueValidationAPIForFootball(details: LeagueDetails, matchDetails: MatchDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league_preview_v1", "check_ticket": "1", "match_key": details.matchKey, "fantasy_type": details.fantasyType, "template_id": details.leagueId]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kFootballMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                                ticketDetais?.isFromTicketScreen = true
                            }
                        }
                    }
                    
                    var leagueDetails:LeagueDetails?
                    
                    if let leagueObj = response?["league"]?.dictionary{
                        leagueDetails = LeagueDetails.getLeagueDetails(details: leagueObj)
                    }

                    var userTeamsArray = Array<UserTeamDetails>()
                    if let teamsArray = response?["user_teams"]?.array{
                        userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: teamsArray, matchDetails: matchDetails)
                    }

                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails!.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!
                                    let joiningAmount = Float(leagueDetails!.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//
//                                        let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = matchDetails
//                                        addCashVC?.selectedGameType = GameType.Football.rawValue
//                                        addCashVC?.userTeamArray = teamArray
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                    playerVC.matchDetails = matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails!.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails!, userTeamArray: teamArray, matchDetails: matchDetails, gameType: GameType.Football.rawValue, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                                playerVC.matchDetails = matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            }
                        }
                        else{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectFootballPlayersViewController") as! SelectFootballPlayersViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callLeagueValidationAPIForBasketball(details: LeagueDetails, matchDetails: MatchDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league_preview_v1", "check_ticket": "1", "match_key": details.matchKey, "fantasy_type": details.fantasyType, "template_id": details.leagueId]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kBasketballMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                                ticketDetais?.isFromTicketScreen = true
                            }
                        }
                    }
                    
                    var leagueDetails:LeagueDetails?
                    
                    if let leagueObj = response?["league"]?.dictionary{
                        leagueDetails = LeagueDetails.getLeagueDetails(details: leagueObj)
                    }

                    var userTeamsArray = Array<UserTeamDetails>()
                    if let teamsArray = response?["user_teams"]?.array{
                        userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: teamsArray, matchDetails: matchDetails)
                    }

                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails!.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!
                                    let joiningAmount = Float(leagueDetails!.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//
//                                        let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = matchDetails
//                                        addCashVC?.selectedGameType = GameType.Basketball.rawValue
//                                        addCashVC?.userTeamArray = teamArray
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                    playerVC.matchDetails = matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails!.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails!, userTeamArray: teamArray, matchDetails: matchDetails, gameType: GameType.Basketball.rawValue, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                                playerVC.matchDetails = matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                
                            }
                        }
                        else{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBasketballPlayersViewController") as! SelectBasketballPlayersViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func callLeagueValidationAPIForBaseball(details: LeagueDetails, matchDetails: MatchDetails)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "join_league_preview_v1", "check_ticket": "1", "match_key": details.matchKey, "fantasy_type": details.fantasyType, "template_id": details.leagueId]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kBaseballMatchURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                                ticketDetais?.isFromTicketScreen = true
                            }
                        }
                    }
                    
                    var leagueDetails:LeagueDetails?
                    
                    if let leagueObj = response?["league"]?.dictionary{
                        leagueDetails = LeagueDetails.getLeagueDetails(details: leagueObj)
                    }

                    var userTeamsArray = Array<UserTeamDetails>()
                    if let teamsArray = response?["user_teams"]?.array{
                        userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: teamsArray, matchDetails: matchDetails)
                    }

                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails!.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!
                                    let joiningAmount = Float(leagueDetails!.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: message, preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
//                                        
//                                        let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
//                                        addCashVC?.leagueDetails = leagueDetails
//                                        addCashVC?.amount = roundFigureAmt
//                                        addCashVC?.matchDetails = matchDetails
//                                        addCashVC?.selectedGameType = GameType.Baseball.rawValue
//                                        addCashVC?.userTeamArray = teamArray
//                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                                            navVC.pushViewController(addCashVC!, animated: true)
//                                        }
//                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                    playerVC.matchDetails = matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails!.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails!, userTeamArray: teamArray, matchDetails: matchDetails, gameType: GameType.Baseball.rawValue, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                                playerVC.matchDetails = matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            }
                        }
                        else{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectBaseballPlayersViewController") as! SelectBaseballPlayersViewController
                            playerVC.matchDetails = matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            weakSelf?.navigationController?.pushViewController(playerVC, animated: true)
                            
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, matchDetails: MatchDetails, gameType: Int, ticketDetails: TicketDetails?)  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let joinedLeagueConfirmVC = storyboard.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
        joinedLeagueConfirmVC?.leagueDetails = leagueDetails
        joinedLeagueConfirmVC?.ticketDetails = ticketDetails
        joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
        joinedLeagueConfirmVC?.matchDetails = matchDetails
        joinedLeagueConfirmVC?.isNeedToShowMatchClosePopup = true
        joinedLeagueConfirmVC?.leagueCategoryName = ""
        joinedLeagueConfirmVC?.selectedGameType = gameType
        navigationController?.pushViewController(joinedLeagueConfirmVC!, animated: true)
    }

}
