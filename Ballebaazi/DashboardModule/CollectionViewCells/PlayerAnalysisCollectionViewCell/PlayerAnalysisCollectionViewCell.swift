//
//  PlayerAnalysisCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 06/02/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class PlayerAnalysisCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblPriceBreakupTitle: UILabel!
    @IBOutlet weak var lblBowlPoints: UILabel!
    @IBOutlet weak var lblStartPoints: UILabel!
    @IBOutlet weak var lblBatPoints: UILabel!
    @IBOutlet weak var lblBat: UILabel!
    @IBOutlet weak var lblBowlAndField: UILabel!
    @IBOutlet weak var lblStart: UILabel!
    @IBOutlet weak var lblTotalPoints: UILabel!
    @IBOutlet weak var lblTotalPointsTitle: UILabel!
    @IBOutlet weak var lblAvg: UILabel!
    @IBOutlet weak var lblBad: UILabel!
    @IBOutlet weak var badImgView: UIImageView!
    @IBOutlet weak var lblGood: UILabel!
    @IBOutlet weak var lblPlayerFormTitle: UILabel!
    @IBOutlet weak var avgImgView: UIImageView!
    @IBOutlet weak var goodImgView: UIImageView!
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var formView: UIView!

    @IBOutlet weak var lblYetToScore: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(details: PlayerDetails, fantasyType: Int) {
        var playerForm = details.formClassic

        if (playerForm == "0") || (playerForm == ""){
            lblYetToScore.isHidden = false
            lblPlayerFormTitle.isHidden = true
            lblPriceBreakupTitle.isHidden = true
            pointsView.isHidden = true
            formView.isHidden = true
        }
        else{
            lblYetToScore.isHidden = true
            lblPlayerFormTitle.isHidden = false
            lblPriceBreakupTitle.isHidden = false
            pointsView.isHidden = false
            formView.isHidden = false
        }
        
        lblGood.text = "Good".localized()
        lblAvg.text = "Average".localized()
        lblBad.text = "Bad".localized()
        lblStart.text = "Start".localized()
        lblBat.text = "Bat".localized()
        lblBowlAndField.text = "BowlAndField".localized()
        lblPlayerFormTitle.text = "Player Form".localized()
        lblPriceBreakupTitle.text = "Points Breakup".localized()
        
        lblStartPoints.text = details.sessionalStartingPoints
        lblBatPoints.text = "0"
        lblBowlPoints.text = "0"
        lblTotalPoints.text = "0"
                
        if fantasyType == FantasyType.Classic.rawValue {
            playerForm = details.formClassic
            lblTotalPoints.text = details.classicPointsForStat
            
            let battingPoints = (Float(details.battingPointsForStat) ?? 0) - (Float(details.sessionalStartingPoints) ?? 0)
            let bowlingPoints = (Float(details.bowlingPointsForStat) ?? 0) - (Float(details.sessionalStartingPoints) ?? 0)
            lblBatPoints.text = String(battingPoints)
            lblBowlPoints.text = String(bowlingPoints)
        }
        else if fantasyType == FantasyType.Batting.rawValue {
            playerForm = details.formBatting
            
            let battingPoints = (Float(details.battingPointsForStat) ?? 0) - (Float(details.sessionalStartingPoints) ?? 0)
            lblBatPoints.text = String(battingPoints)
            lblBowlPoints.text = "NA"
            lblTotalPoints.text = details.battingPointsForStat
        }
        else if fantasyType == FantasyType.Bowling.rawValue {
            playerForm = details.formBowlling
            let bowlingPoints = (Float(details.bowlingPointsForStat) ?? 0) - (Float(details.sessionalStartingPoints) ?? 0)
            lblBowlPoints.text = String(bowlingPoints)
            lblBatPoints.text = "NA"
            lblTotalPoints.text = details.battingPointsForStat
        }

        lblGood.textColor = UIColor(red: 130.0/255, green: 131.0/255, blue: 132.0/255, alpha: 1)
        lblAvg.textColor = UIColor(red: 130.0/255, green: 131.0/255, blue: 132.0/255, alpha: 1)
        lblBad.textColor = UIColor(red: 130.0/255, green: 131.0/255, blue: 132.0/255, alpha: 1)

        if playerForm == "1" {
            goodImgView.image = UIImage(named: "GoodUnselected")
            avgImgView.image = UIImage(named: "AvgUnselected")
            badImgView.image = UIImage(named: "BadSelected")
            lblBad.textColor = UIColor(red: 103.0/255, green: 191.0/255, blue: 83.0/255, alpha: 1)
        }
        else if playerForm == "2" {
            goodImgView.image = UIImage(named: "GoodUnselected")
            avgImgView.image = UIImage(named: "AvgSelected")
            badImgView.image = UIImage(named: "BadUnselected")
            lblAvg.textColor = UIColor(red: 103.0/255, green: 191.0/255, blue: 83.0/255, alpha: 1)
        }
        else if playerForm == "3" {
            goodImgView.image = UIImage(named: "GoodSelected")
            avgImgView.image = UIImage(named: "AvgUnselected")
            badImgView.image = UIImage(named: "BadUnselected")
            lblGood.textColor = UIColor(red: 103.0/255, green: 191.0/255, blue: 83.0/255, alpha: 1)
        }
    }

}
