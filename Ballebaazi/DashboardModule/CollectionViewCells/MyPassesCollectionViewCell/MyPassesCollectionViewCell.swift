//
//  MyRewardsCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class MyPassesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var passesArray = Array<PassesDetails>()
    
    @IBOutlet weak var purchasebutton: SolidButton!
    @IBOutlet weak var purchaseMiddleButton: SolidButton!
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var purchaseButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var placeholderImgView: UIImageView!
    var isViewForPurchased = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configData(dataArray: Array<PassesDetails>, isPurchased: Bool) {
        passesArray = dataArray
        lblNoRecordFound.text = "No pass found Buy Now".localized()
        collectionView.register(UINib(nibName: "SeasonPassCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SeasonPassCollectionViewCell")
        isViewForPurchased = isPurchased
        if AppHelper.isApplicationRunningOnIphoneX() {
            purchaseButtonHeightConstraint.constant = 60
        }
        purchasebutton.layer.cornerRadius = 0
        purchasebutton.setTitle("Purchase Pass".localized(), for: .normal)
        purchaseMiddleButton.setTitle("Buy Pass".localized(), for: .normal)
       // purchasebutton.backgroundColor = 
        collectionView.reloadData()
    }
    
    @IBAction func purchaseButtonTapped(_ sender: Any) {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let passVC = storyboard.instantiateViewController(withIdentifier: "SessionPassesViewController") as! SessionPassesViewController
        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.pushViewController(passVC, animated: true)
        }
    }
    
}


extension MyPassesCollectionViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return passesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.frame.size.width/2
        return CGSize(width: cellWidth, height: cellWidth + 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeasonPassCollectionViewCell", for: indexPath) as! SeasonPassCollectionViewCell
        let details = passesArray[indexPath.item]
        cell.purchaseButton.tag = indexPath.row
        cell.configData(details: details, isPurchased: isViewForPurchased)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let details = passesArray[indexPath.item]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let passDetailsVC = storyboard.instantiateViewController(withIdentifier: "PassDetailsViewController") as! PassDetailsViewController
        passDetailsVC.passesDetails = details
        passDetailsVC.isPassPurchases = true
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(passDetailsVC, animated: true)
        }
    }
}
