//
//  QuizLeagueCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 02/04/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

let dynamicLeageTextColor = UIColor(red: 167.0/255, green: 91.0/255, blue: 242.0/255, alpha: 1)

let bonusQuizLeageTextColor = UIColor(red: 82.0/255, green: 201.0/255, blue: 135.0/255, alpha: 1)

let dynamicLeageBgColor = UIColor(red: 242.0/255, green: 230.0/255, blue: 255.0/255, alpha: 1)
let bonusQuizLeageBgColor = UIColor(red: 229.0/255, green: 250.0/255, blue: 238.0/255, alpha: 1)

class QuizLeagueCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var playButtonView: UIView!
    @IBOutlet weak var leagueNameTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblTotalPlayers: UILabel!
    @IBOutlet weak var lblPlay: UILabel!
    @IBOutlet weak var lblJoiningAmount: UILabel!
    @IBOutlet weak var playerButton: UIButton!
    @IBOutlet weak var lblQuestionCount: UILabel!
    @IBOutlet weak var lblBonusPercentage: UILabel!
    @IBOutlet weak var lblQuizName: UILabel!
    @IBOutlet weak var lblSecondTag: UILabel!
    @IBOutlet weak var lblFirstTag: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configData(details: LeagueDetails) {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblPlay.text = "Play".localized()

        lblQuizName.text = "pts" + (details.winAmount ?? "0") + " - " + (details.leagueName ?? "")
        
        lblJoiningAmount.text = "pts" + details.joiningAmount
        lblBonusPercentage.text = details.bounsPercentage + "percentage_Bonus".localized()

        let maxPlayers = Int(details.maxPlayers) ?? 0
        let totalQuestions = Int(details.totalQuestions) ?? 0
        
        if maxPlayers > 1 {
            lblTotalPlayers.text = "\(details.maxPlayers) \("Players".localized())"
        }
        else{
            lblTotalPlayers.text = "\(details.maxPlayers) \("Player".localized())"
        }

                
        if totalQuestions > 1 {
            lblQuestionCount.text = "\(details.totalQuestions) \("Questions".localized())"
        }
        else{
            lblQuestionCount.text = "\(details.totalQuestions) \("Questions".localized())"
        }
        if details.bonusApplicable == "2"{
            if (details.bounsPercentage == "") || (details.bounsPercentage == "0") {
                lblBonusPercentage.isHidden = true
            }
            else{
                lblBonusPercentage.isHidden = false
            }
        }
        else{
            lblBonusPercentage.isHidden = true
        }
                
        lblFirstTag.isHidden = true
        lblSecondTag.isHidden = true
        leagueNameTopConstraint.constant = 13
        if (details.dynamicLeague == "2") && (details.bonusApplicable == "2"){
            lblFirstTag.text = "D"
            lblSecondTag.text = "B"
            leagueNameTopConstraint.constant = 22
            lblFirstTag.textColor = dynamicLeageTextColor
            lblFirstTag.backgroundColor = dynamicLeageBgColor

            lblSecondTag.textColor = bonusQuizLeageTextColor
            lblSecondTag.backgroundColor = bonusQuizLeageBgColor

            lblFirstTag.isHidden = false
            lblSecondTag.isHidden = false
            lblFirstTag.roundCorners(corners: [.topLeft], radius: 7)
            lblSecondTag.roundCorners(corners: [.bottomRight], radius: 7)
        }
        else if (details.dynamicLeague == "2"){
            leagueNameTopConstraint.constant = 22

            lblFirstTag.text = "D"
            lblFirstTag.isHidden = false
            lblFirstTag.textColor = dynamicLeageTextColor
            lblFirstTag.backgroundColor = dynamicLeageBgColor

            lblFirstTag.roundCorners(corners: [.topLeft, .bottomRight], radius: 7)
        }
        else if (details.bonusApplicable == "2"){
            leagueNameTopConstraint.constant = 22

            lblFirstTag.text = "B"

            lblFirstTag.textColor = bonusQuizLeageTextColor
            lblFirstTag.backgroundColor = bonusQuizLeageBgColor

            lblFirstTag.isHidden = false
            lblFirstTag.roundCorners(corners: [.topLeft, .bottomRight], radius: 7)
        }
        self.layoutIfNeeded()
    }    
}
