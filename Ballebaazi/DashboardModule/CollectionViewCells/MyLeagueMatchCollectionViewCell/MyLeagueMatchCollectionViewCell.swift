//
//  MyLeagueMatchCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class MyLeagueMatchCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var PlacehlderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var placeholderImgView: UIImageView!
    @IBOutlet weak var lblNoMatchFound: UILabel!

    var liveLeagueMatchsArray = Array<MatchDetails>()
    var upcomingLeagueMatchsArrays = Array<MatchDetails>()
    var completedLeagueMatchsArrays = Array<MatchDetails>()
    
    var selectedGameTypeTab = GameType.Cricket.rawValue
    var timer: Timer?
    var isPullToRefresh = false
    @IBOutlet weak var noDataTopConstraint: NSLayoutConstraint!
    var refreshControl: UIRefreshControl?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @objc func refreshData(sender:AnyObject) {
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMyLeaguesOnPullToRefresh"), object: nil)
        refreshControl?.endRefreshing()
    }
    
    func configData(liveArray: Array<MatchDetails>?,upcomingArray: Array<MatchDetails>?, completedArray: Array<MatchDetails>?, gameType: Int) {
        
        if refreshControl == nil {
            refreshControl = UIRefreshControl()
            refreshControl?.tintColor = .clear
            refreshControl?.addTarget(self, action: #selector(refreshData(sender:)), for: UIControl.Event.valueChanged)
            tblView.addSubview(refreshControl!)
        }
        
        selectedGameTypeTab = gameType
        tblView.register(UINib(nibName: "MatchLeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "MatchLeagueTableViewCell")
        tblView.register(UINib(nibName: "TourneyTableViewCell", bundle: nil), forCellReuseIdentifier: "TourneyTableViewCell")

        if upcomingArray != nil{
            upcomingLeagueMatchsArrays = upcomingArray!
        }
        else{
            upcomingLeagueMatchsArrays.removeAll()
        }
        
        if liveArray != nil{
            liveLeagueMatchsArray = liveArray!
        }
        else{
            liveLeagueMatchsArray.removeAll()
        }

        if completedArray != nil{
            completedLeagueMatchsArrays = completedArray!
        }
        else{
            completedLeagueMatchsArrays.removeAll()
        }
        
        if upcomingLeagueMatchsArrays.count > 0 {
            startTimer()
        }
        else{
            timer?.invalidate()
            timer = nil
        }
        placeholderImgView.isHidden = true
        tblView.isHidden = false
        lblNoMatchFound.isHidden = true;
        lblNoMatchFound.text = ""
        PlacehlderHeightConstraint.constant = 295
        if (upcomingLeagueMatchsArrays.count == 0) && (liveLeagueMatchsArray.count == 0) && (completedLeagueMatchsArrays.count == 0){
            placeholderImgView.isHidden = false
            tblView.isHidden = true
            lblNoMatchFound.isHidden = false;

            if gameType == GameType.Cricket.rawValue{
                placeholderImgView.image = UIImage(named: "NoJoinedLeaguePlaceholder")
                lblNoMatchFound.text = "cricket_league".localized()
            }
            else if gameType == GameType.Kabaddi.rawValue{
                placeholderImgView.image = UIImage(named: "KabaddiPlaceholder")
                
                lblNoMatchFound.text = "Oops! No Kabaddi Matches Live as of now!".localized() + "\n" + "Make sure you check out Fantasy Cricket & Fantasy Football. Keep playing, Keep winning!".localized()

                lblNoMatchFound.text = "kabaddi_league".localized()
            }
            else if gameType == GameType.Football.rawValue{
                placeholderImgView.image = UIImage(named: "FootbaalPlaceHolder")
                lblNoMatchFound.text = "football_league".localized()
            }
            else if gameType == GameType.Quiz.rawValue{
                placeholderImgView.image = UIImage(named: "NoQuizFoundIcon")
                lblNoMatchFound.text = "Your performance in the previous games will be visible here. Stay tuned!".localized()
                PlacehlderHeightConstraint.constant = 160
            }
            else if gameType == GameType.Basketball.rawValue{
                placeholderImgView.image = UIImage(named: "BasketballNoMatchesAvilable")
                lblNoMatchFound.text = "basketball_league".localized()
                PlacehlderHeightConstraint.constant = 160
            }
            else if gameType == GameType.Baseball.rawValue{
                placeholderImgView.image = UIImage(named: "BaseballNoMatchesAvilable")
                lblNoMatchFound.text = "baseball_league".localized()
                PlacehlderHeightConstraint.constant = 160
            }            
        }
        
        lblNoMatchFound.layoutIfNeeded()
        placeholderImgView.layoutIfNeeded()
        tblView.reloadData()
        isPullToRefresh = false
    }

    
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if ((liveLeagueMatchsArray.count != 0) && (upcomingLeagueMatchsArrays.count != 0) && (completedLeagueMatchsArrays.count != 0)){
            return 3
        }
        else if ((liveLeagueMatchsArray.count != 0) && (upcomingLeagueMatchsArrays.count != 0)){
            return 2
        }
        else if ((upcomingLeagueMatchsArrays.count != 0) && (completedLeagueMatchsArrays.count != 0)){
            return 2
        }
        else if ((liveLeagueMatchsArray.count != 0) && (completedLeagueMatchsArrays.count != 0)){
            return 2
        }
        else if (liveLeagueMatchsArray.count != 0){
            return 1
        }
        else if (upcomingLeagueMatchsArrays.count != 0){
            return 1
        }
        else if (completedLeagueMatchsArrays.count != 0){
            return 1
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            if liveLeagueMatchsArray.count != 0 {
                return liveLeagueMatchsArray.count
            }
            else if upcomingLeagueMatchsArrays.count != 0 {
                return upcomingLeagueMatchsArrays.count
            }
            else if completedLeagueMatchsArrays.count != 0 {
                return completedLeagueMatchsArrays.count
            }
        }
        else if section == 1{
            
            if (liveLeagueMatchsArray.count != 0) && (upcomingLeagueMatchsArrays.count != 0) {
                return upcomingLeagueMatchsArrays.count
            }
            else if completedLeagueMatchsArrays.count != 0 {
                return completedLeagueMatchsArrays.count
            }
        }
        else if section == 2{
            return completedLeagueMatchsArrays.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        let titleLabel = UILabel(frame: CGRect(x: 12, y: 3, width: tableView.bounds.size.width, height: 22))
        titleLabel.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        titleLabel.font = UIFont(name: "OpenSans-Semibold", size: 15)
        headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        
        if section == 0{
            if liveLeagueMatchsArray.count != 0 {
                titleLabel.text = "Live Matches".localized()
            }
            else if upcomingLeagueMatchsArrays.count != 0 {
                titleLabel.text = "Upcoming Matches".localized()
            }
            else if completedLeagueMatchsArrays.count != 0 {
                titleLabel.text = "Concluded Matches".localized()
            }
        }
        else if section == 1{
            
            if (liveLeagueMatchsArray.count != 0) && (upcomingLeagueMatchsArrays.count != 0) {
                titleLabel.text = "Upcoming Matches".localized()
            }
            else if completedLeagueMatchsArrays.count != 0 {
                titleLabel.text = "Concluded Matches".localized()
            }
        }
        else if section == 2{
            titleLabel.text = "Concluded Matches".localized()
        }
        
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var contestType = 0
        var tagIndex = 2100000

        var mathchDetails: MatchDetails?

        if indexPath.section == 0{
            if liveLeagueMatchsArray.count != 0 {
                mathchDetails = liveLeagueMatchsArray[indexPath.row]
                contestType = LeagueContestType.LiveContest.rawValue
            }
            else if upcomingLeagueMatchsArrays.count != 0 {
                mathchDetails = upcomingLeagueMatchsArrays[indexPath.row]
                contestType = LeagueContestType.UpcomingContest.rawValue
                tagIndex = indexPath.row
            }
            else if completedLeagueMatchsArrays.count != 0 {
                mathchDetails = completedLeagueMatchsArrays[indexPath.row]
                contestType = LeagueContestType.CompletedContest.rawValue
            }
        }
        else if indexPath.section == 1{
            
            if (liveLeagueMatchsArray.count != 0) && (upcomingLeagueMatchsArrays.count != 0) {
                mathchDetails = upcomingLeagueMatchsArrays[indexPath.row]
                contestType = LeagueContestType.UpcomingContest.rawValue
                tagIndex = indexPath.row
            }
            else if completedLeagueMatchsArrays.count != 0 {
                mathchDetails = completedLeagueMatchsArrays[indexPath.row]
                contestType = LeagueContestType.CompletedContest.rawValue
            }
        }
        else if indexPath.section == 2{
            mathchDetails = completedLeagueMatchsArrays[indexPath.row]
            contestType = LeagueContestType.CompletedContest.rawValue
        }
        
        if mathchDetails?.isMatchTourney ?? false {
            var cell = tableView.dequeueReusableCell(withIdentifier: "TourneyTableViewCell") as? TourneyTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TourneyTableViewCell") as? TourneyTableViewCell
            }

            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.tag = tagIndex
            cell?.configData(matchDetails: mathchDetails!)
            
            return cell!
        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "MatchLeagueTableViewCell") as? MatchLeagueTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "MatchLeagueTableViewCell") as? MatchLeagueTableViewCell
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.tag = tagIndex

        if mathchDetails != nil{
            cell?.configData(matchDetails: mathchDetails!,contestType: contestType)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        var mathchDetails: MatchDetails?
        var isViewForLeagueFlow = false
        
//        {Current, Concluded.. }
        var occurence = ""
        
        if indexPath.section == 0{
            if liveLeagueMatchsArray.count != 0 {
                mathchDetails = liveLeagueMatchsArray[indexPath.row]
                isViewForLeagueFlow = true
            }
            else if upcomingLeagueMatchsArrays.count != 0 {
                mathchDetails = upcomingLeagueMatchsArrays[indexPath.row]
                if mathchDetails?.isMatchClosed ?? false{
                    isViewForLeagueFlow = true
                }
            }
            else if completedLeagueMatchsArrays.count != 0 {
                isViewForLeagueFlow = true
                mathchDetails = completedLeagueMatchsArrays[indexPath.row]
            }
        }
        else if indexPath.section == 1{
            
            if liveLeagueMatchsArray.count == 0 {
                isViewForLeagueFlow = true
                mathchDetails = completedLeagueMatchsArrays[indexPath.row]
            }
            else if upcomingLeagueMatchsArrays.count != 0 {
                mathchDetails = upcomingLeagueMatchsArrays[indexPath.row]
                if mathchDetails?.isMatchClosed ?? false{
                    isViewForLeagueFlow = true
                }
            }
            else if completedLeagueMatchsArrays.count != 0 {
                isViewForLeagueFlow = true
                mathchDetails = completedLeagueMatchsArrays[indexPath.row]
            }
        }
        else if indexPath.section == 2{
            mathchDetails = completedLeagueMatchsArrays[indexPath.row]
            isViewForLeagueFlow = true
        }
        
        var sportType = ""

        if selectedGameTypeTab == GameType.Cricket.rawValue{
            sportType = "Cricket"
            let joinedWinnerRankVC = storyboard.instantiateViewController(withIdentifier: "JoinedLeagueViewController") as! JoinedLeagueViewController
            joinedWinnerRankVC.selectedFantasyType = FantasyType.Classic.rawValue
            joinedWinnerRankVC.matchDetails = mathchDetails
            joinedWinnerRankVC.isViewForLeagueFlow = isViewForLeagueFlow

            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(joinedWinnerRankVC, animated: true)
            }
        }
        else if selectedGameTypeTab == GameType.Kabaddi.rawValue{
            sportType = "Kabaddi"
            let joinedWinnerRankVC = storyboard.instantiateViewController(withIdentifier: "JoinedKabaddiLeagueViewController") as! JoinedKabaddiLeagueViewController
            joinedWinnerRankVC.matchDetails = mathchDetails
            joinedWinnerRankVC.isViewForLeagueFlow = isViewForLeagueFlow
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(joinedWinnerRankVC, animated: true)
            }
        }
        else if selectedGameTypeTab == GameType.Football.rawValue{
            sportType = "Football"
            let joinedWinnerRankVC = storyboard.instantiateViewController(withIdentifier: "FootballJoinedLeagueViewController") as! FootballJoinedLeagueViewController
            joinedWinnerRankVC.matchDetails = mathchDetails
            joinedWinnerRankVC.isViewForLeagueFlow = isViewForLeagueFlow
            
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(joinedWinnerRankVC, animated: true)
            }
        }
        else if selectedGameTypeTab == GameType.Basketball.rawValue{
            sportType = "Basketball"
            let joinedWinnerRankVC = storyboard.instantiateViewController(withIdentifier: "JoinedBasketballLeaguesViewController") as! JoinedBasketballLeaguesViewController
            joinedWinnerRankVC.matchDetails = mathchDetails
            joinedWinnerRankVC.isViewForLeagueFlow = isViewForLeagueFlow
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(joinedWinnerRankVC, animated: true)
            }
        }
        else if selectedGameTypeTab == GameType.Baseball.rawValue{
            sportType = "Baseball"
            let joinedWinnerRankVC = storyboard.instantiateViewController(withIdentifier: "JoinedBaseballLeaguesViewController") as! JoinedBaseballLeaguesViewController
            joinedWinnerRankVC.matchDetails = mathchDetails
            joinedWinnerRankVC.isViewForLeagueFlow = isViewForLeagueFlow
            
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(joinedWinnerRankVC, animated: true)
            }
        }
        
        AppxorEventHandler.logAppEvent(withName: "MatchClicked", info: ["MatchID": mathchDetails!.matchKey, "TeamName": mathchDetails!.matchShortName ?? "", "SportType": sportType, "SeriesName": mathchDetails!.seasonKey ?? "", "Occurence": occurence ])

    }
    
    
    // MARK:- Timer Handlers
    func startTimer()  {
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTimerVlaue()  {
        
        let visibleCellsArray = tblView.visibleCells
        for visibleCell  in visibleCellsArray{
            if let cell = visibleCell as? MatchLeagueTableViewCell{
                var mathchDetails: MatchDetails?
                
                if cell.tag != 2100000{
                    if upcomingLeagueMatchsArrays.count > cell.tag{
                        mathchDetails = upcomingLeagueMatchsArrays[cell.tag]
                        if mathchDetails != nil{
                            let raminingTime = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: mathchDetails?.startDateTimestemp)
                            if raminingTime.count == 0 && !mathchDetails!.isMatchClosed{
//                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeUpcomingMatchFromLocal"), object: mathchDetails)
                            }
                        }
                        
                        cell.configData(matchDetails: mathchDetails!,contestType: LeagueContestType.UpcomingContest.rawValue)
                    }
                }
            }
        }
    }

    
}
