//
//  SingleLeagueCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 28/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class SingleLeagueCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ticketImgView: UIImageView!
    @IBOutlet weak var lblBonusTimer: UILabel!

    @IBOutlet weak var jackpotImgView: UIImageView!
    @IBOutlet weak var infinityWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var enteryView: UIView!
    @IBOutlet weak var lblTicketAvailable: UILabel!
    @IBOutlet weak var rightArrow: UIImageView!
    
    @IBOutlet weak var lblJoinedText: UILabel!
    @IBOutlet weak var lblBounsPerentage: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblTotalWinners: UILabel!
    @IBOutlet weak var lblJoiningAmount: UILabel!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var teamPogressView: UIView!
    @IBOutlet weak var progressViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var lblFirstMatchType: UILabel!
    @IBOutlet var lblSecondMatchType: UILabel!
    @IBOutlet var lblThirdMatchType: UILabel!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var lblJoinTitle: UILabel!
    @IBOutlet weak var lblCurrentTeams: UILabel!
    @IBOutlet weak var lblTotalTeams: UILabel!
    
    @IBOutlet weak var leagueWatchIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func changeShadowAndBackgroundButtonView(color: UIColor)  {
        enteryView.backgroundColor = color;
    }
    
    func configData(details: LeagueDetails, matchDetails: MatchDetails, gameType: Int)  {
        
        jackpotImgView.isHidden = true
        lblBonusTimer.textColor = bonusLeageTextColor
        lblBonusTimer.textColor = confirmLeageTextColor
        
        if details.isJackpot == "1"{
            jackpotImgView.isHidden = false
        }
        var maxTeamAllow = 0;
        if details.fantasyType == "1"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForClassic;
            
            if gameType == GameType.Cricket.rawValue {
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForClassic;
            }
            else if gameType == GameType.Kabaddi.rawValue {
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForKabaddi;
            }
            else if gameType == GameType.Football.rawValue {
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForFootball;
            }
            else if gameType == GameType.Basketball.rawValue {
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBasketball;
            }
            else if gameType == GameType.Baseball.rawValue {
                maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBaseball;
            }
        }
        else if details.fantasyType == "2"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBatting;
        }else if details.fantasyType == "3"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForBowling;
        }
        else if details.fantasyType == "4"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForReverse;
        }
        else if details.fantasyType == "5"{
            maxTeamAllow = UserDetails.sharedInstance.maxTeamAllowedForWizard;
        }

        
        AppHelper.showShodowOnLeagueCellsView(innerView: innerView)
        lblBounsPerentage.textColor = bonusLeageTextColor

        progressViewWidthConstraint.constant = 0.0;
        teamPogressView.layer.cornerRadius = 2.0
        teamPogressView.clipsToBounds = true;
        progressView.layer.cornerRadius = 2.0
        progressView.clipsToBounds = true;
        teamPogressView.layoutIfNeeded()
        lblFirstMatchType.isHidden = true
        lblSecondMatchType.isHidden = true
        lblThirdMatchType.isHidden = true
        lblJoiningAmount.isHidden = false;
        lblBounsPerentage.text = ""
        var appliedBounsPercentage = ""
        
         if (details.timeBasedBonusArray.count != 0) && (details.bonusApplicable == "2"){
               
            appliedBounsPercentage = details.bounsPercentage
            lblBounsPerentage.text = details.bounsPercentage + "percentage_Bonus".localized()
            lblBonusTimer.text = ""
            
             let serverUnixTimestamp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
             let dateUnixTimestamp = Double(matchDetails.startDateTimestemp ?? "0") ?? 0
             
             var matchClosingSeconds = 0.0
             
             if dateUnixTimestamp > serverUnixTimestamp {
                 matchClosingSeconds = dateUnixTimestamp - serverUnixTimestamp
             }

             for bonusDetails in details.timeBasedBonusArray{
            
                let defaultBouns = bonusDetails["tbm"].string ?? ""
                if(defaultBouns == "default")   {
                    continue
                }

                 let remaingTimeStr = bonusDetails["tbm"].stringValue
                 let remainingTime = (Double(remaingTimeStr) ?? 0)*60
                 if matchClosingSeconds > remainingTime {
                     let bounsPercentage = bonusDetails["bp"].stringValue
                    if (bounsPercentage != "0") && (bounsPercentage != "default")  {
                                   
                        appliedBounsPercentage = bounsPercentage
                        lblBounsPerentage.text = bounsPercentage + "percentage_Bonus".localized()
                        lblBonusTimer.text = AppHelper.convertRemaingTimeIntoSeconds(totalSeconds:  (matchClosingSeconds - remainingTime))

                    }
                     break;
                 }
             }
         }
         else{
            appliedBounsPercentage = details.bounsPercentage
             lblBounsPerentage.text = details.bounsPercentage + "percentage_Bonus".localized()
             lblBonusTimer.text = ""
         }
        
        
        if details.leagueType == "2" {
            lblTotalWinners.isHidden = true
        }
        else{
            lblTotalWinners.isHidden = false
        }
        
        lblJoinedText.isHidden = true
        rightArrow.isHidden = false
        lblJoinTitle.isHidden = false
        
        lblTicketAvailable.isHidden = true;
        ticketImgView.isHidden = true;
        
  
        
  
        if (details.teamType == "1") {
            if details.joinedLeagueCount == maxTeamAllow{
                lblJoinTitle.text = "Joined".localized()
                lblJoinedText.text = "Joined".localized()
                lblJoinedText.isHidden = false
                rightArrow.isHidden = true
                lblJoinTitle.isHidden = true
                lblJoiningAmount.isHidden = true;
                lblTicketAvailable.isHidden = true;
                ticketImgView.isHidden = true;

                changeShadowAndBackgroundButtonView(color: UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1));
            }
            else if details.joinedLeagueCount > 0{
                lblJoinTitle.text = "Rejoin".localized()
                if details.isTicketAvailable {
                    lblTicketAvailable.isHidden = false;
                    ticketImgView.isHidden = false;
                    ticketImgView.image = UIImage(named: "FreeTicketIcon")
                }
                
                if details.isPassAvailable {
                    lblTicketAvailable.isHidden = false;
                    ticketImgView.isHidden = false;
                    ticketImgView.image = UIImage(named: "PassIcon")
                }

                
                changeShadowAndBackgroundButtonView(color: UIColor(red: 255.0/255, green: 144.0/255, blue: 89/255, alpha: 1));
            }
            else{
                lblJoinTitle.text = "Join".localized()
                if details.isTicketAvailable {
                    lblTicketAvailable.isHidden = false;
                    ticketImgView.isHidden = false;
                    ticketImgView.image = UIImage(named: "FreeTicketIcon")
                }
                
                if details.isPassAvailable {
                    lblTicketAvailable.isHidden = false;
                    ticketImgView.isHidden = false;
                    ticketImgView.image = UIImage(named: "PassIcon")
                }

                if details.isJackpot == "1"{
                    changeShadowAndBackgroundButtonView(color: UIColor(red: 238/255, green: 159/255, blue: 64/255, alpha: 1));
                }else{
                    changeShadowAndBackgroundButtonView(color: UIColor(red: 0/255, green: 167/255, blue: 80/255, alpha: 1));
                }
            }
        }
        else if (details.teamType == "2") {
            if details.joinedLeagueCount > 0{
                lblJoinTitle.text = "Joined".localized()
                lblJoinedText.text = "Joined".localized()
                lblJoinedText.isHidden = false
                rightArrow.isHidden = true
                lblJoinTitle.isHidden = true
                lblJoiningAmount.isHidden = true;
                lblTicketAvailable.isHidden = true;
                ticketImgView.isHidden = true;

                changeShadowAndBackgroundButtonView(color: UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1));
            }
            else{
                lblJoinTitle.text = "Join".localized()
                if details.isTicketAvailable {
                    lblTicketAvailable.isHidden = false;
                    ticketImgView.isHidden = false;
                    ticketImgView.image = UIImage(named: "FreeTicketIcon")
                }
                
                if details.isPassAvailable {
                    lblTicketAvailable.isHidden = false;
                    ticketImgView.isHidden = false;
                    ticketImgView.image = UIImage(named: "PassIcon")
                }

                if details.isJackpot == "1"{
                    changeShadowAndBackgroundButtonView(color: UIColor(red: 238/255, green: 159/255, blue: 64/255, alpha: 1));
                }else{
                    changeShadowAndBackgroundButtonView(color: UIColor(red: 0/255, green: 167/255, blue: 80/255, alpha: 1));
                }
            }
        }
        else if (details.confirmedLeague == "2"){
            if details.joinedLeagueCount > 0{
                lblJoinTitle.text = "Joined".localized()
                lblJoinedText.text = "Joined".localized()
                lblJoinedText.isHidden = false
                rightArrow.isHidden = true
                lblJoinTitle.isHidden = true
                lblJoiningAmount.isHidden = true;
                lblTicketAvailable.isHidden = true;
                ticketImgView.isHidden = true;

                changeShadowAndBackgroundButtonView(color: UIColor(red: 253.0/255, green: 131.0/255, blue: 116.0/255, alpha: 1));
            }
            else{
                lblJoinTitle.text = "Join".localized()
                if details.isTicketAvailable {
                    lblTicketAvailable.isHidden = false;
                    ticketImgView.isHidden = false;
                    ticketImgView.image = UIImage(named: "FreeTicketIcon")
                }
                
                if details.isPassAvailable {
                    lblTicketAvailable.isHidden = false;
                    ticketImgView.isHidden = false;
                    ticketImgView.image = UIImage(named: "PassIcon")
                }

                
                if details.isJackpot == "1"{
                    changeShadowAndBackgroundButtonView(color: UIColor(red: 238/255, green: 159/255, blue: 64/255, alpha: 1));
                }else{
                    changeShadowAndBackgroundButtonView(color: UIColor(red: 0/255, green: 167/255, blue: 80/255, alpha: 1));
                }
            }
        }
        else{
            lblJoinTitle.text = "Join".localized()
            if details.isTicketAvailable {
                lblTicketAvailable.isHidden = false;
                ticketImgView.isHidden = false;
                ticketImgView.image = UIImage(named: "FreeTicketIcon")
            }
            
            if details.isPassAvailable {
                lblTicketAvailable.isHidden = false;
                ticketImgView.isHidden = false;
                ticketImgView.image = UIImage(named: "PassIcon")
            }

            
            if details.isJackpot == "1"{
                changeShadowAndBackgroundButtonView(color: UIColor(red: 238/255, green: 159/255, blue: 64/255, alpha: 1));
            }else{
                changeShadowAndBackgroundButtonView(color: UIColor(red: 0/255, green: 167/255, blue: 80/255, alpha: 1));
            }
        }
        
        lblBounsPerentage.isHidden = true
        lblBonusTimer.isHidden = true
        leagueWatchIcon.isHidden = true
        
        lblFirstMatchType.layer.mask = nil
        lblSecondMatchType.layer.mask = nil
        lblThirdMatchType.layer.mask = nil
        
        if (details.bonusApplicable == "2") && (details.teamType == "1") && (details.confirmedLeague == "2"){
            lblFirstMatchType.roundCorners(corners: .topLeft, radius: 7)
            lblThirdMatchType.roundCorners(corners: .bottomRight, radius: 7)
            lblThirdMatchType.text = "B"
            lblSecondMatchType.text = "M"
            lblFirstMatchType.text = "C"
            lblBounsPerentage.isHidden = false
            lblBonusTimer.isHidden = false
            lblFirstMatchType.isHidden = false
            lblSecondMatchType.isHidden = false
            lblThirdMatchType.isHidden = false
            
            lblThirdMatchType.textColor = bonusLeageTextColor
            lblSecondMatchType.textColor = multipleTeamLeageTextColor
            lblFirstMatchType.textColor = confirmLeageTextColor
            
            lblThirdMatchType.backgroundColor = bonusLeageColor
            lblSecondMatchType.backgroundColor = multipleTeamLeageColor
            lblFirstMatchType.backgroundColor = confirmLeageColor
        }
        else if (details.bonusApplicable == "2") && (details.teamType == "1"){
            lblSecondMatchType.text = "B"
            lblFirstMatchType.text = "M"
            lblFirstMatchType.roundCorners(corners: .topLeft, radius: 7)
            lblSecondMatchType.roundCorners(corners: .bottomRight, radius: 7)
            
            lblSecondMatchType.textColor = bonusLeageTextColor
            lblFirstMatchType.textColor = multipleTeamLeageTextColor
            
            lblFirstMatchType.isHidden = false
            lblSecondMatchType.isHidden = false
            lblBounsPerentage.isHidden = false
            lblBonusTimer.isHidden = false
//            leagueWatchIcon.isHidden = false
            lblSecondMatchType.backgroundColor = bonusLeageColor
            lblFirstMatchType.backgroundColor = multipleTeamLeageColor
        }
        else if (details.teamType == "1") && (details.confirmedLeague == "2"){
            lblSecondMatchType.text = "M"
            lblFirstMatchType.text = "C"
            
            lblSecondMatchType.textColor = multipleTeamLeageTextColor
            lblFirstMatchType.textColor = confirmLeageTextColor
            
            lblFirstMatchType.roundCorners(corners: .topLeft, radius: 7)
            lblSecondMatchType.roundCorners(corners: .bottomRight, radius: 7)
            lblSecondMatchType.isHidden = false
            lblFirstMatchType.isHidden = false
            
            lblSecondMatchType.backgroundColor = multipleTeamLeageColor
            lblFirstMatchType.backgroundColor = confirmLeageColor
        }
        else if (details.bonusApplicable == "2") && (details.confirmedLeague == "2"){
            
            lblSecondMatchType.textColor = bonusLeageTextColor
            lblFirstMatchType.textColor = confirmLeageTextColor
            
            lblSecondMatchType.text = "B"
            lblFirstMatchType.text = "C"
            lblFirstMatchType.roundCorners(corners: .topLeft, radius: 7)
            lblSecondMatchType.roundCorners(corners: .bottomRight, radius: 7)
            lblFirstMatchType.isHidden = false
            lblSecondMatchType.isHidden = false
            lblBounsPerentage.isHidden = false
            lblBonusTimer.isHidden = false
            lblSecondMatchType.backgroundColor = bonusLeageColor
            lblFirstMatchType.backgroundColor = confirmLeageColor
        }
        else if (details.bonusApplicable == "2"){
            lblFirstMatchType.text = "B"
            lblFirstMatchType.textColor = bonusLeageTextColor
            
            lblFirstMatchType.isHidden = false
            lblBounsPerentage.isHidden = false
            lblBonusTimer.isHidden = false
            lblFirstMatchType.backgroundColor = bonusLeageColor
            lblFirstMatchType.roundCorners(corners: [.topLeft, .bottomRight], radius: 7)
        }
        else if (details.teamType == "1"){
            lblFirstMatchType.text = "M"
            
            lblFirstMatchType.textColor = multipleTeamLeageTextColor
            
            lblFirstMatchType.isHidden = false
            lblFirstMatchType.backgroundColor = multipleTeamLeageColor
            lblFirstMatchType.roundCorners(corners: [.topLeft, .bottomRight], radius: 7)
        }
        else if (details.confirmedLeague == "2"){
            lblFirstMatchType.text = "C"
            lblFirstMatchType.textColor = confirmLeageTextColor
            
            lblFirstMatchType.isHidden = false
            lblFirstMatchType.backgroundColor = confirmLeageColor
            lblFirstMatchType.roundCorners(corners: [.topLeft, .bottomRight], radius: 7)
        }
        
        if (details.leagueType == "2"){
            if (details.confirmedLeague == "2"){
                lblFirstMatchType.text = "C"
                lblFirstMatchType.textColor = confirmLeageTextColor
                lblFirstMatchType.isHidden = false
                lblFirstMatchType.backgroundColor = confirmLeageColor
                lblSecondMatchType.isHidden = true
                lblThirdMatchType.isHidden = true
                lblBounsPerentage.isHidden = true
                lblBonusTimer.isHidden = true
                leagueWatchIcon.isHidden = true
                lblFirstMatchType.roundCorners(corners: [.topLeft, .bottomRight], radius: 7)
            }
            else{
                lblFirstMatchType.isHidden = true
                lblSecondMatchType.isHidden = true
                lblThirdMatchType.isHidden = true
                lblBounsPerentage.isHidden = true
                lblBonusTimer.isHidden = true
                leagueWatchIcon.isHidden = true
            }
        }
        
        if (details.leagueType == "3"){
            lblFirstMatchType.isHidden = true
            lblSecondMatchType.isHidden = true
            lblThirdMatchType.isHidden = true
            lblBounsPerentage.isHidden = true
            lblBonusTimer.isHidden = true
            leagueWatchIcon.isHidden = true
        }
        
        if let leagueName = details.leagueName {
            lblTeam.text = leagueName;
        }

        lblJoiningAmount.text = "" + AppHelper.makeCommaSeparatedDigitsWithString(digites: details.joiningAmount)

        
        if details.isInfinity == "1"{
            if (details.leagueWinnerType == "dynamic_winner") {
                lblTotalWinners.text = details.totalWinnersPercent + "%" + " " + "Winners".localized()
            }
            else{
                if Int(details.totalWinners) ?? 0 > 1{
                    lblTotalWinners.text = details.totalWinners + " " + "Winners".localized()
                }
                else{
                    lblTotalWinners.text = details.totalWinners + " " + "Winner".localized()
                }
            }
            lblTotalTeams.text = "/∞"
        }
        else
        {
            if Int(details.totalWinners) ?? 0 > 1{
                lblTotalWinners.text = details.totalWinners + " " + "Winners".localized()
            }
            else{
                lblTotalWinners.text = details.totalWinners + " " + "Winner".localized()
            }
            
            lblTotalTeams.text = "/"+AppHelper.makeCommaSeparatedDigitsWithString(digites: details.maxPlayers)
        }
        guard let totalJoined = NumberFormatter().number(from: details.totalJoined) else {
            return
        }
        
        guard let maxPlayers = NumberFormatter().number(from: details.maxPlayers) else {
            return
        }
        
        lblCurrentTeams.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: String(details.totalJoined))
        
        if details.isInfinity == "1"{
            lblProgress.isHidden = false
            progressView.isHidden = false
            infinityWidthConstraint.constant = 18.0
            
            teamPogressView.backgroundColor = UIColor(red: 255.0/255, green: 115.0/255, blue: 80.0/255, alpha: 1)
            let constantValue = 0.7 * (UIScreen.main.bounds.size.width - 205.0)
            progressViewWidthConstraint.constant = constantValue;
            teamPogressView.layoutIfNeeded()
            layoutIfNeeded()
        }
        else
        {
            lblProgress.isHidden = true
            progressView.isHidden = false
            infinityWidthConstraint.constant = 0.0
            teamPogressView.isHidden = false
            let leagueFilledPercentage = (CGFloat(truncating: totalJoined) * CGFloat(100) / CGFloat(truncating: maxPlayers))
            if leagueFilledPercentage <= 20{
                teamPogressView.backgroundColor = UIColor(red: 255.0/255, green: 205.0/255, blue: 127.0/255, alpha: 1)
            }
            else if leagueFilledPercentage <= 40{
                teamPogressView.backgroundColor = UIColor(red: 255.0/255, green: 168.0/255, blue: 106/255, alpha: 1)
            }
            else if leagueFilledPercentage <= 60{
                teamPogressView.backgroundColor = UIColor(red: 255.0/255, green: 147.0/255, blue: 119.0/255, alpha: 1)
            }
            else if leagueFilledPercentage <= 80{
                teamPogressView.backgroundColor = UIColor(red: 255.0/255, green: 115.0/255, blue: 80.0/255, alpha: 1)
            }
            else {
                teamPogressView.backgroundColor = UIColor(red: 241.0/255, green: 94.0/255, blue: 56.0/255, alpha: 1)
            }

            let constantValue = (CGFloat(truncating: totalJoined)/CGFloat(truncating: maxPlayers)) * (UIScreen.main.bounds.size.width - 187.0)
            progressViewWidthConstraint.constant = constantValue;
            teamPogressView.layoutIfNeeded()
            layoutIfNeeded()
        }

        if (appliedBounsPercentage == "0") || (appliedBounsPercentage == ""){
             lblBounsPerentage.isHidden = true
             lblBonusTimer.isHidden = true
             lblThirdMatchType.isHidden = true
         }
    }
}
