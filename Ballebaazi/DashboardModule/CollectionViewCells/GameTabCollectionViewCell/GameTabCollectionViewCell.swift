//
//  GameTabCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 30/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class GameTabCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lineUpsView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
