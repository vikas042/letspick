//
//  FooterCollectionReusableView.swift
//  Letspick
//
//  Created by Vikash Rajput on 27/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class FooterCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var lblViewMore: UILabel!
    @IBOutlet weak var viewMoreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
