//
//  SeasonPassImageCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class SeasonPassImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTotalPass: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configData(details: PassesDetails) {
        
        AppHelper.showShodowOnCellsView(innerView: innerView)
        
        lblTotalPass.text = details.totalMatches + " " + "MATCHES".localized()

        if details.image.count < 10 {
              self.imgView.image = UIImage(named: "Placeholder")
          }
          else if let url = NSURL(string: details.image){
              imgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
              }, completion: { [weak self] image in
                  if (image != nil){
                      self?.imgView.image = image
                  }
                  else{
                      self?.imgView.image = UIImage(named: "Placeholder")
                  }
              })
          }
          else{
              self.imgView.image = UIImage(named: "Placeholder")
          }
    }
}
