//
//  SwapeKabaddiCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 02/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class SwapeKabaddiCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var captainSepImgView1: UIImageView!
    @IBOutlet weak var captainSepImgView2: UIImageView!


    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var lblRaiderTitle: UILabel!
    @IBOutlet weak var lblAllRounderTitle: UILabel!
    @IBOutlet weak var lblDefenderTitle: UILabel!
    @IBOutlet weak var lblViceCaptain: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblRaiderCount: UILabel!
    @IBOutlet weak var lblAllrounderCount: UILabel!
    @IBOutlet weak var lblDefenderCount: UILabel!
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var teamPreviewButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var lblVicecaptainTitle: UILabel!
    @IBOutlet weak var lblCaptainTitle: UILabel!
    
    @IBOutlet weak var lblTeamNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(_ details: UserTeamDetails, selectedTeam: UserTeamDetails?) {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblTeamNumber.text = "Team".localized().uppercased() + " " + details.teamNumber!
        innerView.clipsToBounds = true
        lblCaptain.text = details.captionName
        lblViceCaptain.text = details.viceCaptionName
        lblAllrounderCount.text = details.totalAllrounderCount
        lblRaiderCount.text = details.totalRaiderCount
        lblDefenderCount.text = details.totalDefenderCount

        lblCaptainTitle.text = "Captain".localized()
        lblVicecaptainTitle.text = "subCaptain".localized()

        lblAllRounderTitle.text = "ALR".localized()
        lblRaiderTitle.text = "RAI".localized()
        lblDefenderTitle.text = "DEF".localized()
        
        if details.teamNumber == selectedTeam?.teamNumber {
            showViewForSelected()
        }
        else{
            showViewForUnselected()
        }
    }
    
    func showViewForSelected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgBlueIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorBlueIcon")
        headerContentView.backgroundColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)
        innerView.backgroundColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1)
        lblRaiderTitle.textColor = UIColor.white
        lblAllRounderTitle.textColor = UIColor.white
        lblDefenderTitle.textColor = UIColor.white
        lblRaiderTitle.textColor = UIColor.white
        lblTeamNumber.textColor = UIColor.white
        lblCaptain.textColor = UIColor.white
        lblViceCaptain.textColor = UIColor.white
        
        lblCaptainTitle.textColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1)
        lblVicecaptainTitle.textColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1)
        lblAllrounderCount.textColor = UIColor.white
        lblDefenderCount.textColor = UIColor.white
        lblRaiderCount.textColor = UIColor.white
        lblAllrounderCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblDefenderCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblRaiderCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        selectButton.isSelected = true;
    }
    
    func showViewForUnselected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorIcon")

        lblTeamNumber.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        headerContentView.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        innerView.backgroundColor = UIColor.white
        lblRaiderTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblAllRounderTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblDefenderTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblRaiderTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        lblViceCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        
        lblCaptainTitle.textColor = UIColor.white
        
        lblVicecaptainTitle.textColor = UIColor.white
        
        lblAllrounderCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblDefenderCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblRaiderCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        
        lblAllrounderCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblDefenderCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblRaiderCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        
        selectButton.isSelected = false;
    }
    
}
