//
//  HomeMatchCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class HomeMatchCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {

    @IBOutlet weak var placeholderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var noDataTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblNoMatchFound: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var placeholderImgView: UIImageView!
    
    lazy var homeMatchsArray = Array<MatchDetails>()
    lazy var bannerArray = Array<BannerDetails>()
    var refreshControl: UIRefreshControl?

    var selectedGameTypeTab = GameType.Cricket.rawValue
    var timer: Timer?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @objc func refreshData(sender:AnyObject) {
        refreshControl?.endRefreshing()
        if homeMatchsArray.count == 0 {
            return
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateListOnPullToRefresh"), object: nil)
    }

    func configData(matchArray: Array<MatchDetails>?, bannersList: Array<BannerDetails>? ,gameType: Int) {
        selectedGameTypeTab = gameType

        if bannersList != nil{
            bannerArray = bannersList!
        }else{
            bannerArray.removeAll()
        }
        
        if refreshControl == nil {
            refreshControl = UIRefreshControl()
            refreshControl?.tintColor = .clear
            refreshControl?.addTarget(self, action: #selector(refreshData(sender:)), for: UIControl.Event.valueChanged)
            tblView.addSubview(refreshControl!)
        }

        tblView.register(UINib(nibName: "CricketBannerTableViewCell", bundle: nil), forCellReuseIdentifier: "CricketBannerTableViewCell")
        tblView.register(UINib(nibName: "MatchTableViewCell", bundle: nil), forCellReuseIdentifier: "MatchTableViewCell")
        tblView.register(UINib(nibName: "TourneyTableViewCell", bundle: nil), forCellReuseIdentifier: "TourneyTableViewCell")
        tblView.register(UINib(nibName: "QuizMatchTableViewCell", bundle: nil), forCellReuseIdentifier: "QuizMatchTableViewCell")

        if matchArray != nil{
            homeMatchsArray = matchArray!
        }
        else{
            homeMatchsArray.removeAll()
        }
        
        tblView.isHidden = false
        placeholderImgView.isHidden = true
        lblNoMatchFound.text = "no_match_found".localized()
        lblNoMatchFound.isHidden = true
        if UIScreen.main.bounds.width <= 375.0 {
            placeholderHeightConstraint.constant = 250
        }
        else{
            placeholderHeightConstraint.constant = 290
        }
        if homeMatchsArray.count == 0{
            placeholderImgView.isHidden = false
            lblNoMatchFound.isHidden = false
            if gameType == GameType.Cricket.rawValue{
                placeholderImgView.image = UIImage(named: "NoJoinedLeaguePlaceholder")
            }
            else if gameType == GameType.Kabaddi.rawValue{
                placeholderImgView.image = UIImage(named: "KabaddiPlaceholder")
                lblNoMatchFound.text = "Oops! No Kabaddi Matches Live as of now!".localized() + "\n" + "Make sure you check out Fantasy Cricket & Fantasy Football. Keep playing, Keep winning!".localized()                
            }
            else if gameType == GameType.Football.rawValue{
                placeholderImgView.image = UIImage(named: "FootbaalPlaceHolder")
            }
            else if gameType == GameType.Quiz.rawValue{
                placeholderImgView.image = UIImage(named: "NoQuizFoundIcon")
                lblNoMatchFound.text = "no_Quiz_found".localized()
                placeholderHeightConstraint.constant = 160
            }
            else if gameType == GameType.Basketball.rawValue{
                placeholderImgView.image = UIImage(named: "BasketballNoMatchesAvilable")
            }
            else if gameType == GameType.Baseball.rawValue{
                placeholderImgView.image = UIImage(named: "BaseballNoMatchesAvilable")
            }

        }
        
        lblNoMatchFound.layoutIfNeeded()
        placeholderImgView.layoutIfNeeded()
        tblView.reloadData()
        startTimer()
         setupLongPressGesture()
    }
    
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.tblView.addGestureRecognizer(longPressGesture)
    }

    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if (selectedGameTypeTab == GameType.Cricket.rawValue) || selectedGameTypeTab == GameType.Football.rawValue{
            if gestureRecognizer.state == .began {
                let touchPoint = gestureRecognizer.location(in: self.tblView)
                if let indexPath = tblView.indexPathForRow(at: touchPoint) {
                    let matchDetails = homeMatchsArray[indexPath.row]
                    if matchDetails.active == "2" {
                        return
                    }

                    let longPressView = MatchLongPressView(frame: APPDELEGATE.window!.frame)
                    longPressView.updateDetails(matchDetails: matchDetails, gameType: selectedGameTypeTab)
                    APPDELEGATE.window?.addSubview(longPressView)
                }
            }
        }
    }

    //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        if (bannerArray.count > 0) && homeMatchsArray.count > 0{
            return 2
        }
        else if bannerArray.count > 0{
            return 1
        }
        else if homeMatchsArray.count > 0{
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if bannerArray.count > 0{
                return 1
            }
        }
        return homeMatchsArray.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if bannerArray.count > 0{
                return 90.0
            }
        }
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            if bannerArray.count > 0{
                return 0
            }
        }
        return 30

    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            if bannerArray.count > 0{
                return nil
            }
        }
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        let titleLabel = UILabel(frame: CGRect(x: 12, y: 3, width: tableView.bounds.size.width, height: 22))
        titleLabel.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        titleLabel.font = UIFont(name: "OpenSans-Semibold", size: 15)
        headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        if selectedGameTypeTab == GameType.Quiz.rawValue {
            titleLabel.text = "Quiz Matches".localized()
        }
        else{
            titleLabel.text = "Upcoming Matches".localized()
        }

        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if bannerArray.count > 0 {
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "CricketBannerTableViewCell") as? CricketBannerTableViewCell
                if cell == nil {
                    cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "CricketBannerTableViewCell") as? CricketBannerTableViewCell
                }
                
                cell?.configData(bannerList: bannerArray);
                return cell!
            }
        }
        
        let details = homeMatchsArray[indexPath.row]
        if details.isMatchTourney {
            var cell = tableView.dequeueReusableCell(withIdentifier: "TourneyTableViewCell") as? TourneyTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TourneyTableViewCell") as? TourneyTableViewCell
            }

            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.tag = indexPath.row
            cell?.configData(matchDetails: details)
            
            return cell!
        }
        else if details.isQuizMatch {
            var cell = tableView.dequeueReusableCell(withIdentifier: "QuizMatchTableViewCell") as? QuizMatchTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "QuizMatchTableViewCell") as? QuizMatchTableViewCell
            }

            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.tag = indexPath.row
            cell?.configData(details: details)
            
            return cell!
        }

        var cell = tableView.dequeueReusableCell(withIdentifier: "MatchTableViewCell") as? MatchTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "MatchTableViewCell") as? MatchTableViewCell
        }

        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.tag = indexPath.row
        cell?.configData(matchDetails: details)
        cell!.freeTicketView.isHidden = true
        
        if details.isPassAvailable{
            cell!.freeTicketView.isHidden = false
            cell?.ticketImgView.image = UIImage(named: "PassIcon")
            cell?.lblFreeTicketAvailable.text = "PASS AVAILABLE".localized()
            cell!.lblFreeTicketAvailable.textColor = UIColor(red: 66.0/255, green: 117.0/255, blue: 211.0/255, alpha: 1)
            cell!.freeTicketView.backgroundColor = UIColor(red: 240.0/255, green: 240.0/255, blue: 240.0/255, alpha: 1)
        }
        else if details.isTicketAvailable{
            cell!.freeTicketView.isHidden = false
            cell?.ticketImgView.image = UIImage(named: "TicketHomeIcon")
            cell?.lblFreeTicketAvailable.text = "FreeTicketAvailable".localized()
            cell!.lblFreeTicketAvailable.textColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
            cell!.freeTicketView.backgroundColor = UIColor(red: 235.0/255, green: 251.0/255, blue: 235.0/255, alpha: 1)
        }
        else if details.customText.count > 0{
            cell!.freeTicketView.isHidden = false
            cell?.ticketImgView.image = UIImage(named: "CustomTextIcon")
            cell?.lblFreeTicketAvailable.text = details.customText
            cell!.lblFreeTicketAvailable.textColor = UIColor(red: 134.0/255, green: 31.0/255, blue: 255.0/255, alpha: 1)
            cell!.freeTicketView.backgroundColor = UIColor(red: 228.0/255, green: 213.0/255, blue: 252.0/255, alpha: 1)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let matchDetails = homeMatchsArray[indexPath.row]
        if matchDetails.active == "2" {
            AppHelper.showAlertView(message: "This match maybe activated soon".localized(), isErrorMessage: true)
            return
        }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        var sportType = ""
        
        if selectedGameTypeTab == GameType.Cricket.rawValue{
            sportType = "Cricket"
            if matchDetails.isQuizMatch {
                let startDateIndia = Double(matchDetails.startDateIndia) ?? 0
                let startDateTimestemp = Double(matchDetails.startDateTimestemp ?? "0") ?? 0
                let serverTimeStemp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
                if startDateIndia > 0 {
                    if startDateTimestemp >= serverTimeStemp {
                        AppHelper.showAlertView(message: "This Quiz will be live soon".localized(), isErrorMessage: true)
                        return;
                    }
                }
                
                let leagueVC = storyboard.instantiateViewController(withIdentifier: "QuizLeaguesViewController") as! QuizLeaguesViewController
                leagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else{
                let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
                leagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
        }
        else if selectedGameTypeTab == GameType.Kabaddi.rawValue{
            sportType = "Kabaddi"
            if matchDetails.isQuizMatch {
                
                let startDateIndia = Double(matchDetails.startDateIndia) ?? 0
                let startDateTimestemp = Double(matchDetails.startDateTimestemp ?? "0") ?? 0
                let serverTimeStemp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
                if startDateIndia > 0 {
                    if startDateTimestemp >= serverTimeStemp {
                        AppHelper.showAlertView(message: "This Quiz will be live soon".localized(), isErrorMessage: true)
                        return;
                    }
                }

                let leagueVC = storyboard.instantiateViewController(withIdentifier: "QuizLeaguesViewController") as! QuizLeaguesViewController
                leagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else{
                let kabaddiLeagueVC = storyboard.instantiateViewController(withIdentifier: "KabaddiLeagueViewController") as! KabaddiLeagueViewController
                kabaddiLeagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(kabaddiLeagueVC, animated: true)
                }
            }
        }
        else if selectedGameTypeTab == GameType.Football.rawValue{
            sportType = "Football"
            if matchDetails.isQuizMatch {
                                
                let startDateIndia = Double(matchDetails.startDateIndia) ?? 0
                let startDateTimestemp = Double(matchDetails.startDateTimestemp ?? "0") ?? 0
                let serverTimeStemp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
                if startDateIndia > 0 {
                    if startDateTimestemp >= serverTimeStemp {
                        AppHelper.showAlertView(message: "This Quiz will be live soon".localized(), isErrorMessage: true)
                        return;
                    }
                }

                let leagueVC = storyboard.instantiateViewController(withIdentifier: "QuizLeaguesViewController") as! QuizLeaguesViewController
                leagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else{
                let footballLeagueVC = storyboard.instantiateViewController(withIdentifier: "FootballLeagueViewController") as! FootballLeagueViewController
                footballLeagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(footballLeagueVC, animated: true)
                }
            }
        }
        else if selectedGameTypeTab == GameType.Quiz.rawValue{
            sportType = "Quiz"
            if matchDetails.isQuizMatch {
                let startDateIndia = Double(matchDetails.startDateIndia) ?? 0
                let startDateTimestemp = Double(matchDetails.startDateTimestemp ?? "0") ?? 0
                let serverTimeStemp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
                if startDateIndia > 0 {
                    if startDateTimestemp >= serverTimeStemp {
                        AppHelper.showAlertView(message: "This Quiz will be live soon".localized(), isErrorMessage: true)
                        return;
                    }
                }

                let leagueVC = storyboard.instantiateViewController(withIdentifier: "QuizLeaguesViewController") as! QuizLeaguesViewController
                leagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
        }
        else if selectedGameTypeTab == GameType.Basketball.rawValue{
            sportType = "Basketball"
            if matchDetails.isQuizMatch {
                let startDateIndia = Double(matchDetails.startDateIndia) ?? 0
                let startDateTimestemp = Double(matchDetails.startDateTimestemp ?? "0") ?? 0
                let serverTimeStemp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
                if startDateIndia > 0 {
                    if startDateTimestemp >= serverTimeStemp {
                        AppHelper.showAlertView(message: "This Quiz will be live soon".localized(), isErrorMessage: true)
                        return;
                    }
                }

                let leagueVC = storyboard.instantiateViewController(withIdentifier: "QuizLeaguesViewController") as! QuizLeaguesViewController
                leagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else{
                let footballLeagueVC = storyboard.instantiateViewController(withIdentifier: "BasketballLeagueViewController") as! BasketballLeagueViewController
                footballLeagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(footballLeagueVC, animated: true)
                }
            }
        }
        else if selectedGameTypeTab == GameType.Baseball.rawValue{
            sportType = "Baseball"
            if matchDetails.isQuizMatch {
                let startDateIndia = Double(matchDetails.startDateIndia) ?? 0
                let startDateTimestemp = Double(matchDetails.startDateTimestemp ?? "0") ?? 0
                let serverTimeStemp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
                if startDateIndia > 0 {
                    if startDateTimestemp >= serverTimeStemp {
                        AppHelper.showAlertView(message: "This Quiz will be live soon".localized(), isErrorMessage: true)
                        return;
                    }
                }

                let leagueVC = storyboard.instantiateViewController(withIdentifier: "QuizLeaguesViewController") as! QuizLeaguesViewController
                leagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(leagueVC, animated: true)
                }
            }
            else{
                let footballLeagueVC = storyboard.instantiateViewController(withIdentifier: "BaseballLeagueViewController") as! BaseballLeagueViewController
                footballLeagueVC.matchDetails = matchDetails
                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                    navVC.pushViewController(footballLeagueVC, animated: true)
                }
            }
        }
        
        AppxorEventHandler.logAppEvent(withName: "MatchClicked", info: ["MatchID": matchDetails.matchKey, "TeamName": matchDetails.matchShortName ?? "", "SportType": sportType, "SeriesName": matchDetails.seasonKey ?? "" ])
    }
    
    // MARK:- Timer Handlers
    func startTimer()  {
        updateTimerVlaue()
        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }

    @objc func updateTimerVlaue()  {
        let visibleCellsArray = tblView.visibleCells
        for visibleCell in visibleCellsArray{
            if let cell = visibleCell as? MatchTableViewCell{
                let details = homeMatchsArray[cell.tag]
                let raminingTime = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: details.startDateTimestemp)
                if raminingTime.count == 0{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeUpcomingMatchFromLocal"), object: details)
                }
                cell.configData(matchDetails: details)
            }
            else if let cell = visibleCell as? TourneyTableViewCell{
                let details = homeMatchsArray[cell.tag]
                let raminingTime = AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: details.startDateTimestemp)
                if raminingTime.count == 0{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeUpcomingMatchFromLocal"), object: details)
                }
                cell.configData(matchDetails: details)
            }
            else if let cell = visibleCell as? QuizMatchTableViewCell{
                let details = homeMatchsArray[cell.tag]
                let startDateIndia = Double(details.startDateIndia) ?? 0
                let startDateTimestemp = Double(details.startDateTimestemp ?? "0") ?? 0
                let serverTimeStemp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
                
                if startDateIndia > 0 {
                    if startDateTimestemp < serverTimeStemp {
                        let raminingTime =  AppHelper.getTimeDifferenceBetweenTwoDates(serverTimestamp: UserDetails.sharedInstance.serverTimeStemp, dateTimestamp: String(startDateTimestemp + details.closingTs * 60.0))
                        if raminingTime.count == 0{
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeUpcomingMatchFromLocal"), object: details)
                        }
                    }
                }
                
                cell.configData(details: details)
            }

        }
    }

}
