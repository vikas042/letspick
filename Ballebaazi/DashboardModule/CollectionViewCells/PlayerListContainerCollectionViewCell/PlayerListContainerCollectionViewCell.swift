//
//  PlayerListContainerCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 06/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let grayOutColor = UIColor(red: 250.0/255, green: 250.0/255, blue: 250.0/255, alpha: 0.35)

let selectedColorColor = UIColor(red: 192/255, green: 238/255, blue: 204/255, alpha: 1.0)

class PlayerListContainerCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {

    lazy var playerListArray = Array<PlayerDetails>()
    lazy var selectedLeagueType = 0
    lazy var selectedGameType = 0
    lazy var isPlaying22Announced = ""
    
    var selectedMathDetails: MatchDetails?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBOutlet weak var tblView: UITableView!
    
    func configData(playerList: Array<PlayerDetails>, leagueType: Int, gameType: Int, mathDetails: MatchDetails, isPlaying22: String)  {
        selectedMathDetails = mathDetails;
        playerListArray = playerList
        selectedGameType = gameType
        selectedLeagueType = leagueType
        tblView.register(UINib(nibName: "PlayersTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayersTableViewCell")
        isPlaying22Announced = isPlaying22            
        self.tblView.reloadData()
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "PlayersTableViewCell") as? PlayersTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "PlayersTableViewCell") as? PlayersTableViewCell
        }
        
        cell?.playerOverlayView.backgroundColor = grayOutColor
        cell?.playerOverlayView.isHidden = true
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.addPlayerButton.tag = indexPath.row
        cell?.playerPointInfoButton.tag = indexPath.row
        cell?.addPlayerButton.addTarget(self, action: #selector(self.addPlayerButtonTapped(button:)), for: .touchUpInside)
        cell?.playerPointInfoButton.addTarget(self, action: #selector(playerInfoButtonTapped(button:)), for: .touchUpInside)
        cell?.contentView.backgroundColor = UIColor.white
        
        if indexPath.row < playerListArray.count {
            let details = playerListArray[indexPath.row]
            if isPlaying22Announced != "1"{
                cell?.configDetails(details: details, legueType: selectedLeagueType, gameType: selectedGameType, isLineupsAnnounced: false)
            }
            else{
                cell?.configDetails(details: details, legueType: selectedLeagueType, gameType: selectedGameType, isLineupsAnnounced: true)
            }
                    
            if selectedGameType == GameType.Cricket.rawValue{
                          
                if (selectedLeagueType == FantasyType.Classic.rawValue) || (selectedLeagueType == FantasyType.Reverse.rawValue) || (selectedLeagueType == FantasyType.Wizard.rawValue) {
                    cell?.contentView.backgroundColor = UIColor.white
                    grayOutCricketClassicCell(cell: cell!, details: details)
                }
                else {
                    var maxCredit = UserDetails.sharedInstance.maxCreditLimitForBatting

                    if selectedLeagueType == FantasyType.Bowling.rawValue{
                        if details.playerPlayingRole == PlayerType.WicketKeeper.rawValue{
                            cell?.playerOverlayView.isHidden = false
                        }
                        maxCredit = UserDetails.sharedInstance.maxCreditLimitForBowling
                    }
                    
                    let minPointsPlayerDetails = playerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
                        return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
                    })
                    
                    let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                        playerDetails.teamShortName == details.teamShortName
                    }

                    let totalPoints = UserDetails.sharedInstance.selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                        creditSum + Float(nextPlayerDetails.credits)!
                    })
                    
                    let remingPoints = maxCredit - totalPoints
                    
                    if totalPlayerFromTeamArray.count >= 3 {
                        cell?.playerOverlayView.isHidden = false
                    }
                    else if UserDetails.sharedInstance.selectedPlayerList.count >= 5 {
                        cell?.playerOverlayView.isHidden = false
                    }
                    else if minPointsPlayerDetails.count != 0{
                        if Float(minPointsPlayerDetails[0].credits)! > remingPoints {
                            cell?.playerOverlayView.isHidden = false
                        }
                    }
                }
            }
            else if selectedGameType == GameType.Kabaddi.rawValue{
                grayOutKabbadiCell(cell: cell!, details: details)
            }
            else if selectedGameType == GameType.Football.rawValue{
                cell?.contentView.backgroundColor = UIColor.white
                grayOutFootballCell(cell: cell!, details: details)
            }
            else if selectedGameType == GameType.Basketball.rawValue{
                cell?.contentView.backgroundColor = UIColor.white
                var indexCount = 0
                
                if details.seasonalRole == PlayerType.PointGuard.rawValue{
                    indexCount = 0
                }
                else if details.seasonalRole == PlayerType.ShootingGuard.rawValue{
                    indexCount = 1
                }
                else if details.seasonalRole == PlayerType.SmallForward.rawValue{
                    indexCount = 2
                }
                else if details.seasonalRole == PlayerType.PowerForward.rawValue{
                    indexCount = 3
                }
                else if details.seasonalRole == PlayerType.Center.rawValue{
                    indexCount = 4
                }

                grayOutForBasketballCell(cell: cell!, details: details, index: indexCount)
            }
            else if selectedGameType == GameType.Baseball.rawValue{
                cell?.contentView.backgroundColor = UIColor.white
                var indexCount = 0
                
                if details.seasonalRole == PlayerType.Outfielders.rawValue{
                    indexCount = 0
                }
                else if details.seasonalRole == PlayerType.Infielder.rawValue{
                    indexCount = 1
                }
                else if details.seasonalRole == PlayerType.Pitcher.rawValue{
                    indexCount = 2
                }
                else if details.seasonalRole == PlayerType.Catcher.rawValue{
                    indexCount = 3
                }

                grayOutForBaseballCell(cell: cell!, details: details, index: indexCount)
            }

            
            if details.isSelected{
                cell?.contentView.backgroundColor = selectedColorColor
                cell?.playerOverlayView.isHidden = true
            }
        }
                        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playerDetails = playerListArray[indexPath.row]
        updatePlayerSelectionArray(playerDetails: playerDetails)
    }
    
    func grayOutKabbadiCell(cell: PlayersTableViewCell, details: PlayerDetails) {
        
        let minPointsPlayerDetails =  UserDetails.sharedInstance.selectedPlayerList.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
        })
        
        let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == details.teamShortName
        }

        let totalPoints = UserDetails.sharedInstance.selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            creditSum + Float(nextPlayerDetails.credits)!
        })
        
        let maxCredit = UserDetails.sharedInstance.maxCreditLimitForClassic
        let remingPoints = maxCredit - totalPoints
        
        if totalPlayerFromTeamArray.count >= 5 {
            cell.playerOverlayView.isHidden = false
        }
        else if UserDetails.sharedInstance.selectedPlayerList.count >= 7 {
            cell.playerOverlayView.isHidden = false
        }
        else if minPointsPlayerDetails.count == 0{
            // Do noting in this case
        }
        else if Float(minPointsPlayerDetails[0].credits)! > remingPoints {
            cell.playerOverlayView.isHidden = false
        }
        else{
            let totalDefenderArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
            }

            let totalAllrounderArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
            }
            
            let totalRaiderArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Raider.rawValue
            }
            if details.playerPlayingRole == PlayerType.Defender.rawValue {
                if totalDefenderArray.count >= 4{
                    cell.playerOverlayView.isHidden = false
                }
            }else if details.playerPlayingRole == PlayerType.AllRounder.rawValue {
                if totalAllrounderArray.count >= 2{
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if details.playerPlayingRole == PlayerType.Raider.rawValue {
                if totalRaiderArray.count >= 3{
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 7) && (totalAllrounderArray.count == 0){
//                cell.contentView.backgroundColor = grayOutColor
                cell.playerOverlayView.isHidden = false

            }
        }
    }
    
    
    func grayOutCricketClassicCell(cell: PlayersTableViewCell, details: PlayerDetails) {
        
        let minPointsPlayerDetails =  UserDetails.sharedInstance.selectedPlayerList.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
        })
        
        let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == details.teamShortName
        }

        let totalPoints = UserDetails.sharedInstance.selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            creditSum + Float(nextPlayerDetails.credits)!
        })
        
        let maxCredit = UserDetails.sharedInstance.maxCreditLimitForClassic
        let remingPoints = maxCredit - totalPoints
        
        if totalPlayerFromTeamArray.count >= 7 {
            cell.playerOverlayView.isHidden = false
        }
        else if UserDetails.sharedInstance.selectedPlayerList.count >= 11 {
            cell.playerOverlayView.isHidden = false

        }
        else if minPointsPlayerDetails.count == 0{
            // Do noting in this case
        }
        else if Float(minPointsPlayerDetails[0].credits)! > remingPoints {
            cell.playerOverlayView.isHidden = false
        }
        else{
                        
            let totalWicketKeeperArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
            }

            
            let totalBatsmenArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
            }
            
            let totalAllRounderArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
            }
            
            let totalBowlerArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
            }
            
            if (UserDetails.sharedInstance.selectedPlayerList.count == 11) && (totalWicketKeeperArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 11) && (totalAllRounderArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 7) && (totalBatsmenArray.count == 0) && (totalWicketKeeperArray.count == 0){
                if (details.playerPlayingRole != PlayerType.WicketKeeper.rawValue) && (details.playerPlayingRole != PlayerType.Batsman.rawValue){
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 7) && (totalBatsmenArray.count == 0) && (totalAllRounderArray.count == 0){
                if (details.playerPlayingRole != PlayerType.AllRounder.rawValue) && (details.playerPlayingRole != PlayerType.Batsman.rawValue){
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 7) && (totalBowlerArray.count == 0) && (totalWicketKeeperArray.count == 0){
                if (details.playerPlayingRole != PlayerType.WicketKeeper.rawValue) && (details.playerPlayingRole != PlayerType.Bowler.rawValue){
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 7) && (totalBowlerArray.count == 0) && (totalAllRounderArray.count == 0){
                if (details.playerPlayingRole != PlayerType.AllRounder.rawValue) && (details.playerPlayingRole != PlayerType.Bowler.rawValue){
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 8) && ((11 -  UserDetails.sharedInstance.selectedPlayerList.count) < (3 - totalBatsmenArray.count)){
                if details.playerPlayingRole != PlayerType.Batsman.rawValue {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (totalBatsmenArray.count == 6) && (totalBowlerArray.count > 3){
                cell.playerOverlayView.isHidden = false

            }
            else if (totalBowlerArray.count == 6) && (totalBatsmenArray.count > 3){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 10) && (totalBatsmenArray.count < 3){
                if details.playerPlayingRole != PlayerType.Batsman.rawValue {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 10) && (totalBowlerArray.count < 3){
                if details.playerPlayingRole != PlayerType.Bowler.rawValue {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 10) && (totalAllRounderArray.count < 1){
                if details.playerPlayingRole != PlayerType.AllRounder.rawValue {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 10) && (totalWicketKeeperArray.count < 1){
                if details.playerPlayingRole != PlayerType.WicketKeeper.rawValue {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (totalBatsmenArray.count == 6) && (totalWicketKeeperArray.count > 1){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalBowlerArray.count == 6) && (totalWicketKeeperArray.count > 1){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalBatsmenArray.count == 6) && (totalAllRounderArray.count > 1){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalBowlerArray.count == 6) && (totalAllRounderArray.count > 1){
                cell.playerOverlayView.isHidden = false

            }
            else if (totalBatsmenArray.count == 6) && (totalBowlerArray.count == 3) && (totalAllRounderArray.count == 0) && (totalAllRounderArray.count == 0){
                
                if (details.playerPlayingRole != PlayerType.WicketKeeper.rawValue) && (details.playerPlayingRole != PlayerType.AllRounder.rawValue) {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (totalBowlerArray.count == 6) && (totalBatsmenArray.count == 3) && (totalAllRounderArray.count == 0) && (totalAllRounderArray.count == 0){
                    
                if (details.playerPlayingRole != PlayerType.WicketKeeper.rawValue) && (details.playerPlayingRole != PlayerType.AllRounder.rawValue) {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (totalBatsmenArray.count == 6) && (totalBowlerArray.count >= 3) && (totalAllRounderArray.count >= 1){
                
                if details.playerPlayingRole != PlayerType.WicketKeeper.rawValue {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (totalBatsmenArray.count == 6) && (totalBowlerArray.count >= 3) && (totalWicketKeeperArray.count >= 1){
                if details.playerPlayingRole != PlayerType.AllRounder.rawValue {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (totalBowlerArray.count == 6) && (totalBatsmenArray.count >= 3) && (totalAllRounderArray.count >= 1){
                if details.playerPlayingRole != PlayerType.WicketKeeper.rawValue {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (totalBowlerArray.count == 6) && (totalBatsmenArray.count >= 3) && (totalWicketKeeperArray.count >= 1){
                if details.playerPlayingRole != PlayerType.AllRounder.rawValue {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if details.playerPlayingRole == PlayerType.WicketKeeper.rawValue {
                if totalWicketKeeperArray.count >= 4{
                    cell.playerOverlayView.isHidden = false
                }
            }else if details.playerPlayingRole == PlayerType.Batsman.rawValue {
                if totalBatsmenArray.count >= 6{
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if details.playerPlayingRole == PlayerType.AllRounder.rawValue {
                if totalAllRounderArray.count >= 4{
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if details.playerPlayingRole == PlayerType.Bowler.rawValue {
                if totalBowlerArray.count >= 6{
                    cell.playerOverlayView.isHidden = false
                }
            }
        }
    }
    
    func grayOutFootballCell(cell: PlayersTableViewCell, details: PlayerDetails) {
        
        let minPointsPlayerDetails =  UserDetails.sharedInstance.selectedPlayerList.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
        })
        
        let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == details.teamShortName
        }

        let totalPoints = UserDetails.sharedInstance.selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            creditSum + Float(nextPlayerDetails.credits)!
        })
        
        let maxCredit = UserDetails.sharedInstance.maxCreditLimitForClassic
        let remingPoints = maxCredit - totalPoints
        
        if totalPlayerFromTeamArray.count >= 7 {
            cell.playerOverlayView.isHidden = false
        }
        else if UserDetails.sharedInstance.selectedPlayerList.count >= 11 {
            cell.playerOverlayView.isHidden = false
        }
        else if minPointsPlayerDetails.count == 0{
            // Do noting in this case
        }
        else if Float(minPointsPlayerDetails[0].credits)! > remingPoints {
            cell.playerOverlayView.isHidden = false
        }
        else{
            
            let totalDefenderArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
            }
            
            let totalMidfielderArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.MidFielder.rawValue
            }
            
            let totalForwardArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Sticker.rawValue
            }
            
            let totalGoalKeeperArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.GoalKeeper.rawValue
            }
            
            if (UserDetails.sharedInstance.selectedPlayerList.count == 11) && (totalGoalKeeperArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 11) && (totalForwardArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 8) && ((11 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (3 - totalDefenderArray.count)){
                if details.playerPlayingRole != PlayerType.Defender.rawValue  {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 8) && ((11 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (3 - totalMidfielderArray.count)){
                if details.playerPlayingRole != PlayerType.MidFielder.rawValue  {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (totalDefenderArray.count == 5) && (totalMidfielderArray.count > 4){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalMidfielderArray.count == 5) && (totalDefenderArray.count > 4){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalMidfielderArray.count == 5) && (totalForwardArray.count > 2){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalDefenderArray.count == 5) && (totalForwardArray.count > 2){
                cell.playerOverlayView.isHidden = false
            }
            else if details.playerPlayingRole == PlayerType.Defender.rawValue {
                if totalDefenderArray.count >= 5{
                    cell.playerOverlayView.isHidden = false
                }
            }else if details.playerPlayingRole == PlayerType.MidFielder.rawValue {
                if totalMidfielderArray.count >= 5{
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if details.playerPlayingRole == PlayerType.Sticker.rawValue {
                if totalForwardArray.count >= 3{
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if details.playerPlayingRole == PlayerType.GoalKeeper.rawValue {
                if totalGoalKeeperArray.count >= 1{
                    cell.playerOverlayView.isHidden = false
                }
            }
        }
    }
    
    
    func grayOutForBaseballCell(cell: PlayersTableViewCell, details: PlayerDetails, index: Int) {
        
        let minPointsPlayerDetails =  UserDetails.sharedInstance.selectedPlayerList.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
        })
        
        let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == details.teamShortName
        }

        let totalPoints = UserDetails.sharedInstance.selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            creditSum + Float(nextPlayerDetails.credits)!
        })
        
        let maxCredit = UserDetails.sharedInstance.maxCreditLimitForClassic
        let remingPoints = maxCredit - totalPoints
        
        if totalPlayerFromTeamArray.count >= 6 {
            cell.playerOverlayView.isHidden = false
        }
        else if UserDetails.sharedInstance.selectedPlayerList.count >= 9 {
            cell.playerOverlayView.isHidden = false

        }
        else if minPointsPlayerDetails.count == 0{
            // Do noting in this case
        }
        else if Float(minPointsPlayerDetails[0].credits)! > remingPoints {
            cell.playerOverlayView.isHidden = false
        }
        else{
            
            let totalOutfieldersArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Outfielders.rawValue
            }
            
            let totalInfielderArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Infielder.rawValue
            }
            
            let totalPitcherArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Pitcher.rawValue
            }
            
            let totalCatcherArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Catcher.rawValue
            }
            
            if (UserDetails.sharedInstance.selectedPlayerList.count == 9) && (totalOutfieldersArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 9) && (totalInfielderArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 6) && ((9 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalOutfieldersArray.count)){
                if details.playerPlayingRole != PlayerType.Outfielders.rawValue  {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 6) && ((9 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (2 - totalInfielderArray.count)){
                if details.playerPlayingRole != PlayerType.Infielder.rawValue  {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (totalOutfieldersArray.count == 5) && (totalInfielderArray.count >= 2) && ((index == 0) || (index == 1)){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalInfielderArray.count == 5) && (totalOutfieldersArray.count >= 2) && ((index == 0) || (index == 1)){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPitcherArray.count == 1) && (index == 2){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalCatcherArray.count == 1) && (index == 3){
                cell.playerOverlayView.isHidden = false
            }
            else if details.playerPlayingRole == PlayerType.Outfielders.rawValue {
                if totalOutfieldersArray.count >= 5{
                    cell.playerOverlayView.isHidden = false
                }
//                else if (totalOutfieldersArray.count == 2) && (totalOutfieldersArray.count == 2){
//                }
            }else if details.playerPlayingRole == PlayerType.Infielder.rawValue {
                if totalInfielderArray.count >= 5{
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if details.playerPlayingRole == PlayerType.Pitcher.rawValue {
                if totalPitcherArray.count >= 1{
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if details.playerPlayingRole == PlayerType.Catcher.rawValue {
                if totalCatcherArray.count >= 1{
                    cell.playerOverlayView.isHidden = false
                }
            }
        }
    }
    
    func grayOutForBasketballCell(cell: PlayersTableViewCell, details: PlayerDetails, index: Int) {
        
        let minPointsPlayerDetails =  UserDetails.sharedInstance.selectedPlayerList.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return Float(nextPlayerDetails.credits)! < Float(playerDetails.credits)!
        })
        
        let totalPlayerFromTeamArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == details.teamShortName
        }

        let totalPoints = UserDetails.sharedInstance.selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            creditSum + Float(nextPlayerDetails.credits)!
        })
        
        let maxCredit = UserDetails.sharedInstance.maxCreditLimitForClassic
        let remingPoints = maxCredit - totalPoints
        
        if totalPlayerFromTeamArray.count >= 5 {
            let lastObj = totalPlayerFromTeamArray.last
            if details.teamShortName == lastObj!.teamShortName {
                cell.playerOverlayView.isHidden = false
            }
            else{
                cell.playerOverlayView.isHidden = true
            }
        }
        else if UserDetails.sharedInstance.selectedPlayerList.count >= 8 {
            cell.playerOverlayView.isHidden = false
        }
        else if minPointsPlayerDetails.count == 0{
            // Do noting in this case
        }
        else if Float(minPointsPlayerDetails[0].credits)! > remingPoints {
            cell.playerOverlayView.isHidden = false
        }
        else{

            let totalPointGuardsArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.PointGuard.rawValue
            }
            
            let totalShootingGuardArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.ShootingGuard.rawValue
            }
            
            let totalSmallForwardArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.SmallForward.rawValue
            }
            
            let totalPowerForwardArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.PowerForward.rawValue
            }
            
            let totalCenterArray = UserDetails.sharedInstance.selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerPlayingRole == PlayerType.Center.rawValue
            }
                        
            if (totalPointGuardsArray.count >= 4) && (index == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalShootingGuardArray.count >= 4) && (index == 1){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalSmallForwardArray.count >= 4) && (index == 2){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPowerForwardArray.count >= 4) && (index == 3){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalCenterArray.count >= 4) && (index == 4){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 5) && (totalPointGuardsArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 8) && (totalShootingGuardArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 8) && (totalSmallForwardArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 8) && (totalPowerForwardArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count == 8) && (totalCenterArray.count == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPointGuardsArray.count >= 4) && (totalShootingGuardArray.count >= 1) && (index == 1){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPointGuardsArray.count >= 4) && (totalSmallForwardArray.count >= 1) && (index == 2){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPointGuardsArray.count >= 4) && (totalPowerForwardArray.count >= 1) && (index == 3){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPointGuardsArray.count >= 4) && (totalCenterArray.count >= 1) && (index == 4){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalShootingGuardArray.count >= 4) && (totalPointGuardsArray.count >= 1) && (index == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalShootingGuardArray.count >= 4) && (totalSmallForwardArray.count >= 1) && (index == 2){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalShootingGuardArray.count >= 4) && (totalPowerForwardArray.count >= 1) && (index == 3){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalShootingGuardArray.count >= 4) && (totalCenterArray.count >= 1) && (index == 4){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalSmallForwardArray.count >= 4) && (totalPointGuardsArray.count >= 1) && (index == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalSmallForwardArray.count >= 4) && (totalShootingGuardArray.count >= 1) && (index == 1){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalSmallForwardArray.count >= 4) && (totalPowerForwardArray.count >= 1) && (index == 3){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalSmallForwardArray.count >= 4) && (totalCenterArray.count >= 1) && (index == 4){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPowerForwardArray.count >= 4) && (totalPointGuardsArray.count >= 1) && (index == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPowerForwardArray.count >= 4) && (totalShootingGuardArray.count >= 1) && (index == 1){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPowerForwardArray.count >= 4) && (totalSmallForwardArray.count >= 1) && (index == 2){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalPowerForwardArray.count >= 4) && (totalCenterArray.count >= 1) && (index == 4){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalCenterArray.count >= 4) && (totalPointGuardsArray.count >= 1) && (index == 0){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalCenterArray.count >= 4) && (totalShootingGuardArray.count >= 1) && (index == 1){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalCenterArray.count >= 4) && (totalSmallForwardArray.count >= 1) && (index == 2){
                cell.playerOverlayView.isHidden = false
            }
            else if (totalCenterArray.count >= 4) && (totalPowerForwardArray.count >= 1) && (index == 3){
                cell.playerOverlayView.isHidden = false
            }

            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalPointGuardsArray.count)){
                if details.playerPlayingRole != PlayerType.PointGuard.rawValue  {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalShootingGuardArray.count)){
                if details.playerPlayingRole != PlayerType.ShootingGuard.rawValue  {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalSmallForwardArray.count)){
                if details.playerPlayingRole != PlayerType.SmallForward.rawValue  {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalPowerForwardArray.count)){
                if details.playerPlayingRole != PlayerType.PowerForward.rawValue  {
                    cell.playerOverlayView.isHidden = false
                }
            }
            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalCenterArray.count)){
                if details.playerPlayingRole != PlayerType.Center.rawValue  {
                    cell.playerOverlayView.isHidden = false
                }
            }

                
                
                
                
                
                
//            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalPointGuardsArray.count)) && (index == 0){
//                cell.playerOverlayView.isHidden = false
//            }
//            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalShootingGuardArray.count)) && (index == 1){
//                cell.playerOverlayView.isHidden = false
//            }
//            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalSmallForwardArray.count)) && (index == 2){
//                cell.playerOverlayView.isHidden = false
//            }
//            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalPowerForwardArray.count)) && (index == 3){
//                cell.playerOverlayView.isHidden = false
//            }
//            else if (UserDetails.sharedInstance.selectedPlayerList.count >= 5) && ((8 -  UserDetails.sharedInstance.selectedPlayerList.count) <= (1 - totalCenterArray.count)) && (index == 4){
//                cell.playerOverlayView.isHidden = false
//            }
//            else if details.playerPlayingRole == PlayerType.PointGuard.rawValue {
//                if totalPointGuardsArray.count >= 1{
//                    cell.playerOverlayView.isHidden = false
//                }
//            }else if details.playerPlayingRole == PlayerType.ShootingGuard.rawValue {
//                if totalShootingGuardArray.count >= 1{
//                    cell.playerOverlayView.isHidden = false
//                }
//            }
//            else if details.playerPlayingRole == PlayerType.SmallForward.rawValue {
//                if totalSmallForwardArray.count >= 3{
//                    cell.playerOverlayView.isHidden = false
//                }
//            }
//            else if details.playerPlayingRole == PlayerType.PowerForward.rawValue {
//                if totalPowerForwardArray.count >= 1{
//                    cell.playerOverlayView.isHidden = false
//                }
//            }
//            else if details.playerPlayingRole == PlayerType.Center.rawValue {
//                if totalCenterArray.count >= 1{
//                    cell.playerOverlayView.isHidden = false
//                }
//            }


//            else if (totalOutfieldersArray.count == 5) && (totalInfielderArray.count >= 2) && ((index == 0) || (index == 1)){
//                cell.playerOverlayView.isHidden = false
//            }
//            else if (totalInfielderArray.count == 5) && (totalOutfieldersArray.count >= 2) && ((index == 0) || (index == 1)){
//                cell.playerOverlayView.isHidden = false
//            }
//            else if (totalPitcherArray.count == 1) && (index == 2){
//                cell.playerOverlayView.isHidden = false
//            }
//            else if (totalCatcherArray.count == 1) && (index == 3){
//                cell.playerOverlayView.isHidden = false
//            }
//            else if details.playerPlayingRole == PlayerType.Outfielders.rawValue {
//                if totalOutfieldersArray.count >= 5{
//                    cell.playerOverlayView.isHidden = false
//                }
//            }else if details.playerPlayingRole == PlayerType.Infielder.rawValue {
//                if totalInfielderArray.count >= 5{
//                    cell.playerOverlayView.isHidden = false
//                }
//            }
//            else if details.playerPlayingRole == PlayerType.Pitcher.rawValue {
//                if totalPitcherArray.count >= 1{
//                    cell.playerOverlayView.isHidden = false
//                }
//            }
//            else if details.playerPlayingRole == PlayerType.Catcher.rawValue {
//                if totalCatcherArray.count >= 1{
//                    cell.playerOverlayView.isHidden = false
//                }
//            }
        }
    }
    
    func getGrayOutPlayerStatus(totalWicketKeeperCount: Int, totalBatsmanCount: Int, totalAllrounderCount: Int, totalBowlerCount: Int, playerType: String) -> Bool {
        
        var temp = 0
        let remainingWicketKeeper = 1 - totalWicketKeeperCount
        let remainingBatsmantKeeper = 3 - totalBatsmanCount
        let remainingAllRounderKeeper = 1 - totalAllrounderCount
        let remainingBowlerKeeper = 3 - totalBowlerCount

        if remainingWicketKeeper > 0 {
            temp = temp + remainingWicketKeeper
        }
        
        if remainingBatsmantKeeper > 0 {
            temp = temp + remainingBatsmantKeeper
        }

        if remainingAllRounderKeeper > 0 {
            temp = temp + remainingAllRounderKeeper
        }

        if remainingBowlerKeeper > 0 {
            temp = temp + remainingBowlerKeeper
        }
        
        if UserDetails.sharedInstance.selectedPlayerList.count >= 7 {
            if temp >= 11 - UserDetails.sharedInstance.selectedPlayerList.count{
                if remainingAllRounderKeeper > 0 {
                    if playerType == PlayerType.AllRounder.rawValue {
                        return false;
                    }
                    return true;
                }
                else if remainingBowlerKeeper > 0 {
                    if playerType == PlayerType.Bowler.rawValue {
                        return false;
                    }
                    return true;
                }
                else if remainingWicketKeeper > 0 {
                    if playerType == PlayerType.WicketKeeper.rawValue {
                        return false;
                    }
                    return true;
                }
                else if remainingBatsmantKeeper > 0 {
                    if playerType == PlayerType.Batsman.rawValue {
                        return false;
                    }
                    return true;
                }
            }
        }
        
        return false
    }
    func filterPlayerList(selectedPlayerList: Array<PlayerDetails>, teamName: String)-> (Int, Int, Int, Int, Int, Float) {
        
        let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamShortName == teamName
        }

        let totalBatsmanArray = selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
        }
        
        let totalBowlerArray = selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
        }
        
        let totalAllrounderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
        }
        
        let totalWicketKeeperArray = selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
        }
        
        let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            creditSum + Float(nextPlayerDetails.credits)!
        })
        
        let maxCredit = UserDetails.sharedInstance.maxCreditLimitForClassic

        return (totalWicketKeeperArray.count, totalBatsmanArray.count, totalAllrounderArray.count, totalBowlerArray.count, totalPlayerFromTeamArray.count, (maxCredit - totalPoints))
    }
        
    @objc func addPlayerButtonTapped(button: UIButton){
        let playerDetails = playerListArray[button.tag]
        updatePlayerSelectionArray(playerDetails: playerDetails)
    }
    
    func updatePlayerSelectionArray(playerDetails: PlayerDetails) {
        var isRecordRemoves = false;
        
        if playerDetails.isSelected {
            isRecordRemoves = true;
        }
        
        playerDetails.isSelected = !playerDetails.isSelected
        
        for i in (0..<UserDetails.sharedInstance.selectedPlayerList.count)
        {
            let details = UserDetails.sharedInstance.selectedPlayerList[i]
            if details.playerKey == playerDetails.playerKey {
                UserDetails.sharedInstance.selectedPlayerList.remove(at: i)
                break
            }
        }
        
        if playerDetails.isSelected {
            UserDetails.sharedInstance.selectedPlayerList.append(playerDetails)
        }
        
        if (selectedLeagueType == FantasyType.Classic.rawValue) || (selectedLeagueType == FantasyType.Reverse.rawValue) || (selectedLeagueType == FantasyType.Wizard.rawValue) {
            
            if selectedGameType == GameType.Cricket.rawValue {
                if selectedMathDetails?.isMatchTourney ?? false {
                    if let validationMsg = AppHelper.validateClassicLeagueForTourney(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName, maxCredit: UserDetails.sharedInstance.maxCreditLimitForClassic, isRecordRemove: isRecordRemoves ){
                        AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                        if playerDetails.isSelected{
                            UserDetails.sharedInstance.selectedPlayerList.removeLast()
                            playerDetails.isSelected = false
                        }
                    }
                }
                else{
                    if let validationMsg = AppHelper.validateClassicLeague(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName, maxCredit: UserDetails.sharedInstance.maxCreditLimitForClassic, isRecordRemove: isRecordRemoves ){
                        AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                        if playerDetails.isSelected{
                            UserDetails.sharedInstance.selectedPlayerList.removeLast()
                            playerDetails.isSelected = false
                        }
                    }
                }
            }
            else if selectedGameType == GameType.Kabaddi.rawValue {
                if let validationMsg = AppHelper.validateKabaddiLeague(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName, maxCredit: UserDetails.sharedInstance.maxCreditLimitForClassic, isRecordRemove: isRecordRemoves ){
                    AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                    if playerDetails.isSelected{
                        UserDetails.sharedInstance.selectedPlayerList.removeLast()
                        playerDetails.isSelected = false
                    }
                }
            }
            else if selectedGameType == GameType.Football.rawValue{
                if let validationMsg = AppHelper.validateFootballClassicLeague(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName, maxCredit: UserDetails.sharedInstance.maxCreditLimitForClassic, isRecordRemove: isRecordRemoves ){
                    AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                    if playerDetails.isSelected{
                        UserDetails.sharedInstance.selectedPlayerList.removeLast()
                        playerDetails.isSelected = false
                    }
                }
            }
            else if selectedGameType == GameType.Basketball.rawValue{
                if let validationMsg = AppHelper.validateBasketballLeague(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName, maxCredit: UserDetails.sharedInstance.maxCreditLimitForClassic, isRecordRemove: isRecordRemoves ){
                    AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                    if playerDetails.isSelected{
                        UserDetails.sharedInstance.selectedPlayerList.removeLast()
                        playerDetails.isSelected = false
                    }
                }
            }
            else if selectedGameType == GameType.Baseball.rawValue{
                if let validationMsg = AppHelper.validateBaseballLeague(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName, maxCredit: UserDetails.sharedInstance.maxCreditLimitForClassic, isRecordRemove: isRecordRemoves ){
                    AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                    if playerDetails.isSelected{
                        UserDetails.sharedInstance.selectedPlayerList.removeLast()
                        playerDetails.isSelected = false
                    }
                }
            }
        }
        else if selectedLeagueType == FantasyType.Batting.rawValue {
            if selectedMathDetails?.isMatchTourney ?? false {
                if let validationMsg = AppHelper.validateBattingLeagueForTourney(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName, maxCredit: UserDetails.sharedInstance.maxCreditLimitForBatting,isRecordRemove: isRecordRemoves){
                    weak var weakSelf = self

                    if (validationMsg == "bowlerSelectionPopUp".localized()) && playerDetails.isSelected{
                        let alert = UIAlertController(title: "Are you sure?".localized(), message: validationMsg, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { action -> Void in
                            weakSelf?.tblView.reloadData()
                            NotificationCenter.default.post(name: Notification.Name("PlayerSelectionUpdateNotification"), object: nil)
                        }))
                        
                        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: { action -> Void in
                            UserDetails.sharedInstance.selectedPlayerList.removeLast()
                            playerDetails.isSelected = false
                            weakSelf?.tblView.reloadData()
                            NotificationCenter.default.post(name: Notification.Name("PlayerSelectionUpdateNotification"), object: nil)
                        }))
                        
                        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        return
                    }
                    else if validationMsg != "bowlerSelectionPopUp".localized(){
                        UserDetails.sharedInstance.selectedPlayerList.removeLast()
                        playerDetails.isSelected = false
                        AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                    }
                }
            }
            else{
                if let validationMsg = AppHelper.validateBattingLeague(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName, maxCredit: UserDetails.sharedInstance.maxCreditLimitForBatting,isRecordRemove: isRecordRemoves){
                    weak var weakSelf = self

                    if (validationMsg == "bowlerSelectionPopUp".localized()) && playerDetails.isSelected{
                        let alert = UIAlertController(title: "Are you sure?".localized(), message: validationMsg, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { action -> Void in
                            weakSelf?.tblView.reloadData()
                            NotificationCenter.default.post(name: Notification.Name("PlayerSelectionUpdateNotification"), object: nil)
                        }))
                        
                        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: { action -> Void in
                            UserDetails.sharedInstance.selectedPlayerList.removeLast()
                            playerDetails.isSelected = false
                            weakSelf?.tblView.reloadData()
                            NotificationCenter.default.post(name: Notification.Name("PlayerSelectionUpdateNotification"), object: nil)
                        }))
                        
                        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        return
                    }
                    else if validationMsg != "bowlerSelectionPopUp".localized(){
                        UserDetails.sharedInstance.selectedPlayerList.removeLast()
                        playerDetails.isSelected = false
                        AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                    }
                }
            }
        }
        else if selectedLeagueType == FantasyType.Bowling.rawValue {
            weak var weakSelf = self
            if selectedMathDetails?.isMatchTourney ?? false {
                if let validationMsg = AppHelper.validateBlowlingLeagueForTourney(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName,maxCredit: UserDetails.sharedInstance.maxCreditLimitForBowling,isRecordRemove: isRecordRemoves){
                    if (validationMsg == "battingSelectionPopUp".localized()) && playerDetails.isSelected{
                        let alert = UIAlertController(title: "Are you sure?".localized(), message: validationMsg, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { action -> Void in
                            weakSelf?.tblView.reloadData()
                            NotificationCenter.default.post(name: Notification.Name("PlayerSelectionUpdateNotification"), object: nil)
                        }))
                        
                        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: { action -> Void in
                            UserDetails.sharedInstance.selectedPlayerList.removeLast()
                            playerDetails.isSelected = false
                            weakSelf?.tblView.reloadData()
                            NotificationCenter.default.post(name: Notification.Name("PlayerSelectionUpdateNotification"), object: nil)
                        }))
                        
                        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        return
                    }
                    
                    UserDetails.sharedInstance.selectedPlayerList.removeLast()
                    playerDetails.isSelected = false
                    AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                }
            }
            else{
                if let validationMsg = AppHelper.validateBlowlingLeague(selectedPlayerList: UserDetails.sharedInstance.selectedPlayerList,teamName: playerDetails.teamShortName,maxCredit: UserDetails.sharedInstance.maxCreditLimitForBowling,isRecordRemove: isRecordRemoves){
                    if (validationMsg == "battingSelectionPopUp".localized()) && playerDetails.isSelected{
                        let alert = UIAlertController(title: "Are you sure?".localized(), message: validationMsg, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { action -> Void in
                            weakSelf?.tblView.reloadData()
                            NotificationCenter.default.post(name: Notification.Name("PlayerSelectionUpdateNotification"), object: nil)
                        }))
                        
                        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: { action -> Void in
                            UserDetails.sharedInstance.selectedPlayerList.removeLast()
                            playerDetails.isSelected = false
                            weakSelf?.tblView.reloadData()
                            NotificationCenter.default.post(name: Notification.Name("PlayerSelectionUpdateNotification"), object: nil)
                        }))
                        
                        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        return
                    }
                    
                    UserDetails.sharedInstance.selectedPlayerList.removeLast()
                    playerDetails.isSelected = false
                    AppHelper.showAlertViewWithoutMessage(titleMessage: validationMsg, isErrorMessage: true)
                }
            }
        }
        
        NotificationCenter.default.post(name: Notification.Name("PlayerSelectionUpdateNotification"), object: nil)
        tblView.reloadData()
    }
    
    @objc func playerInfoButtonTapped(button: UIButton){
                
        let playerDetails = playerListArray[button.tag]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let playerStatsVC = storyboard.instantiateViewController(withIdentifier: "PlayerStatsViewController") as! PlayerStatsViewController
        playerStatsVC.totalPlayerInfoArray = playerListArray
        playerStatsVC.selectedMathDetails = selectedMathDetails
        playerStatsVC.selectedLeagueType = selectedLeagueType
        
        if isPlaying22Announced != "1" {
            playerStatsVC.isLineupsAnnounced = false
        }
        else{
            playerStatsVC.isLineupsAnnounced = true
        }
        
        playerStatsVC.selectedPlayerDetails = playerDetails
        playerStatsVC.gameType = selectedGameType;
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(playerStatsVC, animated: true)
        }

    }

    
    func callGetUserPointsApi(playerDetails: PlayerDetails) {
        if selectedMathDetails == nil {
            return;
        }
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let parameters: Parameters = ["option": "get_player_info","match_key": selectedMathDetails!.matchKey, "user_id": UserDetails.sharedInstance.userID, "player_key": playerDetails.playerKey!]
        
        var urlString = kMatch
        
        if selectedGameType == GameType.Cricket.rawValue {
            urlString = kMatch
        }
        else if selectedGameType == GameType.Kabaddi.rawValue {
            urlString = kKabaddiMatchURL
        }
        else if selectedGameType == GameType.Football.rawValue {
            urlString = kFootballMatchURL
        }
        
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            weak var weakSelf = self

            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string ?? "kErrorMsg".localized()
                if statusCode == "200"{
                    if let response = result!["response"]?.dictionary{
                        if (response["player_info"]?.dictionary) != nil{
                            
                            guard let weakObj = weakSelf else{
                                return;
                            }

                            let playerinfo = PlayerInfoDetails.parserPlayerInformation(playerDetails: response["player_info"]!)
                            if let matchInfoArray = response["match_info"]?.array{
                                let playerinfoArray = PlayerMatchInfo.getPlayerInfoList(dataArray: matchInfoArray)
                                
                                let playerInfoView = PlayerPointInfoView(frame: (APPDELEGATE.window?.frame)!)
                                playerInfoView.playerDetails = playerinfo;
                                playerInfoView.playerInfoArray = playerinfoArray;
                                if weakObj.selectedLeagueType == FantasyType.Classic.rawValue{
                                    playerInfoView.fantasyType = "1"
                                }
                                else if weakObj.selectedLeagueType == FantasyType.Batting.rawValue{
                                    playerInfoView.fantasyType = "2"
                                }
                                else if weakObj.selectedLeagueType == FantasyType.Bowling.rawValue{
                                    playerInfoView.fantasyType = "3"
                                }
                                else if weakObj.selectedLeagueType == FantasyType.Reverse.rawValue{
                                    playerInfoView.fantasyType = "4"
                                }
                                else if weakObj.selectedLeagueType == FantasyType.Wizard.rawValue{
                                    playerInfoView.fantasyType = "5"
                                }

                               playerInfoView.updateData()
                                APPDELEGATE.window?.addSubview(playerInfoView)
                                playerInfoView.alpha = 0
                                UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                                    playerInfoView.alpha = 1
                                }) { _ in
                                    
                                }
                            }
                        }
                    }
                }
                else{
                    AppHelper.showAlertView(message: message, isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
}
