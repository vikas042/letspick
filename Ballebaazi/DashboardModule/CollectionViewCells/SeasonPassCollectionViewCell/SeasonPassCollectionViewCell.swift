//
//  SeasonPassCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class SeasonPassCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblOfferedPrice: UILabel!
    @IBOutlet weak var lblBuywith: UILabel!
    @IBOutlet weak var lblActualPrice: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var priceContainerView: UIView!
    
    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var lblTotalMatches: UILabel!
    @IBOutlet weak var innerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configData(details: PassesDetails, isPurchased: Bool) {
        DispatchQueue.main.async {
            self.priceContainerView.roundViewCorners(corners: [.bottomLeft, .bottomRight], radius: 7)
            self.imgView.roundViewCorners(corners: [.topLeft, .topRight], radius: 7)
        }
        if isPurchased {
            lblBuywith.text = "Purchased With".localized()
            purchaseButton.isHidden = true
        }
        else{
            lblBuywith.text = "Buy With".localized()
            purchaseButton.isHidden = false
        }

        lblDescription.text = details.seasonShortName
        lblTitle.text = details.titleEnglish
        lblTotalMatches.text = details.totalMatches + " " + "MATCHES".localized()
        lblOfferedPrice.text = "pts" + details.passPrice
        lblActualPrice.text = "pts" + details.actualAmount
        AppHelper.showShodowOnCellsView(innerView: innerView)
        if details.image.count < 10 {
          self.imgView.image = UIImage(named: "Placeholder")
        }
        else if let url = NSURL(string: details.image){
          imgView.setImage(with: url as URL, placeholder: UIImage(named: "Placeholder"), progress: { received, total in
          }, completion: { [weak self] image in
              if (image != nil){
                  self?.imgView.image = image
              }
              else{
                  self?.imgView.image = UIImage(named: "Placeholder")
              }
          })
        }
        else{
          self.imgView.image = UIImage(named: "Placeholder")
        }
    }
}
