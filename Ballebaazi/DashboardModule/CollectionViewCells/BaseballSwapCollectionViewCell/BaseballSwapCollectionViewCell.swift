//
//  FootballSwapCollectionViewCell.swift
//  Letspick
//
//  Created by Madstech on 29/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class BaseballSwapCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var captainSepImgView1: UIImageView!
    @IBOutlet weak var captainSepImgView2: UIImageView!
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var lblViceCaptain: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var teamPreviewButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var lblVicecaptainTitle: UILabel!
    @IBOutlet weak var lblCaptainTitle: UILabel!
    @IBOutlet weak var lblTeamNumber: UILabel!
    @IBOutlet weak var lblIfTitle: UILabel!
    @IBOutlet weak var lblPITTitle: UILabel!
    @IBOutlet weak var lblCatTitle: UILabel!
    @IBOutlet weak var lblOfTitle: UILabel!
    @IBOutlet weak var lblIfCount: UILabel!
    @IBOutlet weak var lblPitCount: UILabel!
    @IBOutlet weak var lblCatCount: UILabel!
    @IBOutlet weak var lblOfCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(_ details: UserTeamDetails, selectedTeam: UserTeamDetails?) {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblTeamNumber.text = "Team".localized().uppercased() + " " + details.teamNumber!
        innerView.clipsToBounds = true
        lblCaptain.text = details.captionName
        lblViceCaptain.text = details.viceCaptionName
        
        lblOfCount.text = details.totalOutFielderCount
        lblIfCount.text = details.totalInFielderCount
        lblPitCount.text = details.totalPitcherCount
        lblCatCount.text = details.totalCatcherCount
        
        lblOfTitle.text = "OF".localized()
        lblPITTitle.text = "PIT".localized()
        lblCatTitle.text = "CAT".localized()
        lblIfTitle.text = "IF".localized()

        lblCaptainTitle.text = "Captain".localized()
        lblVicecaptainTitle.text = "subCaptain".localized()

        
        if details.teamNumber == selectedTeam?.teamNumber {
            showViewForSelected()
        }
        else{
            showViewForUnselected()
        }
        
    }
    
    func showViewForSelected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgBlueIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorBlueIcon")

        headerContentView.backgroundColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)
        innerView.backgroundColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1)
        lblIfTitle.textColor = UIColor.white
        lblPITTitle.textColor = UIColor.white
        lblCatTitle.textColor = UIColor.white
        lblOfTitle.textColor = UIColor.white
        lblIfTitle.textColor = UIColor.white
        lblTeamNumber.textColor = UIColor.white
        lblCaptain.textColor = UIColor.white
        lblViceCaptain.textColor = UIColor.white
        lblCaptainTitle.textColor = UIColor.white
        lblVicecaptainTitle.textColor = UIColor.white
        lblPitCount.textColor = UIColor.white
        lblCatCount.textColor = UIColor.white
        lblOfCount.textColor = UIColor.white
        lblIfCount.textColor = UIColor.white
        
        lblPitCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblCatCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblOfCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        lblIfCount.backgroundColor = UIColor(red: 52.0/255, green: 155.0/255, blue: 250.0/255, alpha: 1)
        
        selectButton.isSelected = true;
    }
    
    func showViewForUnselected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorIcon")

        lblTeamNumber.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        headerContentView.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        innerView.backgroundColor = UIColor.white
        lblIfTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblPITTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblCatTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblOfTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblIfTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        lblViceCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        lblCaptainTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 135.0/255, alpha: 1)
        lblVicecaptainTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 135.0/255, alpha: 1)
        lblPitCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblCatCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblOfCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblIfCount.textColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1)
        lblPitCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblCatCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblOfCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        lblIfCount.backgroundColor = UIColor(red: 228.0/255, green: 238.0/255, blue: 247.0/255, alpha: 1)
        
        selectButton.isSelected = false;
    }
    
}
