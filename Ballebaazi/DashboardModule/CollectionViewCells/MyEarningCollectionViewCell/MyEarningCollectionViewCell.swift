//
//  MyEarningCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 27/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class MyEarningCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    var cellblock = {(sucess: Bool) -> () in }
    
    @IBOutlet weak var tblView: UITableView!
    
    var myTransactionArray = Array<[String: Any]>()
    var myEarningsArray = Array<ReferalDetails>()
    lazy var selectedTab = SelectedEarningTab.MyReferrals.rawValue
    lazy var isNeedToShowMyEarningsLoadMore = false
    lazy var isNeedToShowTransactionLoadMore = false

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func loadMoreBlock(complationBlock: @escaping (_ sucess: Bool) -> Void) {
        cellblock = complationBlock
    }
    
    func configData(transactions: Array<[String: Any]>, earningArray: Array<ReferalDetails>, earningsLoadMore: Bool, transactionLoadMore: Bool, tabType: Int) {
        
        isNeedToShowMyEarningsLoadMore = earningsLoadMore
        isNeedToShowTransactionLoadMore = transactionLoadMore
        
        myTransactionArray = transactions
        myEarningsArray = earningArray
        selectedTab = tabType
        lblNoDataFound.isHidden = true
        if selectedTab == SelectedEarningTab.MyReferrals.rawValue {
            if myEarningsArray.count == 0 {
                lblNoDataFound.isHidden = false
            }
            lblNoDataFound.text = "Oops ! No Referrals Found".localized()
        }
        else{
            if myTransactionArray.count == 0 {
                lblNoDataFound.isHidden = false
            }
            lblNoDataFound.text = "Oops ! No Transactions Found".localized()
        }

        tblView.tableFooterView = UIView()
        tblView.register(UINib(nibName: "MyEarningTableViewCell", bundle: nil), forCellReuseIdentifier: "MyEarningTableViewCell")
        tblView.register(UINib(nibName: "AffiliateTransactionsTableViewCell", bundle: nil), forCellReuseIdentifier: "AffiliateTransactionsTableViewCell")
        tblView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        tblView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedTab == SelectedEarningTab.MyReferrals.rawValue {
            return 1
        }
        else{
            return myTransactionArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if selectedTab == SelectedEarningTab.MyReferrals.rawValue {
            return 0
        }
        return 38
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedTab == SelectedEarningTab.MyReferrals.rawValue {
            if isNeedToShowMyEarningsLoadMore {
                return myEarningsArray.count + 1;
            }
            return myEarningsArray.count;
        }
        else{
            let transAction = myTransactionArray[section]
            let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>
            
            if isNeedToShowTransactionLoadMore && (section == myTransactionArray.count - 1) {
                return detailsArray.count + 1;
            }

            return detailsArray.count;
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if selectedTab == SelectedEarningTab.MyReferrals.rawValue {
            return nil;
        }
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 38))
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 4, width: tableView.bounds.size.width, height: 34))
        titleLabel.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        titleLabel.font = UIFont(name: "OpenSans-Semibold", size: 14)
        headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
                        
        let transAction = myTransactionArray[section]
        titleLabel.text = transAction["date"] as? String

        titleLabel.textAlignment = .center
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedTab == SelectedEarningTab.MyReferrals.rawValue {
            if  (indexPath.row < myEarningsArray.count){
                let details = myEarningsArray[indexPath.row]
                if details.lastPlayed.count == 0 {
                    return 58.0
                }
            }
            return 70.0
        }
        return 58.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedTab == SelectedEarningTab.MyReferrals.rawValue {

            if  (indexPath.row == myEarningsArray.count){

                var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell

                if cell == nil {
                    cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.none

                if isNeedToShowMyEarningsLoadMore{
                    cellblock(isNeedToShowMyEarningsLoadMore)
                    isNeedToShowMyEarningsLoadMore = false
                }

                return cell!
            }
            else {
                var cell = tableView.dequeueReusableCell(withIdentifier: "MyEarningTableViewCell") as? MyEarningTableViewCell
                
                if cell == nil {
                    cell = MyEarningTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "MyEarningTableViewCell")
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.none

                let details = myEarningsArray[indexPath.row]
                cell!.configData(details: details)
                return cell!
            }
        }
        else{
            let transAction = myTransactionArray[indexPath.section]
            let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>

            if  (indexPath.section == myTransactionArray.count - 1) && detailsArray.count == indexPath.row{

                var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell

                if cell == nil {
                    cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.none

                if isNeedToShowTransactionLoadMore{
                    cellblock(isNeedToShowTransactionLoadMore)
                    isNeedToShowTransactionLoadMore = false
                }

                return cell!
            }
            else {
                var cell = tableView.dequeueReusableCell(withIdentifier: "AffiliateTransactionsTableViewCell") as? AffiliateTransactionsTableViewCell
                
                if cell == nil {
                    cell = AffiliateTransactionsTableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "AffiliateTransactionsTableViewCell")
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.none

                let details = detailsArray[indexPath.row]
                cell!.configPartnershipData(details: details)
                return cell!
            }
        }
    }

}
