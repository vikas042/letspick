//
//  JusPayAvilableBanksCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class JusPayAvilableBanksCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
