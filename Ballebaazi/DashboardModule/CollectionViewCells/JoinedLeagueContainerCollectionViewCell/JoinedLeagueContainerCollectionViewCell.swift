//
//  JoinedLeagueContainerCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 20/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

protocol JoinedLeagueContainerCollectionViewCellDelegate {
    func refereshLeaguesData()
}

class JoinedLeagueContainerCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {

    @IBOutlet weak var placeholderimgView: UIImageView!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    var delegate: JoinedLeagueContainerCollectionViewCellDelegate?
    
    lazy var userTeamsArray = Array<UserTeamDetails>()
    var matchDetails: MatchDetails?
    var joinedLeagueArray: Array<JoinedLeagueDetails>?
    var selectedLeagueType = FantasyType.Classic.rawValue
    var selectedGameType = GameType.Cricket.rawValue

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configData(gameType: Int, leagueType: Int, selectedMatchDetails: MatchDetails?, leagueArray: Array<JoinedLeagueDetails>?, teamsArray: Array<UserTeamDetails>) {
        selectedGameType = gameType
        selectedLeagueType = leagueType
        joinedLeagueArray = leagueArray
        userTeamsArray = teamsArray
        matchDetails = selectedMatchDetails
    
        tblView.register(UINib(nibName: "JoinedLeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "JoinedLeagueTableViewCell")
        tblView.register(UINib(nibName: "CreatePrivateLeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "CreatePrivateLeagueTableViewCell")
        tblView.estimatedRowHeight = 70;
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.reloadData()
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60;
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let blankView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        blankView.backgroundColor = UIColor.clear
        return blankView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = joinedLeagueArray?.count {
            return count
        }

        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         var cell = tableView.dequeueReusableCell(withIdentifier: "JoinedLeagueTableViewCell") as? JoinedLeagueTableViewCell
         if cell == nil {
             cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "JoinedLeagueTableViewCell") as? JoinedLeagueTableViewCell
         }
         
         cell?.selectionStyle = UITableViewCellSelectionStyle.none
         let count = indexPath.row
         cell?.viewRankingButton.tag = count
         cell?.viewRankingButton.addTarget(self, action: #selector(viewRankingButtonTapped(button:)), for: .touchUpInside)
         let details = joinedLeagueArray?[count]
         if (matchDetails!.isMatchClosed)  {
             cell?.lblTeamCount.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details?.totalJoined ?? "0"))
         }
         else{
             if details?.isInfinity == "1"{
                 cell?.lblTeamCount.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details?.totalJoined ?? "0")) + "/" + "∞"
             }
             else{
                 cell?.lblTeamCount.text = AppHelper.makeCommaSeparatedDigitsWithString(digites: (details?.totalJoined ?? "0")) + "/" + AppHelper.makeCommaSeparatedDigitsWithString(digites: (details?.maxPlayers ?? "0"))
             }
         }
        
        cell?.configData(details: details!, matchDetails: matchDetails!, teamsArray: userTeamsArray, gameType: GameType.Cricket.rawValue)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        var details: JoinedLeagueDetails?
        let count = indexPath.row
        let joinedLeagueTeamVC = storyboard.instantiateViewController(withIdentifier: "JoinedLeagueTeamsViewController") as! JoinedLeagueTeamsViewController
        
        if selectedLeagueType == FantasyType.Classic.rawValue {
            details = joinedLeagueArray![count]
            let classicTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "1"
            })
            joinedLeagueTeamVC.userTeamsArray = classicTeamArray
        }
        else if selectedLeagueType == FantasyType.Batting.rawValue {
            details = joinedLeagueArray![count]
            let battingTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "2"
            })
            joinedLeagueTeamVC.userTeamsArray = battingTeamArray
        }
        else if selectedLeagueType == FantasyType.Bowling.rawValue {
            details = joinedLeagueArray![count]
            let bowlingTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "3"
            })
            joinedLeagueTeamVC.userTeamsArray = bowlingTeamArray
        }
        else if selectedLeagueType == FantasyType.Reverse.rawValue {
            details = joinedLeagueArray![count]
            let reverseTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "4"
            })
            joinedLeagueTeamVC.userTeamsArray = reverseTeamArray
        }
        else if selectedLeagueType == FantasyType.Wizard.rawValue {
            details = joinedLeagueArray![count]
            let wizardTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "5"
            })
            joinedLeagueTeamVC.userTeamsArray = wizardTeamArray
        }

        guard let leagueDetails = details else {
            return;
        }
        
        if leagueDetails.isPrivateLeague  && !leagueDetails.isPrivateLeagueJoined{
            return;
        }
        
        if leagueDetails.isPrivateLeague && !leagueDetails.isPrivateLeagueJoined && !matchDetails!.isMatchClosed && (leagueDetails.totalJoined != leagueDetails.maxPlayers){
            
            let privateLeagueDetails = LeagueDetails()
            privateLeagueDetails.leagueId = leagueDetails.leagueId!
            privateLeagueDetails.joiningAmount = leagueDetails.joiningAmount
            privateLeagueDetails.winAmount = leagueDetails.winAmount
            privateLeagueDetails.winAmount = leagueDetails.winAmount
            privateLeagueDetails.fantasyType = leagueDetails.fantasyType!
            privateLeagueDetails.confirmedLeague = leagueDetails.confirmedLeague
            privateLeagueDetails.matchKey = matchDetails!.matchKey
            privateLeagueDetails.teamType = "2"
            privateLeagueDetails.bonusApplicable = "1"
            privateLeagueDetails.isPrivateLeague = true
            callLeagueValidationAPI(leagueDetails: privateLeagueDetails, categoryName: kPrivateLeague)
            return;
        }
        else{
            joinedLeagueTeamVC.joinedLeagueDetails = details
            joinedLeagueTeamVC.selectedMatchDetails = matchDetails
           
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(joinedLeagueTeamVC, animated: true)
            }
        }
    }
    
    @objc func viewRankingButtonTapped(button: UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        var details: JoinedLeagueDetails?
        let count = button.tag
        let joinedLeagueTeamVC = storyboard.instantiateViewController(withIdentifier: "JoinedLeagueTeamsViewController") as! JoinedLeagueTeamsViewController
        
        if selectedLeagueType == FantasyType.Classic.rawValue {
            details = joinedLeagueArray![count]
            let classicTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "1"
            })
            joinedLeagueTeamVC.userTeamsArray = classicTeamArray
        }
        else if selectedLeagueType == FantasyType.Batting.rawValue {
            details = joinedLeagueArray![count]
            let battingTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "2"
            })
            joinedLeagueTeamVC.userTeamsArray = battingTeamArray
        }
        else if selectedLeagueType == FantasyType.Bowling.rawValue {
            details = joinedLeagueArray![count]
            let bowlingTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "3"
            })
            joinedLeagueTeamVC.userTeamsArray = bowlingTeamArray
        }
        else if selectedLeagueType == FantasyType.Reverse.rawValue {
            details = joinedLeagueArray![count]
            let reverseTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "4"
            })
            joinedLeagueTeamVC.userTeamsArray = reverseTeamArray
        }
        else if selectedLeagueType == FantasyType.Wizard.rawValue {
            details = joinedLeagueArray![count]
            let wizardTeamArray = self.userTeamsArray.filter({ (teamDetails) -> Bool in
                teamDetails.fantasyType == "5"
            })
            joinedLeagueTeamVC.userTeamsArray = wizardTeamArray
        }
        
        guard let leagueDetails = details else {
            return;
        }
        
        if leagueDetails.isPrivateLeague && !leagueDetails.isPrivateLeagueJoined && !matchDetails!.isMatchClosed && (leagueDetails.totalJoined != leagueDetails.maxPlayers){
            
            let privateLeagueDetails = LeagueDetails()
            privateLeagueDetails.leagueId = leagueDetails.leagueId!
            privateLeagueDetails.joiningAmount = leagueDetails.joiningAmount
            privateLeagueDetails.winAmount = leagueDetails.winAmount
            privateLeagueDetails.winAmount = leagueDetails.winAmount
            privateLeagueDetails.fantasyType = leagueDetails.fantasyType!
            privateLeagueDetails.confirmedLeague = leagueDetails.confirmedLeague
            privateLeagueDetails.matchKey = matchDetails!.matchKey
            privateLeagueDetails.teamType = "2"
            privateLeagueDetails.bonusApplicable = "1"
            privateLeagueDetails.isPrivateLeague = true
            callLeagueValidationAPI(leagueDetails: privateLeagueDetails, categoryName: kPrivateLeague)
            return;
        }
        else if leagueDetails.isPrivateLeague && leagueDetails.isPrivateLeagueJoined && !matchDetails!.isMatchClosed {
            
            var userName = UserDetails.sharedInstance.name
            if userName.count == 0 {
                userName = UserDetails.sharedInstance.userName
            }
            
            
            let text = String(format: "%@ has invited you to join the private league %@ on Letspick. Use code %@ to join.\nhttps://Letspick.app.link?league_code=%@", userName, (leagueDetails.leagueName ?? ""), leagueDetails.leagueCode, leagueDetails.leagueCode)
            
            // set up activity view controller
            let textToShare = [ text ]
            let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
            
            // present the view controller
            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navigationVC.present(activityViewController, animated: true, completion: nil)
            }
        }
        else{
            joinedLeagueTeamVC.joinedLeagueDetails = details
            joinedLeagueTeamVC.selectedMatchDetails = matchDetails
           
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(joinedLeagueTeamVC, animated: true)
            }
        }
    }
    
    func callLeagueValidationAPI(leagueDetails: LeagueDetails, categoryName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        let params = ["option": "league_prev_data_v2","check_ticket": "1", "user_id": UserDetails.sharedInstance.userID, "match_key": leagueDetails.matchKey, "league_id": leagueDetails.leagueId, "fantasy_type": leagueDetails.fantasyType, "is_private": "1"]
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kMatch, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelper.sharedInstance.removeSpinner()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if result != nil{
                let statusCode = result!["status"]?.string
                let message = result!["message"]?.string

                DispatchQueue.main.async {
                    let response = result!["response"]?.dictionary
                    let is_multi_joining = response?["is_multi_joining"]?.intValue
                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                    var ticketDetais: TicketDetails?
                    if let applied_ticket = response?["ticket_applied"]?.intValue{
                        UserDetails.sharedInstance.ticketApplied = applied_ticket

                        if (response?["ticket"]?.dictionary) != nil{
                            if (response?["ticket"]?.dictionary) != nil{
                                ticketDetais = TicketDetails.parseTicketDetails(response!["ticket"]!)
                            }
                        }
                    }
                    
                    if statusCode == "401"{
                        let titleMessage = "Oops! Low Balance".localized()
                        
                        if let response = result!["response"]?.dictionary{
                            if let teamsArray = response["teams"]?.array{
                                if teamsArray.count != 0{
                                    let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                        teamDetails.fantasyType == leagueDetails.fantasyType
                                    })
                                    
                                    var teamArray = Array<UserTeamDetails>()
                                    
                                    for teamDetails in fantacyArray{
                                        for validTeamNumber in teamsArray{
                                            if let teamNumber = validTeamNumber.string{
                                                if teamNumber == teamDetails.teamNumber{
                                                    teamArray.append(teamDetails)
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                    let is_multi_joining = response["is_multi_joining"]?.intValue
                                    UserDetails.sharedInstance.isMultiJoiningAllow = is_multi_joining ?? 0
                                    let creditRequired = response["credit_required"]?.stringValue ?? "0"
                                    let responseAmt = Float(creditRequired)!

                                    let joiningAmount = Float(leagueDetails.joiningAmount)!
                                    let roundFigureAmt  = Int(ceil(joiningAmount - responseAmt))
                                    let message = String(format: notEnoughPoints, String(roundFigureAmt))
                                    
                                    let alert = UIAlertController(title: titleMessage, message: "Not enough points to join this league", preferredStyle: UIAlertControllerStyle.alert)
                                    /*
                                    alert.addAction(UIAlertAction(title: "Add Cash".localized(), style: UIAlertActionStyle.default, handler: { action -> Void in
                                        
                                        let addCashVC = storyboard.instantiateViewController(withIdentifier: "AddCashViewController") as? AddCashViewController
                                        addCashVC?.leagueDetails = leagueDetails
                                        addCashVC?.amount = roundFigureAmt
                                        addCashVC?.matchDetails = weakSelf!.matchDetails
                                        addCashVC?.userTeamArray = teamArray
                                        addCashVC?.categoryName = categoryName
                                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                            navVC.pushViewController(addCashVC!, animated: true)
                                        }
                                    }))
                                    */
                                    alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                        navVC.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else{
                                    let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                    playerVC.matchDetails = weakSelf?.matchDetails
                                    playerVC.leagueDetails = leagueDetails
                                    playerVC.ticketDetails = ticketDetais
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                         navVC.pushViewController(playerVC, animated: true)
                                    }
                                }
                            }
                            else{
                                AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                            }
                        }
                        else{
                            AppHelper.showAlertView(message: message ?? "kErrorMsg".localized(), isErrorMessage: true)
                        }
                    }
                    else if (statusCode == "400") || (statusCode == "402"){
                        let titleStr = result!["title"]?.string ?? ""
                        if titleStr == "no_teams"{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                 navVC.pushViewController(playerVC, animated: true)
                            }
                        }
                        else
                        {
                            AppHelper.showAlertView(message: message!, isErrorMessage: true)
                        }
                    }
                    else if statusCode == "200"{
                        let response = result!["response"]?.dictionary
                        
                        if let teamsArray = response?["teams"]?.array{
                            if teamsArray.count != 0{
                                let fantacyArray = UserDetails.sharedInstance.userTeamsArray.filter({ (teamDetails) -> Bool in
                                    teamDetails.fantasyType == leagueDetails.fantasyType
                                })
                                
                                var teamArray = Array<UserTeamDetails>()
                                
                                for teamDetails in fantacyArray{
                                    
                                    for validTeamNumber in teamsArray{
                                        if let teamNumber = validTeamNumber.string{
                                            if teamNumber == teamDetails.teamNumber{
                                                teamArray.append(teamDetails)
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                weakSelf?.goToConfirmLeagueScreen(leagueDetails: leagueDetails, userTeamArray: teamArray, ticketDetails: ticketDetais)
                            }
                            else{
                                let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                                playerVC.matchDetails = weakSelf?.matchDetails
                                playerVC.leagueDetails = leagueDetails
                                playerVC.ticketDetails = ticketDetais
                                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                     navVC.pushViewController(playerVC, animated: true)
                                }
                            }
                        }
                        else{
                            let playerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
                            playerVC.matchDetails = weakSelf?.matchDetails
                            playerVC.leagueDetails = leagueDetails
                            playerVC.ticketDetails = ticketDetais
                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                 navVC.pushViewController(playerVC, animated: true)
                            }
                        }
                    }
                    else{
                        AppHelper.showAlertView(message: message!, isErrorMessage: true)
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    func goToConfirmLeagueScreen(leagueDetails: LeagueDetails, userTeamArray: Array<UserTeamDetails>, ticketDetails: TicketDetails?)  {
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let joinedLeagueConfirmVC = storyboard.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as? JoinLeagueConfirmationViewController
            joinedLeagueConfirmVC?.leagueDetails = leagueDetails
            joinedLeagueConfirmVC?.ticketDetails = ticketDetails
            joinedLeagueConfirmVC?.userTeamsArray = userTeamArray
            joinedLeagueConfirmVC?.matchDetails = self.matchDetails
            joinedLeagueConfirmVC?.leagueCategoryName = kPrivateLeague
            joinedLeagueConfirmVC?.selectedGameType = selectedGameType
            navigationVC.pushViewController(joinedLeagueConfirmVC!, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y < -120{
            delegate?.refereshLeaguesData()
        }
    }
        
}
