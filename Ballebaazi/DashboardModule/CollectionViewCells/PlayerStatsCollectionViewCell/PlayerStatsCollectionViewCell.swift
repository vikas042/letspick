//
//  PlayerStatsCollectionViewCell.swift
//  Letspick
//
//  Created by Madstech on 21/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class PlayerStatsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var checkboxButton: UIButton!
    @IBOutlet weak var lblTotlaCreditTitle: UILabel!
    @IBOutlet weak var lblTotalPointsTitle: UILabel!
    @IBOutlet weak var lblPlayerRole: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblTotalCredits: UILabel!
    @IBOutlet weak var lblpoints: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var playerTypeImageView: UIImageView!
    @IBOutlet weak var lblPredicted: UILabel!
    @IBOutlet weak var lblLastPlayed: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configDetails(details: PlayerDetails, legueType: Int, gameType: Int, selectedPlayerDetails: PlayerDetails?, isLineupsAnnounced: Bool)  {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        
        checkboxButton.isSelected = false
        if selectedPlayerDetails?.playerKey == details.playerKey{
            checkboxButton.isSelected = true
            showSelectedCell()
        }
        else{
            showUnselectedCell()
        }
        
                
        if isLineupsAnnounced {
            if details.isPlayerPlaying{
                lblLastPlayed.isHidden = false
                lblLastPlayed.text = "ANNOUNCED".localized()
                lblLastPlayed.backgroundColor = UIColor(red: 60.0/255, green: 196.0/255, blue: 66.0/255, alpha: 1)
            }
        }
        else{
            lblLastPlayed.isHidden = true
            lblLastPlayed.text = ""
            
            if details.isPlaying11Last == "1"{
                lblLastPlayed.isHidden = false
                lblLastPlayed.text = "LAST".localized()
                lblLastPlayed.backgroundColor = UIColor(red: 255.0/255, green: 184.0/255, blue: 64.0/255, alpha: 1)
            }
            
            if details.isPlaying11Prob == "1"{
                lblPredicted.isHidden = false
                lblPredicted.text = "PREDICTED".localized()
                lblPredicted.backgroundColor = UIColor(red: 83.0/255, green: 173.0/255, blue: 255.0/255, alpha: 1)
            }
            else{
                lblPredicted.isHidden = true
                lblPredicted.text = ""
            }
        }
        
        lblPlayerName.text = details.playerName
        lblTotalCredits.text = details.credits
        
        lblTotlaCreditTitle.text = "Credits".localized()
        lblTotalPointsTitle.text = "Total Points".localized()
        
        lblPlayerRole.text =  ""
        if legueType == FantasyType.Classic.rawValue {
            lblpoints.text = details.classicPoints
        }
        else if legueType == FantasyType.Batting.rawValue{
            lblpoints.text = details.battingPoints
        }
        else if legueType == FantasyType.Bowling.rawValue{
            lblpoints.text = details.bowlingPoints
        }
        
        showPlayerPhotoPlaceholder(details: details, gameType: gameType)
        playerTypeImageView.image = UIImage(named: details.playerPlaceholder)
        if details.imgURL.count > 0 {
            if let url = URL(string: details.imgURL){
                playerTypeImageView.setImage(with: url, placeholder: UIImage(named: details.playerPlaceholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerTypeImageView.image = image
                    }
                    else{
                        self?.playerTypeImageView.image = UIImage(named: details.playerPlaceholder)
                    }
                })
            }
        }
    }
    
    func showPlayerPhotoPlaceholder(details: PlayerDetails, gameType: Int) {
        if details.playerPlayingRole == PlayerType.Batsman.rawValue {
            playerTypeImageView.image = UIImage(named: "BatIcon")
            lblPlayerRole.text = "Batsman".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.Bowler.rawValue {
            playerTypeImageView.image = UIImage(named: "BowlIcon")
            lblPlayerRole.text = "Bowler".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.AllRounder.rawValue {
            lblPlayerRole.text = "All Rounder".localized() + " | " + details.teamShortName
            if gameType == GameType.Kabaddi.rawValue{
                playerTypeImageView.image = UIImage(named: "AllrounderKabaddiPlaceholder")
            }
            else{
                playerTypeImageView.image = UIImage(named: "AllRounderIcon")
            }
        }
        else if details.playerPlayingRole == PlayerType.WicketKeeper.rawValue {
            playerTypeImageView.image = UIImage(named: "WicketIcon")
            lblPlayerRole.text = "Wicket Keeper".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.Defender.rawValue {
            lblPlayerRole.text = "Defender".localized() + " | " + details.teamShortName
            
            if gameType == GameType.Football.rawValue{
                playerTypeImageView.image = UIImage(named: "FootballDefenderIcon")
            }
            else{
                playerTypeImageView.image = UIImage(named: "DefenderIconPlaceholder")
            }
        }
        else if details.playerPlayingRole == PlayerType.Raider.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Raider".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.MidFielder.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "MidFielder".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.Sticker.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Striker".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.GoalKeeper.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Goal Keeper".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.ShootingGuard.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Shooting-Guard".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.PointGuard.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Point-Guard".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.SmallForward.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Samll-Forward".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.Center.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Center".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.PowerForward.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Power-Forward".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.Outfielders.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Outfielder".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.Catcher.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Catcher".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.Infielder.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Infielder".localized() + " | " + details.teamShortName
        }
        else if details.playerPlayingRole == PlayerType.Pitcher.rawValue {
            playerTypeImageView.image = UIImage(named: "RiderPlaceholder")
            lblPlayerRole.text = "Pitcher".localized() + " | " + details.teamShortName
        }
    }
    
    func showSelectedCell() {
        innerView.backgroundColor = UIColor(red: 0.0/255, green: 118.0/255, blue: 226.0/255, alpha: 1)
        lblPlayerRole.textColor = UIColor.white
        lblPlayerName.textColor = UIColor.white
        lblTotalCredits.textColor = UIColor.white
        lblpoints.textColor = UIColor.white
        lblTotalPointsTitle.textColor = UIColor.white
        lblTotlaCreditTitle.textColor = UIColor.white

    }
    
    func showUnselectedCell() {
        innerView.backgroundColor = UIColor.white

        lblPlayerRole.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblTotlaCreditTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblTotalPointsTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)

        lblPlayerName.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        lblTotalCredits.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        lblpoints.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)

    }
}
