//
//  CricketMyTeamCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 25/09/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class CricketMyTeamCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tblView: UITableView!
    var matchDetails: MatchDetails?
    var selectedFantasy = FantasyType.Classic.rawValue
    var teamsArray = Array<UserTeamDetails>()
    @IBOutlet weak var lblDoNotHaveTeam: UILabel!
    @IBOutlet weak var createFirstTeamButton: SolidButton!
    @IBOutlet weak var lblCreateTeamMessage: UILabel!
    @IBOutlet var noTeamView: UIView!
    var maxTeamAllow = 0
    
    @IBOutlet weak var wizardViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblWizardPlayerName: UILabel!
    @IBOutlet weak var lblWizardIcon: UILabel!
    @IBOutlet weak var wizardView: UIView!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configData(userTeamsArray: Array<UserTeamDetails>, fantasyType: Int, selectedMatchDetails: MatchDetails?, leagueInfo: LeagueDetails, teamAllowed: Int)    {
        selectedFantasy = fantasyType;
        tblView.register(UINib(nibName: "TeamTableViewCell", bundle: nil), forCellReuseIdentifier: "TeamTableViewCell")
        matchDetails = selectedMatchDetails
        teamsArray = userTeamsArray
        maxTeamAllow = teamAllowed
        
        tblView.reloadData();
        noTeamView.isHidden = true
        tblView.isHidden = false
        if userTeamsArray.count == 0 {
            noTeamView.isHidden = false
            tblView.isHidden = true
        }
        lblCreateTeamMessage.text = ""
        lblDoNotHaveTeam.text = "You don't have any team as of now.\nCreate your team to participte and earn."

        createFirstTeamButton.setTitle("Create First Team".localized(), for: .normal)
        lblDoNotHaveTeam.isHidden = false
        lblCreateTeamMessage.isHidden = false
        createFirstTeamButton.isHidden = false
        if selectedMatchDetails?.isMatchClosed ?? false {
            lblDoNotHaveTeam.text = "didNotHaveTeam".localized()
            lblDoNotHaveTeam.isHidden = false
            lblCreateTeamMessage.isHidden = false
            createFirstTeamButton.isHidden = true
        }
        
        if (fantasyType == FantasyType.Classic.rawValue) || (fantasyType == FantasyType.Reverse.rawValue) || fantasyType == FantasyType.Wizard.rawValue {
            lblCreateTeamMessage.text = "Create your team of 11 players. Make upto " + String(maxTeamAllow) + " teams and win more."
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                if "hi" == lang[0]{
                    lblCreateTeamMessage.text = "11 खिलाड़ियों की अपनी टीम बनाएं।  " + String(maxTeamAllow) + " टीमें बनाएं और अधिक जीतें"
                }
                else{
                    lblCreateTeamMessage.text = "Create your team of 11 players. Make upto " + String(maxTeamAllow) + " teams and win more."
                }
            }
        }
        else if fantasyType == FantasyType.Batting.rawValue {
            lblCreateTeamMessage.text = "CreateFirstBattingTeam".localized()

        }
        else if fantasyType == FantasyType.Bowling.rawValue {
            lblCreateTeamMessage.text = "CreateFirstBowlingTeam".localized()
        }
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if FantasyType.Wizard.rawValue == selectedFantasy {
            return 210.0
        }
        return 180.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let blankView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        
        blankView.backgroundColor = UIColor.clear
        return blankView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "TeamTableViewCell") as? TeamTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "TeamTableViewCell") as? TeamTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        let details = teamsArray[indexPath.row]
        cell?.configDetails(details: details, selectedMatchDetails: matchDetails, selectedType: selectedFantasy, teamCount: teamsArray.count)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if matchDetails!.isMatchClosed {
            let details = teamsArray[indexPath.row]
            let teamNumber = details.teamNumber ?? ""
            callGetFullScoreTeamPlayersDetails(userID: UserDetails.sharedInstance.userID, teamNumber: teamNumber, userName: UserDetails.sharedInstance.userName)
        }
        else{
            AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Cricket"])

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let teamPlayerList = storyboard.instantiateViewController(withIdentifier: "TeamPreviewViewController") as! TeamPreviewViewController
            let details = teamsArray[indexPath.row]
            teamPlayerList.matchDetails = matchDetails
            teamPlayerList.totalPlayerArray = details.playersArray!
            teamPlayerList.teamNumber = details.teamNumber ?? ""
            teamPlayerList.selectedFantasy = selectedFantasy
            teamPlayerList.firstTeamName = matchDetails!.firstTeamShortName ?? ""
            teamPlayerList.secondTeamName = matchDetails!.secondTeamShortName ?? ""
            
            teamPlayerList.firstTeamkey = matchDetails!.firstTeamKey
            teamPlayerList.secondTeamkey = matchDetails!.secondTeamKey

            
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                navVC.pushViewController(teamPlayerList, animated: true)
            }
        }
    }
    
    func callGetFullScoreTeamPlayersDetails(userID: String, teamNumber: String, userName: String)  {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        let type = "user_team"
        
        var fantasyType = "1"
        
        if selectedFantasy == FantasyType.Classic.rawValue {
            fantasyType = "1"
        }
        else if selectedFantasy == FantasyType.Batting.rawValue {
            fantasyType = "2"
        }
        else if selectedFantasy == FantasyType.Bowling.rawValue {
            fantasyType = "3"
        }
        else if selectedFantasy == FantasyType.Reverse.rawValue {
            fantasyType = "4"
        }
        else if selectedFantasy == FantasyType.Wizard.rawValue {
            fantasyType = "5"
        }

        let parameters = ["option": "full_scoreboard", "match_key": matchDetails!.matchKey, "team_number": teamNumber,"user_id": userID, "type": type, "fantasy_type": fantasyType]
        
        WebServiceHandler.performPOSTRequest(urlString: kSocrescardUrl, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                let statusCode = result!["status"]
                if statusCode == "200"{
                    
                    if let playerArray = result!["response"]?.array{
                        let playerListArray = PlayerDetails.getPlayerDetailsForScoreArray(responseArray: playerArray)
                        if self.matchDetails!.playersGender == "F" {
                            PlayerDetails.changeFemalePlayerPlaceholder(playerArray: playerListArray)
                        }
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        AppxorEventHandler.logAppEvent(withName: "TeamPreviewClicked", info: ["SportType": "Cricket"])

                        let teamPlayerList = storyboard.instantiateViewController(withIdentifier: "TeamPreviewViewController") as! TeamPreviewViewController
                        teamPlayerList.matchDetails = self.matchDetails
                        teamPlayerList.isMatchClosed = true
                        teamPlayerList.totalPlayerArray = playerListArray
                        teamPlayerList.firstTeamName = self.matchDetails!.firstTeamShortName ?? ""
                        teamPlayerList.teamNumber = teamNumber
                        teamPlayerList.userName = userName
                        
                        teamPlayerList.secondTeamName = self.matchDetails!.secondTeamShortName ?? ""
                        teamPlayerList.firstTeamkey = self.matchDetails!.firstTeamKey
                        teamPlayerList.secondTeamkey = self.matchDetails!.secondTeamKey

                        teamPlayerList.selectedFantasy = self.selectedFantasy
                        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                            navVC.pushViewController(teamPlayerList, animated: true)
                        }
                    }
                }
                else{
                    let message = result!["message"]?.string
                    AppHelper.showAlertView(message: message ?? "", isErrorMessage: true)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
    @IBAction func createNewTeamButtonTapped(_ sender: Any) {
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            UserDetails.sharedInstance.selectedPlayerList.removeAll()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let selectPlayerVC = storyboard.instantiateViewController(withIdentifier: "SelectPlayerViewController") as! SelectPlayerViewController
            selectPlayerVC.matchDetails = matchDetails
            let leagueDetails = LeagueDetails()
            
            if selectedFantasy == FantasyType.Classic.rawValue {
                leagueDetails.fantasyType = "1"
            }
            else if selectedFantasy == FantasyType.Batting.rawValue {
                leagueDetails.fantasyType = "2"
            }
            else if selectedFantasy == FantasyType.Bowling.rawValue {
                leagueDetails.fantasyType = "3"
            }
            else if selectedFantasy == FantasyType.Reverse.rawValue {
                leagueDetails.fantasyType = "4"
            }
            else if selectedFantasy == FantasyType.Wizard.rawValue {
                leagueDetails.fantasyType = "5"
            }

            selectPlayerVC.leagueDetails = leagueDetails
            navVC.pushViewController(selectPlayerVC, animated: true)
        }
    }
    
}
