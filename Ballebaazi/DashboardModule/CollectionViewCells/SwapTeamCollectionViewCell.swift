//
//  SwapTeamCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class SwapTeamCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var wizardViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblWizardPlayerName: UILabel!
    @IBOutlet weak var lblWizardIcon: UILabel!
    @IBOutlet weak var wizardView: UIView!
    @IBOutlet weak var captainSepImgView1: UIImageView!
    @IBOutlet weak var captainSepImgView2: UIImageView!
    
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var lblBowTitle: UILabel!
    @IBOutlet weak var lblAllRounderTitle: UILabel!
    @IBOutlet weak var lblBatTitle: UILabel!
    @IBOutlet weak var lblWkTitle: UILabel!
    @IBOutlet weak var lblViceCaptain: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblBowlerCount: UILabel!
    @IBOutlet weak var lblAllrounderCount: UILabel!
    @IBOutlet weak var lblBatsmanCount: UILabel!
    @IBOutlet weak var lblWkCount: UILabel!
    @IBOutlet weak var lblCaptain: UILabel!
    @IBOutlet weak var teamPreviewButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    
    @IBOutlet weak var lblVicecaptainTitle: UILabel!
    @IBOutlet weak var lblCaptainTitle: UILabel!
    @IBOutlet weak var lblTeamNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(_ details: UserTeamDetails, selectedTeam: UserTeamDetails?) {
        AppHelper.showShodowOnCellsView(innerView: innerView)
        lblTeamNumber.text = "Team".localized().uppercased() + " " + details.teamNumber!
        innerView.clipsToBounds = true
        
        lblCaptainTitle.text = "Captain".localized()
        lblVicecaptainTitle.text = "subCaptain".localized()
        
        lblCaptain.text = details.captionName
        lblViceCaptain.text = details.viceCaptionName
        lblAllrounderCount.text = details.totalAllrounderCount
        lblBowlerCount.text = details.totalBowlerCount
        lblWkCount.text = details.totalKeeperCount
        lblBatsmanCount.text = details.totalBatsmanCount
        
        lblAllRounderTitle.text = "ALR".localized()
        lblBatTitle.text = "BAT".localized()
        lblWkTitle.text = "WK".localized()
        lblBowTitle.text = "BOW".localized()
        lblWizardIcon.layer.cornerRadius = 11.0
        lblWizardIcon.layer.borderWidth = 1
        
        if (details.wizardType == "1"){
            wizardView.isHidden = false
            wizardViewHeightConstraint.constant = 30
            lblWizardPlayerName.text = "\(details.wizardPlayerName.uppercased())  (3x)"
        }
        else if (details.wizardType == "2"){
            wizardView.isHidden = false
            wizardViewHeightConstraint.constant = 30
            lblWizardPlayerName.text = "\(details.wizardPlayerName.uppercased())  (5x)".uppercased()
        }
        else if (details.wizardType == "3"){
            wizardView.isHidden = false
            wizardViewHeightConstraint.constant = 30
            lblWizardPlayerName.text = "\(details.wizardPlayerName.uppercased())  (4.5x)"
        }

        if details.teamNumber == selectedTeam?.teamNumber {
            showViewForSelected()
        }
        else{
            showViewForUnselected()
        }
    }
    
    func showViewForSelected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgBlueIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorBlueIcon")
        
        headerContentView.backgroundColor = UIColor(red: 34/255, green: 114/255, blue: 70/255, alpha: 1)
        wizardView.backgroundColor = UIColor(red: 105.0/255, green: 160.0/255, blue: 230.0/255, alpha: 1)
        
        innerView.backgroundColor = UIColor(red: 0/255, green: 156/255, blue: 79/255, alpha: 1)
        lblBowTitle.textColor = UIColor.white
        lblAllRounderTitle.textColor = UIColor.white
        lblBatTitle.textColor = UIColor.white
        lblWkTitle.textColor = UIColor.white
        lblBowTitle.textColor = UIColor.white
        lblTeamNumber.textColor = UIColor.white
        lblCaptain.textColor = UIColor.white
        lblViceCaptain.textColor = UIColor.white
        lblWizardIcon.layer.borderColor = UIColor.white.cgColor

        
        lblWizardPlayerName.textColor = UIColor.white
        lblWizardIcon.textColor = UIColor.white
        lblWizardIcon.backgroundColor = UIColor.clear

        lblWizardIcon.layer.borderWidth = 1.0
        
        lblCaptainTitle.textColor = UIColor.white
        lblVicecaptainTitle.textColor = UIColor.white

        lblAllrounderCount.textColor = UIColor.white
        lblBatsmanCount.textColor = UIColor.white
        lblWkCount.textColor = UIColor.white
        lblBowlerCount.textColor = UIColor.white
        
        var colorCount = UIColor(red: 34/255, green: 114/255, blue: 70/255, alpha: 1)
        
        lblAllrounderCount.backgroundColor = colorCount
        lblBatsmanCount.backgroundColor = colorCount
        lblWkCount.backgroundColor = colorCount
        lblBowlerCount.backgroundColor = colorCount
        selectButton.isSelected = true;
    }
    
    func showViewForUnselected() {
        captainSepImgView1.image = UIImage(named: "CaptainSepratorBgIcon")
        captainSepImgView2.image = UIImage(named: "CaptainSepratorIcon")

        lblTeamNumber.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        headerContentView.backgroundColor = UIColor(red: 130/255, green: 215/255, blue: 182/255, alpha: 1)
        wizardView.backgroundColor = UIColor(red: 241.0/255, green: 245.0/255, blue: 250.0/255, alpha: 1)
        lblWizardIcon.layer.borderColor = UIColor(red: 48.0/255, green: 150.0/255, blue: 244.0/255, alpha: 1).cgColor
        innerView.backgroundColor = UIColor.white
        lblBowTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblAllRounderTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblBatTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblWkTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblBowTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1)
        lblCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        lblViceCaptain.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 48.0/255, alpha: 1)
        
        lblCaptainTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 135.0/255, alpha: 1)
        lblWizardPlayerName.textColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 47.0/255, alpha: 1)
        lblWizardIcon.layer.borderWidth = 0.0

        lblWizardIcon.textColor = UIColor(red: 238.0/255, green: 188.0/255, blue: 65.0/255, alpha: 1)
        lblWizardIcon.backgroundColor = UIColor(red: 45.0/255, green: 46.0/255, blue: 47.0/255, alpha: 1)

        lblVicecaptainTitle.textColor = UIColor(red: 127.0/255, green: 132.0/255, blue: 135.0/255, alpha: 1)
        
        lblWizardIcon.textColor = UIColor.white
        
        var colorCount = UIColor(red: 130/255, green: 215/255, blue: 182/255, alpha: 1)
        var textColorUnselect = UIColor(red: 34/255, green: 114/255, blue: 70/255, alpha: 1)

        lblAllrounderCount.textColor = textColorUnselect
        lblBatsmanCount.textColor = textColorUnselect
        lblWkCount.textColor = textColorUnselect
        lblBowlerCount.textColor = textColorUnselect
        
        lblAllrounderCount.backgroundColor = colorCount
        lblBatsmanCount.backgroundColor = colorCount
        lblWkCount.backgroundColor = colorCount
        lblBowlerCount.backgroundColor = colorCount

        selectButton.isSelected = false;
    }
    
}

extension UIImage {
    func getPixelColor() -> UIColor {

        let pos = CGPoint(x: 0, y: 0)
        let pixelData = self.cgImage!.dataProvider!.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

        let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4

        let r = CGFloat(data[pixelInfo]) / CGFloat(255.0)
        let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
        let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)

        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
}
