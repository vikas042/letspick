//
//  LeaguePreviewDetailsCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class LeaguePreviewDetailsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblInfinityIcon: UILabel!
    var multipleTeamMessage = ""
    let confirmLeagueMessage = "confirmLeagueMessage".localized()
    
    @IBOutlet weak var progressViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressBgView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet var lblFirstMatchTypeMessage: UILabel!
    @IBOutlet var lblSecondMatchTypeMessage: UILabel!
    @IBOutlet var lblThirdMatchTypeMessage: UILabel!
    @IBOutlet var lblFirstTimer: UILabel!
    @IBOutlet var lblSecondTimer: UILabel!
    @IBOutlet var lblThirdTimer: UILabel!
    @IBOutlet var lblFirstMatchType: UILabel!
    @IBOutlet var lblSecondMatchType: UILabel!
    @IBOutlet var lblThirdMatchType: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet var lblJackpotLeague: UILabel!
    @IBOutlet var lblCoinApplicable: UILabel!
    @IBOutlet var imgViewCoinApplicable: UIImageView!

    var isNeedtoShowBBCoins = false
    
    @IBOutlet var imgViewJackpotLeague: UIImageView!
    @IBOutlet weak var lblTotalWinningsTitle: UILabel!
    @IBOutlet weak var lblTotalWinning: UILabel!
    @IBOutlet weak var lblJoinedLeagueCount: UILabel!
    @IBOutlet weak var lblMaxTeam: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(leagueDetails: LeagueDetails, gameType: Int, matchDetails: MatchDetails, isCoinApplicable: Bool) {
        AppHelper.showShodowOnCellsView(innerView: innerView);
        var appliedBonusPercentage = leagueDetails.bounsPercentage
        isNeedtoShowBBCoins = isCoinApplicable
        var bonusMessage = leagueDetails.bounsPercentage + "bonusMessage".localized()
        lblFirstTimer.textColor = confirmLeageTextColor
        lblSecondTimer.textColor = confirmLeageTextColor
        lblThirdTimer.textColor = confirmLeageTextColor
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                if gameType == GameType.Cricket.rawValue {
                    if leagueDetails.leagueType == "1" {
                        multipleTeamMessage = "आप \(UserDetails.sharedInstance.maxTeamAllowedForClassic) टीम से ज्वाइन कर सकते हैं"
                    }
                    else if leagueDetails.leagueType == "2" {
                        multipleTeamMessage = "आप \(UserDetails.sharedInstance.maxTeamAllowedForBatting) टीम से ज्वाइन कर सकते हैं"
                    }
                    else if leagueDetails.leagueType == "3" {
                        multipleTeamMessage = "आप \(UserDetails.sharedInstance.maxTeamAllowedForBowling) टीम से ज्वाइन कर सकते हैं"
                    }
                }
                else if gameType == GameType.Kabaddi.rawValue {
                    multipleTeamMessage = "आप \(UserDetails.sharedInstance.maxTeamAllowedForKabaddi) टीम से ज्वाइन कर सकते हैं"
                }
                else if gameType == GameType.Football.rawValue {
                    multipleTeamMessage = "आप \(UserDetails.sharedInstance.maxTeamAllowedForFootball) टीम से ज्वाइन कर सकते हैं"
                }
                else if gameType == GameType.Basketball.rawValue {
                    multipleTeamMessage = "आप \(UserDetails.sharedInstance.maxTeamAllowedForBasketball) टीम से ज्वाइन कर सकते हैं"
                }
                else if gameType == GameType.Baseball.rawValue {
                    multipleTeamMessage = "आप \(UserDetails.sharedInstance.maxTeamAllowedForBaseball) टीम से ज्वाइन कर सकते हैं"
                }
            }
            else{
                
                if gameType == GameType.Cricket.rawValue {
                    if leagueDetails.leagueType == "1" {
                        multipleTeamMessage = "You can join with \(UserDetails.sharedInstance.maxTeamAllowedForClassic) teams"
                    }
                    else if leagueDetails.leagueType == "2" {
                        multipleTeamMessage = "You can join with \(UserDetails.sharedInstance.maxTeamAllowedForBatting) teams"
                    }
                    else if leagueDetails.leagueType == "3" {
                        multipleTeamMessage = "You can join with \(UserDetails.sharedInstance.maxTeamAllowedForBowling) teams"
                    }
                }
                else if gameType == GameType.Kabaddi.rawValue {
                    multipleTeamMessage = "You can join with \(UserDetails.sharedInstance.maxTeamAllowedForKabaddi) teams"
                }
                else if gameType == GameType.Football.rawValue {
                    multipleTeamMessage = "You can join with \(UserDetails.sharedInstance.maxTeamAllowedForFootball) teams"
                }
                else if gameType == GameType.Basketball.rawValue {
                    multipleTeamMessage = "You can join with \(UserDetails.sharedInstance.maxTeamAllowedForBasketball) teams"
                }
                else if gameType == GameType.Baseball.rawValue {
                    multipleTeamMessage = "You can join with \(UserDetails.sharedInstance.maxTeamAllowedForBaseball) teams"
                }
            }
        }
        
        imgViewJackpotLeague.isHidden = true
        lblJackpotLeague.isHidden = true
        lblCoinApplicable.text = "Earn and Use LP Coins in Reward store".localized()
        lblCoinApplicable.isHidden = true
        imgViewCoinApplicable.isHidden = true

        if isCoinApplicable {
            lblCoinApplicable.isHidden = false
            imgViewCoinApplicable.isHidden = false
        }
        
        lblTotalWinningsTitle.text = "Total Winnings".localized()
        lblTotalWinning.text = "" +  AppHelper.makeCommaSeparatedDigitsWithString(digites: leagueDetails.winAmount ?? "")
        lblInfinityIcon.isHidden = true
        
        if (leagueDetails.timeBasedBonusArray.count != 0) && (leagueDetails.bonusApplicable == "2"){
                    
            let serverUnixTimestamp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
            let dateUnixTimestamp = Double(matchDetails.startDateTimestemp ?? "0") ?? 0
            var matchClosingSeconds = 0.0
            
            if dateUnixTimestamp > serverUnixTimestamp {
                matchClosingSeconds = dateUnixTimestamp - serverUnixTimestamp
            }

            for bonusDetails in leagueDetails.timeBasedBonusArray{
                let defaultBouns = bonusDetails["tbm"].string ?? ""
                if(defaultBouns == "default")   {
                    continue
                }

                let remaingTimeStr = bonusDetails["tbm"].stringValue
                let remainingTime = (Double(remaingTimeStr) ?? 0)*60
                if matchClosingSeconds > remainingTime {
                    let bounsPercentage = bonusDetails["bp"].stringValue
                    appliedBonusPercentage = bounsPercentage
                    bonusMessage = bounsPercentage + "bonusMessage".localized()
                    if (bounsPercentage != "0") && (bounsPercentage != "default")  {
                        lblFirstTimer.text = AppHelper.convertRemaingTimeIntoSeconds(totalSeconds:  (matchClosingSeconds - remainingTime))
                        lblSecondTimer.text = lblFirstTimer.text
                        lblThirdTimer.text = lblFirstTimer.text
                    }

                    break;
                }
            }
        }
        
        if leagueDetails.isJackpot == "1" {
            imgViewJackpotLeague.isHidden = false
            lblJackpotLeague.isHidden = false
        }
                
        if leagueDetails.isInfinity == "1"{
            lblInfinityIcon.isHidden = false
            lblMaxTeam.text = "/∞"
            progressView.isHidden = false
            progressBgView.isHidden = false
            progressView.backgroundColor = UIColor(red: 255.0/255, green: 115.0/255, blue: 80.0/255, alpha: 1)

            let constantValue = 0.7 * (UIScreen.main.bounds.size.width - 70)
            progressViewWidthConstraint.constant = constantValue;
            progressView.layoutIfNeeded()
            lblJoinedLeagueCount.text = leagueDetails.totalJoined
        }
        else{
            progressView.isHidden = false
            progressBgView.isHidden = false
            
            guard let totalJoined = NumberFormatter().number(from: leagueDetails.totalJoined) else {
                return
            }
            
            guard let maxPlayers = NumberFormatter().number(from: leagueDetails.maxPlayers) else {
                return
            }
            
            lblMaxTeam.text = "/" + leagueDetails.maxPlayers
            lblJoinedLeagueCount.text = leagueDetails.totalJoined
            let leagueFilledPercentage = (CGFloat(truncating: totalJoined) * CGFloat(100) / CGFloat(truncating: maxPlayers))
            if leagueFilledPercentage <= 20{
                progressView.backgroundColor = UIColor(red: 255.0/255, green: 205.0/255, blue: 127.0/255, alpha: 1)
            }
            else if leagueFilledPercentage <= 40{
                progressView.backgroundColor = UIColor(red: 255.0/255, green: 168.0/255, blue: 106/255, alpha: 1)
            }
            else if leagueFilledPercentage <= 60{
                progressView.backgroundColor = UIColor(red: 255.0/255, green: 147.0/255, blue: 119.0/255, alpha: 1)
            }
            else if leagueFilledPercentage <= 80{
                progressView.backgroundColor = UIColor(red: 255.0/255, green: 115.0/255, blue: 80.0/255, alpha: 1)
            }
            else {
                progressView.backgroundColor = UIColor(red: 241.0/255, green: 94.0/255, blue: 56.0/255, alpha: 1)
            }
            
            let constantValue = (CGFloat(truncating: totalJoined)/CGFloat(truncating: maxPlayers)) * (UIScreen.main.bounds.size.width - 70)
            progressViewWidthConstraint.constant = constantValue;
            progressView.layoutIfNeeded()
        }
        
        showLeagueType(leagueDetails: leagueDetails, bonusMessage: bonusMessage, appliedBonusPercentage: appliedBonusPercentage)
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" != lang[0]{
                 if !lblFirstTimer.isHidden && (lblFirstTimer.text?.count ?? 0 > 0){
                     lblFirstMatchTypeMessage.text = lblFirstMatchTypeMessage.text! + " for "
                 }
                 
                 if !lblSecondTimer.isHidden && (lblSecondTimer.text?.count ?? 0 > 0){
                     lblSecondMatchTypeMessage.text = lblSecondMatchTypeMessage.text! + " for "
                 }

                 if !lblThirdTimer.isHidden && (lblThirdTimer.text?.count ?? 0 > 0){
                     lblThirdMatchTypeMessage.text = lblThirdMatchTypeMessage.text! + " for "
                 }
            }
        }

        
        infoButton.isHidden = true
        if !lblFirstTimer.isHidden && (lblFirstTimer.text?.count ?? 0 > 0){
            infoButton.isHidden = false
        }
        
        if !lblSecondTimer.isHidden && (lblSecondTimer.text?.count ?? 0 > 0){
            infoButton.isHidden = false
        }

        if !lblThirdTimer.isHidden && (lblThirdTimer.text?.count ?? 0 > 0){
            infoButton.isHidden = false
        }
    }
    
    
    func showLeagueType(leagueDetails: LeagueDetails, bonusMessage: String, appliedBonusPercentage: String)  {
        
        var visibleLableCount = 0
        lblFirstTimer.isHidden = true
        lblSecondTimer.isHidden = true
        lblThirdTimer.isHidden = true
        
        if (leagueDetails.bonusApplicable == "2") && (leagueDetails.teamType == "1") && (leagueDetails.confirmedLeague == "2"){
            lblThirdMatchType.text = "B"
            lblSecondMatchType.text = "M"
            lblFirstMatchType.text = "C"
            lblFirstMatchType.isHidden = false
            lblSecondMatchType.isHidden = false
            lblThirdMatchType.isHidden = false
            lblThirdTimer.isHidden = false
            lblThirdMatchTypeMessage.text = bonusMessage
            lblSecondMatchTypeMessage.text = multipleTeamMessage
            lblFirstMatchTypeMessage.text = confirmLeagueMessage
            visibleLableCount = 3
            
            lblThirdMatchType.textColor = bonusLeageTextColor
            lblSecondMatchType.textColor = multipleTeamLeageTextColor
            lblFirstMatchType.textColor = confirmLeageTextColor

            lblThirdMatchType.backgroundColor = bonusLeageColor
            lblSecondMatchType.backgroundColor = multipleTeamLeageColor
            lblFirstMatchType.backgroundColor = confirmLeageColor
            
            if (appliedBonusPercentage != "") && (appliedBonusPercentage != "0") {
//                var progressTopConstraint:CGFloat = 130
                
//                if leagueDetails.isJackpot == "1" {
//                    progressTopConstraint = 147;
//                }
//                else{
//                    progressTopConstraint = 120;
//                }
                
//                if isNeedtoShowBBCoins {
//                    progressTopConstraint = progressTopConstraint + 40
//                }
//
//                progressViewTopConstraint.constant = progressTopConstraint;

            }
            else{
                lblThirdMatchType.isHidden = true
                lblThirdMatchTypeMessage.isHidden = true
//                var progressTopConstraint:CGFloat = 100

//                if leagueDetails.isJackpot == "1" {
//                    progressTopConstraint = 127;
//                }
//                else{
//                    progressTopConstraint = 100;
//                }
                
//                if isNeedtoShowBBCoins {
//                    progressTopConstraint = progressTopConstraint + 40
//                }
//
//                progressViewTopConstraint.constant = progressTopConstraint;
            }
        }
        else if (leagueDetails.bonusApplicable == "2") && (leagueDetails.teamType == "1"){
           
            lblSecondMatchType.text = "B"
            lblFirstMatchType.text = "M"
            lblSecondTimer.isHidden = false
            lblSecondMatchType.textColor = bonusLeageTextColor
            lblFirstMatchType.textColor = multipleTeamLeageTextColor
            
            lblSecondMatchTypeMessage.text = bonusMessage
            lblFirstMatchTypeMessage.text = multipleTeamMessage
            visibleLableCount = 2
            lblThirdMatchType.isHidden = true
            lblThirdMatchTypeMessage.isHidden = true
            
            lblSecondMatchType.backgroundColor = bonusLeageColor
            lblFirstMatchType.backgroundColor = multipleTeamLeageColor
            
            if (appliedBonusPercentage != "") && (appliedBonusPercentage != "0") {
//                var progressTopConstraint:CGFloat = 95

//                if leagueDetails.isJackpot == "1" {
//                    progressTopConstraint = 122;
//                }
//                else{
//                    progressTopConstraint = 95;
//                }
                
//                if isNeedtoShowBBCoins {
//                    progressTopConstraint = progressTopConstraint + 40
//                }
//
//                progressViewTopConstraint.constant = progressTopConstraint;

            }
            else{
                
                lblSecondMatchType.isHidden = true
                lblSecondMatchTypeMessage.isHidden = true

//                var progressTopConstraint:CGFloat = 75

//                if leagueDetails.isJackpot == "1" {
//                    progressTopConstraint = 102;
//                }
//                else{
//                    progressTopConstraint = 75;
//                }
//
//                if isNeedtoShowBBCoins {
//                    progressTopConstraint = progressTopConstraint + 40
//                }
//
//                progressViewTopConstraint.constant = progressTopConstraint;

            }
        }
        else if (leagueDetails.teamType == "1") && (leagueDetails.confirmedLeague == "2"){
            lblSecondMatchType.text = "M"
            lblFirstMatchType.text = "C"
            
            lblSecondMatchType.textColor = multipleTeamLeageTextColor
            lblFirstMatchType.textColor = confirmLeageTextColor
            
            visibleLableCount = 2
            lblSecondMatchTypeMessage.text = multipleTeamMessage
            lblFirstMatchTypeMessage.text = confirmLeagueMessage
            lblThirdMatchType.isHidden = true
            lblThirdMatchTypeMessage.isHidden = true

            lblSecondMatchType.backgroundColor = multipleTeamLeageColor
            lblFirstMatchType.backgroundColor = confirmLeageColor
            
//            var progressTopConstraint:CGFloat = 95

//            if leagueDetails.isJackpot == "1" {
//                progressTopConstraint = 122;
//            }
//            else{
//                progressTopConstraint = 95;
//            }
            
//            if isNeedtoShowBBCoins {
//                progressTopConstraint = progressTopConstraint + 40
//            }
//
//            progressViewTopConstraint.constant = progressTopConstraint;

        }
        else if (leagueDetails.bonusApplicable == "2") && (leagueDetails.confirmedLeague == "2"){
            
            lblSecondMatchType.textColor = bonusLeageTextColor
            lblFirstMatchType.textColor = confirmLeageTextColor
            lblSecondTimer.isHidden = false
            lblSecondMatchType.text = "B"
            lblFirstMatchType.text = "C"
            visibleLableCount = 2
            lblSecondMatchTypeMessage.text = bonusMessage
            lblFirstMatchTypeMessage.text = confirmLeagueMessage
            lblThirdMatchType.isHidden = true
            lblThirdMatchTypeMessage.isHidden = true

            lblSecondMatchType.backgroundColor = bonusLeageColor
            lblFirstMatchType.backgroundColor = confirmLeageColor
            
            if (appliedBonusPercentage != "") && (appliedBonusPercentage != "0") {
                
                var progressTopConstraint:CGFloat = 95

                if leagueDetails.isJackpot == "1" {
                    progressTopConstraint = 122;
                }
                else{
                    progressTopConstraint = 95;
                }
                
                if isNeedtoShowBBCoins {
                    progressTopConstraint = progressTopConstraint + 40
                }

                progressViewTopConstraint.constant = progressTopConstraint;

            }
            else{
                
                lblSecondMatchType.isHidden = true
                lblSecondMatchTypeMessage.isHidden = true
                
                var progressTopConstraint:CGFloat = 75

                if leagueDetails.isJackpot == "1" {
                    progressTopConstraint = 102;
                }
                else{
                    progressTopConstraint = 75;
                }
                
                if isNeedtoShowBBCoins {
                    progressTopConstraint = progressTopConstraint + 40
                }

                progressViewTopConstraint.constant = progressTopConstraint;

            }
        }
        else if (leagueDetails.bonusApplicable == "2"){
            lblFirstMatchType.text = "B"
            visibleLableCount = 1
            lblFirstMatchType.textColor = bonusLeageTextColor
            lblFirstTimer.isHidden = false

            lblFirstMatchTypeMessage.text = bonusMessage
            lblSecondMatchType.isHidden = true
            lblSecondMatchTypeMessage.isHidden = true
            lblThirdMatchType.isHidden = true
            lblThirdMatchTypeMessage.isHidden = true

            lblFirstMatchType.backgroundColor = bonusLeageColor
            
            if (appliedBonusPercentage != "") && (appliedBonusPercentage != "0") {
                var progressTopConstraint:CGFloat = 72

                if leagueDetails.isJackpot == "1" {
                    progressTopConstraint = 97;
                }
                else{
                    progressTopConstraint = 70;
                }
                
                if isNeedtoShowBBCoins {
                    progressTopConstraint = progressTopConstraint + 40
                }

                progressViewTopConstraint.constant = progressTopConstraint;

            }
            else{
                lblFirstMatchType.isHidden = true
                lblFirstMatchTypeMessage.isHidden = true
                
                var progressTopConstraint:CGFloat = 53

                if leagueDetails.isJackpot == "1" {
                    progressTopConstraint = 77;
                }
                else{
                    progressTopConstraint = 50;
                }
                
                if isNeedtoShowBBCoins {
                    progressTopConstraint = progressTopConstraint + 40
                }

                progressViewTopConstraint.constant = progressTopConstraint;

            }
        }
        else if (leagueDetails.teamType == "1"){
            lblFirstMatchType.text = "M"
            visibleLableCount = 1
            lblFirstMatchType.textColor = multipleTeamLeageTextColor

            lblFirstMatchTypeMessage.text = multipleTeamMessage
            lblSecondMatchType.isHidden = true
            lblSecondMatchTypeMessage.isHidden = true
            lblThirdMatchType.isHidden = true
            lblThirdMatchTypeMessage.isHidden = true
            lblFirstMatchType.backgroundColor = multipleTeamLeageColor

            var progressTopConstraint:CGFloat = 70

            if leagueDetails.isJackpot == "1" {
                progressTopConstraint = 97;
            }
            else{
                progressTopConstraint = 70;
            }
            
            if isNeedtoShowBBCoins {
                progressTopConstraint = progressTopConstraint + 40
            }

            progressViewTopConstraint.constant = progressTopConstraint;

        }
        else if (leagueDetails.confirmedLeague == "2"){
            lblFirstMatchType.text = "C"
            lblFirstMatchType.textColor = confirmLeageTextColor

            lblFirstMatchType.isHidden = false
            lblFirstMatchType.backgroundColor = confirmLeageColor
            visibleLableCount = 1
            lblFirstMatchTypeMessage.text = confirmLeagueMessage
            lblSecondMatchType.isHidden = true
            lblSecondMatchTypeMessage.isHidden = true
            lblThirdMatchType.isHidden = true
            lblThirdMatchTypeMessage.isHidden = true
              
            var progressTopConstraint:CGFloat = 70

            if leagueDetails.isJackpot == "1" {
                progressTopConstraint = 97;
            }
            else{
                progressTopConstraint = 70;
            }
            
            if isNeedtoShowBBCoins {
                progressTopConstraint = progressTopConstraint + 40
            }

            progressViewTopConstraint.constant = progressTopConstraint;
        }
        
        if (leagueDetails.leagueType == "2"){
            if (leagueDetails.confirmedLeague == "2"){
                lblFirstMatchType.text = "C"
                lblFirstMatchType.textColor = confirmLeageTextColor
                lblFirstMatchType.isHidden = false
                lblFirstMatchType.backgroundColor = confirmLeageColor
                lblSecondMatchType.isHidden = true
                lblThirdMatchType.isHidden = true
                visibleLableCount = 1
                lblFirstMatchTypeMessage.text = confirmLeagueMessage
                lblSecondMatchType.isHidden = true
                lblSecondMatchTypeMessage.isHidden = true
                lblThirdMatchType.isHidden = true
                lblThirdMatchTypeMessage.isHidden = true

                var progressTopConstraint:CGFloat = 70

                if leagueDetails.isJackpot == "1" {
                    progressTopConstraint = 97;
                }
                else{
                    progressTopConstraint = 70;
                }
                
                if isNeedtoShowBBCoins {
                    progressTopConstraint = progressTopConstraint + 40
                }

                progressViewTopConstraint.constant = progressTopConstraint;

            }
            else{
                lblFirstMatchType.isHidden = true
                lblSecondMatchType.isHidden = true
                lblThirdMatchType.isHidden = true
                visibleLableCount = 0
                lblFirstMatchTypeMessage.isHidden = true
                lblSecondMatchTypeMessage.isHidden = true
                lblThirdMatchTypeMessage.isHidden = true
                var progressTopConstraint:CGFloat = 70

                if leagueDetails.isJackpot == "1" {
                    progressTopConstraint = 97;
                }
                else{
                    progressTopConstraint = 70;
                }
                
                if isNeedtoShowBBCoins {
                    progressTopConstraint = progressTopConstraint + 40
                }

                progressViewTopConstraint.constant = progressTopConstraint;

            }
        }
        
        if (leagueDetails.leagueType == "3"){
            lblFirstMatchType.isHidden = true
            lblSecondMatchType.isHidden = true
            lblThirdMatchType.isHidden = true
            visibleLableCount = 0
            lblFirstMatchTypeMessage.isHidden = true
            lblSecondMatchTypeMessage.isHidden = true
            lblThirdMatchTypeMessage.isHidden = true

            var progressTopConstraint:CGFloat = 20

            if leagueDetails.isJackpot == "1" {
                progressTopConstraint = 47;
            }
            else{
                progressTopConstraint = 20;
            }
            
            if isNeedtoShowBBCoins {
                progressTopConstraint = progressTopConstraint + 40
            }

            progressViewTopConstraint.constant = progressTopConstraint;

        }
        
        if visibleLableCount == 0{
            lblFirstMatchType.isHidden = true
            lblSecondMatchType.isHidden = true
            lblThirdMatchType.isHidden = true
            lblFirstMatchTypeMessage.isHidden = true
            lblSecondMatchTypeMessage.isHidden = true
            lblThirdMatchTypeMessage.isHidden = true

            lblFirstTimer.isHidden = true
            lblSecondTimer.isHidden = true
            lblThirdTimer.isHidden = true
            var progressTopConstraint:CGFloat = 35

            if leagueDetails.isJackpot == "1" {
                progressTopConstraint = 50;
            }
            else{
                progressTopConstraint = 35;
            }
            
            if isNeedtoShowBBCoins {
                progressTopConstraint = progressTopConstraint + 44
            }

            progressViewTopConstraint.constant = progressTopConstraint;

        }
        else if visibleLableCount == 1{
            var progressTopConstraint:CGFloat = 70

            if leagueDetails.isJackpot == "1" {
                progressTopConstraint = 85;
            }
            else{
                progressTopConstraint = 70;
            }
            if isNeedtoShowBBCoins {
                progressTopConstraint = progressTopConstraint + 40
            }

            progressViewTopConstraint.constant = progressTopConstraint;

        }
        else if visibleLableCount == 2{
            var progressTopConstraint:CGFloat = 100

            if leagueDetails.isJackpot == "1" {
                progressTopConstraint = 115;
            }
            else{
                progressTopConstraint = 100;
            }
            
            if isNeedtoShowBBCoins {
                progressTopConstraint = progressTopConstraint + 40
            }

            progressViewTopConstraint.constant = progressTopConstraint;
        }
        else if visibleLableCount == 3{
            var progressTopConstraint:CGFloat = 120

            if leagueDetails.isJackpot == "1" {
                progressTopConstraint = 135;
            }
            else{
                progressTopConstraint = 120;
            }
            
            if isNeedtoShowBBCoins {
                progressTopConstraint = progressTopConstraint + 40
            }

            progressViewTopConstraint.constant = progressTopConstraint;

        }
        innerView.layoutIfNeeded()
    }

    @IBAction func infoButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: kAlert, message: "BonusTimerMsg".localized(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action -> Void in
        }))

        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
