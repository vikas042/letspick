//
//  GameSelectionCollectionViewCell.swift
//  apiCallingExample
//
//  Created by Ankit Kejriwal on 27/05/19.
//  Copyright © 2019 madstech. All rights reserved.
//

import UIKit

class GameSelectionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblGameType: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var gameImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func display(){
        
    }
}
