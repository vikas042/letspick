//
//  YoutubeVideoCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
//import YouTubePlayer
import SwiftyJSON
import MapleBacon
import youtube_ios_player_helper

class YoutubeVideoCollectionViewCell: UICollectionViewCell, YTPlayerViewDelegate   {

    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var playerView: YTPlayerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(details: JSON) {
        playButton.isHidden = true
        let mediaURL = details["media_url"].string ?? ""
        let mediaType = details["media_type"].string ?? ""
        if mediaType == "2" {
            imgView.isHidden = false
            playerView.isHidden = true
            if let imgURL = URL(string: UserDetails.sharedInstance.promotionImageUrl + mediaURL){
                imgView.setImage(with: imgURL, placeholder: UIImage(named: "ImagePlaceholder"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                     self?.imgView.image = image
                    }
                    else{
                        self?.imgView.image = UIImage(named: "ImagePlaceholder")
                    }
                })
            }
            else{
                self.imgView.image = UIImage(named: "ImagePlaceholder")
            }
        }
        else{
            
            playerView.isHidden = false
            imgView.isHidden = true
            let playerVars = ["playsinline" :1, "autoplay" : 1]


            if mediaURL.contains("http") {
                let videoID = self.getYoutubeId(youtubeUrl: mediaURL) ?? ""
                playerView.load(withVideoId: videoID, playerVars: playerVars)
            }
            else{
                playerView.load(withVideoId: mediaURL, playerVars: playerVars)
            }
            playerView.delegate = self
        }
    }
        
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.playVideo()
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch state {
        
        case .unstarted:
            break;
        
        case .playing:
            break;
            
        case .paused:
            break;
         
        case .ended:
            playButton.isHidden = false
            break;

        default:
            break
        }
    }
    
    @IBAction func replayButtonTapped(_ sender: Any) {
        playButton.isHidden = true
        playerView.playVideo()
    }
}

