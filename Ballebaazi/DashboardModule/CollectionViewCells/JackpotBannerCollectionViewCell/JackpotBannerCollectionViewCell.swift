//
//  JackpotBannerCollectionViewCell.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class JackpotBannerCollectionViewCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    @IBOutlet weak var collectionView: UICollectionView!
    lazy var bannerArray = Array<String>()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(dataArray: Array<String>) {
        bannerArray = dataArray
        collectionView.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell")
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
                 
        if bannerArray.count == 1 {
            return CGSize(width: UIScreen.main.bounds.width - 10, height: 90)
        }
        return CGSize(width: 300, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as? BannerCollectionViewCell
        let bannerStr = bannerArray[indexPath.row]
        cell?.configJackpotData(imageURL: bannerStr)
        return cell!;
    }

}
