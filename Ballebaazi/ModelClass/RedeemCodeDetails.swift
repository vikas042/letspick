//
//  RedeemCodeDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 14/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class RedeemCodeDetails: NSObject {

    lazy var redeemCode = ""
    lazy var winningAmount = ""
    lazy var tickets = ""
    lazy var bonusAmount = ""
    lazy var unusedAmount = ""
    lazy var ticketCounts = 0
    
    lazy var adminMessage = ""

    
    class func parseRedeemCodeDetails(details: JSON) -> RedeemCodeDetails {
        let redeemCodeDetails = RedeemCodeDetails()
        
        redeemCodeDetails.winningAmount = details["winning_amount"].stringValue
        redeemCodeDetails.tickets = details["tickets"].string ?? ""
        redeemCodeDetails.bonusAmount = details["bonus_amount"].stringValue
        redeemCodeDetails.unusedAmount = details["unused_amount"].stringValue
        
        var message = details["message"].string ?? ""

        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                message = details["message_in_hindi"].string ?? ""
            }
        }        

        redeemCodeDetails.adminMessage = message
        if redeemCodeDetails.tickets.count > 0 {
                    
            let ticketsIDArray = redeemCodeDetails.tickets.components(separatedBy: ",")
            redeemCodeDetails.ticketCounts = ticketsIDArray.count
        }
        return redeemCodeDetails
    }
    
}
