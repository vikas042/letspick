//
//  PlayerInfoDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/10/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class PlayerInfoDetails: NSObject {

    
    var teamShortName = ""
    var playerKey = ""
    var playerCredits = ""
    var playerPlayingRole = ""
    var playerName = ""
    var seasonalBattingPoints = ""
    var seasonalBowlingPoints = ""
    var seasonalClassicPoints = ""
    
    class func parserPlayerInformation(playerDetails: JSON) -> PlayerInfoDetails {
        
        let playerInfo = PlayerInfoDetails()
        
        playerInfo.teamShortName = playerDetails["team_short_name"].string ?? ""
        playerInfo.playerKey = playerDetails["player_key"].string ?? ""
        playerInfo.playerCredits = playerDetails["player_credits"].string ?? ""
        playerInfo.playerPlayingRole = playerDetails["player_playing_role"].string ?? ""
        playerInfo.playerName = playerDetails["player_name"].string ?? ""
        playerInfo.seasonalBattingPoints = playerDetails["seasonal_batting_points"].string ?? ""
        playerInfo.seasonalBowlingPoints = playerDetails["seasonal_bowling_points"].string ?? ""
        playerInfo.seasonalClassicPoints = playerDetails["seasonal_classic_points"].string ?? ""
        return playerInfo;
    }
}
