//
//  HowToPlayDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class HowToPlayDetails: NSObject {

    lazy var title = ""
    lazy var message = ""
    lazy var imgURL = ""
    lazy var icon = ""

    
    lazy var mediaArray = Array<JSON>()
    
    class func gethowToPlayArray(dataArray: Array<JSON>) -> Array<HowToPlayDetails> {
        
        var howToPlayDetailsArray = Array<HowToPlayDetails>()

        for details in dataArray {
            let howToPlayDetails = HowToPlayDetails()
            
            howToPlayDetails.title = details["title"].string ?? ""
            howToPlayDetails.message = details["description"].string ?? ""
            howToPlayDetails.imgURL = details["image"].string ?? ""
            howToPlayDetails.icon = details["icon"].string ?? ""
            
            if let video = details["video"].string{
                // 1 for youtube videos
                if video.count > 0{
                    let dict: JSON = ["media_type": "1", "media_url": video]
                    howToPlayDetails.mediaArray.append(dict)
                }
            }
            
            if let image = details["image"].string{
                // 2 for images
                if image.count > 0{
                    let dict: JSON = ["media_type": "2", "media_url": image]
                    howToPlayDetails.mediaArray.append(dict)
                }
            }
            
            howToPlayDetailsArray.append(howToPlayDetails)

        }
        
        return howToPlayDetailsArray
    }
}
