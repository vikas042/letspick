//
//  PanDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/05/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class PanDetails: NSObject {

    lazy var panName = ""
    lazy var panNumber = ""
    lazy var dob = ""
    lazy var state = ""
    
    class func getPanDetails(details: [String: JSON])-> PanDetails?{
        
        let panDetails = PanDetails()
        
        if let panID = details["pan_id"]?.string {
            if panID.count == 0{
                return nil
            }
            
            panDetails.panName = details["pan_name"]?.string ?? ""
            panDetails.panNumber = details["pan_number"]?.string ?? ""
            panDetails.dob = details["dob"]?.string ?? ""
            panDetails.state = details["state"]?.string ?? ""
            return panDetails
        }
        else{
            return nil
        }
    }
    
}
