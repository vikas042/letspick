//
//  PlayerMatchInfo.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/10/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class PlayerMatchInfo: NSObject {

    var matchDate = ""
    var matchShortName = ""
    var playerScoreBatting = ""
    var playerScoreBowling = ""
    var playerClassicScore = ""    
    var selectedByClassic = ""
    var selectedBybowling = ""
    var selectedByBatting = ""
    
    
    class func getPlayerInfoList(dataArray: [JSON]) -> [PlayerMatchInfo] {
        
        var infoArray = [PlayerMatchInfo]()
        
        for details in dataArray{
            let infoDetails = PlayerMatchInfo()
            
            infoDetails.matchDate = details["start_date_india"].string ?? ""
            infoDetails.matchShortName = details["match_short_name"].string ?? ""
            infoDetails.playerScoreBatting = details["player_score_batting"].string ?? ""
            infoDetails.playerScoreBowling = details["player_score_bowling"].string ?? ""
            infoDetails.playerClassicScore = details["player_score"].string ?? ""
            infoDetails.selectedByClassic = details["selected_by_classic"].string ?? ""
            infoDetails.selectedBybowling = details["selected_by_bowling"].string ?? ""
            infoDetails.selectedByBatting = details["selected_by_batting"].string ?? ""
            
            infoArray.append(infoDetails)
        }
        
        return infoArray
    }
}
