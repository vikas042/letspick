//
//  PromotionCodeDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/12/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class PromotionCodeDetails: NSObject {

    lazy var title = ""
    lazy var subTitle = ""
    lazy var endDate = ""
    lazy var promocode = ""
//    lazy var promoType = ""

    class func getPromoCodesList(dataArray: [JSON]) -> [PromotionCodeDetails] {
        var promoArray = Array<PromotionCodeDetails>()
        
        for details in dataArray {
            let promoDetails = PromotionCodeDetails()
            promoDetails.title = details["heading"].string ?? ""
            promoDetails.subTitle = details["sub_heading"].string ?? ""
            promoDetails.promocode = details["promocode"].string ?? "N/A"
            if let expiryDate = details["end_date"].string{
                promoDetails.endDate = AppHelper.getDepositFormattedDate(dateString: expiryDate)
            }
            else{
                promoDetails.endDate = "N/A"
            }
            
            promoArray.append(promoDetails)
        }
        return promoArray
    }
}
