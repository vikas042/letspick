
//
//  UserTeamDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/18/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class UserTeamDetails: NSObject {

    var fantasyType: String?
    var teamNumber: String?
    var totalBatsmanCount: String?
    var totalBowlerCount: String?
    var totalAllrounderCount: String?
    var totalKeeperCount: String?
    var totalTeamScore: String?

    lazy var totalRaiderCount = ""
    lazy var totalDefenderCount = ""
    lazy var totalGoalKeeperCount = ""
    lazy var totalMidFielderCount = ""
    lazy var totalForwardCount = ""
    
    lazy var totalOutFielderCount = ""
    lazy var totalInFielderCount = ""
    lazy var totalPitcherCount = ""
    lazy var totalCatcherCount = ""
    
    lazy var totalShootingGuardCount = ""
    lazy var totalPointGuardCount = ""
    lazy var totalSmallForwardCount = ""
    lazy var totalCenterCount = ""
    lazy var totalPowerForwardCount = ""

    // 1=> Only wizard, 2=> Wizard and Captain, 3=> Wizard and Vice Captain
    lazy var wizardType = ""
    lazy var wizardPlayerName = ""

    lazy var wizardPlayerKey = ""
    lazy var captionPlayerKey = ""
    lazy var viceCaptionPlayerKey = ""

    var captionName: String?
    var viceCaptionName: String?
    
    var playersArray: Array<PlayerDetails>?

    class func getUserTeamsArray(responseArray: Array<JSON>, matchDetails: MatchDetails) -> Array<UserTeamDetails> {
        
        var teamDetailsArray = Array<UserTeamDetails>()
        
        for details in responseArray {
            
            let teamDetails = UserTeamDetails()
            
            teamDetails.fantasyType = details["fantasy_type"].string
            teamDetails.teamNumber = details["team_number"].string
            if let palyerDict = details["players"].dictionary{
                let playersArray = Array(palyerDict.values)
                teamDetails.playersArray = PlayerDetails.getPlayerDetailsArray(responseArray: playersArray, matchDetails: matchDetails)
                if teamDetails.playersArray?.count ?? 0 > 0 {
                    if matchDetails.playersGender == "F" {
                        PlayerDetails.changeFemalePlayerPlaceholder(playerArray: teamDetails.playersArray!)
                    }
                }
            }
            
            
            if let wizardArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.isWizard
            }){
                if wizardArray.count > 0{
                    let playerDetails = wizardArray[0]
                    teamDetails.wizardPlayerName = playerDetails.playerName ?? ""
                    teamDetails.wizardPlayerKey = playerDetails.playerKey ?? ""
                }
            }
            
            if let captionArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.isCaption
            }){
                if captionArray.count > 0{
                    let playerDetails = captionArray[0]
                    teamDetails.captionName = playerDetails.playerName
                    teamDetails.captionPlayerKey = playerDetails.playerKey ?? ""
                    var playerRole = playerDetails.seasonalRole
                    if playerRole.count == 0  {
                        playerRole = playerDetails.playerPlayingRole!
                    }
                }
            }
            
            if let viceCaptionArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.isViceCaption
            }){
                if viceCaptionArray.count > 0{
                    let playerDetails = viceCaptionArray[0]
                    teamDetails.viceCaptionName = playerDetails.playerName
                    teamDetails.viceCaptionPlayerKey = playerDetails.playerKey ?? ""
                    var playerRole = playerDetails.seasonalRole
                    if playerRole.count == 0  {
                        playerRole = playerDetails.playerPlayingRole!
                    }
                }
            }
            
            if teamDetails.wizardPlayerKey == teamDetails.captionPlayerKey {
                teamDetails.wizardType = "2"
            }
            else if teamDetails.wizardPlayerKey == teamDetails.viceCaptionPlayerKey {
                teamDetails.wizardType = "3"
            }
            else if teamDetails.wizardPlayerKey != "" {
                teamDetails.wizardType = "1"
            }
            
            if let batsmansArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Batsman.rawValue
            }){
                teamDetails.totalBatsmanCount = String(batsmansArray.count)
            }
            
            if let bowlerArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Bowler.rawValue
            }){
                teamDetails.totalBowlerCount = String(bowlerArray.count)
            }
            
            if let allrounderArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.AllRounder.rawValue
            }){
                teamDetails.totalAllrounderCount = String(allrounderArray.count)
            }
            
            if let keeperArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.WicketKeeper.rawValue
            }){
                teamDetails.totalKeeperCount = String(keeperArray.count)
            }
            
            if let defenderArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Defender.rawValue
            }){
                teamDetails.totalDefenderCount = String(defenderArray.count)
            }

            if let raiderArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Raider.rawValue
            }){
                teamDetails.totalRaiderCount = String(raiderArray.count)
            }

            if let gaolKeeprArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.GoalKeeper.rawValue
            }){
                teamDetails.totalGoalKeeperCount = String(gaolKeeprArray.count)
            }
            
            if let midFielderArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.MidFielder.rawValue
            }){
                teamDetails.totalMidFielderCount = String(midFielderArray.count)
            }
            
            if let forwardArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Sticker.rawValue
            }){
                teamDetails.totalForwardCount = String(forwardArray.count)
            }
            
            if let outfieldersArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Outfielders.rawValue
            }){
                teamDetails.totalOutFielderCount = String(outfieldersArray.count)
            }

            if let infielderArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Infielder.rawValue
            }){
                teamDetails.totalInFielderCount = String(infielderArray.count)
            }

            if let pitcherArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Pitcher.rawValue
            }){
                teamDetails.totalPitcherCount = String(pitcherArray.count)
            }

            if let catcherArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Catcher.rawValue
            }){
                teamDetails.totalCatcherCount = String(catcherArray.count)
            }

            if let shootingGuardArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.ShootingGuard.rawValue
            }){
                teamDetails.totalShootingGuardCount = String(shootingGuardArray.count)
            }

            if let pointGuardArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.PointGuard.rawValue
            }){
                teamDetails.totalPointGuardCount = String(pointGuardArray.count)
            }

            if let smallForwardArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.SmallForward.rawValue
            }){
                teamDetails.totalSmallForwardCount = String(smallForwardArray.count)
            }

            if let centerArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.Center.rawValue
            }){
                teamDetails.totalCenterCount = String(centerArray.count)
            }
            
            if let powerForwardArray = teamDetails.playersArray?.filter({ (playerDetails) -> Bool in
                playerDetails.seasonalRole == PlayerType.PowerForward.rawValue
            }){
                teamDetails.totalPowerForwardCount = String(powerForwardArray.count)
            }
            
            var totalScore = teamDetails.playersArray?.reduce(0, { creditSum, nextPlayerDetails in
                creditSum + Float(nextPlayerDetails.playerScore)!
            })
            if totalScore == nil{
                totalScore = 0.0
            }
            teamDetails.totalTeamScore = String(totalScore!)
            teamDetailsArray.append(teamDetails)
        }
        
        return teamDetailsArray
    }
    
}
