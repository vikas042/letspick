//
//  AadhaarDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/05/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class AadhaarDetails: NSObject {

    lazy var aadhaarName = ""
    lazy var state = ""
    lazy var dob = ""
    lazy var aadhaarNumber = ""
    lazy var image = ""
    lazy var image1 = ""
    lazy var address1 = ""
    lazy var address2 = ""
    lazy var city = ""
    lazy var zipcode = ""
    
    class func getAadharDetails(details: [String: JSON])-> AadhaarDetails?{
        
        let bankDetails = AadhaarDetails()
        
        if let aadhaarID = details["aadhaar_id"]?.string {
            if aadhaarID.count == 0{
                return nil
            }
            
            bankDetails.aadhaarName = details["aadhaar_name"]?.string ?? ""
            bankDetails.state = details["state"]?.string ?? ""
            bankDetails.dob = details["dob"]?.string ?? ""
            bankDetails.aadhaarNumber = details["aadhaar_number"]?.string ?? ""
            bankDetails.address1 = details["address1"]?.string ?? ""
            bankDetails.address2 = details["address2"]?.string ?? ""
            bankDetails.city = details["city"]?.string ?? ""
            bankDetails.zipcode = details["zipcode"]?.string ?? ""

            return bankDetails
        }
        else{
            return nil
        }
    }
}
