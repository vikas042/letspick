
import UIKit
import SwiftyJSON

class JoinedLeagueDetails: NSObject {

    var leagueId: String?
    var leagueName: String?
    var winAmount: String?
    lazy var joiningAmount = ""
    var totalJoined: String?
    var fantasyType: String?
    var matchKey: String?
    var maxPlayers: String?
    var teamRank: String?
    var nextPageCount: String?
    lazy var leagueType = ""
    lazy var confirmedLeague = ""
    lazy var isInfinity = ""
    lazy var totalWinnersPercent = ""
    lazy var totalWinners = "0"
    lazy var isPrivate = ""

    lazy var teamType = ""
    
    var timeBasedBonusArray = Array<JSON>()
    var bonusApplicable: String?
    lazy var bounsPercentage = ""


    
    lazy var leagueWinnerType = ""

    lazy var winPerUser = ""
    lazy var leagueCode = ""
    lazy var userTeamsPdf = ""
    var contestPlayers: Array<JoinedLeagueUserList>?
    var myTeamPlayers: Array<JoinedLeagueUserList>?
    lazy var bannerImages = Array<String>()

    lazy var isPrivateLeague = false
    lazy var isPrivateLeagueJoined = false
    lazy var totalJoinedSelf = 0

    
    class func getJoinedLeageList(result: [String
        : JSON]) -> Array<JoinedLeagueDetails> {
        
        var joinedLagueDetailsArray = Array<JoinedLeagueDetails>()
        
        if let response = result["response"]{
            let statusCode = result["status"]?.string
            
            if (statusCode == "200"){
                if let leagueArray = response.dictionary!["user_leagues"]?.array{
                    
                    for details in leagueArray{
                        let leagueDetails = JoinedLeagueDetails()
                        
                        leagueDetails.leagueId = details["league_id"].string
                        leagueDetails.leagueName = details["league_name"].string
                        leagueDetails.leagueWinnerType = details["league_winner_type"].string ?? ""

                        leagueDetails.isInfinity = details["is_infinity"].stringValue
                        leagueDetails.winPerUser = details["win_per_user"].stringValue
                        leagueDetails.winAmount = details["win_amount"].string
                        let joiningAmount = details["joining_amount"].string ?? "0"
                        leagueDetails.joiningAmount = String(Int(Float(joiningAmount) ?? 0))
                        leagueDetails.teamType = details["team_type"].string ?? ""
                        
                        leagueDetails.leagueType = details["league_type"].string ?? ""
                        leagueDetails.totalWinners = details["total_winners"].string ?? "0"
                        leagueDetails.totalWinnersPercent = details["total_winners_percent"].string ?? ""
                        let bannerImages = details["banner_image"].string ?? ""
                        if bannerImages.count > 0 {
                            leagueDetails.bannerImages = bannerImages.components(separatedBy: ",")
                        }

                        leagueDetails.totalJoined = details["total_joined"].string ?? "0"
                        leagueDetails.fantasyType = details["fantasy_type"].string
                        leagueDetails.maxPlayers = details["max_players"].string ?? "0"
                        leagueDetails.matchKey = details["match_key"].string
                        leagueDetails.teamRank = details["rank"].string
                        leagueDetails.confirmedLeague = details["confirmed_league"].string ?? ""
                        leagueDetails.leagueCode = details["league_code"].string ?? ""
                        leagueDetails.userTeamsPdf = details["user_teams_pdf"].string ?? ""
                        
                        if let bonusPercent = details["bonus_percent"].string{
                            leagueDetails.bounsPercentage = bonusPercent
                        }

                        if let contestPlayersDict = details["contest_players"].dictionary{
                            if let selfTeamsArray = contestPlayersDict["self"]?.array{
                                leagueDetails.totalJoinedSelf = selfTeamsArray.count
                            }                            
                        }
                        
                        
                        leagueDetails.bonusApplicable = details["bonus_applicable"].string ?? ""
                        
                        if let bonusJSON = details["time_based_bonus"].string{
                            let json = JSON(parseJSON: bonusJSON)
                            if (json.array != nil){
                                leagueDetails.timeBasedBonusArray = json.array!
                            }
                        }
                        
                        let isPrivateLeagueStr = details["is_private"].string

                        if isPrivateLeagueStr == "1"{
                            leagueDetails.isPrivateLeague = true
                        }
                        
                        if isPrivateLeagueStr == "1"{
                            leagueDetails.isPrivateLeague = true
                        }
                        
                        if leagueDetails.totalJoined != "0"{
                            leagueDetails.isPrivateLeagueJoined = true
                        }                        
                        
                        if let playerDeails = details["contest_players"].dictionary{
                            leagueDetails.nextPageCount = playerDeails["next_page"]?.string ?? "0"
                        }
                        else{
                            leagueDetails.nextPageCount = "0"
                        }
                        
                        let (contestPlayers, myTeamPlayers) =  JoinedLeagueUserList.getAllPArticipatedUsers(contestUsers: details["contest_players"].dictionary!)
                        
                        leagueDetails.contestPlayers = contestPlayers
                        leagueDetails.myTeamPlayers = myTeamPlayers
                        joinedLagueDetailsArray.append(leagueDetails)
                    }
                }
            }
            else{
                let message = result["message"]?.string ?? ""
                AppHelper.showAlertView(message: message, isErrorMessage: true)
            }
        }
        
        return joinedLagueDetailsArray
    }
    
    
    class func getUserTeamsList(result: [String
        : JSON], matchDetails: MatchDetails) -> Array<UserTeamDetails> {
        
        var userTeamArray = Array<UserTeamDetails>()
        
        if let response = result["response"]{
            if let flagDict = response.dictionary?["team_flags"]{
                UserDetails.sharedInstance.teamFlag = flagDict;
            }

            if let teamsArray = response.dictionary!["user_teams"]?.array{
                userTeamArray = UserTeamDetails.getUserTeamsArray(responseArray: teamsArray, matchDetails: matchDetails)
            }
        }
        
        return userTeamArray
    }
}
