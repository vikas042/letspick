//
//  UserDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/28/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

let kDirect = "Direct"
let kNoValue = "No Value"


class UserDetails: NSObject {

    static let sharedInstance = UserDetails()
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var timer: Timer?
    var referralCode: String?
    var zipcode: String?
    var profileImageName: String?
    var city: String?
    var state: String?
    
    lazy var userID = ""
    lazy var userName = ""
    lazy var name = ""
    lazy var emailAdress = ""
    lazy var phoneNumber = ""
    lazy var defalutAddCashAmount = "0"
    lazy var guestAccessToken = ""
    lazy var accessToken = ""
    lazy var deviceToken = ""
    lazy var apiSecretKey = ""
    lazy var channel = kDirect
    lazy var registeredDate = kNoValue
    lazy var signupCode = kNoValue
    lazy var serverTimeStemp = "0"
    lazy var isLiveServerUpdate = false
    lazy var referralAmount = "0"
    lazy var gender = ""
    lazy var dob = kNoValue
    lazy var address = ""
    lazy var isUserNameEditable: Bool = false
    lazy var totalCredits = ""
    lazy var bonusCash = "0"
    lazy var signupBonus = "0"
    lazy var isMultiJoiningAllow = 0
    
//    lazy var ticketTitle = ""
    lazy var ticketApplied = 0

    lazy var withdrawableCredits = "0"
    lazy var totalContestJoined = "0"
    lazy var totalContestWon = "0"
    lazy var totalCashWon = "0"
    lazy var phoneVerified: Bool = false
    lazy var emailVerified: Bool = false
    lazy var panVerified: Bool = false
    lazy var bankVerified: Bool = false
    lazy var bankVerifictionSubmitted: Bool = false
    lazy var panVerifictionSubmitted: Bool = false
    lazy var aadhaarVerifictionSubmitted: Bool = false
    lazy var aadhaarVerified: Bool = false
    lazy var userImageUrl = ""
    lazy var teamImageUrl = ""
    lazy var promotionImageUrl = ""
    lazy var panImageUrl = ""
    lazy var bankImageUrl = ""
    lazy var adharImageUrl = ""
    lazy var teams_pdf = ""
    lazy var bannersFilePath = ""
    var pan_verified = ""
    var BankStatus = ""
    var userAffilate = ""
    lazy var currentBBcoins = "0"
    lazy var totalBcoins = "0"


    let maxCreditLimitForClassic: Float = 100.0
    let maxCreditLimitForBatting: Float = 45.0
    let maxCreditLimitForBowling: Float = 45.0
    lazy var closingTimeForMatch: Int = 0
    
    lazy var maxTeamAllowedForKabaddi: Int = 4
    lazy var maxTeamAllowedForFootball: Int = 4
    lazy var maxTeamAllowedForBasketball: Int = 4
    lazy var maxTeamAllowedForBaseball: Int = 4

    lazy var maxTeamAllowedForClassic: Int = 4
    lazy var maxTeamAllowedForBatting: Int = 6
    lazy var maxTeamAllowedForBowling: Int = 6
    lazy var maxTeamAllowedForReverse: Int = 6
    lazy var maxTeamAllowedForWizard: Int = 6

    lazy var balleBaaziCommission: Float = 10.0
    
    lazy var iOSMinVersion = ""
    lazy var iOSMaxVersion = ""
    lazy var updateAppUrl = ""
    lazy var notificationCount = ""
    lazy var deviceIPAddress = ""
    lazy var unusedCash = "0"
    lazy var active_tickets = "0"
    lazy var isUserComingFromOnBoarding: Bool = false
    lazy var timeTrackEvent = ""
    lazy var userLoggedIn: Bool = false
    lazy var isForceUpdateViewOpen: Bool = false

    lazy var statsArray: [JSON] = []
    lazy var selectedPlayerList:Array<PlayerDetails> = []
    lazy var userTeamsArray:Array<UserTeamDetails> = []
    var allSeriesList:Array<SeriesDetails>?
    var allMatchLeagueSeriesList:Array<SeriesDetails>?
    var teamFlag: JSON?

    lazy var isCricketLineupsOut = false
    lazy var isKabaddiLineupsOut = false
    lazy var isFootballLineupsOut = false
    lazy var isBasketballLineupsOut = false
    lazy var isBaseballLineupsOut = false
    lazy var isPartnershipProgramStatus = "0"
    lazy var isFacebookViewHidden = true


    private override init() {
        
    }
    
    func getAllUserDetails(details: JSON)  {
        
        UserDetails.sharedInstance.userID = details["user_id"].string ?? ""
        UserDetails.sharedInstance.emailAdress = details["email"].string ?? ""
        UserDetails.sharedInstance.registeredDate = "N/A"
        
        if let registeredDate = details["registered_date"].string {
            UserDetails.sharedInstance.registeredDate = registeredDate
        }
        
        if let userAffilate = details["is_affiliate"].string {
            UserDetails.sharedInstance.userAffilate = userAffilate
        }

        if let name = details["name"].string {
            UserDetails.sharedInstance.name = name
        }
        
        if let gender = details["gender"].string {
            UserDetails.sharedInstance.gender = gender
        }
        
        if let dob = details["dob"].string {
            UserDetails.sharedInstance.dob = dob
        }
        
        if let referralCode = details["referral_code"].string {
            UserDetails.sharedInstance.referralCode = referralCode
        }
        
        if let referralAmount = details["referral_amount"].string {
            UserDetails.sharedInstance.referralAmount = referralAmount
        }
        
        if let profileImage = details["image"].string {
            UserDetails.sharedInstance.profileImageName = profileImage
        }
        
        if let phone = details["phone"].string {
            UserDetails.sharedInstance.phoneNumber = phone
        }
        
        if let address = details["address"].string {
            UserDetails.sharedInstance.address = address
        }
        
        if let city = details["city"].string {
            UserDetails.sharedInstance.city = city
        }
        
        if let state = details["state"].string {
            UserDetails.sharedInstance.state = state
        }
        
        if let zipcode = details["zipcode"].string {
            UserDetails.sharedInstance.zipcode = zipcode
        }
        
        if let credits = details["credits"].string {
            UserDetails.sharedInstance.withdrawableCredits = credits
        }
        
        if let totalCredits = details["total_cash"].string {
            UserDetails.sharedInstance.totalCredits = totalCredits
        }
        
        if let currentBBcoins = details["current_bbcoins"].string {
            UserDetails.sharedInstance.currentBBcoins = currentBBcoins
        }

        if let totalCoins = details["total_bbcoins"].string {
            UserDetails.sharedInstance.totalBcoins = totalCoins
        }

        
        if let bonusCash = details["bonus_cash"].string {
            UserDetails.sharedInstance.bonusCash = bonusCash
        }
        
        if let unusedAmount = details["unused_amount"].string {
            UserDetails.sharedInstance.unusedCash = unusedAmount
        }
        
        if let signupBonus = details["signup_bonus"].string {
            UserDetails.sharedInstance.signupBonus = signupBonus
        }
        
        if let totalContestJoined = details["total_contest_joined"].string {
            UserDetails.sharedInstance.totalContestJoined = totalContestJoined
        }
        
        if let totalContestWon = details["total_contest_won"].string {
            UserDetails.sharedInstance.totalContestWon = totalContestWon
        }
        
        if let totalCashWon = details["total_contest_won"].string {
            UserDetails.sharedInstance.totalCashWon = totalCashWon
        }
        
        if let emailVerified = details["email_verified"].string {
            if emailVerified == "1"{
                UserDetails.sharedInstance.emailVerified = true
            }
            else{
                UserDetails.sharedInstance.emailVerified = false
            }
        }
        else{
            UserDetails.sharedInstance.emailVerified = false
        }
        
        if let phoneVerified = details["phone_verified"].string {
            if phoneVerified == "1"{
                UserDetails.sharedInstance.phoneVerified = true
            }else{
                UserDetails.sharedInstance.phoneVerified = false
            }
        }else{
            UserDetails.sharedInstance.phoneVerified = false
        }
        
        if let panVerified = details["pan_verified"].string {
            UserDetails.sharedInstance.pan_verified = panVerified
            if panVerified == "1"{
                UserDetails.sharedInstance.panVerified = true
            }
            else if panVerified == "2"{
                UserDetails.sharedInstance.panVerifictionSubmitted = true
            }else{
                UserDetails.sharedInstance.panVerified = false
                UserDetails.sharedInstance.panVerifictionSubmitted = false
            }
        }
        else{
            UserDetails.sharedInstance.panVerified = false
            UserDetails.sharedInstance.panVerifictionSubmitted = false
        }
        
        if let bankVerified = details["bank_verified"].string {
            UserDetails.sharedInstance.BankStatus = bankVerified

            if bankVerified == "1"{
                UserDetails.sharedInstance.bankVerified = true
            }
            else if bankVerified == "2"{
                UserDetails.sharedInstance.bankVerifictionSubmitted = true
            }else{
                UserDetails.sharedInstance.bankVerified = false
                UserDetails.sharedInstance.bankVerifictionSubmitted = false
            }
        }
        else{
            UserDetails.sharedInstance.bankVerified = false
            UserDetails.sharedInstance.bankVerifictionSubmitted = false
        }
        
        if let aadhaarVerified = details["aadhaar_verified"].string {
            if aadhaarVerified == "1"{
                UserDetails.sharedInstance.aadhaarVerified = true
            }
            else if aadhaarVerified == "2"{
                UserDetails.sharedInstance.aadhaarVerifictionSubmitted = true
            }
            else{
                UserDetails.sharedInstance.aadhaarVerified = false
                UserDetails.sharedInstance.aadhaarVerifictionSubmitted = false

            }
        }else{
            UserDetails.sharedInstance.aadhaarVerified = false
            UserDetails.sharedInstance.aadhaarVerifictionSubmitted = false
        }
    }
    
    func startTimer()  {

        if timer != nil {
            return
        }
        updateTimerVlaue()
        weak var weakSelf = self

        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            guard let weakObj = weakSelf else{
                return;
            }

            UIApplication.shared.endBackgroundTask(weakObj.backgroundTaskIdentifier!)
        })

        if #available(iOS 10.0, *) {

            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
                weakSelf?.updateTimerVlaue()
            }
        }
        else {
            
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerVlaue), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTimerVlaue()  {
        if (UserDetails.sharedInstance.serverTimeStemp.count == 0) || (UserDetails.sharedInstance.serverTimeStemp == "0"){
            return
        }
        if let serverTime = UInt64((UserDetails.sharedInstance.serverTimeStemp)){
            UserDetails.sharedInstance.serverTimeStemp = String(serverTime + 1)
        }
    }
    
    func getAllState()  {
        let url = Bundle.main.url(forResource: "States", withExtension: "plist")!
        let soundsData = try! Data(contentsOf: url)
        let myPlist = try! PropertyListSerialization.propertyList(from: soundsData, options: [], format: nil) as? Array<Dictionary<String, String>>
        statsArray.removeAll()
        for details in myPlist! {
            let json = JSON(details)
            if (json["stateName"].stringValue != "Telangana") && (json["stateName"].stringValue != "Odisha") && (json["stateName"].stringValue != "Assam"){
                statsArray.append(json)
            }
        }
    }
    
    class func getMyIPAddress(){
    
        WebServiceHandler.performGETRequest(withURL: "https://api.ipify.org?format=json") { (result, error) in
            
            if result != nil{
                if let ipAddress = result!["ip"].string{
                    UserDetails.sharedInstance.deviceIPAddress = ipAddress;
                }
            }
        }
    }
}
