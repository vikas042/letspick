//
//  PassesDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class PassesDetails: NSObject {

    
    var discount = ""
    var titleHindi = ""
    var titleEnglish = ""
    var fantasyType = ""
    var categoryId = ""
    var descriptionHindi = ""
    var descriptionEnglish = ""
    var totalPurchased = ""
    var passId = ""
    var leagueCategoryId = ""
    var categoryName = ""
    var totalMatches = ""
    var passPrice = ""
    var image = ""
    var passType = ""
    var actualAmount = ""
    var status = ""
    var totalLeagueEntries = ""
    var leagueBuyInAmount = ""
    var seasonName = ""
    var seasonShortName = ""
    var playType = ""
    var seasonKey = ""

    class func getAllPasesDetails(dataArray: Array<JSON>) -> Array<PassesDetails> {
        
        var passesDetailsArray = Array<PassesDetails>()
        for details in dataArray {
            let passesDetails = PassesDetails.parsePassesData(details)
            passesDetailsArray.append(passesDetails)
        }
        
        return passesDetailsArray
    }
        
    class func parsePassesData(_ details: JSON) -> PassesDetails {
        let passesDetails = PassesDetails()
        
        passesDetails.discount = details["discount"].string ?? ""
        passesDetails.titleHindi = details["title_hi"].string ?? ""
        passesDetails.titleEnglish = details["title"].string ?? ""
        passesDetails.fantasyType = details["fantasy_type"].string ?? ""
        passesDetails.categoryId = details["category_id"].string ?? ""
        passesDetails.descriptionHindi = details["description_hi"].string ?? ""
        passesDetails.descriptionEnglish = details["description"].string ?? ""
        passesDetails.totalPurchased = details["total_purchased"].string ?? ""
        passesDetails.passId = details["pass_id"].string ?? ""
        passesDetails.leagueCategoryId = details["league_category_id"].string ?? ""
        passesDetails.categoryName = details["category_name"].string ?? ""
        passesDetails.totalMatches = details["total_matches"].string ?? ""
        passesDetails.passPrice = details["pass_price"].string ?? ""
        
        let imageName = details["image"].string ?? ""
        if imageName.count > 0 {
            passesDetails.image = UserDetails.sharedInstance.promotionImageUrl + imageName
        }
        
        passesDetails.passType = details["pass_type"].string ?? ""
        passesDetails.actualAmount = details["actual_amount"].string ?? ""
        passesDetails.status = details["status"].string ?? ""
        passesDetails.totalLeagueEntries = details["total_league_entries"].string ?? ""
        passesDetails.leagueBuyInAmount = details["league_buy_in_amount"].string ?? ""
        passesDetails.seasonName = details["season_name"].string ?? ""
        passesDetails.seasonShortName = details["season_short_name"].string ?? ""
        passesDetails.playType = details["play_type"].string ?? ""
        passesDetails.seasonKey = details["season_key"].string ?? ""
        
        return passesDetails
    }
  
}
