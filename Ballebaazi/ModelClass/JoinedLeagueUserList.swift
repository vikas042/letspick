//
//  JoinedLeagueUserList.swift
//  Letspick
//
//  Created by Vikash Rajput on 03/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class JoinedLeagueUserList: NSObject {

    var userName: String?
    var rank: String?
    var totalPoints: String?
    var teamNumber: String?
    var userID: String?
    var creditWon = ""
    var coinWon = ""

    
    class func getAllPArticipatedUsers(contestUsers: [String: JSON]) -> (Array<JoinedLeagueUserList>, Array<JoinedLeagueUserList>) {
        
        var leagueUsersArray = Array<JoinedLeagueUserList>()
        var myTeamsArray = Array<JoinedLeagueUserList>()

        if let ownArray = contestUsers["self"]?.array{
            
            for details in ownArray{
                let ownDetails = JoinedLeagueUserList()
                ownDetails.userName = details["user_name"].string
                ownDetails.rank = details["rank"].string
                ownDetails.totalPoints = details["total_points"].string
                ownDetails.teamNumber = details["team_number"].string
                ownDetails.userID = details["user_id"].stringValue
                ownDetails.creditWon = details["credits_won"].string ?? "0"
                ownDetails.coinWon = details["bbcoins_won"].string ?? "0"

                myTeamsArray.append(ownDetails)
            }
        }
        
        if let otherUserArray = contestUsers["others"]?.array{
            
            for details in otherUserArray{
                
                let otherUserdetails = JoinedLeagueUserList()
                otherUserdetails.userName = details["user_name"].string
                otherUserdetails.rank = details["rank"].string
                otherUserdetails.totalPoints = details["total_points"].string
                otherUserdetails.teamNumber = details["team_number"].string
                otherUserdetails.userID = details["user_id"].stringValue
                otherUserdetails.creditWon = details["credits_won"].string ?? "0"
                otherUserdetails.coinWon = details["bbcoins_won"].string ?? "0"

                leagueUsersArray.append(otherUserdetails)
            }
        }
        
        return (leagueUsersArray, myTeamsArray)
    }
    
    
    
    class func getNextPageParticipatedUsers(contestUsers: [String: JSON], isNeedToAddCurrentUser: Bool) -> (Array<JoinedLeagueUserList>, Array<JoinedLeagueUserList>){
        
        var leagueUsersArray = Array<JoinedLeagueUserList>()
        var myTeamArray = Array<JoinedLeagueUserList>()

        if isNeedToAddCurrentUser{
            if let ownArray = contestUsers["current_user"]?.array{
                
                for details in ownArray{
                    
                    let ownDetails = JoinedLeagueUserList()
                    ownDetails.userName = details["user_name"].string
                    ownDetails.rank = details["rank"].string
                    ownDetails.totalPoints = details["total_points"].string
                    ownDetails.teamNumber = details["team_number"].string
                    ownDetails.userID = details["user_id"].stringValue
                    ownDetails.creditWon = details["credits_won"].string ?? "0"
                    ownDetails.coinWon = details["bbcoins_won"].string ?? "0"

                    myTeamArray.append(ownDetails)
                }
            }
        }
        
        if let otherUserArray = contestUsers["all_users"]?.array{
            
            for details in otherUserArray{
                
                let otherUserdetails = JoinedLeagueUserList()
                otherUserdetails.userName = details["user_name"].string
                otherUserdetails.rank = details["rank"].string
                otherUserdetails.totalPoints = details["total_points"].string
                otherUserdetails.teamNumber = details["team_number"].string
                otherUserdetails.userID = details["user_id"].stringValue
                otherUserdetails.creditWon = details["credits_won"].string ?? "0"
                otherUserdetails.coinWon = details["bbcoins_won"].string ?? "0"

                leagueUsersArray.append(otherUserdetails)
            }
        }
        
        return (leagueUsersArray, myTeamArray)
    }
}
