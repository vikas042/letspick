//
//  LiveScoreDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 16/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class LiveScoreDetails: NSObject {

    var firstTeamShortName = ""
    var firstTeamFullName = ""
    var firstTeamBattingID = ""
    var firstTeamFlagName = ""
    var firstTeamScoreDetails = ""
    var firstTeamTotalScore = ""
    var firstTeamTotalTitleScore = ""
    var firstTeamDoNotBats = ""
    var firstTeamExtraScore = ""
    var firstTeamFallOfWickets = ""
    var firstTeamBatsmanArray = Array<LiveScoreMathchPlayerDetails>()
    var firstTeamBowlerArray = Array<LiveScoreMathchPlayerDetails>()

    var secondTeamName = ""
    var secondTeamFullName = ""
    var secondTeamBattingID = ""
    var secondTeamFlagName = ""
    var secondTeamScoreDetails = ""
    var secondTeamExtraScore = ""
    var secondTeamFallOfWickets = ""
    var secondTeamDoNotBats = ""
    var secondTeamTotalScore = ""
    var secondTeamTotalTitleScore = ""
    var secodsTeamBatsmanArray = Array<LiveScoreMathchPlayerDetails>()
    var secondTeamBowlerArray = Array<LiveScoreMathchPlayerDetails>()
    
    var thirdTeamShortName = ""
    var thirdTeamFullName = ""
    var thirdTeamBattingID = ""
    var thirdTeamFlagName = ""
    var thirdTeamScoreDetails = ""
    var thirdTeamExtraScore = ""
    var thirdTeamFallOfWickets = ""
    var thirdTeamDoNotBats = ""
    var thirdTeamTotalScore = ""
    var thirdTeamTotalTitleScore = ""
    var thirdTeamBatsmanArray = Array<LiveScoreMathchPlayerDetails>()
    var thirdTeamBowlerArray = Array<LiveScoreMathchPlayerDetails>()
    
    var fourthTeamShortName = ""
    var fourthTeamFullName = ""
    var fourthTeamBattingID = ""
    var fourthTeamFlagName = ""
    var fourthTeamScoreDetails = ""
    var fourthTeamExtraScore = ""
    var fourthTeamFallOfWickets = ""
    var fourthTeamDoNotBats = ""
    var fourthTeamTotalScore = ""
    var fourthTeamTotalTitleScore = ""
    var fourthTeamBatsmanArray = Array<LiveScoreMathchPlayerDetails>()
    var fourthTeamBowlerArray = Array<LiveScoreMathchPlayerDetails>()
    
    var matchApiType = ""
    var matchKey = ""
    var matchType = ""
    var matchDescription = ""
    
    class func getLiveScoreDetails(liveScoreArray: [JSON]) -> [LiveScoreDetails] {
        
        var liveScoreDetailsArray = [LiveScoreDetails]()
        
        for liveScore in liveScoreArray{
            
            let liveDetails = LiveScoreDetails()
            if liveScore.type != .dictionary{
                continue;
            }

            var matchType = liveScore.dictionary!["match_format"]?.string
            liveDetails.matchDescription = liveScore.dictionary!["result"]?.string ?? ""
            
            if let matchKey = liveScore.dictionary!["match_key"]?.string{
                liveDetails.matchKey =  matchKey
            }

            let apiType = liveScore.dictionary!["api_type"]?.string
            liveDetails.matchApiType = apiType ?? ""
            var response: [String: JSON]?;
            if let responseString = liveScore.dictionary!["response"]?.string{
                let json = JSON(parseJSON: responseString)
                if ((json.dictionary) != nil){
                    response = json.dictionary!
                }
            }
            else if let responseJson = liveScore.dictionary!["response"]?.dictionary
            {
                response = responseJson
            }
            
            if response != nil{
                if apiType == "1"{
                   
                    guard let teamInfo = response!["team_info"]!.dictionary else {return liveScoreDetailsArray}
                    guard let playing22 = response!["playing_22"] else {return liveScoreDetailsArray}
    
                    if let inningsArray = response!["innings"]?.array{
                        
                        if inningsArray.count == 4{
                            if  (matchType == "one-day") || (matchType == "t20"){
                                matchType = "test"
                            }
                        }

                        if  (matchType == "one-day") || (matchType == "t20") || (matchType == "t10"){
                            
                            if inningsArray.count == 1{
                                let firstInningDetails = inningsArray[0]
                                
                                let innigeAttributs = firstInningDetails["@attributes"]
                                
                                liveDetails.firstTeamBattingID = innigeAttributs["batting_team_id"].string ?? ""
                                if let teamDetails = teamInfo[liveDetails.firstTeamBattingID]{
                                    liveDetails.firstTeamShortName = teamDetails["team_short_name"].string ?? ""
                                    liveDetails.firstTeamFullName = teamDetails["team_name"].string ?? ""
                                    liveDetails.firstTeamFlagName = teamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = firstInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.firstTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.firstTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.firstTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + ")"
                                        
                                        if let extra = firstInningDetails["Extras"].dictionary{
                                            liveDetails.firstTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }

                                        if let fallOfWicket = firstInningDetails["FallofWickets"].dictionary{
                                            liveDetails.firstTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }

                                        
                                        if let batsmanDict = firstInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)

                                            if doNotBat.count == 0{
                                                liveDetails.firstTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.firstTeamDoNotBats = doNotBat
                                            }

                                            liveDetails.firstTeamBatsmanArray = basmanArray
                                        }
                                        
                                        if let bowlerDict = firstInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.firstTeamBowlerArray = bowlerArray
                                        }
                                    }
                                }
                            }
                            else if inningsArray.count == 2{
                                
                                // First Inning Details
                                let firstInningDetails = inningsArray[1]
                                let innigeAttributs = firstInningDetails["@attributes"]
                                liveDetails.firstTeamBattingID = innigeAttributs["batting_team_id"].string ?? ""
                                
                                if let firstTeamDetails = teamInfo[liveDetails.firstTeamBattingID]{
                                    liveDetails.firstTeamShortName = firstTeamDetails["team_short_name"].string ?? ""
                                    liveDetails.firstTeamFullName = firstTeamDetails["team_name"].string ?? ""
                                    liveDetails.firstTeamFlagName = firstTeamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = firstInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.firstTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.firstTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.firstTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + ")"
                                        if let extra = firstInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.firstTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }
                                        
                                        if let fallOfWicket = firstInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.firstTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }

                                        if let batsmanDict = firstInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)
                                            if doNotBat.count == 0{
                                                liveDetails.firstTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.firstTeamDoNotBats = doNotBat
                                            }
                                            liveDetails.firstTeamBatsmanArray = basmanArray
                                        }
                                        
                                        if let bowlerDict = firstInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.firstTeamBowlerArray = bowlerArray
                                        }
                                    }
                                }
                                
                                // Second Inning Details
                                
                                let secondInningDetails = inningsArray[0]
                                let secondInnigeAttributs = secondInningDetails["@attributes"]
                                liveDetails.secondTeamBattingID = secondInnigeAttributs["batting_team_id"].string ?? ""
                                if let secondTeamDetails = teamInfo[liveDetails.secondTeamBattingID]{
                                    liveDetails.secondTeamName = secondTeamDetails["team_short_name"].string ?? ""
                                    liveDetails.secondTeamFullName = secondTeamDetails["team_name"].string ?? ""
                                    liveDetails.secondTeamFlagName = secondTeamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = secondInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.secondTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.secondTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.secondTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = secondInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.secondTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }
                                        
                                        if let fallOfWicket = secondInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.secondTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }
                                        
                                        if let batsmanDict = secondInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)
                                            if doNotBat.count == 0{
                                                liveDetails.secondTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.secondTeamDoNotBats = doNotBat
                                            }

                                            liveDetails.secodsTeamBatsmanArray = basmanArray
                                        }

                                        if let bowlerDict = secondInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.secondTeamBowlerArray = bowlerArray
                                        }
                                    }
                                }
                            }
                        }
                        else if (matchType == "test"){
                            
                            if inningsArray.count == 1{
                                let firstInningDetails = inningsArray[0]
                                let innigeAttributs = firstInningDetails["@attributes"]
                                
                                liveDetails.firstTeamBattingID = innigeAttributs["batting_team_id"].string ?? ""
                                if let teamDetails = teamInfo[liveDetails.firstTeamBattingID]{
                                    liveDetails.firstTeamShortName = teamDetails["team_short_name"].string ?? ""
                                    liveDetails.firstTeamFullName = teamDetails["team_name"].string ?? ""
                                    liveDetails.firstTeamFlagName = teamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = firstInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.firstTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.firstTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.firstTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = firstInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.firstTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }

                                        if let fallOfWicket = firstInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.firstTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }
                                        
                                        if let batsmanDict = firstInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)

                                            if doNotBat.count == 0{
                                                liveDetails.firstTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.firstTeamDoNotBats = doNotBat
                                            }
                                            liveDetails.firstTeamBatsmanArray = basmanArray
                                        }
                                        
                                        if let bowlerDict = firstInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.firstTeamBowlerArray = bowlerArray
                                        }

                                    }
                                }
                            }
                            else if inningsArray.count == 2{
                                
                                // First Inning Details
                                let firstInningDetails = inningsArray[1]
                                let innigeAttributs = firstInningDetails["@attributes"]
                                
                                liveDetails.firstTeamBattingID = innigeAttributs["batting_team_id"].string ?? ""
                                
                                if let firstTeamDetails = teamInfo[liveDetails.firstTeamBattingID]{
                                    liveDetails.firstTeamShortName = firstTeamDetails["team_short_name"].string ?? ""
                                    liveDetails.firstTeamFullName = firstTeamDetails["team_name"].string ?? ""
                                    liveDetails.firstTeamFlagName = firstTeamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = firstInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.firstTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.firstTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.firstTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = firstInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.firstTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }

                                        if let fallOfWicket = firstInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.firstTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }
                                        
                                        if let batsmanDict = firstInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)
                                            if doNotBat.count == 0{
                                                liveDetails.firstTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.firstTeamDoNotBats = doNotBat
                                            }
                                            liveDetails.firstTeamBatsmanArray = basmanArray
                                        }
                                        
                                        if let bowlerDict = firstInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.firstTeamBowlerArray = bowlerArray
                                        }

                                    }
                                }
                                
                                // Second Inning Details
                                
                                let secondInningDetails = inningsArray[0]
                                let secondInnigeAttributs = secondInningDetails["@attributes"]
                                liveDetails.secondTeamBattingID = secondInnigeAttributs["batting_team_id"].string ?? ""
                                if let secondTeamDetails = teamInfo[liveDetails.secondTeamBattingID]{
                                    liveDetails.secondTeamName = secondTeamDetails["team_short_name"].string ?? ""
                                    liveDetails.secondTeamFullName = secondTeamDetails["team_name"].string ?? ""
                                    liveDetails.secondTeamFlagName = secondTeamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = secondInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.secondTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.secondTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.secondTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = secondInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.secondTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }

                                        
                                        if let fallOfWicket = secondInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.secondTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }
                                        
                                        if let batsmanDict = secondInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)

                                            if doNotBat.count == 0{
                                                liveDetails.secondTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.secondTeamDoNotBats = doNotBat
                                            }
                                            liveDetails.secodsTeamBatsmanArray = basmanArray
                                        }

                                        if let bowlerDict = secondInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.secondTeamBowlerArray = bowlerArray
                                        }
                                    }
                                }
                            }
                            else if inningsArray.count == 3{
                                
                                // First Inning Details
                                let firstInningDetails = inningsArray[2]
                                let innigeAttributs = firstInningDetails["@attributes"]
                                
                                liveDetails.firstTeamBattingID = innigeAttributs["batting_team_id"].string ?? ""
                                
                                if let firstTeamDetails = teamInfo[liveDetails.firstTeamBattingID]{
                                    liveDetails.firstTeamShortName = firstTeamDetails["team_short_name"].string ?? ""
                                    liveDetails.firstTeamFullName = firstTeamDetails["team_name"].string ?? ""
                                    liveDetails.firstTeamFlagName = firstTeamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = firstInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.firstTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.firstTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.firstTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = firstInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.firstTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }
                                        
                                        if let fallOfWicket = firstInningDetails["FallofWickets"].dictionary{
                                            liveDetails.firstTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }

                                        if let batsmanDict = firstInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)

                                            if doNotBat.count == 0{
                                                liveDetails.firstTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.firstTeamDoNotBats = doNotBat
                                            }
                                            liveDetails.firstTeamBatsmanArray = basmanArray
                                        }
                                        
                                        if let bowlerDict = firstInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.firstTeamBowlerArray = bowlerArray
                                        }

                                    }
                                }
                                
                                // Second Inning Details
                                
                                let secondInningDetails = inningsArray[1]
                                let secondInnigeAttributs = secondInningDetails["@attributes"]
                                liveDetails.secondTeamBattingID = secondInnigeAttributs["batting_team_id"].string ?? ""
                                if let secondTeamDetails = teamInfo[liveDetails.secondTeamBattingID]{
                                    liveDetails.secondTeamName = secondTeamDetails["team_short_name"].string ?? ""
                                    liveDetails.secondTeamFullName = secondTeamDetails["team_name"].string ?? ""
                                    liveDetails.secondTeamFlagName = secondTeamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = secondInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.secondTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.secondTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.secondTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = secondInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.secondTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }
                                        
                                        if let fallOfWicket = secondInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.secondTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }
                                        
                                        if let batsmanDict = secondInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)
                                            
                                            
                                            if doNotBat.count == 0{
                                                liveDetails.secondTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.secondTeamDoNotBats = doNotBat
                                            }

                                            liveDetails.secodsTeamBatsmanArray = basmanArray
                                        }

                                        if let bowlerDict = secondInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.secondTeamBowlerArray = bowlerArray
                                        }
                                    }
                                }
                                
                                // Third Inning Details
                                let thirdInningDetails = inningsArray[0]
                                let thirdInnigeAttributs = thirdInningDetails["@attributes"]
                                let teamBattingID = thirdInnigeAttributs["batting_team_id"].string ?? ""
                                
                                if let totalScore = thirdInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        let scoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        
                                        
                                        /**********************/
                                        // third inning
                                        
                                        liveDetails.thirdTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.thirdTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = thirdInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.thirdTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }
                                        
                                        if let fallOfWicket = thirdInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.thirdTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }
                                        
                                        if let batsmanDict = thirdInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)

                                            if doNotBat.count == 0{
                                                liveDetails.thirdTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.thirdTeamDoNotBats = doNotBat
                                            }
                                            liveDetails.thirdTeamBatsmanArray = basmanArray
                                        }
                                        
                                        if let bowlerDict = thirdInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.thirdTeamBowlerArray = bowlerArray
                                        }
                                        
                                        /**********************/
                                        
                                        
                                        liveDetails.thirdTeamScoreDetails = scoreDetails

                                        if teamBattingID == liveDetails.firstTeamBattingID{
                                            liveDetails.thirdTeamFullName = liveDetails.firstTeamFullName
                                        }
                                        else{
                                            liveDetails.thirdTeamFullName = liveDetails.secondTeamName
                                        }
                                        
                                        let followOn = thirdInnigeAttributs["follow_on"].string ?? "0"
                                        if followOn == "1"{
                                            let temp = liveDetails.firstTeamScoreDetails;
                                            liveDetails.firstTeamScoreDetails = liveDetails.secondTeamScoreDetails
                                            liveDetails.secondTeamScoreDetails = temp
                                            
                                            // Swipe
                                        }
                                    }
                                }
                            }
                            else if inningsArray.count == 4{
                                
                                // First Inning Details
                                let firstInningDetails = inningsArray[3]
                                let innigeAttributs = firstInningDetails["@attributes"]
                                
                                liveDetails.firstTeamBattingID = innigeAttributs["batting_team_id"].string ?? ""
                                
                                if let firstTeamDetails = teamInfo[liveDetails.firstTeamBattingID]{
                                    liveDetails.firstTeamShortName = firstTeamDetails["team_short_name"].string ?? ""
                                    liveDetails.firstTeamFullName = firstTeamDetails["team_name"].string ?? ""
                                    liveDetails.firstTeamFlagName = firstTeamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = firstInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.firstTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.firstTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.firstTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = firstInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.firstTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }
                                        
                                        if let fallOfWicket = firstInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.firstTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }

                                        if let batsmanDict = firstInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)
                                            
                                            if doNotBat.count == 0{
                                                liveDetails.firstTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.firstTeamDoNotBats = doNotBat
                                            }

                                            liveDetails.firstTeamBatsmanArray = basmanArray
                                        }
                                        
                                        if let bowlerDict = firstInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.firstTeamBowlerArray = bowlerArray
                                        }
                                    }
                                    
                                }
                                
                                // Second Inning Details
                                
                                let secondInningDetails = inningsArray[2]
                                let secondInnigeAttributs = secondInningDetails["@attributes"]
                                liveDetails.secondTeamBattingID = secondInnigeAttributs["batting_team_id"].string ?? ""
                                if let secondTeamDetails = teamInfo[liveDetails.secondTeamBattingID]{
                                    liveDetails.secondTeamName = secondTeamDetails["team_short_name"].string ?? ""
                                    liveDetails.secondTeamFullName = secondTeamDetails["team_name"].string ?? ""
                                    liveDetails.secondTeamFlagName = secondTeamDetails["team_flag"].string ?? ""
                                }
                                
                                if let totalScore = secondInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        liveDetails.secondTeamScoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        liveDetails.secondTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.secondTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = secondInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.secondTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }
                                        
                                        if let fallOfWicket = secondInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.secondTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }
                                        
                                        
                                        if let batsmanDict = secondInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)

                                            if doNotBat.count == 0{
                                                liveDetails.secondTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.secondTeamDoNotBats = doNotBat
                                            }
                                            liveDetails.secodsTeamBatsmanArray = basmanArray
                                        }

                                        if let bowlerDict = secondInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.secondTeamBowlerArray = bowlerArray
                                        }

                                    }
                                }
                                
                                // Third Inning Details
                                let thirdInningDetails = inningsArray[1]
                                let thirdInnigeAttributs = thirdInningDetails["@attributes"]
                                let teamBattingID = thirdInnigeAttributs["batting_team_id"].string ?? ""
                                
                                if let totalScore = thirdInningDetails["Total"].dictionary{
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        let scoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        
                                        
                                        /**********************/
                                        // third inning
                                        
                                        liveDetails.thirdTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.thirdTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = thirdInningDetails["Extras"].dictionary{
                                            liveDetails.thirdTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }
                                        
                                        if let fallOfWicket = thirdInningDetails["FallofWickets"].dictionary{
                                            liveDetails.thirdTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }
                                        
                                        if let batsmanDict = thirdInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)

                                            if doNotBat.count == 0{
                                                liveDetails.thirdTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.thirdTeamDoNotBats = doNotBat
                                            }

                                            liveDetails.thirdTeamBatsmanArray = basmanArray
                                        }
                                        
                                        if let bowlerDict = thirdInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.thirdTeamBowlerArray = bowlerArray
                                        }
                                        
                                        /**********************/
                                        liveDetails.thirdTeamScoreDetails = scoreDetails
                                        
                                        liveDetails.thirdTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.thirdTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        
                                        if teamBattingID == liveDetails.firstTeamBattingID{
                                            
                                            liveDetails.thirdTeamFullName = liveDetails.firstTeamFullName
                                        }
                                        else{
                                            liveDetails.thirdTeamFullName = liveDetails.secondTeamFullName
                                        }
                                        
                                        
                                        let followOn = thirdInnigeAttributs[""].string ?? "0"
                                        if followOn == "1"{
                                            let temp = liveDetails.firstTeamScoreDetails;
                                            liveDetails.firstTeamScoreDetails = liveDetails.secondTeamScoreDetails
                                            liveDetails.secondTeamScoreDetails = temp
                                        }
                                    }
                                }
                                
                                // Fourth Inning Details
                                let fourthInningDetails = inningsArray[0]
                                let fourthInnigeAttributs = fourthInningDetails["@attributes"]
                                let fourthTeamBattingID = fourthInnigeAttributs["batting_team_id"].string ?? ""
                                
                                if let totalScore = fourthInningDetails["Total"].dictionary{
                                   
                                    if let attributes = totalScore["@attributes"]?.dictionary{
                                        
                                        let overs = attributes["overs"]?.string ?? "0"
                                        let runScored = attributes["runs_scored"]?.string ?? "0"
                                        let wickets = attributes["wickets"]?.string ?? "0"
                                        let scoreDetails = runScored + "/" + wickets + "(" +  overs + " OVS)"
                                        
                                        
                                        /**********************/
                                        // Fourth inning
                                        
                                        liveDetails.fourthTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.fourthTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        if let extra = fourthInningDetails["Extras"].dictionary{
                                            
                                            liveDetails.fourthTeamExtraScore = LiveScoreDetails.getExtraForOcta(extraJson: extra)
                                        }
                                        
                                        if let fallOfWicket = fourthInningDetails["FallofWickets"].dictionary{
                                            
                                            liveDetails.fourthTeamFallOfWickets = LiveScoreDetails.getFallOfWicketsForOcta(fallofWicketJson: fallOfWicket, playing22: playing22)
                                        }
                                        
                                        if let batsmanDict = fourthInningDetails["Batsmen"].dictionary{
                                            let (basmanArray, doNotBat) = LiveScoreDetails.getBatsmanArray(batsmanDict: batsmanDict, playing22: playing22)
                                            
                                            if doNotBat.count == 0{
                                                liveDetails.fourthTeamDoNotBats = "N/A"
                                            }
                                            else{
                                                liveDetails.fourthTeamDoNotBats = doNotBat
                                            }
                                            
                                            liveDetails.fourthTeamBatsmanArray = basmanArray
                                        }
                                        
                                        if let bowlerDict = fourthInningDetails["Bowlers"].dictionary{
                                            let bowlerArray = LiveScoreDetails.getBowlerArrayFromOcta(bowlerDict: bowlerDict, playing22: playing22)
                                            liveDetails.fourthTeamBowlerArray = bowlerArray
                                        }
                                        
                                        liveDetails.fourthTeamScoreDetails = scoreDetails
                                        liveDetails.fourthTeamFlagName = liveDetails.firstTeamFullName
                                        liveDetails.fourthTeamTotalScore = runScored + "( " + wickets + " wkts," + overs + " ov)"
                                        liveDetails.fourthTeamTotalTitleScore = runScored + "/" + wickets + "(" + overs + " ov)"
                                        
                                        /**********************/
                                        
                                        if fourthTeamBattingID == liveDetails.firstTeamBattingID{
                                            liveDetails.fourthTeamFullName = liveDetails.firstTeamFullName
                                        }
                                        else{
                                           liveDetails.fourthTeamFullName = liveDetails.secondTeamFullName
                                        }
                                        
                                        let followOn = fourthInnigeAttributs["follow_on"].string ?? "0"
                                        if followOn == "1"{
                                            let temp = liveDetails.firstTeamScoreDetails;
                                            liveDetails.firstTeamScoreDetails = liveDetails.secondTeamScoreDetails
                                            liveDetails.secondTeamScoreDetails = temp
                                        }
                                    }
                                }
                                
                            }
                        }
                        liveScoreDetailsArray.append(liveDetails)
                    }
                }
                else if apiType == "2"{

                    if (matchType == "one-day") || (matchType == "t20") || (matchType == "t10"){
                        guard let teamInfo = response!["team_info"]!.dictionary else {return liveScoreDetailsArray}
                        
                        if let matchSummery = response!["match_summery"]?.dictionary{
                            let teamKeyDetails = matchSummery["teams"]!.dictionary
                            let totalPlayersDetails = matchSummery["players"]!.dictionary
                            if let inningsDetails = matchSummery["innings"]?.dictionary{

                                var teamDetailsArray = [[String: Any]]()
                                
                                for (key, value) in inningsDetails{
                                    
                                    let keyNameArr = key.components(separatedBy: "_1")
                                    var teamKeyInfo: JSON?
                                    if keyNameArr.count > 0{
                                        teamKeyInfo = teamKeyDetails?[keyNameArr[0]]
                                    }
                                    
                                    var playerBatsmanArray = Array<LiveScoreMathchPlayerDetails>()
                                    var playerbowlerArray = Array<LiveScoreMathchPlayerDetails>()
                                    var playing11: Array<JSON>?

                                    if teamKeyInfo != nil{
                                        let teamKey = teamKeyInfo!["key"].string
                                        if teamInfo[teamKey!] != nil{
                                            if let matchDetails = teamKeyInfo!["match"].dictionary{
                                                playing11 = matchDetails["playing_xi"]?.array
                                            }
                                        }
                                    }
                                    
                                    var didNotBat = ""
                                    
                                    if let batsmanArray = value.dictionary!["batting_order"]?.array{
                                        
                                        for playerNameKey in batsmanArray{
                                            let playerDetails = totalPlayersDetails![playerNameKey.string!]?.dictionary
                                            
                                            let liverPyerDetails = LiveScoreMathchPlayerDetails()
                                            liverPyerDetails.playerName = playerDetails!["name"]?.string ?? ""
                                            
                                            if let match = playerDetails!["match"]?.dictionary{
                                                if let playerInning = match["innings"]?.dictionary{
                                                    
                                                    if let inningDetails = playerInning["1"]?.dictionary{
                                                        if let battingDetails = inningDetails["batting"]?.dictionary{
                                                            
                                                            liverPyerDetails.battingRun = battingDetails["runs"]?.stringValue ?? "0"
                                                            liverPyerDetails.battingBowl = battingDetails["balls"]?.stringValue ?? "0"
                                                            liverPyerDetails.battingFours = battingDetails["fours"]?.stringValue ?? "0"
                                                            liverPyerDetails.battingSixes = battingDetails["sixes"]?.stringValue ?? "0"
                                                            liverPyerDetails.batsmanStrikeRate = battingDetails["strike_rate"]?.stringValue ?? "0"
                                                            let outSummery = battingDetails["out_str"]?.string ?? ""
                                                            liverPyerDetails.batsmanStatusMessage = outSummery
                                                            if let dismised = battingDetails["dismissed"]?.boolValue{
                                                                if dismised == false{
                                                                 liverPyerDetails.batsmanStatusMessage = "NOT OUT"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            var isPlayerExists = false
                                            
                                            if playing11 != nil{
                                                for playing11Key in playing11!{
                                                    if playing11Key.stringValue == playerNameKey.stringValue{
                                                        isPlayerExists = true
                                                        break;
                                                    }
                                                }
                                            }
                                            
                                            if !isPlayerExists{
                                                if didNotBat.count == 0{
                                                    didNotBat = liverPyerDetails.playerName
                                                }
                                                else{
                                                    didNotBat = didNotBat + "," + liverPyerDetails.playerName
                                                }
                                            }
                                            playerBatsmanArray.append(liverPyerDetails)
                                        }
                                    }
                                    
                                    if let bowlerArray = value.dictionary!["bowling_order"]?.array{
                                        
                                        for playerNameKey in bowlerArray{
                                            let playerDetails = totalPlayersDetails![playerNameKey.string!]?.dictionary
                                            
                                            let liverPyerDetails = LiveScoreMathchPlayerDetails()
                                            liverPyerDetails.playerName = playerDetails!["name"]?.string ?? ""
                                            playerbowlerArray.append(liverPyerDetails)
                                            
                                            if let match = playerDetails!["match"]?.dictionary{
                                                if let playerInning = match["innings"]?.dictionary{
                                                    
                                                    if let inningDetails = playerInning["1"]?.dictionary{
                                                        if let bowlingDetails = inningDetails["bowling"]?.dictionary{
                                                            
                                                            liverPyerDetails.bowlerRunConceded = bowlingDetails["runs"]?.stringValue ?? "0"
                                                            liverPyerDetails.bowlerBallsBowled = bowlingDetails["balls"]?.stringValue ?? "0"
                                                            liverPyerDetails.bowlerMaindenOvers = bowlingDetails["maiden_overs"]?.stringValue ?? "0"
                                                            liverPyerDetails.bowlerWicketTaken = bowlingDetails["wickets"]?.stringValue ?? "0"
                                                            liverPyerDetails.bowlerEcoRate = bowlingDetails["economy"]?.stringValue ?? "0"
                                                            liverPyerDetails.bowlerOverBowled = bowlingDetails["overs"]?.string ?? ""
                                                            
                                                            liverPyerDetails.bowlerNoBowl = "0"
                                                            liverPyerDetails.bowlerWideBalls = "0"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    var fallOfWickets = ""
                                    
                                    if let fallOfWicketsArray = value.dictionary!["fall_of_wickets"]?.array{
                                        
                                        for fallDescription in fallOfWicketsArray{
                                            if fallOfWickets.count == 0{
                                                fallOfWickets = fallDescription.string ?? ""
                                            }
                                            else{
                                                fallOfWickets = fallOfWickets + ", " + (fallDescription.string ?? "")
                                            }
                                        }
                                    }

                                    
                                    let noballa = value.dictionary!["noball"]?.stringValue ?? "0"
                                    let legbye = value.dictionary!["legbye"]?.stringValue ?? "0"
                                    let bye = value.dictionary!["bye"]?.stringValue ?? "0"
                                    let wide = value.dictionary!["wide"]?.stringValue ?? "0"
                                    let extras = value.dictionary!["extras"]?.stringValue ?? "0"

                                    let extraRunsStr = extras + "(b " + bye + ", lb " + legbye + ", w " + wide + ", nb " + noballa + ")"
                                    
                                    
                                    let run = value.dictionary!["runs"]?.stringValue ?? "0"
                                    let wicket = value.dictionary!["wickets"]?.stringValue ?? "0"
                                    let overs = value.dictionary!["overs"]?.string ?? "0"
                                    let fequiredFormat = run + "/" + wicket + "(" + overs + " OVS)"
                                    
                                    if teamKeyInfo != nil{
                                        let teamKey = teamKeyInfo!["key"].string
                                        if let teamBasicDetails = teamInfo[teamKey!]{

                                            let teamShortName = teamBasicDetails["team_short_name"].string ?? ""
                                            let teamFullName = teamBasicDetails["team_name"].string ?? ""

                                            let teamFlag = teamBasicDetails["team_flag"].string ?? ""
                                            
                                            let teamTotalScore = run + "( " + wicket + " wkts," + overs + " ov)"
                                            let teamTotalTitleScore = run + "/" + wicket + "(" + overs + " ov)"
                                            
                                            if didNotBat.count == 0{
                                                didNotBat = "N/A"
                                            }
                                            let details: [String: Any] = ["team_short_name": teamShortName, "team_flag": teamFlag, "team_score": fequiredFormat, "team_name": teamFullName, "teamTotalScore": teamTotalScore, "teamTotalTitleScore": teamTotalTitleScore, "batsman_array":  playerBatsmanArray, "bowler_array":  playerbowlerArray, "extra": extraRunsStr, "fall_of_wickets": fallOfWickets, "didNotBat": didNotBat]
                                            
                                            
                                            if (overs == "0") || (overs == "0.0"){
                                                
                                            }
                                            else{
                                                teamDetailsArray.append(details)
                                            }
                                        }
                                    }
                                }
                                
                                if teamDetailsArray.count == 1{
                                   
                                    let details = teamDetailsArray[0]
                                    liveDetails.firstTeamShortName = details["team_short_name"] as! String;
                                    liveDetails.firstTeamFullName = details["team_name"] as! String;
                                    liveDetails.firstTeamFlagName = details["team_flag"] as! String;
                                    liveDetails.firstTeamScoreDetails = details["team_score"] as! String;
                                    liveDetails.firstTeamTotalScore = details["teamTotalScore"] as! String;
                                    liveDetails.firstTeamTotalTitleScore = details["teamTotalTitleScore"] as! String;
                                    liveDetails.firstTeamExtraScore = details["extra"] as! String;
                                    liveDetails.firstTeamFallOfWickets = details["fall_of_wickets"] as! String;
                                    liveDetails.firstTeamDoNotBats = details["didNotBat"] as! String;

                                    liveDetails.firstTeamBatsmanArray = details["batsman_array"] as! Array<LiveScoreMathchPlayerDetails>
                                    liveDetails.firstTeamBowlerArray = details["bowler_array"] as! Array<LiveScoreMathchPlayerDetails>
                                    liveScoreDetailsArray.append(liveDetails)
                                }
                                else if teamDetailsArray.count == 2{
                                    
                                    // First team Details
                                    let firsrdetails = teamDetailsArray[1]
                                    
                                    liveDetails.secondTeamName = firsrdetails["team_short_name"] as! String;
                                    liveDetails.secondTeamFullName = firsrdetails["team_name"] as! String;
                                    liveDetails.secondTeamFlagName = firsrdetails["team_flag"] as! String;
                                    liveDetails.secondTeamScoreDetails = firsrdetails["team_score"] as! String;
                                    liveDetails.secondTeamTotalScore = firsrdetails["teamTotalScore"] as! String;
                                    liveDetails.secondTeamTotalTitleScore = firsrdetails["teamTotalTitleScore"] as! String;
                                    liveDetails.secondTeamExtraScore = firsrdetails["extra"] as! String;
                                    liveDetails.secondTeamFallOfWickets = firsrdetails["fall_of_wickets"] as! String;
                                    liveDetails.secondTeamDoNotBats = firsrdetails["didNotBat"] as! String;

                                    liveDetails.secodsTeamBatsmanArray = firsrdetails["batsman_array"] as! Array<LiveScoreMathchPlayerDetails>
                                    liveDetails.secondTeamBowlerArray = firsrdetails["bowler_array"] as! Array<LiveScoreMathchPlayerDetails>
                                    

                                    // Second team Details
                                    let secondDetails = teamDetailsArray[0]
                                    
                                    liveDetails.firstTeamShortName = secondDetails["team_short_name"] as! String;
                                    liveDetails.firstTeamFullName = secondDetails["team_name"] as! String;
                                    liveDetails.firstTeamFlagName = secondDetails["team_flag"] as! String;
                                    liveDetails.firstTeamScoreDetails = secondDetails["team_score"] as! String;
                                    liveDetails.firstTeamTotalScore = secondDetails["teamTotalScore"] as! String;
                                    liveDetails.firstTeamTotalTitleScore = secondDetails["teamTotalTitleScore"] as! String;
                                    liveDetails.firstTeamExtraScore = secondDetails["extra"] as! String;
                                    liveDetails.firstTeamFallOfWickets = secondDetails["fall_of_wickets"] as! String;
                                    liveDetails.firstTeamDoNotBats = secondDetails["didNotBat"] as! String;
                                    liveDetails.firstTeamBatsmanArray = secondDetails["batsman_array"] as! Array<LiveScoreMathchPlayerDetails>
                                    liveDetails.firstTeamBowlerArray = secondDetails["bowler_array"] as! Array<LiveScoreMathchPlayerDetails>
                                    liveScoreDetailsArray.append(liveDetails)
                                }
                            }
                        }
                    }
                }
                else if apiType == "3"{

                    if (matchType == "one-day") || (matchType == "t20") || (matchType == "t10"){
                        
                        if let matchSummery = response!["match_summery"]?.dictionary{
                            var teamAkey = ""
//                            var teamBkey = ""

                            if let teamDetails = matchSummery["teama"]?.dictionary{
                                teamAkey = teamDetails["team_id"]?.stringValue ?? ""
                            }

                            if let inningsArray = matchSummery["innings"]?.array{
                                for inningDetails in inningsArray {
                                    var batsmanArray = Array<LiveScoreMathchPlayerDetails>()
                                    
                                    if let batsmanDetailsArray = inningDetails["batsmen"].array{
                                        for details in batsmanDetailsArray {
                                            let livePlyerDetails = LiveScoreMathchPlayerDetails()
                                            livePlyerDetails.playerName = details["name"].string ?? ""
                                            
                                            var runsScored = details["runs"].stringValue
                                            var sixesScored = details["sixes"].stringValue
                                            var foursScored = details["fours"].stringValue
                                            var ballsFaced = details["balls_faced"].stringValue
                                            let howout = details["how_out"].string ?? ""
                                            let striceRateStr = details["strike_rate"].stringValue

                                            if runsScored == "" {
                                                runsScored = "0"
                                            }

                                            if sixesScored == "" {
                                                sixesScored = "0"
                                            }

                                            if foursScored == "" {
                                                foursScored = "0"
                                            }

                                            if ballsFaced == "" {
                                                ballsFaced = "0"
                                            }
                                            
                                            livePlyerDetails.batsmanStatusMessage = howout
                                            livePlyerDetails.battingRun = runsScored;
                                            livePlyerDetails.battingSixes = sixesScored;
                                            livePlyerDetails.battingFours = foursScored;
                                            livePlyerDetails.battingBowl = ballsFaced;
                                            livePlyerDetails.batsmanStrikeRate = striceRateStr;
                                            batsmanArray.append(livePlyerDetails)
                                        }
                                    }
                                    
                                    var bowlerArray = Array<LiveScoreMathchPlayerDetails>()

                                    if let bowlerDetailsArray = inningDetails["bowlers"].array{
                                        for details in bowlerDetailsArray {
                                        
                                            let livePlyerDetails = LiveScoreMathchPlayerDetails()
                                            let playerName = details["name"].string ?? ""
                                            
                                            let runsConceded = details["runs_conceded"].stringValue
                                            let oversBowled = details["overs"].stringValue
                                            let wicketsTaken = details["wickets"].stringValue
                                            let wides = details["wides"].stringValue
                                            let noBalls = details["noballs"].stringValue
                                            let maidensBowled = details["maidens"].stringValue
                                            let ecoRateStr = details["econ"].stringValue
                                            
                                            livePlyerDetails.playerName = playerName
                                            livePlyerDetails.bowlerRunConceded = runsConceded
                                            livePlyerDetails.bowlerOverBowled = oversBowled
                                            livePlyerDetails.bowlerWicketTaken = wicketsTaken
                                            livePlyerDetails.bowlerWideBalls = wides
                                            livePlyerDetails.bowlerNoBowl = noBalls
                                            livePlyerDetails.bowlerMaindenOvers = maidensBowled
                                            livePlyerDetails.bowlerEcoRate = ecoRateStr;
                                            bowlerArray.append(livePlyerDetails)
                                        }
                                    }
                                    var didNotBatNames = ""
                                    var fowNames = ""

                                    if let fallOfWickts = inningDetails["fows"].array{
                                        
                                        for fowDict in fallOfWickts {
                                            let name = fowDict["name"].string ?? ""
                                            if fowNames.count == 0 {
                                                fowNames = name
                                            }
                                            else{
                                                fowNames = fowNames + ",\(name)"
                                            }
                                        }

                                    }
                                    
                                    if let didNotBatArray = inningDetails["did_not_bat"].array{

                                        for didNotBatDict in didNotBatArray {
                                            let name = didNotBatDict["name"].string ?? ""
                                            if didNotBatNames.count == 0 {
                                                didNotBatNames = name
                                            }
                                            else{
                                                didNotBatNames = didNotBatNames + ",\(name)"
                                            }
                                        }
                                    }
                                    var totalRuns = ""
                                    var totalRunsWithOver = ""

                                    if let equationsDict = inningDetails["equations"].dictionary{
                                        let wickets = equationsDict["wickets"]?.stringValue ?? "0"
                                        let runs = equationsDict["runs"]?.stringValue ?? "0"
                                        let overs = equationsDict["overs"]?.stringValue ?? "0"
                                        totalRuns = "\(runs)/\(wickets)"
                                        totalRunsWithOver = "\(totalRuns)(\(overs))"
                                    }

                                    var extraRauns = ""
                                    
                                    if let extraDict = inningDetails["extra_runs"].dictionary{
                                        var wides = extraDict["wides"]?.stringValue ?? "0"
                                        if wides == "" {
                                            wides = "0"
                                        }
                                        var penalty = extraDict["penalty"]?.stringValue ?? "0"
                                        if penalty == "" {
                                            penalty = "0"
                                        }

                                        var legbyes = extraDict["legbyes"]?.stringValue ?? "0"
                                        if legbyes == "" {
                                            legbyes = "0"
                                        }

                                        var byes = extraDict["byes"]?.stringValue ?? "0"
                                        if byes == "" {
                                            byes = "0"
                                        }

                                        var noballs = extraDict["noballs"]?.stringValue ?? "0"
                                        if noballs == "" {
                                            noballs = "0"
                                        }

                                        var total = extraDict["total"]?.stringValue ?? "0"
                                        if total == "" {
                                            total = "0"
                                        }

                                        extraRauns = "\(total)(b\(byes),lb\(legbyes),w\(wides),nb\(noballs),p\(penalty)"
                                    }

                                    let teamShortName = inningDetails["short_name"].string ?? ""
                                    let teamName = inningDetails["name"].string ?? ""

                                    let battingTeamTd = inningDetails["batting_team_id"].stringValue
                                    if battingTeamTd == teamAkey {
                                        liveDetails.firstTeamBatsmanArray = batsmanArray
                                        liveDetails.firstTeamBowlerArray = bowlerArray
                                        liveDetails.firstTeamDoNotBats = didNotBatNames
                                        liveDetails.firstTeamExtraScore = extraRauns
                                        liveDetails.firstTeamTotalScore = totalRuns
                                        liveDetails.firstTeamScoreDetails = totalRuns
                                        liveDetails.firstTeamTotalTitleScore = totalRunsWithOver
                                        liveDetails.firstTeamShortName = teamShortName
                                        liveDetails.firstTeamFullName = teamName
                                        liveDetails.firstTeamFallOfWickets = fowNames
                                    }
                                    else{
                                        liveDetails.secodsTeamBatsmanArray = batsmanArray
                                        liveDetails.secondTeamBowlerArray = bowlerArray
                                        liveDetails.secondTeamDoNotBats = didNotBatNames
                                        liveDetails.secondTeamExtraScore = extraRauns
                                        liveDetails.secondTeamTotalScore = totalRunsWithOver
                                        liveDetails.secondTeamScoreDetails = totalRuns
                                        liveDetails.secondTeamTotalTitleScore = totalRunsWithOver
                                        liveDetails.secondTeamName = teamShortName
                                        liveDetails.secondTeamFullName = teamShortName
                                        liveDetails.secondTeamFallOfWickets = fowNames
                                    }
                                    liveScoreDetailsArray.append(liveDetails)
                                }
                            }
                        }
                    }
                }

            }
        }
        
        return liveScoreDetailsArray
    }
    
    
    
    class func getFallOfWicketsForOcta(fallofWicketJson: [String: JSON], playing22: JSON) -> String{
        var fallOfwicketStr = ""
        
        if let fallofWicketArray = fallofWicketJson["FallofWicket"]?.array {
            
            for details in fallofWicketArray{
                
                if let attributes = details["@attributes"].dictionary{
                    
                    let order = attributes["order"]?.string ?? "0"
                    let overBall = attributes["over_ball"]?.string ?? "0"
                    let playerId = attributes["player_id"]?.string ?? "0"
                    let runs = attributes["runs"]?.string ?? "0"
                    
                    let palyerDetails = playing22[playerId].dictionary
                    let playerName = palyerDetails!["player_name"]?.string ?? ""
                    
                    let requiredStr = runs + "/" + order + "(" + playerName + " ," + overBall + ")"
                    
                    if fallOfwicketStr.count == 0{
                        fallOfwicketStr = requiredStr
                    }
                    else{
                        fallOfwicketStr = fallOfwicketStr + "," + requiredStr
                    }
                }
            }
        }
        
        return fallOfwicketStr
    }
    
    
    class func getExtraForOcta(extraJson: [String: JSON]) -> String{
        var extraRunsStr = ""
        
        if let extraDetails = extraJson["@attributes"]?.dictionary {
            
            let byes = extraDetails["byes"]?.string ?? "0"
            let legByes = extraDetails["legByes"]?.string ?? "0"
            let noBalls = extraDetails["no_balls"]?.string ?? "0"
            let penalties = extraDetails["penalties"]?.string ?? "0"
            let totalExtras = extraDetails["total_extras"]?.string ?? "0"
            let wides = extraDetails["wides"]?.string ?? "0"
            
            extraRunsStr = totalExtras + "(b " + byes + ", lb " + legByes + ", w " + wides + ", nb " + noBalls + ", p " + penalties + ")"
        }
        
        return extraRunsStr
    }
    
    class func getBatsmanArray(batsmanDict: [String: JSON], playing22: JSON) -> (Array<LiveScoreMathchPlayerDetails>, String){
        
        var batsmanDetailsArray = Array<LiveScoreMathchPlayerDetails>()
        var doNotPlaye = ""

        if let batsmanArray = batsmanDict["Batsman"]?.array{
            for batsmanDetails in batsmanArray{
                if let batsmanAttributsDetails = batsmanDetails["@attributes"].dictionary{
                    
                    let batsmanId = batsmanAttributsDetails["id"]?.string ?? ""
                    let howOut = batsmanAttributsDetails["how_out"]?.string ?? ""
                    if let playerDetails = playing22[batsmanId].dictionary{
                        let playerName = playerDetails["player_name"]?.string ?? ""
                        
                        if howOut.count == 0{
                            if doNotPlaye.count == 0{
                                doNotPlaye = playerName
                            }
                            else{
                                doNotPlaye = doNotPlaye + "," + playerName
                            }
                        }
                        else{
                            
                            let livePlyerDetails = LiveScoreMathchPlayerDetails()
                            livePlyerDetails.playerName = playerName
                            let runsScored = batsmanAttributsDetails["runs_scored"]?.string ?? "0"
                            let sixesScored = batsmanAttributsDetails["sixes_scored"]?.string ?? "0"
                            let foursScored = batsmanAttributsDetails["fours_scored"]?.string ?? "0"
                            let ballsFaced = batsmanAttributsDetails["balls_faced"]?.string ?? "0"
                            
                            livePlyerDetails.battingRun = runsScored;
                            livePlyerDetails.battingSixes = sixesScored;
                            livePlyerDetails.battingFours = foursScored;
                            livePlyerDetails.battingBowl = ballsFaced;
                            
                            let striceRate = Float(runsScored)!*100.0 / Float(ballsFaced)!
                            let striceRateStr = String(format: "%.2f", striceRate)
                            livePlyerDetails.batsmanStrikeRate = striceRateStr;
                            
                            let dismissalId = batsmanAttributsDetails["dismissal_id"]?.string
                            
                            if dismissalId == "0"{
                                livePlyerDetails.batsmanStatusMessage = "Not Out"
                            }
                            else if dismissalId == "1"{
                                let bowledBy = batsmanAttributsDetails["bowled_by"]?.string ?? "0"
                                if let playerDetails = playing22[bowledBy].dictionary{
                                    let playerName = playerDetails["player_name"]?.string ?? ""
                                    livePlyerDetails.batsmanStatusMessage = "b " + playerName
                                }
                            }
                            else if dismissalId == "2"{
                                
                                let bowledBy = batsmanAttributsDetails["bowled_by"]?.string ?? "0"
                                if let playerDetails = playing22[bowledBy].dictionary{
                                    let playerName = playerDetails["player_name"]?.string ?? ""
                                    livePlyerDetails.batsmanStatusMessage = "c & b " + playerName
                                }
                            }
                            else if dismissalId == "3"{
                                let bowledBy = batsmanAttributsDetails["bowled_by"]?.string ?? "0"
                                if let bowledByPlayerDetails = playing22[bowledBy].dictionary{
                                    let bowledByPlayerName = bowledByPlayerDetails["player_name"]?.string ?? ""
                                    let caughtBy = batsmanAttributsDetails["caught_by"]?.string ?? "0"
                                    if let caughtByPlayerDetails = playing22[caughtBy].dictionary{
                                        let caughtByPlayerName = caughtByPlayerDetails["player_name"]?.string ?? ""
                                        livePlyerDetails.batsmanStatusMessage = "c " + caughtByPlayerName + " b" + bowledByPlayerName
                                    }
                                }
                            }
                            else if (dismissalId == "4") || (dismissalId == "5") || (dismissalId == "6") || (dismissalId == "8") || (dismissalId == "9") || (dismissalId == "10") || (dismissalId == "11") || (dismissalId == "13") || (dismissalId == "15") || (dismissalId == "16") || (dismissalId == "17") || (dismissalId == "18"){
                                livePlyerDetails.batsmanStatusMessage = howOut
                            }
                            else if dismissalId == "7"{
                                let bowledBy = batsmanAttributsDetails["bowled_by"]?.string ?? "0"
                                if let playerDetails = playing22[bowledBy].dictionary{
                                    let playerName = playerDetails["player_name"]?.string ?? ""
                                    livePlyerDetails.batsmanStatusMessage = "lbw b " + playerName
                                }
                            }
                            else if dismissalId == "12"{
                                let bowledBy = batsmanAttributsDetails["bowled_by"]?.string ?? "0"
                                var bowledByPlayerName = ""
                                if let bowledByPlayerDetails = playing22[bowledBy].dictionary{
                                    bowledByPlayerName = bowledByPlayerDetails["player_name"]?.string ?? ""
                                }
                                let caughtBy = batsmanAttributsDetails["caught_by"]?.string ?? "0"
                                var caughtByPlayerName = ""
                                
                                if let caughtByPlayerDetails = playing22[caughtBy].dictionary{
                                    caughtByPlayerName = caughtByPlayerDetails["player_name"]?.string ?? ""
                                }
                                
                                livePlyerDetails.batsmanStatusMessage = "st " + caughtByPlayerName + "b " + bowledByPlayerName
                            }
                            else if dismissalId == "14"{
                                
                                let bowledBy = batsmanAttributsDetails["bowled_by"]?.string ?? "0"
                                if let bowledByPlayerDetails = playing22[bowledBy].dictionary{
                                    let bowledByPlayerName = bowledByPlayerDetails["player_name"]?.string ?? ""
                                    livePlyerDetails.batsmanStatusMessage = "c(Sub) " + bowledByPlayerName + "b " + bowledByPlayerName
                                }
                            }
                            
                            batsmanDetailsArray.append(livePlyerDetails)
                        }
                    }
                }
            }
        }
        
        return (batsmanDetailsArray, doNotPlaye)
    }
    
    
    class func getBowlerArrayFromOcta(bowlerDict: [String: JSON], playing22: JSON) -> Array<LiveScoreMathchPlayerDetails>{
        
        var bowlerDetailsArray = Array<LiveScoreMathchPlayerDetails>()        
        if let bowlerArray = bowlerDict["Bowler"]?.array{
            
            for bolwerDetails in bowlerArray{
                
                if let bowlerAttributsDetails = bolwerDetails["@attributes"].dictionary{
                    let bowlerId = bowlerAttributsDetails["id"]?.string ?? ""
                    let playerDetails = playing22[bowlerId].dictionary
                    let playerName = playerDetails!["player_name"]?.string ?? ""
                    
                    let livePlyerDetails = LiveScoreMathchPlayerDetails()
                    let runsConceded = bowlerAttributsDetails["runs_conceded"]?.string ?? "0"
                    let oversBowled = bowlerAttributsDetails["overs_bowled"]?.string ?? "0"
                    let wicketsTaken = bowlerAttributsDetails["wickets_taken"]?.string ?? "0"
                    let wides = bowlerAttributsDetails["wides"]?.string ?? "0"
                    let noBalls = bowlerAttributsDetails["no_balls"]?.string ?? "0"
                    let maidensBowled = bowlerAttributsDetails["maidens_bowled"]?.string ?? "0"
                    let ballsBowled = bowlerAttributsDetails["balls_bowled"]?.string ?? "0"
                    
                    livePlyerDetails.playerName = playerName
                    livePlyerDetails.bowlerRunConceded = runsConceded
                    livePlyerDetails.bowlerOverBowled = oversBowled
                    livePlyerDetails.bowlerWicketTaken = wicketsTaken
                    livePlyerDetails.bowlerWideBalls = wides
                    livePlyerDetails.bowlerNoBowl = noBalls
                    livePlyerDetails.bowlerMaindenOvers = maidensBowled
                    livePlyerDetails.bowlerBallsBowled = ballsBowled
                    
                    let ecoRate = Float(livePlyerDetails.bowlerRunConceded)! / Float(livePlyerDetails.bowlerOverBowled)!
                    
                    let ecoRateStr = String(format: "%.2f", ecoRate)
                    
                    livePlyerDetails.bowlerEcoRate = ecoRateStr;
                    
                    bowlerDetailsArray.append(livePlyerDetails)
                }
            }
        }
        
        return bowlerDetailsArray
    }
    
    
    
    class func getBowlerArrayFromCricketAPI(bowlerDict: [String: JSON], playing22: JSON) -> Array<LiveScoreMathchPlayerDetails>{
        
        var bowlerDetailsArray = Array<LiveScoreMathchPlayerDetails>()
        if let bowlerArray = bowlerDict["Batsman"]?.array{
            
            for bolwerDetails in bowlerArray{
                
                if let bowlerAttributsDetails = bolwerDetails["@attributes"].dictionary{
                    let bowlerId = bowlerAttributsDetails["id"]?.string ?? ""
                    let playerDetails = playing22[bowlerId].dictionary
                    let playerName = playerDetails!["player_name"]?.string ?? ""
                    
                    let livePlyerDetails = LiveScoreMathchPlayerDetails()
                    let runsConceded = bowlerAttributsDetails["runs_conceded"]?.string ?? "0"
                    let oversBowled = bowlerAttributsDetails["overs_bowled"]?.string ?? "0"
                    let wicketsTaken = bowlerAttributsDetails["wickets_taken"]?.string ?? "0"
                    let wides = bowlerAttributsDetails["wides"]?.string ?? "0"
                    let noBalls = bowlerAttributsDetails["no_balls"]?.string ?? "0"
                    let maidensBowled = bowlerAttributsDetails["maidens_bowled"]?.string ?? "0"
                    let ballsBowled = bowlerAttributsDetails["balls_bowled"]?.string ?? "0"
                    
                    livePlyerDetails.playerName = playerName
                    livePlyerDetails.bowlerRunConceded = runsConceded
                    livePlyerDetails.bowlerOverBowled = oversBowled
                    livePlyerDetails.bowlerWicketTaken = wicketsTaken
                    livePlyerDetails.bowlerWideBalls = wides
                    livePlyerDetails.bowlerNoBowl = noBalls
                    livePlyerDetails.bowlerMaindenOvers = maidensBowled
                    livePlyerDetails.bowlerBallsBowled = ballsBowled
                    
                    bowlerDetailsArray.append(livePlyerDetails)
                }
            }
        }
        
        return bowlerDetailsArray
    }
    
}




