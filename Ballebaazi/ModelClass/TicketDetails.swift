//
//  TicketDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 28/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class TicketDetails: NSObject {

    var ticketTitle = ""
    var leagueId = ""
    var matchName = ""
    var matchKey = ""
    var ticketType = ""
    var ticketExpiry = ""
    var joiningAmount = ""
    var gameType = ""
    var startDateUnix = ""
    var matchShortName = ""
    var leagueName = ""


    var leagueCategory = ""
    var ticketId = ""
    var fantasyType = ""
    
    var templateID = ""
    var totalPass: Float = 0

    var isFromTicketScreen = false

    
    class func getTicketDetails(dataArray: Array<JSON>) -> (Array<TicketDetails>, Bool) {
        var ticketArray = Array<TicketDetails>()
        var isAnyMatchAvailable = false
        
        for details in dataArray {
            let ticketDetals = TicketDetails.parseTicketDetails(details)
            if ticketDetals.ticketType == "2"{
                isAnyMatchAvailable = true
            }
            ticketArray.append(ticketDetals)
        }
        
        return (ticketArray, isAnyMatchAvailable)
    }
    
    class func parseTicketDetails(_ details: JSON) -> TicketDetails {
        let ticketDetals = TicketDetails()
        ticketDetals.ticketTitle = details["ticket_title"].string ?? ""
        
        ticketDetals.leagueCategory = details["league_category"].string ?? ""
        ticketDetals.ticketId = details["ticket_id"].string ?? ""
        ticketDetals.fantasyType = details["fantasy_type"].string ?? "1"
        if ticketDetals.fantasyType == "" {
            ticketDetals.fantasyType = "1"
        }
        ticketDetals.leagueId = details["league_id"].string ?? ""
        ticketDetals.matchName = details["match_name"].string ?? ""
        ticketDetals.matchKey = details["match_key"].string ?? ""
        ticketDetals.ticketType = details["ticket_type"].string ?? ""
        ticketDetals.matchShortName = details["match_short_name"].string ?? ""
        let totalPass = details["total_passes"].stringValue
        ticketDetals.totalPass = Float(totalPass) ?? 0

        let joiningAmout = details["joining_amount"].string ?? ""
        ticketDetals.joiningAmount = String(Int((Float(joiningAmout) ?? 0)))
                    
        ticketDetals.leagueName = details["league_name"].string ?? ""
        ticketDetals.gameType = details["play_type"].stringValue
        ticketDetals.startDateUnix = details["start_date_unix"].stringValue

        ticketDetals.ticketExpiry = details["ticket_expiry"].string ?? ""
        return ticketDetals
    }
}
