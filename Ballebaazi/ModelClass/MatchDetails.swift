//
//
//  MatchDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/12/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class MatchDetails: NSObject {

    var seasonKey: String?
    lazy var seasonShortName = ""
    var firstTeamKey = ""
    var secondTeamKey = ""

    var firstTeamName = ""
    var secondTeamName = ""
    
    var hostName = ""
    var city = ""
    var stadium = ""
    var country = ""
    var firstTeamRank = ""
    var secondTeamRank = ""
    var information = ""
    var tossWinner = ""

    
    var customText = ""


    
    
    var matchKey = ""
    var active = ""
    var matchName = ""

    var matchFantasyType = ""

    var matchShortName: String?
    var startDateTimestemp: String?
    var firstTeamShortName: String?
    var secondTeamShortName: String?
    var matchStatus: String = ""
    var matchType: String?
    var matchResult: String?
    var totalContestJoined = "0"
    var isMatchClosed = false
    var firstTeamImageUrl = ""
    var secondTeamImageUrl = ""
    var firstTeamLiveScoreShortName = ""
    var secondTeamLiveScoreShortName = ""
    var firstTeamLiveScore = ""
    var secondTeamLiveScore = ""
    var thirdTeamLiveScore = ""
    var fourthTeamLiveScore = ""
    var firstTeamOver = ""
    var secondTeamOver = ""
    var thirdTeamOver = ""
    var fourthTeamOver = ""
    
    var isViewForTestMatch = false
    var adminStatus = ""
    var show_playing22 = "0"
    var templateID = ""
    var playersGender = ""
    var imageName = ""
    var activePlayers = ""
    var startDateIndia = ""
    var matchViewName = ""
    var quizCloseInerval: Double = 0
    
    var isTicketAvailable = false
    var isPassAvailable = false
    var isMatchTourney = false
    var isQuizMatch = false
    var matchIndex = 0
    var closingTs: Double = 0

    
    class func parseQuizDetails(details: JSON) -> MatchDetails {

        let matchDetails = MatchDetails()
        matchDetails.matchType = details["match_format"].string ?? ""
        matchDetails.isQuizMatch = true
        
        matchDetails.matchShortName = details["match_short_name"].string
        matchDetails.matchName = details["match_name"].string ?? ""
        matchDetails.active = details["active"].string ?? ""
        matchDetails.matchKey = details["match_key"].string ?? ""
        matchDetails.activePlayers = details["active_player"].string ?? "0"
        
        let startDateIndia = details["start_date_india"].string ?? ""
        
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
        if let date = dfmatter.date(from: startDateIndia){
            let dateStamp = date.timeIntervalSince1970
            matchDetails.startDateIndia = String(dateStamp)
        }
        
        matchDetails.matchViewName = details["match_view_name"].string ?? ""
        let quizCloseInerval = details["closed"].string ?? "0"
        matchDetails.quizCloseInerval = Double(quizCloseInerval) ?? 0
        let matchIndex = details["match_show_index"].stringValue
        var matchIndexInt = Int(matchIndex) ?? 0
        if matchIndexInt > 0 {
            matchIndexInt = matchIndexInt - 1
        }
        
        matchDetails.matchIndex = matchIndexInt
        matchDetails.startDateTimestemp = "0"
        if let startTime = details["start_date_unix"].string{
            let closingTime = details["closing_ts"].intValue
            matchDetails.closingTs = Double(closingTime)
            matchDetails.startDateTimestemp = String(startTime)
        }
        
        if let matchImage = details["match_image"].string{
            matchDetails.imageName = UserDetails.sharedInstance.teamImageUrl + matchImage
        }

        return matchDetails
    }
    
    
    class func getAllQuizArray(dataArray: [JSON], isNeedToSort: Bool) -> [MatchDetails] {
        var matchDataArray = Array<MatchDetails>()
        
        for details in dataArray {
            let matchDetails = MatchDetails.parseQuizDetails(details: details)
            matchDataArray.append(matchDetails)
        }
            
        if isNeedToSort {
            matchDataArray.sort { (nextMatchDetails, details) -> Bool in
                nextMatchDetails.matchIndex < details.matchIndex
            }
        }
        return matchDataArray
    }
    
    class func getAllMatchDetails(responseResult: JSON) -> Array<MatchDetails> {
       
        var matchDetailsArray = Array<MatchDetails>()
        if !UserDetails.sharedInstance.isLiveServerUpdate{
            let serverTimestemp = responseResult.dictionary!["server_timestamp"]?.stringValue
            UserDetails.sharedInstance.serverTimeStemp = serverTimestemp ?? "0"
        }
        
        let response =  responseResult.dictionary!["response"]
        let matchList = response?.dictionary!["matches"]?.array
        if let seriesList = response?.dictionary!["seasons"]?.array{
            UserDetails.sharedInstance.allSeriesList = SeriesDetails.getSeriesDetails(allSeriesList: seriesList)
        }
        
        let filePaths =  responseResult.dictionary!["file_path"]
        UserDetails.sharedInstance.userImageUrl = filePaths?.dictionary!["user_images"]?.string ?? ""
        UserDetails.sharedInstance.teamImageUrl = (filePaths?.dictionary!["team_images"]?.string)!
        UserDetails.sharedInstance.promotionImageUrl = filePaths?.dictionary!["promotion_images"]?.string ?? ""
        UserDetails.sharedInstance.panImageUrl = filePaths?.dictionary!["pan_images"]?.string ?? ""
        UserDetails.sharedInstance.bankImageUrl = filePaths?.dictionary!["bank_images"]?.string ?? ""
        UserDetails.sharedInstance.bannersFilePath = filePaths?.dictionary!["promotion_images"]?.string ?? ""
        UserDetails.sharedInstance.bankImageUrl = filePaths?.dictionary!["aadhaar_images"]?.string ?? ""
        UserDetails.sharedInstance.teams_pdf = filePaths?.dictionary!["teams_pdf"]?.string ?? ""

        if matchList != nil {
            for details in matchList! {
                
                let matchDetails = MatchDetails()
                matchDetails.seasonKey = details["season_key"].string
                matchDetails.seasonShortName = details["season_short_name"].string ?? ""
                matchDetails.matchKey = details["match_key"].string ?? ""
                matchDetails.firstTeamKey = details["team_a_key"].string ?? "0"
                matchDetails.secondTeamKey = details["team_b_key"].string ?? "0"
                matchDetails.active = details["active"].stringValue 
                matchDetails.playersGender = details["gender_match_category"].string ?? ""
                matchDetails.matchShortName = details["match_short_name"].string
                matchDetails.customText = details["custom_text"].string ?? ""
                
                matchDetails.firstTeamName = details["team_a_name"].string ?? ""
                matchDetails.secondTeamName = details["team_b_name"].string ?? ""
                matchDetails.hostName = details["host_name"].string ?? ""
                matchDetails.city = details["city"].string ?? ""
                matchDetails.stadium = details["stadium"].string ?? ""
                matchDetails.country = details["country"].string ?? ""
                matchDetails.firstTeamRank = details["team_a_rank"].string ?? ""
                matchDetails.secondTeamRank = details["team_b_rank"].string ?? ""
                matchDetails.information = details["information"].string ?? ""
                matchDetails.tossWinner = details["toss_winner"].string ?? ""


                
                matchDetails.adminStatus = details["admin_status"].stringValue
                matchDetails.matchFantasyType = details["match_fantasy_type"].string ?? ""

                let matchClosedStatus = details["closed"].string

                if matchClosedStatus == "0"{
                    matchDetails.isMatchClosed = false
                }
                else{
                    matchDetails.isMatchClosed = true
                }
                if let matchStatus = details["match_status"].string{
                    matchDetails.matchStatus = matchStatus
                }
                
                if let matchResult = details["match_result"].string{
                    matchDetails.matchResult = matchResult
                }

                matchDetails.show_playing22 = details["show_playing22"].string ?? "0"

                matchDetails.firstTeamShortName = details["team_a_short_name"].string
                matchDetails.secondTeamShortName = details["team_b_short_name"].string
                matchDetails.matchType = details["match_format"].string
                
                if let startTime = details["start_date_unix"].string{
                    var closingTime = details["closing_ts"].intValue
                    if closingTime == 0{
                        closingTime = UserDetails.sharedInstance.closingTimeForMatch
                    }
//                    matchDetails.closingTs = Double(closingTime)
//                    let calcultedTime = Int(startTime)! - closingTime
//                    matchDetails.startDateTimestemp = String(calcultedTime)
                    matchDetails.startDateTimestemp = startTime
                }
                
                if let firstTeamImageName = details["team_a_flag"].string{
                    matchDetails.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                }
                
                if let secondTeamImageName = details["team_b_flag"].string{
                    matchDetails.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                }
                
                if let matchImage = details["match_image"].string{
                    matchDetails.imageName = UserDetails.sharedInstance.teamImageUrl + matchImage
                }
                
                if matchDetails.active == "4"{
                    matchDetails.isMatchClosed = false
                }
                
                if matchDetails.matchType?.lowercased() == "Tourney".lowercased() {
                    matchDetails.isMatchTourney = true
                }
                matchDetailsArray.append(matchDetails)
            }
        }
        
        return matchDetailsArray
    }
    
    
    class func getSelectedMatchDetails(responseResult: JSON) -> MatchDetails? {
        
        let response =  responseResult.dictionary!["response"]
        if let details = response!["selected_match"].dictionary{
            
            let matchDetails = MatchDetails()
            matchDetails.seasonKey = details["season_key"]?.string
            matchDetails.seasonShortName = details["season_short_name"]?.string ?? ""
            matchDetails.matchKey = details["match_key"]?.string ?? ""
            matchDetails.matchShortName = details["match_short_name"]?.string
            matchDetails.firstTeamShortName = details["team_a_short_name"]?.string
            matchDetails.secondTeamShortName = details["team_b_short_name"]?.string
            matchDetails.matchType = details["match_format"]?.string
            let matchClosedStatus = details["closed"]?.string
            matchDetails.adminStatus = details["admin_status"]?.stringValue ?? "0"
            matchDetails.show_playing22 = details["show_playing22"]?.string ?? "0"
            matchDetails.customText = details["custom_text"]?.string ?? ""

            matchDetails.firstTeamKey = details["team_a_key"]?.string ?? "0"
            matchDetails.secondTeamKey = details["team_b_key"]?.string ?? "0"
            matchDetails.playersGender = details["gender_match_category"]?.string ?? ""
            matchDetails.active = details["active"]?.stringValue ?? ""
            matchDetails.matchFantasyType = details["match_fantasy_type"]?.string ?? ""
            matchDetails.firstTeamName = details["team_a_name"]?.string ?? ""
            matchDetails.secondTeamName = details["team_b_name"]?.string ?? ""
            matchDetails.hostName = details["host_name"]?.string ?? ""
            matchDetails.city = details["city"]?.string ?? ""
            matchDetails.stadium = details["stadium"]?.string ?? ""
            matchDetails.country = details["country"]?.string ?? ""
            matchDetails.firstTeamRank = details["team_a_rank"]?.string ?? ""
            matchDetails.secondTeamRank = details["team_b_rank"]?.string ?? ""
            matchDetails.information = details["information"]?.string ?? ""
            matchDetails.tossWinner = details["toss_winner"]?.string ?? ""


            if matchClosedStatus == "0"{
                matchDetails.isMatchClosed = false
            }
            else{
                matchDetails.isMatchClosed = true
            }

            if let startTime = details["start_date_unix"]?.string{
                var closingTime = details["closing_ts"]?.intValue ?? UserDetails.sharedInstance.closingTimeForMatch
                if closingTime == 0{
                    closingTime = UserDetails.sharedInstance.closingTimeForMatch
                }
//                matchDetails.closingTs = Double(closingTime)
//                let calcultedTime = Int(startTime)! - closingTime
//                matchDetails.startDateTimestemp = String(calcultedTime)
                
                matchDetails.startDateTimestemp = startTime
            }
            
            if let matchResult = details["match_result"]?.string{
                matchDetails.matchResult = matchResult
            }

            if let matchStatus = details["match_status"]?.string{
                matchDetails.matchStatus = matchStatus
            }

            if let firstTeamImageName = details["team_a_flag"]?.string{
                matchDetails.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
            }
            
            if let matchImage = details["match_image"]?.string{
                matchDetails.imageName = UserDetails.sharedInstance.teamImageUrl + matchImage
            }

            if let secondTeamImageName = details["team_b_flag"]?.string{
                matchDetails.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
            }
            if matchDetails.active == "4"{
                matchDetails.isMatchClosed = false
            }
            if matchDetails.matchType?.lowercased() == "Tourney".lowercased() {
                matchDetails.isMatchTourney = true
            }


            return matchDetails
        }
        
        return nil
    }
    
        
    class func getAllMatchDetailsForLeague(responseResult: JSON) -> (Array<MatchDetails>, Array<MatchDetails>, Array<MatchDetails>) {
        
        var liveMatchDetailsArray = Array<MatchDetails>()
        var upcomingMatchDetailsArray = Array<MatchDetails>()
        var completedMatchDetailsArray = Array<MatchDetails>()
        
        let response =  responseResult.dictionary!["response"]
        let seriesList = response?.dictionary!["seasons"]?.array
        UserDetails.sharedInstance.allMatchLeagueSeriesList = SeriesDetails.getSeriesDetails(allSeriesList: seriesList)
        
        if let matchDict = response?.dictionary!["matches"]?.dictionary{
            
            if let liveMatches = matchDict["live"]?.array{
               
                for details in liveMatches {
                    
                    let matchDetails = MatchDetails()
                    matchDetails.seasonKey = details["season_key"].string
                    matchDetails.seasonShortName = details["season_short_name"].string ?? ""
                    matchDetails.matchKey = details["match_key"].string ?? ""
                    matchDetails.active = details["active"].stringValue
                    matchDetails.matchFantasyType = details["match_fantasy_type"].string ?? ""
                    matchDetails.firstTeamName = details["team_a_name"].string ?? ""
                    matchDetails.secondTeamName = details["team_b_name"].string ?? ""
                    matchDetails.hostName = details["host_name"].string ?? ""
                    matchDetails.city = details["city"].string ?? ""
                    matchDetails.stadium = details["stadium"].string ?? ""
                    matchDetails.country = details["country"].string ?? ""
                    matchDetails.firstTeamRank = details["team_a_rank"].string ?? ""
                    matchDetails.customText = details["custom_text"].string ?? ""

                    matchDetails.secondTeamRank = details["team_b_rank"].string ?? ""
                    matchDetails.information = details["information"].string ?? ""
                    matchDetails.tossWinner = details["toss_winner"].string ?? ""


                    matchDetails.matchShortName = details["match_short_name"].string
                    matchDetails.firstTeamShortName = details["team_a_short_name"].string
                    matchDetails.secondTeamShortName = details["team_b_short_name"].string
                    let matchClosedStatus = details["closed"].string
                    matchDetails.adminStatus = details["admin_status"].stringValue
                    matchDetails.show_playing22 = details["show_playing22"].string ?? "0"
                    matchDetails.firstTeamKey = details["team_a_key"].string ?? "0"
                    matchDetails.secondTeamKey = details["team_b_key"].string ?? "0"
                    matchDetails.playersGender = details["gender_match_category"].string ?? ""

                    if matchClosedStatus == "0"{
                        matchDetails.isMatchClosed = false
                    }
                    else{
                        matchDetails.isMatchClosed = true
                    }

                    matchDetails.matchType = details["match_format"].string
                   
                    if let startTime = details["start_date_unix"].string{
                        var closingTime = details["closing_ts"].intValue
                        if closingTime == 0{
                            closingTime = UserDetails.sharedInstance.closingTimeForMatch
                        }

//                        matchDetails.closingTs = Double(closingTime)
//                        let calcultedTime = Int(startTime)! - closingTime
//                        matchDetails.startDateTimestemp = String(calcultedTime)
                        matchDetails.startDateTimestemp = startTime
                    }
                    
                    if let matchStatus = details["match_status"].string{
                        matchDetails.matchStatus = matchStatus
                    }

                    if let matchResult = details["match_result"].string{
                        matchDetails.matchResult = matchResult
                    }
                    
                    matchDetails.totalContestJoined = details["total_contest"].stringValue

                    if let firstTeamImageName = details["team_a_flag"].string{
                        matchDetails.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                    }
                    
                    if let secondTeamImageName = details["team_b_flag"].string{
                        matchDetails.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                    }
                    if matchDetails.active == "4"{
                        matchDetails.isMatchClosed = false
                    }
                    
                    if let matchImage = details["match_image"].string{
                        matchDetails.imageName = UserDetails.sharedInstance.teamImageUrl + matchImage
                    }

                    if matchDetails.matchType?.lowercased() == "Tourney".lowercased() {
                        matchDetails.isMatchTourney = true
                    }


                    liveMatchDetailsArray.append(matchDetails)
                }
            }
            
            if let upcomingsMatches = matchDict["upcomings"]?.array{
                
                for details in upcomingsMatches {

                    let matchDetails = MatchDetails()
                    matchDetails.seasonKey = details["season_key"].string
                    matchDetails.seasonShortName = details["season_short_name"].string ?? ""
                    matchDetails.matchKey = details["match_key"].string ?? ""
                    matchDetails.active = details["active"].stringValue
                    matchDetails.matchFantasyType = details["match_fantasy_type"].string ?? ""
                    matchDetails.customText = details["custom_text"].string ?? ""

                    matchDetails.matchShortName = details["match_short_name"].string
                    matchDetails.firstTeamShortName = details["team_a_short_name"].string
                    matchDetails.secondTeamShortName = details["team_b_short_name"].string
                    let matchClosedStatus = details["closed"].string
                    matchDetails.adminStatus = details["admin_status"].stringValue
                    matchDetails.show_playing22 = details["show_playing22"].string ?? "0"
                    matchDetails.firstTeamKey = details["team_a_key"].string ?? "0"
                    matchDetails.secondTeamKey = details["team_b_key"].string ?? "0"
                    matchDetails.playersGender = details["gender_match_category"].string ?? ""
                    matchDetails.matchFantasyType = details["match_fantasy_type"].string ?? ""
                    matchDetails.firstTeamName = details["team_a_name"].string ?? ""
                    matchDetails.secondTeamName = details["team_b_name"].string ?? ""
                    matchDetails.hostName = details["host_name"].string ?? ""
                    matchDetails.city = details["city"].string ?? ""
                    matchDetails.stadium = details["stadium"].string ?? ""
                    matchDetails.country = details["country"].string ?? ""
                    matchDetails.firstTeamRank = details["team_a_rank"].string ?? ""
                    matchDetails.secondTeamRank = details["team_b_rank"].string ?? ""
                    matchDetails.information = details["information"].string ?? ""
                    matchDetails.tossWinner = details["toss_winner"].string ?? ""


                    if matchClosedStatus == "0"{
                        matchDetails.isMatchClosed = false
                    }
                    else{
                        matchDetails.isMatchClosed = true
                    }

                    matchDetails.matchType = details["match_format"].string
                    
                    if let matchStatus = details["match_status"].string{
                        matchDetails.matchStatus = matchStatus
                    }
                    
                    if let matchImage = details["match_image"].string{
                        matchDetails.imageName = UserDetails.sharedInstance.teamImageUrl + matchImage
                    }

                    if let matchResult = details["match_result"].string{
                        matchDetails.matchResult = matchResult
                    }

                    if let startTime = details["start_date_unix"].string{
                        var closingTime = details["closing_ts"].intValue
                        if closingTime == 0{
                            closingTime = UserDetails.sharedInstance.closingTimeForMatch
                        }

//                        let calcultedTime = Int(startTime)! - closingTime
//                        matchDetails.closingTs = Double(closingTime)
//                        matchDetails.startDateTimestemp = String(calcultedTime)
                        matchDetails.startDateTimestemp = startTime
                    }

                    matchDetails.totalContestJoined = details["total_contest"].stringValue
                    
                    if let firstTeamImageName = details["team_a_flag"].string{
                        matchDetails.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                    }
                    
                    if let secondTeamImageName = details["team_b_flag"].string{
                        matchDetails.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                    }
                    if matchDetails.active == "4"{
                        matchDetails.isMatchClosed = false
                    }
                    if matchDetails.matchType?.lowercased() == "Tourney".lowercased() {
                        matchDetails.isMatchTourney = true
                    }

                    upcomingMatchDetailsArray.append(matchDetails)
                }
            }

            
            if let completedMatches = matchDict["completed"]?.array{
                
                for details in completedMatches {
                    
                    let matchDetails = MatchDetails()
                    matchDetails.seasonKey = details["season_key"].string
                    matchDetails.seasonShortName = details["season_short_name"].string ?? ""
                    matchDetails.matchKey = details["match_key"].string ?? ""
                    matchDetails.active = details["active"].stringValue
                    matchDetails.matchFantasyType = details["match_fantasy_type"].string ?? ""
                    matchDetails.firstTeamName = details["team_a_name"].string ?? ""
                    matchDetails.customText = details["custom_text"].string ?? ""

                    matchDetails.secondTeamName = details["team_b_name"].string ?? ""
                    matchDetails.hostName = details["host_name"].string ?? ""
                    matchDetails.city = details["city"].string ?? ""
                    matchDetails.stadium = details["stadium"].string ?? ""
                    matchDetails.country = details["country"].string ?? ""
                    matchDetails.firstTeamRank = details["team_a_rank"].string ?? ""
                    matchDetails.secondTeamRank = details["team_b_rank"].string ?? ""
                    matchDetails.information = details["information"].string ?? ""
                    matchDetails.tossWinner = details["toss_winner"].string ?? ""


                    matchDetails.matchShortName = details["match_short_name"].string
                    matchDetails.firstTeamShortName = details["team_a_short_name"].string
                    matchDetails.secondTeamShortName = details["team_b_short_name"].string
                    let matchClosedStatus = details["closed"].string
                    matchDetails.adminStatus = details["admin_status"].stringValue
                    matchDetails.show_playing22 = details["show_playing22"].string ?? "0"
                    matchDetails.firstTeamKey = details["team_a_key"].string ?? "0"
                    matchDetails.secondTeamKey = details["team_b_key"].string ?? "0"
                    matchDetails.playersGender = details["gender_match_category"].string ?? ""
                    matchDetails.matchFantasyType = details["match_fantasy_type"].string ?? ""

                    if matchClosedStatus == "0"{
                        matchDetails.isMatchClosed = false
                    }
                    else{
                        matchDetails.isMatchClosed = true
                    }

                    if let matchResult = details["match_result"].string{
                        matchDetails.matchResult = matchResult
                    }
                    
                    if let matchImage = details["match_image"].string{
                        matchDetails.imageName = UserDetails.sharedInstance.teamImageUrl + matchImage
                    }

                    matchDetails.matchType = details["match_format"].string

                    if let startTime = details["start_date_unix"].string{
                        var closingTime = details["closing_ts"].intValue
                        if closingTime == 0{
                            closingTime = UserDetails.sharedInstance.closingTimeForMatch
                        }

//                        let calcultedTime = Int(startTime)! - closingTime
//                        matchDetails.closingTs = Double(closingTime)
//                        matchDetails.startDateTimestemp = String(calcultedTime)
                        matchDetails.startDateTimestemp = startTime
                    }

                    if let matchStatus = details["match_status"].string{
                        matchDetails.matchStatus = matchStatus
                    }

                    matchDetails.totalContestJoined = details["total_contest"].stringValue

                    if let firstTeamImageName = details["team_a_flag"].string{
                        matchDetails.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                    }
                    
                    if let secondTeamImageName = details["team_b_flag"].string{
                        matchDetails.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                    }
                    if matchDetails.active == "4"{
                        matchDetails.isMatchClosed = false
                    }
                    if matchDetails.matchType?.lowercased() == "Tourney".lowercased() {
                        matchDetails.isMatchTourney = true
                    }

                    completedMatchDetailsArray.append(matchDetails)
                }
            }
        }
        
        return (liveMatchDetailsArray, upcomingMatchDetailsArray, completedMatchDetailsArray)
    }
    
    
    
    class func parseMatchDetails(details: [String: JSON]) ->  MatchDetails{
        
        let matchDetails = MatchDetails()
        matchDetails.seasonKey = details["season_key"]?.string
        matchDetails.seasonShortName = details["season_short_name"]?.string ?? ""
        matchDetails.matchKey = details["match_key"]?.string ?? ""
        matchDetails.matchShortName = details["match_short_name"]?.string
        matchDetails.firstTeamShortName = details["team_a_short_name"]?.string
        matchDetails.secondTeamShortName = details["team_b_short_name"]?.string
        matchDetails.matchType = details["match_format"]?.string
        let matchClosedStatus = details["closed"]?.string
        matchDetails.adminStatus = details["admin_status"]?.stringValue ?? "0"
        matchDetails.show_playing22 = details["show_playing22"]?.string ?? "0"
        matchDetails.matchFantasyType = details["match_fantasy_type"]?.string ?? ""
        matchDetails.customText = details["custom_text"]?.string ?? ""

        matchDetails.firstTeamName = details["team_a_name"]?.string ?? ""
        matchDetails.secondTeamName = details["team_b_name"]?.string ?? ""
        matchDetails.hostName = details["host_name"]?.string ?? ""
        matchDetails.city = details["city"]?.string ?? ""
        matchDetails.stadium = details["stadium"]?.string ?? ""
        matchDetails.country = details["country"]?.string ?? ""
        matchDetails.firstTeamRank = details["team_a_rank"]?.string ?? ""
        matchDetails.secondTeamRank = details["team_b_rank"]?.string ?? ""
        matchDetails.information = details["information"]?.string ?? ""
        matchDetails.tossWinner = details["toss_winner"]?.string ?? ""


        matchDetails.firstTeamKey = details["team_a_key"]?.string ?? "0"
        matchDetails.secondTeamKey = details["team_b_key"]?.string ?? "0"
        matchDetails.playersGender = details["gender_match_category"]?.string ?? ""
        matchDetails.active = details["active"]?.stringValue ?? ""

        if let matchImage = details["match_image"]?.string{
            matchDetails.imageName = UserDetails.sharedInstance.teamImageUrl + matchImage
        }

        if matchClosedStatus == "0"{
            matchDetails.isMatchClosed = false
        }
        else{
            matchDetails.isMatchClosed = true
        }
        
        if let startTime = details["start_date_unix"]?.string{
            var closingTime = details["closing_ts"]?.intValue ?? UserDetails.sharedInstance.closingTimeForMatch
            if closingTime == 0{
                closingTime = UserDetails.sharedInstance.closingTimeForMatch
            }

//            let calcultedTime = Int(startTime)! - closingTime
//            matchDetails.closingTs = Double(closingTime)
//            matchDetails.startDateTimestemp = String(calcultedTime)
            matchDetails.startDateTimestemp = startTime
        }
        
        if let matchResult = details["match_result"]?.string{
            matchDetails.matchResult = matchResult
        }
        
        if let matchStatus = details["match_status"]?.string{
            matchDetails.matchStatus = matchStatus
        }
        
        if let firstTeamImageName = details["team_a_flag"]?.string{
            matchDetails.firstTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
        }
        
        if let secondTeamImageName = details["team_b_flag"]?.string{
            matchDetails.secondTeamImageUrl = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
        }
        if matchDetails.active == "4"{
            matchDetails.isMatchClosed = false
        }
        
        if matchDetails.matchType?.lowercased() == "Tourney".lowercased() {
            matchDetails.isMatchTourney = true
        }

        return matchDetails
    }
    
}





