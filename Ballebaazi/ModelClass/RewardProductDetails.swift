//
//  RewardProductDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 25/02/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class RewardProductDetails: NSObject {

    var descriptionHindi = ""
    var descriptionEnglish = ""
    var ticketType = ""
    var image = ""
    var rewardCategoryId = ""
    var rewardId = ""
    var leftItems = ""
    var rewardProdId = ""
    var rewardSubType = ""
    var coins = ""
    var categoryName = ""
    var rewardNameEnglish = ""
    var realAmount = ""
    var maxLimit = ""
    var rewardNameHindi = ""
    var expiry_date = ""
    var status = ""
    var remainingDays = ""

    var isProductClaimed = false

    class func getAllProductList(dataArray: Array<JSON>, claimedProduct: Array<JSON>) -> Array<RewardProductDetails> {
        var rewardProductsList = Array<RewardProductDetails>()

        for details in dataArray {
            let rewardsDetails = RewardProductDetails()
            rewardsDetails.descriptionHindi = details["description_hi"].string ?? ""
            let image = details["image"].string ?? ""
            if image.count > 5{
                rewardsDetails.image = UserDetails.sharedInstance.promotionImageUrl + image
            }
            rewardsDetails.descriptionEnglish = details["description"].string ?? ""
            rewardsDetails.ticketType = details["ticket_type"].string ?? ""
            rewardsDetails.rewardCategoryId = details["reward_category_id"].string ?? ""
            rewardsDetails.rewardId = details["reward_id"].string ?? ""
            rewardsDetails.leftItems = details["left_items"].string ?? ""
            rewardsDetails.rewardProdId = details["reward_prod_id"].string ?? ""
            rewardsDetails.rewardSubType = details["reward_sub_type"].string ?? ""
            rewardsDetails.status = details["status"].stringValue 
            rewardsDetails.coins = details["bbcoins"].stringValue
            rewardsDetails.categoryName = details["category_name"].string ?? ""
            rewardsDetails.rewardNameEnglish = details["reward_name"].string ?? ""
            rewardsDetails.realAmount = details["real_amount"].string ?? ""
            rewardsDetails.maxLimit = details["max_limit"].string ?? ""
            rewardsDetails.rewardNameHindi = details["reward_name_hi"].string ?? ""
            rewardsDetails.expiry_date = details["expiry_date"].string ?? ""

            let filterProduct = claimedProduct.filter { (claimedDetails) -> Bool in
                return claimedDetails["reward_product_id"].stringValue == rewardsDetails.rewardProdId
            }
            
            if filterProduct.count > 0{
                let filterProductDetails = filterProduct[0]
                let raminingDays = filterProductDetails["days_remaining"].stringValue
                if raminingDays != "0" {
                    rewardsDetails.remainingDays = raminingDays
                    rewardsDetails.isProductClaimed = true
                }
            }

            rewardProductsList.append(rewardsDetails)
        }
        return rewardProductsList
    }
    
    
}
