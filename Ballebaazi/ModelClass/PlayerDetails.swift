//
//  PlayerDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/12/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class PlayerDetails: NSObject {

    var playerKey: String?
    var seasonalRole = ""
    var playerPlayingRole: String?
    var playerName: String?
    lazy var teamShortName = ""
    lazy var playerShortName = ""

    var formClassic = "0"
    var formBatting = "0"
    var formBowlling = "0"
    var isPlaying11Last = "0"
    var isPlaying11Prob = "0"
    var playerScore = "0"
    
    var battingPoints = "0"
    var bowlingPoints = "0"
    var classicPoints = "0"
    
    var reversePoints = "0"
    var wizardPoints = "0"

    var battingPointsForStat = "0"
    var bowlingPointsForStat = "0"
    var classicPointsForStat = "0"

    var sessionalStartingPoints = "0"

    var credits = "0"
    var teamKey = ""
    var isSelected = false
    var isCaption = false
    var isViceCaption = false
    var isPlayerPlaying = false
    var isWizard = false

    var startingPoints: String?
    var battingRunsPoints: String?
    var battingFoursPoints: String?
    var battingSixsPoints: String?
    var battingStrickRatePoints: String?
    var battingCentryPoints = "0"
    
    var bowlerWicketPoints: String?
    var bowlerMaidenOverPoints: String?
    var bowlerEconomyPoints: String?
    
    var catchPoints: String?
    var runoutPoints: String?
    
    var totalClassicCaptainScore = "0"
    var totalClassicViceCaptainScore = "0"
    var totalClasscPlayerScore = "0"
    var totalBattingCaptainScore = "0"
    var totalBattingViceCaptainScore  = "0"
    var totalBattingPlayerScore = "0"    
    var totalBowlingCaptainScore = "0"
    var totalBowlingViceCaptainScore = "0"
    var totalBowlingPlayerScore = "0"
    var battingDuckPoints = "0"
    var pointsBowlingWicket4 = "0"
    var pointsBowlingWicket5 = "0"
    var pointsBowlingEconomy = "0"

    var raidUnsuccess = "0"
    var isSubstitute = "0"
    var greenCard = "0"
    var startingKabaddiPoints = "0"
    var yellowCard = "0"
    var superTackle = "0"
    var gettingAllOut = "0"
    var tacklesSuccess = "0"
    var raidTouch = "0"
    var raidBonus = "0"
    var pushingAllOut = "0"
    var redCard = "0"

    
    var penalitySaved = "0"
    var goalConceded = "0"
    var cleanSheet = "0"
    
    var goalScored = "0"
    var goalAssist = "0"
    var passesCompleted = "0"
    var shotOnTarget = "0"
    var goalShotSaved = "0"
    var tacklesMade = "0"
    var footballYellowCard = "0"
    var footballRedCard = "0"
    var ownGoal = "0"
    var penalityWon = "0"
    var minPlayed = "0"
    
    
    var min_played = "0"
    var goal_scored = "0"
    var goal_assist = "0"
    var passes_completed = "0"
    var shot_on_target = "0"
    var clean_sheet = "0"
    var goal_shot_saved = "0"
    var tackles_made = "0"
    var penality_saved = "0"
    var red_card = "0"
    var yellow_card = "0"
    var own_goal = "0"
    var goal_conceded = "0"
    var penality_won = "0"
        
    var reboundsActual = "0"
    var blocks = "0"
    var turnovers = "0"
    var steals = "0"
    var blocksActual = "0"
    var assistActual = "0"
    var turnoversActual = "0"
    var stealsActual = "0"
    var rebounds = "0"
    var assist = "0"
    
    var baseballBBH = "0"
    var baseballBBHActual = "0"
    var battleInActual = "0"
    var battleIn = "0"
    var strikeOutActual = "0"
    var strikeOut = "0"
    var runScoreActual = "0"
    var runScore = "0"
    var tripleActual = "0"
    var triple = "0"
    var hitAllowed = "0"
    var hitAllowedActual = "0"
    var homeRunActual = "0"
    var homeRun = "0"
    var double = "0"
    var doubleActual = "0"
    var earnRun = "0"
    var earnRunActual = "0"
    var single = "0"
    var singleActual = "0"
    var inningPitch = "0"
    var inningPitchActual = "0"
    var baseballBBHPitching = "0"
    var baseballBBHPitchingActual = "0"
    var stolenActual = "0"
    var stolen = "0"
    var defaultScore = "0"
    var defaultScoreActual = "0"

    var wizardScore = "0"
    var captainScoreWizard = "0"
    var viceCaptainScoreWizard = "0"
    var captainScoreReverse = "0"
    var viceCaptainScoreReverse = "0"

    
    
    var captainRoleSelected = "0"
    var viceCaptainRoleSelected = "0"
    var classicSelected = "0"
    var battingSelected = "0"
    var bowlingSelected = "0"
    var wizardRoleSelected = "0"
    var wizardSelected = "0"
    var reverseSelected = "0"

    
    var imgURL = ""
    var playerPlaceholder = "PlayerMalePlaceholder"
    
    
    class func getPlayerDetailsArray(responseArray: Array<JSON>, matchDetails: MatchDetails?) -> Array<PlayerDetails> {
        
        var playerDetailsArray = Array<PlayerDetails>()
        
        for details in responseArray {
            
            let playerDetails = PlayerDetails()
            playerDetails.teamKey = details["team_key"].stringValue
            playerDetails.playerKey = details["player_key"].string
            if (playerDetails.playerKey?.count == 0) || (playerDetails.playerKey == nil) {
                playerDetails.playerKey = details["players_key"].string
            }

            playerDetails.playerPlayingRole = details["player_playing_role"].string?.lowercased()
            playerDetails.playerName = details["player_name"].string
            playerDetails.seasonalRole = details["seasonal_role"].string?.lowercased() ?? ""
            if playerDetails.seasonalRole.count == 0 {
                playerDetails.seasonalRole = playerDetails.playerPlayingRole ?? ""
            }
            let playerPhoto = details["player_photo"].string ?? ""
            if UserDetails.sharedInstance.teamImageUrl.count != 0{
                playerDetails.imgURL = UserDetails.sharedInstance.teamImageUrl + playerPhoto
            }
            let isPlaying = details["is_playing"].stringValue
            
            if isPlaying == "1"{
                playerDetails.isPlayerPlaying = true
            }
            else{
                playerDetails.isPlayerPlaying = false
            }

            if let playerScore = details["scores"].string{
                playerDetails.playerScore = playerScore
            }
            
            var playersRole = details["players_role"].string?.lowercased()
            if playersRole == nil {
                playersRole = details["player_role"].string?.lowercased()
            }
            
            if "captain" == playersRole{
                playerDetails.isCaption = true
            }
            
            if "vice_captain" == playersRole{
                playerDetails.isViceCaption = true
            }
            
            if "wizard" == playersRole{
                playerDetails.isWizard = true
            }
            
            if "captain_wizard" == playersRole{
                playerDetails.isCaption = true
                playerDetails.isWizard = true
            }
            
            if "vice_captain_wizard" == playersRole{
                playerDetails.isViceCaption = true
                playerDetails.isWizard = true
            }

            playerDetails.classicPoints = details["seasonal_classic_points"].stringValue
            playerDetails.battingPoints = details["seasonal_batting_points"].stringValue
            playerDetails.bowlingPoints = details["seasonal_bowling_points"].stringValue
            playerDetails.sessionalStartingPoints = details["seasonal_start_points"].stringValue

            playerDetails.reversePoints = details["seasonal_reverse_points"].stringValue
            playerDetails.wizardPoints = details["seasonal_wizard_points"].stringValue
            
            playerDetails.isPlaying11Prob = details["is_playing11_prob"].stringValue
            playerDetails.isPlaying11Last = details["is_playing11_last"].stringValue
            
            /************************************************/
            // New points system
            playerDetails.startingPoints = details["points_starting"].string ?? "0"
            playerDetails.battingRunsPoints = details["points_batting_runs"].string ?? "0"
            playerDetails.battingFoursPoints = details["points_batting_fours"].string ?? "0"
            playerDetails.battingSixsPoints = details["points_batting_sixes"].string ?? "0"
            playerDetails.battingStrickRatePoints = details["points_batting_rate"].string ?? "0"
            let pointsBatting50 = details["points_batting_50"].string ?? "0"
            let pointsBatting100 = details["points_batting_100"].string ?? "0"
            
            if Double(pointsBatting100)! > 0.0{
                playerDetails.battingCentryPoints = String((Double(pointsBatting100) ?? 0) + (Double(pointsBatting50) ?? 0))
            }
            else if Double(pointsBatting50)! > 0.0{
                playerDetails.battingCentryPoints = pointsBatting50
            }
            
            playerDetails.bowlerWicketPoints = details["points_bowling_wickets"].string ?? "0"
            playerDetails.bowlerMaidenOverPoints = details["points_bowling_maidens"].string ?? "0"
            playerDetails.bowlerEconomyPoints = details["bowling_economy"].string ?? "0"
            playerDetails.catchPoints = details["points_fielding_catch"].string ?? "0"
            let runoutPoints = details["points_fielding_runout"].string ?? "0"
            let stumpPoints = details["points_fielding_stumped"].string ?? "0"
            
            let totalRunOutPoints = (Float(runoutPoints) ?? 0) + (Float(stumpPoints) ?? 0)
            playerDetails.runoutPoints = String(format: "%.2f", totalRunOutPoints)
            playerDetails.totalClassicCaptainScore = details["captain_score"].string ?? "0"
            playerDetails.totalClassicViceCaptainScore = details["vice_captain_score"].string ?? "0"
            playerDetails.totalClasscPlayerScore = details["player_score"].string ?? "0"
            
            playerDetails.wizardScore = details["wizard_score"].string ?? "0"
            playerDetails.captainScoreWizard = details["captain_score_wizard"].string ?? "0"
            playerDetails.viceCaptainScoreWizard = details["vice_captain_score_wizard"].string ?? "0"
            playerDetails.captainScoreReverse = details["captain_score_reverse"].string ?? "0"
            playerDetails.viceCaptainScoreReverse = details["vice_captain_score_reverse"].string ?? "0"

            playerDetails.totalBattingCaptainScore = details["captain_score_batting"].string ?? "0"
            playerDetails.totalBattingViceCaptainScore = details["vice_captain_score_batting"].string ?? "0"
            playerDetails.totalBattingPlayerScore = details["player_score_batting"].string ?? "0"
            
            playerDetails.totalBowlingCaptainScore = details["captain_score_bowling"].string ?? "0"
            playerDetails.totalBowlingViceCaptainScore = details["vice_captain_score_bowling"].string ?? "0"
            playerDetails.totalBowlingPlayerScore = details["player_score_bowling"].string ?? "0"
            playerDetails.pointsBowlingEconomy = details["points_bowling_economy"].string ?? "0"
            playerDetails.battingDuckPoints = details["points_batting_duck"].string ?? "0"
            playerDetails.pointsBowlingWicket4 = details["points_bowling_wicket4"].string ?? "0"
            playerDetails.pointsBowlingWicket5 = details["points_bowling_wicket5"].string ?? "0"
            
            /************************************************/
            // FOR Kabaddi
            playerDetails.raidUnsuccess = details["raid_unsuccess"].stringValue
            playerDetails.isSubstitute = details["is_substitute"].stringValue
            playerDetails.greenCard = details["green_card"].stringValue
            playerDetails.startingKabaddiPoints = details["starting_7"].stringValue
            playerDetails.yellowCard = details["yellow_card"].stringValue
            playerDetails.superTackle = details["super_tackle"].stringValue
            playerDetails.gettingAllOut = details["getting_all_out"].stringValue
            playerDetails.tacklesSuccess = details["tackles_success"].stringValue
            playerDetails.raidTouch = details["raid_touch"].stringValue
            playerDetails.raidBonus = details["raid_bonus"].stringValue
            playerDetails.pushingAllOut = details["pushing_all_out"].stringValue
            playerDetails.redCard = details["red_card"].stringValue

             /************************************************/
            
            // FOR Football
            playerDetails.min_played = details["min_played"].string ?? "0"
            playerDetails.goal_scored = details["goal_scored"].string ?? "0"
            playerDetails.goal_assist = details["goal_assist"].string ?? "0"
            playerDetails.passes_completed = details["passes_completed"].string ?? "0"
            playerDetails.shot_on_target = details["shot_on_target"].string ?? "0"
            playerDetails.clean_sheet = details["clean_sheet"].string ?? "0"
            playerDetails.goal_shot_saved = details["goal_shot_saved"].string ?? "0"
            playerDetails.penality_saved = details["penality_saved"].string ?? "0"
            playerDetails.tackles_made = details["tackles_made"].string ?? "0"
            playerDetails.red_card = details["red_card"].string ?? "0"
            playerDetails.yellow_card = details["yellow_card"].string ?? "0"
            playerDetails.own_goal = details["own_goal"].string ?? "0"
            playerDetails.goal_conceded = details["goal_conceded"].string ?? "0"
            playerDetails.penality_won = details["penality_won"].string ?? "0"

            let minPlayed = details["p_min_played"].string ?? "0"
            let defaultScore = details["p_default_score"].string ?? "0"
            let playingPoint = (Int(minPlayed) ?? 0) + (Int(defaultScore) ?? 0)
            playerDetails.minPlayed = String(playingPoint)

            playerDetails.minPlayed = details["p_min_played"].string ?? "0"
            playerDetails.goalConceded = details["p_goal_conceded"].string ?? "0"
            playerDetails.cleanSheet = details["p_clean_sheet"].string ?? "0"
            playerDetails.penalitySaved = details["p_penality_saved"].string ?? "0"
            
            playerDetails.goalScored = details["p_goal_scored"].string ?? "0"
            playerDetails.goalAssist = details["p_goal_assist"].string ?? "0"
            playerDetails.passesCompleted = details["p_passes_completed"].string ?? "0"
            playerDetails.shotOnTarget = details["p_shot_on_target"].string ?? "0"
            playerDetails.goalShotSaved = details["p_goal_shot_saved"].string ?? "0"
            playerDetails.tacklesMade = details["p_tackles_made"].string ?? "0"
            playerDetails.footballYellowCard = details["p_yellow_card"].string ?? "0"
            playerDetails.footballRedCard = details["p_red_card"].string ?? "0"
            playerDetails.ownGoal = details["p_own_goal"].string ?? "0"
            playerDetails.penalityWon = details["p_penality_won"].string ?? "0"
            /************************************************/
            
            // For basketball
            playerDetails.reboundsActual = details["rebounds_actual"].string ?? "0"
            playerDetails.blocks = details["blocks"].string ?? "0"
            playerDetails.turnovers = details["turnovers"].string ?? "0"
            playerDetails.steals = details["steals"].string ?? "0"
            playerDetails.blocksActual = details["blocks_actual"].string ?? "0"
            playerDetails.assistActual = details["assist_actual"].string ?? "0"
            playerDetails.turnoversActual = details["turnovers_actual"].string ?? "0"
            playerDetails.stealsActual = details["steals_actual"].string ?? "0"
            playerDetails.rebounds = details["rebounds"].string ?? "0"
            playerDetails.assist = details["assist"].string ?? "0"

            if let playerScore = details["point_scored"].string{
                if playerScore.count != 0 {
                    playerDetails.playerScore = playerScore
                }
            }
            
            
            /*************************************************************/
            
            // For Baseball
            playerDetails.baseballBBH = details["base_on_balls_bbh_or_walk"].string ?? "0"
            playerDetails.baseballBBHActual = details["base_on_balls_bbh_or_walk_actual"].string ?? "0"
            playerDetails.battleInActual = details["runs_batted_in_rbi_actual"].string ?? "0"
            playerDetails.battleIn = details["runs_batted_in_rbi"].string ?? "0"
            playerDetails.strikeOutActual = details["strikeout_so_actual"].string ?? "0"
            playerDetails.strikeOut = details["strikeout_so"].string ?? "0"
            playerDetails.runScoreActual = details["run_scored_r_actual"].string ?? "0"
            playerDetails.runScore = details["run_scored_r"].string ?? "0"
            playerDetails.tripleActual = details["triple_3b_actual"].string ?? "0"
            playerDetails.triple = details["triple_3b"].string ?? "0"
            playerDetails.hitAllowed = details["hit_allowed_h"].string ?? "0"
            playerDetails.hitAllowedActual = details["hit_allowed_h_actual"].string ?? "0"
            playerDetails.homeRunActual = details["home_run_hr_actual"].string ?? "0"
            playerDetails.homeRun = details["home_run_hr"].string ?? "0"
            playerDetails.double = details["double_2b"].string ?? "0"
            playerDetails.doubleActual = details["double_2b_actual"].string ?? "0"
            playerDetails.earnRun = details["earned_run_er"].string ?? "0"
            playerDetails.earnRunActual = details["earned_run_er_actual"].string ?? "0"
            playerDetails.single = details["single_1b"].string ?? "0"
            playerDetails.singleActual = details["single_1b_actual"].string ?? "0"
            playerDetails.inningPitch = details["inning_pitched_ip"].string ?? "0"
            playerDetails.inningPitchActual = details["inning_pitched_ip_actual"].string ?? "0"
            playerDetails.baseballBBHPitching = details["base_on_balls_bbh_or_walk_pitching"].string ?? "0"
            playerDetails.baseballBBHPitchingActual = details["base_on_balls_bbh_or_walk_pitching_actual"].string ?? "0"
            playerDetails.stolenActual = details["stolen_base_sb_actual"].string ?? "0"
            playerDetails.stolen = details["stolen_base_sb"].string ?? "0"
            playerDetails.defaultScore = details["default_score"].string ?? "0"
            playerDetails.defaultScoreActual = details["default_score_actual"].string ?? "0"
            
            
            
            
            
            if let credits = details["player_credits"].string{
                playerDetails.credits = credits
            }
            else{
                playerDetails.credits = "0"
            }
            
            if playerDetails.credits == "0"{
                if let credits = details["credits"].string{
                    playerDetails.credits = credits
                }
            }
            playerDetails.teamShortName = details["team_short_name"].string ?? ""

            if playerDetails.teamShortName.count == 0 {
                            
                if playerDetails.teamKey == matchDetails?.firstTeamKey {
                    playerDetails.teamShortName = matchDetails?.firstTeamShortName ?? ""
                }
                else if playerDetails.teamKey == matchDetails?.secondTeamKey {
                    playerDetails.teamShortName = matchDetails?.secondTeamKey ?? ""
                }
            }
                        
            let playerNameArray = playerDetails.playerName?.components(separatedBy: " ")
            playerDetails.playerShortName = playerDetails.playerName ?? ""
            
            var nameplayerNameArray = ""
            
            if playerNameArray != nil {
                if playerNameArray!.count > 1 {
                    let name = playerNameArray![0]
                    if name.count > 2{
                        nameplayerNameArray = String(name.prefix(1))
                        for i in 1 ..< playerNameArray!.count{
                            nameplayerNameArray = nameplayerNameArray + " " +  playerNameArray![i]
                        }
                    }
                }
            }
            
            if nameplayerNameArray.count > 0 {
                playerDetails.playerShortName = nameplayerNameArray
            }            
            
            
            
            playerDetails.viceCaptainRoleSelected = details["vice_captain_role_selected"].string ?? "0"
            playerDetails.captainRoleSelected = details["captain_role_selected"].string ?? "0"
            playerDetails.wizardRoleSelected = details["wizard_role_selected"].string ?? "0"
            playerDetails.classicSelected = details["classic_selected"].string ?? "0"
            playerDetails.battingSelected = details["batting_selected"].string ?? "0"
            playerDetails.bowlingSelected = details["bowling_selected"].string ?? "0"
            playerDetails.wizardSelected = details["wizard_selected"].string ?? "0"
            playerDetails.reverseSelected = details["reverse_selected"].string ?? "0"

            
            
            playerDetailsArray.append(playerDetails)
        }
        
        return playerDetailsArray
    }
    
    
    class func getPlayerDetailsForScoreArray(responseArray: Array<JSON>) -> Array<PlayerDetails> {
        
        var playerDetailsArray = Array<PlayerDetails>()
        
        for details in responseArray {
            
            let playerDetails = PlayerDetails()

            playerDetails.playerName = details["name"].string
            playerDetails.teamKey = details["team_key"].stringValue

            playerDetails.playerPlayingRole = details["seasonal_role"].string?.lowercased()
            playerDetails.playerKey = details["player_key"].string
            if (playerDetails.playerKey?.count == 0) || (playerDetails.playerKey == nil) {
                playerDetails.playerKey = details["players_key"].string
            }
            playerDetails.seasonalRole = details["seasonal_role"].string?.lowercased() ?? ""
            let playerPhoto = details["player_photo"].string ?? ""
            if UserDetails.sharedInstance.teamImageUrl.count != 0{
                playerDetails.imgURL = UserDetails.sharedInstance.teamImageUrl + playerPhoto
            }

            let isPlaying = details["is_playing"].stringValue
            if isPlaying == "1"{
                playerDetails.isPlayerPlaying = true
            }
            else{
                playerDetails.isPlayerPlaying = false
            }
            
            var playersRole = details["players_role"].string?.lowercased()
            if playersRole == nil {
                playersRole = details["player_role"].string?.lowercased()
            }
            
            if "captain" == playersRole{
                playerDetails.isCaption = true
            }
            
            if "vice_captain" == playersRole{
                playerDetails.isViceCaption = true
            }
            
            if "wizard" == playersRole{
                playerDetails.isWizard = true
            }
            
            if "captain_wizard" == playersRole{
                playerDetails.isCaption = true
                playerDetails.isWizard = true
            }
            
            if "vice_captain_wizard" == playersRole{
                playerDetails.isViceCaption = true
                playerDetails.isWizard = true
            }

            
            playerDetails.startingPoints = details["points_starting"].string ?? "0"
            playerDetails.isPlaying11Prob = details["is_playing11_prob"].stringValue
            playerDetails.isPlaying11Last = details["is_playing11_last"].stringValue
            playerDetails.battingRunsPoints = details["points_batting_runs"].string ?? "0"
            playerDetails.battingFoursPoints = details["points_batting_fours"].string ?? "0"
            playerDetails.battingSixsPoints = details["points_batting_sixes"].string ?? "0"
            playerDetails.battingStrickRatePoints = details["points_batting_rate"].string ?? "0"
            
            let pointsBatting50 = details["points_batting_50"].string ?? "0"
            let pointsBatting100 = details["points_batting_100"].string ?? "0"
            
            if Double(pointsBatting100)! > 0.0{
                if Double(pointsBatting100)! > 0.0{
                    playerDetails.battingCentryPoints = String((Double(pointsBatting100) ?? 0) + (Double(pointsBatting50) ?? 0))
                }
                else if Double(pointsBatting50)! > 0.0{
                    playerDetails.battingCentryPoints = pointsBatting50
                }
            }
            else if Double(pointsBatting50)! > 0.0{
                playerDetails.battingCentryPoints = pointsBatting50
            }
            
            playerDetails.bowlerWicketPoints = details["points_bowling_wickets"].string ?? "0"
            playerDetails.bowlerMaidenOverPoints = details["points_bowling_maidens"].string ?? "0"
            playerDetails.bowlerEconomyPoints = details["bowling_economy"].string ?? "0"
            playerDetails.catchPoints = details["points_fielding_catch"].string ?? "0"
            let runoutPoints = details["points_fielding_runout"].string ?? "0"
            let stumpPoints = details["points_fielding_stumped"].string ?? "0"
            let totalRunOutPoints = (Float(runoutPoints) ?? 0) + (Float(stumpPoints) ?? 0)
            playerDetails.runoutPoints = String(format: "%.2f", totalRunOutPoints)
            playerDetails.totalClassicCaptainScore = details["captain_score"].string ?? "0"
            playerDetails.totalClassicViceCaptainScore = details["vice_captain_score"].string ?? "0"
            playerDetails.totalClasscPlayerScore = details["player_score"].string ?? "0"
            playerDetails.totalBattingCaptainScore = details["captain_score_batting"].string ?? "0"
            playerDetails.totalBattingViceCaptainScore = details["vice_captain_score_batting"].string ?? "0"
            playerDetails.totalBattingPlayerScore = details["player_score_batting"].string ?? "0"
            playerDetails.totalBowlingCaptainScore = details["captain_score_bowling"].string ?? "0"
            playerDetails.totalBowlingViceCaptainScore = details["vice_captain_score_bowling"].string ?? "0"
            playerDetails.totalBowlingPlayerScore = details["player_score_bowling"].string ?? "0"
            playerDetails.pointsBowlingEconomy = details["points_bowling_economy"].string ?? "0"
            playerDetails.battingDuckPoints = details["points_batting_duck"].string ?? "0"
            playerDetails.pointsBowlingWicket4 = details["points_bowling_wicket4"].string ?? "0"
            playerDetails.pointsBowlingWicket5 = details["points_bowling_wicket5"].string ?? "0"
            playerDetails.teamShortName = details["team_short_name"].string ?? ""

            playerDetails.wizardScore = details["wizard_score"].string ?? "0"
            playerDetails.captainScoreWizard = details["captain_score_wizard"].string ?? "0"
            playerDetails.viceCaptainScoreWizard = details["vice_captain_score_wizard"].string ?? "0"
            playerDetails.captainScoreReverse = details["captain_score_reverse"].string ?? "0"
            playerDetails.viceCaptainScoreReverse = details["vice_captain_score_reverse"].string ?? "0"

            /************************************************/
            // FOR Kabaddi
            playerDetails.raidUnsuccess = details["raid_unsuccess"].stringValue
            playerDetails.isSubstitute = details["is_substitute"].stringValue
            playerDetails.greenCard = details["green_card"].stringValue
            playerDetails.startingKabaddiPoints = details["starting_7"].stringValue
            playerDetails.yellowCard = details["yellow_card"].stringValue
            playerDetails.superTackle = details["super_tackle"].stringValue
            playerDetails.gettingAllOut = details["getting_all_out"].stringValue
            playerDetails.tacklesSuccess = details["tackles_success"].stringValue
            playerDetails.raidTouch = details["raid_touch"].stringValue
            playerDetails.raidBonus = details["raid_bonus"].stringValue
            playerDetails.pushingAllOut = details["pushing_all_out"].stringValue
            playerDetails.redCard = details["red_card"].stringValue

            /************************************************/

            
            
            // FOR Football
            
            playerDetails.min_played = details["min_played"].string ?? "0"
            playerDetails.goal_scored = details["goal_scored"].string ?? "0"
            playerDetails.goal_assist = details["goal_assist"].string ?? "0"
            playerDetails.passes_completed = details["passes_completed"].string ?? "0"
            playerDetails.shot_on_target = details["shot_on_target"].string ?? "0"
            playerDetails.clean_sheet = details["clean_sheet"].string ?? "0"
            playerDetails.goal_shot_saved = details["goal_shot_saved"].string ?? "0"
            playerDetails.penality_saved = details["penality_saved"].string ?? "0"
            playerDetails.tackles_made = details["tackles_made"].string ?? "0"
            playerDetails.red_card = details["red_card"].string ?? "0"
            playerDetails.yellow_card = details["yellow_card"].string ?? "0"
            playerDetails.own_goal = details["own_goal"].string ?? "0"
            playerDetails.goal_conceded = details["goal_conceded"].string ?? "0"
            playerDetails.penality_won = details["penality_won"].string ?? "0"
            
            let minPlayed = details["p_min_played"].string ?? "0"
            let defaultScore = details["p_default_score"].string ?? "0"
            let playingPoint = (Int(minPlayed) ?? 0) + (Int(defaultScore) ?? 0)
            playerDetails.minPlayed = String(playingPoint)
            
            playerDetails.goalConceded = details["p_goal_conceded"].string ?? "0"
            playerDetails.cleanSheet = details["p_clean_sheet"].string ?? "0"
            playerDetails.penalitySaved = details["p_penality_saved"].string ?? "0"
            playerDetails.goalScored = details["p_goal_scored"].string ?? "0"
            playerDetails.goalAssist = details["p_goal_assist"].string ?? "0"
            playerDetails.passesCompleted = details["p_passes_completed"].string ?? "0"
            playerDetails.shotOnTarget = details["p_shot_on_target"].string ?? "0"
            playerDetails.goalShotSaved = details["p_goal_shot_saved"].string ?? "0"
            playerDetails.tacklesMade = details["p_tackles_made"].string ?? "0"
            playerDetails.footballYellowCard = details["p_yellow_card"].string ?? "0"
            playerDetails.footballRedCard = details["p_red_card"].string ?? "0"
            playerDetails.ownGoal = details["p_own_goal"].string ?? "0"
            playerDetails.penalityWon = details["p_penality_won"].string ?? "0"
            /************************************************/
            
            
            // For basketball
            playerDetails.reboundsActual = details["rebounds_actual"].string ?? "0"
            playerDetails.blocks = details["blocks"].string ?? "0"
            playerDetails.turnovers = details["turnovers"].string ?? "0"
            playerDetails.steals = details["steals"].string ?? "0"
            playerDetails.blocksActual = details["blocks_actual"].string ?? "0"
            playerDetails.assistActual = details["assist_actual"].string ?? "0"
            playerDetails.turnoversActual = details["turnovers_actual"].string ?? "0"
            playerDetails.stealsActual = details["steals_actual"].string ?? "0"
            playerDetails.rebounds = details["rebounds"].string ?? "0"
            playerDetails.assist = details["assist"].string ?? "0"

            
            /*************************************************************/
            
            // For Baseball
            playerDetails.baseballBBH = details["base_on_balls_bbh_or_walk"].string ?? "0"
            playerDetails.baseballBBHActual = details["base_on_balls_bbh_or_walk_actual"].string ?? "0"
            playerDetails.battleInActual = details["runs_batted_in_rbi_actual"].string ?? "0"
            playerDetails.battleIn = details["runs_batted_in_rbi"].string ?? "0"
            playerDetails.strikeOutActual = details["strikeout_so_actual"].string ?? "0"
            playerDetails.strikeOut = details["strikeout_so"].string ?? "0"
            playerDetails.runScoreActual = details["run_scored_r_actual"].string ?? "0"
            playerDetails.runScore = details["run_scored_r"].string ?? "0"
            playerDetails.tripleActual = details["triple_3b_actual"].string ?? "0"
            playerDetails.triple = details["triple_3b"].string ?? "0"
            playerDetails.hitAllowed = details["hit_allowed_h"].string ?? "0"
            playerDetails.hitAllowedActual = details["hit_allowed_h_actual"].string ?? "0"
            playerDetails.homeRunActual = details["home_run_hr_actual"].string ?? "0"
            playerDetails.homeRun = details["home_run_hr"].string ?? "0"
            playerDetails.double = details["double_2b"].string ?? "0"
            playerDetails.doubleActual = details["double_2b_actual"].string ?? "0"
            playerDetails.earnRun = details["earned_run_er"].string ?? "0"
            playerDetails.earnRunActual = details["earned_run_er_actual"].string ?? "0"
            playerDetails.single = details["single_1b"].string ?? "0"
            playerDetails.singleActual = details["single_1b_actual"].string ?? "0"
            playerDetails.inningPitch = details["inning_pitched_ip"].string ?? "0"
            playerDetails.inningPitchActual = details["inning_pitched_ip_actual"].string ?? "0"
            playerDetails.baseballBBHPitching = details["base_on_balls_bbh_or_walk_pitching"].string ?? "0"
            playerDetails.baseballBBHPitchingActual = details["base_on_balls_bbh_or_walk_pitching_actual"].string ?? "0"
            playerDetails.stolenActual = details["stolen_base_sb_actual"].string ?? "0"
            playerDetails.stolen = details["stolen_base_sb"].string ?? "0"
            playerDetails.defaultScore = details["default_score"].string ?? "0"
            playerDetails.defaultScoreActual = details["default_score_actual"].string ?? "0"
            
            
            
            if let playerScore = details["point_scored"].string{
                if playerScore.count != 0 {
                    playerDetails.playerScore = playerScore
                }
            }
            playerDetailsArray.append(playerDetails)
        }
        
        return playerDetailsArray
    }
        
    class func getOtherTeamsPlayerDetailsArray(response:JSON) -> Array<PlayerDetails> {
        var playerDetailsArray = Array<PlayerDetails>()

        if let temp = response["players"].dictionary{
            var responseArray = Array<JSON>()
            
            for(_, value) in temp{
                responseArray.append(value)
            }
            
            for details in responseArray {
                
                let playerDetails = PlayerDetails()

                playerDetails.teamKey = details["team_key"].stringValue
                playerDetails.playerKey = details["player_key"].string
                if (playerDetails.playerKey?.count == 0) || (playerDetails.playerKey == nil) {
                    playerDetails.playerKey = details["players_key"].string
                }
                playerDetails.playerPlayingRole = details["player_playing_role"].string?.lowercased()
                playerDetails.playerName = details["player_name"].string
                playerDetails.seasonalRole = details["seasonal_role"].string?.lowercased() ?? ""
                let playerPhoto = details["player_photo"].string ?? ""
                if UserDetails.sharedInstance.teamImageUrl.count != 0{
                    playerDetails.imgURL = UserDetails.sharedInstance.teamImageUrl + playerPhoto
                }

                
                /************************************************/
                // New points system
                playerDetails.startingPoints = details["points_starting"].string ?? "0"
                playerDetails.battingRunsPoints = details["points_batting_runs"].string ?? "0"
                playerDetails.battingFoursPoints = details["points_batting_fours"].string ?? "0"
                playerDetails.battingSixsPoints = details["points_batting_sixes"].string ?? "0"
                playerDetails.battingStrickRatePoints = details["points_batting_rate"].string ?? "0"
                let pointsBatting50 = details["points_batting_50"].string ?? "0"
                let pointsBatting100 = details["points_batting_100"].string ?? "0"
                playerDetails.isPlaying11Prob = details["is_playing11_prob"].stringValue
                playerDetails.isPlaying11Last = details["is_playing11_last"].stringValue

                playerDetails.wizardScore = details["wizard_score"].string ?? "0"
                playerDetails.captainScoreWizard = details["captain_score_wizard"].string ?? "0"
                playerDetails.viceCaptainScoreWizard = details["vice_captain_score_wizard"].string ?? "0"
                playerDetails.captainScoreReverse = details["captain_score_reverse"].string ?? "0"
                playerDetails.viceCaptainScoreReverse = details["vice_captain_score_reverse"].string ?? "0"

                if Double(pointsBatting100)! > 0.0{
                    if Double(pointsBatting100)! > 0.0{
                        playerDetails.battingCentryPoints = String((Double(pointsBatting100) ?? 0) + (Double(pointsBatting50) ?? 0))
                    }
                    else if Double(pointsBatting50)! > 0.0{
                        playerDetails.battingCentryPoints = pointsBatting50
                    }
                }
                else if Double(pointsBatting50)! > 0.0{
                    playerDetails.battingCentryPoints = pointsBatting50
                }
                
                playerDetails.bowlerWicketPoints = details["points_bowling_wickets"].string ?? "0"
                playerDetails.bowlerMaidenOverPoints = details["points_bowling_maidens"].string ?? "0"
                playerDetails.bowlerEconomyPoints = details["bowling_economy"].string ?? "0"
                playerDetails.catchPoints = details["points_fielding_catch"].string ?? "0"
                let runoutPoints = details["points_fielding_runout"].string ?? "0"
                let stumpPoints = details["points_fielding_stumped"].string ?? "0"
                
                let totalRunOutPoints = (Float(runoutPoints) ?? 0) + (Float(stumpPoints) ?? 0)
                playerDetails.runoutPoints = String(format: "%.2f", totalRunOutPoints)
                playerDetails.totalClassicCaptainScore = details["captain_score"].string ?? "0"
                playerDetails.totalClassicViceCaptainScore = details["vice_captain_score"].string ?? "0"
                playerDetails.totalClasscPlayerScore = details["player_score"].string ?? "0"
                playerDetails.totalBattingCaptainScore = details["captain_score_batting"].string ?? "0"
                playerDetails.totalBattingViceCaptainScore = details["vice_captain_score_batting"].string ?? "0"
                playerDetails.totalBattingPlayerScore = details["player_score_batting"].string ?? "0"
                
                playerDetails.totalBowlingCaptainScore = details["captain_score_bowling"].string ?? "0"
                playerDetails.totalBowlingViceCaptainScore = details["vice_captain_score_bowling"].string ?? "0"
                playerDetails.totalBowlingPlayerScore = details["player_score_bowling"].string ?? "0"
                playerDetails.pointsBowlingEconomy = details["points_bowling_economy"].string ?? "0"
                playerDetails.battingDuckPoints = details["points_batting_duck"].string ?? "0"
                playerDetails.pointsBowlingWicket4 = details["points_bowling_wicket4"].string ?? "0"
                playerDetails.pointsBowlingWicket5 = details["points_bowling_wicket5"].string ?? "0"
                
               
                
                /************************************************/
                // FOR Kabaddi
                playerDetails.raidUnsuccess = details["raid_unsuccess"].stringValue
                playerDetails.isSubstitute = details["is_substitute"].stringValue
                playerDetails.greenCard = details["green_card"].stringValue
                playerDetails.startingKabaddiPoints = details["starting_7"].stringValue
                playerDetails.yellowCard = details["yellow_card"].stringValue
                playerDetails.superTackle = details["super_tackle"].stringValue
                playerDetails.gettingAllOut = details["getting_all_out"].stringValue
                playerDetails.tacklesSuccess = details["tackles_success"].stringValue
                playerDetails.raidTouch = details["raid_touch"].stringValue
                playerDetails.raidBonus = details["raid_bonus"].stringValue
                playerDetails.pushingAllOut = details["pushing_all_out"].stringValue
                playerDetails.redCard = details["red_card"].stringValue
                /************************************************/

                
                // FOR Football'
                
                playerDetails.min_played = details["min_played"].string ?? "0"
                playerDetails.goal_scored = details["goal_scored"].string ?? "0"
                playerDetails.goal_assist = details["goal_assist"].string ?? "0"
                playerDetails.passes_completed = details["passes_completed"].string ?? "0"
                playerDetails.shot_on_target = details["shot_on_target"].string ?? "0"
                playerDetails.clean_sheet = details["clean_sheet"].string ?? "0"
                playerDetails.goal_shot_saved = details["goal_shot_saved"].string ?? "0"
                playerDetails.penality_saved = details["penality_saved"].string ?? "0"
                playerDetails.tackles_made = details["tackles_made"].string ?? "0"
                playerDetails.red_card = details["red_card"].string ?? "0"
                playerDetails.yellow_card = details["yellow_card"].string ?? "0"
                playerDetails.own_goal = details["own_goal"].string ?? "0"
                playerDetails.goal_conceded = details["goal_conceded"].string ?? "0"
                playerDetails.penality_won = details["penality_won"].string ?? "0"
                
                
                let minPlayed = details["p_min_played"].string ?? "0"
                let defaultScore = details["p_default_score"].string ?? "0"
                let playingPoint = (Int(minPlayed) ?? 0) + (Int(defaultScore) ?? 0)
                playerDetails.minPlayed = String(playingPoint)                
                playerDetails.minPlayed = details["p_min_played"].string ?? "0"
                playerDetails.goalConceded = details["p_goal_conceded"].string ?? "0"
                playerDetails.cleanSheet = details["p_clean_sheet"].string ?? "0"
                playerDetails.penalitySaved = details["p_penality_saved"].string ?? "0"
                playerDetails.goalScored = details["p_goal_scored"].string ?? "0"
                playerDetails.goalAssist = details["p_goal_assist"].string ?? "0"
                playerDetails.passesCompleted = details["p_passes_completed"].string ?? "0"
                playerDetails.shotOnTarget = details["p_shot_on_target"].string ?? "0"
                playerDetails.goalShotSaved = details["p_goal_shot_saved"].string ?? "0"
                playerDetails.tacklesMade = details["p_tackles_made"].string ?? "0"
                playerDetails.footballYellowCard = details["p_yellow_card"].string ?? "0"
                playerDetails.footballRedCard = details["p_red_card"].string ?? "0"
                playerDetails.ownGoal = details["p_own_goal"].string ?? "0"
                playerDetails.penalityWon = details["p_penality_won"].string ?? "0"
                /************************************************/

                
                // For basketball
                playerDetails.reboundsActual = details["rebounds_actual"].string ?? "0"
                playerDetails.blocks = details["blocks"].string ?? "0"
                playerDetails.turnovers = details["turnovers"].string ?? "0"
                playerDetails.steals = details["steals"].string ?? "0"
                playerDetails.blocksActual = details["blocks_actual"].string ?? "0"
                playerDetails.assistActual = details["assist_actual"].string ?? "0"
                playerDetails.turnoversActual = details["turnovers_actual"].string ?? "0"
                playerDetails.stealsActual = details["steals_actual"].string ?? "0"
                playerDetails.rebounds = details["rebounds"].string ?? "0"
                playerDetails.assist = details["assist"].string ?? "0"

                if let playerScore = details["point_scored"].string{
                    if playerScore.count != 0 {
                        playerDetails.playerScore = playerScore
                    }
                }
                
                /*************************************************************/
                
                // For Baseball
                playerDetails.baseballBBH = details["base_on_balls_bbh_or_walk"].string ?? "0"
                playerDetails.baseballBBHActual = details["base_on_balls_bbh_or_walk_actual"].string ?? "0"
                playerDetails.battleInActual = details["runs_batted_in_rbi_actual"].string ?? "0"
                playerDetails.battleIn = details["runs_batted_in_rbi"].string ?? "0"
                playerDetails.strikeOutActual = details["strikeout_so_actual"].string ?? "0"
                playerDetails.strikeOut = details["strikeout_so"].string ?? "0"
                playerDetails.runScoreActual = details["run_scored_r_actual"].string ?? "0"
                playerDetails.runScore = details["run_scored_r"].string ?? "0"
                playerDetails.tripleActual = details["triple_3b_actual"].string ?? "0"
                playerDetails.triple = details["triple_3b"].string ?? "0"
                playerDetails.hitAllowed = details["hit_allowed_h"].string ?? "0"
                playerDetails.hitAllowedActual = details["hit_allowed_h_actual"].string ?? "0"
                playerDetails.homeRunActual = details["home_run_hr_actual"].string ?? "0"
                playerDetails.homeRun = details["home_run_hr"].string ?? "0"
                playerDetails.double = details["double_2b"].string ?? "0"
                playerDetails.doubleActual = details["double_2b_actual"].string ?? "0"
                playerDetails.earnRun = details["earned_run_er"].string ?? "0"
                playerDetails.earnRunActual = details["earned_run_er_actual"].string ?? "0"
                playerDetails.single = details["single_1b"].string ?? "0"
                playerDetails.singleActual = details["single_1b_actual"].string ?? "0"
                playerDetails.inningPitch = details["inning_pitched_ip"].string ?? "0"
                playerDetails.inningPitchActual = details["inning_pitched_ip_actual"].string ?? "0"
                playerDetails.baseballBBHPitching = details["base_on_balls_bbh_or_walk_pitching"].string ?? "0"
                playerDetails.baseballBBHPitchingActual = details["base_on_balls_bbh_or_walk_pitching_actual"].string ?? "0"
                playerDetails.stolenActual = details["stolen_base_sb_actual"].string ?? "0"
                playerDetails.stolen = details["stolen_base_sb"].string ?? "0"
                playerDetails.defaultScore = details["default_score"].string ?? "0"
                playerDetails.defaultScoreActual = details["default_score_actual"].string ?? "0"
                
                
                
                
                /*********************************************/
                let isPlaying = details["is_playing"].stringValue
                
                if isPlaying == "1"{
                    playerDetails.isPlayerPlaying = true
                }
                else{
                    playerDetails.isPlayerPlaying = false
                }
                
                if let playerScore = details["scores"].string{
                    playerDetails.playerScore = playerScore
                }

                var playersRole = details["players_role"].string?.lowercased()
                if playersRole == nil {
                    playersRole = details["player_role"].string?.lowercased()
                }
                
                if "captain" == playersRole{
                    playerDetails.isCaption = true
                }
                
                if "vice_captain" == playersRole{
                    playerDetails.isViceCaption = true
                }
                
                if "wizard" == playersRole{
                    playerDetails.isWizard = true
                }
                
                if "captain_wizard" == playersRole{
                    playerDetails.isCaption = true
                    playerDetails.isWizard = true
                }
                
                if "vice_captain_wizard" == playersRole{
                    playerDetails.isViceCaption = true
                    playerDetails.isWizard = true
                }


                playerDetails.teamShortName = details["team_short_name"].string ?? ""

                playerDetailsArray.append(playerDetails)
            }
        }
        
        return playerDetailsArray
    }
    
    
    class func changeFemalePlayerPlaceholder( playerArray: Array<PlayerDetails>)  {
        
        for details in playerArray {
            details.playerPlaceholder = "PlayerFemalePlaceholder"
        }
    }
}
