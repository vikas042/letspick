//
//  BankDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/05/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class BankDetails: NSObject {

    lazy var bankName = ""
    lazy var bankBranch = ""
    lazy var ifscCode = ""
    lazy var accountNumber = ""

    
    class func getBankDetails(details: [String: JSON])-> BankDetails?{
        
        let bankDetails = BankDetails()
        
        if let bankID = details["bank_id"]?.string {
            if bankID.count == 0{
                return nil
            }
            
            bankDetails.bankName = details["bank_name"]?.string ?? ""
            bankDetails.bankBranch = details["bank_branch"]?.string ?? ""
            bankDetails.ifscCode = details["ifsc_code"]?.string ?? ""
            bankDetails.accountNumber = details["account_number"]?.string ?? ""
            return bankDetails
        }
        else{
            return nil
        }
    }
}
