//
//  WithdrawOptionDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 04/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


enum WithdrawOptionType: Int {
    case Bank = 1000
    case Paytm = 2000
}

class WithdrawOptionDetails: NSObject {
    
    var instrument_name = ""
    var instrument_type = ""
    var max_amount = ""
    var min_amount = ""
    var is_paytm_linked = ""
    var status = ""

    var withdrawType = 0

    
    var bankDetails: BankDetails?
    
    
    class func getAllPaymentMethods(detailsArray: [JSON]) -> Array<WithdrawOptionDetails> {
        var optionArray = Array<WithdrawOptionDetails>()
        
        for details in detailsArray{
            let status = details["status"].string ?? "0"
            if status == "1" {
                let withdrawOptionDetails = WithdrawOptionDetails()
                withdrawOptionDetails.instrument_name = details["instrument_name"].string ?? ""
                withdrawOptionDetails.instrument_type = details["instrument_type"].string ?? ""
                withdrawOptionDetails.max_amount = details["max_amount"].string ?? ""
                withdrawOptionDetails.min_amount = details["min_amount"].string ?? ""
                withdrawOptionDetails.is_paytm_linked = details["is_paytm_linked"].string ?? "0"
                withdrawOptionDetails.status = status

                if withdrawOptionDetails.instrument_type == "1" {
                    withdrawOptionDetails.withdrawType = WithdrawOptionType.Bank.rawValue
                }
                else if withdrawOptionDetails.instrument_type == "2" {
                    withdrawOptionDetails.withdrawType = WithdrawOptionType.Paytm.rawValue
                }

                if let bankDetailsArray = details["bankDetails"].array {
                    if bankDetailsArray.count > 0{
                        let bankDetails = bankDetailsArray[0]
                        
                        let bankDetailsModel = BankDetails()
                        bankDetailsModel.bankName = bankDetails["bank_name"].string ?? ""
                        bankDetailsModel.bankBranch = bankDetails["bank_branch"].string ?? ""
                        bankDetailsModel.accountNumber = bankDetails["account_number"].string ?? ""
                        bankDetailsModel.ifscCode = bankDetails["ifsc_code"].string ?? ""

                        withdrawOptionDetails.bankDetails = bankDetailsModel
                    }
                }
                optionArray.append(withdrawOptionDetails)
            }
        }
        
        return optionArray
    }
    

    
}
