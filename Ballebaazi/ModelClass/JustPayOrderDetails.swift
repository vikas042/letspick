//
//  JustPayOrderDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class JustPayOrderDetails: NSObject {

    lazy var orderID = ""
    lazy var statusId = ""
    lazy var customerPhone = ""
    lazy var productId = ""
    lazy var customerId = ""
    lazy var merchantId = ""
    lazy var customerEmail = ""
    lazy var currency = ""
    lazy var amount = ""
    lazy var authToken = ""

}

