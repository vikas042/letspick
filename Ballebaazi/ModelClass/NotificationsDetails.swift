//
//  NotificationsDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 12/09/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationsDetails: NSObject {

    lazy var notificationMessage = ""
    lazy var notificationDate = ""
    lazy var notificationDateAndTime = ""

    lazy var notificationTitle = ""
    lazy var notificationId = ""
    lazy var notificationImage = ""

    class func getAllNotificationList(responseArray: [JSON]) -> [NotificationsDetails] {
        
        var notificationArray = [NotificationsDetails]()
        
        for details in responseArray{
            
            let notificationDetails = NotificationsDetails()
            notificationDetails.notificationMessage = details["message"].string ?? ""
            notificationDetails.notificationId = details["notification_id"].string ?? ""
            notificationDetails.notificationDate = AppHelper.getTransactionFormattedDate(dateString: details["date_added"].string ?? "")
            notificationDetails.notificationDateAndTime = details["date_added"].string ?? ""

            notificationDetails.notificationImage = details["image"].string ?? ""
            notificationDetails.notificationTitle = details["title"].string ?? "Letspick"
            notificationArray.append(notificationDetails)
        }
        
        return notificationArray
    }
}
