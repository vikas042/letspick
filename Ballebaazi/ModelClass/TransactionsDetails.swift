

//
//  TransactionsDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 10/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class TransactionsDetails: NSObject {

    var transactionDate = ""
    var transactionTime = ""
    var bbCoins = ""

    var matchName = ""
    var transactionMessage = ""
    var leagueName = ""
    var transactionType = ""
    var transactionAmount = ""
    var firstImageName = ""
    var secondImageName = ""
    var transactionID = ""
    var isNegativeTranaction = false
    
    var rewardName = ""
    var rewardNameHindi = ""

    // For partnership
    var receiverUsername = ""
    var affiliateAmount = ""
    var byAdmin = ""

    
    class func getAllTransactionsArray(result: [String: JSON] , negativeTxnTypes: String, transactionKey: String) -> Array<TransactionsDetails> {
        
        var transactionArray = Array<TransactionsDetails>()
        if let response = result["response"]?.dictionary{
        if let responseArray = response[transactionKey]?.array{
            
            for details in responseArray{
                
                let transactionDetails = TransactionsDetails()
                
                let transactionDate = AppHelper.getTransactionFormattedDate(dateString: details["transaction_date"].string ?? "")

                transactionDetails.transactionDate = transactionDate
                transactionDetails.transactionTime = AppHelper.getTransactionFormattedTime(dateString: details["transaction_date"].string ?? "")

                transactionDetails.rewardName = details["reward_name"].string ?? "N/A"
                transactionDetails.rewardNameHindi = details["reward_name_hi"].string ?? "N/A"

                transactionDetails.matchName = details["match_name"].string ?? ""
                transactionDetails.transactionMessage = details["transaction_message"].string ?? ""
                transactionDetails.leagueName = details["league_name"].string ?? ""
                transactionDetails.transactionType = details["transaction_type"].string ?? ""
                transactionDetails.transactionAmount = details["amount"].string ?? ""
                transactionDetails.bbCoins = details["bbcoins"].string ?? "0"
                if transactionDetails.bbCoins.count == 0{
                    transactionDetails.bbCoins = "0"
                }
                transactionDetails.transactionID = details["row_id"].string ?? ""
                transactionDetails.byAdmin = details["by_admin"].stringValue

                if negativeTxnTypes.count > 0{
                    let transActionIDs = negativeTxnTypes.components(separatedBy: ",")
                    for Id in transActionIDs {
                        if Id == transactionDetails.transactionType{
                            transactionDetails.isNegativeTranaction = true
                            break;
                        }
                    }
                }
            
                if let firstTeamImageName = details["team_a_flag"].string{
                    transactionDetails.firstImageName = UserDetails.sharedInstance.teamImageUrl + firstTeamImageName
                }
                
                if let secondTeamImageName = details["team_b_flag"].string{
                    transactionDetails.secondImageName = UserDetails.sharedInstance.teamImageUrl + secondTeamImageName
                }
                transactionArray.append(transactionDetails)
            }
        }
        }
        
        return transactionArray
    }
    
    class func getAllPartnershipTransactionsArray(responseArray: [JSON]) -> Array<TransactionsDetails> {
        
        var transactionArray = Array<TransactionsDetails>()
        
        for details in responseArray{
            
            let transactionDetails = TransactionsDetails()
            
            let transactionDate = AppHelper.getTransactionFormattedDate(dateString: details["date_added"].string ?? "")
            transactionDetails.transactionDate = transactionDate
            transactionDetails.matchName = details["match_name"].string ?? ""
            transactionDetails.receiverUsername = details["receiver_username"].string ?? ""

            transactionDetails.affiliateAmount = details["affiliate_amount"].stringValue
            transactionArray.append(transactionDetails)
        }
        
        return transactionArray
    }
}



