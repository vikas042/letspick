//
//  ClaimButton.swift
//  Letspick
//
//  Created by Vikash Rajput on 13/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit

class ClaimButton: UIButton {
 
    func updateLayerProperties() {

        layer.cornerRadius = 5.0;
        titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 14)
        setTitleColor(UIColor(red: 181.0/255, green: 184.0/255, blue: 187.0/255, alpha: 1), for: .disabled)
        setTitleColor(UIColor.white, for: .normal)

        if isEnabled{
            backgroundColor = UIColor(red: 40.0/255, green: 141.0/255, blue: 233.0/255, alpha: 1)
        }
        else{
            backgroundColor = UIColor(red: 244.0/255, green: 247.0/255, blue: 249.0/255, alpha: 1)
        }
        
        if isSelected{
            backgroundColor = UIColor(red: 176.0/255, green: 210.0/255, blue: 247.0/255, alpha: 1)
        }

    }


    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayerProperties()
    }
}
