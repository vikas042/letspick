//
//  AnnouncementDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/07/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class AnnouncementDetails: NSObject {

    var message = ""
    
    class func parseAnnoumentDetails(details: [String: JSON]) -> AnnouncementDetails {
        let announcementDetails = AnnouncementDetails()
        announcementDetails.message = details["message"]?.string ?? ""
        return announcementDetails
    }
}
