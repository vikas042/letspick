//
//  WebServiceHandler.swift
//  Posh_IT
//
//  Created by Vikash Rajput on 3/8/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WebServiceHandler: NSObject {
    
    class func performGETRequest(withURL urlString: String, completion completionBlock: @escaping (_ result: JSON?, _ error: Error?) -> Void) {
        
        let apiToken = UserDetails.sharedInstance.accessToken
        print(urlString);
        let osVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String

        var language = "en"
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                language = "hi"
            }
        }
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "accessToken": apiToken,
            "loginId": UserDetails.sharedInstance.userID,
            "platform": "iOS",
            "osVersion": osVersion,
            "versionCode": appVersion,
            "versionName": buildVersion,
            "language": language
        ]
        
        Alamofire.request(urlString, method : .get, parameters : nil, encoding : JSONEncoding.default , headers : headers).responseData { dataResponse in
            
            let statusCode = dataResponse.response?.statusCode
            
            if statusCode == 200{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                if responseJSON.dictionary != nil{
                    let statusCodeOnResponse = responseJSON.dictionary!["status"]?.string
                    if let closingTime = responseJSON.dictionary!["closing_ts"]?.stringValue {
                        UserDetails.sharedInstance.closingTimeForMatch = Int(closingTime)!
                    }
                    
                    if let iosUpdateUrl = responseJSON.dictionary!["ios_update_url"]?.stringValue {
                        UserDetails.sharedInstance.updateAppUrl = iosUpdateUrl
                    }
                    
                    
                    if let unserMaintenance = responseJSON.dictionary!["under_maintenance"]?.stringValue {
                        
                        if unserMaintenance == "yes"{
                            
                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                
                                if !(navVC.viewControllers.last != nil) is UnderMaintenanceViewController{
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let underMaintenanceVC = storyboard.instantiateViewController(withIdentifier: "UnderMaintenanceViewController")
                                    navVC.pushViewController(underMaintenanceVC, animated: true)
                                }
                            }
                        }
                    }
                    
                    if let maxTeamForClassic = responseJSON.dictionary!["max_teams"]?.stringValue {
                        
                        if urlString.contains("kabaddi") {
                            UserDetails.sharedInstance.maxTeamAllowedForKabaddi = Int(maxTeamForClassic) ?? 4;
                        }
                        else if urlString.contains("football") {
                            UserDetails.sharedInstance.maxTeamAllowedForFootball = Int(maxTeamForClassic) ?? 4;
                        }
                        else if urlString.contains("basketball") {
                            UserDetails.sharedInstance.maxTeamAllowedForBasketball = Int(maxTeamForClassic) ?? 4;
                        }
                        else if urlString.contains("baseball") {
                            UserDetails.sharedInstance.maxTeamAllowedForBaseball = Int(maxTeamForClassic) ?? 4;
                        }
                        else{
                            UserDetails.sharedInstance.maxTeamAllowedForClassic = Int(maxTeamForClassic) ?? 4;
                            UserDetails.sharedInstance.maxTeamAllowedForReverse = Int(maxTeamForClassic) ?? 4;
                            UserDetails.sharedInstance.maxTeamAllowedForWizard = Int(maxTeamForClassic) ?? 4;

                        }
                    }
                    
                    if let maxTeamForBowling = responseJSON.dictionary!["max_teams_other"]?.stringValue {
                        UserDetails.sharedInstance.maxTeamAllowedForBatting = Int(maxTeamForBowling) ?? 4;
                        UserDetails.sharedInstance.maxTeamAllowedForBowling = Int(maxTeamForBowling) ?? 4;
                    }
                    
                    let serverTimestemp = responseJSON.dictionary!["server_timestamp"]?.stringValue
                    UserDetails.sharedInstance.serverTimeStemp = serverTimestemp ?? "0"
                    if let defaultAmount = responseJSON.dictionary!["payment_default_amount"]?.stringValue {
                        UserDetails.sharedInstance.defalutAddCashAmount = defaultAmount
                    }
                    
                    if let notificationsCount = responseJSON.dictionary!["new_notifications"]?.stringValue{
                        UserDetails.sharedInstance.notificationCount = notificationsCount;
                    }
                    
                    if let referralAmount = responseJSON.dictionary!["referral_amount"]?.stringValue {
                        UserDetails.sharedInstance.referralAmount = referralAmount
                        UserDefaults.standard.set(referralAmount, forKey: "referral_amount")
                        UserDefaults.standard.synchronize()
                    }
                    
                    if !UserDetails.sharedInstance.isLiveServerUpdate{
                        
                        UserDetails.sharedInstance.isLiveServerUpdate = true
                        UserDetails.sharedInstance.serverTimeStemp = serverTimestemp ?? "0"
                    }
                    
                    if statusCodeOnResponse == "200" {
                        AppHelper.updateValueInCoreData(urlString: urlString, resultData: dataResponse.data!)
                        let savedJson = AppHelper.getValueFromCoreData(urlString: urlString)
                        
                        if let userDetails = responseJSON.dictionary!["this_user"] {
                            
                            do {
                                let data = try userDetails.rawData()
                                AppHelper.updateValueInCoreData(urlString: userDetailsUrl, resultData: data)
                                if let savedUserJson = AppHelper.getValueFromCoreData(urlString: userDetailsUrl){
                                    UserDetails.sharedInstance.getAllUserDetails(details: savedUserJson)
                                }
                            } catch let error as NSError {
                                print("Could not save. \(error), \(error.userInfo)")
                            }
                        }
                        
                        completionBlock(savedJson, dataResponse.error)
                    }
                    else if statusCodeOnResponse == "801"{
                        AppHelper.callGenrateUserAccessTokenAPIWithLoader(isNeedToShowLoader: false, completionBlock: {
                            WebServiceHandler.performGETRequest(withURL: urlString, completion: { (result, error) in
                                completionBlock(result, error)
                                
                            }) })
                    }
                    else{
                        completionBlock(responseJSON, dataResponse.error)
                    }
                }
                else{
                    print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                    completionBlock(nil, dataResponse.error)
                }
            }
            else{
                print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                completionBlock(nil, dataResponse.error)
            }
        }
    }
    
    class func performPOSTRequest(urlString: String?, andParameters params: Parameters?, andAcessToken accessToken: String?, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
        
        var apiToken = accessToken
        if apiToken == nil {
            apiToken = ""
        }
        
        let osVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String

        var language = "en"
        
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            if "hi" == lang[0]{
                language = "hi"
            }
        }
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "accessToken": apiToken!,
            "loginId": UserDetails.sharedInstance.userID,
            "platform": "iOS",
            "osVersion": osVersion,
            "versionCode": appVersion,
            "versionName": buildVersion,
            "language": language
        ]
    
        print(params!)
        Alamofire.request(urlString!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
//                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in

                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    let responseJSON = JSON(response.data as AnyObject);
                    print("responseJSON::", responseJSON)
                    
                    

                    if let statusCodeOnResponse = responseJSON.dictionary?["status"]?.string{
                        if !UserDetails.sharedInstance.isLiveServerUpdate{
                            let serverTimestemp = responseJSON.dictionary!["server_timestamp"]?.stringValue
                            UserDetails.sharedInstance.isLiveServerUpdate = true
                            UserDetails.sharedInstance.serverTimeStemp = serverTimestemp ?? "0"
                        }
                        
                        if let notificationsCount = responseJSON.dictionary!["new_notifications"]?.stringValue{
                            UserDetails.sharedInstance.notificationCount = notificationsCount;
                        }
                        
                        if let defaultAmount = responseJSON.dictionary!["payment_default_amount"]?.stringValue {
                            UserDetails.sharedInstance.defalutAddCashAmount = defaultAmount
                        }

                        
                        if let unserMaintenance = responseJSON.dictionary!["under_maintenance"]?.stringValue {
                            
                            if unserMaintenance == "yes"{
                                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let underMaintenanceVC = storyboard.instantiateViewController(withIdentifier: "UnderMaintenanceViewController")
                                    navVC.pushViewController(underMaintenanceVC, animated: true)
                                }
                            }
                        }
                        
                        if let maxTeamForClassic = responseJSON.dictionary!["max_teams"]?.stringValue {
                            if urlString?.contains("kabaddi") ?? false{
                                UserDetails.sharedInstance.maxTeamAllowedForKabaddi = Int(maxTeamForClassic) ?? 4;
                            }
                            else if urlString?.contains("football") ?? false{
                                UserDetails.sharedInstance.maxTeamAllowedForFootball = Int(maxTeamForClassic) ?? 4;
                            }
                            else if urlString?.contains("basketball") ?? false{
                                UserDetails.sharedInstance.maxTeamAllowedForBasketball = Int(maxTeamForClassic) ?? 4;
                            }
                            else if urlString?.contains("baseball") ?? false{
                                UserDetails.sharedInstance.maxTeamAllowedForBaseball = Int(maxTeamForClassic) ?? 4;
                            }
                            else{
                                UserDetails.sharedInstance.maxTeamAllowedForClassic = Int(maxTeamForClassic) ?? 4;
                                UserDetails.sharedInstance.maxTeamAllowedForReverse = Int(maxTeamForClassic) ?? 4;
                                UserDetails.sharedInstance.maxTeamAllowedForWizard = Int(maxTeamForClassic) ?? 4;

                            }
                        }
                        
                        if let maxTeamForBowling = responseJSON.dictionary!["max_teams_other"]?.stringValue {
                            UserDetails.sharedInstance.maxTeamAllowedForBatting = Int(maxTeamForBowling) ?? 4;
                            UserDetails.sharedInstance.maxTeamAllowedForBowling = Int(maxTeamForBowling) ?? 4;
                        }
                        
                        if let iosUpdateUrl = responseJSON.dictionary!["ios_update_url"]?.stringValue {
                            UserDetails.sharedInstance.updateAppUrl = iosUpdateUrl
                        }
                        
                        if statusCodeOnResponse == "200" {
                            
                            if let userDetails = responseJSON.dictionary!["this_user"] {
                                
                                do {
                                    let data = try userDetails.rawData()
                                    AppHelper.updateValueInCoreData(urlString: userDetailsUrl, resultData: data)
                                    if let savedUserJson = AppHelper.getValueFromCoreData(urlString: userDetailsUrl){
                                        UserDetails.sharedInstance.getAllUserDetails(details: savedUserJson)
                                    }
                                } catch let error as NSError {
                                    print("Could not save. \(error), \(error.userInfo)")
                                }
                            }
                            completionBlock(responseJSON.dictionary, nil)
                        }
                        else if statusCodeOnResponse == "801"{
                            AppHelper.callGenrateUserAccessTokenAPIWithLoader(isNeedToShowLoader: false, completionBlock: {
                                WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken, completion: { (result, error) in
                                    completionBlock(result, error)
                                })
                            })
                        }
                        else{
                            completionBlock(responseJSON.dictionary, nil)
                        }
                        
                    }
                    else{
                        completionBlock(nil, response.error)
                    }
                 
                }
                 
                else{
                    completionBlock(nil, response.error)
                }
        }
    }
    
    class func performMultipartRequest(urlString: String?, fileName: String, params: Parameters?, imageDataArray: Array<Data>?, accessToken: String?, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
        
        var apiToken = accessToken
        if apiToken == nil {
            apiToken = ""
        }
        
        let osVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String

        let headers: HTTPHeaders = [
            "accessToken": apiToken!,
            "loginId": UserDetails.sharedInstance.userID,
            "platform": "iOS",
            "osVersion": osVersion,
            "versionCode": appVersion,
            "versionName": buildVersion
        ]
        
        print(params!)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageDataArray != nil{
                var count = 0
                for data in imageDataArray!{
                    var imageName = fileName
                    if count != 0{
                        imageName = imageName + String(count) + ".png"
                    }
                    else{
                        imageName = imageName + ".png"
                    }
                    multipartFormData.append(data, withName: "image", fileName: imageName, mimeType: "image/jpeg")
                    count += 1
                }
            }
        }, usingThreshold: UInt64.init(), to: urlString!, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.error != nil{
                        completionBlock(nil, response.error)
                        return
                    }

                    let statusCode = response.response?.statusCode
                    if statusCode == 200{
                        let responseJSON = JSON(response.data as AnyObject);
//                        print("responseJSON::", responseJSON)
                        
                        let statusCodeOnResponse = responseJSON.dictionary!["status"]?.string
                        
                        if !UserDetails.sharedInstance.isLiveServerUpdate{
                            let serverTimestemp = responseJSON.dictionary!["server_timestamp"]?.stringValue
                            UserDetails.sharedInstance.isLiveServerUpdate = true
                            UserDetails.sharedInstance.serverTimeStemp = serverTimestemp ?? "0"
                        }
                        
                        if statusCodeOnResponse == "200" {
                            
                            if let userDetails = responseJSON.dictionary!["this_user"] {
                                
                                do {
                                    let data = try userDetails.rawData()
                                    AppHelper.updateValueInCoreData(urlString: userDetailsUrl, resultData: data)
                                    if let savedUserJson = AppHelper.getValueFromCoreData(urlString: userDetailsUrl){
                                        UserDetails.sharedInstance.getAllUserDetails(details: savedUserJson)
                                    }
                                } catch let error as NSError {
                                    print("Could not save. \(error), \(error.userInfo)")
                                }
                            }
                            
                            completionBlock(responseJSON.dictionary, nil)

                        }
                        else if statusCodeOnResponse == "801"{
                            AppHelper.callGenrateUserAccessTokenAPIWithLoader(isNeedToShowLoader: false, completionBlock: {
                                
                                WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken, completion: { (result, error) in
                                    completionBlock(result, error)
                                })
                            })
                        }
                        else{
                            completionBlock(responseJSON.dictionary, nil)
                        }
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionBlock(nil, error)
            }
        }
    }
}



extension String {
    
//    static func ==(lhs: String, rhs: String) -> Bool {
//        return lhs.compare(rhs, options: .numeric) == .orderedSame
//    }
//    
    static func <(lhs: String, rhs: String) -> Bool {
        return lhs.compare(rhs, options: .numeric) == .orderedAscending
    }
    
//    static func <=(lhs: String, rhs: String) -> Bool {
//        return lhs.compare(rhs, options: .numeric) == .orderedAscending || lhs.compare(rhs, options: .numeric) == .orderedSame
//    }
//
//    static func >(lhs: String, rhs: String) -> Bool {
//        return lhs.compare(rhs, options: .numeric) == .orderedDescending
//    }
//
//    static func >=(lhs: String, rhs: String) -> Bool {
//        return lhs.compare(rhs, options: .numeric) == .orderedDescending || lhs.compare(rhs, options: .numeric) == .orderedSame
//    }
    
}
