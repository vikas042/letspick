//
//  ClaimDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 26/02/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class ClaimDetails: NSObject {

    lazy var address = ""
    lazy var rewardId = ""
    lazy var claimBBcoins = ""
    lazy var rewardClaimId = ""
    lazy var rewardType = ""
    lazy var image = ""
    lazy var rewardProductId = ""
    lazy var rewardNameHindi = ""
    lazy var rewardNameEnglish = ""
    lazy var status = ""
    lazy var date = ""


    class func getAllClaimList(dataArray: Array<JSON>) -> Array<ClaimDetails> {
        var claimDataArray = Array<ClaimDetails>()
        
        for details in dataArray {
            let claimDetails = ClaimDetails()
            
            claimDetails.address = details["address"].string ?? ""
            claimDetails.rewardId = details["reward_id"].string ?? ""
            claimDetails.claimBBcoins = details["claim_bbcoins"].string ?? ""
            claimDetails.rewardClaimId = details["reward_claim_id"].string ?? ""
            claimDetails.rewardType = details["reward_type"].string ?? ""
            let image = details["image"].string ?? ""
            if image.count > 5 {
                claimDetails.image = UserDetails.sharedInstance.promotionImageUrl + image
            }
            claimDetails.rewardProductId = details["reward_product_id"].string ?? ""
            claimDetails.rewardNameHindi = details["reward_name_hi"].string ?? ""
            claimDetails.rewardNameEnglish = details["reward_name"].string ?? ""
            claimDetails.status = details["status"].string ?? ""
            claimDetails.date = details["created_at"].string ?? ""


            claimDataArray.append(claimDetails)
        }
        
        return claimDataArray
    }

    
}
