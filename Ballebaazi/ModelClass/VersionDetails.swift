//
//  VersionDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 09/12/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class VersionDetails: NSObject {

    lazy var versionNumber = ""
    lazy var versionChanges = ""
    lazy var deviceType = ""
    lazy var versionUrl = ""

    
    class func getAlliOSVersionHistory(dataArray: Array<JSON>) -> Array<VersionDetails> {
        var versionsArray = Array<VersionDetails>()
        
        for details in dataArray {
            let deviceType = details["device_type"].stringValue
            if deviceType == "1" {
                let versionDetails = VersionDetails()
                versionDetails.versionNumber = details["version_number"].string ?? ""
                
                versionDetails.versionChanges = details["whats_new_en"].string ?? ""
                
                if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                    if "hi" == lang[0]{
                        let changesHindi = details["whats_new_hi"].string ?? ""
                        versionDetails.versionChanges = changesHindi
                    }
                }
                
                versionDetails.deviceType = "1"
                versionDetails.versionUrl = details["version_url"].string ?? ""
                versionsArray.append(versionDetails)
            }
        }
        
        return versionsArray
    }
}
