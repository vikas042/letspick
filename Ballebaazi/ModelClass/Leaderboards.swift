//
//  Leaderboards.swift
//  Letspick
//
//  Created by Vikash Rajput on 24/01/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class Leaderboards: NSObject {
    
    
    var leaderboardId = ""
    var title = ""
    var seasonName = ""
    var startDate = ""
    var endDate = ""
    var winAmount = ""
    var totalWinners = ""
    var classicImage = ""
    var battingImage = ""
    var bowlingImage = ""


    
    class func getAllLeaderboards(dataArray: [JSON]) -> [Leaderboards] {
        
        var leaderboardsArray = Array<Leaderboards>()
                
        for details in dataArray{
            let leaderboardDetails = Leaderboards()
            leaderboardDetails.leaderboardId = details["leaderboard_id"].string ?? ""
//            leaderboardDetails.leaderboardType = details["leaderboard_type"].string ?? ""
            leaderboardDetails.title = details["title"].string ?? ""
//            leaderboardDetails.seasonKey = details["season_key"].string ?? ""
            leaderboardDetails.seasonName = details["season_name"].string ?? ""
            leaderboardDetails.startDate = details["start_date"].string ?? ""
            leaderboardDetails.endDate = details["end_date"].string ?? ""
//            leaderboardDetails.totalMatches = details["total_matches"].string ?? "0"
//            leaderboardDetails.totalUsers = details["total_users"].string ?? "0"
//            leaderboardDetails.winning_text = details["winning_text"].string ?? ""
            
            leaderboardDetails.bowlingImage = details["bowling_image"].string ?? ""
            leaderboardDetails.battingImage = details["batting_image"].string ?? ""
            leaderboardDetails.classicImage = details["classic_image"].string ?? ""

            leaderboardDetails.winAmount = details["win_amount"].string ?? "0"
            leaderboardDetails.totalWinners = details["total_winners"].string ?? "0"
//            leaderboardDetails.leaderboardStatus = details["leaderboard_status"].string ?? ""
//            leaderboardDetails.dateAdded = details["date_added"].string ?? ""
//            leaderboardDetails.modifiedDate = details["modified_date"].string ?? ""
            
            leaderboardsArray.append(leaderboardDetails)
        }
        
        return leaderboardsArray
    }
}
