//
//  RewardAddressDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 07/03/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class RewardAddressDetails: NSObject {

    lazy var address = ""

    lazy var city = ""
    lazy var state = ""
    lazy var pincode = ""
    lazy var addLine2 = ""
    
    class func parseAddressDetails(details: JSON) -> RewardAddressDetails {
        let addressInfo = RewardAddressDetails()
        
        addressInfo.address = details["address"].string ?? ""
        addressInfo.city = details["city"].string ?? ""
        addressInfo.state = details["state"].string ?? ""
        addressInfo.pincode = details["pincode"].string ?? ""
        addressInfo.addLine2 = details["add_line2"].string ?? ""
        
        return addressInfo
    }

}
