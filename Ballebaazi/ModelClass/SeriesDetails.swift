//
//  SeriesDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/12/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class SeriesDetails: NSObject {
    
//    var seasonId = ""
    var seasonKey = ""
    var seasonShortName = ""
//    var isActive = true
    
    
    class func getSeriesDetails(allSeriesList: Array<JSON>?) -> Array<SeriesDetails> {
        
        var seriesDetailsArray = Array<SeriesDetails>()
        
        for details in allSeriesList! {
            let seriesDetails = SeriesDetails()
//            seriesDetails.seasonId = details["season_id"].string!
            seriesDetails.seasonKey = details["season_key"].string!
            seriesDetails.seasonShortName = details["season_short_name"].string!
//            seriesDetails.isActive = details["active"]
            seriesDetailsArray.append(seriesDetails)

        }
        
        return seriesDetailsArray
    }
    
}
