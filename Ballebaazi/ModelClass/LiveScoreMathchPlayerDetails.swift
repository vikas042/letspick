
//
//  LiveScoreMathchPlayerDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 26/07/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class LiveScoreMathchPlayerDetails: NSObject {

    var playerName = ""
    
    var battingRun = ""
    var battingBowl = ""
    var battingFours = ""
    var battingSixes = ""
    var batsmanStrikeRate = ""
    var batsmanStatusMessage = ""
    
    var bowlerRunConceded = ""
    var bowlerWicketTaken = ""
    var bowlerWideBalls = ""
    var bowlerNoBowl = ""
    var bowlerOverBowled = ""
    var bowlerMaindenOvers = ""
    var bowlerBallsBowled = ""
    var bowlerEcoRate = ""

}
