//
//  LeaderboardsUsers.swift
//  Letspick
//
//  Created by Vikash Rajput on 24/01/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class LeaderboardsUsers: NSObject {

    var leaderboardID = ""
//    var userID = ""
    var username = ""
    var allPoints = ""
//    var totalMatches = ""
    var rank = ""
    var oldRank = ""
//    var creditsWon = ""
//    var creditsAdded = ""
    var dateAdded = ""
//    var modifiedDate = ""
    var image = ""

    class func getAllLeaderboardsUsers(dataArray: [JSON]) -> [LeaderboardsUsers] {
        
        var leaderboardsArray = Array<LeaderboardsUsers>()
        
        for details in dataArray{
            
            let leaderboardDetails = LeaderboardsUsers.parseDetails(details: details)
            leaderboardsArray.append(leaderboardDetails)
        }
        
        return leaderboardsArray
    }
    
    class func parseDetails(details: JSON) -> LeaderboardsUsers{
        let leaderboardDetails = LeaderboardsUsers()
        leaderboardDetails.leaderboardID = details["leaderboard_id"].string ?? ""
//        leaderboardDetails.userID = details["user_id"].string ?? ""
        leaderboardDetails.username = details["username"].string ?? ""
        leaderboardDetails.allPoints = details["all_points"].string ?? "0"
//        leaderboardDetails.totalMatches = details["total_matches"].string ?? ""
        leaderboardDetails.rank = details["rank"].string ?? "0"
        leaderboardDetails.oldRank = details["old_rank"].string ?? "0"
//        leaderboardDetails.creditsWon = details["credits_won"].string ?? "0"
//        leaderboardDetails.creditsAdded = details["credits_added"].string ?? "0"
        leaderboardDetails.dateAdded = details["date_added"].string ?? ""
//        leaderboardDetails.modifiedDate = details["modified_date"].string ?? ""
        leaderboardDetails.image = details["image"].string ?? ""

        return leaderboardDetails
    }
    
}
