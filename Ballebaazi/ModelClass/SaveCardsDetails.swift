//
//  SaveCardsDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 24/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class SaveCardsDetails: NSObject {
    lazy var nameOnCard = ""
    lazy var cardReference = ""
    lazy var cardNumber = ""
    lazy var cardType = ""
    lazy var nickname = ""
    lazy var cardToken = ""
    lazy var cardIssuer = ""
    lazy var cardExpMonth = ""
    lazy var cardExpYear = ""
    lazy var cardBrand = ""
    lazy var cardIsin = ""
    lazy var cardFingerprint = ""
    lazy var expired = false

    class func getAllSavedCards(dataArray: Array<JSON>) -> Array<SaveCardsDetails> {
        var savedCardArray = Array<SaveCardsDetails>()
        for details in dataArray {
            let cardDetails = SaveCardsDetails()
            
            cardDetails.nameOnCard = details["nameOnCard"].string ?? ""
            cardDetails.cardReference = details["cardReference"].string ?? ""
            cardDetails.cardNumber = details["cardNumber"].string ?? ""
            cardDetails.cardType = details["cardType"].string ?? ""
            cardDetails.nickname = details["nickname"].string ?? ""
            cardDetails.cardToken = details["cardToken"].string ?? ""
            cardDetails.cardIssuer = details["cardIssuer"].string ?? ""
            cardDetails.cardExpMonth = details["cardExpMonth"].string ?? ""
            cardDetails.cardExpYear = details["cardExpYear"].string ?? ""
            cardDetails.cardBrand = details["cardBrand"].string ?? ""
            cardDetails.cardIsin = details["cardIsin"].string ?? ""
            cardDetails.cardFingerprint = details["cardFingerprint"].string ?? ""
            cardDetails.expired = details["expired"].boolValue

            savedCardArray.append(cardDetails)
        }
        return savedCardArray
    }
}
