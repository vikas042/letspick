//
//  PendingRequest.swift
//  Letspick
//
//  Created by Vikash Rajput on 17/12/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class PendingRequest: NSObject {
    var withdrawID = ""
    var amount = ""
    var requestedDate = ""
    var withdraw_tds = ""

    
    
    class func parsePendingRequest(withdrawData: [String: JSON]) -> PendingRequest {
        let pendingRequest = PendingRequest()
        pendingRequest.withdrawID = withdrawData["withdraw_id"]?.stringValue ?? ""
        pendingRequest.amount = withdrawData["amount"]?.stringValue ?? ""
        pendingRequest.requestedDate = withdrawData["date_requested"]?.stringValue ?? ""
        pendingRequest.withdraw_tds = withdrawData["withdraw_tds"]?.stringValue ?? ""

        return pendingRequest
    }
    
}
