//
//  LeagueDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/12/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class LeagueDetails: NSObject {    
    
    lazy var leagueId = ""
    lazy var referenceLeagueID = ""
    lazy var leagueWinnerType = ""
    lazy var fantasyType = ""
    lazy var matchKey = ""
    lazy var totalWinners = ""
    lazy var leagueType = ""
    lazy var isInfinity = ""
    lazy var totalWinnersPercent = ""
    lazy var leagueMsg = ""
    lazy var winPerUser = ""
    lazy var maxPlayers = "0"
    lazy var totalQuestions = "0"

    lazy var totalJoined = "0"
    lazy var joiningAmount = ""
    lazy var templateID = ""
    lazy var isJackpot = ""
    lazy var bannerImages = Array<String>()
    lazy var isTicketAvailable = false
    lazy var isPassAvailable = false

    var timeBasedBonusArray = Array<JSON>()
    var bonusApplicable: String?
    var dynamicLeague = ""

    lazy var bounsPercentage = "0"
    lazy var categoryId = ""
    var leagueName: String?
    var teamType: String?
    var confirmedLeague: String?
    var winAmount: String?
    var joinedLeagueCount = 0
    var isPrivateLeague = false
    
    class func getCategoryLeagueDetails(responseResult: JSON) -> Array<LeagueDetails> {
        
        var leagueDetailsArray = Array<LeagueDetails>()
        let response =  responseResult.dictionary!["response"]
        
        if let matchLeague = response?.dictionary!["match_leagues"]?.array{
            for details in matchLeague {
                
                let leagueDetails = LeagueDetails()
                leagueDetails.leagueId = details["league_id"].stringValue
                leagueDetails.referenceLeagueID = details["reference_league"].stringValue
                
                leagueDetails.categoryId = details["category"].stringValue

                leagueDetails.fantasyType = details["fantasy_type"].string!
                leagueDetails.matchKey = details["match_key"].string!
                leagueDetails.leagueName = details["league_name"].string
                leagueDetails.teamType = details["team_type"].string
                leagueDetails.winPerUser = details["win_per_user"].string ?? "0"
                leagueDetails.templateID = details["template_id"].string ?? "0"
                leagueDetails.confirmedLeague = details["confirmed_league"].string
                leagueDetails.leagueWinnerType = details["league_winner_type"].string ?? ""
                
                if let bonusJSON = details["time_based_bonus"].string{
                    let json = JSON(parseJSON: bonusJSON)
                    if (json.array != nil){
                        leagueDetails.timeBasedBonusArray = json.array!
                    }
                }

                let bannerImages = details["banner_image"].string ?? ""
                if bannerImages.count > 0 {
                    leagueDetails.bannerImages = bannerImages.components(separatedBy: ",")
                }

                leagueDetails.bonusApplicable = details["bonus_applicable"].string
                leagueDetails.totalJoined = details["total_joined"].stringValue
                if leagueDetails.totalJoined.count == 0{
                    leagueDetails.totalJoined = "0"
                }
                leagueDetails.leagueType = details["league_type"].string ?? ""
                leagueDetails.isJackpot = details["is_jackpot"].stringValue
                leagueDetails.isInfinity = details["is_infinity"].string ?? ""
                leagueDetails.maxPlayers = details["max_players"].stringValue
                if leagueDetails.maxPlayers.count == 0{
                    leagueDetails.maxPlayers = "0"
                }

                let joiningAmout = details["joining_amount"].string ?? "0"
                leagueDetails.joiningAmount = String(Int((Float(joiningAmout) ?? 0)))
                
                leagueDetails.winAmount = details["win_amount"].string
                leagueDetails.totalWinners = details["total_winners"].string ?? ""
                leagueDetails.totalWinnersPercent = details["total_winners_percent"].string ?? ""
                leagueDetails.leagueMsg = details["league_msg"].string ?? ""
                let teamsGroup = details["user_teams_group"].string ?? ""
                if teamsGroup.count > 0{
                    let teamsGroupArray = teamsGroup.components(separatedBy: ",")
                    leagueDetails.joinedLeagueCount = teamsGroupArray.count
                }

                if let userTeam = details["user_teams"].array{
                    leagueDetails.joinedLeagueCount = userTeam.count
                }
                
                if let bonusPercent = details["bonus_percent"].string{
                    leagueDetails.bounsPercentage = bonusPercent
                }
                
                leagueDetailsArray.append(leagueDetails)
            }
        }
        
        return leagueDetailsArray
    }
    
    class func getAllLeagueDetails(responseResult: JSON, matchDetails: MatchDetails) -> Array<LeagueDetails> {
        
        var leagueDetailsArray = Array<LeagueDetails>()        
        let response =  responseResult.dictionary!["response"]
        
        if let userLeague = response?.dictionary!["user_teams"]?.array{
            if let flagDict = response?.dictionary!["team_flags"]{
                UserDetails.sharedInstance.teamFlag = flagDict;
            }

            UserDetails.sharedInstance.userTeamsArray = UserTeamDetails.getUserTeamsArray(responseArray: userLeague, matchDetails: matchDetails)
        }
        
        if let matchLeague = response?.dictionary!["match_leagues"]?.array{
            for details in matchLeague {
                
                let leagueDetails = LeagueDetails()
                leagueDetails.leagueId = details["league_id"].stringValue
                leagueDetails.referenceLeagueID = details["reference_league"].stringValue
                leagueDetails.fantasyType = details["fantasy_type"].string!
                leagueDetails.matchKey = details["match_key"].string!
                leagueDetails.leagueName = details["league_name"].string
                leagueDetails.teamType = details["team_type"].string
                leagueDetails.winPerUser = details["win_per_user"].string ?? "0"
                leagueDetails.templateID = details["template_id"].string ?? "0"
                leagueDetails.isJackpot = details["is_jackpot"].stringValue
                leagueDetails.categoryId = details["category"].stringValue

                leagueDetails.leagueWinnerType = details["league_winner_type"].string ?? ""
                if let bonusJSON = details["time_based_bonus"].string{
                    let json = JSON(parseJSON: bonusJSON)
                    if (json.array != nil){
                        leagueDetails.timeBasedBonusArray = json.array!
                    }
                }

                let bannerImages = details["banner_image"].string ?? ""
                if bannerImages.count > 0 {
                    leagueDetails.bannerImages = bannerImages.components(separatedBy: ",")
                }

                leagueDetails.confirmedLeague = details["confirmed_league"].string
                leagueDetails.bonusApplicable = details["bonus_applicable"].string
                leagueDetails.totalJoined = details["total_joined"].stringValue
                if leagueDetails.totalJoined.count == 0{
                    leagueDetails.totalJoined = "0"
                }

                leagueDetails.leagueType = details["league_type"].string ?? ""
                leagueDetails.isInfinity = details["is_infinity"].string ?? ""
                leagueDetails.maxPlayers = details["max_players"].stringValue
                if leagueDetails.maxPlayers.count == 0{
                    leagueDetails.maxPlayers = "0"
                }

                let joiningAmout = details["joining_amount"].string ?? "0"
                leagueDetails.joiningAmount = String(Int((Float(joiningAmout) ?? 0)))

                leagueDetails.winAmount = details["win_amount"].string
                leagueDetails.totalWinners = details["total_winners"].string ?? ""
                leagueDetails.totalWinnersPercent = details["total_winners_percent"].string ?? ""
                leagueDetails.leagueMsg = details["league_msg"].string ?? ""
                let teamsGroup = details["user_teams_group"].string ?? ""
                if teamsGroup.count > 0{
                    let teamsGroupArray = teamsGroup.components(separatedBy: ",")
                    leagueDetails.joinedLeagueCount = teamsGroupArray.count
                }

                if let userTeam = details["user_teams"].array{
                    leagueDetails.joinedLeagueCount = userTeam.count
                }
                print(details)
                if let bonusPercent = details["bonus_percent"].string{
                    leagueDetails.bounsPercentage = bonusPercent
                }
                leagueDetailsArray.append(leagueDetails)
            }
        }
        
        return leagueDetailsArray
    }
    
    
    class func getAllQuizLeagueDetails(dataArray: [JSON]) -> Array<LeagueDetails> {
              
        var leagueDetailsArray = Array<LeagueDetails>()
        for details in dataArray {
            
            let leagueDetails = LeagueDetails()
            
            leagueDetails.leagueId = details["league_id"].stringValue
            leagueDetails.referenceLeagueID = details["reference_league"].stringValue
            leagueDetails.fantasyType = details["fantasy_type"].string ?? "1"
            leagueDetails.matchKey = details["match_key"].string!
            leagueDetails.leagueName = details["league_name"].string
            leagueDetails.teamType = details["team_type"].string
            leagueDetails.winPerUser = details["win_per_user"].string ?? "0"
            leagueDetails.templateID = details["template_id"].string ?? "0"
            leagueDetails.isJackpot = details["is_jackpot"].stringValue
            leagueDetails.categoryId = details["category"].stringValue
            leagueDetails.dynamicLeague = details["dynamic_league"].stringValue
            leagueDetails.leagueMsg = details["league_msg"].string ?? ""


            leagueDetails.leagueWinnerType = details["league_winner_type"].string ?? ""
            if let bonusJSON = details["time_based_bonus"].string{
                let json = JSON(parseJSON: bonusJSON)
                if (json.array != nil){
                    leagueDetails.timeBasedBonusArray = json.array!
                }
            }

            let bannerImages = details["banner_image"].string ?? ""
            if bannerImages.count > 0 {
                leagueDetails.bannerImages = bannerImages.components(separatedBy: ",")
            }

            leagueDetails.confirmedLeague = details["confirmed_league"].string
            leagueDetails.bonusApplicable = details["bonus_applicable"].string
            leagueDetails.totalJoined = details["total_joined"].stringValue
            if leagueDetails.totalJoined.count == 0{
                leagueDetails.totalJoined = "0"
            }

            leagueDetails.leagueType = details["league_type"].string ?? ""
            leagueDetails.isInfinity = details["is_infinity"].string ?? ""
            leagueDetails.maxPlayers = details["max_players"].stringValue
            if leagueDetails.maxPlayers.count == 0{
                leagueDetails.maxPlayers = "0"
            }

            leagueDetails.totalQuestions = details["question_count"].stringValue
            let joiningAmout = details["joining_amount"].string ?? "0"
            leagueDetails.joiningAmount = String(Int((Float(joiningAmout) ?? 0)))

            leagueDetails.winAmount = details["win_amount"].string
            leagueDetails.totalWinners = details["total_winners"].string ?? ""
            leagueDetails.totalWinnersPercent = details["total_winners_percent"].string ?? ""
            leagueDetails.leagueMsg = details["league_msg"].string ?? ""
            let teamsGroup = details["user_teams_group"].string ?? ""
            if teamsGroup.count > 0{
                let teamsGroupArray = teamsGroup.components(separatedBy: ",")
                leagueDetails.joinedLeagueCount = teamsGroupArray.count
            }

            if let userTeam = details["user_teams"].array{
                leagueDetails.joinedLeagueCount = userTeam.count
            }
            print(details)
            if let bonusPercent = details["bonus_percent"].string{
                leagueDetails.bounsPercentage = bonusPercent
            }
            leagueDetailsArray.append(leagueDetails)
        }

        return leagueDetailsArray
    }
    
    class func getLeagueDetails(details: [String : JSON]) -> LeagueDetails {
        
        let leagueDetails = LeagueDetails()
        leagueDetails.leagueId = details["league_id"]?.stringValue ?? ""
        leagueDetails.referenceLeagueID = details["reference_league"]!.stringValue

        leagueDetails.fantasyType = details["fantasy_type"]?.string ?? ""
        leagueDetails.templateID = details["template_id"]?.string ?? ""
        leagueDetails.matchKey = details["match_key"]?.string ?? ""
        leagueDetails.leagueName = details["league_name"]?.string ?? ""
        leagueDetails.teamType = details["team_type"]?.string ?? ""
        leagueDetails.confirmedLeague = details["confirmed_league"]?.string ?? ""
        leagueDetails.categoryId = details["category"]?.stringValue ?? ""

        leagueDetails.bonusApplicable = details["bonus_applicable"]?.string ?? ""
        leagueDetails.winPerUser = details["win_per_user"]?.string ?? "0"
        leagueDetails.isJackpot = details["is_jackpot"]?.stringValue ?? ""
        leagueDetails.leagueWinnerType = details["league_winner_type"]?.string ?? ""
        
        let bannerImages = details["banner_image"]?.string ?? ""
        if bannerImages.count > 0 {
            leagueDetails.bannerImages = bannerImages.components(separatedBy: ",")
        }

        leagueDetails.totalJoined = details["total_joined"]?.stringValue ?? "0"
        if leagueDetails.totalJoined.count == 0{
            leagueDetails.totalJoined = "0"
        }

        leagueDetails.leagueType = details["league_type"]?.string ?? ""
        leagueDetails.isInfinity = details["is_infinity"]?.string ?? ""
        leagueDetails.maxPlayers = details["max_players"]?.stringValue ?? "0"
        
        if leagueDetails.maxPlayers.count == 0{
            leagueDetails.maxPlayers = "0"
        }

        let joiningAmout = details["joining_amount"]?.string ?? "0"
        leagueDetails.joiningAmount = String(Int((Float(joiningAmout) ?? 0)))

        leagueDetails.winAmount = details["win_amount"]?.string ?? ""
        leagueDetails.totalWinners = details["total_winners"]?.string ?? ""
        leagueDetails.totalWinnersPercent = details["total_winners_percent"]?.string ?? ""
        leagueDetails.leagueMsg = details["league_msg"]?.string ?? ""
        let teamsGroup = details["user_teams_group"]?.string ?? ""
        
        if let bonusJSON = details["time_based_bonus"]?.string{
            let json = JSON(parseJSON: bonusJSON)
            if (json.array != nil){
                leagueDetails.timeBasedBonusArray = json.array!
            }
        }

        
        if teamsGroup.count > 0{
            let teamsGroupArray = teamsGroup.components(separatedBy: ",")
            leagueDetails.joinedLeagueCount = teamsGroupArray.count
        }

        if let userTeam = details["user_teams"]?.array{
            leagueDetails.joinedLeagueCount = userTeam.count
        }
        
        if let bonusPercent = details["bonus_percent"]?.string{
            leagueDetails.bounsPercentage = bonusPercent
        }
        return leagueDetails
    }
    
    
    class func getJoinedLeagueCounts(responseResult: JSON) -> (totalClassicTeam: String, totalBattingTeam: String, totalBowlingTeam: String, totalReverseTeam: String, totalWizardTeam: String) {
        
        let response =  responseResult.dictionary!["response"]
        var classicTeam = response?.dictionary!["total_classic_leagues"]?.stringValue ?? "0"
        var batingTeam = response?.dictionary!["total_batting_leagues"]?.stringValue ?? "0"
        var bowlingTeam = response?.dictionary!["total_bowling_leagues"]?.stringValue ?? "0"
        var reverseTeam = response?.dictionary!["total_reverse_leagues"]?.stringValue ?? "0"
        var wizardTeam = response?.dictionary!["total_wizard_leagues"]?.stringValue ?? "0"

        if classicTeam.count == 0 {
            classicTeam = "0"
        }
        
        if batingTeam.count == 0 {
            batingTeam = "0"
        }

        if bowlingTeam.count == 0 {
            bowlingTeam = "0"
        }

        if reverseTeam.count == 0 {
            reverseTeam = "0"
        }

        if wizardTeam.count == 0 {
            wizardTeam = "0"
        }

        return (classicTeam,batingTeam,bowlingTeam, reverseTeam, wizardTeam)
    }
}
