//
//  ReferalDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 19/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReferalDetails: NSObject {

    lazy var username = ""
    lazy var referralCode = ""
    lazy var lastPlayed = ""
    lazy var dateAdded = ""

    lazy var name = ""
    lazy var totalEarnings = ""
   
    
    class func getAllReferalDetails(dataArray: Array<JSON>) -> Array<ReferalDetails> {
        var referalsArray = Array<ReferalDetails>()
        
        for details in dataArray {
            referalsArray.append(ReferalDetails.parseReferalDetails(details: details))
        }
        
        return referalsArray
    }
    
    class func parseReferalDetails(details: JSON) -> ReferalDetails {
        let referalDetails = ReferalDetails()
        
        referalDetails.username = details["username"].string ?? ""
        referalDetails.referralCode = details["referral_code"].string ?? ""
        let lastPlayedDate = AppHelper.getTransactionFormattedDate(dateString: details["last_contest_date"].string ?? "")

        referalDetails.lastPlayed = lastPlayedDate
        referalDetails.dateAdded = AppHelper.getRequiredFormattedDate(dateString: details["date_added"].string ?? "")

        referalDetails.name = details["name"].string ?? ""
        referalDetails.totalEarnings = details["total_earnings"].stringValue 
        
        return referalDetails
    }

}
