
//
//  BannerDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/03/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class BannerDetails: NSObject {

    var image = ""
    var promotionID = ""
    var bannerDescription = ""
    var websiteUrl = ""
    var matchKey = ""
    var videoUrl = ""
    var title = ""
    var redirectType = ""
    var gameType = ""
    var sortingOrder = "0"
    var redirectSportType = ""
    var endDate = ""
    var endDateTimestamp = ""
    var buttonTxt = ""

    
    class func getPromoBannerDetails(dataArray: [JSON]) -> [BannerDetails]{
        
        var bannerDetailsArray = Array<BannerDetails>()
        for details in dataArray{
            let bannerDetails = BannerDetails()
            bannerDetails.image = details["image"].string ?? ""
            bannerDetails.promotionID = details["promotion_id"].string ?? ""
            bannerDetails.bannerDescription = details["description"].string ?? ""
            bannerDetails.websiteUrl = details["website_url"].string ?? ""
            bannerDetails.matchKey = details["match_key"].string ?? ""
            bannerDetails.videoUrl = details["video_url"].string ?? ""
            bannerDetails.endDate = details["end_date"].string ?? ""
            bannerDetails.buttonTxt = details["button_txt"].string ?? "Go To"


            if bannerDetails.endDate.count != 0 {
                let dfmatter = DateFormatter()
                dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
                let date = dfmatter.date(from: bannerDetails.endDate)
                let dateStamp = date!.timeIntervalSince1970
                bannerDetails.endDateTimestamp = String(dateStamp)
            }
            
            bannerDetails.title = details["title"].string ?? ""
            bannerDetails.redirectType = details["redirect_type"].stringValue
            bannerDetails.gameType = details["play_type"].stringValue
            bannerDetails.sortingOrder = details["sorting_order"].stringValue
            bannerDetails.redirectSportType = details["redirect_sport_type"].stringValue

            bannerDetailsArray.append(bannerDetails)
        }
        bannerDetailsArray = bannerDetailsArray.sorted(by: { (details, nextDetails) -> Bool in
            return Int(details.sortingOrder) ?? 0 < Int(nextDetails.sortingOrder) ?? 0
        })
        return bannerDetailsArray
    }
    
    class func parseBannerDetails(details: JSON) -> BannerDetails {
                    
        let bannerDetails = BannerDetails()
        bannerDetails.image = details["image"].string ?? ""
        bannerDetails.promotionID = details["promotion_id"].string ?? ""
        bannerDetails.bannerDescription = details["description"].string ?? ""
        bannerDetails.websiteUrl = details["website_url"].string ?? ""
        bannerDetails.matchKey = details["match_key"].string ?? ""
        bannerDetails.videoUrl = details["video_url"].string ?? ""
        bannerDetails.endDate = details["end_date"].string ?? ""
        bannerDetails.buttonTxt = details["button_txt"].string ?? "Go To"

        if bannerDetails.endDate.count != 0 {
            let dfmatter = DateFormatter()
            dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
            let date = dfmatter.date(from: bannerDetails.endDate)
            let dateStamp = date!.timeIntervalSince1970
            bannerDetails.endDateTimestamp = String(dateStamp)
        }
        
        bannerDetails.title = details["title"].string ?? ""
        bannerDetails.redirectType = details["redirect_type"].stringValue
        bannerDetails.gameType = details["play_type"].stringValue
        bannerDetails.sortingOrder = details["sorting_order"].stringValue
        bannerDetails.redirectSportType = details["redirect_sport_type"].stringValue
        return bannerDetails;
    }

}


