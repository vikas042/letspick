//
//  JackpotBannerDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 18/10/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class JackpotBannerDetails: NSObject {
    var image = ""
    
    class func getAllBannersList(dataArray: [JSON]) -> Array<JackpotBannerDetails> {
        var bannerArray = Array<JackpotBannerDetails>()
        
        for details in dataArray {
            let bannerDetails = JackpotBannerDetails()
            
            let imageUrl = details["banner_image"].string ?? ""
            bannerDetails.image = imageUrl
            bannerArray.append(bannerDetails)
        }
        
        return bannerArray
    }
    
}




