//
//  LeagueWinnersRank.swift
//  Letspick
//
//  Created by Vikash Rajput on 29/06/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class LeagueWinnersRank: NSObject {

    var leagueID: String?
    var rankCount: String?
    var winAmount: String?
    var winProduct = ""
    var ticketName = ""
    var winPercent = ""
    var bbcoins = ""


    
    class func getAllRankDetails(response: [String : JSON]) -> Array<LeagueWinnersRank> {
        var rankDetailsArray = Array<LeagueWinnersRank>()
        
        if let ranksArray = response["response"]?.array{
            
            for details in ranksArray{
                let rankDetails = LeagueWinnersRank()                
                let rankFrom = details["win_from"].string
                let rankTo = details["win_to"].string                
                if rankFrom == rankTo{
                    rankDetails.rankCount = rankTo!
                }
                else{
                    rankDetails.rankCount = rankFrom! + "-" + rankTo!
                }
                
                rankDetails.rankCount = "Rank".localized() + " :" + rankDetails.rankCount!
                rankDetails.winProduct = details["win_product"].string ?? ""
                rankDetails.ticketName = details["ticket_name"].string ?? ""
                rankDetails.winPercent = details["win_percent"].string ?? ""
                rankDetails.winAmount = details["win_amount"].stringValue
                rankDetails.bbcoins = details["bbcoins"].stringValue

                rankDetailsArray.append(rankDetails)
            }
        }
        
        return rankDetailsArray
        
    }
    
}
