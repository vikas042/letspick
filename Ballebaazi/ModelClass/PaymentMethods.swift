//
//  PaymentMethods.swift
//  Letspick
//
//  Created by Vikash Rajput on 08/01/20.
//  Copyright © 2020 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class PaymentMethods: NSObject {
    
    lazy var paymentMethod = ""
    lazy var paymentMethodType = ""
    lazy var paymentMethodDescription = ""

    class func getAllPaymentOption(dataArray: [JSON]) -> [PaymentMethods] {
        var paymentMethodsArray = [PaymentMethods]();
        for details in dataArray {
            let methods = PaymentMethods()
            methods.paymentMethodType = details["paymentMethodType"].string ?? ""
            methods.paymentMethod = details["paymentMethod"].string ?? ""
            methods.paymentMethodDescription = details["description"].string ?? ""
            paymentMethodsArray.append(methods)
        }
        return paymentMethodsArray
    }
}
