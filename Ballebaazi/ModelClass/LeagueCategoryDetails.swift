//
//  LeagueCategoryDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 27/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class LeagueCategoryDetails: NSObject {
    
    lazy var visiableLeagueCount = 0
    lazy var categoryID = ""
    lazy var categoryName = ""
    lazy var categoryMessage = ""
    lazy var categoryImg = ""

    lazy var leaguesArray = Array<LeagueDetails>()
    
    class func parseCategoryDetails(categoryDetails: JSON, leagueArray: Array<LeagueDetails>, recomnadedLeaguesArray: [JSON]) -> (([LeagueCategoryDetails], [LeagueCategoryDetails], [LeagueCategoryDetails], [LeagueCategoryDetails], [LeagueCategoryDetails])){

        var allLeaguesArray = leagueArray
        var tempRecommdLeaguesArray = Array<LeagueDetails>()
        for recommandedDetails in recomnadedLeaguesArray {
            let leagueAmount = recommandedDetails["joining_amount"].string ?? "0"
            let catId = recommandedDetails["category_id"].string ?? ""
            let leagueJoiningAmount = Float(leagueAmount) ?? 0

            let removedArray = allLeaguesArray.filter { (details) -> Bool in
                (details.categoryId == catId) && (Float(details.joiningAmount) ?? 0 == leagueJoiningAmount)
            }
            
            allLeaguesArray.removeAll { (details) -> Bool in
                (details.categoryId == catId) && (Float(details.joiningAmount) ?? 0 == leagueJoiningAmount)
            }
            
            let removedArrayDuplicateRecords = tempRecommdLeaguesArray.filter { (details) -> Bool in
                (details.categoryId == catId) && (Float(details.joiningAmount) ?? 0 == leagueJoiningAmount)
            }
            
            if removedArrayDuplicateRecords.count == 0 {
                tempRecommdLeaguesArray.append(contentsOf: removedArray)
            }
        }
        
        var classicArray = Array<LeagueCategoryDetails>()
        var bowlingArray = Array<LeagueCategoryDetails>()
        var battingArray = Array<LeagueCategoryDetails>()
        var reverseArray = Array<LeagueCategoryDetails>()
        var wizardArray = Array<LeagueCategoryDetails>()

        let classicLeagueArray = allLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "1"
        }
        
        let battingLeagueArray = allLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "2"
        }
        
        let bowlingLeagueArray = allLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "3"
        }
        
        let reverseLeagueArray = allLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "4"
        }

        let wizardLeagueArray = allLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "5"
        }

        
        if let classicCatDict = categoryDetails["1"].dictionary{
            if let classicCatArray = classicCatDict["cz"]?.array{
                for details in classicCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    let visiableCount = details["tv"].stringValue
                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = classicLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;

                    catDetails.visiableLeagueCount = Int(visiableCount) ?? 0;
                    if filterLeagueArray.count < catDetails.visiableLeagueCount{
                        catDetails.visiableLeagueCount = filterLeagueArray.count
                    }
                    
                    if filterLeagueArray.count > 0{
                        classicArray.append(catDetails)
                    }
                }
            }
        }
        
        if let battingCatDict = categoryDetails["2"].dictionary{
            if let battingCatArray = battingCatDict["cz"]?.array{
                for details in battingCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    let visiableCount = details["tv"].stringValue
                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = battingLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = Int(visiableCount) ?? 0;
                    if filterLeagueArray.count > 0{
                        battingArray.append(catDetails)
                    }
                }
            }
        }
        
        if let bowlingCatDict = categoryDetails["3"].dictionary{
            if let bowlingCatArray = bowlingCatDict["cz"]?.array{
                for details in bowlingCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let visiableCount = details["tv"].stringValue
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = bowlingLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = Int(visiableCount) ?? 0;
                    if filterLeagueArray.count > 0{
                        bowlingArray.append(catDetails)
                    }
                }
            }
        }
        
        
        if let reverseCatDict = categoryDetails["4"].dictionary{
            if let reverseCatArray = reverseCatDict["cz"]?.array{
                for details in reverseCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let visiableCount = details["tv"].stringValue
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = reverseLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = Int(visiableCount) ?? 0;
                    if filterLeagueArray.count > 0{
                        reverseArray.append(catDetails)
                    }
                }
            }
        }
        
        if let wizardCatDict = categoryDetails["5"].dictionary{
            if let wizardCatArray = wizardCatDict["cz"]?.array{
                for details in wizardCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let visiableCount = details["tv"].stringValue
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = wizardLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = Int(visiableCount) ?? 0;
                    if filterLeagueArray.count > 0{
                        wizardArray.append(catDetails)
                    }
                }
            }
        }
        
            
        
        let classicRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "1"
        }
        
        let battingRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "2"
        }
        
        let bowlingRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "3"
        }
        
        let reverseRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "4"
        }

        let wizardRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "5"
        }

        if classicRecommanedLeagueArray.count > 0 {
            let catDetails = LeagueCategoryDetails()
            catDetails.leaguesArray = classicRecommanedLeagueArray
            catDetails.categoryID = "";
            catDetails.categoryName = "Recomanded for you";
            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.categoryImg = categoryImg;
            catDetails.visiableLeagueCount = Int(classicRecommanedLeagueArray.count) ;
            classicArray.insert(catDetails, at: 0)
        }
        
        if battingRecommanedLeagueArray.count > 0 {
            let catDetails = LeagueCategoryDetails()
            catDetails.leaguesArray = battingRecommanedLeagueArray
            catDetails.categoryID = "";
            catDetails.categoryName = "Recomanded for you";
            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.categoryImg = categoryImg;
            catDetails.visiableLeagueCount = Int(battingRecommanedLeagueArray.count) ;
            battingArray.insert(catDetails, at: 0)
        }

        if bowlingRecommanedLeagueArray.count > 0 {
            let catDetails = LeagueCategoryDetails()
            catDetails.leaguesArray = bowlingRecommanedLeagueArray
            catDetails.categoryID = "";
            catDetails.categoryName = "Recomanded for you";
            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.categoryImg = categoryImg;
            catDetails.visiableLeagueCount = Int(bowlingRecommanedLeagueArray.count) ;
            bowlingArray.insert(catDetails, at: 0)
        }

        if reverseRecommanedLeagueArray.count > 0 {
            let catDetails = LeagueCategoryDetails()
            catDetails.leaguesArray = reverseRecommanedLeagueArray
            catDetails.categoryID = "";
            catDetails.categoryName = "Recomanded for you";
            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.categoryImg = categoryImg;
            catDetails.visiableLeagueCount = Int(reverseRecommanedLeagueArray.count) ;
            reverseArray.insert(catDetails, at: 0)
        }

        if wizardRecommanedLeagueArray.count > 0 {
            let catDetails = LeagueCategoryDetails()
            catDetails.leaguesArray = wizardRecommanedLeagueArray
            catDetails.categoryID = "";
            catDetails.categoryName = "Recomanded for you";
            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.categoryImg = categoryImg;
            catDetails.visiableLeagueCount = Int(wizardRecommanedLeagueArray.count) ;
            wizardArray.insert(catDetails, at: 0)
        }

        
        return (classicArray, battingArray, bowlingArray, reverseArray, wizardArray)
    }
    
    /*
    class func parseCategoryDetailsWithFilter(categoryDetails: JSON, leagueArray: Array<LeagueDetails>, rangArray: Array<Any>, poolArray: Array<Any>, teamsArray: Array<Any>, leaguesTypeArray: Array<String>) -> (([LeagueCategoryDetails], [LeagueCategoryDetails], [LeagueCategoryDetails], [LeagueCategoryDetails], [LeagueCategoryDetails])){
        
        var localFilterArray = Array<LeagueDetails>()
        var allLeagueTempArray = leagueArray
        var isRangeFilterApplied = false
        var ispoolFilterApplied = false
        var isTeamFilterApplied = false
        var isEntryFilterApplied = false

        
        
        
//          var tempRecommdLeaguesArray = Array<LeagueDetails>()
//          for recommandedDetails in recomnadedLeaguesArray {
//              let leagueAmount = recommandedDetails["joining_amount"].string ?? "0"
//              let catId = recommandedDetails["category_id"].string ?? ""
//              let leagueJoiningAmount = Float(leagueAmount) ?? 0
//
//              let removedArray = allLeaguesArray.filter { (details) -> Bool in
//                  (details.categoryId == catId) && (Float(details.joiningAmount) ?? 0 == leagueJoiningAmount)
//              }
//
//              allLeaguesArray.removeAll { (details) -> Bool in
//                  (details.categoryId == catId) && (Float(details.joiningAmount) ?? 0 == leagueJoiningAmount)
//              }
//
//              let removedArrayDuplicateRecords = tempRecommdLeaguesArray.filter { (details) -> Bool in
//                  (details.categoryId == catId) && (Float(details.joiningAmount) ?? 0 == leagueJoiningAmount)
//              }
//
//              if removedArrayDuplicateRecords.count == 0 {
//                  tempRecommdLeaguesArray.append(contentsOf: removedArray)
//              }
//          }
        
        
        for filerDetails in rangArray{
            if let tempDetails = filerDetails as? [String: Float] {
                let minValue = tempDetails["min"]!
                let maxValue = tempDetails["max"]!
                isRangeFilterApplied = true
                let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                    ((Float(details.joiningAmount) ?? 0) >= minValue) && ((Float(details.joiningAmount) ?? 0) <= maxValue)
                }
                if tempFilterArray.count > 0 {
                    localFilterArray.append(contentsOf: tempFilterArray)
                }
            }
        }
        
        if isRangeFilterApplied {
            allLeagueTempArray = localFilterArray
            localFilterArray.removeAll()
        }
        
        for filerDetails in poolArray{
            if let tempDetails = filerDetails as? [String: Float] {
                let minValue = tempDetails["min"]!
                let maxValue = tempDetails["max"]!
                ispoolFilterApplied = true
                let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                    ((Float(details.winAmount ?? "0") ?? 0) >= minValue) && ((Float(details.winAmount ?? "0") ?? 0) <= maxValue)
                }
                if tempFilterArray.count > 0 {
                    localFilterArray.append(contentsOf: tempFilterArray)
                }
            }
        }
        
        if ispoolFilterApplied {
            allLeagueTempArray = localFilterArray
            localFilterArray.removeAll()
        }

        for filerDetails in teamsArray{
            if let tempDetails = filerDetails as? [String: Float] {
                let minValue = tempDetails["min"]!
                let maxValue = tempDetails["max"]!
                isTeamFilterApplied = true
                let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                    ((Float(details.maxPlayers) ?? 0) >= minValue) && ((Float(details.maxPlayers) ?? 0) <= maxValue)
                }
                if tempFilterArray.count > 0 {
                    localFilterArray.append(contentsOf: tempFilterArray)
                }
            }
        }

        var isConfirmedLeaguesFilter = false
        for filerStr in leaguesTypeArray{
            if filerStr == "Confirmed League".localized(){
                isConfirmedLeaguesFilter = true
            }
        }

        if isTeamFilterApplied {
            allLeagueTempArray = localFilterArray
            localFilterArray.removeAll()
        }
        
        var isMultwinnerAndSingleEnty = 0
        var isMultiEntryAndSingleWinner = 0
        
        var isSingleEntrySingleWinner = 0
        var isMultiEntryMultiWinner = 0

        for title in leaguesTypeArray {
            if title == "Single Entry".localized() {
                isSingleEntrySingleWinner = isSingleEntrySingleWinner + 1
                isMultwinnerAndSingleEnty = isMultwinnerAndSingleEnty + 1
            } else if title == "Multi Entry".localized() {
                isMultiEntryMultiWinner = isMultiEntryMultiWinner + 1
                isMultiEntryAndSingleWinner = isMultiEntryAndSingleWinner + 1
            }
            else if title == "Single Winner".localized() {
                isSingleEntrySingleWinner = isSingleEntrySingleWinner + 1
                isMultiEntryAndSingleWinner = isMultiEntryAndSingleWinner + 1
            } else if title == "Multi Winners".localized() {
                isMultiEntryMultiWinner = isMultiEntryMultiWinner + 1
                isMultwinnerAndSingleEnty = isMultwinnerAndSingleEnty + 1
            }
        }
                
        if isConfirmedLeaguesFilter && (leaguesTypeArray.count == 1) {
            let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                return details.confirmedLeague == "2"
            }
            isEntryFilterApplied = true
            if tempFilterArray.count > 0 {
                localFilterArray.append(contentsOf: tempFilterArray)
            }
        }
        else{
            for filerStr in leaguesTypeArray{
                isEntryFilterApplied = true
                let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                    if isConfirmedLeaguesFilter{
                        
                        if isMultwinnerAndSingleEnty == 2 {
                            return (details.teamType == "2") && ((Int(details.totalWinners) ?? 0)  > 1) && (details.confirmedLeague == "2")
                        }
                        else if isMultiEntryAndSingleWinner == 2 {
                            return (details.teamType == "1") && (details.totalWinners == "1") && (details.confirmedLeague == "2")
                        }
                        else if isSingleEntrySingleWinner == 2 {
                            return (details.teamType == "2") && ((Int(details.totalWinners) ?? 0) == 1) && (details.confirmedLeague == "2")
                        }
                        else if isMultiEntryMultiWinner == 2 {
                            return (details.teamType == "1") && ((Int(details.totalWinners) ?? 0) > 1) && (details.confirmedLeague == "2")
                        }
                        else if filerStr == "Single Entry".localized(){
                            return (details.teamType == "2") && (details.confirmedLeague == "2")
                        }else if filerStr == "Multi Entry".localized(){
                            return (details.teamType == "1") && (details.confirmedLeague == "2")
                        }else if filerStr == "Single Winner".localized(){
                            return (details.totalWinners == "1") && (details.confirmedLeague == "2")
                        }else if filerStr == "Multi Winners".localized(){
                            return ((Int(details.totalWinners) ?? 0)  > 1) && (details.confirmedLeague == "2")
                        }
                        else{
                            return false
                        }
                    }else {
                        
                        if isMultwinnerAndSingleEnty == 2 {
                            return (details.teamType == "2") && ((Int(details.totalWinners) ?? 0)  > 1)
                        }
                        else if isMultiEntryAndSingleWinner == 2 {
                            return (details.teamType == "1") && (details.totalWinners == "1")
                        }
                        else if isSingleEntrySingleWinner == 2 {
                            return (details.teamType == "2") && ((Int(details.totalWinners) ?? 0) == 1)
                        }
                        else if isMultiEntryMultiWinner == 2 {
                            return (details.teamType == "1") && ((Int(details.totalWinners) ?? 0) > 1)
                        }
                        else if filerStr == "Single Entry".localized(){
                            return details.teamType == "2"
                        }else if filerStr == "Multi Entry".localized(){
                            return details.teamType == "1"
                        }else if filerStr == "Single Winner".localized(){
                            return details.totalWinners == "1"
                        }else if filerStr == "Multi Winners".localized(){
                            return (Int(details.totalWinners) ?? 0)  > 1
                        }
                        else if filerStr == "Confirmed League".localized(){
                            return details.confirmedLeague == "2"
                        }
                        else{
                            return false
                        }
                    }
                }

                if tempFilterArray.count > 0 {
                    localFilterArray.append(contentsOf: tempFilterArray)
                }
            }
        }

        if isEntryFilterApplied {
           allLeagueTempArray = localFilterArray
           localFilterArray.removeAll()
        }
        
        localFilterArray = allLeagueTempArray
        if (rangArray.count == 0) && (poolArray.count == 0) && (teamsArray.count == 0) && (leaguesTypeArray.count == 0) {
            localFilterArray = leagueArray
        }
            
        var classicArray = Array<LeagueCategoryDetails>()
        var battingArray = Array<LeagueCategoryDetails>()
        var bowlingArray = Array<LeagueCategoryDetails>()
        var reverseArray = Array<LeagueCategoryDetails>()
        var wizardArray = Array<LeagueCategoryDetails>()

        let classicLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "1"
        }
        
        let battingLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "2"
        }
        
        let bowlingLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "3"
        }
        
        let reverseLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "4"
        }
        
        let wizardLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "5"
        }

        if let classicCatDict = categoryDetails["1"].dictionary{
            if let classicCatArray = classicCatDict["cz"]?.array{
                for details in classicCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""
                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = classicLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count

                    if filterLeagueArray.count > 0{
                        classicArray.append(catDetails)
                    }
                }
            }
        }
        
        if let battingCatDict = categoryDetails["2"].dictionary{
            if let battingCatArray = battingCatDict["cz"]?.array{
                for details in battingCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = battingLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count
                    if filterLeagueArray.count > 0{
                        battingArray.append(catDetails)
                    }
                }
            }
        }
        
        if let bowlingCatDict = categoryDetails["3"].dictionary{
            if let bowlingCatArray = bowlingCatDict["cz"]?.array{
                for details in bowlingCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = bowlingLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count
                    if filterLeagueArray.count > 0{
                        bowlingArray.append(catDetails)
                    }
                }
            }
        }
        
        if let reverseCatDict = categoryDetails["4"].dictionary{
            if let reverseCatArray = reverseCatDict["cz"]?.array{
                for details in reverseCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = reverseLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count
                    if filterLeagueArray.count > 0{
                        reverseArray.append(catDetails)
                    }
                }
            }
        }
        
        if let wizardCatDict = categoryDetails["5"].dictionary{
            if let wizardCatArray = wizardCatDict["cz"]?.array{
                for details in wizardCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = wizardLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count
                    if filterLeagueArray.count > 0{
                        wizardArray.append(catDetails)
                    }
                }
            }
        }
        return (classicArray, battingArray, bowlingArray, reverseArray, wizardArray)
    }
 */
    
    
    class func parseCategoryDetailsWithFilter(categoryDetails: JSON, leagueArray: Array<LeagueDetails>, recomnadedLeaguesArray: Array<JSON>, rangArray: Array<Any>, poolArray: Array<Any>, teamsArray: Array<Any>, leaguesTypeArray: Array<String>) -> (([LeagueCategoryDetails], [LeagueCategoryDetails], [LeagueCategoryDetails], [LeagueCategoryDetails], [LeagueCategoryDetails])){
        
        var localFilterArray = Array<LeagueDetails>()
        var allLeagueTempArray = leagueArray
        var isRangeFilterApplied = false
        var ispoolFilterApplied = false
        var isTeamFilterApplied = false
        var isEntryFilterApplied = false
           
        
        for filerDetails in rangArray{
            if let tempDetails = filerDetails as? [String: Float] {
                let minValue = tempDetails["min"]!
                let maxValue = tempDetails["max"]!
                isRangeFilterApplied = true
                let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                    ((Float(details.joiningAmount) ?? 0) >= minValue) && ((Float(details.joiningAmount) ?? 0) <= maxValue)
                }
                if tempFilterArray.count > 0 {
                    localFilterArray.append(contentsOf: tempFilterArray)
                }
            }
        }
        
        if isRangeFilterApplied {
            allLeagueTempArray = localFilterArray
            localFilterArray.removeAll()
        }
        
        for filerDetails in poolArray{
            if let tempDetails = filerDetails as? [String: Float] {
                let minValue = tempDetails["min"]!
                let maxValue = tempDetails["max"]!
                ispoolFilterApplied = true
                let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                    ((Float(details.winAmount ?? "0") ?? 0) >= minValue) && ((Float(details.winAmount ?? "0") ?? 0) <= maxValue)
                }
                if tempFilterArray.count > 0 {
                    localFilterArray.append(contentsOf: tempFilterArray)
                }
            }
        }
        
        if ispoolFilterApplied {
            allLeagueTempArray = localFilterArray
            localFilterArray.removeAll()
        }

        for filerDetails in teamsArray{
            if let tempDetails = filerDetails as? [String: Float] {
                let minValue = tempDetails["min"]!
                let maxValue = tempDetails["max"]!
                isTeamFilterApplied = true
                let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                    ((Float(details.maxPlayers) ?? 0) >= minValue) && ((Float(details.maxPlayers) ?? 0) <= maxValue)
                }
                if tempFilterArray.count > 0 {
                    localFilterArray.append(contentsOf: tempFilterArray)
                }
            }
        }

        var isConfirmedLeaguesFilter = false
        for filerStr in leaguesTypeArray{
            if filerStr == "Confirmed League".localized(){
                isConfirmedLeaguesFilter = true
            }
        }

        if isTeamFilterApplied {
            allLeagueTempArray = localFilterArray
            localFilterArray.removeAll()
        }
        
        var isMultwinnerAndSingleEnty = 0
        var isMultiEntryAndSingleWinner = 0
        
        var isSingleEntrySingleWinner = 0
        var isMultiEntryMultiWinner = 0

        for title in leaguesTypeArray {
            if title == "Single Entry".localized() {
                isSingleEntrySingleWinner = isSingleEntrySingleWinner + 1
                isMultwinnerAndSingleEnty = isMultwinnerAndSingleEnty + 1
            } else if title == "Multi Entry".localized() {
                isMultiEntryMultiWinner = isMultiEntryMultiWinner + 1
                isMultiEntryAndSingleWinner = isMultiEntryAndSingleWinner + 1
            }
            else if title == "Single Winner".localized() {
                isSingleEntrySingleWinner = isSingleEntrySingleWinner + 1
                isMultiEntryAndSingleWinner = isMultiEntryAndSingleWinner + 1
            } else if title == "Multi Winners".localized() {
                isMultiEntryMultiWinner = isMultiEntryMultiWinner + 1
                isMultwinnerAndSingleEnty = isMultwinnerAndSingleEnty + 1
            }
        }
                
        if isConfirmedLeaguesFilter && (leaguesTypeArray.count == 1) {
            let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                return details.confirmedLeague == "2"
            }
            isEntryFilterApplied = true
            if tempFilterArray.count > 0 {
                localFilterArray.append(contentsOf: tempFilterArray)
            }
        }
        else{
            for filerStr in leaguesTypeArray{
                isEntryFilterApplied = true
                let tempFilterArray = allLeagueTempArray.filter { (details) -> Bool in
                    if isConfirmedLeaguesFilter{
                        
                        if isMultwinnerAndSingleEnty == 2 {
                            return (details.teamType == "2") && ((Int(details.totalWinners) ?? 0)  > 1) && (details.confirmedLeague == "2")
                        }
                        else if isMultiEntryAndSingleWinner == 2 {
                            return (details.teamType == "1") && (details.totalWinners == "1") && (details.confirmedLeague == "2")
                        }
                        else if isSingleEntrySingleWinner == 2 {
                            return (details.teamType == "2") && ((Int(details.totalWinners) ?? 0) == 1) && (details.confirmedLeague == "2")
                        }
                        else if isMultiEntryMultiWinner == 2 {
                            return (details.teamType == "1") && ((Int(details.totalWinners) ?? 0) > 1) && (details.confirmedLeague == "2")
                        }
                        else if filerStr == "Single Entry".localized(){
                            return (details.teamType == "2") && (details.confirmedLeague == "2")
                        }else if filerStr == "Multi Entry".localized(){
                            return (details.teamType == "1") && (details.confirmedLeague == "2")
                        }else if filerStr == "Single Winner".localized(){
                            return (details.totalWinners == "1") && (details.confirmedLeague == "2")
                        }else if filerStr == "Multi Winners".localized(){
                            return ((Int(details.totalWinners) ?? 0)  > 1) && (details.confirmedLeague == "2")
                        }
                        else{
                            return false
                        }
                    }else {
                        
                        if isMultwinnerAndSingleEnty == 2 {
                            return (details.teamType == "2") && ((Int(details.totalWinners) ?? 0)  > 1)
                        }
                        else if isMultiEntryAndSingleWinner == 2 {
                            return (details.teamType == "1") && (details.totalWinners == "1")
                        }
                        else if isSingleEntrySingleWinner == 2 {
                            return (details.teamType == "2") && ((Int(details.totalWinners) ?? 0) == 1)
                        }
                        else if isMultiEntryMultiWinner == 2 {
                            return (details.teamType == "1") && ((Int(details.totalWinners) ?? 0) > 1)
                        }
                        else if filerStr == "Single Entry".localized(){
                            return details.teamType == "2"
                        }else if filerStr == "Multi Entry".localized(){
                            return details.teamType == "1"
                        }else if filerStr == "Single Winner".localized(){
                            return details.totalWinners == "1"
                        }else if filerStr == "Multi Winners".localized(){
                            return (Int(details.totalWinners) ?? 0)  > 1
                        }
                        else if filerStr == "Confirmed League".localized(){
                            return details.confirmedLeague == "2"
                        }
                        else{
                            return false
                        }
                    }
                }

                if tempFilterArray.count > 0 {
                    localFilterArray.append(contentsOf: tempFilterArray)
                }
            }
        }

        if isEntryFilterApplied {
           allLeagueTempArray = localFilterArray
           localFilterArray.removeAll()
        }
        
        localFilterArray = allLeagueTempArray
        if (rangArray.count == 0) && (poolArray.count == 0) && (teamsArray.count == 0) && (leaguesTypeArray.count == 0) {
            localFilterArray = leagueArray
        }
           
           
           
//        var tempRecommdLeaguesArray = Array<LeagueDetails>()
//        for recommandedDetails in recomnadedLeaguesArray {
//            let leagueAmount = recommandedDetails["joining_amount"].string ?? "0"
//            let catId = recommandedDetails["category_id"].string ?? ""
//            let leagueJoiningAmount = Float(leagueAmount) ?? 0
//
//            let removedArray = localFilterArray.filter { (details) -> Bool in
//                (details.categoryId == catId) && (Float(details.joiningAmount) ?? 0 == leagueJoiningAmount)
//            }
//
//            localFilterArray.removeAll { (details) -> Bool in
//                (details.categoryId == catId) && (Float(details.joiningAmount) ?? 0 == leagueJoiningAmount)
//            }
//
//            let removedArrayDuplicateRecords = tempRecommdLeaguesArray.filter { (details) -> Bool in
//                (details.categoryId == catId) && (Float(details.joiningAmount) ?? 0 == leagueJoiningAmount)
//            }
//
//            if removedArrayDuplicateRecords.count == 0 {
//                tempRecommdLeaguesArray.append(contentsOf: removedArray)
//            }
//        }
//
        
        var classicArray = Array<LeagueCategoryDetails>()
        var battingArray = Array<LeagueCategoryDetails>()
        var bowlingArray = Array<LeagueCategoryDetails>()
        var reverseArray = Array<LeagueCategoryDetails>()
        var wizardArray = Array<LeagueCategoryDetails>()

        let classicLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "1"
        }
        
        let battingLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "2"
        }
        
        let bowlingLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "3"
        }
        
        let reverseLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "4"
        }
        
        let wizardLeagueArray = localFilterArray.filter { (leagueDetails) -> Bool in
            leagueDetails.fantasyType == "5"
        }

        if let classicCatDict = categoryDetails["1"].dictionary{
            if let classicCatArray = classicCatDict["cz"]?.array{
                for details in classicCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""
                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = classicLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count

                    if filterLeagueArray.count > 0{
                        classicArray.append(catDetails)
                    }
                }
            }
        }
        
        if let battingCatDict = categoryDetails["2"].dictionary{
            if let battingCatArray = battingCatDict["cz"]?.array{
                for details in battingCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = battingLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count
                    if filterLeagueArray.count > 0{
                        battingArray.append(catDetails)
                    }
                }
            }
        }
        
        if let bowlingCatDict = categoryDetails["3"].dictionary{
            if let bowlingCatArray = bowlingCatDict["cz"]?.array{
                for details in bowlingCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = bowlingLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count
                    if filterLeagueArray.count > 0{
                        bowlingArray.append(catDetails)
                    }
                }
            }
        }
        
        if let reverseCatDict = categoryDetails["4"].dictionary{
            if let reverseCatArray = reverseCatDict["cz"]?.array{
                for details in reverseCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = reverseLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count
                    if filterLeagueArray.count > 0{
                        reverseArray.append(catDetails)
                    }
                }
            }
        }
        
        if let wizardCatDict = categoryDetails["5"].dictionary{
            if let wizardCatArray = wizardCatDict["cz"]?.array{
                for details in wizardCatArray{
                    let catDetails = LeagueCategoryDetails()
                    let categoryID = details["cid"].string ?? ""
                    let categoryName = details["cn"].string ?? ""
                    let categoryMessage = details["cm"].string ?? ""
                    let categoryImg = details["pht"].string ?? ""

                    var filterLeagueArray = Array<LeagueDetails>()
                    
                    if let categoryleague = details["lo"].array{
                        for leagueID in categoryleague{
                            let filteredArray = wizardLeagueArray.filter { (leagueDetails) -> Bool in
                                (leagueDetails.leagueId == leagueID.string) || (leagueDetails.referenceLeagueID == leagueID.string)
                            }
                            if filteredArray.count > 0{
                                filterLeagueArray.append(filteredArray[0])
                            }
                        }
                    }
                    catDetails.leaguesArray = filterLeagueArray
                    catDetails.categoryID = categoryID;
                    catDetails.categoryName = categoryName;
                    catDetails.categoryMessage = categoryMessage;
                    catDetails.categoryImg = categoryImg;
                    catDetails.visiableLeagueCount = filterLeagueArray.count
                    if filterLeagueArray.count > 0{
                        wizardArray.append(catDetails)
                    }
                }
            }
        }
        
                
//        let classicRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
//            leagueDetails.fantasyType == "1"
//        }
//
//        let battingRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
//            leagueDetails.fantasyType == "2"
//        }
//
//        let bowlingRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
//            leagueDetails.fantasyType == "3"
//        }
//
//        let reverseRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
//            leagueDetails.fantasyType == "4"
//        }
//
//        let wizardRecommanedLeagueArray = tempRecommdLeaguesArray.filter { (leagueDetails) -> Bool in
//            leagueDetails.fantasyType == "5"
//        }

//        if classicRecommanedLeagueArray.count > 0 {
//            let catDetails = LeagueCategoryDetails()
//            catDetails.leaguesArray = classicRecommanedLeagueArray
//            catDetails.categoryID = "";
//            catDetails.categoryName = "Recomanded for you";
//            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.visiableLeagueCount = Int(classicRecommanedLeagueArray.count) ;
//            classicArray.insert(catDetails, at: 0)
//        }
//
//        if battingRecommanedLeagueArray.count > 0 {
//            let catDetails = LeagueCategoryDetails()
//            catDetails.leaguesArray = battingRecommanedLeagueArray
//            catDetails.categoryID = "";
//            catDetails.categoryName = "Recomanded for you";
//            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.visiableLeagueCount = Int(battingRecommanedLeagueArray.count) ;
//            battingArray.insert(catDetails, at: 0)
//        }
//
//        if bowlingRecommanedLeagueArray.count > 0 {
//            let catDetails = LeagueCategoryDetails()
//            catDetails.leaguesArray = bowlingRecommanedLeagueArray
//            catDetails.categoryID = "";
//            catDetails.categoryName = "Recomanded for you";
//            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.visiableLeagueCount = Int(bowlingRecommanedLeagueArray.count) ;
//            bowlingArray.insert(catDetails, at: 0)
//        }
//
//        if reverseRecommanedLeagueArray.count > 0 {
//            let catDetails = LeagueCategoryDetails()
//            catDetails.leaguesArray = reverseRecommanedLeagueArray
//            catDetails.categoryID = "";
//            catDetails.categoryName = "Recomanded for you";
//            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.visiableLeagueCount = Int(reverseRecommanedLeagueArray.count) ;
//            reverseArray.insert(catDetails, at: 0)
//        }
//
//        if wizardRecommanedLeagueArray.count > 0 {
//            let catDetails = LeagueCategoryDetails()
//            catDetails.leaguesArray = wizardRecommanedLeagueArray
//            catDetails.categoryID = "";
//            catDetails.categoryName = "Recomanded for you";
//            catDetails.categoryMessage = "Recomanded for you";
//            catDetails.visiableLeagueCount = Int(wizardRecommanedLeagueArray.count) ;
//            wizardArray.insert(catDetails, at: 0)
//        }

        
        return (classicArray, battingArray, bowlingArray, reverseArray, wizardArray)
    }
}
