
//
//  LocalMessages.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/28/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import Foundation
import UIKit


let kAlert = "Letspick"

let kPrivateLeague = "Private League";

/*
let "kErrorMsg".localized() = "Oops! Your internet seems to be on a power nap. Please check your internet settings"
//let "kErrorMsg".localized() = "Something went wrong. Please try again"


let "kNoInternetConnection".localized() = "Check your Internet connection"

let "EnterEmailMsg".localized() = "Please enter email ID."

let "EnterEmailOrUserNameMsg".localized() = "Please enter user name or email ID."

let "EnterUserNameMsg".localized() = "Please enter user name."

let "EnterValidEmailMsg".localized() = "Please enter a valid email ID."

let "EnterPasswordMsg".localized() = "Please enter your password."

let EnterOldPasswordMsg = "Please enter your old password."

let EnterNewPasswordMsg = "Please enter your new password."

let EnterConfirmPasswordMsg = "Please confirm your password."

let PasswordNotMatchedMsg = "Confirm password must be same."

let "EnterNameMsg".localized() = "Please enter your name."

let "EnterPhoneMsg".localized() = "Please enter your phone number."

let EnterAddressMsg = "Please enter your address."

let EnterZipCodeMsg = "Please enter your pin code."

let EnterCityCodeMsg = "Please enter your city."

let EnterCountryMsg = "Please enter your country."

let "MinPasswordLenghtMsg".localized() = "Password is too short, atleast 6 character long."

let "SelectDOB".localized() = "Please select date of birth."

let SelectPanImage = "Please select pan image."

let "SelectState".localized() = "Please select your State."

let "EnterPANNumberMsg".localized() = "Please enter your pan number."

let "EnterAccountNumberMsg".localized() = "Please enter account number."

let "EnterConfirmAccountNumberMsg".localized() = "Please confirm account number."

let "EnterConfirmAccountNumberNotMatchedMsg".localized() = "Confirm account number must be same."

let "EnterIFscCode".localized() = "Please enter IFSC code."

let "SelectBank".localized() = "Please select the bank."

let "EnterBranch".localized() = "Please enter branch name."

let "SelectBank".localized()Image = "Please select Cheque or passbook image."

let "EnterAadharNumber".localized() = "Please enter aadhar number."

let EnterWithdrawableAmount = "Please enter amount to withdraw."

let MinWithdrawableAmountLimit = "Withdraw amount must be equal or greater than pts200."

let LogoutMessage = "Are you sure you want to logout?"

let CreatTeamMsg = "Let's create a team first!"

let "PaymentCancelMsg".localized() = "You cancelled the payment."

let "PaymentSuccessMsg".localized() = "Your payment has been processed successfully."

let "ExitMessage".localized() = "Are you sure want to exit?"

let WithdrawCashTitle = "Woohoo! Withdraw your hard earned cash"

let WithdrawCashMessage = "To withdraw cash you need to verify your account."




//MARK:- Player Selection Messages


let maxLimitDefenders = "Not more than 4 Defenders"
let max2AllRoundersKabaddi = "You can select only 2 All Rounders"
let maxLimitRaider = "Not more than 3 Raiders"
let maxTeamPlayerForKabaddi = "Maximum 5 players from one team"
let max7ForKabaddiPlayer = "You have selected 7 players. Click on Next to select Captain & Vice Captain."
let picOneAllRoundersKabaddi = "Pick at least one All Rounder"



let max11Player = "Max 11 players allowed!\n Your team looks great! Go ahead, save your team."

let max7PlayerFromTeam = "Maximum 7 player from one team."

let max5Batsman = "Not more than 5 Batsmen."


let max5Defender = "Not more than 5 Defenders."
let max5Midfielder = "Not more than 5 Midfielders."
let max3Forwars = "Not more than 3 Forwards."
let maxGoalKeeper = "You can select only 1 Gaol Keeper."
let pickOneGoalKeeper = "Pick one Gaol Keeper."
let pickOneForward = "Pick atleast 1 Forward."

let min3MidFielder = "Midfielder Missing!\nEvery team needs atleast 3 Midfielder."
let min3Defender = "Defender Missing!\nEvery team needs atleast 3 Defenders."


let SelectMin3Defenders = "You need to select atleast 3 Defenders."
let SelectMin3MidFielder = "You need to select atleast 3 Midfielders."



let max5Bowler = "Not more than 5 Bowlers!\nWho will score the runs?"

let "max3Allrounder".localized() = "Not more than 3 all-rounders!\nDon't be greedy, 3 is all you needy."

//let max1WicketKeeper = "Not more than 1 wicket-keeper!\nHave you ever seen 2 keepers behind the stumps?"
let max1WicketKeeper = "You can select only 1 Wicket-Keeper"

let "pickOneKeeper".localized() = "Pick one Wicket-Keeper!\nWho will sledge behind the stumps?"

let pickOneAllRounder = "Pick atleast 1 all-rounder,They bat, bowl & field. 3 for the price of 1!"

let min3Batsman = "Batsmen Missing!\nEvery team needs atleast 3 Batsmen."

let min3Bowler = "Bowlers Missing!\nEvery team needs atleast 3 Bowlers."

let max5BowlerFanticy = "Not more than 5 Bowler!\nWho will score the runs?"

let max3PlayerFromTeam = "Maximum 3 player from one team."

let "bowlerSelectionPopUp".localized() = "This is batting fantasy! You want to choose a bowler?"

let "battingSelectionPopUp".localized() = "This is bowling fantasy! You want to choose a batsman?"

let keeperCannotSelect = "You can't select keeper in bowling fantasy."

let "SelectAadharImage".localized() = "Please select aadhar images."

let SelectMin3Bowlers = "You need to select atleast 3 Bowlers."

let SelectMin3Batsman = "You need to select atleast 3 batsmen."


*/
