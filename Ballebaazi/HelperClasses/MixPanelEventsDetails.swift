//
//  MixPanelEventsDetails.swift
//  Letspick
//
//  Created by Vikash Rajput on 22/11/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Branch
import FacebookCore
import FBSDKLoginKit


let kOnboardedEvent = "Onboarded"
let kName  = "Name "
let kChannel = "Channel"
let kNameMixpanel  = "$name "

let kUserName = "User Name"
let kEmail = "Email"
let kEmailMixpanel = "$email"

let kPhoneNumber = "Phone Number"
let kDOB = "DOB"
let kReferalCode = "Referal Code"
let kUserCity = "User City"
let kSignUpCode = "Sign Up Code"
let kRegisteredDate = "Registered Date"
let kPrivateLeagueCode = "Private League Code"
let kOnboardPlatform = "Onboard Platform"
let kUserID = "User ID"
let kOTPRequested = "OTP Requested"
let kPaymentUnsuccessful = "Payment Unsuccessful"




class MixPanelEventsDetails: NSObject {

    class func intialiseMixpanel() {
        return;
        
    }
    
    class func updateUsernamePeopleProperties() {
        return;
    }
    
    class func setPeopleProperties(isFromEdit: Bool) {
        
    }

//    class func trackEvent(eventName: String, properties: [String: MixpanelType]?) {
//        return;
//        Mixpanel.mainInstance().track(event: eventName, properties: properties)
//        Mixpanel.mainInstance().flush()
//    }
    
    class func setupSuperProperties() {
        return;
        var chanelName = UserDetails.sharedInstance.channel
        
        if chanelName.count == 0 {
            chanelName = kDirect
        }

    }
    
    class func updateBranchEvent(registrationMode: String) {
        Branch.getInstance().setIdentity(UserDetails.sharedInstance.userID)

        let event = BranchEvent.standardEvent(.completeRegistration)
        event.eventDescription = "User created an account"
        event.customData["User ID"] = UserDetails.sharedInstance.userID
        event.customData["Mode"] = registrationMode
        event.customData["Channel"] = UserDetails.sharedInstance.channel
        event.customData["Onboarding Medium"] = PLATFORM_iPHONE
        event.customData["Verification"] = "Yes"
        event.logEvent()
        
        
        AppEvents.logEvent(.completedRegistration, parameters: ["User ID": UserDetails.sharedInstance.userID, "Onboarding Medium": PLATFORM_iPHONE, "Description": "User created an account", "Channel": UserDetails.sharedInstance.channel , "Mode" : registrationMode])

    }
    
    class func customDepositSuccessFullEvent(amountDeposit: String, promoCode: String, paymentMethod: String) {
        
        let event = BranchEvent.customEvent(withName: "Deposit Successful")
        event.customData[amount] = amountDeposit
        event.customData[promo_code] = promoCode
        event.customData[payment_method] = paymentMethod
        event.customData[PLATFORM] = PLATFORM_iPHONE
        event.logEvent()
        
        AppEvents.logPurchase(MixPanelEventsDetails.convertStringToDecimal(amount: amount).doubleValue, currency: "INR", parameters: ["User ID": UserDetails.sharedInstance.userID, "Onboarding Medium": PLATFORM_iPHONE, "Description": "Deposit Successful", payment_method: paymentMethod, promo_code: promoCode])
    }
    
    class func customDepositSuccessFull(amount: String, paymentMethod: String) {

        let event = BranchEvent.standardEvent(.purchase)
        event.currency = .INR
        event.eventDescription = "Deposit Successful"
        event.revenue = MixPanelEventsDetails.convertStringToDecimal(amount: amount)
        event.customData[payment_method] = paymentMethod
        event.logEvent()
    }
    
    class func convertStringToDecimal(amount: String) -> NSDecimalNumber{
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        return formatter.number(from: amount) as? NSDecimalNumber ?? 0
    }
    
    class func depositInitiated(amountInitiated: String, paymentMethod: String) {
        let event = BranchEvent.standardEvent(.initiatePurchase)
        event.currency = .INR
        event.revenue = MixPanelEventsDetails.convertStringToDecimal(amount: amountInitiated)
        event.eventDescription = "Deposit Initiated"
        event.customData[payment_method] = paymentMethod
        event.logEvent()
    }

    class func completeLogin() {

        let event = BranchEvent.standardEvent(.login)
        Branch.getInstance().setIdentity(UserDetails.sharedInstance.userID)

        event.transactionID = UserDetails.sharedInstance.userID
        event.eventDescription = "User Logged In Successfully"
        event.customData["User ID"] = UserDetails.sharedInstance.userID
        event.customData["Onboarding Medium"] = PLATFORM_iPHONE
        event.logEvent()
    }

    class func loginBranchIdentity() {
        if UserDetails.sharedInstance.userID.count > 0 {
            Branch.getInstance().setIdentity(UserDetails.sharedInstance.userID)
        }
    }
    
    
    class func branchLogout() {
        Branch.getInstance().logout()
    }

    
}
