//
//  AppHelper.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/30/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import Reachability
import CoreData
import SwiftyJSON
import Foundation
import ApxorSDK

class AppHelper: NSObject {
    
    var appLoaderView: AppLoaderView?
    static let sharedInstance = AppHelper()
    var messageView: MessageDisplayView?
    
    class func resetDefaults() {
        if UserDetails.sharedInstance.guestAccessToken.count == 0{
            AppHelper.getGuestToken()
        }

        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if (key != "lastForceUpdateTime") && !key.contains("WizRocket"){
                defaults.removeObject(forKey: key)
            }
        }
        deleteAllRecordsFromCoreData()        
        UserDetails.sharedInstance.name = ""
        UserDetails.sharedInstance.userName = ""
        UserDetails.sharedInstance.profileImageName = ""
        UserDetails.sharedInstance.phoneNumber = ""
        UserDetails.sharedInstance.dob = ""
        UserDetails.sharedInstance.userID = ""
        UserDetails.sharedInstance.emailAdress = ""
        UserDetails.sharedInstance.accessToken = ""
        UserDetails.sharedInstance.channel = ""
        UserDetails.sharedInstance.registeredDate = ""
        UserDetails.sharedInstance.signupCode = ""        
    }
    
    class func saveUserDetails()  {

        UserDefaults.standard.set(UserDetails.sharedInstance.userID, forKey: "user_id")
        UserDefaults.standard.set(UserDetails.sharedInstance.apiSecretKey, forKey: "api_secret_key")
        UserDefaults.standard.set(UserDetails.sharedInstance.phoneNumber, forKey: "phone")
        UserDefaults.standard.set(UserDetails.sharedInstance.emailAdress, forKey: "email_id")
        UserDefaults.standard.set(UserDetails.sharedInstance.accessToken, forKey: "access_token")
        UserDefaults.standard.set(UserDetails.sharedInstance.name, forKey: "name")
        UserDefaults.standard.set(UserDetails.sharedInstance.userName, forKey: "userName")
        UserDefaults.standard.set(UserDetails.sharedInstance.bonusCash, forKey: "bonusCash")
        UserDefaults.standard.set(UserDetails.sharedInstance.unusedCash, forKey: "unusedCash")
        UserDefaults.standard.set(UserDetails.sharedInstance.totalCredits, forKey: "totalCredits")
        UserDefaults.standard.set(UserDetails.sharedInstance.signupBonus, forKey: "signupBonus")
        UserDefaults.standard.set(UserDetails.sharedInstance.address, forKey: "address")
        UserDefaults.standard.set(UserDetails.sharedInstance.dob, forKey: "dob")
        UserDefaults.standard.set(UserDetails.sharedInstance.referralAmount, forKey: "referralAmount")
        UserDefaults.standard.set(UserDetails.sharedInstance.gender, forKey: "gender")

        UserDefaults.standard.synchronize()
    }
    
    class func isApplicationRunningOnIphoneX() -> Bool{
        
        if UIDevice().userInterfaceIdiom == .phone {
            
            if (UIScreen.main.bounds.height >= 812 ){
                return true
            }
        }

        return false
    }
    
    class func getUserDetails()  {
        

        if let userID = UserDefaults.standard.value(forKey: "user_id"){
            UserDetails.sharedInstance.userID = userID as! String
        }
        
        if let marketingTitle = UserDefaults.standard.value(forKey: "marketing_title"){
            UserDetails.sharedInstance.channel = marketingTitle as! String
        }
                
        if let userName = UserDefaults.standard.value(forKey: "userName"){
            UserDetails.sharedInstance.userName = userName as! String
        }
        
        if let apiSecretKey = UserDefaults.standard.value(forKey: "api_secret_key"){
            UserDetails.sharedInstance.apiSecretKey = apiSecretKey as! String
        }

        if let phone = UserDefaults.standard.value(forKey: "phone"){
            UserDetails.sharedInstance.phoneNumber = phone as! String
            ApxorSDK.setUserIdentifier(UserDetails.sharedInstance.phoneNumber)
        }
        
        if let name = UserDefaults.standard.value(forKey: "name"){
            UserDetails.sharedInstance.name = name as! String
        }
        
        if let emailId = UserDefaults.standard.value(forKey: "email_id"){
            UserDetails.sharedInstance.emailAdress = emailId as! String
        }
        
        if let accessToken = UserDefaults.standard.value(forKey: "access_token"){
            UserDetails.sharedInstance.accessToken = accessToken as! String
        }
        
        if let referralAmount = UserDefaults.standard.value(forKey: "referralAmount"){
            UserDetails.sharedInstance.referralAmount = (referralAmount as! String)
        }
        
        if let bonusCash = UserDefaults.standard.value(forKey: "bonusCash"){
            UserDetails.sharedInstance.bonusCash = (bonusCash as! String)
        }
        
        if let dob = UserDefaults.standard.value(forKey: "dob") as? String{
            UserDetails.sharedInstance.dob = dob
        }


        if let unusedCash = UserDefaults.standard.value(forKey: "unusedCash"){
            UserDetails.sharedInstance.unusedCash = (unusedCash as! String)
        }
        
//        if let totalCredits = UserDefaults.standard.value(forKey: "totalCredits"){
//            UserDetails.sharedInstance.totalCredits = (totalCredits as! String)
//        }

        if let signupBonus = UserDefaults.standard.value(forKey: "signupBonus"){
            UserDetails.sharedInstance.signupBonus = (signupBonus as! String)
        }
        
        if let address = UserDefaults.standard.value(forKey: "address"){
            UserDetails.sharedInstance.signupBonus = (address as! String)
        }
        
        if let dob = UserDefaults.standard.value(forKey: "dob"){
            UserDetails.sharedInstance.dob = (dob as! String)
        }
        
        if let gender = UserDefaults.standard.value(forKey: "gender"){
            UserDetails.sharedInstance.gender = (gender as! String)
        }
        
    }
    
    class func makeDashbaordAsRootViewController()  {
        
        let storybaord = UIStoryboard.init(name: "Main", bundle: nil)
        let dashbaordVC = storybaord.instantiateViewController(withIdentifier: "DashboardViewController")
        let navVC = UINavigationController.init(rootViewController: dashbaordVC)
        navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
        navVC.navigationBar.isTranslucent = false;
        navVC.navigationBar.isHidden = true;
        APPDELEGATE.window?.rootViewController = navVC;
        AppHelper.callConfigAPI();
    }
    
    class func callLoginViewController()  {
        
        let storybaord = UIStoryboard.init(name: "Login", bundle: nil)
        let dashbaordVC = storybaord.instantiateViewController(withIdentifier: "LoginViewController")
        let navVC = UINavigationController.init(rootViewController: dashbaordVC)
        navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
        navVC.navigationBar.isTranslucent = false;
        navVC.navigationBar.isHidden = true;
        APPDELEGATE.window?.rootViewController = navVC;
     //   AppHelper.callConfigAPI();
    }
    
    
    class func makeDashbaordAsRootViewControllerWithPreSelectedLeaderboardTab()  {
        
        let storybaord = UIStoryboard.init(name: "Main", bundle: nil)
        let dashbaordVC = storybaord.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        let navVC = UINavigationController.init(rootViewController: dashbaordVC)
        navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
        dashbaordVC.preselectedTab = 300
        navVC.navigationBar.isTranslucent = false;
        navVC.navigationBar.isHidden = true;
        APPDELEGATE.window?.rootViewController = navVC;
    }
    
    class func makePromotionViewControllerAsRootViewController()  {
       
        let storybaord = UIStoryboard.init(name: "Main", bundle: nil)
        let promotionsVC = storybaord.instantiateViewController(withIdentifier: "PromotionsViewController") as? PromotionsViewController
        promotionsVC?.isFromSetting = true
        promotionsVC!.isFromNotification = true

        let navVC = UINavigationController.init(rootViewController: promotionsVC!)
        navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
        navVC.navigationBar.isTranslucent = false;
        navVC.navigationBar.isHidden = false;
        APPDELEGATE.window?.rootViewController = navVC;
    }

    class func makeLeagueViewControllerAsRootViewController(matchKey: String, seasonKey: String)  {

        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let leagueVC = storyboard.instantiateViewController(withIdentifier: "LeagueViewController") as? LeagueViewController
        leagueVC!.isFormNotification = true
        leagueVC?.matchKey = matchKey
        leagueVC?.seasonKey = seasonKey
        leagueVC?.isFormNotification = true
        let navVC = UINavigationController.init(rootViewController: leagueVC!)
        navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
        navVC.navigationBar.isTranslucent = false;
        navVC.navigationBar.isHidden = true;
        APPDELEGATE.window?.rootViewController = navVC;
    }
    
    class func makeLoginAsRootViewController()  {
        
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "RegisterUsingPhoneNumberViewController")
//        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController")

        let navVC = UINavigationController.init(rootViewController: loginVC)
        navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
        navVC.navigationBar.isHidden = true;
        APPDELEGATE.window?.rootViewController = navVC;
    }
    
    class func makeUserNameAsRootViewController()  {
        
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "EnterUserNameViewController")
        let navVC = UINavigationController.init(rootViewController: loginVC)
        navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
        navVC.navigationBar.isHidden = true;
        APPDELEGATE.window?.rootViewController = navVC;
    }

    class func makeJoinPrivateLeagueViewController(leagueCode: String?)  {
        
        let storybaord = UIStoryboard.init(name: "Main", bundle: nil)
        let joinPrivateLagueVC = storybaord.instantiateViewController(withIdentifier: "JoinPrivateLeagueViewController") as! JoinPrivateLeagueViewController
        joinPrivateLagueVC.leagueCode = leagueCode
        let navVC = UINavigationController.init(rootViewController: joinPrivateLagueVC)
        navVC.navigationBar.barTintColor = UIColor(red: 2.0/255, green: 93.0/255, blue: 186.0/255, alpha: 1)
        navVC.navigationBar.isTranslucent = false;
        APPDELEGATE.window?.rootViewController = navVC;
    }

    class func addBackButtonOnNavigationbar(title: String, target: UIViewController, isNeedToShowShadow: Bool)  {
        
        let button = UIButton.init(type: .custom)
        button.setTitle("     " + title, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 19.0)
        button.setImage(UIImage.init(named: "BackButtonArrow"), for: UIControlState.normal)
        button.sizeToFit()
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.addTarget(target, action: #selector(target.backButtonTapped), for: .touchUpInside)
        let navItem = target.navigationItem
        
        if isNeedToShowShadow {
            
            target.navigationController?.navigationBar.layer.masksToBounds = false
            target.navigationController?.navigationBar.layer.shadowColor = UIColor(red: 9.0/255, green: 98.0/255, blue: 195.0/255, alpha: 1.0).cgColor
            target.navigationController?.navigationBar.layer.shadowOpacity = 1.0
            target.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: -10, height: 0)
            target.navigationController?.navigationBar.layer.shadowRadius = 7.0
        }
        
        navItem.leftBarButtonItem = UIBarButtonItem.init(customView: button)
    }
    
    class func designBottomTabDesing(_ bottomView: UIView) {
        
        bottomView.layer.cornerRadius = 0;
        bottomView.layer.shadowColor = UIColor(red: 23.0/255, green: 37.0/255, blue: 42.0/255, alpha: 0.5).cgColor
        bottomView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bottomView.layer.shadowOpacity = 0.33
        bottomView.layer.shadowRadius = 3.0
        bottomView.layer.masksToBounds = false
    }
    
    class func shadowOnButton(_ button: UIButton) {
        
        button.layer.shadowColor = UIColor(red: 23.0/255, green: 37.0/255, blue: 42.0/255, alpha: 0.5).cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 0)
        button.layer.shadowOpacity = 0.33
        button.layer.shadowRadius = 3.0
        button.layer.masksToBounds = false
    }

    class func getMatchRemaingTime(startDateTimeStemp: String) -> Double{
        
        let serverUnixTimestamp = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
        let dateUnixTimestamp = Double(startDateTimeStemp) ?? 0
        
        var totalSeconds = 0.0
        
        if dateUnixTimestamp > serverUnixTimestamp {
            totalSeconds = dateUnixTimestamp - serverUnixTimestamp
        }
        else{
            totalSeconds = serverUnixTimestamp - dateUnixTimestamp
        }
        
        return totalSeconds
        
    }

    class func addBackButtonWithSubTitleOnNavigationbar(title: String, target: UIViewController, isNeedToShowShadow: Bool, subtitle: String)  {
        
        let button = UIButton.init(type: .custom)
        button.setTitle(title, for: UIControlState.normal)
        button.titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 19.0)
        let subtitleLable = UILabel(frame: CGRect(x: 47, y: 19, width: 200, height: 12))
        subtitleLable.text = subtitle
        subtitleLable.font = UIFont(name: "OpenSans-Semibold", size: 11.0)
        subtitleLable.textColor = UIColor.white
        button.addSubview(subtitleLable)
        button.setImage(UIImage.init(named: "BackButtonArrow"), for: UIControlState.normal)
        button.sizeToFit()
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.addTarget(target, action: #selector(target.backButtonTapped), for: .touchUpInside)
        let navItem = target.navigationItem
        
        if isNeedToShowShadow {
            target.navigationController?.navigationBar.layer.masksToBounds = false
            target.navigationController?.navigationBar.layer.shadowColor = UIColor(red: 9.0/255, green: 98.0/255, blue: 195.0/255, alpha: 1.0).cgColor
            target.navigationController?.navigationBar.layer.shadowOpacity = 1.0
            target.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: -10, height: 0)
            target.navigationController?.navigationBar.layer.shadowRadius = 7.0
        }
        
        let floatingButton = UIButton.init(type: .custom)
        floatingButton.setImage(UIImage.init(named: "FloatingButtonIcon"), for: UIControlState.normal)
        floatingButton.addTarget(target, action: #selector(target.floatingButtonTapped), for: .touchUpInside)
        navItem.leftBarButtonItem = UIBarButtonItem.init(customView: button)
        navItem.rightBarButtonItem = UIBarButtonItem.init(customView: floatingButton)
    }
    
    class func getGuestToken() {
        
        let parameters: Parameters = ["option": "generate","type": "guest"]

        WebServiceHandler.performPOSTRequest(urlString: kToakenURL, andParameters: parameters, andAcessToken: "") { (result, error) in

            if result != nil{
                let guestToken = result!["response"]!["access_token"].string
                UserDetails.sharedInstance.guestAccessToken = guestToken ?? "";
                let isSocial = result!["response"]!["is_social_enable"].stringValue
                if isSocial == "1" {
                    UserDetails.sharedInstance.isFacebookViewHidden = false
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateGuestToken"), object: nil)
                }
            }
            else{
                AppHelper.showAlertView(message: "kErrorMsg".localized(), isErrorMessage: true)
            }
        }
    }
    
        
    class func showShodowOnBottomTab(bottomView: UIView) {
        
        bottomView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        bottomView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bottomView.layer.shadowOpacity = 0.33
        bottomView.layer.shadowRadius = 6.0
        bottomView.layer.masksToBounds = false
    }

    class func showShodowOnHeaderView(bottomView: UIView) {
        
        bottomView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        bottomView.layer.shadowOffset = CGSize(width: -10, height: 0)
        bottomView.layer.shadowOpacity = 1.0
        bottomView.layer.shadowRadius = 7.0
        bottomView.layer.masksToBounds = false
    }
    
    class func showShodowOnCellsView(innerView: UIView)  {
    
        innerView.layer.cornerRadius = 7.0
        innerView.layer.shadowColor = UIColor(red: 78.0/255, green: 84.0/255, blue: 90.0/255, alpha: 0.5).cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        innerView.layer.shadowOpacity = 0.5
        innerView.layer.shadowRadius = 3
        innerView.layer.masksToBounds = false
    }
    
    class func showShodowOnAccountCellsView(innerView: UIView)  {
    
        innerView.layer.cornerRadius = 7.0
        innerView.layer.shadowColor = UIColor(red: 78.0/255, green: 84.0/255, blue: 90.0/255, alpha: 0.5).cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        innerView.layer.shadowOpacity = 1.6
        innerView.layer.shadowRadius = 3.5
        innerView.layer.masksToBounds = false
    }

    
    class func showShodowOnLeagueCellsView(innerView: UIView)  {
        
        innerView.layer.cornerRadius = 7.0
        innerView.layer.shadowColor = UIColor(red: 29.0/255, green: 28.0/255, blue: 61.0/255, alpha: 1.0).cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        innerView.layer.shadowOpacity = 0.30
        innerView.layer.shadowRadius = 3.5
        innerView.layer.masksToBounds = false
    }
    
    class func showShodowOnAllSeriesView(innerView: UIView)  {
        
        innerView.layer.shadowColor = UIColor.black.cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        innerView.layer.shadowOpacity = 0.75
        innerView.layer.shadowRadius = 6.5
        innerView.layer.masksToBounds = false
    }

    
    class func saveValueInCoreData(urlString: String, result: Data){
        
        var managedContext: NSManagedObjectContext?
        // 1
        if #available(iOS 10.0, *) {
            managedContext =
                APPDELEGATE.persistentContainer.viewContext
        } else {
            managedContext = APPDELEGATE.managedObjectContext
        }
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: "ResponseDetails",
                                       in: managedContext!)!
        
        let response = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        // 3
        response.setValue(urlString, forKeyPath: "urlString")
        response.setValue(result, forKeyPath: "result")
        response.setValue(Date(), forKeyPath: "createTime")

        // 4
        do {
            try managedContext?.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    class func getValueFromCoreData(urlString: String) -> JSON?{
        
        var managedContext: NSManagedObjectContext?
        // 1
        if #available(iOS 10.0, *) {
            managedContext =
                APPDELEGATE.persistentContainer.viewContext
        } else {
            managedContext = APPDELEGATE.managedObjectContext
        }
        
        let entity = NSEntityDescription.entity(forEntityName: "ResponseDetails", in: managedContext!)!
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entity
        fetchRequest.predicate = NSPredicate(format: "urlString == %@", urlString)

        do {
            let result = try managedContext!.fetch(fetchRequest)
            if result.count > 0 {
                if let responseDetails = result[0] as? ResponseDetails{
                    let createTime = responseDetails.createTime

                    let createTimestemp = createTime?.timeIntervalSince1970
                    let currentTimestemp = Date().timeIntervalSince1970

                    let totalTimeDifference = currentTimestemp - createTimestemp!
                    if totalTimeDifference > 3600{
                        return nil
                    }
                    
                    let resultData = responseDetails.result
                    let responseJSON = JSON(resultData as AnyObject);
                    return responseJSON
                }
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return nil
    }
    
    
    class func updateValueInCoreData(urlString: String, resultData: Data){
        
        var managedContext: NSManagedObjectContext?
        // 1
        if #available(iOS 10.0, *) {
            managedContext =
                APPDELEGATE.persistentContainer.viewContext
        } else {
            managedContext = APPDELEGATE.managedObjectContext
        }
        
        let entity = NSEntityDescription.entity(forEntityName: "ResponseDetails", in: managedContext!)!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entity
        fetchRequest.predicate = NSPredicate(format: "urlString == %@", urlString)
        
        do {
            let result = try managedContext!.fetch(fetchRequest)
            if result.count > 0 {
               
                if let responseDetails = result[0] as? ResponseDetails{
                    
                    responseDetails.setValue(urlString, forKeyPath: "urlString")
                    responseDetails.setValue(resultData, forKeyPath: "result")
                    responseDetails.setValue(Date(), forKeyPath: "createTime")
                }
                try managedContext?.save()
            }
            else{
                AppHelper.saveValueInCoreData(urlString: urlString, result: resultData)
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
    
    
    class func deleteAllRecordsFromCoreData(){
        
        var managedContext: NSManagedObjectContext?
        // 1
        if #available(iOS 10.0, *) {
            managedContext =
                APPDELEGATE.persistentContainer.viewContext
        } else {
            managedContext = APPDELEGATE.managedObjectContext
        }
        
        let entity = NSEntityDescription.entity(forEntityName: "ResponseDetails", in: managedContext!)!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entity
        
        do {
            let result = try managedContext!.fetch(fetchRequest)
            
            for details in result {
                managedContext?.delete(details as! ResponseDetails);
            }
            try managedContext?.save()
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
    
    class func getTimeDifferenceBetweenTwoDates(serverTimestamp: String?, dateTimestamp: String?) -> String {
        
        if (serverTimestamp == nil) {
            return ""
        }
        
        if (dateTimestamp == nil) {
            return ""
        }
        
        let serverUnixTimestamp = Double(serverTimestamp!) ?? 0
        let dateUnixTimestamp = Double(dateTimestamp!) ?? 0
        
        var totalSeconds = 0.0
        
        if dateUnixTimestamp > serverUnixTimestamp {
            totalSeconds = dateUnixTimestamp - serverUnixTimestamp
        }
        else{
            return ""
            totalSeconds = serverUnixTimestamp - dateUnixTimestamp
        }
        
        let hours = Int(totalSeconds/3600)
        let days = Int(hours/24)
        let tempHours = Float(hours)        
        let remaingHour = Int((tempHours.truncatingRemainder(dividingBy: 24)))
        let remaingSecs = (totalSeconds.truncatingRemainder(dividingBy: 3600))
        let minutes = Int(remaingSecs / 60)
        let secs = Int((remaingSecs.truncatingRemainder(dividingBy: 60)))
        
        var minStr = ""
        var secStr = ""
        
        if (minutes < 10) {
            minStr = "0" + String(minutes)
        }
        else{
            minStr = String(minutes)
        }
        
        if (secs < 10) {
            secStr = "0" + String(secs)
        }
        else{
            secStr = String(secs)
        }
        if days > 0{
//            let requiredString = String(days) + "d " + String(remaingHour) + "h " + minStr + "m " + secStr + "s"
            let requiredString = String(days) + "d " + String(remaingHour) + "h " + minStr + "m "

            return requiredString
        }
        else{
            if hours > 0{
                if hours >= 1{
                    let requiredString = String(hours) + "h " + minStr + "m "
                    return requiredString
                }
                else{
                    let requiredString = String(hours) + "h " + minStr + "m " + secStr + "s"
                    return requiredString
                }
            }
            else{
                let requiredString = minStr + "m " + secStr + "s"
                return requiredString
            }
        }
    }
    
    class func convertRemaingTimeIntoSeconds(totalSeconds: Double) -> String {
        
        let hours = Int(totalSeconds/3600)
        let days = Int(hours/24)
        
        let tempHours = Float(hours)
        
        let remaingHour = Int((tempHours.truncatingRemainder(dividingBy: 24)))
        let remaingSecs = (totalSeconds.truncatingRemainder(dividingBy: 3600))
        let minutes = Int(remaingSecs / 60)
        let secs = Int((remaingSecs.truncatingRemainder(dividingBy: 60)))
        
        var minStr = ""
        var secStr = ""
        
        if (minutes < 10) {
            minStr = "0" + String(minutes)
        }
        else{
            minStr = String(minutes)
        }
        
        if (secs < 10) {
            secStr = "0" + String(secs)
        }
        else{
            secStr = String(secs)
        }
        if days > 0{
//            let requiredString = String(days) + "d " + String(remaingHour) + "h " + minStr + "m " + secStr + "s"
            let requiredString = String(days) + "d " + String(remaingHour) + "h " + minStr + "m "

            return requiredString
        }
        else{
            if hours > 0{
                if hours >= 1{
                    let requiredString = String(hours) + "h " + minStr + "m "
                    return requiredString
                }
                else{
                    let requiredString = String(hours) + "h " + minStr + "m " + secStr + "s"
                    return requiredString
                }
            }
            else{
                let requiredString = minStr + "m " + secStr + "s"
                return requiredString
            }
        }
    }
    
    class func showToast(message: String) {
        AppHelper.showAlertView(message: message, isErrorMessage: false)
//        let toast = Toast(text: message)
//
//        toast.show()
    }
    
    
    class func getTimeDifferenceBetweenTwoDatesInReverserOrder(serverTimestamp: String?, dateTimestamp: String?) -> String {
        
        if (serverTimestamp == nil) {
            return ""
        }
        
        if (dateTimestamp == nil) {
            return ""
        }
        
        let serverUnixTimestamp = Double(serverTimestamp!)
        let dateUnixTimestamp = Double(dateTimestamp!)
        
        var totalSeconds = 0.0
        
        if dateUnixTimestamp! > serverUnixTimestamp! {
            totalSeconds = dateUnixTimestamp! - serverUnixTimestamp!
        }
        else{
            totalSeconds = serverUnixTimestamp! - dateUnixTimestamp!
        }

        if totalSeconds < 0 {
            totalSeconds = 0
        }
        let hours = Int(totalSeconds/3600)
        let days = Int(hours/24)
        
        let tempHours = Float(hours)
        
        let remaingHour = Int((tempHours.truncatingRemainder(dividingBy: 24)))
        let remaingSecs = (totalSeconds.truncatingRemainder(dividingBy: 3600))
        let minutes = Int(remaingSecs / 60)
        let secs = Int((remaingSecs.truncatingRemainder(dividingBy: 60)))
        
        var minStr = ""
        var secStr = ""
        
        if (minutes < 10) {
            minStr = "0" + String(minutes)
        }
        else{
            minStr = String(minutes)
        }
        
        if (secs < 10) {
            secStr = "0" + String(secs)
        }
        else{
            secStr = String(secs)
        }
        if days > 0{
//            let requiredString = String(days) + "d " + String(remaingHour) + "h " + minStr + "m " + secStr + "s"
            let requiredString = String(days) + "d " + String(remaingHour) + "h " + minStr + "m "

            return requiredString
        }
        else{
            if hours > 0{
                
                if hours >= 1{
                    let requiredString = String(hours) + "h " + minStr + "m "
                    return requiredString
                }
                else{
                    let requiredString = String(hours) + "h " + minStr + "m " + secStr + "s"
                    return requiredString
                }
            }
            else{
                let requiredString = minStr + "m " + secStr + "s"
                return requiredString
            }
        }
    }
            
    class func callGenrateUserAccessTokenAPIWithLoader(isNeedToShowLoader: Bool, completionBlock: @escaping () -> Void) {
        
        let parameters: Parameters = ["option": "generate","type": "users", "username": UserDetails.sharedInstance.userID, "password": UserDetails.sharedInstance.apiSecretKey]
    
        WebServiceHandler.performPOSTRequest(urlString: kToakenURL, andParameters: parameters, andAcessToken: "") { (result, error) in
            
            if result != nil{
                
                if let response = result!["response"]{
                    if let accessToken = response.dictionary?["access_token"]{
                        UserDetails.sharedInstance.accessToken = accessToken.string!
                        UserDefaults.standard.set(UserDetails.sharedInstance.accessToken, forKey: "access_token")
                        UserDefaults.standard.synchronize()
                    }
                }
            }
            completionBlock()
        }
    }
    
    class func isNotValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return !emailTest.evaluate(with: email)
    }
    
    class func showAlertViewWithoutMessage(titleMessage: String, isErrorMessage: Bool)  {
        
        AppHelper.sharedInstance.messageView?.removeFromSuperview()
        AppHelper.sharedInstance.messageView = MessageDisplayView(frame: CGRect(x: 0, y: -80, width: UIScreen.main.bounds.width, height: 60))
        if isErrorMessage {
            AppHelper.sharedInstance.messageView?.messageDisplay(message: titleMessage, color: UIColor(red: 251.0/255, green: 151.0/255, blue: 138.0/255, alpha: 1))
        }
        else{
            AppHelper.sharedInstance.messageView?.messageDisplay(message: titleMessage, color: UIColor(red: 57.0/255, green: 217.0/255, blue: 172.0/255, alpha: 1))
        }

        APPDELEGATE.window?.addSubview(AppHelper.sharedInstance.messageView!)
        AppHelper.sharedInstance.messageView?.showAnimation()
    }
    
    
    class func showMatchCLosedAlertAndPopToRootViewController()  {

        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            if let dashboardVC = navigationVC.viewControllers.first as? DashboardViewController{
                if dashboardVC.preselectedTab == 100{
                    
                    if (dashboardVC is EnterUserNameViewController) || (dashboardVC is EnterOTPViewController) || (dashboardVC is EnterPhoneNumberViewController){
                        return;
                    }

                    
                    let visiableVC = navigationVC.visibleViewController
                    if visiableVC is DashboardViewController{
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateListOnPullToRefresh"), object: nil)
                        return;
                    }
                    else{
                                        
                        let alert = UIAlertController(title: "This match is closed", message: nil, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateListOnPullToRefresh"), object: nil)
                            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                navigationVC.popToRootViewController(animated: true)
                            }
                        }))
                        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
//            let dashboardVC = navigationVC.visibleViewController
//            if (dashboardVC is EnterUserNameViewController) || (dashboardVC is EnterOTPViewController) || (dashboardVC is EnterPhoneNumberViewController){
//                return;
//            }
//            else if dashboardVC is DashboardViewController{
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateListOnPullToRefresh"), object: nil)
//                return;
//            }
//        }
//
//        let alert = UIAlertController(title: "This match is closed", message: nil, preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action -> Void in
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateListOnPullToRefresh"), object: nil)
//            if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
//                navigationVC.popToRootViewController(animated: true)
//            }
//        }))
//        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    class func makeCommaSeparatedDigitsWithString(digites: String) -> String {
        if digites.count == 0{
            return "0"
        }
        
        let floatValue = Float(digites) ?? 0.0
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        guard var requiredString = formatter.string(from: NSNumber(value: floatValue)) else{
            return "0"
        }
        
        requiredString = requiredString.replacingOccurrences(of: ",", with: "");
        requiredString = requiredString.replacingOccurrences(of: ".00", with: "");
        return requiredString
    }
    
    class func makeCommaSeparatedDigitsForDecimalWithString(digites: String) -> String {
        if digites.count == 0 {
            return "0"
        }
        
        let floatValue = Float(digites) ?? 0.0
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        guard var requiredString = formatter.string(from: NSNumber(value: floatValue)) else{
            return "0"
        }
        
        requiredString = requiredString.replacingOccurrences(of: ",", with: "");
        requiredString = requiredString.replacingOccurrences(of: ".00", with: "");        
        return requiredString
    }
    
    class func formatDecimalNumberString(digites: String) -> String {
        if digites.count == 0 {
            return "0"
        }
        
        let floatValue = Float(digites) ?? 0.0
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        
        guard var requiredString = formatter.string(from: NSNumber(value: floatValue)) else{
            return "0.00"
        }
        requiredString = requiredString.replacingOccurrences(of: ",", with: "");
        return requiredString
    }
    
    class func callConfigAPI(){
        let urlString = kSignupURL + "?option=get_configs&user_id=" + UserDetails.sharedInstance.userID
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            if result != nil{
                let status = result!["status"]
                if status == "200"{
                    if let response = result!["response"].dictionary{
                        if let configArray = response["configs"]?.array{
                            if configArray.count > 0{
                                let sportsLandingOrderTemp = configArray[0]["sports_landing_order"].string ?? "1,2,3"
//                                let sportsLandingOrderTemp = "1,2,3,4,5,6"
                                let tempArray = sportsLandingOrderTemp.components(separatedBy: ",")
                                var sportsLandingOrder = ""
                                
                                for str in tempArray {
                                    if (str == "1") || (str == "2") || (str == "3") || (str == "4") || (str == "5") || (str == "6"){
                                        if sportsLandingOrder.count == 0 {
                                            sportsLandingOrder = str
                                        }
                                        else{
                                            sportsLandingOrder = sportsLandingOrder + "," + str
                                        }
                                    }
                                }
                                UserDefaults.standard.set(sportsLandingOrder, forKey: "sports_landing_order")
                                UserDefaults.standard.synchronize()
                                let iOSMinVersion = configArray[0]["iphone_min_version"].string ?? ""
                                let landingSport = configArray[0]["landing_sport"].string ?? ""
                                if (landingSport == "1") || (landingSport == "2") || (landingSport == "3") || (landingSport == "4") || (landingSport == "5") || (landingSport == "6") {
                                    UserDefaults.standard.set(landingSport, forKey: "landingSport")
                                }
                                else{
                                    UserDefaults.standard.set("1", forKey: "landingSport")
                                }

                                let partnershipProgramStatuslandingSport = configArray[0]["partnership_program_status"].string ?? ""
                                UserDetails.sharedInstance.isPartnershipProgramStatus = partnershipProgramStatuslandingSport
                                let iOSMaxVersion = configArray[0]["iphone_max_version"].string ?? ""
                                let updateDuration = configArray[0]["iphone_duration"].string ?? ""
                                let partnershipProgramStatus = configArray[0]["partnership_program_status"].stringValue
                                let rewardStatus = configArray[0]["reward_status"].string ?? ""
                                let passStatus = configArray[0]["pass_status"].string ?? ""
                                UserDefaults.standard.set(passStatus, forKey: "passStatus")
                                UserDefaults.standard.set(rewardStatus, forKey: "rewardStatus")
                                UserDefaults.standard.set(partnershipProgramStatus, forKey: "partnershipProgramStatus")

                                UserDefaults.standard.set(iOSMinVersion, forKey: "iOSMinVersion")
                                UserDefaults.standard.set(iOSMaxVersion, forKey: "iOSMaxVersion")
                                UserDefaults.standard.synchronize()
                                showUpdatePopup(result: result!, iOSMinVersion: iOSMinVersion, iOSMaxVersion: iOSMaxVersion, updateDuration: updateDuration)
                            }
                        }
                        
                        if let userInfo = response["user_info"]?.dictionary{
                            let total_classic = userInfo["total_classic"]?.stringValue
                            let total_classic_kb = userInfo["total_classic_kb"]?.stringValue
                            let total_classic_fb = userInfo["total_classic_fb"]?.stringValue
                            let total_classic_bk = userInfo["total_classic_bk"]?.stringValue
                            let total_classic_bs = userInfo["total_classic_bs"]?.stringValue
                            let total_bowling = userInfo["total_bowling"]?.stringValue
                            let total_batting = userInfo["total_batting"]?.stringValue
                            
                            let total_reverse = userInfo["total_reverse"]?.stringValue
                            let total_wizard = userInfo["total_wizard"]?.stringValue

                            UserDefaults.standard.set(total_classic, forKey: "total_classic")
                            UserDefaults.standard.set(total_classic_kb, forKey: "total_classic_kb")
                            UserDefaults.standard.set(total_classic_fb, forKey: "total_classic_fb")
                            UserDefaults.standard.set(total_classic_bk, forKey: "total_classic_bsk")
                            UserDefaults.standard.set(total_classic_bs, forKey: "total_classic_bsb")

                            UserDefaults.standard.set(total_bowling, forKey: "total_bowling")
                            UserDefaults.standard.set(total_batting, forKey: "total_batting")
                            UserDefaults.standard.set(total_reverse, forKey: "total_reverse")
                            UserDefaults.standard.set(total_wizard, forKey: "total_wizard")

                            let params = [batting_contest: total_batting, bowling_contest: total_bowling, classic_contest: total_classic, kabaddi_contest: total_classic_kb, football_contest: total_classic_fb, basketball_contest: total_classic_bk, baseball_contest: total_classic_bs, device: PLATFORM_iPHONE, user_name: UserDetails.sharedInstance.userName, custome_name: UserDetails.sharedInstance.name]
                            CleverTapEventDetails.updateProfile(updateProfile: params as [String : Any])
                            UserDefaults.standard.synchronize()
                        }
                    }
                }
            }
        }        
    }
        
    class func showUpdatePopupFromSetting() {

        guard let iOSMinVersion = UserDefaults.standard.value(forKey: "iOSMinVersion") as? String else {
            return
        }
        
        guard let iOSMaxVersion = UserDefaults.standard.value(forKey: "iOSMaxVersion") as? String else {
            return
        }
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String

        if UserDetails.sharedInstance.userLoggedIn{
            
            if !UserDetails.sharedInstance.isForceUpdateViewOpen{
                if appVersion < iOSMinVersion{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let forceUpdateVC = storyboard.instantiateViewController(withIdentifier: "ForceUpdateViewController")
                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                        navVC.pushViewController(forceUpdateVC, animated: false)
                    }
                }
                else if appVersion < iOSMaxVersion{
                    let forceUpdateView = UpdateView(frame: APPDELEGATE.window!.frame)
                    forceUpdateView.isForceUpdateEnable = false
                    APPDELEGATE.window?.addSubview(forceUpdateView)
                }
                else{
                    AppHelper.showAlertView(message: "no_update_available".localized(), isErrorMessage: false)
                }
            }
        }
    }
    
    class func showUpdatePopup(result: JSON, iOSMinVersion: String, iOSMaxVersion: String, updateDuration: String) {
        if let unserMaintenance = result["under_maintenance"].string {
            if unserMaintenance != "yes"{

                UserDetails.sharedInstance.iOSMaxVersion = iOSMinVersion
                UserDetails.sharedInstance.iOSMaxVersion = iOSMaxVersion
                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String

                if UserDetails.sharedInstance.userLoggedIn{
                    
                    if !UserDetails.sharedInstance.isForceUpdateViewOpen{
                        if appVersion < iOSMinVersion{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let forceUpdateVC = storyboard.instantiateViewController(withIdentifier: "ForceUpdateViewController")
                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                navVC.pushViewController(forceUpdateVC, animated: false)
                            }
                        }
                        else if appVersion < iOSMaxVersion{
                            
                            let serverTime: String = (UserDefaults.standard.value(forKey: "lastForceUpdateTime") ?? "0") as! String
                            if UserDetails.sharedInstance.serverTimeStemp.count == 0{
                                UserDetails.sharedInstance.serverTimeStemp = "0"
                            }
                            let duration = (Double(updateDuration) ?? 0.0)  * 60.0 * 60.0
                            
                            if (Double(UserDetails.sharedInstance.serverTimeStemp)! - Double(serverTime)!) > duration{
                                let forceUpdateView = UpdateView(frame: APPDELEGATE.window!.frame)
                                forceUpdateView.isForceUpdateEnable = false
                                APPDELEGATE.window?.addSubview(forceUpdateView)
                                UserDetails.sharedInstance.isForceUpdateViewOpen = true
                                UserDefaults.standard.set(UserDetails.sharedInstance.serverTimeStemp, forKey: "lastForceUpdateTime")
                                UserDefaults.standard.synchronize()
                            }
                        }
                    }
                }
            }
        }
    }
    
    class func getLandingOrder() -> (Int, Int, Int, Int, Int, Int) {
                
        if let sportsLandingOrder = UserDefaults.standard.value(forKey: "sports_landing_order") as? String {
            let landigOrderArray = sportsLandingOrder.components(separatedBy: ",")
            
            if landigOrderArray.count >= 6 {
                let firstTab = landigOrderArray[0]
                let secondTab = landigOrderArray[1]
                let thirdTab = landigOrderArray[2]
                let fourthTab = landigOrderArray[3]
                let fifthTab = landigOrderArray[4]
                let sixthTab = landigOrderArray[5]

                var firstTabType = GameType.None.rawValue
                var secondTabType = GameType.None.rawValue
                var thirdTabType = GameType.None.rawValue
                var fourthTabType = GameType.None.rawValue
                var fifthTabType = GameType.None.rawValue
                var sixthTabType = GameType.None.rawValue

                if firstTab == "1" {
                    firstTabType = GameType.Cricket.rawValue
                }
                else if firstTab == "2" {
                    firstTabType = GameType.Kabaddi.rawValue
                }
                else if firstTab == "3" {
                    firstTabType = GameType.Football.rawValue
                }
                else if firstTab == "4" {
                    firstTabType = GameType.Quiz.rawValue
                }
                else if firstTab == "5" {
                    firstTabType = GameType.Basketball.rawValue
                }
                else if firstTab == "6" {
                    firstTabType = GameType.Baseball.rawValue
                }

                if secondTab == "1" {
                    secondTabType = GameType.Cricket.rawValue
                }
                else if secondTab == "2" {
                  //  secondTabType = GameType.Kabaddi.rawValue
                }
                else if secondTab == "3" {
                    secondTabType = GameType.Football.rawValue
                }
                else if secondTab == "4" {
                    secondTabType = GameType.Quiz.rawValue
                }
                else if secondTab == "5" {
                    secondTabType = GameType.Basketball.rawValue
                }
                else if secondTab == "6" {
                    secondTabType = GameType.Baseball.rawValue
                }

                if thirdTab == "1" {
                    thirdTabType = GameType.Cricket.rawValue
                }
                else if thirdTab == "2" {
                    thirdTabType = GameType.Kabaddi.rawValue
                }
                else if thirdTab == "3" {
                    thirdTabType = GameType.Football.rawValue
                }
                else if thirdTab == "4" {
                    thirdTabType = GameType.Quiz.rawValue
                }
                else if thirdTab == "5" {
                    thirdTabType = GameType.Basketball.rawValue
                }
                else if thirdTab == "6" {
                    thirdTabType = GameType.Baseball.rawValue
                }

                if fourthTab == "1" {
                    fourthTabType = GameType.Cricket.rawValue
                }
                else if fourthTab == "2" {
                    fourthTabType = GameType.Kabaddi.rawValue
                }
                else if fourthTab == "3" {
                    fourthTabType = GameType.Football.rawValue
                }
                else if fourthTab == "4" {
                    fourthTabType = GameType.Quiz.rawValue
                }
                else if fourthTab == "5" {
                    fourthTabType = GameType.Basketball.rawValue
                }
                else if fourthTab == "6" {
                    fourthTabType = GameType.Baseball.rawValue
                }
                
                if fifthTab == "1" {
                    fifthTabType = GameType.Cricket.rawValue
                }
                else if fifthTab == "2" {
                    fifthTabType = GameType.Kabaddi.rawValue
                }
                else if fifthTab == "3" {
                    fifthTabType = GameType.Football.rawValue
                }
                else if fifthTab == "4" {
                    fifthTabType = GameType.Quiz.rawValue
                }
                else if fifthTab == "5" {
                    fifthTabType = GameType.Basketball.rawValue
                }
                else if fifthTab == "6" {
                    fifthTabType = GameType.Baseball.rawValue
                }

                if sixthTab == "1" {
                    sixthTabType = GameType.Cricket.rawValue
                }
                else if sixthTab == "2" {
                    sixthTabType = GameType.Kabaddi.rawValue
                }
                else if sixthTab == "3" {
                    sixthTabType = GameType.Football.rawValue
                }
                else if sixthTab == "4" {
                    sixthTabType = GameType.Quiz.rawValue
                }
                else if sixthTab == "5" {
                    sixthTabType = GameType.Basketball.rawValue
                }
                else if sixthTab == "6" {
                    sixthTabType = GameType.Baseball.rawValue
                }

                return (firstTabType, secondTabType, thirdTabType, fourthTabType, fifthTabType, sixthTabType)
            }
            else if landigOrderArray.count == 5 {
                let firstTab = landigOrderArray[0]
                let secondTab = landigOrderArray[1]
                let thirdTab = landigOrderArray[2]
                let fourthTab = landigOrderArray[3]
                let fifthTab = landigOrderArray[4]

                var firstTabType = GameType.None.rawValue
                var secondTabType = GameType.None.rawValue
                var thirdTabType = GameType.None.rawValue
                var fourthTabType = GameType.None.rawValue
                var fifthTabType = GameType.None.rawValue

                if firstTab == "1" {
                    firstTabType = GameType.Cricket.rawValue
                }
                else if firstTab == "2" {
                    firstTabType = GameType.Kabaddi.rawValue
                }
                else if firstTab == "3" {
                    firstTabType = GameType.Football.rawValue
                }
                else if firstTab == "4" {
                    firstTabType = GameType.Quiz.rawValue
                }
                else if firstTab == "5" {
                    firstTabType = GameType.Basketball.rawValue
                }
                else if firstTab == "6" {
                    firstTabType = GameType.Baseball.rawValue
                }

                if secondTab == "1" {
                    secondTabType = GameType.Cricket.rawValue
                }
                else if secondTab == "2" {
                    secondTabType = GameType.Kabaddi.rawValue
                }
                else if secondTab == "3" {
                    secondTabType = GameType.Football.rawValue
                }
                else if secondTab == "4" {
                    secondTabType = GameType.Quiz.rawValue
                }
                else if secondTab == "5" {
                    secondTabType = GameType.Basketball.rawValue
                }
                else if secondTab == "6" {
                    secondTabType = GameType.Baseball.rawValue
                }


                if thirdTab == "1" {
                    thirdTabType = GameType.Cricket.rawValue
                }
                else if thirdTab == "2" {
                    thirdTabType = GameType.Kabaddi.rawValue
                }
                else if thirdTab == "3" {
                    thirdTabType = GameType.Football.rawValue
                }
                else if thirdTab == "4" {
                    thirdTabType = GameType.Quiz.rawValue
                }
                else if thirdTab == "5" {
                    thirdTabType = GameType.Basketball.rawValue
                }
                else if thirdTab == "6" {
                    thirdTabType = GameType.Baseball.rawValue
                }

                if fourthTab == "1" {
                    fourthTabType = GameType.Cricket.rawValue
                }
                else if fourthTab == "2" {
                    fourthTabType = GameType.Kabaddi.rawValue
                }
                else if fourthTab == "3" {
                    fourthTabType = GameType.Football.rawValue
                }
                else if fourthTab == "4" {
                    fourthTabType = GameType.Quiz.rawValue
                }
                else if fourthTab == "5" {
                    fourthTabType = GameType.Basketball.rawValue
                }
                else if fourthTab == "6" {
                    fourthTabType = GameType.Baseball.rawValue
                }

                
                if fifthTab == "1" {
                    fifthTabType = GameType.Cricket.rawValue
                }
                else if fifthTab == "2" {
                    fifthTabType = GameType.Kabaddi.rawValue
                }
                else if fifthTab == "3" {
                    fifthTabType = GameType.Football.rawValue
                }
                else if fifthTab == "4" {
                    fifthTabType = GameType.Quiz.rawValue
                }
                else if fifthTab == "5" {
                    fifthTabType = GameType.Basketball.rawValue
                }
                else if fifthTab == "6" {
                    fifthTabType = GameType.Baseball.rawValue
                }



                return (firstTabType, secondTabType, thirdTabType, fourthTabType, fifthTabType, GameType.None.rawValue)
            }
            else if landigOrderArray.count == 4 {
                let firstTab = landigOrderArray[0]
                let secondTab = landigOrderArray[1]
                let thirdTab = landigOrderArray[2]
                let fourthTab = landigOrderArray[3]

                var firstTabType = GameType.None.rawValue
                var secondTabType = GameType.None.rawValue
                var thirdTabType = GameType.None.rawValue
                var fourthTabType = GameType.None.rawValue

                if firstTab == "1" {
                    firstTabType = GameType.Cricket.rawValue
                }
                else if firstTab == "2" {
                    firstTabType = GameType.Kabaddi.rawValue
                }
                else if firstTab == "3" {
                    firstTabType = GameType.Football.rawValue
                }
                else if firstTab == "4" {
                    firstTabType = GameType.Quiz.rawValue
                }
                else if firstTab == "5" {
                    firstTabType = GameType.Basketball.rawValue
                }
                else if firstTab == "6" {
                    firstTabType = GameType.Baseball.rawValue
                }

                if secondTab == "1" {
                    secondTabType = GameType.Cricket.rawValue
                }
                else if secondTab == "2" {
                    secondTabType = GameType.Kabaddi.rawValue
                }
                else if secondTab == "3" {
                    secondTabType = GameType.Football.rawValue
                }
                else if secondTab == "4" {
                    secondTabType = GameType.Quiz.rawValue
                }
                else if secondTab == "5" {
                    secondTabType = GameType.Basketball.rawValue
                }
                else if secondTab == "6" {
                    secondTabType = GameType.Baseball.rawValue
                }

                if thirdTab == "1" {
                    thirdTabType = GameType.Cricket.rawValue
                }
                else if thirdTab == "2" {
                    thirdTabType = GameType.Kabaddi.rawValue
                }
                else if thirdTab == "3" {
                    thirdTabType = GameType.Football.rawValue
                }
                else if thirdTab == "4" {
                    thirdTabType = GameType.Quiz.rawValue
                }
                else if thirdTab == "5" {
                    thirdTabType = GameType.Basketball.rawValue
                }
                else if thirdTab == "6" {
                    thirdTabType = GameType.Baseball.rawValue
                }

                if fourthTab == "1" {
                    fourthTabType = GameType.Cricket.rawValue
                }
                else if fourthTab == "2" {
                    fourthTabType = GameType.Kabaddi.rawValue
                }
                else if fourthTab == "3" {
                    fourthTabType = GameType.Football.rawValue
                }
                else if fourthTab == "4" {
                    fourthTabType = GameType.Quiz.rawValue
                }
                else if fourthTab == "5" {
                    fourthTabType = GameType.Basketball.rawValue
                }
                else if fourthTab == "6" {
                    fourthTabType = GameType.Baseball.rawValue
                }

                return (firstTabType, secondTabType, thirdTabType, fourthTabType, GameType.None.rawValue, GameType.None.rawValue)
            }
            else if landigOrderArray.count >= 3 {
                let firstTab = landigOrderArray[0]
                let secondTab = landigOrderArray[1]
                let thirdTab = landigOrderArray[2]
                
                var firstTabType = GameType.None.rawValue
                var secondTabType = GameType.None.rawValue
                var thirdTabType = GameType.None.rawValue

                if firstTab == "1" {
                    firstTabType = GameType.Cricket.rawValue
                }
                else if firstTab == "2" {
                    firstTabType = GameType.Kabaddi.rawValue
                }
                else if firstTab == "3" {
                    firstTabType = GameType.Football.rawValue
                }
                else if firstTab == "4" {
                    firstTabType = GameType.Quiz.rawValue
                }
                else if firstTab == "5" {
                    firstTabType = GameType.Basketball.rawValue
                }
                else if firstTab == "6" {
                    firstTabType = GameType.Baseball.rawValue
                }
                
                
                if secondTab == "1" {
                    secondTabType = GameType.Cricket.rawValue
                }
                else if secondTab == "2" {
                    secondTabType = GameType.Kabaddi.rawValue
                }
                else if secondTab == "3" {
                    secondTabType = GameType.Football.rawValue
                }
                else if secondTab == "4" {
                    secondTabType = GameType.Quiz.rawValue
                }
                else if secondTab == "5" {
                    secondTabType = GameType.Basketball.rawValue
                }
                else if secondTab == "6" {
                    secondTabType = GameType.Baseball.rawValue
                }

                if thirdTab == "1" {
                    thirdTabType = GameType.Cricket.rawValue
                }
                else if thirdTab == "2" {
                    thirdTabType = GameType.Kabaddi.rawValue
                }
                else if thirdTab == "3" {
                    thirdTabType = GameType.Football.rawValue
                }
                else if thirdTab == "4" {
                    thirdTabType = GameType.Quiz.rawValue
                }
                else if thirdTab == "5" {
                    thirdTabType = GameType.Basketball.rawValue
                }
                else if thirdTab == "6" {
                    thirdTabType = GameType.Baseball.rawValue
                }
                
                
                return (firstTabType, secondTabType, thirdTabType, GameType.None.rawValue, GameType.None.rawValue, GameType.None.rawValue)
            }
            else if landigOrderArray.count >= 2 {
                let firstTab = landigOrderArray[0]
                let secondTab = landigOrderArray[1]
                
                var firstTabType = GameType.None.rawValue
                var secondTabType = GameType.None.rawValue
                
                if firstTab == "1" {
                    firstTabType = GameType.Cricket.rawValue
                }
                else if firstTab == "2" {
                    firstTabType = GameType.Kabaddi.rawValue
                }
                else if firstTab == "3" {
                    firstTabType = GameType.Football.rawValue
                }
                else if firstTab == "4" {
                    firstTabType = GameType.Quiz.rawValue
                }
                else if firstTab == "5" {
                    firstTabType = GameType.Basketball.rawValue
                }
                else if firstTab == "6" {
                    firstTabType = GameType.Baseball.rawValue
                }


                if secondTab == "1" {
                    secondTabType = GameType.Cricket.rawValue
                }
                else if secondTab == "2" {
                    secondTabType = GameType.Kabaddi.rawValue
                }
                else if secondTab == "3" {
                    secondTabType = GameType.Football.rawValue
                }
                else if secondTab == "4" {
                     secondTabType = GameType.Quiz.rawValue
                }
                else if secondTab == "5" {
                    secondTabType = GameType.Basketball.rawValue
                }
                else if secondTab == "6" {
                    secondTabType = GameType.Baseball.rawValue
                }

                return (firstTabType, secondTabType, GameType.None.rawValue, GameType.None.rawValue, GameType.None.rawValue, GameType.None.rawValue)
            }
            else if landigOrderArray.count >= 1 {
                let firstTab = landigOrderArray[0]
                var firstTabType = GameType.None.rawValue
                
                if firstTab == "1" {
                    firstTabType = GameType.Cricket.rawValue
                }
                else if firstTab == "2" {
                    firstTabType = GameType.Kabaddi.rawValue
                }
                else if firstTab == "3" {
                    firstTabType = GameType.Football.rawValue
                }
                else if firstTab == "4" {
                    firstTabType = GameType.Quiz.rawValue
                }
                else if firstTab == "5" {
                    firstTabType = GameType.Basketball.rawValue
                }
                else if firstTab == "6" {
                    firstTabType = GameType.Baseball.rawValue
                }

                return (firstTabType, GameType.None.rawValue, GameType.None.rawValue, GameType.None.rawValue, GameType.None.rawValue, GameType.None.rawValue)
            }
            else{
                return (GameType.Cricket.rawValue, GameType.Kabaddi.rawValue, GameType.Football.rawValue, GameType.None.rawValue, GameType.None.rawValue, GameType.None.rawValue)
            }
        }
        
        return (GameType.Cricket.rawValue, GameType.Kabaddi.rawValue, GameType.Football.rawValue, GameType.None.rawValue, GameType.None.rawValue, GameType.None.rawValue)
    }
    
    
    class func getFantasyLandingOrder(fantasyType: String) -> (Int, Int, Int, Int, Int) {
        let landigOrderArray = fantasyType.components(separatedBy: ",")

        if landigOrderArray.count >= 5 {
            let firstTab = landigOrderArray[0]
            let secondTab = landigOrderArray[1]
            let thirdTab = landigOrderArray[2]
            let fourthTab = landigOrderArray[3]
            let fifthTab = landigOrderArray[4]

            var firstTabType = FantasyType.None.rawValue
            var secondTabType = FantasyType.None.rawValue
            var thirdTabType = FantasyType.None.rawValue
            var fourthTabType = FantasyType.None.rawValue
            var fifthTabType = FantasyType.None.rawValue

            if firstTab == "1" {
                firstTabType = FantasyType.Classic.rawValue
            }
            else if firstTab == "2" {
                firstTabType = FantasyType.Batting.rawValue
            }
            else if firstTab == "3" {
                firstTabType = FantasyType.Bowling.rawValue
            }
            else if firstTab == "4" {
                firstTabType = FantasyType.Reverse.rawValue
            }
            else if firstTab == "5" {
                firstTabType = FantasyType.Wizard.rawValue
            }
           
            if secondTab == "1" {
                secondTabType = FantasyType.Classic.rawValue
            }
            else if secondTab == "2" {
                secondTabType = FantasyType.Batting.rawValue
            }
            else if secondTab == "3" {
                secondTabType = FantasyType.Bowling.rawValue
            }
            else if secondTab == "4" {
                secondTabType = FantasyType.Reverse.rawValue
            }
            else if secondTab == "5" {
                secondTabType = FantasyType.Wizard.rawValue
            }

            if thirdTab == "1" {
                thirdTabType = FantasyType.Classic.rawValue
            }
            else if thirdTab == "2" {
                thirdTabType = FantasyType.Batting.rawValue
            }
            else if thirdTab == "3" {
                thirdTabType = FantasyType.Bowling.rawValue
            }
            else if thirdTab == "4" {
                thirdTabType = FantasyType.Reverse.rawValue
            }
            else if thirdTab == "5" {
                thirdTabType = FantasyType.Wizard.rawValue
            }
            
            if fourthTab == "1" {
                fourthTabType = FantasyType.Classic.rawValue
            }
            else if fourthTab == "2" {
                fourthTabType = FantasyType.Batting.rawValue
            }
            else if fourthTab == "3" {
                fourthTabType = FantasyType.Bowling.rawValue
            }
            else if fourthTab == "4" {
                fourthTabType = FantasyType.Reverse.rawValue
            }
            else if fourthTab == "5" {
                fourthTabType = FantasyType.Wizard.rawValue
            }
            
            if fifthTab == "1" {
                fifthTabType = FantasyType.Classic.rawValue
            }
            else if fifthTab == "2" {
                fifthTabType = FantasyType.Batting.rawValue
            }
            else if fifthTab == "3" {
                fifthTabType = FantasyType.Bowling.rawValue
            }
            else if fifthTab == "4" {
                fifthTabType = FantasyType.Reverse.rawValue
            }
            else if fifthTab == "5" {
                fifthTabType = FantasyType.Wizard.rawValue
            }

            return (firstTabType, secondTabType, thirdTabType, fourthTabType, fifthTabType)
        }
        else if landigOrderArray.count == 4 {
            let firstTab = landigOrderArray[0]
            let secondTab = landigOrderArray[1]
            let thirdTab = landigOrderArray[2]
            let fourthTab = landigOrderArray[3]

            var firstTabType = FantasyType.None.rawValue
            var secondTabType = FantasyType.None.rawValue
            var thirdTabType = FantasyType.None.rawValue
            var fourthTabType = FantasyType.None.rawValue

            if firstTab == "1" {
                firstTabType = FantasyType.Classic.rawValue
            }
            else if firstTab == "2" {
                firstTabType = FantasyType.Batting.rawValue
            }
            else if firstTab == "3" {
                firstTabType = FantasyType.Bowling.rawValue
            }
            else if firstTab == "4" {
                firstTabType = FantasyType.Reverse.rawValue
            }
            else if firstTab == "5" {
                firstTabType = FantasyType.Wizard.rawValue
            }

            if secondTab == "1" {
                secondTabType = FantasyType.Classic.rawValue
            }
            else if secondTab == "2" {
                secondTabType = FantasyType.Batting.rawValue
            }
            else if secondTab == "3" {
                secondTabType = FantasyType.Bowling.rawValue
            }
            else if secondTab == "4" {
                secondTabType = FantasyType.Reverse.rawValue
            }
            else if secondTab == "5" {
                secondTabType = FantasyType.Wizard.rawValue
            }

            if thirdTab == "1" {
                thirdTabType = FantasyType.Classic.rawValue
            }
            else if thirdTab == "2" {
                thirdTabType = FantasyType.Batting.rawValue
            }
            else if thirdTab == "3" {
                thirdTabType = FantasyType.Bowling.rawValue
            }
            else if thirdTab == "4" {
                thirdTabType = FantasyType.Reverse.rawValue
            }
            else if thirdTab == "5" {
                thirdTabType = FantasyType.Wizard.rawValue
            }
            
            if fourthTab == "1" {
                fourthTabType = FantasyType.Classic.rawValue
            }
            else if fourthTab == "2" {
                fourthTabType = FantasyType.Batting.rawValue
            }
            else if fourthTab == "3" {
                fourthTabType = FantasyType.Bowling.rawValue
            }
            else if fourthTab == "4" {
                fourthTabType = FantasyType.Reverse.rawValue
            }
            else if fourthTab == "5" {
                fourthTabType = FantasyType.Wizard.rawValue
            }
   
            return (firstTabType, secondTabType, thirdTabType, fourthTabType, FantasyType.None.rawValue)
        }
        else if landigOrderArray.count >= 3 {
         let firstTab = landigOrderArray[0]
         let secondTab = landigOrderArray[1]
         let thirdTab = landigOrderArray[2]

         var firstTabType = FantasyType.None.rawValue
         var secondTabType = FantasyType.None.rawValue
         var thirdTabType = FantasyType.None.rawValue

         if firstTab == "1" {
             firstTabType = FantasyType.Classic.rawValue
         }
         else if firstTab == "2" {
             firstTabType = FantasyType.Batting.rawValue
         }
         else if firstTab == "3" {
             firstTabType = FantasyType.Bowling.rawValue
         }
         else if firstTab == "4" {
             firstTabType = FantasyType.Reverse.rawValue
         }
         else if firstTab == "5" {
             firstTabType = FantasyType.Wizard.rawValue
         }

         if secondTab == "1" {
             secondTabType = FantasyType.Classic.rawValue
         }
         else if secondTab == "2" {
             secondTabType = FantasyType.Batting.rawValue
         }
         else if secondTab == "3" {
             secondTabType = FantasyType.Bowling.rawValue
         }
         else if secondTab == "4" {
             secondTabType = FantasyType.Reverse.rawValue
         }
         else if secondTab == "5" {
             secondTabType = FantasyType.Wizard.rawValue
         }

         if thirdTab == "1" {
             thirdTabType = FantasyType.Classic.rawValue
         }
         else if thirdTab == "2" {
             thirdTabType = FantasyType.Batting.rawValue
         }
         else if thirdTab == "3" {
             thirdTabType = FantasyType.Bowling.rawValue
         }
         else if thirdTab == "4" {
             thirdTabType = FantasyType.Reverse.rawValue
         }
         else if thirdTab == "5" {
             thirdTabType = FantasyType.Wizard.rawValue
         }

         return (firstTabType, secondTabType, thirdTabType, FantasyType.None.rawValue, FantasyType.None.rawValue)
        }
        else if landigOrderArray.count >= 2 {
             let firstTab = landigOrderArray[0]
             let secondTab = landigOrderArray[1]

             var firstTabType = FantasyType.None.rawValue
             var secondTabType = FantasyType.None.rawValue

             if firstTab == "1" {
                 firstTabType = FantasyType.Classic.rawValue
             }
             else if firstTab == "2" {
                 firstTabType = FantasyType.Batting.rawValue
             }
             else if firstTab == "3" {
                 firstTabType = FantasyType.Bowling.rawValue
             }
             else if firstTab == "4" {
                 firstTabType = FantasyType.Reverse.rawValue
             }
             else if firstTab == "5" {
                 firstTabType = FantasyType.Wizard.rawValue
             }

             if secondTab == "1" {
                 secondTabType = FantasyType.Classic.rawValue
             }
             else if secondTab == "2" {
                 secondTabType = FantasyType.Batting.rawValue
             }
             else if secondTab == "3" {
                 secondTabType = FantasyType.Bowling.rawValue
             }
             else if secondTab == "4" {
                 secondTabType = FantasyType.Reverse.rawValue
             }
             else if secondTab == "5" {
                 secondTabType = FantasyType.Wizard.rawValue
             }

             return (firstTabType, secondTabType, FantasyType.None.rawValue, FantasyType.None.rawValue, FantasyType.None.rawValue)
        }
        else if landigOrderArray.count >= 1 {

            let firstTab = landigOrderArray[0]
             var firstTabType = FantasyType.None.rawValue

             if firstTab == "1" {
                 firstTabType = FantasyType.Classic.rawValue
             }
             else if firstTab == "2" {
                 firstTabType = FantasyType.Batting.rawValue
             }
             else if firstTab == "3" {
                 firstTabType = FantasyType.Bowling.rawValue
             }
             else if firstTab == "4" {
                 firstTabType = FantasyType.Reverse.rawValue
             }
             else if firstTab == "5" {
                 firstTabType = FantasyType.Wizard.rawValue
             }

             return (firstTabType, FantasyType.None.rawValue, FantasyType.None.rawValue, FantasyType.None.rawValue, FantasyType.None.rawValue)
        }
        else{
            return (FantasyType.Classic.rawValue, FantasyType.None.rawValue, FantasyType.None.rawValue, FantasyType.None.rawValue, FantasyType.None.rawValue)
        }
    }
    
    class func getTheSortedArray(dataArray: [String]) -> [String] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MM, yyyy"// yyyy-MM-dd"
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            let locatStr = lang[0]
            if locatStr == "hi"{
                dateFormatter.locale = Locale(identifier: locatStr)
            }
            else{
                dateFormatter.locale = Locale(identifier: "en_US")
            }
        }

        var convertedArray: [Date] = []
        for dat in dataArray {
            let date = dateFormatter.date(from: dat)
            if let date = date {
                convertedArray.append(date)
            }
        }
        let dateArray = convertedArray.sorted(by: { $0.compare($1) == .orderedDescending })
        
        var convertedStringArray: [String] = []
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            let locatStr = lang[0]
            if locatStr == "hi"{
                dateFormatter.locale = Locale(identifier: locatStr)
            }
            else{
                dateFormatter.locale = Locale(identifier: "en_US")
            }
        }

        
        for dat in dateArray {
            let date = dateFormatter.string(from: dat)
            convertedStringArray.append(date)
        }
        
        
        return convertedStringArray
    }
    
    class func showAlertView(message: String, isErrorMessage: Bool)  {
        AppHelper.sharedInstance.messageView?.removeFromSuperview()
        AppHelper.sharedInstance.messageView = MessageDisplayView(frame: CGRect(x: 0, y: -80, width: UIScreen.main.bounds.width, height: 60))
        
        if isErrorMessage {
            AppHelper.sharedInstance.messageView?.messageDisplay(message: message, color: UIColor(red: 251.0/255, green: 151.0/255, blue: 138.0/255, alpha: 1))
        }
        else{
            AppHelper.sharedInstance.messageView?.messageDisplay(message: message, color: UIColor(red: 57.0/255, green: 217.0/255, blue: 172.0/255, alpha: 1))
        }
        
        APPDELEGATE.window?.addSubview(AppHelper.sharedInstance.messageView!)
        AppHelper.sharedInstance.messageView?.showAnimation()
        
    }
    
    class func isInterNetConnectionAvailable() -> Bool {
        
        let reachability = Reachability()!.connection
        
        if reachability == .cellular {
            print("Reachable via Mobile Data")
            return true
        }
        if reachability == .wifi {
            print("Reachable via WiFi")
            return true
        }else {
            print("Reachable via None")
            AppHelper.showAlertView(message: "kNoInternetConnection".localized(), isErrorMessage: true)
            return false
        }
    }
    
    class func showPrivacyPolicyView() -> PrivacyPolicyView {
        let privacyView = PrivacyPolicyView.init(frame: (APPDELEGATE.window?.frame)!)
        APPDELEGATE.window?.addSubview(privacyView)
        privacyView.alpha = 0
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseOut, animations: {
            privacyView.alpha = 1
        }) { _ in
            
        }

        return privacyView
    }
    
    func displaySpinner() {
        if (appLoaderView != nil) {
            removeSpinner()
        }
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            guard let weakObj = weakSelf else { return }
            weakObj.appLoaderView = AppLoaderView(frame: UIScreen.main.bounds)
            APPDELEGATE.window?.addSubview(weakObj.appLoaderView!)
            weakSelf?.appLoaderView?.showAnimationOnLoader()
        }
    }
    
    func removeSpinner() {
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.appLoaderView?.removeFromSuperview()
            weakSelf?.appLoaderView = nil;
        }
    }
    
    class func validateClassicLeague(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
        if isRecordRemove{
            return nil
        }
        else if selectedPlayerList.count > 11 {
            return "max11Player".localized();
        }
        else{
            
            let lastPlayerDetails = selectedPlayerList.last
            let team1PlayesList = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamShortName == teamName
            }

            if team1PlayesList.count > 7{
                return "max7PlayerFromTeam".localized();
            }
            else if (selectedPlayerList.count - team1PlayesList.count)  > 7{
                return "max7PlayerFromTeam".localized();
            }
            else{
                
                let totalBatsmanArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
                }
                
                let totalBowlerArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
                }
                
                let totalAllrounderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
                }
                
                let totalWicketKeeperArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
                }
                
                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    creditSum + Float(nextPlayerDetails.credits)!
                })
                
                if (totalPoints > maxCredit) {
                    let remaingPoints = totalPoints - Float(lastPlayerDetails!.credits)!
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                        }
                    }

                    return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
                }
                else if totalBatsmanArray.count > 6{
                    return "max6Batsman".localized();
                }
                else if totalBowlerArray.count > 6{
                    return "max6Bowler".localized();
                }
                else if totalAllrounderArray.count > 4{
                    return "max4Allrounder".localized();
                }
                else if totalWicketKeeperArray.count > 4{
                    return "max4WicketKeeper".localized();
                }
                    
                let remainingWicketKeeper = 1 - totalWicketKeeperArray.count
                let remainingBatsmantKeeper = 3 - totalBatsmanArray.count
                let remainingAllRounderKeeper = 1 - totalAllrounderArray.count
                let remainingBowlerKeeper = 3 - totalBowlerArray.count

                var temp = 0
                
                if remainingWicketKeeper > 0 {
                    temp = temp + remainingWicketKeeper
                }
                
                if remainingBatsmantKeeper > 0 {
                    temp = temp + remainingBatsmantKeeper
                }

                if remainingAllRounderKeeper > 0 {
                    temp = temp + remainingAllRounderKeeper
                }

                if remainingBowlerKeeper > 0 {
                    temp = temp + remainingBowlerKeeper
                }
                
                if selectedPlayerList.count >= 6 {
                    if temp > 11 - selectedPlayerList.count{
                        if remainingAllRounderKeeper > 0 {
                            return "pickOneAllRounder".localized();
                        }
                        else if remainingBowlerKeeper > 0 {
                            return "min3Bowler".localized();
                        }
                        else if remainingWicketKeeper > 0 {
                            return "pickOneKeeper".localized();
                        }
                        else if remainingBatsmantKeeper > 0 {
                            return "min3Batsman".localized();
                        }
                    }
                }
            }
        }
        
        return nil
    }
    
    
    class func validateClassicLeagueForTourney(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
        if isRecordRemove{
            return nil
        }
        else if selectedPlayerList.count > 11 {
            return "max11Player".localized();
        }
        else{
            
            let lastPlayerDetails = selectedPlayerList.last
            let team1PlayesList = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamShortName == teamName
            }

            if team1PlayesList.count > 7{
                return "max7PlayerFromTeam".localized();
            }
            else{
                
                let totalBatsmanArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Batsman.rawValue
                }
                
                let totalBowlerArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Bowler.rawValue
                }
                
                let totalAllrounderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
                }
                
                let totalWicketKeeperArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.WicketKeeper.rawValue
                }
                
                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    creditSum + Float(nextPlayerDetails.credits)!
                })
                
                if (totalPoints > maxCredit) {
                    let remaingPoints = totalPoints - Float(lastPlayerDetails!.credits)!
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                        }
                    }

                    return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
                }
                else if totalBatsmanArray.count > 6{
                    return "max6Batsman".localized();
                }
                else if totalBowlerArray.count > 6{
                    return "max6Bowler".localized();
                }
                else if totalAllrounderArray.count > 4{
                    return "max4Allrounder".localized();
                }
                else if totalWicketKeeperArray.count > 4{
                    return "max4WicketKeeper".localized();
                }
                    
                let remainingWicketKeeper = 1 - totalWicketKeeperArray.count
                let remainingBatsmantKeeper = 3 - totalBatsmanArray.count
                let remainingAllRounderKeeper = 1 - totalAllrounderArray.count
                let remainingBowlerKeeper = 3 - totalBowlerArray.count

                var temp = 0
                
                if remainingWicketKeeper > 0 {
                    temp = temp + remainingWicketKeeper
                }
                
                if remainingBatsmantKeeper > 0 {
                    temp = temp + remainingBatsmantKeeper
                }

                if remainingAllRounderKeeper > 0 {
                    temp = temp + remainingAllRounderKeeper
                }

                if remainingBowlerKeeper > 0 {
                    temp = temp + remainingBowlerKeeper
                }
                
                if selectedPlayerList.count >= 6 {
                    if temp > 11 - selectedPlayerList.count{
                        if remainingAllRounderKeeper > 0 {
                            return "pickOneAllRounder".localized();
                        }
                        else if remainingBowlerKeeper > 0 {
                            return "min3Bowler".localized();
                        }
                        else if remainingWicketKeeper > 0 {
                            return "pickOneKeeper".localized();
                        }
                        else if remainingBatsmantKeeper > 0 {
                            return "min3Batsman".localized();
                        }
                    }
                }
            }
        }
        
        return nil
    }
    
  class func validateBattingLeagueForTourney(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
      
      if selectedPlayerList.count > 5 {
          return "max5Batsman".localized();
      }
      else{
          
          let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
              playerDetails.teamShortName == teamName
          }
          
          if totalPlayerFromTeamArray.count > 3{
              return "max3PlayerFromTeam".localized();
          }
          else{
              
              let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                  
                  creditSum + Float(nextPlayerDetails.credits)!
              })
              
              let playerDetails = selectedPlayerList.last
              
              if (totalPoints > maxCredit) {
                  
                  let remaingPoints = totalPoints - Float(playerDetails!.credits)!
                  if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                      if "hi" == lang[0]{
                          return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                      }
                  }

                  return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
              }
                  else if playerDetails?.playerPlayingRole == PlayerType.Bowler.rawValue{
                      return "bowlerSelectionPopUp".localized()
              }
          }
      }
      
      return nil
  }
    
    class func validateBattingLeague(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
        
        if selectedPlayerList.count > 5 {
            return "max5Batsman".localized();
        }
        else{
            
            let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamShortName == teamName
            }
            
            if totalPlayerFromTeamArray.count > 3{
                return "max3PlayerFromTeam".localized();
            }
            else if (selectedPlayerList.count - totalPlayerFromTeamArray.count)  > 3{
                return "max3PlayerFromTeam".localized();
            }
            else{
                
                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    
                    creditSum + Float(nextPlayerDetails.credits)!
                })
                
                let playerDetails = selectedPlayerList.last
                
                if (totalPoints > maxCredit) {
                    
                    let remaingPoints = totalPoints - Float(playerDetails!.credits)!
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                        }
                    }

                    return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
                }
                    else if playerDetails?.playerPlayingRole == PlayerType.Bowler.rawValue{
                        return "bowlerSelectionPopUp".localized()
                }
            }
        }
        
        return nil
    }
    
    class func validateBlowlingLeagueForTourney(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
        
        if selectedPlayerList.count > 5 {
            return "max5BowlerFanticy".localized();
        }
        else{
            
            let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamShortName == teamName
            }
            
            if totalPlayerFromTeamArray.count > 3{
                return "max3PlayerFromTeam".localized();
            }
            else{
                
                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    creditSum + Float(nextPlayerDetails.credits)!
                })
                
                let playerDetails = selectedPlayerList.last
                
                if isRecordRemove{
                    return nil
                }
                else if playerDetails?.playerPlayingRole == PlayerType.WicketKeeper.rawValue{
                    return "keeperCannotSelect".localized()
                }
                else if (totalPoints > maxCredit) {
                    let remaingPoints = totalPoints - Float(playerDetails!.credits)!
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                        }
                    }

                    return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
                }
                else if playerDetails?.playerPlayingRole == PlayerType.Batsman.rawValue{
                    return "battingSelectionPopUp".localized()
                }

            }
        }
        
        return nil
    }
    
    class func validateBlowlingLeague(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
        
        if selectedPlayerList.count > 5 {
            return "max5BowlerFanticy".localized();
        }
        else{
            
            let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamShortName == teamName
            }
            
            if totalPlayerFromTeamArray.count > 3{
                return "max3PlayerFromTeam".localized();
            }
            else if (selectedPlayerList.count - totalPlayerFromTeamArray.count)  > 3{
                return "max3PlayerFromTeam".localized();
            }
            else{
                
                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    creditSum + Float(nextPlayerDetails.credits)!
                })
                
                let playerDetails = selectedPlayerList.last
                
                if isRecordRemove{
                    return nil
                }
                else if playerDetails?.playerPlayingRole == PlayerType.WicketKeeper.rawValue{
                    return "keeperCannotSelect".localized()
                }
                else if (totalPoints > maxCredit) {
                    let remaingPoints = totalPoints - Float(playerDetails!.credits)!
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                        }
                    }

                    return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
                }
                else if playerDetails?.playerPlayingRole == PlayerType.Batsman.rawValue{
                    return "battingSelectionPopUp".localized()
                }

            }
        }
        
        return nil
    }
    
    class func validateKabaddiLeague(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
        
        if selectedPlayerList.count > 7 {
            return "max3PlayerFromTeam".localized();
        }
        else{
            
            let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamShortName == teamName
            }
            
            if totalPlayerFromTeamArray.count > 5{
                return "maxTeamPlayerForKabaddi".localized();
            }
            else if (selectedPlayerList.count - totalPlayerFromTeamArray.count)  > 5{
                return "maxTeamPlayerForKabaddi".localized();
            }
            else{
                
                let totalDefenderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
                }

                let totalAllrounderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.AllRounder.rawValue
                }
                
                let totalRaiderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Raider.rawValue
                }
                
                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    creditSum + Float(nextPlayerDetails.credits)!
                })
                
                if (totalPoints > maxCredit) {
                    let playerDetails = selectedPlayerList.last
                    let remaingPoints = totalPoints - Float(playerDetails!.credits)!
                    
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                        }
                    }

                    return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
                }
                else if totalDefenderArray.count > 4{
                    return "maxLimitDefenders".localized();
                }
                else if totalAllrounderArray.count > 2{
                    return "max2AllRoundersKabaddi".localized();
                }
                else if totalRaiderArray.count > 3{
                    return "maxLimitRaider".localized();
                }
                else if (selectedPlayerList.count == 7) && (totalAllrounderArray.count == 0){
                    return "picOneAllRoundersKabaddi".localized();
                }
                
            }
        }
        
        return nil
    }
    
    class func validateBaseballLeague(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
        
        if selectedPlayerList.count > 9 {
            return "max9BaseballPlayer".localized();
        }
        else{
            
            let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamShortName == teamName
            }
            
            if totalPlayerFromTeamArray.count > 6{
                return "max6PlayerFromTeam".localized();
            }
            else if (selectedPlayerList.count - totalPlayerFromTeamArray.count)  > 6{
                return "max6PlayerFromTeam".localized();
            }
            else{
                
                let totalOutfieldersArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Outfielders.rawValue
                }

                let totalInfielderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Infielder.rawValue
                }
                
                let totalPitcherArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Pitcher.rawValue
                }

                let totalCatcherArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Catcher.rawValue
                }

                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    creditSum + Float(nextPlayerDetails.credits)!
                })
                                
                if (totalPoints > maxCredit) {
                    let playerDetails = selectedPlayerList.last
                    let remaingPoints = totalPoints - Float(playerDetails!.credits)!
                    
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                        }
                    }

                    return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
                }
                else if totalOutfieldersArray.count > 5{
                    return "MaxOutFielder".localized();
                }
                else if totalInfielderArray.count > 5{
                    return "MaxInFielder".localized();
                }
                else if totalPitcherArray.count > 1{
                    return "MaxPitcher".localized();
                }
                else if totalCatcherArray.count > 1{
                    return "MaxCatcher".localized();
                }
                
                let remainingOutfielders = 2 - totalOutfieldersArray.count
                let remainingInfielder = 2 - totalInfielderArray.count
                let remainingPitcher = 1 - totalPitcherArray.count
                let remainingCatcher = 1 - totalCatcherArray.count

                var temp = 0
                
                if remainingOutfielders > 0 {
                    temp = temp + remainingOutfielders
                }
                
                if remainingInfielder > 0 {
                    temp = temp + remainingInfielder
                }

                if remainingPitcher > 0 {
                    temp = temp + remainingPitcher
                }

                if remainingCatcher > 0 {
                    temp = temp + remainingCatcher
                }

                if selectedPlayerList.count >= 6 {
                    if temp > 9 - selectedPlayerList.count{
                        if remainingOutfielders > 0 {
                            return "MinOutFielder".localized();
                        }
                        else if remainingInfielder > 0 {
                            return "MinInFielder".localized();
                        }
                        else if remainingPitcher > 0 {
                            return "MinPitcher".localized();
                        }
                        else if remainingCatcher > 0 {
                            return "MinCatcher".localized();
                        }
                    }
                }
                
                
//                else if (selectedPlayerList.count >= 8) && (totalPitcherArray.count == 0){
//                    if lastPlayer!.seasonalRole != PlayerType.Pitcher.rawValue  {
//                        return "MinPitcher".localized();
//                    }
//                }
//                else if (selectedPlayerList.count >= 8) && (totalCatcherArray.count == 0){
//                    if lastPlayer!.seasonalRole != PlayerType.Catcher.rawValue  {
//                        return "MinCatcher".localized();
//                    }
//                }
            }
        }
        
        return nil
    }
    
    class func validateBasketballLeague(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
        
        if selectedPlayerList.count > 8 {
            return "max8BasketballPlayer".localized();
        }
        else{
            
            let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamShortName == teamName
            }
            
            if totalPlayerFromTeamArray.count > 5{
                return "max5PlayerFromTeam".localized();
            }
            else if (selectedPlayerList.count - totalPlayerFromTeamArray.count)  > 5{
                return "max5PlayerFromTeam".localized();
            }
            else{
                
                let totalPointGuardArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.PointGuard.rawValue
                }

                let totalShootingGuardArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.ShootingGuard.rawValue
                }
                
                let totalSmallForwardArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.SmallForward.rawValue
                }

                let totalPowerForwardArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.PowerForward.rawValue
                }
                
                let totalCenterArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Center.rawValue
                }
                
                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    creditSum + Float(nextPlayerDetails.credits)!
                })
                                
                if (totalPoints > maxCredit) {
                    let playerDetails = selectedPlayerList.last
                    let remaingPoints = totalPoints - Float(playerDetails!.credits)!
                    
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                        }
                    }

                    return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
                }
                else if totalPointGuardArray.count > 4{
                    return "MaxPointGuard".localized();
                }
                else if totalShootingGuardArray.count > 4{
                    return "MaxShootingGuard".localized();
                }
                else if totalSmallForwardArray.count > 4{
                    return "MaxSamllForward".localized();
                }
                else if totalPowerForwardArray.count > 4{
                    return "MaxPowerForward".localized();
                }
                else if totalCenterArray.count > 4{
                    return "MaxCenter".localized();
                }
                                    
                let remainingPointGuard = 1 - totalPointGuardArray.count
                let remainingShootingGuard = 1 - totalShootingGuardArray.count
                let remainingSmallForward = 1 - totalSmallForwardArray.count
                let remainingPowerForward = 1 - totalPowerForwardArray.count
                let remainingCenter = 1 - totalCenterArray.count

                var temp = 0
                
                if remainingPointGuard > 0 {
                    temp = temp + remainingPointGuard
                }
                
                if remainingShootingGuard > 0 {
                    temp = temp + remainingShootingGuard
                }

                if remainingSmallForward > 0 {
                    temp = temp + remainingSmallForward
                }

                if remainingPowerForward > 0 {
                    temp = temp + remainingPowerForward
                }
                
                if remainingCenter > 0 {
                    temp = temp + remainingCenter
                }

                if selectedPlayerList.count >= 6 {
                    if temp > 8 - selectedPlayerList.count{
                        if remainingPointGuard > 0 {
                            return "MinPointGuard".localized();
                        }
                        else if remainingShootingGuard > 0 {
                            return "MinShootingGuard".localized();
                        }
                        else if remainingSmallForward > 0 {
                            return "MinSamllForward".localized();
                        }
                        else if remainingPowerForward > 0 {
                            return "MinPowerForward".localized();
                        }
                        else if remainingCenter > 0 {
                            return "MinCenter".localized();
                        }
                    }
                }
            }
        }
        
        return nil
    }
    
    
    class func validateFootballClassicLeague(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {
        
        if selectedPlayerList.count > 11 {
            return "max11Player".localized();
        }
        else{
            
            let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamShortName == teamName
            }
            
            if totalPlayerFromTeamArray.count > 7{
                return "max7PlayerFromTeam".localized();
            }
            else if (selectedPlayerList.count - totalPlayerFromTeamArray.count)  > 7{
                return "max7PlayerFromTeam".localized();
            }
            else{
                
                let totalDefenderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Defender.rawValue
                }
                
                let totalMidfielderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.MidFielder.rawValue
                }
                
                let totalForwardArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.Sticker.rawValue
                }
                
                let totalGoalKeeperArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerPlayingRole == PlayerType.GoalKeeper.rawValue
                }
                
                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    creditSum + Float(nextPlayerDetails.credits)!
                })
                

                if (selectedPlayerList.count == 11) && (totalGoalKeeperArray.count == 0){
                    return "pickOneGoalKeeper".localized();
                }
                else if (selectedPlayerList.count == 11) && (totalForwardArray.count == 0){
                    return "pickOneStriker".localized();
                }
                else if (selectedPlayerList.count == 11) && (totalMidfielderArray.count == 0){
                    return "pickOneMidFielder".localized();
                }
                else if (selectedPlayerList.count == 11) && (totalDefenderArray.count == 0){
                    return "pickOneDefender".localized();
                }
                else if totalDefenderArray.count > 5{
                    return "max5Defender".localized();
                }
                else if totalMidfielderArray.count > 5{
                    return "max5Midfielder".localized();
                }
                else if totalForwardArray.count > 3{
                    return "max3Forwars".localized();
                }
                else if totalGoalKeeperArray.count > 1{
                    return "maxGoalKeeper".localized();
                }
                else if (selectedPlayerList.count == 11) && (totalGoalKeeperArray.count == 0){
                    return "pickOneGoalKeeper".localized();
                }
                else if (selectedPlayerList.count == 11) && (totalForwardArray.count == 0){
                    return "pickOneStriker".localized();
                }
                else if (selectedPlayerList.count == 11) && (totalDefenderArray.count < 3){
                    return "pickOneDefender".localized();
                }
                else if (selectedPlayerList.count == 11) && (totalMidfielderArray.count < 3){
                    return "pickOneMidFielder".localized();
                }
                else if (totalPoints > maxCredit) {
                    let playerDetails = selectedPlayerList.last
                    let remaingPoints = totalPoints - Float(playerDetails!.credits)!
                    
                    if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                        if "hi" == lang[0]{
                            return "आपने अधिकतम लाभ उठाया है!\nमाफ़ करना आपके पास केवल " + String(maxCredit - remaingPoints) + " क्रेडिट बचे हैं";
                        }
                    }

                    return "You have maxed out!\nSorry, you have only got " + String(maxCredit - remaingPoints) + " credits left.";
                }
                else if (totalDefenderArray.count == 5) && (totalMidfielderArray.count > 4){
                    return "pickOneGoalKeeper".localized()
                }
                else if (totalMidfielderArray.count == 5) && (totalDefenderArray.count > 4){
                    return "pickOneGoalKeeper".localized()
                }
                else if (totalMidfielderArray.count == 5) && (totalForwardArray.count > 2){
                    return "SelectMin3Defenders".localized()
                }
                else if (totalDefenderArray.count == 5) && (totalForwardArray.count > 2){
                    return "SelectMin3MidFielder".localized()
                }
            }
        }
        
        return nil
    }
    
    class func getRequiredFormattedDate(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "dd MMMM, yyyy"
            // Apply date format
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }

            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr

        }
        return ""
    }
    
    class func convertTimestanceToDate(timestance: Double) -> Date{
        let date = Date(timeIntervalSince1970: timestance)
        return date
    }
    
    class func convertDateForPlayerPointsView(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "dd MMM YYYY"
            
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }

            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr
            
        }
        return ""
    }

    
    class func getTransactionFormattedDate(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "dd MMMM, yyyy"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }
            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr
            
        }
        return ""
    }
    
    class func getDepositFormattedDate(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "dd MMM, yyyy"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }
            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr
            
        }
        return ""
    }
    
    class func getTransactionFormattedTime(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "h:mm a"
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"

            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr
            
        }
        return ""
    }

    
    class func getTicketFormattedDate(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "dd MMM, yyyy"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }
            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr
            
        }
        return ""
    }

    class func dictionary(toString dict: Any?) -> String? {
        if let aDict = dict {
            if dict == nil || !JSONSerialization.isValidJSONObject(aDict) {
                return "{}"
            }
        }
        var data: String? = nil
        if let aDict = dict, let anError = try?
            JSONSerialization.data(withJSONObject: aDict, options: []) {
            data = String(data: anError, encoding: .utf8)
        }
        return data
    }

    class func getPaymentParams(details:  JustPayOrderDetails) -> NSMutableDictionary {
        let paymentParams = NSMutableDictionary()
        paymentParams["merchant_id"] = details.merchantId
        paymentParams["client_id"] = JusPayClientID
        paymentParams["customer_id"] = details.customerId
        paymentParams["customer_email"] = details.customerEmail
        paymentParams["customer_phone_number"] = details.customerPhone
//        paymentParams["environment"] = "prod" | "sandbox"
        paymentParams["environment"] = "prod"
        paymentParams["session_token"] = details.authToken
        paymentParams["service"] = "in.juspay.ec";
        paymentParams["end_urls"] = ["http://tjp.Letspick.com/juspay/response"]
        paymentParams["order_id"] = details.orderID
        paymentParams["amount"] = details.amount
        
        return paymentParams
    }

    
    class func getNotificationFormattedTime(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "dd MMM, yyyy hh:mm a"
            dateFormatter.timeZone = NSTimeZone.local

            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }

            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr
            
        }
        return ""
    }
    
    class func getTicketFormattedTime(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "hh:mm a"
//            dateFormatter.timeZone = NSTimeZone.local

            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }

            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            
            return selectedDateStr
            
        }
        return ""
    }
    
    class func getNotificationFormattedDate(dateString: String) -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dateFormatter.date(from: dateString){
            // Set date format
            dateFormatter.dateFormat = "dd MMM, yyyy"
            if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
                let locatStr = lang[0]
                if locatStr == "hi"{
                    dateFormatter.locale = Locale(identifier: locatStr)
                }
                else{
                    dateFormatter.locale = Locale(identifier: "en_US")
                }
            }
            // Apply date format
            let selectedDateStr = dateFormatter.string(from: date)
            return selectedDateStr
        }
        return ""
    }
    
    
    class func getUserRegistrationDate(dateString: String) -> Date?{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        return date
    }
    
    class func getFormattedCurrentDate() -> String{
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy"
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            let locatStr = lang[0]
            if locatStr == "hi"{
                dateFormatter.locale = Locale(identifier: locatStr)
            }
            else{
                dateFormatter.locale = Locale(identifier: "en_US")
            }
        }
        let selectedDateStr = dateFormatter.string(from: Date())
        return selectedDateStr
    }

}

extension String {
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [kCTFontAttributeName as NSAttributedStringKey: font], context: nil)
        return boundingBox.height
    }
}


extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}


extension String {
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }

    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func localized() ->String {
        if let lang = UserDefaults.standard.value(forKey: kAppLanguague) as? Array<String>{
            let path = Bundle.main.path(forResource: lang[0], ofType: "lproj")
            if path == nil{
                let path = Bundle.main.path(forResource: "en", ofType: "lproj")
                let bundle = Bundle(path: path!)
                return NSLocalizedString(self, tableName: "Ballebaazi", bundle: bundle!, value: "", comment: "")
            }
            let bundle = Bundle(path: path!)
            return NSLocalizedString(self, tableName: "Ballebaazi", bundle: bundle!, value: "", comment: "")

        }
        else{
            let path = Bundle.main.path(forResource: "en", ofType: "lproj")
            let bundle = Bundle(path: path!)
            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        }
    }
}


extension UILabel{

    func showHTML(htmlString: String, isCenterAligment: Bool) {

        let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center

        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)

        if #available(iOS 11.0, *) {
            let newFont = UIFontMetrics.default.scaledFont(for:  UIFont(name: "OpenSans-Semibold", size: 16)!)
            
            let mattrStr = NSMutableAttributedString(attributedString: attrStr!)
            mattrStr.beginEditing()
            mattrStr.enumerateAttribute(.font, in: NSRange(location: 0, length: mattrStr.length), options: .longestEffectiveRangeNotRequired) { (value, range, _) in
                if let oFont = value as? UIFont, let newFontDescriptor = oFont.fontDescriptor.withFamily(newFont.familyName).withSymbolicTraits(oFont.fontDescriptor.symbolicTraits) {
                    let nFont = UIFont(descriptor: newFontDescriptor, size: 16.0)
                    mattrStr.removeAttribute(.font, range: range)
                    mattrStr.addAttribute(.font, value: nFont, range: range)
                    if isCenterAligment{
                        mattrStr.addAttribute(.paragraphStyle, value: style, range: range)
                        mattrStr.addAttribute(.foregroundColor, value: UIColor(red: 127.0/255, green: 127.0/255, blue: 127.0/255, alpha: 1), range: range)
                    }
                }
            }
            
            mattrStr.endEditing()
            self.attributedText = mattrStr
            
        } else {
            self.attributedText = attrStr
        }
    }
}
class Colors {
    var gl:CAGradientLayer!

    init(colorLeft:UIColor,colorRight:UIColor) {
       // let colorTop = UIColor(red: 192.0 / 255.0, green: 38.0 / 255.0, blue: 42.0 / 255.0, alpha: 1.0).cgColor
        //let colorBottom = UIColor(red: 35.0 / 255.0, green: 2.0 / 255.0, blue: 2.0 / 255.0, alpha: 1.0).cgColor

        self.gl = CAGradientLayer()
        self.gl.transform = CATransform3DMakeRotation(CGFloat.pi / 2, 0, 0, 1)
        
        self.gl.colors = [colorRight.cgColor, colorLeft.cgColor]
        self.gl.locations = [0.0, 1.0]
    }
}
