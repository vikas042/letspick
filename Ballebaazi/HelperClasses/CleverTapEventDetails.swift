//
//  CleverTapEventDetails.swift
//  Letspick
//
//  Created by Madstech on 26/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON
import CleverTapSDK

let PLATFORM_iPHONE = "iPhone";
let PLATFORM = "Platform";
let identity = "Identity";
let phone_number = "Phone";
let signup_channel = "Signup Channel";
let onBoarding_medium = "Onboarding Medium";
let sign_up = "Sign Up";
let mode = "Mode";
let amount = "Amount";
let promo_code = "Promo code";
let payment_method = "Payment Method";
let deposite_success = "Deposit Successful";
let user_properties = "User Properties";
let otp_requested_date = "OTP Requested Date";
let otp_requested = "OTP Requested"
let mobile_number = "Mobile Number"
let joined_league = "League Joined ";
let game_type = "Game Type";
let series_name = "Series Name";
let match_name = "Match Name";
let id = "ID";
let match_category = "Match Category";
let fantasy_type = "Fantasy Type";
let league_type = "League Type";
let buy_in = "Buy in";
let max_team = "Max teams";
let prize_pool = "Prize Pool";
let league_category = "League Category";
let entry_type = "Entry Type";
let bonus_allowed = "Bonus Allowed";
let money_applied = "Money Applied";
let joined_mode = "Joining Mode";
let sign_up_code = "Signup Code";
let verification = "Verification";
let registration_date = "Registration date";
let user_name = "User Name";
let custome_name = "Customer Name";
let dob = "DOB";
let device = "Device";
let pan_status = "Pan Status";
let banck_status = "Bank Status";
let unused_ballence = "Unused Balance";
let winning_ballence = "Winnings Balance";
let bonus_ballence = "Bonus Balance";
let tot_wallet_ballence = "Total Wallet Balance";
let batting_contest = "Total Batting Contest";
let bowling_contest = "Total Bowling Contest";
let classic_contest = "Total Classic Contest";
let football_contest = "Total Football Contest";
let kabaddi_contest = "Total Kabaddi Contest";
let basketball_contest = "Total Basketball Contest";
let baseball_contest = "Total Baseball Contest";

let name = "Name";
let email_address = "Email Address";
let referral_code_used = "Referral code used";
let last_login_date = "Last login date";
let login = "Login";
let user_id = "user ID";
let login_date = "Login Date";

class CleverTapEventDetails: NSObject {

    class func updateLoginEvent(params: [String: Any]){
        CleverTap.sharedInstance()?.onUserLogin(params)
    }
    
    class func updateProfile(updateProfile: [String: Any]){
        CleverTap.sharedInstance()?.profilePush(updateProfile)
    }
    
    class func sendEventToCleverTap(eventName: String ,params: [String: Any]){
        CleverTap.sharedInstance()?.recordEvent(eventName, withProps: params)
    }
    
}
