//
//  Constants.swift
//  Letspick
//
//  Created by Vikash Rajput on 5/28/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import Foundation
import UIKit


enum PlayerType: String {
    case Batsman = "batsman"
    case Bowler = "bowler"
    case WicketKeeper = "keeper"
    case AllRounder = "allrounder"
    case Raider = "raider"
    case Defender = "defender"
    case MidFielder = "midfielder"
    case GoalKeeper = "goalkeeper"
    case Sticker = "forward"
    case ShootingGuard = "shootingguard"
    case PointGuard = "pointguard"
    case SmallForward = "smallforward"
    case Center = "center"
    case PowerForward = "powerforward"
    case Outfielders = "outfielders"
    case Catcher = "catcher"
    case Infielder = "infielder"
    case Pitcher = "pitcher"    
}

//let MIXPANELTOKEN = "553af86c7b3f1c29e483c56bae933d3c" // TEST Live

let MIXPANELTOKEN = "fa89f3a9a0d8366a7dedcf4686e8e1eb" // BB Tech Live
//let MIXPANELTOKEN = "7a82ab44f9445171bca8edb9b16e9bb0" // Vikash Test Account

let selectedTabColor = UIColor(red: 0.0/255, green: 136.0/255, blue: 64.0/255, alpha: 1)

let UnselectedTabColor = UIColor(red: 96.0/255, green: 96.0/255, blue: 96.0/255, alpha: 1)

let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate

let userDetailsUrl = "userDetailsUrlForLocalDataBase"

//15df8065-b829-443a-bcf0-786dd776f889 // APXOR LIVE
//5913354a-3bf1-47c3-b599-803859025d06 // APXOR TEST

//http://lpcapi.letspick.io/myoauth
let BASEURL = "http://lpcapi.letspick.io/"
let BASEURLKABADDI = "http://13.35.207:8091/"
let BASEURLFOOTBALL = "http://lpfapi.letspick.io/"
let BASEURLBASKETBALL = "http://13.35.207:8096/"
let BASEURLBASEBALL = "http://13.35.207:8097/"
let POINTSYSTEMURL = "http://letspick.io/"






let kAppLanguague = "AppleLanguages"

let kToakenURL = BASEURL + "myoauth"

let kSignupURL = BASEURL + "register"                   

let kLoginURL = BASEURL + "login"

let kForgotPasswordURL = BASEURL + "forgotpassword"

let kVerifyPhone = BASEURL + "verify"

let kMatch = BASEURL + "match"

let kRewardProgram = BASEURL + "reward-program"

let kSocresUrl = BASEURL + "scores"

let kSocrescardUrl = BASEURL + "scoreboard"

let kUserUrl = BASEURL + "user"

let kPartnershipURL = BASEURL + "partnership"

let kPmomotionUrl = BASEURL + "promos"

let kPassesURL = BASEURL + "passes"

let kSupport = BASEURL + "support"

let kLeaderboardURL = BASEURL + "leaderboard"

let kKabaddiMatchURL = BASEURLKABADDI + "kabaddi/match"
let kKabaddiSocresUrl = BASEURLKABADDI + "kabaddi/scores"
let kKabaddiSocrescardUrl = BASEURLKABADDI + "kabaddi/scoreboard"
let kFootballMatchURL = BASEURLFOOTBALL + "football/match"
let kFootballSocresUrl = BASEURLFOOTBALL + "football/scores"
let kFootballSocrescardUrl = BASEURLFOOTBALL + "football/scoreboard"
let kQuizMatchURL = BASEURLKABADDI + "quiz/match"

let kBasketballMatchURL = BASEURLBASKETBALL + "basketball/match"
let kBasketballSocrescardUrl = BASEURLBASKETBALL + "basketball/scoreboard"
let kBasketballSocresUrl = BASEURLBASKETBALL + "basketball/scores"

let kBaseballMatchURL = BASEURLBASEBALL + "baseball/match"
let kBaseballSocrescardUrl = BASEURLBASEBALL + "baseball/scoreboard"
let kBaseballSocresUrl = BASEURLBASEBALL + "baseball/scores"


// PointsURl

//LetsPickURL

let LetsPickBaseURL = "http://lpcapi.letspick.io/"
let LetsPickLoginRegisterURL =  LetsPickBaseURL + "register"
let notEnoughPoints = "Not enough points to join this league"
