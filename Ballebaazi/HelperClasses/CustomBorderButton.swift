//
//  CustomBorderButton.swift
//  Letspick
//
//  Created by Vikash Rajput on 05/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class CustomBorderButton: UIButton {

    func updateLayerProperties() {
        layer.masksToBounds = false
        
        layer.cornerRadius = 5.0;
        layer.borderWidth = 1.0
        backgroundColor = UIColor.white
        layer.borderColor = UIColor(red: 0/255, green: 136/255, blue: 64/255, alpha: 1).cgColor
        setTitleColor(UIColor(red: 0/255, green: 136/255, blue: 64/255, alpha: 1), for: .normal)
        titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 14)
        if tag != 50000 {
                    
            layer.shadowColor = UIColor(red: 23.0/255, green: 37.0/255, blue: 42.0/255, alpha: 0.5).cgColor
            layer.shadowOffset = CGSize(width: 0, height: 4)
            layer.shadowOpacity = 0.3
            layer.shadowRadius = 1.1
        }
        else{
            layer.masksToBounds = true
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayerProperties()
    }
}
