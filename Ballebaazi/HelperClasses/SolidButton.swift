//
//  SolidButton.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/4/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class SolidButton: UIButton {

 
    func updateLayerProperties() {

        layer.cornerRadius = 5.0;
        titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 14)
        setTitleColor(UIColor(red: 181.0/255, green: 184.0/255, blue: 187.0/255, alpha: 1), for: .disabled)
        setTitleColor(UIColor.white, for: .normal)
        

        if isEnabled{
            backgroundColor = UIColor(red: 0/255, green: 136/255, blue: 64/255, alpha: 1)
        }
        else{
            backgroundColor = UIColor(red: 244.0/255, green: 247.0/255, blue: 249.0/255, alpha: 1)
        }
        
        if isSelected && tag == 5000{
            backgroundColor = UIColor(red: 176.0/255, green: 210.0/255, blue: 247.0/255, alpha: 1)
        }

    }


    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayerProperties()
    }
}
