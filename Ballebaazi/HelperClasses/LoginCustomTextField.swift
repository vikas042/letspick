//
//  LoginCustomTextField.swift
//  Letspick
//
//  Created by Vikash Rajput on 6/4/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
protocol LoginCustomTextFieldDelegate: UITextFieldDelegate {
    func textField(_ textField: UITextField, didDeleteBackwardAnd wasEmpty: Bool)
}

class LoginCustomTextField: UITextField {
    /*
    override func deleteBackward() {
        // see if text was empty
        let wasEmpty = text == nil || text! == ""

        // then perform normal behavior
        super.deleteBackward()

        // now, notify delegate (if existent)
        (delegate as? LoginCustomTextFieldDelegate)?.textField(self, didDeleteBackwardAnd: wasEmpty)
    }
    

    override func draw(_ rect: CGRect) {
        let placeholderText = placeholder
        if (placeholderText != nil) {
            attributedPlaceholder = NSAttributedString(string: placeholderText!, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        }

    }
    */
 

}
